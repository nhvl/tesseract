﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Enums;
using Tesseract.Core.Services.Implementation.LearningManagement;
using Tesseract.Web.Common;
using Xunit;

namespace Tesseract.Test
{
    public class ClassBuilderTest : IClassFixture<ClassBuilder>
    {
        private readonly ClassBuilder _builder = new ClassBuilder();

        [Fact]
        public async Task CanCreateTemplateAsync()
        {
            Class classItem = DummyClass();
            const long facultyId = 1;
            var template = await _builder.ExportTemplate(classItem, facultyId);

            Assert.Equal(0, template.ClassId);
            Assert.Equal(facultyId, template.CreatedBy);

            Assert.True(template.IsTemplate);
        }

        private Class DummyClass()
        {
            return new Class();
        }
    }
}