﻿using System;
using Tesseract.Web.Common;
using Xunit;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Enums;

namespace Tesseract.Test
{
    public class MediaUrlHeplerTest
    {
        [Theory]
        [InlineData("invalid Url for test")]
        [InlineData("https://example.com/quotes-are-“part”-hhh")]
        [InlineData("✪sd.xc/dsdsf--fds-fd")]
        [InlineData("example.com")]
        [InlineData("example.com/")]
        [InlineData("4:00am")]
        [InlineData("abc.txt")]
        public void OnlyGetHttps(string url)
        {
            Assert.ThrowsAsync<ArgumentException>(() => MediaUrlHepler.GetMediaInfo(url));
        }

        [Theory]
        [InlineData("https://www.youtube.com/watch?v=QH2-TGUlwu4", MediaTypeEnum.VideoLink)]
        [InlineData("https://youtube.com/watch?v=QH2-TGUlwu4", MediaTypeEnum.VideoLink)]
        [InlineData("https://youtu.be/watch?v=QH2-TGUlwu4", MediaTypeEnum.VideoLink)] 
        [InlineData("https://soundcloud.com/andreasedstr-m/rick-astley-never-gonna-give", MediaTypeEnum.AudioLink)]
        [InlineData("http://snd.sc/15Eoz92", MediaTypeEnum.AudioLink)]
        [InlineData("https://vimeo.com/43476107", MediaTypeEnum.VideoLink)]
        [InlineData("https://www.vimeo.com/43476107", MediaTypeEnum.VideoLink)]
        [InlineData("https://player.vimeo.com/43476107", MediaTypeEnum.VideoLink)]

        public void CanExtractInfoFromSupportSites(string url, MediaTypeEnum type)
        {
            Assert.True(MediaUrlHepler.GetMediaInfo(url).Result.MediaType == type);
        }

        [Theory]
        [InlineData("https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4", MediaTypeEnum.VideoLink, "video/mp4")]
        public void CanExtractInfoFromExtensions(string url, MediaTypeEnum type, string contentType)
        {
            var mediaInfo = MediaUrlHepler.GetMediaInfo(url).Result;
            Assert.Equal(type, mediaInfo.MediaType);
            Assert.Equal(contentType, mediaInfo.ContentType);
        }

        [Theory]
        [InlineData("https://upload.wikimedia.org/wikipedia/commons/transcoded/c/c0/Big_Buck_Bunny_4K.webm/Big_Buck_Bunny_4K.webm.720p.webm", MediaTypeEnum.VideoLink, "video/webm")]
        public void CanExtractInfoFromAnonymouseUrl(string url, MediaTypeEnum type, string contentType)
        {
            var mediaInfo = MediaUrlHepler.GetMediaInfo(url).Result;
            Assert.Equal(type, mediaInfo.MediaType);
            Assert.Equal(contentType, mediaInfo.ContentType);
        }
    }
}