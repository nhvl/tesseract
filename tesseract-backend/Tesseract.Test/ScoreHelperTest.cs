using System;
using System.Collections.Generic;
using Tesseract.Infrastructure.Utils;
using Xunit;

namespace Tesseract.Test
{
    public class ScoreHelperTest
    {
        [Fact]
        public void CumulativeGrade()
        {
            IEnumerable<(decimal Score, decimal MaxScore, decimal Weight, bool IsExclude, bool IsCredit)> scoreData =
                new List<(decimal Score, decimal MaxScore, decimal Weight, bool IsExclude, bool IsCredit)> {
                (Score: 90, MaxScore: 100, Weight: 2, IsExclude: false, IsCredit: false),
                (Score: 80, MaxScore: 100, Weight: 1, IsExclude: false, IsCredit: false),
            };
            Assert.Equal(0.866667m, ScoreHepler.CalculateCumulativeGrade(scoreData), 6);

            scoreData =
                new List<(decimal Score, decimal MaxScore, decimal Weight, bool IsExclude, bool IsCredit)> {
                (Score: 5, MaxScore: 10, Weight: 1, IsExclude: false, IsCredit: false),
                (Score: 50, MaxScore: 100, Weight: 1, IsExclude: false, IsCredit: false),
                (Score: 9999, MaxScore: 10000, Weight: 1, IsExclude: false, IsCredit: false)
            };
            Assert.Equal(0.666633m, ScoreHepler.CalculateCumulativeGrade(scoreData), 6);

            scoreData =
                new List<(decimal Score, decimal MaxScore, decimal Weight, bool IsExclude, bool IsCredit)> {
                (Score: 90, MaxScore: 0, Weight: 2, IsExclude: false, IsCredit: false),
            };
            Assert.Throws<DivideByZeroException>(() => ScoreHepler.CalculateCumulativeGrade(scoreData));

            // TODO: need to check and fix this test, it's not Exception anymore
            //scoreData =
            //    new List<(decimal Score, decimal MaxScore, decimal Weight, bool IsExclude, bool IsCredit)> {
            //    (Score: 90, MaxScore: 100, Weight: 0, IsExclude: false, IsCredit: false),
            //};
            //Assert.Throws<DivideByZeroException>(() => ScoreHepler.CalculateCumulativeGrade(scoreData));

            scoreData =
                new List<(decimal Score, decimal MaxScore, decimal Weight, bool IsExclude, bool IsCredit)> {
                (Score: 0, MaxScore: 100, Weight: 2, IsExclude: false, IsCredit: false),
            };
            Assert.Equal(0, ScoreHepler.CalculateCumulativeGrade(scoreData));
        }
    }
}
