﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Tesseract.Web.Common;
using Xunit;

namespace Tesseract.Test
{
    public class XssSanitizedStringConverterTest: IClassFixture<XssSanitizedStringConverter>
    {
        private class ParsingModel
        {
            public string stringValue { get; set; } = "Test string value";
            public XssSanitizedString nonXssValue { get; set; } = "Test non-xss value";
            public XssSanitizedString xssValue { get; set; } = "Test <script><ul></ul></script> value";
        }

        private readonly XssSanitizedStringConverter _converter;
        private readonly ParsingModel _testingModel = new ParsingModel();
        private readonly string _testingJson = @"{""stringValue"":""Test string value"",""nonXssValue"":""Test non-xss value"",""xssValue"":""Test <ul></ul> value""}";

        public XssSanitizedStringConverterTest(XssSanitizedStringConverter converter)
        {
            _converter = converter;
        }

        [Fact]
        public void CanSerialize()
        {
            XssSanitizedString string1 = new XssSanitizedString(null);
            Assert.Equal(_testingJson, JsonConvert.SerializeObject(_testingModel, Formatting.None, _converter));
        }

        [Fact]
        public void CanDeserialize()
        {
            var parsedModel = JsonConvert.DeserializeObject<ParsingModel>(_testingJson, _converter);
            Assert.Equal(_testingModel.stringValue, parsedModel.stringValue);
            Assert.Equal(_testingModel.nonXssValue, parsedModel.nonXssValue);
            Assert.Equal(_testingModel.xssValue, parsedModel.xssValue);
        }
    }
}