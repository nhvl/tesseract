﻿using System.Collections.Generic;
using System.IO;
using Tesseract.Web.Common;
using Xunit;

namespace Tesseract.Test
{
    public class XssSanitizedStringTest
    {
        [Fact]
        public void CanAssignFromNull()
        {
            XssSanitizedString string1 = new XssSanitizedString(null);
            Assert.Equal(string1.ToString(), string.Empty);
        }

        [Theory]
        [InlineData("Testing 1<2 & 4+1>3, now 20% off!")]
        public void CanAssignFromString(string inputString)
        {
            XssSanitizedString test = inputString;
            Assert.Equal(test.ToString(), inputString);
            Assert.Equal(new XssSanitizedString(inputString).ToString(), inputString);
        }

        [Fact]
        public void CanCompare()
        {
            const string TestString = "Testing «ταБЬℓσ»: 1<2 & 4+1>3, now 20% off!";
            XssSanitizedString string1 = TestString;
            XssSanitizedString string2 = new XssSanitizedString(TestString);
            Assert.Equal(string1.ToString(), string2.ToString());
            Assert.Equal(string1, string2);
        }

        [Fact]
        public void CanRemoveHtmlTag()
        {
            List<(string rawString, string expectedString)> testData = new List<(string, string)> {
                ("Hello <script  >alert('Oopsie')</script> script"      , "Hello alert('Oopsie') script"),
                ("Hello <style>p {color: black}</style > style"         , "Hello p {color: black} style"),
                ("Hello \n\t  World"                                    , "Hello \n\t  World"),
                ("Hello <div>test123</div > div"                        , "Hello test123 div"),
                ("Hello <li>demo</li> li"                               , "Hello <li>demo</li> li"),
                ("Hello <i>demo</i> i"                                  , "Hello <i>demo</i> i"),
                ("Hello <u>demo</u> u"                                  , "Hello <u>demo</u> u"),
                ("Hello <em>demo</em> em"                               , "Hello <em>demo</em> em"),
                ("Hello <h1>demo</h1> h1"                               , "Hello <h1>demo</h1> h1"),
                ("Hello <p>demo</p> p"                                  , "Hello <p>demo</p> p"),
                ("Hello <b>demo</b> b"                                  , "Hello <b>demo</b> b"),
                ("Hello <strong>demo</strong> strong"                   , "Hello <strong>demo</strong> strong"),
                ("Hello <ul><li>demo</li></ul> ul"                      , "Hello <ul><li>demo</li></ul> ul"),
                ("Hello <div><li>demo</li></div> divli"                 , "Hello <li>demo</li> divli"),
                ("Hello <li><div>demo</div></li> lidiv"                 , "Hello <li>demo</li> lidiv"),
                ("<a href=\"home\" onclick=\":alert(1)\">Click</a>"     , "<a href=\"home\">Click</a>"),
                ("<a href=\"javascript:alert(1)\">Click</a>"            , "<a href=\"javascript%3Aalert(1)\">Click</a>"),
            };

            foreach(var (rawString, expectedString) in testData)
            {
                Assert.Equal(expectedString, new XssSanitizedString(rawString).ToString());
            }
        }

        //[Fact] This function is for profiling performance of SanitizedString in proccessing big string. 30KB string in this scenario.
        // public void CanSanitizeBigString()
        // {
        //     string path = @".\\Resource\BigNaughtyString.txt";
        //     Assert.True(File.Exists(path));

        //     string memoryUnfriendlyString = File.ReadAllText(path);
        //     XssSanitizedString testString = new XssSanitizedString(memoryUnfriendlyString);
        //     Assert.Equal(memoryUnfriendlyString, testString.ToString());
        // }
    }
}