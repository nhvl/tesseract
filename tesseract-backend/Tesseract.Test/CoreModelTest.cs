﻿using System.Collections.Generic;
using System.IO;
using Tesseract.Web.Common;
using Xunit;
using Tesseract.Core;
using Tesseract.Core.Entities.Identity;
using System.Linq;
using System.Reflection;
using System;
using System.ComponentModel.DataAnnotations;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Test
{
    public class CoreModelTest
    {
        private IEnumerable<Type> GetEntityTypes()
        {
            var entityTypes = typeof(BaseEntity)
                .Assembly.GetTypes()
                .Where(t => typeof(BaseEntity).IsAssignableFrom(t));

            Assert.True(entityTypes.Count() > 0, "There is no entity type to check. Should be wrong domain.");
            return entityTypes.ToArray();
        }

        [Fact]
        public void EnsureNoDatetimeDefault()
        {
            var entityTypes = typeof(ApplicationUser)
                .Assembly.GetTypes()
                .Where(t => t.Namespace.StartsWith("Tesseract.Core.Entities"));

            Assert.True(entityTypes.Count() > 0, "There is no entity type to check. A wrong domain name could be used.");
            foreach (var type in entityTypes)
            {
                var props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(prop => prop.PropertyType == typeof(DateTime) && prop.CustomAttributes.All(attr => attr.AttributeType != typeof(RequiredAttribute)));

                Assert.True(props.Count() == 0, $"There could be DateTime properties with default 0 in class {type.Namespace}.{type.Name}: " + string.Join(", ", props.Select(prop => prop.Name)));
            }
        }

        [Fact]
        public void EnsureStringPropsHaveMaxLength() 
        {
            var noMaxLengthprops = new List<string>();
            var ignoredProperties = new[] {
                typeof(Comment).GetProperty(nameof(Comment.Content)),
                typeof(Discussion).GetProperty(nameof(Discussion.Content)),
                typeof(EmailTemplate).GetProperty(nameof(EmailTemplate.Body))
            };

            foreach (var type in GetEntityTypes())
            {
                var props = type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where( prop => 
                    prop.PropertyType == typeof(string) &&
                    prop.CanWrite &&
                    !ignoredProperties.Contains(prop) &&
                    prop.CustomAttributes.All(attr => attr.AttributeType != typeof(MaxLengthAttribute)));
                noMaxLengthprops.AddRange(props.Select(pr => $"{type.Name}::{pr.Name}"));
            }

            Assert.True(noMaxLengthprops.Count() == 0, $"These attribute should have max length constraint:\n" + string.Join("\n", noMaxLengthprops));
        }
    }
}