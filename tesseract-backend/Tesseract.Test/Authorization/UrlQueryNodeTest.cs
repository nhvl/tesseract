using System.Linq;
using System.Text.RegularExpressions;
using Tesseract.Core.Entities.Main;
using Tesseract.Web.Authorization.QueryChallenge;
using Xunit;

namespace Tesseract.Test.Authorization
{
    public class UrlQueryNodeTest
    {
        [Theory]
        [InlineData(@"", 0)]
        [InlineData(@"/", 0)]
        [InlineData(@"/a", 1)]
        [InlineData(@"Account/Me", 2)]
        [InlineData(@"/Account/Me", 2)]
        [InlineData(@"/Faculty/Me/Activity", 3)]
        [InlineData(@"/Faculty//Activity/", 2)]
        [InlineData(@"/Faculty/2/Activity/", 2)]
        [InlineData(@"/Faculty/3/Activity/1", 2)]
        [InlineData(@"/Faculty/Activity/2/Item/3", 3)]
        public void CanParseValue(string queryPath, int nodeCount)
        {
            var nodes = UrlQueryNode.BuildQueryTree(queryPath);
            Assert.NotNull(nodes);
            Assert.Equal(nodeCount, nodes.Count());
            if (Regex.IsMatch(queryPath, "^/Faculty/%d"))
            {
                Assert.True(nodes[0].Is.Identity());
                Assert.Equal(typeof(Faculty), nodes[0].GetNodeType());
            }
        }
    }
}
