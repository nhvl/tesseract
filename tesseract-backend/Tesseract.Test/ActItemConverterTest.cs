﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Enums;
using Tesseract.Web.Common;
using Xunit;

namespace Tesseract.Test
{
    public class ActItemConverterTest: IClassFixture<ActItemConverter>
    {
        private readonly ActItemConverter _converter;

        public ActItemConverterTest(ActItemConverter converter)
        {
            _converter = converter;
        }

        [Fact]
        public void CanSerializeV1Items()
        {
            var jsonString = @"{
                    ""NumericMin"": 1,
                    ""NumericMax"": 2,
                    ""Title"": ""test"",
                    ""Image"": """",
                    ""Content"": ""<h1><strong>test123</strong></h1>"",
                    ""Type"": 7,
                    ""MediaInfo"": {
                        ""Link"": null,
                        ""ContentType"": null,
                        ""MediaType"": 0
                    },
                    ""MatchQuizzItems"": [],
                    ""Options"": []
                }";
            var item = JsonConvert.DeserializeObject<BaseActItem>(jsonString, _converter);
            Assert.True(item != null);
            Assert.True(item is NumberActItem);
        }

        [Theory]
        [InlineData(typeof(NumberActItem), EActItemType.NumericQuestion, @" ""NumericMin"": 1,""NumericMax"": 2")]
        [InlineData(typeof(DefaultActItem), EActItemType.Text, "")]
        [InlineData(typeof(MatchActItem), EActItemType.MatchQuiz, @"""MatchQuizzItems"": [{""left"": ""1"",""right"": ""1""},{""left"": ""2"",""right"": ""2""},{""left"": ""3"",""right"": ""3""}]")]
        [InlineData(typeof(ChoiceActItem), EActItemType.ChoiceQuiz, "")]
        [InlineData(typeof(MediaActItem), EActItemType.Media, "")]
        [InlineData(typeof(EmbedActItem), EActItemType.Embed, "")]
        public void CanDeserializeItem(Type v2Type, EActItemType itemType, string extraData )
        {
            var jsonString = @"{
                    ""Title"": ""test"",
                    ""Image"": ""abc"",
                    ""Content"": ""<h1><strong>test123</strong></h1>"",
                    ""Type"": " + $"{(int)itemType}, {extraData}" + @"
                }";
            var item = JsonConvert.DeserializeObject<BaseActItem>(jsonString, _converter);
            Assert.True(item != null);
            Assert.True(item.GetType() == v2Type);
            Assert.Equal("test", item.Title);
            Assert.Equal("abc", item.Image);
            Assert.Equal("<h1><strong>test123</strong></h1>", item.Content);
        }
    }
}