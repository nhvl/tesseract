﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.IO;

namespace ApiGw
{
    public class Program
    {
        public static void Main(string[] args)
        { 
            try
            {

                CreateWebHostBuilder(args).Build().Run();
            }
            catch(Exception ex)
            {
                Log.Logger.Fatal(ex.Message);
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
 
        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                    .ConfigureAppConfiguration(ic => ic.AddJsonFile("configuration.json"))
                    .UseStartup<Startup>()
                    .ConfigureLogging((hostingContext, loggingbuilder) =>
                    {
                        loggingbuilder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                        loggingbuilder.AddConsole();
                        loggingbuilder.AddDebug();
                    })
                    .UseSerilog((builderContext, config) =>
                    {
                        config
                            .MinimumLevel.Information()
                            .Enrich.FromLogContext()
                            .WriteTo.Console();
                    });
        }
    }
}
