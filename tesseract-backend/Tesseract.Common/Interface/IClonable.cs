﻿namespace Tesseract.Common.Interface
{
    public interface IClonable<T>
    {
        T Clone();
    }
}
