﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tesseract.Common.Extensions
{
    public static class IEnumerableExtension
    {
        private static Random rng = new Random();

        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> list)
        {
            int n = list.Count();
            var newArray = list.ToArray();
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = newArray[k];
                newArray[k] = newArray[n];
                newArray[n] = value;
            }

            return newArray;
        }

        public static IEnumerable<D> SelectNotNull<S, D>(this IEnumerable<S> xs, Func<S, D?> selector) where D: struct => xs.Select(selector).Where(x => x.HasValue).Select(x => x.Value);
        public static IEnumerable<D> SelectNotNull<S, D>(this IEnumerable<S> xs, Func<S, D> selector) => xs.Select(selector).Where(x => x != null);



        public static Dictionary<T, U> ToDictionary<T, U>(this IEnumerable<KeyValuePair<T, U>> keypairs) => keypairs.ToDictionary(x => x.Key, y => y.Value);
        public static async Task<Dictionary<T, U>> ToDictionaryAsync<T, U>(this Task<IEnumerable<KeyValuePair<T, U>>> keypairsTask) => (await keypairsTask).ToDictionary(x => x.Key, y => y.Value);
        public static async Task<Dictionary<T, U>> ToDictionaryAsync<T, U>(this Task<KeyValuePair<T, U>[]> keypairsTask) => (await keypairsTask).ToDictionary(x => x.Key, y => y.Value);

        public static async Task ForEachAsync<T>(this IEnumerable<T> list, Func<T, Task> func)
        {
            foreach (var value in list)
            {
                await func(value);
            }
        }
    }
}
