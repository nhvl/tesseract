using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tesseract.Common.Extensions
{
    public static class ListExtension
    {
        public static T Pop<T>(this List<T> list)
        {
            int lastIndex = list.Count - 1;
            T top = list[lastIndex];
            list.RemoveAt(lastIndex);
            return top;
        }

        public static async Task ForEachAsync<T>(this List<T> list, Func<T, Task> func)
        {
            foreach (var value in list)
            {
                await func(value);
            }
        }
        public static async Task<T> FirstOrDefaultAsync<T>(this Task<IList<T>> task) => (await task).FirstOrDefault();

    }
}
