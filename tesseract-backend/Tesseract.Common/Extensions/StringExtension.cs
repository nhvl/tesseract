using System;

namespace Tesseract.Common.Extensions
{
    public static class StringExtension
    {
        public static bool IsLong(this string self) => long.TryParse(self, out long _);
        public static long? ToLong(this string self) => long.TryParse(self, out long value) ? value as long? : null ;
        public static string TruncateLongString(this string str, int maxLength) => str?.Substring(0, Math.Min(str.Length, maxLength));
    }
}
