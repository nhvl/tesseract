﻿using CommandLine;
using Microsoft.Azure.Documents;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Config;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Data.NoSql;
using Tesseract.Web.Common;

namespace Tesseract.Executable
{
    class Program
    {
        [Verb("cloneCosmosDb", HelpText = "Clone cosmos data")]
        class CloneCosmosOptions
        {
            [Option("src", Required = true, HelpText = "Url of Cosmos DB to copy from.")]
            public string SrcUrl { get; set; }
            [Option("des", Required = true, HelpText = "Url of Cosmos DB to copy to.")]
            public string DesUrl { get; set; }

            [Option("src-db", Required = true, HelpText = "Source Database.")]
            public string SrcDb { get; set; }
            [Option("des-db", Required = true, HelpText = "Destination Database.")]
            public string DesDb { get; set; }

            [Option("src-key", Required = true, HelpText = "Key of source Database.")]
            public string SrcKey { get; set; }
            [Option("des-key", Required = true, HelpText = "Key of destination Database.")]
            public string DesKey { get; set; }
        }

        static int Main(string[] args)
        {
            return Parser.Default.ParseArguments<CloneCosmosOptions>(args)
                    .MapResult(
                        (CloneCosmosOptions opts) => RunCloneCosmos(opts),
                        errs => 1
                        );
        }

        static int RunCloneCosmos(CloneCosmosOptions opts)
        {
            var srcDbSettings = new DocumentDbSettings
            {
                DatabaseId = opts.SrcDb,
                EndpointUrl = opts.SrcUrl,
                MasterKey = opts.SrcKey,
            };

            var serializerSetting = new JsonSerializerSettings();
            serializerSetting.ContractResolver = new CamelCasePropertyNamesContractResolver();
            serializerSetting.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            serializerSetting.Converters.Add(new DatetimeToNumberConverter());
            serializerSetting.Converters.Add(new XssSanitizedStringConverter());
            serializerSetting.Converters.Add(new ActItemConverter());
            serializerSetting.Converters.Add(new TimeSpanToNumberConverter());

            var srcRepo = new DocumentDBRepository(Options.Create(srcDbSettings), null, Options.Create(serializerSetting));

            var desDbSettings = new DocumentDbSettings
            {
                DatabaseId = opts.DesDb,
                EndpointUrl = opts.DesUrl,
                MasterKey = opts.DesKey,
            };

            var desRepo = new DocumentDBRepository(Options.Create(desDbSettings), null, Options.Create(serializerSetting));
            DocumentDBInitializer.Init(desRepo, null);

            Sync<StudentActDoc>(srcRepo, desRepo).Wait();

            var syncMethod = typeof(Program).GetMethod(nameof(Program.Sync));

            var tasks = DocumentDBInitializer.GetNoSqlDataTypes().Select(noSqlType => Task.Run(() =>
            {
                var fetchTask = syncMethod.MakeGenericMethod(noSqlType).Invoke(null, new[] { srcRepo, desRepo }) as Task;
                fetchTask.Wait();
            })).ToArray();

            Task.WaitAll(tasks);
            return 0;
        }

        public static async Task Sync<T>(DocumentDBRepository srcRepo, DocumentDBRepository desRepo) where T : BaseDocument
        {
            var records = await srcRepo.GetAll<T>();
            var Ids = CsvReadHelper.ReaderFromFile<StudentItem>(@"C:\Users\nhvl\Desktop\Book1.csv").ToList();
            foreach (var record in records)
            {
                var studentId = TryGetProperty(record, "StudentId");
                var newStudentId = studentId;
                foreach(var s  in Ids)
                {
                    if (studentId.ToString() == s.StudentId.ToString())
                    {
                        newStudentId = s.UserId.ToString();
                        break;
                    }
                }
                
                if(long.TryParse(newStudentId.ToString(), out long newId))
                {
                    Console.WriteLine(studentId + " " + newStudentId);
                    TrySetProperty(record, "StudentId", newId);
                    await desRepo.UpsertAsync<T>(record);
                }                
            }
        }
        private class StudentItem
        {
            public long StudentId { get; set; }
            public long UserId { get; set; } }
        private static void TrySetProperty(object obj, string property, object value)
        {
            var prop = obj.GetType().GetProperty(property, BindingFlags.Public | BindingFlags.Instance);
            if (prop != null && prop.CanWrite)
                prop.SetValue(obj, value, null);
        }
        private static object TryGetProperty(object obj, string property)
        {
            var prop = obj.GetType().GetProperty(property, BindingFlags.Public | BindingFlags.Instance);
            if (prop != null && prop.CanRead)
                return prop.GetValue(obj);
            return "";
        }
    }
}
