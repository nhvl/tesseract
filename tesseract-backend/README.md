
## Config

On `Tesseract.Web` project, you can create an `appsettings.local.json` file to override the settings in `appsettings.json`.

`appsettings.local.json`

```json
{
    "ConnectionStrings": {
        "DefaultConnection": "Server=(local);Integrated Security=false;Initial Catalog=TesseractNew; User Id=sa;Password=asd123;"
    }
}
```

## Store databae

## Update schemma

1. Update Entity class
2. Update `MainDbContext.cs`
3. Create EF Migration
    ```cmd
    cd .\Tesseract.Web\
    dotnet ef migrations add --project ..\Tesseract.Infrastructure\Tesseract.Infrastructure.csproj --context MainDbContext MIGRATION_B
    ```
4. Update DB:

    1. Auto migrate (`DatabaseSettings.AutoMigration = true`)

        Just build and run

    2. Manuallly:

        ```bash
        dotnet ef database update --context MainDbContext
        ```
5. Create script:
    ```cmd
    dir ..\..\DBscripts
    dotnet ef migrations list --context MainDbContext
    dotnet ef migrations script --context MainDbContext MIGRATION_A --output ../../DBscripts/0006_MIGRATION_B.sql
    ```
    
6. Run SQL in Server

    ```powershell
    Add-Migration <name> -Project Tesseract.Infrastructure
    Script-Migration -Output "../DBscripts/<id like 0001>_<name>.sql"
    Remove-Migration -Project Tesseract.Infrastructure
    ```

## List of urls the backend support
*  /swagger

## Useful Commands

* Set User Secret for project to overwrite appSettings
    ```bash
    dotnet user-secrets set "ConnectionStrings:DefaultConnection" <connection-string> --project "tesseract-backend/Tesseract.Web/Tesseract.Web.csproj"
    ```

## Run XUnit test

```bash
dotnet test tesseract\tesseract-backend\Tesseract.Test\Tesseract.Test.csproj
```

## Install

```bash
dotnet tool install --global dotnet-ef
```
