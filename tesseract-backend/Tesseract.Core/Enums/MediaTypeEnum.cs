﻿namespace Tesseract.Core.Enums
{
    public enum MediaTypeEnum
    {
        ImageFile = 1,
        VideoFile = 2,
        AudioFile = 3,
        VideoLink = 4,
        AudioLink = 5,
        ImageLink = 6,
    }
}