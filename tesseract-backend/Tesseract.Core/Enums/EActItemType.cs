﻿namespace Tesseract.Core.Enums
{
    public enum EActItemType
    {
        Heading = 1,
        Text = 2,
        Media = 3,

        Embed = 5, // Google Map | Drive Slide | Drive Doc | Driver Sheet

        Discussion = 6,
        NumericQuestion = 7,
        ChoiceQuiz = 8,
        MatchQuiz = 9,
        TextQuiz = 10,
        PollQuiz = 11,
    }
}
