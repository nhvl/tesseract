﻿namespace Tesseract.Core.Enums
{
    public enum AuditActionType
    {
        Unknown = 0,
        Create,
        Update,
        Delete
    }
}