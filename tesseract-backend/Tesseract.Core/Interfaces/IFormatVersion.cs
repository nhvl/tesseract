
namespace Tesseract.Core.Entities.NoSql.Interfaces
{
    public interface IFormatVersion
    {
        int FormatVersion { get; set; }
    } 
}