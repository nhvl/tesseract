﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tesseract.Core.Interfaces
{
    public interface ITimeTraveling
    {
        /// <summary>
        /// Travel object throuch time.
        /// </summary>
        /// <param name="from">The time beginning point. </param>
        /// <param name="to">The target point.</param>
        /// <param name="limit">Limit info.</param>
        void TimeTravel(DateTime from, DateTime to, DateTime? limit = null);
    }

    public static class DateTimeTravelExtension
    {
        public static DateTime Travel(this DateTime self , DateTime from, DateTime to, DateTime? limit = null)
        {
            var newTime = to + (self - from);
            return limit == null || newTime < limit ? newTime : limit.Value;
        }
    }
}
