﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Services.Interface.LearningManagement;

namespace Tesseract.Core.Services.Implementation.LearningManagement
{
    public class ClassBuilder: IClassBuilder
    {
        public Task<Class> BuildFromtemplate(Class template, GradingTerm gradingTerm, long facultyId, string className = null)
        {
            if (template.Activities == null) throw new ArgumentNullException(nameof(template.Activities));
            if (template.Term == null) throw new ArgumentNullException(nameof(template.Term));
            if (gradingTerm == null) throw new ArgumentNullException(nameof(gradingTerm));

            var newClass = template.Clone();
            newClass.GradingTerm = gradingTerm.GradingTermId;
            newClass.IsTemplate = false;
            if (! string.IsNullOrEmpty(className)){
                newClass.ClassName = className;
            }

            var activityCloneMap = template.Activities.ToDictionary(act => act.ActivityId, act => act.Clone());
            foreach (var templateActivity in activityCloneMap.Values)
            {
                templateActivity.TimeTravel(template.Term.StartDate, gradingTerm.StartDate, null);
                if (templateActivity.ParentActivityId.HasValue)
                {
                    templateActivity.Parent = activityCloneMap.TryGetValue(templateActivity.ParentActivityId.Value, out Activity newParent) ? newParent : null;
                }
                else
                {
                    templateActivity.Parent = null;
                }


                templateActivity.ParentActivityId = null;
                newClass.Activities.Add(templateActivity);
            }

            return Task.FromResult(newClass);
        }

        public Task<Class> ExportTemplate(Class classItem, long facultyId)
        {
            if (classItem.Activities == null) throw new ArgumentNullException(nameof(classItem.Activities));

            var templateClass = classItem.Clone();
            templateClass.IsTemplate = true;
            templateClass.CreatedBy = facultyId;
            templateClass.ClassName = CreateTemplateName(templateClass);

            var activityCloneMap = classItem.Activities.ToDictionary(act => act.ActivityId, act => act.Clone());
            foreach (var act in activityCloneMap.Values)
            {
                if (act.ParentActivityId.HasValue)
                {
                    act.Parent = activityCloneMap.TryGetValue(act.ParentActivityId.Value, out Activity newParent) ? newParent : null;
                }
                else
                {
                    act.Parent = null;
                }

                act.ParentActivityId = null;

                templateClass.Activities.Add(act);
            }

            return Task.FromResult(templateClass);
        }

        private string CreateTemplateName(Class classItem) => !string.IsNullOrEmpty(classItem?.ClassName) ? $"{classItem.ClassName} Template" : string.Empty;
    }
}