﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Core.Services.Interface.LearningManagement
{
    public interface IClassBuilder
    {
        Task<Class> BuildFromtemplate(Class template, GradingTerm gradingTerm, long facultyId, string className = null);
        Task<Class> ExportTemplate(Class classItem, long facultyId);
    }
}
