﻿using System;

namespace Tesseract.Core.Services.Interface.Authorization
{
    public class EntityIndex<Tkey>
    {
        public EntityIndex(Type entityType, Tkey key)
        {
            Key = key;
            EntityType = entityType;
        }

        public Tkey Key { get; set; }
        public Type EntityType { get; set; }
        public override string ToString() => $"{EntityType}[{Key}]";
    }
}
