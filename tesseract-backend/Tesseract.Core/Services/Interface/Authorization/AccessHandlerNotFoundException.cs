﻿using System;
using System.Runtime.Serialization;

namespace Tesseract.Core.Services.Interface.Authorization
{
    public class AccessHandlerNotFoundException : Exception
    {
        public AccessHandlerNotFoundException()
        {
        }

        public AccessHandlerNotFoundException(string message) : base(message)
        {
        }

        public AccessHandlerNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AccessHandlerNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
