﻿using System.Threading.Tasks;

namespace Tesseract.Core.Services.Interface.Authorization
{
    /// <summary>
    /// Get the access entity handler.
    /// </summary>
    public interface IResouceAccessHandler
    {
        /// <summary>
        /// Find the entity handler of exception.
        /// </summary>
        /// <param name="identity">Identity index.</param>
        /// <returns>IEntityHandler</returns>
        /// <exception cref="AccessHandlerNotFoundException"></exception>
        IEntityHandler Identity(EntityIndex<long> identity);
    }

    public interface IEntityHandler
    {
        /// <summary>
        /// Whether Entity can access resource.
        /// </summary>
        /// <param name="resource">Resouce index.</param>
        /// <returns>Whether Entity can access resource.</returns>
        /// <exception cref="AccessHandlerNotFoundException"></exception>
        Task<bool> CanAccess(EntityIndex<string> resource);
    }
}
