﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Core.Services.Interface.Repository
{
    public interface IClassRepo
    {
        Task<Class> AddTemplate(Class template);
        Task<Class> AddClass(Class classItem, long facultyId);

        Task<Class> GetClass(long classId);
        Task<Class> GetTemplate(long templateId);
        Task<IEnumerable<Class>> GetTemplates(long facultyId);
        Task DeleteTemplate(long templateId);
    }
}
