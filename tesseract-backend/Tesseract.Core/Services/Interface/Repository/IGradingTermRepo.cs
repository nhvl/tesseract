﻿using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Core.Services.Interface.Repository
{
    public interface IGradingTermRepo
    {
        Task<GradingTerm> GetTerm(long GradingTermId);
    }
}
