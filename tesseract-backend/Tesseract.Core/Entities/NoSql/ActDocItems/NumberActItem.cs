﻿namespace Tesseract.Core.Entities.NoSql
{
    public class NumberActItem : BaseActItem
    {
        public decimal? NumericMin { get; set; }
        public decimal? NumericMax { get; set; }
    }
}