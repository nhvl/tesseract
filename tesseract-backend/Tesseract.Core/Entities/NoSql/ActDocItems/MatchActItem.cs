﻿using System;
using System.Linq;
using Tesseract.Common.Extensions;
namespace Tesseract.Core.Entities.NoSql
{
    public class MatchActItem : BaseActItem
    {
        public MatchQuizzItem[] MatchQuizzItems { get; set; }

        public MatchQuizzItem[] MatchQuizzItemPreview(){
            if (MatchQuizzItems == null) return null;
            var lefts = MatchQuizzItems.Select(i => (left: i.Left, leftImage: i.LeftImage)).ToArray();
            var rights = MatchQuizzItems.Select(i => (right: i.Right, rightImage: i.RightImage)).Shuffle().ToList();

            if (lefts.Length != rights.Count){
                throw new InvalidOperationException(); 
            }
            
            return lefts.Select(l => {
                var (right, rightImage) = rights.Pop();
                return new MatchQuizzItem { Left = l.left, LeftImage = l.leftImage, Right = right, RightImage = rightImage };
            }).ToArray();
        }
    }
    public class MatchQuizzItem
    {
        public string Left { get; set; }
        public string LeftImage { get; set; }
        public string Right { get; set; }
        public string RightImage { get; set; }
    }
}