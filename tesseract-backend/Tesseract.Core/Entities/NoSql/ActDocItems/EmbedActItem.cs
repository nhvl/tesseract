using Tesseract.Core.Enums;

namespace Tesseract.Core.Entities.NoSql
{
    public class EmbedActItem : BaseActItem
    {
        public EmbedInfo EmbedInfo { get; set; }
    }

    public class EmbedInfo
    {
        public EmbedInfo() { }

        public EmbedInfo(string link, string embedApp = null,string embedId = null, string contentType = null, string type = null, long lastEditedUtc = 0)
        {
            this.Link = link;
            this.ContentType = contentType;
            this.EmbedId = embedId;
            this.EmbedApp = embedApp;
            this.LastEditedUtc = lastEditedUtc;
            this.Type = type;
        }
        public string EmbedId { get; set; }
        public string Link { get; set; }
        public string ContentType { get; set; } // mime
        public string EmbedApp { get; set; }
        public string Type { get; set; }
        public long LastEditedUtc { get; set; }
    }
}