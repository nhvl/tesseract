﻿using System;
using Tesseract.Core.Entities.NoSql.Interfaces;
using Tesseract.Core.Enums;

namespace Tesseract.Core.Entities.NoSql
{
    public class BaseActItem: IFormatVersion
    {
        public BaseActItem() { }
        public int FormatVersion { get; set; } = 2;
        public string Id { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string Content { get; set; }
        public EActItemType Type { get; set; } 
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class DefaultActItem : BaseActItem { };
}
