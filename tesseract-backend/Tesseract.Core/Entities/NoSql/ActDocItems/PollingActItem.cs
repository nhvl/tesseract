﻿namespace Tesseract.Core.Entities.NoSql
{
    public class PollingActItem : BaseActItem
    {
        public PollQuizItem[] PollItems { get; set; }
    }
    public class PollQuizItem
    {
        public string Label { get; set; }
        public string Image { get; set; }
    }
}