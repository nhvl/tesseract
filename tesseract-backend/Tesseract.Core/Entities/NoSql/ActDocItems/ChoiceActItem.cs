﻿using System;

namespace Tesseract.Core.Entities.NoSql
{
    public class ChoiceActItem : BaseActItem
    {
        public bool IsMultipleAnswer { get; set; }
        public ActOption[] Options { get; set; }
    }
    public class ActOption
    {
        public string Label { get; set; }
        public string Image { get; set; }
        public bool IsCorrect { get; set; }

        public override bool Equals(object obj)
        {
            return obj is ActOption option &&
                   Label == option.Label &&
                   Image == option.Image;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Label, Image);
        }
    }
}