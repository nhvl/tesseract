﻿namespace Tesseract.Core.Entities.NoSql
{
    public class DiscussionActItem : BaseActItem
    {
        public long DiscussionId { get; set; }
    }
}
