﻿using Tesseract.Core.Enums;

namespace Tesseract.Core.Entities.NoSql
{
    public class MediaActItem : BaseActItem
    {
        public MediaInfo MediaInfo { get; set; }
    }

    public class MediaInfo
    {
        public MediaInfo() { }

        public MediaInfo(string link, MediaTypeEnum mediaType, string contentType = null)
        {
            this.Link = link;
            this.MediaType = mediaType;
            this.ContentType = contentType;
        }

        public string Link { get; set; }
        public string ContentType { get; set; } // mime
        public MediaTypeEnum MediaType { get; set; } // imageFile | videoFile | audioFile | videoLink | audioLink
    }
}