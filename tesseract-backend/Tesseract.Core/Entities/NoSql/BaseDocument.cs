﻿using Newtonsoft.Json;
using System;

namespace Tesseract.Core.Entities.NoSql
{
    public abstract class BaseDocument
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "dateCreated")]
        public DateTime? DateCreated { get; set; }
        [JsonProperty(PropertyName = "dateUpdated")]
        public DateTime? DateUpdated { get; set; }
        [JsonProperty(PropertyName = "createdBy")]
        public long? CreatedBy { get; set; }
        [JsonProperty(PropertyName = "updatedBy")]
        public long? UpdatedBy { get; set; }
        [JsonProperty(PropertyName = "isDeleted")]
        public bool IsDeleted { get; set; }

        [JsonProperty("_etag")]
        public string ETag { get; set; }
    }
}