﻿using Newtonsoft.Json;

namespace Tesseract.Core.Entities.NoSql
{
    public class SubmissionAttachment
    {
        //[JsonProperty(PropertyName = "id", Required = Required.Always)]
        //public string Id { get; set; }

        [JsonProperty(PropertyName = "contentType", Required = Required.Always)]
        public string ContentType { get; set; }

        [JsonProperty(PropertyName = "fileName", Required = Required.Always)]
        public string FileName { get; set; }

        [JsonProperty(PropertyName = "url", Required = Required.Always)]
        public string Url { get; set; }
    }

    public class Submission : BaseDocument
    {
        [JsonProperty(PropertyName = "activityId", Required = Required.Always)]
        public long ActivityId { get; set; }

        [JsonProperty(PropertyName = "studentId", Required = Required.Always)]
        public long StudentId { get; set; }

        [JsonProperty(PropertyName = "content")]
        public string Content { get; set; }

        [JsonProperty(PropertyName = "attachments")]
        public SubmissionAttachment[] Attachments { get; set; }
    }
}

