﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Core.Entities.NoSql
{
    public class DistrictReport
    {
        private SchoolReport[] schoolReports;
        public long DistrictId { get; set; }
        public SchoolReport[] SchoolReports
        {
            get => schoolReports;
            set
            {
                schoolReports = value;
                if (schoolReports == null)
                {
                    FacultyCount = new ActivePassiveCount();
                    return;
                }

                var actives = schoolReports.SelectMany(sr => sr.XSchoolFaculties.Active).Select(falId => (FalId: falId, IsActive: true)).ToArray();
                var passives = schoolReports.SelectMany(sr => sr.XSchoolFaculties.Passive).Select(falId => (FalId: falId, IsActive: true)).ToArray();

                var xfacultyLookup = actives.Concat(passives)
                                            .GroupBy(record => record.FalId)
                                            .Where(group => group.Count() > 1)
                                            .ToLookup(group => group.Any(rc => rc.IsActive), group => group.Count() - 1);

                var xFacultyModifier = new ActivePassiveCount { Active = Enumerable.Sum(xfacultyLookup[true]), Passive = Enumerable.Sum(xfacultyLookup[false]) };

                FacultyCount = schoolReports.Select(sr => sr.FacultyCount).Aggregate((sr1, sr2) => sr1 + sr2) - xFacultyModifier;
            }
        }

        public ActivePassiveCount FacultyCount { get; private set; }
    }

    public class SchoolReport : BaseDocument
    {
        public SchoolReport(){}
        public SchoolReport (long schoolId, long gradingTerm){
            this.SchoolId = schoolId;
            this.GradingTerm = gradingTerm;
        }

        public long SchoolId { get; set; }
        public long GradingTerm { get; set; }
        public ActivePassiveCount ActivityCount { get; set; }
        public ActivePassiveCount FacultyCount { get; set; }
        public ActivePassiveCount StudentCount { get; set; }

        public FiveCScore FiveCScore { get; set; }

        public (IEnumerable<long> Active, IEnumerable<long> Passive) XSchoolFaculties { get; set; }
    }

    public class FiveCScore
    {
        public Dictionary<byte, int> Communication { get; set; }
        public Dictionary<byte, int> Collaboration { get; set; }
        public Dictionary<byte, int> Character { get; set; }
        public Dictionary<byte, int> Creativity { get; set; }
        public Dictionary<byte, int> CriticalThinking { get; set; }

        public FiveCScore() { }
        public FiveCScore(IEnumerable<ActivityScore> scores)
        {
            this.Character        = CreateDictionary(scores.Select(score => score.Character));
            this.Collaboration    = CreateDictionary(scores.Select(score => score.Collaboration));
            this.Creativity       = CreateDictionary(scores.Select(score => score.Creativity));
            this.Communication    = CreateDictionary(scores.Select(score => score.Communication));
            this.CriticalThinking = CreateDictionary(scores.Select(score => score.CriticalThinking));

            Dictionary<byte, int> CreateDictionary(IEnumerable<byte?> scoreValues) =>
               scoreValues.Where(s => IsValidFiveCScore(s))
                   .GroupBy(x => x.Value)
                   .ToDictionary(x => x.Key, y => y.Count());
        }

        private static bool IsValidFiveCScore(byte? score)
        {
            return score != null && score.HasValue && 0 < score.Value && score.Value <= 3;
        }
    }

    public class ActivePassiveCount
    {
        public ActivePassiveCount()
        {
            // For JSON Serialization
        }
        public ActivePassiveCount(int total, int active)
        {
            if (total < active)
            {
                throw new ArgumentException($"{active} of {total}");
            }

            this.Active = active;
            this.Passive = total - active;
        }

        public int Active { get; set; }
        public int Passive { get; set; }
        private int Total() => Active + Passive;

        public static ActivePassiveCount operator +(ActivePassiveCount a, ActivePassiveCount b) => new ActivePassiveCount(a.Total() + b.Total(), a.Active + b.Active);
        public static ActivePassiveCount operator -(ActivePassiveCount a, ActivePassiveCount b) => new ActivePassiveCount(a.Total() - b.Total(), a.Active - b.Active);
    }
}

