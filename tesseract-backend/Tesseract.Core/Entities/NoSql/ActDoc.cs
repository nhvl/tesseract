﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Tesseract.Common.Interface;
using Tesseract.Core.Interfaces;

namespace Tesseract.Core.Entities.NoSql
{

    public abstract class BaseActDoc : BaseDocument
    {
        [JsonProperty(PropertyName = "activityId", Required = Required.Always)]
        public long ActivityId { get; set; }

        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "banner")]
        public string Banner { get; set; }

        [JsonProperty(PropertyName = "summary")]
        public string Summary { get; set; }

    }
    public class ActDoc : BaseActDoc, IClonable<ActDoc>, ITimeTraveling
    {
        [JsonProperty(PropertyName = "items")]
        public BaseActItem[] Items { get; set; }

        /// <summary>
        /// Clone an <see cref="ActDoc"/> class to reuse its data for another activity.
        /// </summary>
        /// <param name="activityId">Activity of new act doc</param>
        /// <returns></returns>
        public ActDoc Clone(long activityId)
        {
            var newActDoc = new ActDoc();
            newActDoc.ActivityId = activityId;
            newActDoc.CopyContentFrom(this);
            return newActDoc;
        }

        public ActDoc Clone()
        {
            var actDoc = CloneExtensions.CloneFactory.GetClone(this);
            return actDoc;
        }

        public void TimeTravel(DateTime from, DateTime to, DateTime? limit = null)
        {
            if (this.Items == null) return;

            foreach(var item in Items)
            {
                item.StartDate = item.StartDate?.Travel(from, to, limit);
                item.EndDate = item.EndDate?.Travel(from, to, limit);
            }
        }

        public void CopyContentFrom(ActDoc copyFromDoc){
            this.Banner = copyFromDoc.Banner;
            this.Summary = copyFromDoc.Summary;
            this.Title = copyFromDoc.Title;
            this.Items = copyFromDoc.Items;
        }
    }

    public class StudentActDoc : ActDoc
    {
        public StudentActDoc(){}
        public StudentActDoc(ActDoc doc){
            this.CopyContentFrom(doc);
            this.Answers = doc.Items.Where(x => x != null && x.Id != null).ToDictionary(x => x.Id, x => null as string);
        }

        [JsonProperty(PropertyName = "studentId", Required = Required.Always)]
        public long StudentId { get; set; }

        [JsonProperty(PropertyName = "answers")]
        public Dictionary<string, string> Answers { get; set; }
    }
}
