﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Tesseract.Core.Enums;

namespace Tesseract.Core.Entities.Log
{
    public class AuditChange
    {
        public string TableName { get; set; }
        public List<KeyValuePair<string, object>> PrimaryKey { get; set; }
        [Required]
        public DateTime DateTimeStamp { get; private set; } = DateTime.UtcNow;
        public AuditActionType AuditAction { get; set; }
        public List<AuditDelta> Changes { get; set; }
        public List<PropertyEntry> TemporaryProperties { get; set; }

        public AuditChange()
        {
            Changes = new List<AuditDelta>();
            TemporaryProperties = new List<PropertyEntry>();
        }
    }
}
