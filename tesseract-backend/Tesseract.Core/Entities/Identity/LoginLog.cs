﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tesseract.Core.Entities.Identity
{
    [Table("LoginLogs")]
    public class LoginLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long LoginLogID { get; set; }

        [MaxLength(50)]
        public string LoginContent { get; set; }
        public bool Success { get; set; }
        public DateTime? LoginTime { get; set; }
        [MaxLength(20)]
        public string RemoteIP { get; set; }
        public long? UserID { get; set; }
        [MaxLength(50)]
        public string Domain { get; set; }
        [MaxLength(50)]
        public string Role { get; set; }
    }
}
