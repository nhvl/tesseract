﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Core.Entities.Identity
{
    public class ApplicationUser : IdentityUser<long>
    {
        [MaxLength(70)]
        public string FirstName { get; set; }
        [MaxLength(70)]
        public string LastName { get; set; }
        [MaxLength(70)]
        public string Salutation { get; set; }
        [MaxLength(70)]
        public string Nickname { get; set; }

        public long ActiveSchoolId { get; set; }

        [MaxLength(20)]
        public string Language { get; set; }

        [MaxLength(2083)]
        public string Avatar { get; set; }

        public DateTime? Dob { get; set; }

        public DateTime? LastActive { get; set; }

        public long DistrictId { get; set; }

        [NotMapped]
        public string FullName => $"{LastName}, {FirstName}";
        [NotMapped]
        public string DisplayName => string.IsNullOrEmpty(Nickname) ? $"{Salutation} {FirstName} {LastName}".Trim() : Nickname;

        public ICollection<ActivityScore> Scores { get; set; }

        #region Foreign Key shadow properties
        private ICollection<SchoolStudent> SchoolStudents { get; } = new List<SchoolStudent>();
        private ICollection<SchoolFaculty> SchoolFaculties { get; } = new List<SchoolFaculty>();
        private ICollection<ParentStudent> ParentsOfStudent { get; } = new List<ParentStudent>();
        private ICollection<ParentStudent> StudentsOfParent { get; } = new List<ParentStudent>();
        private ICollection<ClassFaculty> ClassFaculties { get; } = new List<ClassFaculty>();
        private ICollection<ClassStudent> ClassStudents { get; } = new List<ClassStudent>();
        public virtual Parent Parent { get; set; }
        public virtual Faculty Faculty { get; set; }
        public virtual Student Student { get; set; }
        #endregion
    }
}
