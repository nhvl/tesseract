﻿using System.Security.Claims;

namespace Tesseract.Core.Entities.Identity
{
    public class Permission
    {
        public const string ClaimType = "AspNetUserPermissionClaim";

        public string Name { get; set; }
        private Permission(string name) => Name = name;
        public override string ToString() => this.Name;
        public Claim ToClaim() => new Claim(ClaimType, this.Name);

        public static Permission CanEditClass      => new Permission(Cons.CanEditClass);
        public static Permission CanAddStudent     => new Permission(Cons.CanAddStudent);
        public static Permission CanEditSchoolRole => new Permission(Cons.CanEditSchoolRole);

        public static Permission[] All => new [] {CanEditClass, CanAddStudent, CanEditSchoolRole};

        public static class Cons
        {
            public const string CanEditClass      = nameof(CanEditClass);
            public const string CanAddStudent     = nameof(CanAddStudent);
            public const string CanEditSchoolRole = nameof(CanEditSchoolRole);
        }
    }
}
