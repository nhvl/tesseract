﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Tesseract.Core.Entities.Identity
{
    public class ApplicationRole : IdentityRole<long>
    {
        [MaxLength(2048)]
        public string Description { get; set; }

        public bool IsSystemRole { get; set; }

        public ApplicationRole() : base() { }
        public ApplicationRole(string roleName, bool isSystemRole = true) : base(roleName)
        {
            this.IsSystemRole = isSystemRole;
        }
    }
    public class ApplicationRoleItem
    {
        public string Name { get; set; }
        public bool IsSystemRole { get; set; }
    }
    public static class ApplicationRoleValue
    {
        public static ApplicationRoleItem[] All() => new[] {
            new ApplicationRoleItem{Name=Admin,IsSystemRole=true}
            , new ApplicationRoleItem{Name=Faculty,IsSystemRole=true}
            , new ApplicationRoleItem{Name=Parent,IsSystemRole=true}
            , new ApplicationRoleItem{Name=Student,IsSystemRole=true}
            , new ApplicationRoleItem{Name=SuperAdmin,IsSystemRole=true}
            , new ApplicationRoleItem{Name=SchoolAdmin,IsSystemRole=true}
            , new ApplicationRoleItem{Name=StudentManager,IsSystemRole=false}};
        public const string Admin = nameof(Admin);
        public const string Faculty = nameof(Faculty);
        public const string Parent = nameof(Parent);
        public const string Student = nameof(Student);
        public const string SuperAdmin = nameof(SuperAdmin);
        public const string SchoolAdmin = nameof(SchoolAdmin);
        public const string StudentManager = nameof(StudentManager);
    }
}
