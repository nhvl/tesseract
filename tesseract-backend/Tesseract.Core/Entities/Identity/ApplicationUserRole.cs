﻿using Microsoft.AspNetCore.Identity;

namespace Tesseract.Core.Entities.Identity
{
    public class ApplicationUserRole : IdentityUserRole<long>
    {
        public long ApplicationUserRoleId { get; set; }
        public long? SchoolId { get; set; }
    }
}