﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;
using Tesseract.Common.Interface;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Interfaces;

namespace Tesseract.Core.Entities.Main
{
    public enum ActivityType {
        Activity = 1,
        Assignment = 2,
        Assessment = 3,
        Discussion = 4,
    }

    [Table("Activities")]
    public class Activity : BaseEntity, IClonable<Activity>, ITimeTraveling
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ActivityId { get; set; }

        public long ClassId { get; set; }

        public ActivityType Type { get; set; }

        [MaxLength(200)]
        public string Title { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        public DateTime? DateDue { get; set; }
        public DateTime? DateAssigned { get; set; }
        public DateTime? DateCutoff { get; set; }

        public int SortIndex { get; set; }

        // ActivityType.Activity
        public long? ParentActivityId { get; set; }
        public DateTime? PublishStartDate { get; set; }
        public DateTime? PublishEndDate { get; set; }

        // ActivityType.Assignment & Assessment
        public long? CategoryId { get; set; }
        [MaxLength(10)]
        public string Color { get; set; }
        public bool IsGraded { get; set; }

        [Column(TypeName = "decimal(9, 2)")]
        public decimal MaxScore { get; set; }

        [Column(TypeName = "decimal(9, 2)")]
        public decimal Weight { get; set; }

        public bool IsCredit { get; set; }

        [NotMapped]
        public ActDoc Document{ get; set;}
        [NotMapped]
        public Activity Parent{ get; set;}

        // --------

        public virtual Class Class { get; set; }
        public ICollection<ActivityScore> Scores { get; set; }

        // --------

        public bool IsPublic() => this.DateAssigned.HasValue && this.DateAssigned.Value <= DateTime.UtcNow;
        public bool IsOverdue() => this.DateDue.HasValue && this.DateDue.Value <= DateTime.UtcNow;
        public bool IsOpen() => this.DateDue.HasValue && this.DateDue.Value > DateTime.UtcNow;
        public bool IsAllowSubmission() {
            if (!this.DateCutoff.HasValue) return true;
            return DateTime.UtcNow < this.DateCutoff.Value;
        }

        // Note: 2019-06-11 huyn@denovu.com:
        // Please use this static method in EF queries instead of this.IsOpen method.
        // EF has issue while resolving instance method in queries.
        public static Expression<Func<Activity, bool>> IsOpenExpr =
            (Activity x) => x.Type != ActivityType.Activity
                                                    && x.DateDue.HasValue &&
                                                    x.DateDue.Value > DateTime.UtcNow;
        public static Expression<Func<Activity, bool>> IsPublicExpr =
            (Activity x) => x.Type == ActivityType.Activity ? (
                                x.PublishStartDate.HasValue &&  x.PublishStartDate.Value < DateTime.UtcNow
                                && (!x.PublishEndDate.HasValue || x.PublishEndDate.Value > DateTime.UtcNow)
                            ) : (x.DateAssigned.HasValue && x.DateAssigned.Value < DateTime.UtcNow);
        public static Expression<Func<Activity, bool>> Not<Activity>(Expression<Func<Activity, bool>> one)
        {
            var candidateExpr = one.Parameters[0];
            var body = Expression.Not(one.Body);

            return Expression.Lambda<Func<Activity, bool>>(body, candidateExpr);
        }

        public Activity Clone()
        {
            var newInstance = CloneExtensions.CloneFactory.GetClone(this);
            newInstance.ClassId = 0;
            newInstance.ActivityId = 0;

            return newInstance;
        }

        public void TimeTravel(DateTime from, DateTime to, DateTime? limit = null)
        {
            this.DateAssigned = this.DateAssigned?.Travel(from, to, limit);
            this.DateCutoff = this.DateCutoff?.Travel(from, to, limit);
            this.DateDue = this.DateDue?.Travel(from, to, limit);
            this.PublishEndDate = this.PublishEndDate?.Travel(from, to, limit);
            this.PublishStartDate = this.PublishStartDate?.Travel(from, to, limit);

            this.Document?.TimeTravel(from, to, limit);
        }



    }
}
