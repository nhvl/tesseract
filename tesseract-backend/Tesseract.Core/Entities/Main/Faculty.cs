﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Common;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Core.Entities.Main
{
    [Table("Faculty")]
    public class Faculty : BaseEntity
    {
        [Key]
        public long FacultyId { get; set; }
        [MaxLength(50)]
        public string ExternalId { get; set; }
        public ApplicationUser User { get; set; }

        [NotMapped]
        public ICollection<School> Schools { get; set; }
    }
}