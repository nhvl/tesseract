﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Tesseract.Core.Entities.Main
{
    [Table("Polls")]
    public class Poll : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long PollId { get; set; }
        [Required]
        public long StudentId { get; set; }
        [Required]
        public long ActivityId { get; set; }
        [Required]
        [MaxLength(50)]
        public string PollQuizId { get; set; }
        [Required]
        public byte VoteContent { get; set; }
    }
}
