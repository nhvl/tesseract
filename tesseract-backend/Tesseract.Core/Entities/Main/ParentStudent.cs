﻿using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Interfaces;

namespace Tesseract.Core.Entities.Main
{
    [Table("ParentStudent")]
    public class ParentStudent
    {
        public long ParentId { get; set; }
        [NotMapped]
        public ApplicationUser Parent { get; set; }

        public long StudentId { get; set; }
        [NotMapped]
        public ApplicationUser Student { get; set; }
    }
}