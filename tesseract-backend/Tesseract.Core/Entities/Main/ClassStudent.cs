﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Interfaces;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Core.Entities.Main
{
    [Table("ClassStudent")]
    public class ClassStudent
    {
        public long ClassId { get; set; }
        public Class Class { get; set; }

        public long StudentId { get; set; }
        public ApplicationUser Student { get; set; }

        [Column(TypeName = "decimal(7,6)")]
        public decimal? CumulativeGrade { get; set; }

        public int SortIndex { get; set; } = 10000;

        public DateTime? LastAccessed { get; set; }
    }
}
