﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Interfaces;

namespace Tesseract.Core.Entities.Main
{
    [Table("SchoolAdmin")]
    public class SchoolAdmin : IJoinEntity<School>, IJoinEntity<ApplicationUser>
    {
        public long UserId { get; set; }

        public ApplicationUser User { get; set; }
        ApplicationUser IJoinEntity<ApplicationUser>.Navigation
        {
            get => User;
            set => User = value;
        }
        public long SchoolId { get; set; }

        public School School { get; set; }
        School IJoinEntity<School>.Navigation
        {
            get => School;
            set => School = value;
        }
    }
}
