﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Tesseract.Core.Entities.Main
{
    [Table("GradingTerms")]
    public class GradingTerm : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long GradingTermId { get; set; }
        public long SchoolId { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        #region Foreign Key shadow properties
        private ICollection<Class> Classes { get; } = new List<Class>();
        #endregion
    }
}
