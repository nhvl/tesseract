﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tesseract.Core.Entities.Main
{
    [Table("CommentSeen")]
    public class CommentSeen : BaseEntity
    {
        public CommentSeen()
        {
        }


        [MaxLength(256)]
        public string ThreadId { get; set; }

        [Required]
        public long UserId { get; set; }

        [Required]
        public DateTime LastSeen { get; set; }
    }
}
