﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Interfaces;


namespace Tesseract.Core.Entities.Main
{
    [Table("SchoolStudent")]
    public class SchoolStudent
    {
        public long SchoolId { get; set; }
        public long StudentId { get; set; }
        public long GradingPeriod { get; set; }
        [Column(TypeName = "decimal(7,6)")]
        public decimal? Grade { get; set; }
        public School School { get; set; }
        public ApplicationUser Student { get; set; }
    }
}
