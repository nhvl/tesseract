﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Common;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Core.Entities.Main
{
    [Table("Schools")]
    public class School : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SchoolId { get; set; }
        [MaxLength(100)]
        public string SchoolName { get; set; }
        public long CurrentGradingTerm { get; set; }
        public long DistrictId { get; set; }
        [MaxLength(2000)]
        public string LogoUrl { get; set; }

        [MaxLength(2000)]
        public string IconUrl { get; set; }

        #region Foreign Key shadow properties
        private ICollection<SchoolAdmin> SchoolAdmins { get; } = new List<SchoolAdmin>();
        private ICollection<SchoolStudent> SchoolStudents { get; } = new List<SchoolStudent>();
        private ICollection<SchoolFaculty> SchoolFaculties { get; } = new List<SchoolFaculty>();
        private ICollection<Class> Classes { get; } = new List<Class>();
        #endregion
    }
}