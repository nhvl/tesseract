﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Tesseract.Core.Entities.Main
{
    [Table("ActivityCategories")]
    public class ActivityCategory : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ActivityCategoryId  { get; set; }
        [MaxLength(200)]
        public string Name { get; set; }
        [MaxLength(10)]
        public string Color { get; set; }
        public bool IsGraded { get; set; }
        [Column(TypeName = "decimal(9, 2)")]
        public decimal MaxScore { get; set; }
        [Column(TypeName = "decimal(9, 2)")]
        public decimal Weight { get; set; }
        public long FacultyId { get; set; }
    }
}
