﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Core.Entities.Main
{
    public enum NotificationType {
        Email = 1,
        SMS = 2,
        PwaPush = 3,
    }

    public enum NotificationSendType {
        DontSend = 1,
        Imediately = 2,
        Daily = 3,
        Weekly = 4,
    }

    [Table("UserNotificationSettings")]
    public class UserNotificationSetting : BaseEntity
    {
        [ForeignKey("User")]
        public long UserId { get; set; }

        public NotificationType Type { get; set; }
        public NotificationSendType SendType { get; set; }
        public TimeSpan SendTime { get; set; }
        public byte WeeklyDay { get; set; }

        public ApplicationUser User { get; set; }

        public DateTime? GetSendingTime()
        {
            // TODO: refactor this to avoid static functions in Core domain
            return GetNextSendTime(SendType, DateTime.Now, SendTime, WeeklyDay);
        }

        static DateTime? GetNextSendTime(NotificationSendType sendType, DateTime now, TimeSpan sendTime, byte weekDays)
        {
            switch (sendType)
            {
                case NotificationSendType.DontSend: return null;
                case NotificationSendType.Imediately: return DateTime.MinValue;
                case NotificationSendType.Daily: return GetNextDate(now, sendTime);
                case NotificationSendType.Weekly: return GetNextDate(now, ByteToDayOfWeeks(weekDays), sendTime);
                default: return null;
            }
        }

        const byte Sunday    = 0b01000000;
        const byte Monday    = 0b00100000;
        const byte Tuesday   = 0b00010000;
        const byte Wednesday = 0b00001000;
        const byte Thursday  = 0b00000100;
        const byte Friday    = 0b00000010;
        const byte Saturday  = 0b00000001;

        static List<DayOfWeek> ByteToDayOfWeeks(byte days)
        {
            var list = new List<DayOfWeek>(7);
            if ((days & Sunday   ) == Sunday   ) list.Add(DayOfWeek.Sunday);
            if ((days & Monday   ) == Monday   ) list.Add(DayOfWeek.Monday);
            if ((days & Tuesday  ) == Tuesday  ) list.Add(DayOfWeek.Tuesday);
            if ((days & Wednesday) == Wednesday) list.Add(DayOfWeek.Wednesday);
            if ((days & Thursday ) == Thursday ) list.Add(DayOfWeek.Thursday);
            if ((days & Friday   ) == Friday   ) list.Add(DayOfWeek.Friday);
            if ((days & Saturday ) == Saturday ) list.Add(DayOfWeek.Saturday);
            return list;
        }

        static DateTime GetNextDate(DateTime now, TimeSpan time)
        {
            var timeOfDay = now.TimeOfDay;
            return timeOfDay < time
                ? now.Date + time
                : now.Date + TimeSpan.FromDays(1) + time;
        }

        static DateTime? GetNextDate(DateTime now, ICollection<DayOfWeek> days, TimeSpan time)
        {
            if (days.Count < 1) return null;
            var sendTime = GetNextDate(now, time);
            while (!days.Contains(sendTime.DayOfWeek))
            {
                sendTime = sendTime.AddDays(1);
            }

            return sendTime;
        }
    }
}
