﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Tesseract.Core.Entities.Main
{
    public class EmailTemplate:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmailTemplateId { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(256)]
        public string BccEmailAddresses { get; set; }
        [MaxLength(500)]
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
