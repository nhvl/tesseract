﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Core.Entities.Main
{
    [Table("Students")]
    public class Student : BaseEntity
    {
        [Key]
        public long StudentId { get; set; }

        [MaxLength(50)]
        public string ExternalId { get; set; }

        [Required]
        [MaxLength(20)]
        public string StudentNumber { get; set; }

        public ApplicationUser User { get; set; }
        [NotMapped]
        public ICollection<School> Schools { get; set; }
        public short Grade { get; set; }        
    }
}
