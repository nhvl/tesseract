﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Common;

namespace Tesseract.Core.Entities.Main
{
    [Table("Localizations")]
    public class Localization
    {
        public Localization()
        {

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long RecordId { get; set; }

        public string Locale { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
