﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tesseract.Core.Entities.Main
{
    // TODO must be remove this Entity after everything is ok
    public class EmailAccount:BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmailAccountId { get; set; }
        [MaxLength(256)]
        public string Email { get; set; }
        public bool EnableSsl { get; set; }
        [MaxLength(50)]
        public string DisplayName { get; set; }
        [MaxLength(50)]
        public string Password { get; set; }
        [MaxLength(50)]
        public string Username { get; set; }
        public int Port { get; set; }
        [MaxLength(256)]
        public string Host { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public string FriendlyName => this.Email + (String.IsNullOrWhiteSpace(this.DisplayName) ? "" : $" ({this.DisplayName})");
    }
}
