﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Common.Interface;
using Tesseract.Core.Common;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Core.Entities.Main
{
    [Table("Classes")]
    public class Class : BaseEntity, IClonable<Class>
    {
        public Class()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ClassId { get; set; }

        [MaxLength(50)]
        public string ClassName { get; set; }

        [MaxLength(10)]
        public string Period { get; set; }

        public long GradingTerm { get; set; }
        public GradingTerm Term { get; set; }
        public long SchoolId { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        [Required]
        public bool UseDefaultGradesRange { get; set; }
        [MaxLength(2000)]
        public GradeRange[] GradeRanges { get; set; }

        public bool IsTemplate { get; set; }
        [NotMapped]
        public ICollection<Activity> Activities { get; } = new List<Activity>();

        public Class Clone()
        {
           var classItem = CloneExtensions.CloneFactory.GetClone(this);
            classItem.ClassId = 0;
           return classItem;
        }

        #region Foreign Key shadow properties
        private ICollection<ClassFaculty> ClassFaculties { get; } = new List<ClassFaculty>();
        private ICollection<ClassStudent> ClassStudents { get; } = new List<ClassStudent>();
        public School School { get; set; }
        #endregion
    }
}
