﻿using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Interfaces;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Core.Entities.Main
{
    [Table("SchoolFaculty")]
    public class SchoolFaculty
    {
        public long FacultyId { get; set; }
        public ApplicationUser Faculty { get; set; }

        public long SchoolId { get; set; }
        public School School { get; set; }
    }
}