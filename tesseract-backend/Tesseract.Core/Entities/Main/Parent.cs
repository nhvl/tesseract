﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Common;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Core.Entities.Main
{
    [Table("Parents")]
    public class Parent : BaseEntity
    {
        [Key]
        public long ParentId { get; set; }
        public ApplicationUser User { get; set; }
    }
}