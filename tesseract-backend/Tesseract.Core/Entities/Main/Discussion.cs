﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Enums;

namespace Tesseract.Core.Entities.Main
{
    [Table("Discussions")]
    public class Discussion : BaseEntity
    {
        public Discussion()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long DiscussionId { get; set; }
        [MaxLength(500)]
        public string Title { get; set; }
        public string Content { get; set; }

        [MaxLength(2000)]
        public string Link { get; set; }
        [MaxLength(100)]
        public string ContentType { get; set; }
        public MediaTypeEnum MediaType { get; set; }

        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }

        public long ClassId { get; set; }
        public long ActivityId { get; set; }

        [MaxLength(256)]
        public string ThreadId { get; set; }

        public bool IsGraded { get; set; }
        public long? DiscussionActivityId { get; set; }

    }
}

