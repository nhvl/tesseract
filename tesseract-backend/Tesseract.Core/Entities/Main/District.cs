﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tesseract.Core.Entities.Main
{
    [Table("Districts")]
    public class District : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long DistrictId { get; set; }
        [MaxLength(50)]
        public string DistrictName { get; set; }

        [MaxLength(50)]
        public string Domain { get; set; }

        [MaxLength(2000)]
        public string LogoUrl { get; set; }

        public ICollection<School> Schools { get; set; }
    }
}