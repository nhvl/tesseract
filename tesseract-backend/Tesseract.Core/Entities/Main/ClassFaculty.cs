﻿using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Interfaces;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Core.Entities.Main
{
    [Table("ClassFaculty")]
    public class ClassFaculty
    {
        public long ClassId { get; set; }
        public Class Class { get; set; }

        public long FacultyId { get; set; }
        public ApplicationUser Faculty { get; set; }

        public int SortIndex { get; set; } = 10000;
    }
}
