﻿using Tesseract.Common.Extensions;

namespace Tesseract.Core.Entities.Main
{
    public class GradeRange
    {
        private static readonly int maxLength = 10;
        private string _letterGrade;
        public string LetterGrade
        {
            get => _letterGrade;
            set => _letterGrade = value.TruncateLongString(maxLength);
        }

        public decimal PercentGrade { get; set; }
    }
    
}
