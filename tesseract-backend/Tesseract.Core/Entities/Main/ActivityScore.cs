﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Core.Entities.Main
{
    [Table("ActivityScores")]
    public class ActivityScore
    {
        public long ActivityId { get; set; }
        public Activity Activity { get; set; }

        public long StudentId { get; set; }
        public ApplicationUser Student { get; set; }

        [Column(TypeName = "decimal(9, 2)")]
        public decimal? Score { get; set; }
        public DateTime? GradeDate { get; set; }

        public DateTime? SavedDate { get; set; }
        public bool IsSubmitted { get; set; }

        public bool HasSubmission() => this.SavedDate != null;
        public bool IsPendingScore() => this.HasSubmission() && this.Score == null;

        public byte? Communication    { get; set; }
        public byte? Collaboration    { get; set; }
        public byte? Character        { get; set; }
        public byte? Creativity       { get; set; }
        public byte? CriticalThinking { get; set; }

        public bool IsExclude { get; set; }

        [MaxLength(256)]
        public string ThreadId { get; set; }
    }
}
