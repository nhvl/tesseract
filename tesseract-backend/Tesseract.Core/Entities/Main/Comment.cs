﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tesseract.Core.Entities.Main
{
    [Table("Comments")]
    public class Comment : BaseEntity
    {
        public Comment()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CommentId { get; set; }

        [MaxLength(256)]
        public string ThreadId { get; set; }

        // public long UserId { get; set; }
        public long? ReplyTo { get; set; }

        public string Content { get; set; }
    }
}
