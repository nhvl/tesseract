﻿namespace Tesseract.Core.Entities.Config
{
    public class DocumentDbSettings
    {
        public string DatabaseId { get; set; }
        public string MasterKey { get; set; }
        public string EndpointUrl { get; set; }
    }
}