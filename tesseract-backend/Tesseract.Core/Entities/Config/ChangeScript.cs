﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tesseract.Core.Entities.Config
{
    [Table("ChangeScripts")]
    public class ChangeScript
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ChangeScriptID { get; set; }

        [Required]
        public DateTime DateExecuted { get; set; }

        [MaxLength(50)]
        public string ScriptName { get; set; }
    }
}
