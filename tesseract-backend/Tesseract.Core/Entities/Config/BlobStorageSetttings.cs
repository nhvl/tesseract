﻿namespace Tesseract.Core.Entities.Config
{
    public class BlobStorageSetttings
    {
        public string Account { get; set; }
        public string Key { get; set; }
        public string ImageContainer { get; set; }
    }
}