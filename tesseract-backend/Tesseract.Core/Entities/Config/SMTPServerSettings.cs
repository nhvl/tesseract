﻿using System;

namespace Tesseract.Core.Entities.Config
{
    public class SMTPServerSettings
    {
        public string SupportedEmail { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool EnableSsl { get; set; }
        public int Port { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public string FriendlyName => this.Email + (String.IsNullOrWhiteSpace(this.DisplayName) ? "" : $" ({this.DisplayName})");
        public bool IsSandBox { get; set; }
        public string ToValidSandBoxEmail { get; set; }
    }
}