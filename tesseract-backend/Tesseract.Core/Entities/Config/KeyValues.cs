﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tesseract.Core.Entities.Config
{
    [Table("KeyValues")]
    public class KeyValue
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long KeyValueID { get; set; }

        public long? DistrictID { get; set; }

        public long? SchoolID { get; set; }

        [MaxLength(50)]
        public string KeyName { get; set; }

        [MaxLength(100)]
        public string TextValue { get; set; }

        public int OrderIndex { get; set; }

        public bool IsActive { get; set; }

    }
}
