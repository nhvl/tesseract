using Newtonsoft.Json;

namespace Tesseract.Core.Entities.Config
{
    public class GoogleApiSettings
    {
        [JsonIgnore]
        public string ProjectId { get; set; }
        [JsonProperty("developerKey")]
        public string DeveloperKey { get; set; }
        [JsonProperty("clientId")]
        public string ClientId { get; set; }

        // Exclude this property from json parse
        [JsonIgnore]
        public string ClientSecret { get; set; }

        // TODO add dynamic redirectUri base on current host
        [JsonProperty("redirectUri")]
        public string RedirectUri { get; set; }
    }
}
