﻿namespace Tesseract.Core.Entities.Config
{
    public class ElasticsearchLogSettings
    {
        public bool IsEnabled { get; set; }
        public string Uri { get; set; }
        public string TypeName { get; set; }
        public string IndexFormat { get; set; }
    }
}