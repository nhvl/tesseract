﻿namespace Tesseract.Core.Entities.Config
{
    public class AnalyticsReportingSettings
    {
        public string ProjectId { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}
