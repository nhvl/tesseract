﻿using System;
using System.Runtime.Serialization;

namespace Tesseract.Core.Exceptions
{
    public class ConflictResourceException: Exception
    {
        public ConflictResourceException()
        {
        }

        public ConflictResourceException(string message) : base(message)
        {
        }

        public ConflictResourceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ConflictResourceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}