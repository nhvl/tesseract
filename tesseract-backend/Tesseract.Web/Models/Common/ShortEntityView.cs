﻿namespace Tesseract.Web.Models.Common
    {
        public class ShortEntityView
        {
            public ShortEntityView(long id, string name)
            {
                Id = id;
                Name = name;
            }

            public long Id{ get; set; }
            public string Name { get; set; }
        }
    }
