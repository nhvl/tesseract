﻿using System.ComponentModel.DataAnnotations;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Web.Models
{
    public class ActivityCategoryViewModel
    {
        [Required]
        public long ActivityCategoryId { get; set; }
        [Required]
        [StringLength(200, ErrorMessage = "The {0} must be at max {1} characters long.")]
        public string Name { get; set; }
        [Required]
        [StringLength(10, ErrorMessage = "The {0} must be at max {1} characters long.")]
        public string Color { get; set; }
        [Required]
        public bool IsGraded { get; set; }
        [Required]
        [Range(1, 10000, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public decimal MaxScore { get; set; }
        [Required]
        [Range(1, 100, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public decimal Weight { get; set; }
        [Required]
        public long FacultyId { get; set; }

        public bool ShouldRecalculateGrade(ActivityCategory category)
        {
            return (
                (this.IsGraded != category.IsGraded) ||
                (this.IsGraded && (
                    this.MaxScore != category.MaxScore ||
                    this.Weight != category.Weight
                ))
            );
        }
    }
}
