﻿namespace Tesseract.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Security.Claims;
    using Newtonsoft.Json;
    using Tesseract.Core.Entities.Identity;

    public class CreateRoleRequest
    {
        [Required]
        [StringLength(256, ErrorMessage = "Name length can't be more than 256.")]
        public string Name { get; set; }

        public bool IsSystemRole { get; set; }

        [StringLength(2048, ErrorMessage = "Description length can't be more than 2048.")]
        public string Description { get; set; }

        public string[] Permissions { get; set; }
    }

    public class UpdateRoleRequest
    {
        [Required]
        [StringLength(256, ErrorMessage = "Name length can't be more than 256.")]
        public string Name { get; set; }

        public bool IsSystemRole { get; set; }

        [StringLength(2048, ErrorMessage = "Description length can't be more than 2048.")]
        public string Description { get; set; }

        public string[] Permissions { get; set; }

        public void WriteTo(ApplicationRole role)
        {
            if (!string.IsNullOrEmpty(this.Name))
            {
                role.Name = this.Name;
            }

            if (this.Description != null)
            {
                role.Description = this.Description;
            }
        }
    }

    public class RoleViewModel
    {
        public long RoleId { get; set; }

        public string Name { get; set; }

        public bool IsSystemRole { get; set; }

        public string Description { get; set; }

        public string[] Permissions { get; set; }

        public static RoleViewModel Create(long roleId, string name, bool isSystem, string description)
        {
            return new RoleViewModel
            {
                RoleId = roleId,
                Name = name,
                Description = description,
                IsSystemRole = isSystem,
            };
        }

        public static RoleViewModel Create(ApplicationRole role, IList<Claim> claims)
        {
            return new RoleViewModel
            {
                RoleId = role.Id,
                Name = role.Name,
                IsSystemRole = role.IsSystemRole,
                Description = role.Description,
                Permissions = claims == null ? null : claims.Select(c => c.Value).ToArray(),
            };
        }

        public static RoleViewModel Create(ApplicationRole role, string[] permissions)
        {
            return new RoleViewModel
            {
                RoleId = role.Id,
                Name = role.Name,
                IsSystemRole = role.IsSystemRole,
                Description = role.Description,
                Permissions = permissions,
            };
        }
    }
}
