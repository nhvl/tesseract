﻿using System.ComponentModel.DataAnnotations;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Web.Models
{
    public class CreateCommentRequest
    {
        [Required]
        public string Content { get; set; }

        public long? ReplyTo { get; set; }

        public Comment ToComment(string threadId, long userId)
        {
            return new Comment
            {
                ThreadId = threadId,
                // UserId = userId,
                CreatedBy = userId,
                Content = this.Content,
                ReplyTo = this.ReplyTo,
            };
        }
    }
}
