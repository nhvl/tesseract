﻿namespace Tesseract.Web.Models
{
    public class ApplyTemplateModel{
        public long TemplateId { get; set; }
        public long GradingTermId { get; set; }
        public string ClassName { get; set; }
    }
}
