﻿using System;
using System.ComponentModel.DataAnnotations;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Web.Models
{
    public class CreateGradingTermRequest
    {
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }

        public GradingTerm ToGradingTerm(long schoolId)
        {
            return new GradingTerm
            {
                SchoolId = schoolId,
                Name = Name,
                StartDate = StartDate,
                EndDate = EndDate,
            };
        }
    }

    public class UpdateGradingTermRequest
    {
        [MaxLength(100)]
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public void WriteTo(GradingTerm term)
        {
            if (!string.IsNullOrEmpty(Name)) term.Name = Name;
            if (StartDate.HasValue) term.StartDate = StartDate.Value;
            if (EndDate.HasValue) term.EndDate = EndDate.Value;
        }
    }

    public class GradingTermResponse
    {
        public long     GradingTermId { get; private set; }
        public long     SchoolId      { get; private set; }
        public string   Name          { get; private set; }
        public DateTime StartDate     { get; private set; }
        public DateTime EndDate       { get; private set; }

        public static GradingTermResponse Create(GradingTerm term)
        {
            return new GradingTermResponse
            {
                GradingTermId = term.GradingTermId,
                SchoolId      = term.SchoolId,
                Name          = term.Name,
                StartDate     = term.StartDate,
                EndDate       = term.EndDate,
            };
        }
    }
}
