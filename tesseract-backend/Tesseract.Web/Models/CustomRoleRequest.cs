﻿using System.ComponentModel.DataAnnotations;

namespace Tesseract.Web.Models
{
    public class CustomRoleRequest
    {
        [Required]
        public string[] CustomRoles { get; set; }
    }
}
