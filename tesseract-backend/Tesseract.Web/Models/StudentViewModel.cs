﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CsvHelper.Configuration.Attributes;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Web.Models
{
    public class StudentView
    {
        [Required]
        public long StudentId { get; set; }
        [Required]
        public string ExternalId { get; set; }
        [Required]
        public string StudentNumber { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string Nickname { get; set; }
        [Required]
        public long UserId { get; set; }
        public int Grade { get; set; }

        public decimal? CumulativeGrade { get; set; }
        public DateTime? LastAccessed { get; set; }
    }

    public class ParentStudentView: StudentView
    {
        public OpenActivityView[] Activities { get; internal set; }

    }

    public class OpenActivityView
    {
        public OpenActivityView(long id, DateTime? due)
        {
            Id = id;
            Due = due;
        }

        public long Id { get; set; }
        public DateTime? Due { get; set; }
    }

    public class CreateStudentRequest
    {
        public string ExternalId { get; set; }
        [Required]
        public string StudentNumber { get; set; }
        [Optional]
        public short Grade { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Optional] //Attribute for CSVHelper
        public string Salutation { get; set; }

        [Optional] //Attribute for CSVHelper
        public string Nickname { get; set; }
        [Optional]
        public string PhoneNumber { get; set; }

        [Optional] //Attribute for CSVHelper
        public long SchoolId { get; set; }

        // TODO: move to eplicit cast
        public Student ToStudent(long userId)
        {
            return new Student
            {
                ExternalId = ExternalId,
                StudentNumber = StudentNumber,
                Grade = Grade,
                StudentId = userId,
            };
        }

        // TODO: move to eplicit cast
        public ApplicationUser ToUser(long districtId)
        {
            return new ApplicationUser
            {
                DistrictId  = districtId,
                Email       = Email,
                UserName    = Email,
                FirstName   = !String.IsNullOrEmpty(FirstName)?FirstName.Trim():FirstName,
                LastName    = !String.IsNullOrEmpty(LastName)?LastName.Trim():LastName,
                Salutation  = !String.IsNullOrEmpty(Salutation)?Salutation.Trim():Salutation,
                Nickname    = !String.IsNullOrEmpty(Nickname)?Nickname.Trim():Nickname,
                PhoneNumber = !String.IsNullOrEmpty(PhoneNumber)?PhoneNumber.Trim():PhoneNumber,
            };
        }
    }

    public class BatchUpdateStudentItemRequest
    {
        public string ExternalId { get; set; }
        [Required]
        public string StudentNumber { get; set; }
        [Optional]
        public string Grade { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Optional] //Attribute for CSVHelper
        public string Salutation { get; set; }

        [Optional] //Attribute for CSVHelper
        public string Nickname { get; set; }
        [Optional]
        public string PhoneNumber { get; set; }

        [Optional] //Attribute for CSVHelper
        public string SchoolId { get; set; }

        [Ignore] //Attribute for CSVHelper
        public string Status { get; set; }
        public List<string> Results { get; set; } = new List<string>();
        public List<string> Errors { get; set; } = new List<string>();

        // TODO: move to eplicit cast
        public Student ToStudent(long userId)
        {
            short.TryParse(Grade, out short _grade);
            return new Student
            {
                ExternalId = ExternalId,
                StudentNumber = StudentNumber,
                Grade = _grade,
                StudentId = userId,
            };
        }

        // TODO: move to eplicit cast
        public ApplicationUser ToUser(long districtId)
        {
            return new ApplicationUser
            {
                DistrictId  = districtId,
                Email       = Email,
                UserName    = Email,
                FirstName   = !String.IsNullOrEmpty(FirstName)?FirstName.Trim():FirstName,
                LastName    = !String.IsNullOrEmpty(LastName)?LastName.Trim():LastName,
                Salutation  = !String.IsNullOrEmpty(Salutation)?Salutation.Trim():Salutation,
                Nickname    = !String.IsNullOrEmpty(Nickname)?Nickname.Trim():Nickname,
                PhoneNumber = !String.IsNullOrEmpty(PhoneNumber)?PhoneNumber.Trim():PhoneNumber,
            };
        }
        
        public void ToUpdateUser(ApplicationUser user)
        {
            if (!string.IsNullOrEmpty(Email)) {
                user.Email = Email;
                user.UserName = Email;
            }
            if (FirstName != null) user.FirstName = FirstName.Trim();
            if (!string.IsNullOrEmpty(LastName)) user.LastName = LastName.Trim();
            if (PhoneNumber != null) user.PhoneNumber = PhoneNumber.Trim();
        }
    }

    public class UpdateStudentRequest
    {
        public string ExternalId    { get; set; }
        public string StudentNumber { get; set; }
        public short  Grade         { get; set; }

        public string Email         { get; set; }
        public string FirstName     { get; set; }
        public string LastName      { get; set; }
        public string Salutation    { get; set; }
        public string Nickname      { get; set; }
        public string PhoneNumber   { get; set; }

        public void WriteTo(Student student)
        {
            if (!string.IsNullOrEmpty(ExternalId)) student.ExternalId = ExternalId;
            if (!string.IsNullOrEmpty(StudentNumber)) student.StudentNumber = StudentNumber;
            if (0 <= Grade) student.Grade = Grade;
        }

        public void WriteTo(ApplicationUser user)
        {
            if (!string.IsNullOrEmpty(Email)) {
                user.Email = Email.Trim();
                user.UserName = Email.Trim();
            }
            if (!string.IsNullOrEmpty(LastName)) user.LastName = LastName.Trim();
            if (FirstName    != null) user.FirstName   = FirstName.Trim();
            if (Salutation   != null) user.Salutation  = Salutation.Trim();
            if (Nickname     != null) user.Nickname    = Nickname.Trim();
            if (PhoneNumber  != null) user.PhoneNumber = PhoneNumber.Trim();
        }
    }

    public class StudentResponse
    {
        public long   StudentId     { get; private set; }
        public string ExternalId    { get; private set; }
        public string StudentNumber { get; private set; }
        public long   UserId        { get; private set; }
        public string Email         { get; private set; }
        public string FirstName     { get; private set; }
        public string LastName      { get; private set; }
        public string Salutation    { get; private set; }
        public string Nickname      { get; private set; }
        public string PhoneNumber   { get; private set; }

        public static StudentResponse Create(Student student, ApplicationUser user)
        {
            return new StudentResponse
            {
                StudentId     = student.StudentId,
                ExternalId    = student.ExternalId,
                StudentNumber = student.StudentNumber,
                UserId        = student.StudentId,
                Email         = user.Email,
                FirstName     = user.FirstName,
                LastName      = user.LastName,
                Salutation    = user.Salutation,
                Nickname      = user.Nickname,
                PhoneNumber   = user.PhoneNumber,
            };
        }
    }
}
