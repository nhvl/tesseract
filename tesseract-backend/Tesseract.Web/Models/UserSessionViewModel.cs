﻿using System.Collections.Generic;

namespace Tesseract.Web.Models
{
    public class UserSessionViewModel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string Nickname { get; set; }

        public string Email { get; set; }
        public long ActiveSchoolId { get; set; }
        public IList<string> Role { get; set; }
        public string Token { get; set; }
        public int FacultyId { get; set; }
    }
}
