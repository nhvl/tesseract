﻿using Newtonsoft.Json;
using System;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Enums;
using Tesseract.Web.Common;

namespace Tesseract.Web.Models
{
    public class ActDocModelBase
    {
        public long ActivityId { get; set; }
        public XssSanitizedString Title { get; set; }
        public string Banner { get; set; }
        public XssSanitizedString Summary { get; set; }
    }
    public class ActDocPreview: ActDocModelBase
    {
        public ActItemBaseModel[] Items { get; set; }
        public DateTime? DateUpdated { get; set; }
    }

    public class ActDocView : ActDocRequest
    {
        public string id { get; set; }
        public DateTime dateUpdated { get; set; }
    }

    public class ActDocRequest: ActDocModelBase
    {
        public ActItemRequest[] Items { get; set; }
    }

    public class StudentActDocRequest : ActDocModelBase
    {
        public long StudentId { get; set; }
        public StudentActItemRequest[] Items { get; set; }

    }
    public class StudentActDocView : StudentActDocRequest
    {
        public string id { get; set; }
        public DateTime? DateCreated { get; set; }
    }

    public class StudentActDocReview : ActDocModelBase
    {
        public long StudentId { get; set; }
        public string id { get; set; }
        public DateTime dateUpdated { get; set; }
        public StudentActItemReview[] Items { get; set; }
    }

    public class ActItemBaseModel
    {
        public string Id { get; set; }
        public EActItemType Type { get; set; }
        public XssSanitizedString Title { get; set; }
        public string Image { get; set; }
        public XssSanitizedString Content { get; set; }
        [JsonProperty("Link")]
        public string MediaInfoLink { get; set; }
        [JsonProperty("ContentType")]
        public string MediaInfoContentType { get; set; }
        [JsonProperty("MediaType")]
        public MediaTypeEnum MediaInfoMediaType { get; set; }
        [JsonProperty("EmbedLink")]
        public string EmbedInfoLink { get; set; }
        [JsonProperty("EmbedContentType")]
        public string EmbedInfoContentType { get; set; }
        [JsonProperty("EmbedId")]
        public string EmbedId { get; set; }
        [JsonProperty("EmbedApp")]
        public string EmbedApp { get; set; }
        [JsonProperty("EmbedType")]
        public string EmbedType { get; set; }
        [JsonProperty("EmbedLastEdited")]
        public long LastEditedUtc { get; set; }
        public MatchQuizzItem[] matchQuizzItems { get; set; }

        public bool IsMultipleAnswer { get; set; }
        public ActOptionView[] Options { get; set; }

        public PollItemPreview[] PollItems { get; set; }

        public long? DiscussionId { get; set; }
        public decimal? NumericMin { get; set; }
        public decimal? NumericMax { get; set; }
    }

    public class ActItemPreview : ActItemBaseModel { }

    public class ActItemRequest : ActItemBaseModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class StudentActItemRequest: ActItemBaseModel
    {
        public bool FromTeacher { get; set; }
        public string TextAnswer { get; set; }
        public MatchQuizzItem[] MatchQuizzAnswer { get; set; }
        public ActOption[] OptionsAnswer { get; set; }
        public int? PollItemsAnswer { get; set; }
        public int? NumberAnswer { get; set; }
    }

    public class StudentActItemReview : StudentActItemRequest
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class ActOptionView
    {
        public XssSanitizedString Label { get; set; }
        public string Image { get; set; }
        public bool IsCorrect { get; set; }
    }

    public class PollItemView
    {
        public XssSanitizedString Label { get; set; }
        public string Image { get; set; }
    }
    public class PollItemPreview : PollItemView
    {
        public int Votes { get; set; }
    }
}
