﻿using System.ComponentModel.DataAnnotations;

namespace Tesseract.Web.Models
{
    public class RegisterStudentToSchoolViewModel
    {
        [Required]
        public long StudentId { get; set; }
        [Required]
        public short SchoolYear { get; set; }
        [Required]
        public decimal Grade { get; set; }
        [Required]
        public long GradingPeriod { get; set; }
    }
}
