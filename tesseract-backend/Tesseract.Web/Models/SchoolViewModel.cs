﻿using System.ComponentModel.DataAnnotations;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Web.Models
{
    public class SchoolViewModel
    {
        [Required]
        public long SchoolId { get; set; }
        [Required]
        public long DistrictId { get; set; }
        [Required]
        public long CurrentGradingTerm { get; set; }
        [Required]
        public string SchoolName { get; set; }

        public string LogoUrl { get; set; }
        public string IconUrl { get; set; }
    }

    public class SchoolResponse {
        public long   SchoolId           { get; set; }
        public string SchoolName         { get; set; }
        public long   CurrentGradingTerm { get; set; }
        public long   DistrictId         { get; set; }
        public string LogoUrl            { get; set; }
        public string IconUrl            { get; set; }
        public GradingTerm[] tGradingTerm { get; set; }

        public static SchoolResponse Create(School school, GradingTerm[] tGradingTerm) {
            return new SchoolResponse {
                SchoolId           = school.SchoolId,
                SchoolName         = school.SchoolName,
                CurrentGradingTerm = school.CurrentGradingTerm,
                DistrictId         = school.DistrictId,
                LogoUrl            = school.LogoUrl,
                IconUrl            = school.IconUrl,
                tGradingTerm       = tGradingTerm,
            };
        }
    }
}
