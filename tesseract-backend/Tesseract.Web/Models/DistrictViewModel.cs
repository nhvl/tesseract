﻿namespace Tesseract.Web.Models
{
    public class DistrictViewModel
    {
        public long DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string Domain { get; set; }
        public string LogoUrl { get; set; }
    }
}
