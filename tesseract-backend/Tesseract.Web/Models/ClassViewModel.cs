﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Web.Models
{
    public class ShortClassViewModel
    {
        [Required]
        public string ClassName { get; set; }
        [Required]
        public string Period { get; set; }
    }

    public class ClassViewModel: ShortClassViewModel
    {
        [Required]
        public long SchoolId { get; set; }
        [Required]
        public long GradingTerm { get; set; }
        public long ClassId { get; set; }
        public string Description { get; set; }
        public bool UseDefaultGradesRange { get; set; }
        public GradeRange[] GradeRanges { get; set; }
        public bool IsTemplate { get; private set; }

        public static ClassViewModel FromEntity(Core.Entities.Main.Class entity)
        {
            return new ClassViewModel
            {
                ClassId               = entity.ClassId,
                ClassName             = entity.ClassName,
                SchoolId              = entity.SchoolId,
                Period                = entity.Period,
                GradingTerm           = entity.GradingTerm,
                Description           = entity.Description,
                UseDefaultGradesRange = entity.UseDefaultGradesRange,
                IsTemplate            = entity.IsTemplate,
                GradeRanges           = entity.GradeRanges,
            };
        }

        public void WriteTo(Core.Entities.Main.Class entity)
        {
            if (!string.IsNullOrEmpty(this.ClassName)) entity.ClassName = this.ClassName;

            if (!string.IsNullOrEmpty(this.Period)) entity.Period = this.Period;
            if (this.GradingTerm > 0) entity.GradingTerm = this.GradingTerm;
            if (this.Description != null) entity.Description = this.Description;
            entity.UseDefaultGradesRange = this.UseDefaultGradesRange;
            entity.GradeRanges = this.UseDefaultGradesRange ? null : GradeRanges;
        }

        public Core.Entities.Main.Class ToEntity() {
            return new Core.Entities.Main.Class
            {
                ClassName             = this.ClassName,
                SchoolId              = this.SchoolId,
                Period                = this.Period,
                GradingTerm           = this.GradingTerm,
                Description           = this.Description,
                UseDefaultGradesRange = this.UseDefaultGradesRange,
                GradeRanges           = this.UseDefaultGradesRange ? null: GradeRanges
            };
        }
    }

    public class StudentClassViewModel : ClassViewModel
    {
        public new long ClassId { get; set; }
        public decimal? Grade { get; set; }
        public int SortIndex { get; set; }
        public IEnumerable<UserViewModel> Faculties { get; set; }
    }

    public class FacultyUpdateClassRequest
    {
        [Required]
        public long         ClassId               { get; set; }
        public long?        GradingTerm           { get; set; }
        public string       Description           { get; set; }
        public string       ClassName             { get; set; }
        public string       Period                { get; set; }
        public bool?        UseDefaultGradesRange { get; set; }
        public GradeRange[] GradeRanges           { get; set; }

        public int?         SortIndex             { get; set; }

        public bool WriteTo(Core.Entities.Main.Class entity)
        {
            var isChanged = false;

            if (!string.IsNullOrEmpty(this.ClassName) && !this.ClassName.Equals(entity.ClassName, StringComparison.InvariantCulture))
            {
                isChanged = true;
                entity.ClassName = this.ClassName;
            }

            if (!string.IsNullOrEmpty(this.Period) && !this.Period.Equals(entity.Period, StringComparison.InvariantCulture))
            {
                isChanged = true;
                entity.Period = this.Period;
            }

            if (this.GradingTerm.HasValue && this.GradingTerm.Value > 0 && this.GradingTerm.Value != entity.GradingTerm)
            {
                isChanged = true;
                entity.GradingTerm = this.GradingTerm.Value;
            }

            if (this.Description != null && !this.Description.Equals(entity.Description, StringComparison.InvariantCulture))
            {
                isChanged = true;
                entity.Description = this.Description;
            }

            if (this.UseDefaultGradesRange.HasValue && this.UseDefaultGradesRange.Value != entity.UseDefaultGradesRange) {
                isChanged = true;
                entity.UseDefaultGradesRange = this.UseDefaultGradesRange.Value;
            }

            if (this.GradeRanges != null) {
                isChanged = true;
                entity.GradeRanges = this.GradeRanges;
            }

            return isChanged;
        }

        public bool WriteTo(Core.Entities.Main.ClassFaculty entity)
        {
            var isChanged = false;

            if (this.SortIndex.HasValue && this.SortIndex.Value != entity.SortIndex)
            {
                isChanged = true;
                entity.SortIndex = this.SortIndex.Value;
            }

            return isChanged;
        }
    }

    public class StudentClassUpdateRequest
    {
        [Required]
        public long ClassId { get; set; }
        public int? SortIndex { get; set; }

        public bool WriteTo(Core.Entities.Main.ClassStudent entity)
        {
            var isChanged = false;

            if (this.SortIndex.HasValue && this.SortIndex.Value != entity.SortIndex)
            {
                isChanged = true;
                entity.SortIndex = this.SortIndex.Value;
            }

            return isChanged;
        }
    }
}
