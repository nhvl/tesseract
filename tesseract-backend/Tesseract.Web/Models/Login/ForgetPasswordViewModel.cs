﻿namespace Tesseract.Web.Models
{
    public class ForgetPasswordViewModel
    {
        public string Email { get; set; }
        public string Host { get; set; }
    }
}
