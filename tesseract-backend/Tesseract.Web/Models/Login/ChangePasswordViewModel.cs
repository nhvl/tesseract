﻿using System.ComponentModel.DataAnnotations;

namespace Tesseract.Web.Models
{
    public class ChangePasswordViewModel
    {
        [Required]
        public string Password { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}
