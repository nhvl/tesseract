﻿using System.ComponentModel.DataAnnotations;

namespace Tesseract.Web.Models
{
    public class ParentRegisterViewModel : RegisterViewModel
    {
        [Required]
        [Display(Name = "Student Number")]
        public string StudentNumber { get; set; }

        [Required]
        [Display(Name = "Student Phone Number")]
        [Phone]
        public string StudentPhoneNumber { get; set; }
    }
}
