﻿using System;
using System.Collections.Generic;

namespace Tesseract.Web.Models
{
    public class UserViewModel
    {
        public long Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string Nickname { get; set; }

        public long ActiveSchoolId { get; set; }

        public string Language { get; set; }

        public string Avatar { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime? LastActive { get; set; }

        public long DistrictId { get; set; }
    }

    public class LoggedInUserViewModel: UserViewModel
    {
        public string Token { get; set; }
    }

    public class DetailUserViewModel : UserViewModel
    {
        public long UserId => this.Id;
        public string Role { get; set; }
        public IEnumerable<string> Permissions { get; set; }
        public IEnumerable<long> Schools { get; set; }
        public IEnumerable<object> SchoolList { get; set; }
        public long FacultyId { get; set; }
        public long StudentId { get; set; }
        public string StudentNumber { get; set; }
        public long ParentId { get; set; }
    }
}
