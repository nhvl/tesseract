﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tesseract.Web.Models
{
    public class CreateSchoolViewModel
    {
        public string SchoolName { get; set; }
        public string LogoUrl { get; set; }
        public string IconUrl { get; set; }
        public string GradingTermName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
