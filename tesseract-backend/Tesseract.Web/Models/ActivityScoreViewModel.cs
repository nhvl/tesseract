﻿using System;
using Tesseract.Core.Entities.NoSql;

namespace Tesseract.Web.Models
{
    public interface ISubmissionData
    {
        DateTime? SavedDate { get; set; }
    }

    public interface IScoreData
    {
        decimal? Score { get; set; }
        byte? Communication { get; set; }
        byte? Collaboration { get; set; }
        byte? Character { get; set; }
        byte? Creativity { get; set; }
        byte? CriticalThinking { get; set; }
    }

    public class BaseActivityScoreViewModel
    {
        public long ActivityId { get; set; }
        public long StudentId { get; set; }
    }

    public class ScoreRequestViewModel: IScoreData
    {
        public decimal? Score { get; set; }
        public byte? Communication { get; set; }
        public byte? Collaboration { get; set; }
        public byte? Character { get; set; }
        public byte? Creativity { get; set; }
        public byte? CriticalThinking { get; set; }
        public bool? IsExclude { get; set; }
    }

    public class ActivitySubmitViewModel
    {
        public string Content { get; set; }
        public SubmissionAttachment[] Attachments { get; set; }
    }

    public class ActivityScoreInfomationViewModel : BaseActivityScoreViewModel, ISubmissionData, IScoreData
    {
        public decimal? Score { get; set; }
        public byte? Communication { get; set; }
        public byte? Collaboration { get; set; }
        public byte? Character { get; set; }
        public byte? Creativity { get; set; }
        public byte? CriticalThinking { get; set; }
        public DateTime? SavedDate { get; set; }
        public string ThreadId { get; set; }

        public string FullName { get; set; }
        public string ExternalId { get; set; }
        public bool IsLate { get; set; }
        public bool IsSubmitted { get; set; }
    }
}
