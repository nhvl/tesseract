﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Entities.NoSql;

namespace Tesseract.Web.Models
{
    public class CreateFacultyRequest
    {
        [Required]
        public string Email { get; set; }
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string Nickname { get; set; }
        public string PhoneNumber { get; set; }
        public string ExternalId { get; set; }

        public long[] Schools { get; set; }

        public Faculty ToFaculty(long userId)
        {
            return new Faculty
            {
                ExternalId = ExternalId,
                FacultyId = userId
            };
        }

        public ApplicationUser ToUser(long districtId)
        {
            return new ApplicationUser
            {
                DistrictId  = districtId,
                Email       = Email,
                FirstName   = FirstName,
                LastName    = LastName,
                Salutation  = Salutation,
                Nickname    = Nickname,
                PhoneNumber = PhoneNumber,
                UserName    = Email,
            };
        }

        public SchoolFaculty[] ToSchoolFaculties(long facultyId)
        {
            return this.Schools.Select(schoolId => new SchoolFaculty {FacultyId = facultyId, SchoolId = schoolId}).ToArray();
        }
    }

    public class UpdateFacultyRequest
    {
        public string ExternalId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string Nickname { get; set; }
        public string PhoneNumber { get; set; }
        public long[] Schools { get; set; }

        public void WriteTo(Faculty faculty)
        {
            if (!string.IsNullOrEmpty(ExternalId)) faculty.ExternalId = ExternalId;
        }

        public void WriteTo(ApplicationUser user)
        {
            if (!string.IsNullOrEmpty(Email)) {
                user.Email = Email;
                user.UserName = Email;
            }
            if (!string.IsNullOrEmpty(LastName)) user.LastName = LastName;
            if (FirstName    != null) user.FirstName   = FirstName;
            if (Salutation   != null) user.Salutation  = Salutation;
            if (Nickname     != null) user.Nickname    = Nickname;
            if (PhoneNumber  != null) user.PhoneNumber = PhoneNumber;
        }

        public SchoolFaculty[] ToSchoolFaculties(long facultyId)
        {
            return this.Schools.Select(schoolId => new SchoolFaculty {FacultyId = facultyId, SchoolId = schoolId}).ToArray();
        }
    }

    public class FacultyDistrictView
    {
        [Required]
        public long FacultyId { get; set; }
        [Required]
        public long SchoolId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string Nickname { get; set; }
        [Required]
        public ActivePassiveCount ActivityCount { get; set; }
    }
}
