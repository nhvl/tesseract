﻿using System;
using System.ComponentModel.DataAnnotations;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Web.Models
{
    public class EditPublishRequest
    {
        public long ActivityId { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }

    public class ActivityModelBase
    {
        [Required]
        [StringLength(200, ErrorMessage = "The {0} must be at max {1} characters long.")]
        public string Title { get; set; }
        [StringLength(500, ErrorMessage = "The {0} must be at max {1} characters long.")]
        public string Description { get; set; }
        [Required]
        public ActivityType Type { get; set; }
        public DateTime? DateAssigned { get; set; }
        public DateTime? DateDue { get; set; }
        public DateTime? DateCutoff { get; set; }

        public long? ParentActivityId { get; set; }
        public int? SortIndex { get; set; }
        public DateTime? PublishStartDate { get; set; }
        public DateTime? PublishEndDate { get; set; }

        public long? CategoryId { get; set; }
        public string Color { get; set; }
        [Required]
        public bool IsGraded { get; set; }
        public decimal? MaxScore { get; set; }
        public decimal? Weight { get; set; }
        public bool? IsCredit { get; set; }
    }

    public class CreateActivityRequest : ActivityModelBase
    {
        public Activity ToEntity(long classId, long userId) {
            return new Activity()
            {
                ClassId          = classId,
                Type             = this.Type,
                Title            = this.Title,
                Description      = this.Description,

                ParentActivityId = this.ParentActivityId,
                SortIndex        = this.SortIndex ?? -1,

                DateDue          = this.DateDue,
                DateAssigned     = this.DateAssigned,
                DateCutoff       = this.DateCutoff,

                CategoryId       = this.CategoryId,

                Color            = this.Color,
                IsGraded         = this.IsGraded,
                MaxScore         = this.MaxScore ?? 10,
                Weight           = this.Weight ?? 1,
                IsCredit         = this.IsCredit ?? false,

                CreatedBy        = userId,
            };
        }
    }

    public class EditActivityRequest : ActivityModelBase
    {
        public bool ShouldRecalculateGrade(Activity activity)
        {
            return (
                (this.DateAssigned != activity.DateAssigned) ||
                (this.IsGraded != activity.IsGraded) ||
                (this.IsGraded && (
                    this.MaxScore != activity.MaxScore ||
                    this.Weight != activity.Weight
                )) ||
                this.IsCredit != activity.IsCredit
            );
        }

        public void WriteTo(Activity activity, ActivityCategory category, long userId)
        {
            activity.Type = Type; // TODO: do we allow change activity type?
            if (!string.IsNullOrEmpty(Title)) activity.Title = this.Title;
            if (Description != null) activity.Description = this.Description;

            activity.ParentActivityId = ParentActivityId;
            if (SortIndex.HasValue && 0 <= SortIndex.Value) activity.SortIndex = SortIndex.Value;

            activity.DateDue      = this.DateDue;
            activity.DateAssigned = this.DateAssigned;
            activity.DateCutoff   = this.DateCutoff;
            activity.CategoryId   = category == null ? null : category.ActivityCategoryId as long?;
            activity.Color        = category == null ? this.Color : category.Color;
            activity.IsGraded     = category == null ? this.IsGraded : category.IsGraded;
            activity.MaxScore     = category != null ? category.MaxScore : (
                                    MaxScore.HasValue ? MaxScore.Value : activity.MaxScore);
            activity.Weight       = category != null ? category.Weight : (
                                    Weight.HasValue ? Weight.Value : activity.Weight);
            activity.IsCredit     = IsCredit.HasValue ? IsCredit.Value : activity.IsCredit;
            activity.UpdatedBy = userId;
        }

        public Activity ToEntity(long activityId, long userId) {
            return new Activity()
            {
                ActivityId   = activityId,
                Title        = this.Title,
                Type         = this.Type,
                Description  = this.Description,

                DateDue      = this.DateDue,
                DateAssigned = this.DateAssigned,
                DateCutoff   = this.DateCutoff,

                CategoryId   = this.CategoryId,

                Color        = this.Color,
                IsGraded     = this.IsGraded,
                MaxScore     = this.MaxScore ?? 10,
                Weight       = this.Weight ?? 1,
                IsCredit     = this.IsCredit ?? false,

                UpdatedBy    = userId,
            };
        }
    }

    public class BatchEditActivityRequest: EditActivityRequest {
        public long ActivityId { get; set; }
    }


    public class ActivityDetailModelView : ActivityModelBase
    {
        public long ActivityId { get; set; }
        public ShortClassViewModel Class { get; set; }
        public int SubmisstionCount { get; set; }
        public int UnGradeCount { get; set; }
    }

    public class StudyingActivityModelView : ActivityModelBase
    {
        public long ActivityId { get; set; }
        public long ClassId { get; set; }
        public ActivityScoreDto Score { get; set; }
    }
}
