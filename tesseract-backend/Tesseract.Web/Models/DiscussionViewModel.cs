﻿using System;
using System.ComponentModel.DataAnnotations;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Enums;

namespace Tesseract.Web.Models
{
    public class CreateDiscussionRequest
    {
        public long? ActivityId { get; set; }

        [Required]
        public string Title { get; set; }
        public string Content { get; set; }
        public string Link { get; set; }
        public string ContentType { get; set; }
        public MediaTypeEnum MediaType { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool IsGraded { get; set; }
        public long? DiscussionActivityId { get; set; }
        public string Color { get; set; }
        public decimal? MaxScore { get; set; }
        public decimal? Weight { get; set; }
        public bool? IsCredit { get; set; }

        public Discussion ToDiscussion(long classId, long userId, string threadId)
        {
            return new Discussion
            {
                ClassId = classId,
                ActivityId = ActivityId.HasValue ? ActivityId.Value : -1,
                Title = Title,
                Content = Content,
                Link = Link,
                ContentType = ContentType,
                MediaType = MediaType,
                StartTime = StartTime,
                EndTime = EndTime,
                CreatedBy = userId,
                ThreadId = threadId,
                IsGraded = IsGraded,
                DiscussionActivityId = DiscussionActivityId,
            };
        }
        public Activity ToActivity(long classId, long userId)
        {
            return new Activity()
            {
                ClassId      = classId,
                Type         = ActivityType.Discussion,
                Title        = this.Title,
                Color        = this.Color,
                IsGraded     = this.IsGraded,
                MaxScore     = this.MaxScore ?? 10,
                Weight       = this.Weight ?? 1,
                IsCredit     = this.IsCredit ?? false,

                DateDue      = this.EndTime,
                DateCutoff   = this.EndTime,
                DateAssigned = this.StartTime,

                CreatedBy    = userId,
            };
        }
    }

    public class UpdateDiscussionRequest
    {
        public long? ActivityId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Link { get; set; }
        public string ContentType { get; set; }
        public MediaTypeEnum? MediaType { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool IsGraded { get; set; }
        public long? DiscussionActivityId { get; set; }
        public string Color { get; set; }
        public decimal? MaxScore { get; set; }
        public decimal? Weight { get; set; }
        public bool? IsCredit { get; set; }

        public void WriteTo(Discussion entity, Activity act, long? userId = null)
        {
            if (ActivityId.HasValue) entity.ActivityId = ActivityId.Value;
            if (!string.IsNullOrEmpty(Title)) entity.Title = Title;
            if (Content != null) entity.Content = Content;
            if (Link != null) entity.Link = Link;
            if (ContentType != null) entity.ContentType = ContentType;
            if (MediaType.HasValue) entity.MediaType = MediaType.Value;
            entity.StartTime = StartTime;
            entity.EndTime = EndTime;
            if (userId.HasValue) entity.UpdatedBy = userId.Value;
            entity.IsGraded = IsGraded;
            entity.DiscussionActivityId = DiscussionActivityId;
            if (act == null) act = new Activity { CreatedBy = userId };
            act.IsGraded     = IsGraded;
            act.Type         = ActivityType.Discussion;
            act.ClassId      = entity.ClassId;
            act.Title        = entity.Title;
            act.Color        = Color;
            act.MaxScore     = MaxScore.HasValue ? MaxScore.Value : act.MaxScore;
            act.Weight       = Weight.HasValue ? Weight.Value : act.Weight;
            act.IsCredit     = IsCredit.HasValue ? IsCredit.Value: act.IsCredit;
            act.DateDue      = EndTime;
            act.DateCutoff   = EndTime;
            act.DateAssigned = StartTime;
        }
    }
}
