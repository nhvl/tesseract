﻿using System;
using System.Collections.Generic;
using Tesseract.Core.Entities.NoSql;

namespace Tesseract.Web.Models.Parent
{
    public class ParentStudentDetailView
    {
        public ParentStudentDetailView(FiveCScore scores, IEnumerable<StudentActivityView> activities)
        {
            Scores = scores ?? throw new ArgumentNullException(nameof(scores));
            Activities = activities ?? throw new ArgumentNullException(nameof(activities));
        }

        public FiveCScore Scores { get; set; }
        public IEnumerable<StudentActivityView> Activities { get; set; }
    }
}