﻿using System;
using System.Collections.Generic;
using Tesseract.Web.Models.Common;

namespace Tesseract.Web.Models.Parent
{

    public class StudentActivityView
    {
        public ShortEntityView Class { get; set; }
        public IEnumerable<ShortEntityView> Teachers { get; set; }
        public ShortEntityView Term { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public long Id { get; set; }
        public DateTime? Due { get; set; }
        public bool IsOpen { get; set; }
        public bool Warning { get; set; }
    }
}