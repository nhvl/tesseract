﻿using System;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Web.Models
{
    public class UserNotificationSettingViewModel
    {
        public NotificationType Type { get; set; }
        public NotificationSendType SendType { get; set; }
        public TimeSpan SendTime { get; set; }
        public byte WeeklyDay { get; set; }

        public static UserNotificationSettingViewModel Create(UserNotificationSetting setting)
        {
            return new UserNotificationSettingViewModel
            {
                Type = setting.Type,
                SendType = setting.SendType,
                SendTime = setting.SendTime,
                WeeklyDay = setting.WeeklyDay,
            };
        }
    }
}
