﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tesseract.Web.Models
{
    public class PowerSearchViewModel
    {
        [Required]
        public List<SearchView> Students { get; set; }
        [Required]
        public List<SearchView> Classes { get; set; }
        [Required]
        public List<SearchView> Pages { get; set; }
        [Required]
        public List<SearchView> Assignments { get; set; }
        [Required]
        public List<SearchView> Assessments { get; set; }
        [Required]
        public List<SearchView> Faculties{ get; set; }
        [Required]
        public List<SearchView> Schools { get; set; }
        [Required]
        public List<SearchView> Parents { get; set; }
    }
    public class SearchView
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
