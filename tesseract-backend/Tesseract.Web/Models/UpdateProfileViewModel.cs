﻿using System.ComponentModel.DataAnnotations;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Web.Models
{
    public class UpdateProfileViewModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string Nickname { get; set; }

        //[Phone]
        public string PhoneNumber { get; set; }

        public string Language { get; set; }
        public string Avatar { get; set; }

        public void WriteTo(ApplicationUser user) {
            if (!string.IsNullOrEmpty(this.FirstName )) user.FirstName  = this.FirstName;
            if (!string.IsNullOrEmpty(this.LastName  )) user.LastName   = this.LastName;
            if (this.Salutation != null               ) user.Salutation = this.Salutation;
            if (this.Nickname   != null               ) user.Nickname   = this.Nickname;
            if (!string.IsNullOrEmpty(this.Language  )) user.Language   = this.Language;
            if (!string.IsNullOrEmpty(this.Avatar    )) user.Avatar     = this.Avatar;
            if (this.PhoneNumber != null && user.PhoneNumber != this.PhoneNumber)
            {
                user.PhoneNumber = this.PhoneNumber;
                user.PhoneNumberConfirmed = false;
            }

        }
    }
}
