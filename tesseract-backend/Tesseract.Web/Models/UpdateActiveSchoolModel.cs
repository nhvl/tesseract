﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tesseract.Web.Models
{
    public class UpdateActiveSchoolModel
    {
        public long SchoolId { get; set; }
    }
}
