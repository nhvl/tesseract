﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;

namespace Tesseract.Web.Service.Background
{
    public class DistrictReportRunner : IHostedService, IDisposable
    {
        private readonly ILogger _logger;
        private readonly int _interval;
        private Timer _timer;
        public IServiceScopeFactory _serviceScopeFactory;
       
        public DistrictReportRunner(ILogger<DistrictReportRunner> logger,
                                    IOptions<ServiceSettings> serviceSettings,
                                    IServiceScopeFactory serviceScopeFactory)
        {
            _interval = serviceSettings.Value?.ReportRunnerInSeconds ?? 1800;
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(_interval));
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                try
                {
                    var districtService = scope.ServiceProvider.GetRequiredService<IDistrictService>();
                    Task.Run(async() =>
                    {
                        try
                        {
                            await districtService.CreateQuickReports();
                            _logger.LogInformation("Created and stored district reports.");
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, ex.Message);
                        }
                    }).Wait();
                }
                catch (InvalidOperationException ex)
                {
                    _logger.LogError(ex, $"Can not found service {nameof(IDistrictService)}");
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}