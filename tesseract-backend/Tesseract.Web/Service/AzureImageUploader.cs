﻿using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Config;

namespace Tesseract.Web.Service
{
    public class AzureImageUploader : IImageUploader
    {
        private readonly CloudBlobContainer _container;

        public AzureImageUploader(IOptions<BlobStorageSetttings> settings)
        {
            try
            {
                StorageCredentials storageCredentials = new StorageCredentials(settings.Value.Account, settings.Value.Key);
                CloudStorageAccount storageAccount = new CloudStorageAccount(storageCredentials, true);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                _container = blobClient.GetContainerReference(settings.Value.ImageContainer);
            }
            catch (Exception ex)
            {
                throw new Exception("Can not access to Azure CloudStorageAccount server.", ex);
            }
        }

        public async Task<Uri> UploadFileToStorage(Stream sourceStream, string fileName, string mimeType)
        {
            CloudBlockBlob blockBlob = _container.GetBlockBlobReference(fileName);
            await blockBlob.UploadFromStreamAsync(sourceStream);
            blockBlob.Properties.ContentType = mimeType;
            return blockBlob.Uri;
        }
    }
}

