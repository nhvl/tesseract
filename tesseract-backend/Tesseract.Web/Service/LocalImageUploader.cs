﻿using System;
using System.Threading.Tasks;
using System.IO;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Tesseract.Web.Common;
using Microsoft.AspNetCore.Http;

namespace Tesseract.Web.Service
{
   public class LocalImageUploader: IImageUploader
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        
        public LocalImageUploader(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<Uri> UploadFileToStorage(Stream sourceStream, string fileName, string mimeType)
        {
            var folderName = Path.Combine(AppConstants.Path.Resources, AppConstants.Path.Images);
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

            var fullPath = Path.Combine(pathToSave, fileName);
           
            using (var fileStream = File.Create(fullPath))
            {
                sourceStream.Seek(0, SeekOrigin.Begin);
                sourceStream.CopyTo(fileStream);
            }

            var hostingPath = Path.Combine(folderName, fileName);
            var request = _httpContextAccessor.HttpContext.Request;
            var uri = new Uri($"{request.Scheme}://{request.Host.Value}/{hostingPath.Replace('\\', '/')}");
            return await Task.FromResult(uri);
        }
    }
}

