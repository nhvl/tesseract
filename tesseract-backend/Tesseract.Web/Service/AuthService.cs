﻿#nullable enable

using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Tesseract.Common.Extensions;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Exceptions;
using Tesseract.Infrastructure.Configuration;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Infrastructure.Utils;
using Tesseract.Web.Common;

namespace Tesseract.Web.Service
{
    public interface IAuthService {
        Task<string?> CreateToken(ApplicationUser user, IList<string> roles, Claim[] additionalClaims);
    }

    public class AuthService: IAuthService {
        private readonly AuthenticationSettings _authTokenSettings;
        private readonly IFacultyService _facultyService;
        private readonly IStudentService _studentService;
        private readonly IParentService _parentService;
        private readonly ISchoolService _schoolService;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly TesseractUserManager _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public AuthService(
            AuthenticationSettings authTokenSettings,
            IFacultyService facultyService,
            IStudentService studentService,
            IParentService parentService,
            ISchoolService schoolService,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<ApplicationRole> roleManager
        )
        {
            _authTokenSettings = authTokenSettings ?? throw new ArgumentNullException(nameof(authTokenSettings));
            _facultyService = facultyService ?? throw new ArgumentNullException(nameof(facultyService));
            _studentService = studentService;
            _parentService = parentService;
            _schoolService = schoolService;
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _userManager = signInManager.UserManager as TesseractUserManager ?? throw new ArgumentNullException(nameof(_userManager));
            _roleManager = roleManager;
        }

        public async Task<string?> CreateToken(ApplicationUser user, IList<string> roles, Claim[] additionalClaims)
        {
            var claims = await LoadClaim(user, roles);
            claims.AddRange(additionalClaims);

            if (claims == null) return null;

            var tokenString = TokenHelper.IssueToken(_authTokenSettings, user, roles, claims);
            return tokenString;
        }

        private async Task<List<Claim>> LoadClaim(ApplicationUser user, IList<string> roles)
        {
            var claims = (await _userManager.GetClaimsAsync(user))
                .Concat(await _roleManager.GetClaimsAsync(roles))
                .Concat(new[] {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                })
                .ToList();

            var roleClaims = await LoadRoleClaim(user, roles);
            if (roleClaims != null) {
                claims.AddRange(roleClaims);
            }

            return claims;
        }

        public async Task<List<Claim>?> LoadRoleClaim(ApplicationUser user, IList<string> roles)
        {
            var claims = new List<Claim>();

            if (roles.Contains(ApplicationRoleValue.Faculty))
            {
                var faculty = await _facultyService.GetFacultyByUserIdAsync(user.Id) ?? throw new ResourceNotFoundException();
                claims.Add(new Claim(AppConstants.Security.ClaimTypes.FacultyId, faculty.FacultyId.ToString()));

                var schools = await _schoolService.GetSchoolsOfFaculty(faculty.FacultyId);
                if (schools.Count < 1) throw new ApplicationException($"Faculty #{faculty.FacultyId} has no schools");

                if (schools.All(school => school.SchoolId != user.ActiveSchoolId))
                {
                    user.ActiveSchoolId = schools.First().SchoolId;
                    await _userManager.UpdateAsync(user);
                }

                claims.RemoveAll(x => x.Type == AppConstants.Security.ClaimTypes.ActiveSchoolId);
                claims.Add(new Claim(AppConstants.Security.ClaimTypes.ActiveSchoolId, user.ActiveSchoolId.ToString()));

                return claims;
            }

            if (roles.Contains(ApplicationRoleValue.Student))
            {
                var student = (await _studentService.GetStudentByUserId(user.Id)) ?? throw new ResourceNotFoundException();
                claims.Add(new Claim(AppConstants.Security.ClaimTypes.StudentId, student.StudentId.ToString()));

                var schools = await _schoolService.GetSchoolsOfStudent(student.StudentId);

                if (schools.Count < 1) throw new ApplicationException($"Faculty #{student.StudentId} has no schools");

                if (schools.All(school => school.SchoolId != user.ActiveSchoolId))
                {
                    var currentSchool = (await _schoolService.GetCurrentSchoolsOfStudent(student.StudentId).FirstOrDefaultAsync())?.School
                        ?? schools.First();

                    user.ActiveSchoolId = currentSchool.SchoolId;
                    await _userManager.UpdateAsync(user);
                }

                claims.RemoveAll(x => x.Type == AppConstants.Security.ClaimTypes.ActiveSchoolId);
                claims.Add(new Claim(AppConstants.Security.ClaimTypes.ActiveSchoolId, user.ActiveSchoolId.ToString()));

                return claims;
            }

            if (roles.Contains(ApplicationRoleValue.Parent))
            {
                var parent = await _parentService.GetParentByUserIdAsync(user.Id) ?? throw new ResourceNotFoundException();
                return new List<Claim> {
                    new Claim(AppConstants.Security.ClaimTypes.ParentId, parent.ParentId.ToString()),
                };
            }

            return null;
        }
    }
}
