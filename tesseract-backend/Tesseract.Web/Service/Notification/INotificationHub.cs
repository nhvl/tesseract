﻿using System.Threading.Tasks;

namespace Tesseract.Web.Service.Notification
{
    public interface INotificationHubClient
    {
        Task ThreadChanged(string threadId);
        Task Notify(string message);
    }
}
