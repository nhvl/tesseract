﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Tesseract.Web.Service.Notification
{
    public class NotificationHub : Hub<INotificationHubClient>
    {
    }

    public interface INotificationHubHelper
    {
        Task SendData(long userId, string data);
    }

    public class NotificationHubHelper : INotificationHubHelper
    {
        private readonly IHubContext<NotificationHub> _hubContext;

        public NotificationHubHelper(IHubContext<NotificationHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task SendData(long userId, string data)
        {
            await _hubContext.Clients.User(userId.ToString()).SendAsync("ClassUpdated", data);
        }
    }
}
