﻿using Microsoft.AspNetCore.Http;
using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace Tesseract.Web.Service
{
    public interface IMediaStorageService
    {
        Task<Uri> StoreMediaFile(IFormFile file, Size? newSize = null);
        Task<Uri> StoreGeneralFile(IFormFile file);
        Task<Uri> StoreGeneralFile(MemoryStream stream, string fileName, string mimeType);
    }
}