namespace Tesseract.Web.Service.GoogleApi
{
    public class GoogleMineType{
        public const string Document = "application/vnd.google-apps.document"; 
        public const string SpreadSheet = "application/vnd.google-apps.spreadsheet"; 
        public const string Presentation = "application/vnd.google-apps.presentation"; 
        public const string File = "application/vnd.google-apps.file"; 
    }
}
