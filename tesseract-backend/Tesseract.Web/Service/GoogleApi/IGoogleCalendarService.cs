using System.Threading.Tasks;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;

namespace Tesseract.Web.Service.GoogleApi
{
    public interface IGoogleCalendarService
    {
        Task<CalendarService> GetServiceAsync(long userId);
        Task<CalendarListResource> GetCalendarListResourceAsync(long userId);
        Task<CalendarList> GetCalendarListAsync(long userId);
    }
}
