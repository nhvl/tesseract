using System.IO;
using System.Threading.Tasks;
using Google.Apis.Drive.v3;

namespace Tesseract.Web.Service.GoogleApi
{
    public interface IGoogleDriveService
    {
        Task<DriveService> GetServiceAsync(long userId);
        string ExportMineType(string sourceMineType, out string extension);
        Task<MemoryStream> ExportFileAsync(Google.Apis.Drive.v3.Data.File fileData, long userId, string mimeType);
        Task<MemoryStream> DownloadFileAsync(Google.Apis.Drive.v3.Data.File fileData, long userId);
        Task<Google.Apis.Drive.v3.Data.File> FileInfoAsync(string fileId, long userId, string fields = "");
        Google.Apis.Drive.v3.Data.Permission FilePermission(DriveService service, string fileId, string permissionId,long userId);
        Task<Google.Apis.Drive.v3.Data.Permission> ShareFileAsync(string fileId, long userId);
    }
}
