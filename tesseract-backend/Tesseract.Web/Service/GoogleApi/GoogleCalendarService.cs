using System.Threading.Tasks;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Microsoft.AspNetCore.Http;
using Tesseract.Core.Entities.Config;

namespace Tesseract.Web.Service.GoogleApi
{
    public class GoogleCalendarService : IGoogleCalendarService
    {
        private readonly GoogleApiSettings _gaSettings;
        private readonly IGoogleAuthService _googleAuthService;
        public GoogleCalendarService(GoogleApiSettings gaSettings,
        IGoogleAuthService googleAuthService)
        {
            _gaSettings = gaSettings;
            _googleAuthService = googleAuthService;
        }

        public async Task<CalendarService> GetServiceAsync(long userId){
            var credentials = await _googleAuthService.GetUserCredentialAsync(userId);

            if(credentials == null)
                return null;
                
            //Create Calendar API service.
            CalendarService service = new CalendarService(new BaseClientService.Initializer()
            {
                ApiKey = _gaSettings.DeveloperKey,
                HttpClientInitializer = credentials,
                ApplicationName = _gaSettings.ProjectId,
            });

            return service;
        }

        #region CalendarList
        
        public async Task<CalendarListResource> GetCalendarListResourceAsync(long userId){
            var service = await GetServiceAsync(userId);

            return service.CalendarList;
        }

        public async Task<CalendarList> GetCalendarListAsync(long userId){
            var service = await GetCalendarListResourceAsync(userId);

            var request = service.List();
            
            var list = await request.ExecuteAsync();
            return list;
        }

        public async Task<CalendarListEntry> GetCalendarListEntryAsync(long userId, string calendarId){
            var service = await GetCalendarListResourceAsync(userId);

            var request = service.Get(calendarId);
            
            var entry = await request.ExecuteAsync();
            return entry;
        }
        #endregion

        #region Events
        
        #endregion
    }
}
