using System;
using System.IO;
using System.Threading.Tasks;
using Google.Apis.Download;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Requests;
using Google.Apis.Services;
using Microsoft.AspNetCore.Http;
using Tesseract.Core.Entities.Config;

namespace Tesseract.Web.Service.GoogleApi
{
    public class GoogleDriveService : IGoogleDriveService
    {
        private readonly GoogleApiSettings _gaSettings;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IGoogleAuthService _googleAuthService;
        public GoogleDriveService(GoogleApiSettings gaSettings,
                    IHttpContextAccessor httpContextAccessor,
                    IGoogleAuthService googleAuthService)
        {
            _gaSettings = gaSettings;
            _httpContextAccessor = httpContextAccessor;
            _googleAuthService = googleAuthService;
        }

        public async Task<DriveService> GetServiceAsync(long userId)
        {
            var credentials = await _googleAuthService.GetUserCredentialAsync(userId);

            if(credentials == null)
                return null;
                
            //Create Drive API service.
            DriveService service = new DriveService(new BaseClientService.Initializer()
            {
                ApiKey = _gaSettings.DeveloperKey,
                HttpClientInitializer = credentials,
                ApplicationName = _gaSettings.ProjectId,                
            });

            return service;
        }

        public async Task<MemoryStream> DownloadFileAsync(Google.Apis.Drive.v3.Data.File fileData, long userId){
            var service = await GetServiceAsync(userId);
            FilesResource.GetRequest request = service.Files.Get(fileData.Id);
            // FilesResource.ExportRequest request = service.Files.Export(fileData.Id, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            var file = request.Execute();
            string FileName = file.Name;

            MemoryStream streamDownload = new MemoryStream();

            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                // TODO log for google Download status
                switch (progress.Status)
                {
                    case DownloadStatus.Downloading:
                        {
                            // Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                    case DownloadStatus.Completed:
                        {
                            
                            // Console.WriteLine("Download Completed.");
                            break;
                        }
                    case DownloadStatus.Failed:
                        {
                            // Console.WriteLine("Download failed.");
                            break;
                        }
                }
            };
            request.DownloadWithStatus(streamDownload);
            var writeFile = new StreamWriter(streamDownload);
            await writeFile.FlushAsync();
            streamDownload.Seek(0, SeekOrigin.Begin);

            return streamDownload;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="userId"></param>
        /// <param name="fields">add fields = "*" to get all fields</param>
        /// <returns></returns>
        public async Task<Google.Apis.Drive.v3.Data.File> FileInfoAsync(string fileId, long userId, string fields = ""){
            var service = await GetServiceAsync(userId);
            FilesResource.GetRequest request = service.Files.Get(fileId);
            if(!String.IsNullOrEmpty(fields))
                request.Fields = fields;
            var file = request.Execute();
            return file;
        }
        
        public async Task<MemoryStream> ExportFileAsync(Google.Apis.Drive.v3.Data.File fileData, long userId, string mimeType){
            var service = await GetServiceAsync(userId);
            FilesResource.ExportRequest request = service.Files.Export(fileData.Id, mimeType);
            
            string FileName = fileData.Name;

            MemoryStream streamDownload = new MemoryStream();

            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                // TODO log for google Download status
                switch (progress.Status)
                {
                    case DownloadStatus.Downloading:
                        {
                            // Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                    case DownloadStatus.Completed:
                        {
                            
                            // Console.WriteLine("Download Completed.");
                            break;
                        }
                    case DownloadStatus.Failed:
                        {
                            // Console.WriteLine("Download failed.");
                            break;
                        }
                }
            };
            request.DownloadWithStatus(streamDownload);
            var writeFile = new StreamWriter(streamDownload);
            await writeFile.FlushAsync();
            streamDownload.Seek(0, SeekOrigin.Begin);

            return streamDownload;
        }

        public Google.Apis.Drive.v3.Data.Permission FilePermission(DriveService service, string fileId, string permissionId,long userId){
            Permission permission = service.Permissions.Get(
                fileId, permissionId).Execute();

            return permission;
        }

        /// <summary>
        /// see more: https://developers.google.com/drive/api/v3/manage-sharing
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Permission> ShareFileAsync(string fileId, long userId){
            var service = await GetServiceAsync(userId);
            var batch = new BatchRequest(service);
            var result = new Permission();
            BatchRequest.OnResponse<Permission> callback = delegate (
                Permission permission,
                RequestError error,
                int index,
                System.Net.Http.HttpResponseMessage message)
            {
                if (error != null)
                {
                    // Handle error
                    // Console.WriteLine(error.Message);
                    throw new Exception(error.Message);
                    // result = null;
                }
                else
                {
                    // Console.WriteLine("Permission ID: " + permission.Id);
                    result = permission;
                }
            };

            Permission anyonePermission = new Permission()
            {
                Type = "anyone",
                Role = "reader"
            };
            var request = service.Permissions.Create(anyonePermission, fileId);
            request.Fields = "*";

            // Permission domainPermission = new Permission()
            // {
            //     Type = "domain",
            //     Role = "reader",
            //     Domain = "dory.vn",
            // };
            // var request = service.Permissions.Create(domainPermission, fileData.Id);
            // request.Fields = "id";
            batch.Queue(request, callback);
            try{
                await batch.ExecuteAsync();
            }
            catch (Exception ex){
                Console.WriteLine(ex.Message);
            }
            
            return result;
        }

        #region Utilities

        public string ExportMineType(string sourceMimeType, out string extension){
            var mimeType = "";
            extension = "";
            switch (sourceMimeType)
            {
                case GoogleMineType.Document :
                    // mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; 
                    // extension = ".docx";
                    // break;
                case GoogleMineType.SpreadSheet :
                    // mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; 
                    // extension = ".xlsx";
                    // break;
                case GoogleMineType.Presentation :
                    // mimeType = "application/vnd.openxmlformats-officedocument.presentationml.presentation"; 
                    // extension = ".pptx";
                    // break;
                    mimeType = "application/pdf";
                    extension = ".pdf";
                    break;
                default:
                    mimeType = sourceMimeType;
                    break;
            }

            return mimeType;
        }
        #endregion
    }
}
