using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Responses;

namespace Tesseract.Web.Service.GoogleApi
{
    public interface IGoogleAuthService
    {
        Task<TokenResponse> ExchangeAuthenticationCodeAsync(long userId, string AuthCode);
        Task<UserCredential> GetUserCredentialAsync(long userId);
        Task<TokenResponse> GetTokenAsync(long userId);
        Task RevokeTokenAsync(long userId);
    }
}
