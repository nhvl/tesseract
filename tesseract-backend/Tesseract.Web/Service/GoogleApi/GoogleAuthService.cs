using System;
using System.Threading;
using System.Threading.Tasks;
using EasyCaching.Core;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Calendar.v3;
using Google.Apis.Drive.v3;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.Http;
using Tesseract.Core.Entities.Config;
using Tesseract.Web.Common;
using Tesseract.Web.Extensions;

namespace Tesseract.Web.Service.GoogleApi
{
    public class GoogleAuthService : IGoogleAuthService
    {
        private string fileDataStore = "Google.Api.Auth.Store";
        private string[] Scopes = { DriveService.Scope.Drive, 
                                DriveService.Scope.DriveFile, 
                                DriveService.Scope.DriveMetadata,
                                //TODO remove Calendar Service when unverify google console App
                                // CalendarService.Scope.Calendar,
                                // CalendarService.Scope.CalendarEvents 
                                };
        private readonly GoogleApiSettings _gaSettings;
        private readonly IEasyCachingProvider _cache;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public GoogleAuthService(GoogleApiSettings gaSettings,
        IEasyCachingProviderFactory cacheFactory,
        IHttpContextAccessor httpContextAccessor)
        {
            _gaSettings = gaSettings;
            _cache = cacheFactory.GetCachingProvider(AppConstants.Cache.CacheDefault);
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<UserCredential> GetUserCredentialAsync(long userId)
        {
            // var token = new TokenResponse();// { RefreshToken = refreshToken }; 
            var flow = GetCodeFlow();
            // var token = await flow.LoadTokenAsync(userId.ToString(), CancellationToken.None);
            
            var cachedKey = $"{fileDataStore}-{userId}";
            var cachedValue = await _cache.GetAsync<TokenResponse>(cachedKey);
            var token = cachedValue.HasValue ? cachedValue.Value : await flow.LoadTokenAsync(userId.ToString(), CancellationToken.None);

            if(token != null && token.IsExpired(flow.Clock)){
                token = await flow.RefreshTokenAsync(userId.ToString(), token.RefreshToken, CancellationToken.None);
                await _cache.SetAsync(cachedKey, token, TimeSpan.FromSeconds((double)token.ExpiresInSeconds));
            }

            if(token == null){
                return null;
            }
            
            var credentials = new UserCredential(flow,
                userId.ToString(),
                token);
                
            return credentials;
        }

        public async Task RevokeTokenAsync(long userId)
        {
            var credentials = await GetUserCredentialAsync(userId);
            var revoke = await credentials.RevokeTokenAsync(CancellationToken.None);
            if(revoke)
            {
                var cachedKey = $"{fileDataStore}-{userId}";
                await _cache.RemoveAsync(cachedKey);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="AuthCode">One time code from google authentication with accessType="offline" and responseType="code"</param>
        /// <returns></returns>
        public async Task<TokenResponse> ExchangeAuthenticationCodeAsync(long userId, 
        string AuthCode){
            var redirectUri = _httpContextAccessor.HttpContext.Request.GetHostUrl();
            var flow = GetCodeFlow();
            var tokenResponse = await flow.ExchangeCodeForTokenAsync(userId.ToString(), 
                                            AuthCode, 
                                            redirectUri, 
                                            CancellationToken.None);
            return tokenResponse;
        }

        public async Task<TokenResponse> GetTokenAsync(long userId)
        {
            var credentials = await GetUserCredentialAsync(userId);
            if(credentials == null)
                return null;

            var token = credentials.Token;
            // if(token.IsExpired(credentials.Flow.Clock)){
            //     var refreshCredential = await credentials.RefreshTokenAsync(CancellationToken.None);
            //     if(!refreshCredential)
            //         return null;
            // }

            return token;
        }

        #region Utilities
        private GoogleAuthorizationCodeFlow GetCodeFlow(){
            ClientSecrets secrets = new ClientSecrets()
            {
                ClientId = _gaSettings.ClientId,
                ClientSecret = _gaSettings.ClientSecret,
            };
            var flow = new GoogleAuthorizationCodeFlow(
            new GoogleAuthorizationCodeFlow.Initializer 
            {
                ClientSecrets = secrets,
                Scopes = Scopes,
                DataStore = new FileDataStore(fileDataStore),
            });
            return flow;
        }
        #endregion
    }
}
