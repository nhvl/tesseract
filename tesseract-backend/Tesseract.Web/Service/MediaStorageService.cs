﻿using ImageMagick;
using Microsoft.AspNetCore.Http;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Web.Common;
using Tesseract.Web.Exceptions;
using Tesseract.Web.Extensions;

namespace Tesseract.Web.Service
{
    public class MediaStorageService : IMediaStorageService
    {
        private readonly IImageUploader _uploader;

        public MediaStorageService(IImageUploader uploader)
        {
            _uploader = uploader;
        }

        public async Task<Uri> StoreGeneralFile(IFormFile file)
        {

            var savingName = (DateTime.UtcNow.Ticks+"_"+ RequestHelper.GetFileName(file));
            using (var stream = file.OpenReadStream())
            {
                return await _uploader.UploadFileToStorage(stream, savingName, file.ContentType);
            }
        }

        public async Task<Uri> StoreGeneralFile(MemoryStream stream, string fileName, string mimeType)
        {
            var savingName = (DateTime.UtcNow.Ticks+"_"+ fileName);
            return await _uploader.UploadFileToStorage(stream, savingName, mimeType);
        }

        public async Task<Uri> StoreMediaFile(IFormFile file, Size? size)
        {
            var savingName = (DateTime.UtcNow.Ticks+"_"+ RequestHelper.GetFileName(file));
            const string UnsupportedTypeError = "Only JPEG and PNG images are supported."; // TODO: localizatoin
            using (var stream = file.OpenReadStream())
            {
                try
                {
                    using (MagickImage image = new MagickImage(stream))
                    {
                        if (!image.IsJpg() && !image.IsPng())
                        {
                            throw new UnsupportedMediaTypeException(UnsupportedTypeError);
                        }

                        if (size != null && size != Size.Empty)
                        {
                            image.Resize(new MagickGeometry(size.Value.Width, size.Value.Height)
                            {
                                IgnoreAspectRatio = false
                            });
                        }

                        using (var imageStream = new MemoryStream())
                        {
                            image.Write(imageStream);
                            imageStream.Seek(0, SeekOrigin.Begin);
                            return await _uploader.UploadFileToStorage(imageStream, savingName, file.ContentType);
                        }
                    }
                }
                catch(MagickException)
                {
                    throw new UnsupportedMediaTypeException(UnsupportedTypeError); 
                }
            }
        }
    }
}
