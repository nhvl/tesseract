﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Tesseract.Web.Service
{
    public interface IImageUploader
    {
        Task<Uri> UploadFileToStorage(Stream fileStream, string fileName, string mimeType);
    }
}