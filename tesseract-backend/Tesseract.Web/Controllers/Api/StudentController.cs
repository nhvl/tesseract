﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DiffLib;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;
using Tesseract.Web.Models;

namespace Tesseract.Web.Controllers.Api
{
    [ApiController]
    [Authorize(
        AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme,
        Roles = ApplicationRoleValue.Student,
        Policy = AppConstants.Security.Policy.SchoolAccess
    )]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]/{studentId}/school/{schoolId}")]
    public partial class StudentController : ABasicApiController
    {
        private readonly ISchoolService _schoolService;
        private readonly IGradingTermService _gradingTermService;
        private readonly IClassService _classService;
        private readonly IActivityService _activityService;
        private readonly IActDocService _actDocService;
        private readonly IDiscussionService _discussionService;
        private readonly ICommentService _commentService;
        private readonly IUserService _userService;
        private readonly IFacultyService _facultyService;
        private readonly IMapper _mapper;

        public StudentController(
            ISchoolService schoolService,
            IGradingTermService gradingTermService,
            IClassService classService,
            IActivityService activityService,
            IActDocService actDocService,
            IDiscussionService discussionService,
            ICommentService commentService,
            IUserService userService,
            IFacultyService facultyService,
            IMapper mapper
        )
        {
            _schoolService = schoolService;
            _gradingTermService = gradingTermService;
            _classService = classService;
            _activityService = activityService;
            _actDocService = actDocService;
            _discussionService = discussionService;
            _commentService = commentService;
            _userService = userService;
            _facultyService = facultyService;
            _mapper = mapper;
        }

        #region ActivityScore
        [HttpGet("activity/{activityId}/submission")]
        public async Task<IActionResult> GetSubmission([FromRoute]long activityId, [FromRoute]long studentId, [FromServices] ISubmissionService submissionService)
        {
            var submission = await submissionService.Get(activityId, studentId);
            return Ok(submission);
        }

        [HttpPut("activity/{activityId}/submission")]
        public async Task<IActionResult> MakeSubmission(
            [FromRoute]long activityId, [FromRoute]long studentId,
            [FromBody]ActivitySubmitViewModel submission,
            [FromQuery] bool isSubmitted)
        {
            var applicationUser = await GetCurrentUser();
            if (applicationUser == null) return Unauthorized();

            var activity = await _activityService.GetActivity(activityId);

            if (activity == null) return NotFound($"Activity({activityId}) not found");

            if (!activity.IsAllowSubmission()) return BadRequest($"Cannot submit to activity ({activityId}) anymore");

            var s = await _activityService.MakeSubmission(activityId, studentId, submission.Content, submission.Attachments, applicationUser.Id, isSubmitted);
            return Ok(s);
        }
        #endregion

        #region ActDoc
        [HttpGet("activity/{activityId}/faculty-doc")]
        public async Task<IActionResult> GetTeacherActDoc([FromRoute] long studentId, [FromRoute] long activityId)
        {
            var doc = await _actDocService.Get(activityId);
            if (doc == null) return NotFound($"ActDoc(activityId:{activityId}) not found");

            doc.Items = doc.Items.Where(item =>
                (!item.StartDate.HasValue || item.StartDate.Value < DateTime.UtcNow) &&
                (!item.EndDate  .HasValue || DateTime.UtcNow < item.EndDate.Value)
            ).ToArray();

            return Ok(_mapper.Map<ActDocPreview>(doc));
        }

        void MaskStudentActDoc(StudentActDoc doc, bool removeFeedback) {
            doc.Items = doc.Items.Where(item => (!item.StartDate.HasValue || item.StartDate.Value < DateTime.Now)).ToArray();

            if (removeFeedback) {
                foreach (var item in doc.Items)
                {
                    switch (item)
                    {
                        case ChoiceActItem choiceItem:
                            if (choiceItem.Options != null)
                                foreach (var opt in choiceItem.Options) if (opt != null) opt.IsCorrect = false;
                            break;
                        case MatchActItem matchItem:
                            matchItem.MatchQuizzItems = matchItem.MatchQuizzItemPreview();
                            break;
                        case NumberActItem numberItem:
                            numberItem.NumericMax = null;
                            numberItem.NumericMin = null;
                            break;
                    }
                }
            }
        }

        [HttpGet("activity/{activityId}/doc", Name = "GetStudentActDoc")]
        public async Task<IActionResult> GetActDoc([FromRoute] long studentId, [FromRoute] long activityId)
        {
            var doc = await _actDocService.GetOrCreateStudentDoc(activityId, studentId);
            if (doc == null) return NotFound($"StudentActDoc(activityId:{activityId}, studentId:{studentId}) not found");

            var activity = await _activityService.GetActivity(activityId);
            if (activity == null) return NotFound($"Activity({activityId}) not found");

            MaskStudentActDoc(doc, activity.IsAllowSubmission());
            return Ok(_mapper.Map<StudentActDocView>(doc));
        }

        [HttpPost("activity/{activityId}/doc")]
        public async Task<IActionResult> CreateActDoc([FromRoute] long studentId, [FromRoute] long activityId, [FromBody] StudentActDocRequest docModel)
        {
            var user = await GetCurrentUser() ?? throw new UnauthorizedAccessException();
            var activity = await _activityService.GetActivity(activityId);
            if (activity == null) return NotFound($"Activity({activityId}) not found");
            if (!activity.IsAllowSubmission()) return BadRequest($"Cannot submit to activity ({activityId}) anymore");

            var eDoc = await _actDocService.GetStudentDoc(activityId, studentId);
            if (eDoc != null) return Conflict($"Document for Activity({activityId}) already exist.");

            var doc = _mapper.Map<StudentActDoc>(docModel);
            doc.ActivityId = activityId;
            doc.StudentId = studentId;
            doc.CreatedBy = doc.UpdatedBy = user.Id;

            await _actDocService.CreateStudentDoc(doc);
            return CreatedAtRoute("GetStudentActDoc", new { activityId }, _mapper.Map<StudentActDocView>(doc));
        }

        [HttpPut("activity/{activityId}/doc")]
        public async Task<IActionResult> UpdateActDoc(
            [FromRoute] long studentId, [FromRoute] long activityId,
            [FromBody] StudentActDocRequest docModel, [FromServices]ILogger<StudentController> _logger)
        {
            var user = await GetCurrentUser() ?? throw new UnauthorizedAccessException();

            var activity = await _activityService.GetActivity(activityId);
            if (activity == null) return NotFound($"Activity({activityId}) not found");

            if (!activity.IsAllowSubmission()) return BadRequest($"Cannot submit to activity ({activityId}) anymore");

            var doc = await _actDocService.GetOrCreateStudentDoc(activityId, studentId);
            if (doc == null) return NotFound($"StudentActDoc(activityId:{activityId}, studentId:{studentId}) not found");

            var answerItems = docModel.Items.Where(x => x.Id != null && doc.Answers.Keys.Contains(x.Id)).ToArray();
            foreach(var itemModal in answerItems) {
                var docItem = doc.Items.FirstOrDefault(x => x.Id == itemModal.Id);
                if (docItem == null) {
                    _logger.Log(LogLevel.Warning, $"Can not find doc item from answer ID #{itemModal?.Id}.");
                    continue;
                }
                object answer = null;
                switch(docItem.Type){
                    case Core.Enums.EActItemType.PollQuiz:
                        answer = itemModal.PollItemsAnswer;
                        //Update polling into sql database
                        if (answer != null)
                        {
                            await _activityService.SetPolling(activityId, studentId, itemModal.Id, Convert.ToByte(answer));
                        }
                        break;
                    case Core.Enums.EActItemType.NumericQuestion:
                        answer = itemModal.NumberAnswer;
                        break;
                    case Core.Enums.EActItemType.TextQuiz:
                        answer = itemModal.TextAnswer;
                        break;
                    case Core.Enums.EActItemType.ChoiceQuiz:
                        answer = itemModal.OptionsAnswer;
                        break;
                    case Core.Enums.EActItemType.MatchQuiz:
                        answer = itemModal.MatchQuizzAnswer;
                        break;
                    default:
                        _logger.Log(LogLevel.Warning, $"Doc item #{itemModal?.Id} can not be parsed to answer because it's not question type.");
                        continue;
                }

                doc.Answers[itemModal.Id] = answer != null ? JsonConvert.SerializeObject(answer) : null;
            }

            doc.Items = docModel.Items.Select(item => {
                if (item.Id != null && doc.Answers.Keys.Contains(item.Id)) {
                    return doc.Items.FirstOrDefault(x => x.Id == item.Id);
                }

                return _mapper.Map<BaseActItem>(item);
            }).ToArray();

            var updatedDoc = await _actDocService.UpdateStudentDoc(doc);
            return Ok(_mapper.Map<StudentActDocView>(updatedDoc));
        }

        [HttpPost("activity/{activityId}/doc/pull")]
        public async Task<IActionResult> MergeActDoc([FromRoute] long studentId, [FromRoute] long activityId)
        {
            var sDoc = await _actDocService.GetStudentDoc(activityId, studentId);
            if (sDoc == null)  return BadRequest($"StudentActDoc(studentId:{studentId}, activityId:{activityId}) not found");

            var fDoc = await _actDocService.Get(activityId);
            if (fDoc.DateUpdated < sDoc.DateCreated) return BadRequest($"StudentActDoc is update to date");

            {
                var items = ThreeWayMerge(sDoc, fDoc);
                sDoc.CopyContentFrom(fDoc);
                sDoc.Answers = fDoc.Items.ToDictionary(item => item.Id, item => sDoc.Answers.ContainsKey(item.Id) ? sDoc.Answers[item.Id] : null);
                sDoc.Items = items;

                // TODO: Temporary use DateCreated to track faculty ActDoc version that clone from
                sDoc.DateCreated = fDoc.DateUpdated;

                await _actDocService.UpdateStudentDoc(sDoc);
            }

            var activity = await _activityService.GetActivity(activityId);
            if (activity == null) return NotFound($"Activity({activityId}) not found");

            MaskStudentActDoc(sDoc, activity.IsAllowSubmission());
            return Ok(_mapper.Map<StudentActDocView>(sDoc));
        }

        BaseActItem[] ThreeWayMerge(StudentActDoc sDoc, ActDoc fDoc) {
            var fDict = fDoc.Items.ToDictionary(item => item.Id, item => item);
            var sDict = sDoc.Items.ToDictionary(item => item.Id, item => item);

            var localItemsIds = sDoc.Items.Select(item => item.Id).ToArray();
            var baseItemIds = localItemsIds.Where(id => sDoc.Answers.ContainsKey(id)).ToArray();
            var remoteItemIds = fDoc.Items.Select(item => item.Id).ToArray();
            var mergedItemIds = Merge.Perform(baseItemIds, localItemsIds, remoteItemIds,
                new BasicReplaceInsertDeleteDiffElementAligner<string>(),
                new TakeLeftThenRightMergeConflictResolver<string>())
                .ToArray();

            var items = mergedItemIds.Select(id =>
                (fDict.ContainsKey(id) ? fDict[id] : (
                 sDict.ContainsKey(id) ? sDict[id] : null))
                ).Where(i => i != null);
            var missingItems = sDoc.Items
                .Where(i => !mergedItemIds.Contains(i.Id))
                .Where(i => !sDoc.Answers.ContainsKey(i.Id))
                ;
            return items.Concat(missingItems).ToArray();
        }

        BaseActItem[] NaiveMerge(StudentActDoc sDoc, ActDoc fDoc) {
            var studentItems = sDoc.Items.Where(item => !sDoc.Answers.ContainsKey(item.Id)).ToArray();
            return fDoc.Items.Concat(studentItems).ToArray();
        }
        #endregion

        #region PollingVote
        [HttpGet("activity/{activityId}/pollQuizId/{pollQuizId}")]
        public async Task<IActionResult> GetStudentPollingVote([FromRoute] long activityId, [FromRoute] long studentId, [FromRoute] string pollQuizId)
        {
            return Ok(await _activityService.GetStudentVote(activityId, studentId, pollQuizId));
        }

        [HttpPut("activity/{activityId}/pollQuizId/{pollQuizId}/vote/{vote}")]
        public async Task<IActionResult> UpdatePollingVote(
            [FromRoute] long activityId, [FromRoute] long studentId,
            [FromRoute] string pollQuizId, [FromRoute] byte vote)
        {
            var pollItem = await _activityService.SetPolling(activityId, studentId, pollQuizId, vote);
            if (pollItem == null) return NotFound();
            return Ok(pollItem);
        }

        #endregion

        [HttpGet]
        public async Task<IActionResult> GetSchool([FromRoute] long schoolId, [FromRoute] long studentId)
        {
            var school = await _schoolService.GetSchoolById(schoolId);
            return Ok(SchoolResponse.Create(school, (await _gradingTermService.GetGradingTerms(schoolId)).ToArray()));
        }
    }
}
