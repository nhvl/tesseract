using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Tesseract.Web.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Tesseract.Web.Service.GoogleApi;
using System;
using Tesseract.Core.Entities.Config;
using Tesseract.Web.Service;
using System.Linq;
using Google.Apis.Drive.v3.Data;
using Google;
using Google.Apis.Auth.OAuth2.Responses;

namespace Tesseract.Web.Controllers.Api
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]")]
    public class GoogleApiController : ABasicApiController
    {   
        private readonly IMediaStorageService _mediaStorageService;
        public GoogleApiController(IMediaStorageService mediaStorageService)
        {
            this._mediaStorageService = mediaStorageService;
        }

        #region Auth
        [HttpGet("auth/exchangeAuthenticationCode")]
        public async Task<IActionResult> ExchangeAuthenticationCode([FromServices] IGoogleAuthService _googleAuthService,[FromQuery] string code)
        {
            var currentUser = await GetCurrentUser();
            try{
                var result = await _googleAuthService.ExchangeAuthenticationCodeAsync(currentUser.Id, code);
                return Ok(result.AccessToken);
            }
            catch(TokenResponseException ex){
                // invalid_grant means Token has been expired or revoked.
                if(ex.Error.Error == "invalid_grant")
                    return BadRequest($"{ex.Message}. Please, try again.");
                throw ex;
            }
        }
        [HttpGet("auth/accessToken")]
        public async Task<IActionResult> GetAccessToken([FromServices] IGoogleAuthService _googleAuthService)
        {
            var currentUser = await GetCurrentUser();
            try{
                var refreshCredential = await _googleAuthService.GetTokenAsync(currentUser.Id);
                return Ok(refreshCredential == null ? "" : refreshCredential.AccessToken);
            }
            catch(TokenResponseException ex){
                // invalid_grant means Token has been expired or revoked.
                if(ex.Error.Error == "invalid_grant")
                    return Ok();

                throw ex;
            }
            
        }

        [HttpGet("auth/clientInfo")]
        public IActionResult GetClientInfo([FromServices] GoogleApiSettings _gaSettings)
        {
            return Ok(_gaSettings);
        }

        [HttpGet("auth/revoke")]
        public async Task<IActionResult> RevokeToken([FromServices] IGoogleAuthService _googleAuthService)
        {
            var currentUser = await GetCurrentUser();
            await _googleAuthService.RevokeTokenAsync(currentUser.Id);
            return Ok();
        }
        #endregion

        #region Drive
        [HttpPost("drive/download")]
        public async Task<IActionResult> DownloadFile(
            [FromServices] IGoogleDriveService _googleDriveService,
            Google.Apis.Drive.v3.Data.File fileData)
        {
            var currentUser = await GetCurrentUser();
            var exchangeMineType = _googleDriveService.ExportMineType(fileData.MimeType, out var extension);

            var streamDownload = string.IsNullOrEmpty(extension)
                ? await _googleDriveService.DownloadFileAsync(fileData, currentUser.Id)
                : await _googleDriveService.ExportFileAsync(fileData, currentUser.Id, exchangeMineType);

            var path = await _mediaStorageService.StoreGeneralFile(streamDownload, fileData.Name + extension, exchangeMineType);
            return Ok(path);
        }

        [HttpGet("drive/fileInfo")]
        public async Task<IActionResult> fileInfo(
            [FromServices] IGoogleDriveService _googleDriveService,
            [FromQuery] string fileId)
        {
            var currentUser = await GetCurrentUser();

            var fileInfo = await _googleDriveService.FileInfoAsync(fileId, currentUser.Id);

            return Ok(fileInfo);
        }

        [HttpGet("drive/filePermissionInfo")]
        public async Task<IActionResult> filePermissionInfo(
            [FromServices] IGoogleDriveService _googleDriveService,
            [FromQuery] string fileId)
        {
            var currentUser = await GetCurrentUser();
            var permission = new Permission();
            try{
                var service = await _googleDriveService.GetServiceAsync(currentUser.Id);
                if(service == null)
                {
                    return Ok();
                }
                permission = _googleDriveService.FilePermission(service, fileId, "anyoneWithLink", currentUser.Id);
            }
            catch(GoogleApiException ex){
                if(ex.Error.Code != 404 && ex.Error.Code != 403)
                    throw ex;
            }
            return Ok(permission);
        }

        [HttpGet("drive/shareFile")]
        public async Task<IActionResult> shareFile(
            [FromServices] IGoogleDriveService _googleDriveService,
            [FromQuery] string fileId)
        {
            var currentUser = await GetCurrentUser();
            // var getRole = await _googleApiService.FileInfoAsync(fileId, currentUser.Id, "permissions");

            var shareFile = await _googleDriveService.ShareFileAsync(fileId, currentUser.Id);
            return Ok(shareFile);
        }
        #endregion

        #region Calendar
        [HttpGet("calendar/CalendarList")]
        public async Task<IActionResult> CalendarList(
            [FromServices] IGoogleCalendarService _googleCalendarService)
        {
            var currentUser = await GetCurrentUser();
            // var getRole = await _googleApiService.FileInfoAsync(fileId, currentUser.Id, "permissions");

            var calendarList = await _googleCalendarService.GetCalendarListResourceAsync(currentUser.Id);
            return Ok(calendarList);
        }
        #endregion
    }
}
