﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;
using Tesseract.Web.Models;

namespace Tesseract.Web.Controllers.Api
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = AppConstants.Security.Policy.CrossedPortalAccess)]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]")]
    public class SchoolController : ABasicApiController
    {
        private readonly ISchoolService _schoolService;

        public SchoolController(ISchoolService schoolService)
        {
            _schoolService = schoolService;
        }

        [HttpPost("{schoolId}/student")]
        public async Task<IActionResult> AddStudentToSchool([FromRoute]long schoolId, RegisterStudentToSchoolViewModel student)
        {
            await _schoolService.RegisterStudentAsync(new SchoolStudent
            {
                SchoolId      = schoolId,
                StudentId     = student.StudentId,
                Grade         = student.Grade,
                GradingPeriod = student.GradingPeriod,
            });
            return Ok();
        }

        [HttpPost("{schoolId}/faculty")]
        public async Task<IActionResult> AddFacultyToSchool([FromRoute]long schoolId, [FromBody] long facultyId)
        {
            await _schoolService.RegisterFacultyAsync(schoolId, facultyId);
            return Ok();
        }
    }
}
