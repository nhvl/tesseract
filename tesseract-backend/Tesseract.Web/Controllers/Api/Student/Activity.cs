using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Web.Controllers.Api
{
    public partial class StudentController
    {
        [HttpGet("activity")]
        public async Task<IActionResult> GetActivitiesForStudent([FromRoute] long studentId, [FromRoute] long schoolId, [FromQuery] ActivityType? type)
        {
            var records = await _activityService.GetActivitiesOfStudent(studentId, schoolId, null, type);
            await _activityService.EnsureActivitiesHasActivityScoreWithThreadId(records, studentId);
            var dto = GeneralRecord.ToGeneralDto(records);
            dto.ActivityCountDiscussions = await _discussionService.CountDiscussionOfActivity(dto.Activities.Select(a => a.ActivityId).ToArray());
            return Ok(dto.ToObject());
        }

        [HttpGet("class/{classId}/activity")]
        public async Task<IActionResult> GetActivitiesInClassForStudent([FromRoute] long studentId, [FromRoute] long schoolId, [FromRoute] long classId, [FromQuery] ActivityType? type)
        {
            var records = await _activityService.GetActivitiesOfStudent(studentId, schoolId, classId, type);
            await _activityService.EnsureActivitiesHasActivityScoreWithThreadId(records, studentId);
            var dto = GeneralRecord.ToGeneralDto(records);
            dto.ActivityCountDiscussions = await _discussionService.CountDiscussionOfActivity(dto.Activities.Select(a => a.ActivityId).ToArray());
            return Ok(dto.ToObject());
        }

        [HttpGet("activity/{activityId}")]
        public async Task<IActionResult> GetActivityForStudent([FromRoute]long studentId, [FromRoute]long activityId)
        {
            var data = await _activityService.GetActivityWithScore(studentId, activityId);
            if (data == null || data.Activity == null) return NotFound();

            if (data.ActivityScore == null) data.ActivityScore = await _activityService.GetActivityScoreOrCreate(activityId, studentId);
            else await _activityService.EnsureActivityScoreHasThreadId(data.ActivityScore);

            var dto = GeneralRecord.ToGeneralDto(new[]{data});
            return Ok(dto.ToObject());
        }
    }
}
