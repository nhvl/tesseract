using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tesseract.Common.Extensions;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Web.Controllers.Api
{
    public partial class StudentController
    {
        [HttpGet("class/{classId}/discussion")]
        public async Task<IActionResult> GetClassDiscussions([FromRoute]long schoolId, [FromRoute]long classId)
        {
            var discussions = (await _discussionService.GetDiscussionsOfClassAsStudent(classId)).ToArray();
            var dto = await IncludeDataForDiscussions(discussions);
            return Ok(dto.ToObject());
        }

        [HttpGet("activity/{activityId}/discussion")]
        public async Task<IActionResult> GetActivityDiscussions([FromRoute]long schoolId, [FromRoute]long activityId)
        {
            var discussions = (await _discussionService.GetDiscussionsOfActivityAsStudent(activityId)).ToArray();
            var dto = await IncludeDataForDiscussions(discussions);
            return Ok(dto.ToObject());
        }

        [HttpGet("class/{classId}/discussion/{discussionId}")]
        public async Task<IActionResult> GetDiscussion([FromRoute]long schoolId, [FromRoute]long classId, [FromRoute]long discussionId)
        {
            var discussion = await _discussionService.GetById(discussionId);
            if (!discussion.StartTime.HasValue || DateTime.UtcNow < discussion.StartTime.Value) return NotFound();
            var discussions = new[] {discussion};
            var users = (await GetCreatedUserInfoInDiscussions(discussions));
            return Ok(new GeneralDto{
                Discussions = discussions,
                Users = users,
            }.ToObject());
        }

        private async Task<ApplicationUser[]> GetCreatedUserInfoInDiscussions(Discussion[] discussions) {
            var userIds = discussions.Select(d => d.CreatedBy).Where(userId => userId.HasValue).Select(userId => userId.Value).ToArray();
            return (await _userService.GetUsersById(userIds)).ToArray();
        }
        private async Task<GeneralDto> IncludeDataForDiscussions(Discussion[] discussions)
        {
            var threadIds = discussions.Select(d => d.ThreadId).ToArray();

            var pLastComments = _commentService.GetLastCommentOfThreads(threadIds);

            var user = await GetCurrentUser();
            var pThreadUnreads = (_commentService.CountNewComments(user.Id, threadIds));

            var lastComments = (await pLastComments).ToArray();
            var pUsers = _userService.GetUsersById(
                discussions.SelectNotNull(d => d.CreatedBy).Concat(
                        lastComments.SelectNotNull(c => c.CreatedBy)
                    )
                    .Distinct()
                    .ToArray());

            var users = (await pUsers).ToArray();
            var threadUnreads = (await pThreadUnreads).ToArray();
            
            return new GeneralDto {
                Discussions = discussions,
                Users = users,
                ThreadUnreads = threadUnreads,
                Comments = lastComments,
            };
        }
    }
}
