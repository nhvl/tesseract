using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Models;

namespace Tesseract.Web.Controllers.Api
{
    public partial class StudentController
    {
        [HttpGet("class")]
        public async Task<IActionResult> GetClassFromStudentId([FromRoute] long schoolId, [FromRoute] long studentId)
        {
            var classRecords = await (await _classService.GetClassesOfStudent(studentId, schoolId)).ToListAsync();
            var facultyRecords = await _facultyService.GetFacultiesOfClasses(classRecords.Select(r => r.Class.ClassId).ToArray()).ToListAsync();

            // var dto = GeneralRecord.ToGeneralDto(classRecords.Concat(facultyRecords));
            // return Ok(dto.ToObject());

            var classModels = classRecords.Select(x =>
            {
                var model = _mapper.Map<StudentClassViewModel>(x.Class);
                model.Faculties = facultyRecords.Where(r => r.ClassFaculty.ClassId == x.Class.ClassId).Select(r => _mapper.Map<UserViewModel>(r.User)).ToArray();
                model.Grade = x.ClassStudent.CumulativeGrade;
                model.SortIndex = x.ClassStudent.SortIndex;
                return model;
            });
            return Ok(classModels);
        }

        [HttpPut("class")]
        public async Task<IActionResult> BatchUpdateClassStudents(
            [FromRoute] long studentId, [FromRoute] long schoolId,
            [FromBody] StudentClassUpdateRequest[] data)
        {
            var classIds = data.Select(c => c.ClassId).ToArray();
            var rs = await _classService.GetClassesOfStudentInSchool(schoolId, studentId, classIds).ToListAsync();
            var dto = GeneralRecord.ToGeneralDto(rs);
            var classes = dto.Classes;
            var classStudents = dto.ClassStudents;
            var notFoundClassIds = classIds.Where(classId => classes.All(c => c.ClassId != classId)).ToArray();
            if (notFoundClassIds.Length > 0) return NotFound($"Class not found: {string.Join(", ", notFoundClassIds)}");

            var changedClassStudents = new List<ClassStudent>(classStudents.Length);

            foreach (var vm in data)
            {
                var classStudent = classStudents.FirstOrDefault(c => c.ClassId == vm.ClassId);
                if (classStudent == null) return Unauthorized();

                var isChanged = vm.WriteTo(classStudent);
                if (isChanged) changedClassStudents.Add(classStudent);
            }

            await _classService.UpdateClassStudents(changedClassStudents);

            return Ok(dto.ToObject());
        }

        [HttpGet("asFacultyClass")]
        public async Task<IActionResult> GetAsFacultyClass([FromRoute] long schoolId, [FromRoute] long studentId)
        {
            var classRecords = await _classService.GetClassesOfFacultyInSchool(schoolId, studentId).ToListAsync();
            var dto = GeneralRecord.ToGeneralDto(classRecords);
            return Ok(dto.ToObject());
        }
        [HttpGet("gradingTerm/{gradingTermId}/asFacultyClass")]
        public async Task<IActionResult> GetAsFacultyClass([FromRoute] long schoolId, [FromRoute] long studentId, [FromRoute] long gradingTermId)
        {
            var classRecords = await _classService.GetClassesOfFacultyInSchool(schoolId, studentId, gradingTermId).ToListAsync();
            var dto = GeneralRecord.ToGeneralDto(classRecords);
            return Ok(dto.ToObject());
        }
    }
}
