using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tesseract.Web.Models;

namespace Tesseract.Web.Controllers.Api
{
    public partial class ParentController : ABasicApiController
    {
        [HttpGet("{parentId}/students")]
        public async Task<IActionResult> GetStudents([FromRoute] long parentId)
        {
            var students = (await _studentService.GetStudentsOfParent(parentId).ToListAsync())
                .Select(r => {
                    var s = r.Student;
                    if (s.User == null) s.User = r.User;
                    return s;
                })
                .ToList();

            var studentModels = _mapper.Map<List<ParentStudentView>>(students);
            await Task.WhenAll(studentModels.Select(async (model) => {
                var activities = await _studentService.GetOpenActivities(model.StudentId);
                model.Activities = activities.Select(act => new OpenActivityView(act.ActivityId, act.DateDue)).ToArray();
            }));

            return Ok(studentModels);
        }
    }
}
