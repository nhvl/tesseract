using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Web.Controllers.Api
{
    public partial class ParentController : ABasicApiController
    {
        [HttpGet("{parentId}/student/{studentId}/activity")]
        public async Task<IActionResult> GetStudentActivities([FromRoute] long parentId, [FromRoute] long studentId)
        {
            if (!await ValidateParentStudentRelationShip(parentId, studentId)) throw new InvalidCredentialException(parentId.ToString());

            var schools = await _schoolService.GetCurrentSchoolsOfStudent(studentId).Select(r => r.School).ToArrayAsync();
            var records = await _activityService.GetActivitiesOfStudentAsParent(studentId, schools.Select(s => s.SchoolId).ToArray()).ToListAsync();
            var dto = GeneralRecord.ToGeneralDto(records);
            dto.Schools = schools;

            return Ok(dto.ToObject());
        }

        [HttpGet("{parentId}/student/{studentId}/class/{classId}/activity")]
        public async Task<IActionResult> GetStudentClassActivities([FromRoute]long parentId, [FromRoute]long studentId, [FromRoute]long classId)
        {
            if (!await ValidateParentStudentRelationShip(parentId, studentId)) throw new InvalidCredentialException(parentId.ToString());

            var xs = await _activityService.GetActivitiesOfStudentInClass(studentId, classId);

            return Ok(GeneralRecord.ToGeneralDto(xs).ToObject());
        }
    }
}
