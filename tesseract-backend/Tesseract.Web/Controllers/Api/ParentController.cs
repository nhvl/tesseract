﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Web.Controllers.Api
{
    [ApiController]

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = ApplicationRoleValue.Parent, Policy = AppConstants.Security.Policy.GeneralAccess)]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]")]
    public partial class ParentController : ABasicApiController
    {
        private readonly IFacultyService  _facultyService;
        private readonly IStudentService  _studentService;
        private readonly IParentService   _parentService;
        private readonly ISchoolService   _schoolService;
        private readonly IClassService    _classService;
        private readonly IActivityService _activityService;
        private readonly IMapper          _mapper;

        public ParentController(
            IFacultyService  facultyService,
            IStudentService  studentService,
            IParentService   parentService,
            ISchoolService   schoolService,
            IClassService    classService,
            IActivityService activityService,
            IMapper          mapper
        )
        {
            _facultyService  = facultyService;
            _studentService  = studentService;
            _parentService   = parentService;
            _schoolService   = schoolService;
            _classService    = classService;
            _activityService = activityService;
            _mapper          = mapper;
        }

        #region Class
        [HttpGet("{parentId}/student/{studentId}/class")]
        public async Task<IActionResult> GetStudentClasses([FromRoute]long parentId, [FromRoute]long studentId)
        {
            if (!await ValidateParentStudentRelationShip(parentId, studentId)) throw new InvalidCredentialException(parentId.ToString());

            var dto = await GetStudentClasses(studentId);
            return Ok(dto.ToObject());
        }
        #endregion

        #region Class
        [HttpGet("{parentId}/student/{studentId}/class/{classId}/faculty")]
        public async Task<IActionResult> GetStudentClassFaculties([FromRoute]long parentId, [FromRoute]long studentId, [FromRoute]long classId)
        {
            if (!await ValidateParentStudentRelationShip(parentId, studentId)) throw new InvalidCredentialException(parentId.ToString());

            var records = await _facultyService.GetFacultiesOfClasses(new[]{classId}).ToListAsync();
            return Ok(GeneralRecord.ToGeneralDto(records).ToObject());
        }
        #endregion

        #region ActivityScore
        [HttpGet("{parentId}/student/{studentId}/5c")]
        public async Task<IActionResult> GetStudent5cScore([FromRoute]long parentId, [FromRoute]long studentId)
        {
            if (!await ValidateParentStudentRelationShip(parentId, studentId)) throw new InvalidCredentialException(parentId.ToString());

            var scoreModel = await GetStudent5cScore(studentId);
            return Ok(scoreModel);
        }
        #endregion

        // ------

        private async Task<GeneralDto> GetStudentClasses(long studentId)
        {
            var classStudentRecords = await _classService.GetCurrentClassesOfStudent(studentId).ToListAsync();
            var classIds = classStudentRecords.Select(x => x.Class.ClassId).Distinct().ToArray();

            var classFacultyRecords = await _facultyService.GetFacultiesOfClasses(classIds).ToListAsync();
            var classStudentDto = GeneralRecord.ToGeneralDto(classStudentRecords);
            var classFacultyDto = GeneralRecord.ToGeneralDto(classFacultyRecords);

            classStudentDto.Users = classFacultyDto.Users;
            classStudentDto.Faculties = classFacultyDto.Faculties;
            classStudentDto.ClassFaculties = classFacultyDto.ClassFaculties;

            return classStudentDto;
        }

        private async Task<FiveCScore> GetStudent5cScore(long studentId)
        {
            var scores = await _studentService.GetActivityScores(studentId);
            return new FiveCScore(scores);
        }


        // ------

        private Task<bool> ValidateParentStudentRelationShip(long parentId, long studentId)
        {
            return _studentService.GetStudentsOfParent(parentId).AnyAsync(r => r.Student.StudentId == studentId);
        }
    }
}
