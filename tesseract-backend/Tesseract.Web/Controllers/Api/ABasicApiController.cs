﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Extensions;

namespace Tesseract.Web.Controllers.Api
{
    public class ABasicApiController : ControllerBase
    {
        public async Task<T> ExecuteResource<T>(Func<Task<T>> executingFnc) => ModelState.IsValid ? await executingFnc() : default;

        protected Task<ApplicationUser> GetCurrentUser() => GetService<SignInManager<ApplicationUser>>()?.UserManager.FindByIdAsync(User.Identity.Name);
        protected async Task<IList<string>> GetCurrentRoles()
        {
            var user = await GetCurrentUser();
            if (user == null) return new List<string>(0);
            return (await GetService<SignInManager<ApplicationUser>>()?.UserManager.GetRolesAsync(user)) ?? new List<string>(0);
        }

        protected T GetService<T>() => (T)this.HttpContext.RequestServices.GetService(typeof(T));

        protected async Task<long> GetRequestDistrictId()
        {
            var config = this.GetService<IConfiguration>();

            var isCustomBinderUsed = config.GetSection("CustomBinderValues:enabled").Get<bool>();
            var domain = isCustomBinderUsed ? config.GetSection("CustomBinderValues:data:domain").Get<string>() : this.HttpContext.GetDistrictDomain();

            return (await this.GetService<IDistrictService>().GetByDomain(domain))?.DistrictId ?? throw new KeyNotFoundException();
        }

        protected static bool TryGetUploadingFile(IEnumerable<IFormFile> files, out IFormFile file) => (file = files.FirstOrDefault())?.Length > 0;
    }
}
