﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;
using Tesseract.Web.Models;
using Tesseract.Web.Service.Notification;

namespace Tesseract.Web.Controllers.Api
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Faculty, Student", Policy = AppConstants.Security.Policy.CrossedPortalAccess)]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/thread")]
    public class CommentController : ABasicApiController
    {
        private readonly ICommentService _commentService;
        private readonly IUserService _userService;
        private readonly IHubContext<NotificationHub, INotificationHubClient> _hubContext;

        public CommentController(ICommentService commentService, IUserService userService, IHubContext<NotificationHub, INotificationHubClient> hubContext)
        {
            _commentService = commentService;
            _userService = userService;
            _hubContext = hubContext;
        }

        [HttpGet("{threadId}/comments")]
        public async Task<IActionResult> GetCommentsOfThread([FromRoute] string threadId)
        {
            var pUser = GetCurrentUser();
            var pComments = _commentService.GetCommentsOfThread(threadId);
            var pUserIds = _commentService.GetUserIdsOfThread(threadId);

            var user = await pUser;
            var pCommentSeen = _commentService.GetCommentSeen(threadId, user.Id);
            var userIds = (await pUserIds).Where(u => u != user.Id).ToArray();
            var users = await _userService.GetUsersById(userIds);
            users.Add(user);
            var comments = await pComments;
            var commentSeen = await pCommentSeen;

            return Ok(new GeneralDto
            {
                Users = users.ToArray(),
                Comments = comments.ToArray(),
                CommentSeens = commentSeen == null ? new CommentSeen[0] : new[]{commentSeen},
            }.ToObject());
        }

        [HttpPost("{threadId}/comments")]
        public async Task<IActionResult> CreateComment([FromRoute] string threadId, [FromBody] CreateCommentRequest vm)
        {
            var currentUser = await GetCurrentUser();
            if (currentUser == null) return Unauthorized();

            var comment = await _commentService.Create(vm.ToComment(threadId, currentUser.Id));

            await Notify(threadId, currentUser.Id);

            return Ok(comment);
        }

        [HttpDelete("{threadId}/comments/{commentId}")]
        public async Task<IActionResult> Delete([FromRoute] string threadId, [FromRoute] long commentId)
        {
            var comment = await _commentService.Delete(commentId);
            return Ok(comment);
        }

        [HttpPost("{threadId}/seen")]
        public async Task<IActionResult> SetSeen([FromRoute] string threadId, [FromBody] DateTime? lastSeen)
        {
            var currentUser = await GetCurrentUser();
            if (currentUser == null) return Unauthorized();

            var lastSeenValue = ((lastSeen.HasValue && lastSeen.Value < DateTime.UtcNow)
                ? lastSeen.Value : DateTime.UtcNow);

            var commentSeen = await _commentService.SetCommentSeen(threadId, currentUser.Id, lastSeenValue);

            return Ok(commentSeen);
        }

        private async Task Notify(string threadId, long creatorUserId)
        {
            var userIds = (await _commentService.GetUserIdsOfThread(threadId)).Where(u => u != creatorUserId);
            await Task.WhenAll(userIds.Select(userId => _hubContext.Clients.User(userId.ToString()).ThreadChanged(threadId)));
        }
    }
}
