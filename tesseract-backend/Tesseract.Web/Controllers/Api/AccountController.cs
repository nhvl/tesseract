﻿#nullable enable

using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Configuration;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Infrastructure.Utils;
using Tesseract.Web.Common;
using Tesseract.Web.Extensions;
using Tesseract.Web.Models;
using Tesseract.Web.Service;

namespace Tesseract.Web.Controllers.Api
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = AppConstants.Security.Policy.CrossedPortalAccess)]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]/[action]")]
    public class AccountController : ABasicApiController
    {
        private readonly AuthenticationSettings _authTokenSettings;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IFacultyService _facultyService;
        private readonly IStudentService _studentService;
        private readonly IParentService _parentService;
        private readonly ISchoolService _schoolService;
        private readonly IAppMailService _appMailService;
        private readonly IUserNotificationSettingService _userNotificationSettingService;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IUserService _userService;
        private readonly IAuthService _authService;
        private readonly IMapper _mapper;
        private readonly ILogger<AccountController> _logger;

        public AccountController(
            AuthenticationSettings authTokenSettings,
            SignInManager<ApplicationUser> signInManager,
            IUserService userService,
            IFacultyService facultyService,
            IStudentService studentService,
            IParentService parentService,
            ISchoolService schoolService,
            IAppMailService appMailService,
            IUserNotificationSettingService userNotificationSettingService,
            RoleManager<ApplicationRole> roleManager,
            IAuthService authService,
            IMapper mapper,
            ILogger<AccountController> logger)
        {
            _userService = userService;
            _authTokenSettings = authTokenSettings ?? throw new ArgumentNullException(nameof(authTokenSettings));
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _facultyService = facultyService ?? throw new ArgumentNullException(nameof(facultyService));
            _studentService = studentService;
            _parentService = parentService;
            _schoolService = schoolService;
            _appMailService = appMailService ?? throw new ArgumentNullException(nameof(appMailService));
            _userNotificationSettingService = userNotificationSettingService;
            _roleManager = roleManager;
            _authService = authService;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(
            [ModelBinder] string domain,
            [FromBody] LoginViewModel login,
            [FromServices] ILoginLogService loginLogService
        )
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var err = await TryLogin(login.Email, login.Password, login.RememberMe);
            if (err != null) return BadRequest(err);

            var user = await _signInManager.UserManager.FindByEmailAsync(login.Email);

            if (user.DistrictId != await GetRequestDistrictId()) return BadRequest();

            var roles = await _signInManager.UserManager.GetRolesAsync(user);

            var token = await _authService.CreateToken(user, roles, new[] {
                new Claim(AppConstants.Security.ClaimTypes.DistrictDomain, domain),
            });
            var loginSucceeded = !string.IsNullOrEmpty(token);

            await loginLogService.CreateLoginAsync(new LoginLog
            {
                LoginContent = login.Email,
                Success      = loginSucceeded,
                LoginTime    = DateTime.UtcNow,
                RemoteIP     = HttpContext.Connection.RemoteIpAddress.ToString(),
                UserID       = user.Id,
                Domain       = HttpContext.Request.Host.Value,
                Role         = string.Join(',', roles)
            });

            if (!loginSucceeded) return BadRequest(ErrorByCode.LoginInvalid);

            var userModel = _mapper.Map<LoggedInUserViewModel>(user);
            userModel.Token = token;
            return Ok(userModel);
        }

        private async Task<ErrorByCode?> TryLogin(string email, string password, bool isPersistent)
        {
            var result = await _signInManager.PasswordSignInAsync(email, password, isPersistent, true);
            if (result.Succeeded) return null;

            if (result.IsLockedOut) return (ErrorByCode.UserIsLockedOut);
            if (result.RequiresTwoFactor) return (ErrorByCode.LoginRequiresTwoFactor);
            if (result.IsNotAllowed) {
                var user = await _signInManager.UserManager.FindByEmailAsync(email);
                var isEmailConfirmed = await _signInManager.UserManager.IsEmailConfirmedAsync(user);
                if (!isEmailConfirmed) return (ErrorByCode.EmailNotConfirm);
            }
            return (ErrorByCode.LoginInvalid);
        }

        [HttpGet]
        public async Task<IActionResult> Me([FromServices] IStudentService studentService, [FromServices] IParentService parentService)
        {
            var user = await this.GetCurrentUser();
            if (user == null)
            {
                return this.Challenge();
            }

            var userManager = this._signInManager.UserManager;

            var roles = await this._userService.GetUserRoles(user);
            var permissions = (await this._userService.GetRoleClaims(roles))
                .Concat(await userManager.GetClaimsAsync(user))
                .Where(x => x.Type == Permission.ClaimType)
                .Select(x => x.Value)
                .Distinct()
                .ToArray();

            // First system roles (no custome roles)
            var systemRoles = roles.Where(r => r.IsSystemRole).ToArray();
            var role =
                systemRoles.Any(r => r.Name == ApplicationRoleValue.Student) && this.User.Claims.Any(c => c.Type == AppConstants.Security.ClaimTypes.StudentId) ? systemRoles.First(r => r.Name == ApplicationRoleValue.Student) : (
                systemRoles.Any(r => r.Name == ApplicationRoleValue.Faculty) && this.User.Claims.Any(c => c.Type == AppConstants.Security.ClaimTypes.FacultyId) ? systemRoles.First(r => r.Name == ApplicationRoleValue.Faculty) : (
                systemRoles.Any(r => r.Name == ApplicationRoleValue.Parent) && this.User.Claims.Any(c => c.Type == AppConstants.Security.ClaimTypes.ParentId) ? systemRoles.First(r => r.Name == ApplicationRoleValue.Parent) :
                systemRoles.FirstOrDefault()))
            ;

            var userModel = this._mapper.Map<DetailUserViewModel>(user);
            userModel.Role = role?.Name ?? string.Empty;
            userModel.Permissions = permissions;

            if (this.User.IsInRole(ApplicationRoleValue.Faculty))
            {
                var faculty = await this._facultyService.GetFacultyByUserIdAsync(user.Id);
                if (faculty == null)
                {
                    return this.BadRequest();
                }

                var schools = await this._schoolService.GetSchoolsOfFaculty(faculty.FacultyId);
                userModel.FacultyId = faculty.FacultyId;
                userModel.Schools = schools.Select(x => x.SchoolId).Distinct();
                userModel.SchoolList = schools.Select(x => new { x.SchoolId, x.SchoolName }).Distinct();
            }
            else if (this.User.IsInRole(ApplicationRoleValue.Student))
            {
                var student = await studentService.GetStudentByUserId(user.Id);
                var schools = await this._schoolService.GetSchoolsOfStudent(student.StudentId);
                userModel.StudentId = student.StudentId;
                userModel.StudentNumber = student.StudentNumber;
                userModel.Schools = schools.Select(x => x.SchoolId).Distinct();
                userModel.SchoolList = schools.Select(x => new { x.SchoolId, x.SchoolName }).Distinct();
            }
            else if (this.User.IsInRole(ApplicationRoleValue.Parent))
            {
                var parent = await parentService.GetParentByUserIdAsync(user.Id);
                userModel.ParentId = parent.ParentId;
            }

            return this.Ok(userModel);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeRole([FromBody] string role, [ModelBinder] string domain) {
            var user = await GetCurrentUser();
            var roles = await _signInManager.UserManager.GetRolesAsync(user);
            if (!roles.Contains(role)) return Forbid();

            var token = await _authService.CreateToken(user, new [] { role }, new[] {
                new Claim(AppConstants.Security.ClaimTypes.DistrictDomain, domain),
            });
            if (string.IsNullOrEmpty(token))
            {
                return BadRequest();
            }

            return Ok(token);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProfile(UpdateProfileViewModel model)
        {
            var applicationUser = await _signInManager.UserManager.FindByIdAsync(User.Identity.Name);
            if (applicationUser == null) return Unauthorized();

            if (!ModelState.IsValid) return BadRequest(ModelState);
            // TODO: validate PhoneNumber

            model.WriteTo(applicationUser);

            var result = await _signInManager.UserManager.UpdateAsync(applicationUser);
            if (result.Succeeded) return Ok();
            return BadRequest(result);
        }

        [HttpPut]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            var applicationUser = await GetCurrentUser();
            if (applicationUser == null) return Unauthorized();

            if (!ModelState.IsValid) return BadRequest(ModelState);

            var result = await _signInManager.UserManager.ChangePasswordAsync(applicationUser, model.Password, model.NewPassword);

            if (result.Succeeded) return Ok();
            return BadRequest(result);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgetPasswordViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var result = await _appMailService.ForgotPassword(model.Email, model.Host);
            return Ok(new { Result = result ? 1 : -1});
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var user = await _signInManager.UserManager.FindByNameAsync(model.Email);
            if (user == null) {
                // Don't reveal that the user does not exist
                return Ok(new {IsSuccess = false, Result="AccountNotExist"});
            }

            var result = await _signInManager.UserManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (!result.Succeeded) return Ok(new { IsSuccess = false, Result = result.Errors.FirstOrDefault()?.Code });

            var token = await _signInManager.UserManager.GenerateEmailConfirmationTokenAsync(user);
            await _signInManager.UserManager.ConfirmEmailAsync(user, token);

            return Ok(new { IsSuccess = true, Result = "Ok"});
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid) return BadRequest(ModelState);

            var result = await _signInManager.UserManager.CreateAsync(_mapper.Map<ApplicationUser>(model), model.Password);

            if (result.Succeeded) return Ok(result.Succeeded);
            else return BadRequest(result);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateActiveSchool([FromBody]UpdateActiveSchoolModel model)
        {
            var schoolId = model.SchoolId;

            var user = await GetCurrentUser();
            if (user == null)
            {
                return new ChallengeResult();
            }

            var applicationRoles = await _signInManager.UserManager.GetRolesAsync(user);

            // check current user is faculty and the schoolId is relate with him
            var facultyIdClaim = User.Claims.FirstOrDefault(c => c.Type == AppConstants.Security.ClaimTypes.FacultyId);
            if (facultyIdClaim == null)
            {
                return new ForbidResult();
            }

            var faculty = (await _facultyService.GetFacultyByUserIdAsync(user.Id));
            if (faculty == null)
            {
                return NotFound($"Faculty({user.Id})");
            }

            var school = (await _schoolService.GetSchoolOfFaculty(faculty.FacultyId, schoolId));
            if (school == null)
            {
                return NotFound($"School({schoolId})");
            }

            // update ActiveSchoolId to applicationUser
            user.ActiveSchoolId = schoolId;
            await _signInManager.UserManager.UpdateAsync(user);

            // remove and add ActiveSchoolId again
            var claims = User.Claims.ToList();
            claims.RemoveAll(x => x.Type == AppConstants.Security.ClaimTypes.ActiveSchoolId);
            claims.Add(new Claim(AppConstants.Security.ClaimTypes.ActiveSchoolId, schoolId.ToString()));
            string tokenString = TokenHelper.IssueToken(_authTokenSettings, user, applicationRoles, claims);

            if (string.IsNullOrEmpty(tokenString))
            {
                return BadRequest();
            }

            return Ok(tokenString);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterParent(
            [FromBody]ParentRegisterViewModel model,
            [FromServices]IStudentService studentService,
            [FromServices]IParentService parentService)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var districtId = await GetRequestDistrictId();

            var student = await studentService.GetStudentByStudentNumber(model.StudentNumber, model.StudentPhoneNumber);
            if (student == null)
            {
                return BadRequest(ErrorByCode.StudentNotFound);
            }

            if (student.User == null)
            {
                return StatusCode(500, ErrorByCode.StudentNotFound);
            }

            if (student.User.DistrictId != districtId)
            {
                return StatusCode(500, ErrorByCode.StudentNotFound);
            }

            var user = _mapper.Map<ApplicationUser>(model);
            user.DistrictId = districtId;
            var result = await _signInManager.UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return BadRequest(result);
            }

            var parentUser = await _signInManager.UserManager.FindByEmailAsync(model.Email);
            await _signInManager.UserManager.AddToRoleAsync(parentUser,  ApplicationRoleValue.Parent); ;

            if (!await SendRequestConfirmEmail(parentUser))
            {
                _logger.LogError($"SendRequestConfirmEmail FAIL {model.Email}");
            }

            var parent = await parentService.CreateParentAsync(new Parent
            {
                ParentId = parentUser.Id,
            });

            await parentService.AddStudentForParent(parent.ParentId, student.StudentId);

            if (!await _appMailService.NotifyStudentForNewParent(student.User, parentUser))
            {
                _logger.LogError("NotifyStudentForNewParent FAIL");
            }

            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RequestConfirmEmail([FromBody] string email)
        {
            var applicationUser = await _signInManager.UserManager.FindByEmailAsync(email);
            if (applicationUser == null)
            {
                return NotFound();
            }

            if (await SendRequestConfirmEmail(applicationUser))
            {
                return Ok();
            }

            return StatusCode(500);
        }

        private async Task<bool> SendRequestConfirmEmail(ApplicationUser applicationUser)
        {
            var token = await _signInManager.UserManager.GenerateEmailConfirmationTokenAsync(applicationUser);

            var confirmationUrl = Url.Action(nameof(ValidateConfirmEmail), null, new { user = applicationUser.Email, code = token }, Request.Scheme);
            return await _appMailService.ConfirmEmail(applicationUser, confirmationUrl);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ValidateConfirmEmail([FromQuery]string user, [FromQuery]string code)
        {
            var applicationUser = await _signInManager.UserManager.FindByEmailAsync(user);
            var result = await _signInManager.UserManager.ConfirmEmailAsync(applicationUser, code);
            return result.Succeeded ? Redirect($"{HttpContext.Request.GetHostUrl()}/login") : NotFound() as IActionResult;
        }
    }
}
