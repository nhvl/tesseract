﻿using AutoMapper;
using CsvHelper;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.AspNetCore;
using Google.Apis.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Net.Mime;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Config;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;
using Tesseract.Web.Models;

namespace Tesseract.Web.Controllers.Api
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]/[action]")]
    public class TestController : Controller
    {
        [HttpGet]
        [GoogleScopedAuthorize("https://www.googleapis.com/auth/analytics.readonly")]
        public async Task<IActionResult> GoogleAnalyticsReport([FromServices] IGoogleAuthProvider auth, [FromServices] AnalyticsReportingSettings clientInfo, long userId)
        {
            var cred = await auth.GetCredentialAsync();
            var service = new AnalyticsReportingService(new BaseClientService.Initializer
            {
                HttpClientInitializer = cred
            })
            { };

            // Create the ReportRequest object.
            var request = new SearchUserActivityRequest
            {
                DateRange = new DateRange() { StartDate = "2019-04-01", EndDate = "2019-06-04" },
                ViewId = "194938252",
                User = new User
                {
                    Type = "USER_ID",
                    UserId = userId.ToString()
                },
                PageSize = 1000
            };

            try
            {
                // TODO: page token
                var response = await service.UserActivity.Search(request).ExecuteAsync();

                // TODO: Can't Get User Activity with User ID bug? https://issuetracker.google.com/issues/130161158

                return Json(response);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
