﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EasyCaching.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Cryptography.Md5HashExtension;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;
using Tesseract.Web.Models;

namespace Tesseract.Web.Controllers.Api
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = AppConstants.Security.Policy.CrossedPortalAccess)]
    [ApiController]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]")]
    public class DistrictController : ABasicApiController
    {
        private readonly IDistrictService _districtService;

        private readonly IMapper _mapper;
        private readonly IEasyCachingProvider _cache;

        public DistrictController(
            IDistrictService districtService,
            IMapper mapper,
            IEasyCachingProviderFactory cacheFactory)
        {
            _districtService = districtService;
            _mapper = mapper;
            _cache = cacheFactory.GetCachingProvider(AppConstants.Cache.CacheDefault);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetDistrict([ModelBinder] string domain)
        {
            if (string.IsNullOrEmpty(domain)) return BadRequest();

            var district = await GetDistrictByDomain(domain);
            if (district == null) return NotFound($"District {domain} not found.");

            return Ok(_mapper.Map<DistrictViewModel>(district));
        }

        internal async Task<District> GetDistrictByDomain(string domain)
        {
            if (string.IsNullOrEmpty(domain)) return null;

            var cacheResult = await _cache.GetAsync(
                MakeCacheKey(domain),
                () => _districtService.GetByDomain(domain),
                TimeSpan.FromMinutes(10)
            );

            return cacheResult.Value;
        }

        const int maxItems = 5;

        [HttpGet("{role}/[action]")]
        public async Task<IActionResult> PowerSearch([FromRoute]string role, [FromQuery]string q, [FromQuery]long? schoolId)
        {
            var applicationUser = await GetCurrentUser();
            var roles = await GetCurrentRoles();
            if (!roles.Contains(role)) return BadRequest();

            var searchResult = await _districtService.PowerSearch(applicationUser.Id, role, q, maxItems, schoolId);

            return Ok(new PowerSearchViewModel()
            {
                Students = searchResult.Students.Select(x => new SearchView { Id = x.Item1.StudentId, Name = x.Item2.FullName }).ToList(),
                Classes = searchResult.Classes.Select(x => new SearchView { Id = x.ClassId, Name = x.ClassName }).ToList(),
                Pages = searchResult.Activities.Where(x => x.Type == ActivityType.Activity).Select(x => new SearchView { Id = x.ActivityId, Name = x.Title }).ToList(),
                Assessments = searchResult.Activities.Where(x => x.Type == ActivityType.Assessment).Select(x => new SearchView { Id = x.ActivityId, Name = x.Title }).ToList(),
                Assignments = searchResult.Activities.Where(x => x.Type == ActivityType.Assignment).Select(x => new SearchView { Id = x.ActivityId, Name = x.Title }).ToList(),
                Faculties = searchResult.Faculties.Select(x => new SearchView { Id = x.Item1.FacultyId, Name = x.Item2.FullName }).ToList(),
                Parents = searchResult.Parents.Select(x => new SearchView { Id = x.Item1.ParentId, Name = x.Item2.FullName }).ToList(),
                Schools = searchResult.Schools.Select(x => new SearchView { Id = x.SchoolId, Name = x.SchoolName }).ToList(),
            });
        }

        internal static string MakeCacheKey(string districtDomain) => (nameof(District) + districtDomain).ToMD5Hash();
    }
}
