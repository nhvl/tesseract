﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CsvHelper;
using Hangfire;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Enums;
using Tesseract.Core.Exceptions;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;
using Tesseract.Web.Extensions;
using Tesseract.Web.Models;
using Tesseract.Web.Service.Notification;

namespace Tesseract.Web.Controllers.Api
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = ApplicationRoleValue.Faculty, Policy = AppConstants.Security.Policy.SchoolAccess)]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]/{facultyId}/school/{schoolId}")]
    public partial class FacultyController : ABasicApiController
    {
        private readonly MainDbContext _mainDbContext;
        private readonly IUserService _userService;
        private readonly IStudentService _studentService;
        private readonly ISchoolService _schoolService;
        private readonly IClassService _classService;
        private readonly IActivityCategoryService _activityCategoryService;
        private readonly IActivityService _activityService;
        private readonly IDiscussionService _discussionService;
        private readonly IActDocService _actDocService;
        private readonly IActivityScoreService _activityScoreService;
        private readonly ICommentService _commentService;
        private readonly IAppMailService _appMailService;
        private readonly IKeyValueService _keyValueService;
        private readonly INotificationDispatcherService _notificationDispatcherService;
        private readonly IMapper _mapper;

        public FacultyController(
            MainDbContext mainDbContext,
            IStudentService studentService,
            ISchoolService schoolService,
            IClassService classService,
            IActivityService activityService,
            IAppMailService appMailService,
            IUserService userService,
            IActDocService actDocService,
            IActivityScoreService activityScoreService,
            ICommentService commentService,
            IDiscussionService discussionService,
            IActivityCategoryService categoryService,
            IKeyValueService keyValueService,
            INotificationDispatcherService notificationDispatcherService,
            IMapper mapper
        )
        {
            _mainDbContext = mainDbContext;
            _schoolService = schoolService;
            _studentService = studentService;
            _classService = classService;
            _activityService = activityService;
            _appMailService = appMailService;
            _userService = userService;
            _actDocService = actDocService;
            _activityScoreService = activityScoreService;
            _discussionService = discussionService;
            _commentService = commentService;
            _activityCategoryService = categoryService;
            _keyValueService = keyValueService;
            _notificationDispatcherService = notificationDispatcherService;
            _mapper = mapper;
        }

        #region Student
        [HttpPost("class/{classId}/student/batch")]
        public async Task<IActionResult> BatchUpdateStudent([FromRoute]long classId,
                                                            [FromRoute]long schoolId,
                                                            [FromForm]IList<IFormFile> files,
                                                            [FromServices]UserManager<ApplicationUser> userManager)
        {
            if (!TryGetUploadingFile(files, out IFormFile file)) return BadRequest("File not included or null.");

            if(Path.GetExtension(file.FileName).ToLower() != ".csv") return BadRequest("The file must be a CSV.");

            var _class = await _classService.GetClassAsync(classId);
            if (_class == null) return NotFound($"Class({classId})");

            var students = await _studentService.GetStudentsInClass(classId).Select(r => r.Student.StudentId).ToListAsync();

            var districtId = await GetRequestDistrictId();
            var records = new List<BatchUpdateStudentItemRequest>();

            try
            {
                records = CsvReadHelper.ReaderFromFile<BatchUpdateStudentItemRequest>(file).ToList();
                if(records.Count < 1)
                    return BadRequest("The file have no data.");
            }
            catch(HeaderValidationException ex)
            {
                var headerNames = String.Join(',', ex.HeaderNames);
                return BadRequest($"'{headerNames}' header {(ex.HeaderNames.Length > 1 ? "are" : "is")} missing.");
            }
            catch(CsvHelper.TypeConversion.TypeConverterException ex){
                return BadRequest($"Value '{ex.Text}' is not valid in {ex.MemberMapData.Member.Name}.");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            foreach (var record in records)
            {
                ModelState.Clear();
                TryValidateModel(record);
                short _grade = 0;
                if(!String.IsNullOrEmpty(record.Grade) && !short.TryParse(record.Grade, out _grade))
                    record.Errors.Add($"{nameof(record.Grade)} is not a number.");

                if(!ModelState.IsValid){
                    record.Errors.AddRange(ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage));
                    continue;
                }

                var user = await userManager.FindByEmailAsync(record.Email);
                if (user == null)
                {
                    user = record.ToUser(districtId);
                    var createUserResult = await _userService.CreateUser(user);
                    if (!createUserResult.Succeeded){
                        record.Errors.AddRange(createUserResult.Errors.Select(e => e.Description));
                        continue;
                    }

                    await userManager.AddToRoleAsync(user, ApplicationRoleValue.Student);
                    await _appMailService.NewUserSetPassword(user, Request.GetHostUrl());

                    record.Results.Add("New user was added");
                    record.Status = "Imported";
                }
                else{
                    var originalEmail = user.Email;
                    record.ToUpdateUser(user);
                    var updateUser = await _userService.UpdateUser(user, originalEmail);
                    if(updateUser.Succeeded){
                        record.Results.Add("User is updated");
                        record.Status = "Updated";
                    }
                    else{
                        record.Errors.AddRange(updateUser.Errors.Select(e => e.Description));
                    }
                }

                var student = await _studentService.GetStudentByUserId(user.Id);
                if (student == null) {
                    student = record.ToStudent(user.Id);
                    try
                    {
                        student = await _studentService.CreateStudentAsync(student, schoolId);
                        record.Results.Add("New student was added.");
                    }
                    catch(ConflictResourceException ex)
                    {
                        record.Errors.Add(ex.Message);
                        continue;
                    }
                }

                // check student exists in class or not
                if (students.Contains(student.StudentId)) {
                    record.Results.Add("Student was already exists in class.");
                } else {
                    await _classService.RegisterStudentAsync(classId, student.StudentId);
                    record.Results.Add("Student was imported to class.");
                }
            }

            return Ok(records);
        }

        [HttpPost("class/{classId}/student/importToClass")]
        public async Task<IActionResult> ImportStudentsToClass([FromRoute]long classId,
        [FromRoute]long schoolId, [FromBody]List<long> studentIds)
        {
            var _class = await _classService.GetClassAsync(classId);
            if(_class == null) return BadRequest($"Class Id '{classId}' is not exists.");

            var students = new List<Student>();
            //TODO: Get Student from class.
            if (students != null && students.Count > 0)
                studentIds = studentIds.Where(s => students.Any(cs => cs.StudentId != s)).ToList();

            foreach (var studentId in studentIds)
            {
                await _classService.RegisterStudentAsync(classId, studentId);
            }

            return Ok("Imported Successfully");
        }

        [HttpPost("class/{classId}/student/importAllToClass")]
        public async Task<IActionResult> ImportAllStudentsToClass([FromRoute]long classId
            , [FromRoute]long schoolId
            , [FromQuery]long filteredClassId
            , [FromQuery]string searchText)
        {
            var total = await _studentService.GetStudentsOfSchool(schoolId, excludedClassId: classId, filteredClassId: filteredClassId, searchText: searchText);
            return await ImportStudentsToClass(classId, schoolId, total.Students.Select(s => s.StudentId).ToList());
        }

        [HttpGet("student")]
        public async Task<IActionResult> GetStudents([FromRoute]long facultyId)
        {
            var dto = await _studentService.GetAllStudentsOfTeacherAsync(facultyId, null);
            return Ok(dto.ToObject());
        }

        [HttpGet("schoolStudent")]
        public async Task<IActionResult> GetAllStudentsInSchool([FromRoute]long schoolId)
        {
            var dto = await _studentService.GetStudentsOfSchool(schoolId);
            return Ok(dto.ToObject());
        }

        [HttpGet("page/{pageIndex}/size/{pageSize}/schoolStudent")]
        public async Task<IActionResult> GetAllStudentsInSchoolPagination([FromRoute]long schoolId
            , [FromRoute]int pageIndex
            , [FromRoute]int pageSize
            , [FromQuery]long excludedClassId
            , [FromQuery]long filteredClassId
            , [FromQuery]string searchText
            , [FromQuery]string sortColumnKey
            , [FromQuery]string sortOrder)
        {
            var dto = await _studentService.GetStudentsOfSchool(schoolId, pageIndex, pageSize, excludedClassId, filteredClassId, searchText, sortColumnKey, sortOrder);
            return Ok(dto.ToObject());
        }

        [HttpGet("totalSchoolStudent")]
        public async Task<IActionResult> GetAllStudentsInSchoolTotalPagination([FromRoute]long schoolId
            , [FromQuery]long excludedClassId
            , [FromQuery]long filteredClassId
            , [FromQuery]string searchText)
        {
            var total = await _studentService.GetStudentsOfSchool(schoolId, excludedClassId: excludedClassId, filteredClassId: filteredClassId, searchText: searchText);
            return Ok(total.Students.Length);
        }

        [HttpGet("class/{classId}/student")]
        public async Task<IActionResult> GetStudentsOfClass([FromRoute] long classId)
        {
            var studentsView = (await _studentService.GetStudentsInClass(classId, includeUserInfo: true)
                .ToListAsync())
                .Select(r => new StudentView
                {
                    StudentId = r.Student.StudentId,
                    ExternalId = r.Student.ExternalId,
                    StudentNumber = r.Student.StudentNumber,
                    Grade = r.Student.Grade,
                    UserId = r.Student.StudentId,
                    FirstName = r.User.FirstName,
                    LastName = r.User.LastName,
                    Salutation = r.User.Salutation,
                    Nickname = r.User.Nickname,
                    LastAccessed = r.ClassStudent.LastAccessed,
                    CumulativeGrade = r.ClassStudent.CumulativeGrade,
                });
            return Ok(studentsView);
        }

        [HttpPost("class/{classId}/student")]
        //[PermissionClaim(Permission.Cons.CanAddStudent)] huyn 2019-08-23 enable this permission request after finish the custom role assigning feature in Admin Portal
        public async Task<IActionResult> CreateClassStudent(
            [FromRoute] long schoolId,
            [FromRoute] long classId,
            [FromBody] CreateStudentRequest studentReq,
            [FromServices]UserManager<ApplicationUser> userManager)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var districtId = await GetRequestDistrictId();
            if (districtId < 1) return BadRequest("districtId");
            var user = studentReq.ToUser(districtId);

            School school = null;
            if (0 < studentReq.SchoolId) {
                school = await _schoolService.GetSchoolById(studentReq.SchoolId);
                if (school == null) return BadRequest($"School({studentReq.SchoolId}) not found");
            }

            var createUserResult = await _userService.CreateUser(user);
            if (!createUserResult.Succeeded) return BadRequest(createUserResult);
            else {
                await userManager.AddToRoleAsync(user, ApplicationRoleValue.Student);
                await _appMailService.NewUserSetPassword(user, Request.GetHostUrl());
            }


            var student = studentReq.ToStudent(user.Id);
            await _studentService.CreateStudentAsync(student, schoolId);

            if (school != null) {
                await _schoolService.RegisterStudentAsync(new SchoolStudent {
                    SchoolId = school.SchoolId,
                    StudentId = student.StudentId,
                    GradingPeriod= school.CurrentGradingTerm,
                    School= school,
                    Student = user
                });
            }

            await _classService.RegisterStudentAsync(classId, student.StudentId);

            return Ok(StudentResponse.Create(student, user));
        }

        [HttpPut("class/{classId}/student/{studentId}")]
        public async Task<IActionResult> UpdateClassStudent(
            [FromRoute] long schoolId,
            [FromRoute] long classId,
            [FromRoute] long studentId,
            [FromBody] UpdateStudentRequest studentReq)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var student = await _studentService.GetStudentById(studentId);
            if (student == null) return NotFound();
            var user = student.User;
            if (user == null) return StatusCode(500, $"User for Student({studentId}) NOT FOUND");

            var originalEmail = user.Email;
            studentReq.WriteTo(user);
            var updateUserResult = await _userService.UpdateUser(user, originalEmail);
            if (!updateUserResult.Succeeded) return BadRequest(updateUserResult);

            studentReq.WriteTo(student);
            await _studentService.UpdateStudentAsync(student);

            return Ok(StudentResponse.Create(student, user));
        }

        [HttpGet("student/{studentId}")]
        public async Task<IActionResult> GetStudent([FromRoute] long studentId)
        {
            var student = await _studentService.GetStudentById(studentId);
            if (student == null) return NotFound(studentId);

            return Ok(new GeneralDto {
                Users = new[] {student.User},
                Students = new[]{student},
            }.ToObject());
        }

        [HttpGet("student/{studentId}/teacher-view")]
        public async Task<IActionResult> GetDetailForFaculty([FromRoute]long facultyId, [FromRoute] long studentId, [FromQuery] long? classId)
        {
            var data = await _studentService.GetStudentDetailForTeacherAsync(facultyId, studentId, classId);
            if (data == null) return NotFound(studentId);

            return Ok(data.ToObject());
        }
        #endregion

        //TES-216
        #region Category
        [HttpGet("Category")]
        public async Task<IActionResult> GetCategories([FromRoute]long facultyId)
        {
            var categories = await _activityCategoryService.GetCategories(facultyId);

            return Ok(categories);
        }

        [HttpPost("Category")]
        public async Task<IActionResult> CreateCategory([FromRoute]long facultyId, [FromBody] ActivityCategoryViewModel category)
        {
            var newCategory = await _activityCategoryService.SetCategory(new ActivityCategory
            {
                Name = category.Name,
                Color = category.Color,
                IsGraded = category.IsGraded,
                MaxScore = category.MaxScore,
                Weight = category.Weight,
                FacultyId = facultyId
            });
            return Ok(newCategory);
        }

        [HttpPut("Category")]
        public async Task<IActionResult> UpdateCategory([FromRoute]long facultyId, [FromBody] ActivityCategoryViewModel categoryModel)
        {
            var category = await _activityCategoryService.GetCategory(categoryModel.ActivityCategoryId);
            List<long> recalculateClassIds = new List<long>();
            if (categoryModel.ShouldRecalculateGrade(category))
            {
                recalculateClassIds = new List<long>((await _activityService.GetActivitiesByCategory(category.ActivityCategoryId)).Select(x => x.ClassId).Distinct());
            }

            var updatedCategory = await _activityCategoryService.SetCategory(new ActivityCategory
            {
                ActivityCategoryId = categoryModel.ActivityCategoryId,
                Name = categoryModel.Name,
                Color = categoryModel.Color,
                IsGraded = categoryModel.IsGraded,
                MaxScore = categoryModel.MaxScore,
                Weight = categoryModel.Weight,
                FacultyId = facultyId
            });

            foreach(var classId in recalculateClassIds)
            {
                await _activityService.ReCalculateCumulativeGrade(classId);
            }

            return Ok(updatedCategory);
        }

        [HttpDelete("Category/{categoryId}")]
        public async Task<IActionResult> DeleteCategory([FromRoute]long facultyId, [FromRoute]long categoryId)
        {
            var category = await _activityCategoryService.GetCategory(categoryId);
            if (category == null) return NotFound($"Activity Category ({categoryId})");

            await _activityCategoryService.DeleteCategory(category);

            return Ok(null);
        }
        #endregion

        #region Class template
        [HttpPost("class/{classId}/template")]
        public IActionResult CreateTemplate([FromRoute]long facultyId, [FromRoute]long classId, [FromServices]IBackgroundJobClient backgroundJobs)
        {
            backgroundJobs.Enqueue<IClassTemplateService>((templateService) => templateService.CreateTemplate(classId, facultyId));
            return Ok();
        }

        [HttpGet("class_templates")]
        public async Task<IActionResult> GetTemplates([FromRoute]long facultyId, [FromServices]IClassTemplateService templateService)
        {
            var templates = await templateService.GetTemplates(facultyId);
            return Ok(templates);
        }

        [HttpDelete("template/{classTemplateId}")]
        public async Task<IActionResult> DeleteTemplate([FromRoute]long classTemplateId, [FromServices]IClassTemplateService templateService)
        {
            await templateService.DeleteTemplate(classTemplateId);
            return Ok();
        }

        [HttpPost("class/create_from_template")]
        public async Task<IActionResult> ApplyTemplate([FromBody]ApplyTemplateModel templateModel, [FromServices]IBackgroundJobClient backgroundJobs)
        {
            var userId = (await this.GetCurrentUser()).Id;
            var jobId = backgroundJobs.Enqueue<IClassTemplateService>((templateService) => templateService.ApplyTemplate(templateModel.TemplateId, new ApplyTemplateConfig(){
                GradingTermId = templateModel.GradingTermId,
                ClassName = templateModel.ClassName,
            }));

            backgroundJobs.ContinueJobWith<INotificationHubHelper>(jobId, (hubContext) => hubContext.SendData(userId, "DiscussionChanged"));

            return Ok();
        }
        #endregion

        [HttpGet("activity/{activityId}/student/{studentId}/submission")]
        public async Task<IActionResult> GetSubmission([FromRoute]long activityId, [FromRoute]long studentId, [FromServices] ISubmissionService submissionService)
        {
            var submission = await submissionService.Get(activityId, studentId);
            return Ok(submission);
        }

        #region ActDoc
        [HttpGet("activity/{activityId}/doc", Name = "GetActDoc")]
        public async Task<IActionResult> GetActDoc([FromRoute] long activityId)
        {
            var doc = await _actDocService.Get(activityId);
            if (doc == null) return NotFound();

            var actDoc = _mapper.Map<ActDocView>(doc);
            for (var i = 0; i < actDoc.Items.Length; i++)
            {
                if (actDoc.Items[i].Type == EActItemType.PollQuiz)
                {
                    for (var j = 0; j < actDoc.Items[i].PollItems.Length; j++)
                    {
                        actDoc.Items[i].PollItems[j].Votes = await _activityService.GetPollingCount(activityId, actDoc.Items[i].Id, (byte)j);
                    }
                }
            }

            return Ok(actDoc);
        }

        [HttpPost("activity/{activityId}/doc")]
        public async Task<IActionResult> CreateActDoc([FromRoute] long activityId, [FromBody] ActDocRequest docModel)
        {
            var user = await GetCurrentUser();
            if (user == null) return Unauthorized();

            var a = await _activityService.GetActivity(activityId);
            if (a == null) return NotFound($"Activity({activityId}) not found");

            {
                var eDoc = await _actDocService.Get(activityId);
                if (eDoc != null) return Conflict(new { code = "ExistActDoc", message = $"Document for Activity({activityId}) already exist." });
            }

            var doc = _mapper.Map<ActDoc>(docModel);
            doc.ActivityId = activityId;
            doc.CreatedBy = user.Id;
            doc.UpdatedBy = user.Id;
            doc.DateCreated = DateTime.UtcNow;
            doc.DateUpdated = DateTime.UtcNow;

            var document = await _actDocService.Create(doc);
            return CreatedAtRoute("GetActDoc", new { activityId }, _mapper.Map<ActDocView>(document));
        }

        [HttpPut("activity/{activityId}/doc")]
        public async Task<IActionResult> UpdateActDoc([FromBody] ActDocRequest doc)
        {
            var user = await GetCurrentUser();
            if (user == null) return Unauthorized();

            var eDoc = await _actDocService.Get(doc.ActivityId);
            if (eDoc == null) return NotFound();

            eDoc.Title = doc.Title.ToString();
            eDoc.Summary = doc.Summary.ToString();
            eDoc.Banner = doc.Banner;

            eDoc.Items = doc.Items.Select(item => _mapper.Map<BaseActItem>(item)).ToArray();

            eDoc.UpdatedBy = user.Id;
            eDoc.DateUpdated = DateTime.UtcNow;

            var nDoc = await _actDocService.Update(eDoc);
            return Ok(_mapper.Map<ActDocView>(nDoc));
        }
        #endregion

        #region StudentActDoc
        [HttpGet("student/{studentId}/activity/{activityId}/doc", Name = "PreviewStudentActDoc")]
        public async Task<IActionResult> PreviewStudentActDoc([FromRoute]long facultyId, [FromRoute] long studentId, [FromRoute] long activityId)
        {
            var doc = await _actDocService.GetStudentDoc(activityId, studentId);

            return doc != null ? Ok(_mapper.Map<StudentActDocReview>(doc)) : Ok(null) as IActionResult;
        }
        #endregion
    }

    /// <summary>
    /// Controller for api of Faculty Portal to connect resouces which isn't limitted on a specific school.
    /// </summary>
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = ApplicationRoleValue.Faculty, Policy = AppConstants.Security.Policy.GeneralAccess)]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/faculty/{facultyId}")]

    public class FreeSchoolFacultyController: ABasicApiController
    {
        [HttpGet("school")]
        public async Task<IActionResult> GetSchoolsOfFaculty([FromRoute] long facultyId, [FromServices] ISchoolService schoolService, [FromServices] IGradingTermService gradingTermService)
        {
            var xs = (await schoolService.GetSchoolsOfFaculty(facultyId));
            var ts = (await gradingTermService.GetGradingTerms(xs.Select(x => x.SchoolId).ToArray()));
            return Ok(xs.Select(x => SchoolResponse.Create(x, ts.Where(t => t.SchoolId == x.SchoolId).ToArray())));
        }
    }
}
