﻿namespace Tesseract.Web.Controllers.Api
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Tesseract.Core.Entities.Identity;
    using Tesseract.Core.Entities.Main;
    using Tesseract.Core.Exceptions;
    using Tesseract.Infrastructure.Services.Interface;
    using Tesseract.Web.Common;

    [Authorize]
    [ApiController]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/config")]
    public class KeyValueController : ABasicApiController
    {
        private readonly IKeyValueService _keyValueService;
        private readonly ISchoolService _schoolService;

        public KeyValueController(IKeyValueService keyValueService, ISchoolService schoolService, IGradingTermService gradingTermService)
        {
            _keyValueService = keyValueService;
            _schoolService = schoolService;
        }

        [HttpGet("district/{districtId}/ActivityCategoryColor")]
        public async Task<IActionResult> GetDistrictActivityCategoryColors([FromRoute] long districtId, [ModelBinder] string domain, [FromServices] IDistrictService districtService)
        {
            var isMultipleSchoolUser = User.IsInRole(ApplicationRoleValue.Admin) || User.IsInRole(ApplicationRoleValue.SchoolAdmin) || User.IsInRole(ApplicationRoleValue.Parent);
            if (!isMultipleSchoolUser)
            {
                return Forbid();
            }

            var district = await districtService.GetByDomain(domain) ?? throw new ResourceNotFoundException();
            if (districtId != district.DistrictId)
            {
                return Forbid();
            }

            var categoryColors = await _keyValueService.GetActivityCategoryColors(districtId, null);
            return Ok((categoryColors.Length < 1) ? null : categoryColors);
        }

        [HttpGet("school/{schoolId}/ActivityCategoryColor")]
        public async Task<IActionResult> GetSchoolActivityCategoryColors([FromRoute] long schoolId)
        {
            var s = await _schoolService.GetSchoolById(schoolId);
            if (s == null)
            {
                return NotFound($"School({schoolId}) not found");
            }

            var categoryColors = await _keyValueService.GetActivityCategoryColors(null, schoolId);
            return Ok((categoryColors.Length < 1) ? null : categoryColors);
        }

        [HttpGet("district/{districtId}/BadgeColor")]
        public async Task<IActionResult> GetDistrictBadgeColors([FromRoute] long districtId, [ModelBinder] string domain, [FromServices] IDistrictService districtService)
        {
            var isMultipleSchoolUser = User.IsInRole(ApplicationRoleValue.Admin) || User.IsInRole(ApplicationRoleValue.SchoolAdmin) || User.IsInRole(ApplicationRoleValue.Parent);
            if (!isMultipleSchoolUser)
            {
                return Forbid();
            }

            var district = await districtService.GetByDomain(domain) ?? throw new ResourceNotFoundException();
            if (districtId != district.DistrictId)
            {
                return Forbid();
            }

            return Ok(await _keyValueService.GetScoreBadge(districtId, null));
        }

        [HttpGet("school/{schoolId}/BadgeColor")]
        public async Task<IActionResult> GetSchoolBadgeColors([FromRoute] long schoolId)
        {
            var s = await _schoolService.GetSchoolById(schoolId);
            if (s == null)
            {
                return NotFound();
            }

            return Ok(await _keyValueService.GetScoreBadge(null, schoolId));
        }

        [HttpGet("GoogleTagManager")]
        public async Task<IActionResult> GetGoogleTagManagerId([ModelBinder] string domain, [FromServices] IDistrictService districtService)
        {
            var district = await districtService.GetByDomain(domain) ?? throw new ResourceNotFoundException();

            return Ok(await _keyValueService.GetGoogleTagManagerId(district.DistrictId, null));
        }

        [HttpGet("district/{districtId}/GoogleTagManager")]
        public async Task<IActionResult> GetDistrictGoogleTagManagerId([FromRoute] long districtId, [ModelBinder] string domain, [FromServices] IDistrictService districtService)
        {
            var isMultipleSchoolUser = User.IsInRole(ApplicationRoleValue.Admin) || User.IsInRole(ApplicationRoleValue.SchoolAdmin) || User.IsInRole(ApplicationRoleValue.Parent);
            if (!isMultipleSchoolUser)
            {
                return Forbid();
            }

            var district = await districtService.GetByDomain(domain) ?? throw new ResourceNotFoundException();
            if (districtId != district.DistrictId)
            {
                return Forbid();
            }

            return Ok(await _keyValueService.GetGoogleTagManagerId(districtId, null));
        }

        [HttpGet("school/{schoolId}/GoogleTagManager")]
        public async Task<IActionResult> GetSchoolGoogleTagManagerId([FromRoute] long schoolId)
        {
            var s = await _schoolService.GetSchoolById(schoolId);
            if (s == null)
            {
                return NotFound();
            }

            return Ok(await _keyValueService.GetGoogleTagManagerId(null, schoolId));
        }

        [HttpGet("GradeRange")]
        public async Task<IActionResult> GetGradeRange([ModelBinder] string domain, [FromServices] IDistrictService districtService)
        {
            var district = await districtService.GetByDomain(domain) ?? throw new ResourceNotFoundException();

            return Ok(await _keyValueService.GetDefaultGradeRange(district.DistrictId, null));
        }

        [HttpGet("district/{districtId}/GradeRange")]
        public async Task<IActionResult> GetDistrictGradeRange([FromRoute] long districtId, [ModelBinder] string domain, [FromServices] IDistrictService districtService)
        {
            var isMultipleSchoolUser = User.IsInRole(ApplicationRoleValue.Admin) || User.IsInRole(ApplicationRoleValue.SchoolAdmin) || User.IsInRole(ApplicationRoleValue.Parent);
            if (!isMultipleSchoolUser)
            {
                return Forbid();
            }

            var district = await districtService.GetByDomain(domain) ?? throw new ResourceNotFoundException();
            if (districtId != district.DistrictId) return Forbid();

            return Ok(await _keyValueService.GetDefaultGradeRange(districtId, null));
        }

        [HttpGet("school/{schoolId}/GradeRange")]
        public async Task<IActionResult> GetSchoolGradeRange([FromRoute] long schoolId)
        {
            var s = await _schoolService.GetSchoolById(schoolId);
            if (s == null)
            {
                return NotFound();
            }

            return Ok(await _keyValueService.GetDefaultGradeRange(null, schoolId));
        }

        [HttpPost("school/{schoolId}/GradeRange")]
        public async Task<IActionResult> UpdateSchoolGradeRange([FromRoute] long schoolId, [FromBody]GradeRange[] gradeRanges)
        {
            var s = await _schoolService.GetSchoolById(schoolId);
            if (s == null)
            {
                return NotFound();
            }

            return Ok(await _keyValueService.SetDefaultGradeRange(null, schoolId, gradeRanges));
        }
    }
}
