﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyCaching.Core;
using Microsoft.AspNetCore.Mvc;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;

namespace Tesseract.Web.Controllers.Api
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]")]
    public class LocalizationController : ABasicApiController
    {
        private readonly ILocalizationService _service;
        private readonly IEasyCachingProvider _cache;

        public LocalizationController(ILocalizationService service, IEasyCachingProviderFactory cacheFactory)
        {
            _cache = cacheFactory.GetCachingProvider(AppConstants.Cache.CacheDefault);
            _service = service;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = AppConstants.Cache.Profile.StaticApi)]
        public async Task<IActionResult> GetAllLocales()
        {
            const string cacheKey = nameof(_service.GetAllLocales);
            var cacheResult = await _cache.GetAsync(cacheKey, async () => await _service.GetAllLocales(), TimeSpan.FromMinutes(10));

            return Ok(cacheResult.Value);
        }

        [HttpGet("{language}")]
        [ResponseCache(CacheProfileName = AppConstants.Cache.Profile.StaticApi)]
        public async Task<IActionResult> GetLocale([FromRoute] string language)
        {
            var cacheKey = nameof(_service.GetLocale) + language;
            var cacheResult = await _cache.GetAsync(cacheKey, async () =>
            {
                var records = await _service.GetLocale(language);
                return records.ToDictionary(x => x.Key, y => y.Value);
            }, TimeSpan.FromMinutes(10));

            return Ok(cacheResult.Value);
        }

        [HttpPost("{language}")]
        public async Task<IActionResult> UpdateLocale([FromRoute] string language, [FromBody] Dictionary<string, string> dict)
        {
            var result = await _service.UpdateLocale(language, dict.Select(record => new Localization
            {
                Locale = language,
                Key = record.Key,
                Value = record.Value,
            }));

            var cacheKey = nameof(_service.GetLocale) + language;
            await _cache.RemoveAsync(cacheKey);
            return Ok(result);
        }
    }
}
