﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;

namespace Tesseract.Web.Controllers.Api
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "SA, Admin, Faculty", Policy = AppConstants.Security.Policy.CrossedPortalAccess)]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]")]
    public class ClassController : ABasicApiController
    {
        private readonly IClassService _classService;
        private readonly IAuthorizationService _authorizationService;


        public ClassController(
            IClassService classService,
            IAuthorizationService authorizationService
        )
        {
            _classService = classService;
            _authorizationService = authorizationService;
        }

        [HttpDelete("{classId}")]
        public async Task<IActionResult> Delete([FromRoute] long classId)
        {
            var classItem = await _classService.GetClassAsync(classId);
            if (classItem == null) return NotFound($"Class({classId}");

            await _classService.DeleteClassAsync(classItem);
            return Ok(classItem);
        }


        [HttpPost("{classId}/student")]
        public async Task<IActionResult> AddStudentToClass([FromRoute] long classId, [FromBody] long studentId)
        {
            var classItem = await _classService.GetClassAsync(classId);
            if (classItem == null) return NotFound($"Class({classId}");

            await _classService.RegisterStudentAsync(classId, studentId);
            return Ok();
        }


        [HttpPost("{classId}/faculty")]
        public async Task<IActionResult> AddFacultyToClass([FromRoute] long classId, [FromBody] long facultyId)
        {
            var classItem = await _classService.GetClassAsync(classId);
            if (classItem == null) return NotFound($"Class({classId}");

            await _classService.RegisterFacultyAsync(classId, facultyId);
            return Ok();
        }
    }
}
