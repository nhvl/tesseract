﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Web.Common;
using Tesseract.Web.Service;

namespace Tesseract.Web.Controllers.Api
{
    // TODO: rename ImageController-> MediaController
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]/[action]")]
    public class ImageController : ABasicApiController
    {
        private readonly IMediaStorageService _mediaStorageService;

        public ImageController(IMediaStorageService mediaStorageService)
        {
            _mediaStorageService = mediaStorageService;
        }

        [HttpPost, DisableRequestSizeLimit]
        public async Task<IActionResult> Upload([FromForm]IList<IFormFile> files)
        {
            if (!TryGetUploadingFile(files, out IFormFile file))
            {
                return BadRequest("File Not Included");
            }

            var path = await _mediaStorageService.StoreMediaFile(file);
            return Ok(path);
        }

        [HttpPost, DisableRequestSizeLimit]
        public async Task<IActionResult> UploadFile([FromForm]IList<IFormFile> files)
        {
            if (!TryGetUploadingFile(files, out IFormFile file))
            {
                return BadRequest("File Not Included");
            }

            var path = await _mediaStorageService.StoreGeneralFile(file);
            return Ok(path);
        }

        [HttpPost, DisableRequestSizeLimit]
        public async Task<IActionResult> UploadMedia([FromForm]IList<IFormFile> files)
        {
            return await UploadFile(files);
        }

        [HttpGet]
        public async Task<IActionResult> GetMediaInfo(string url)
        {
            var mediaInfo = await MediaUrlHepler.GetMediaInfo(url);
            return mediaInfo != null ? Ok(new MediaInfo()) : BadRequest() as IActionResult;
        }
    }
}
