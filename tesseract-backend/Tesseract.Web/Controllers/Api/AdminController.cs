﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Authentication;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using EasyCaching.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;
using Tesseract.Web.Exceptions;
using Tesseract.Web.Extensions;
using Tesseract.Web.Models;
using Tesseract.Web.Service;

namespace Tesseract.Web.Controllers.Api
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = ApplicationRoleValue.Admin + "," + ApplicationRoleValue.SchoolAdmin, Policy = AppConstants.Security.Policy.GeneralAccess)]
    [ApiController]
    [ApiVersion("1.0")]
    [Route(AppConstants.AppName + "/v{version:apiVersion}/[controller]")]
    public partial class AdminController : ABasicApiController
    {
        private readonly MainDbContext _mainDbContext;
        private readonly IDistrictService _districtService;
        private readonly ISchoolService _schoolService;
        private readonly IGradingTermService _gradingTermService;
        private readonly IUserService _userService;
        private readonly IFacultyService _facultyService;
        private readonly IStudentService _studentService;
        private readonly IActivityService _activityService;
        private readonly IConfiguration _config;
        private readonly IAppMailService _appMailService;
        private readonly ISecurityService _passwordService;
        private readonly IAuthService _authService;
        private readonly TesseractUserManager _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMapper _mapper;

        public AdminController(
            MainDbContext mainDbContext,
            IDistrictService districtService,
            ISchoolService schoolService,
            IGradingTermService gradingTermService,
            IUserService userService,
            IFacultyService facultyService,
            IStudentService studentService,
            IActivityService activityService,
            IAppMailService appMailService,
            ISecurityService passwordService,
            IAuthService authService,
            IConfiguration configuration,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<ApplicationRole> roleManager,
            IMapper mapper
        )
        {
            _mainDbContext      = mainDbContext;
            _districtService    = districtService;
            _schoolService      = schoolService;
            _gradingTermService = gradingTermService;
            _userService        = userService;
            _facultyService     = facultyService;
            _studentService     = studentService;
            _activityService    = activityService;
            _config             = configuration;
            _appMailService     = appMailService;
            _passwordService    = passwordService;
            _authService        = authService;
            _userManager        = signInManager.UserManager as TesseractUserManager ?? throw new InvalidOperationException();
            _roleManager        = roleManager;
            _mapper             = mapper;
        }

        #region District
        [HttpPut("district/{districtId}/logo")]
        public async Task<IActionResult> UpdateLogo(
            [FromRoute] long districtId,
            [ModelBinder] string domain,
            [FromForm] IList<IFormFile> files,
            [FromServices] IEasyCachingProvider _cache,
            [FromServices] IMediaStorageService mediaStorageService)
        {
            var file = files.FirstOrDefault();
            if (file == null || file.Length == 0) return BadRequest(new { code = "FileNotFound" });

            var roles = await GetCurrentRoles();
            if (!roles.Contains(ApplicationRoleValue.Admin)) return Unauthorized();

            var district = await _districtService.GetByDomain(domain);
            if (district == null) return NotFound();
            if (district.DistrictId != districtId) return Forbid();

            Uri path;
            try
            {
                path = await mediaStorageService.StoreMediaFile(file, new Size(300, 300));
            }
            catch (UnsupportedMediaTypeException)
            {
                return StatusCode(415, new { code = "InvalidImage" });
            }

            district.LogoUrl = path.AbsoluteUri;
            district = await _districtService.Update(district);
            _ = _cache.RemoveAsync(DistrictController.MakeCacheKey(domain));

            return Ok(district);
        }
        #endregion

        #region School
        [HttpGet("district/{districtId}/school")]
        public async Task<IActionResult> GetSchoolsOfDistrict([FromRoute] long districtId)
        {
            var isDistrictAdmin = await IsDistrictAdmin();
            var user = await GetCurrentUser();
            var schools = isDistrictAdmin
                ? await _schoolService.GetSchoolsInDistrict(districtId).ToListAsync()
                : await _schoolService.GetSchoolsOfSchoolAdmin(user.Id, districtId).ToListAsync();

            var ts = await _gradingTermService.GetGradingTerms(schools.Select(s => s.SchoolId).ToArray());

            return Ok(schools.Select(s => SchoolResponse.Create(s, ts.Where(t => t.SchoolId == s.SchoolId).ToArray())));
        }

        [HttpGet("district/{districtId}/school/{schoolId}")]
        public async Task<IActionResult> GetSchoolOfDistrict([FromRoute] long districtId, [FromRoute] long schoolId)
        {
            var isDistrictAdmin = await IsDistrictAdmin();
            var user = await GetCurrentUser();
            var s = isDistrictAdmin
                ? (await _schoolService.GetSchoolInDistrict(districtId, schoolId))
                : (await _schoolService.GetSchoolOfSchoolAdmin(user.Id, schoolId))
                ;

            var ts = (await _gradingTermService.GetGradingTerms(schoolId)).ToArray();

            return Ok(SchoolResponse.Create(s, ts));
        }

        [HttpPost("school")]
        public async Task<IActionResult> CreateSchool([FromBody] CreateSchoolViewModel school)
        {
            var districtId = await GetRequestDistrictId();
            var newSchool = await _schoolService.CreateSchool(new School
            {
                DistrictId = districtId,
                CurrentGradingTerm = -1,
                SchoolName = school.SchoolName,
                LogoUrl = school.LogoUrl,
                IconUrl = school.IconUrl
            }, new GradingTerm
            {
                SchoolId = -1,
                Name = school.GradingTermName,
                StartDate = school.StartDate,
                EndDate = school.EndDate
            });

            if (newSchool == null) return BadRequest($"Cannot create School");

            return Ok(newSchool);
        }

        [HttpPut("school/{schoolId}")]
        public async Task<IActionResult> UpdateSchool([FromRoute] long schoolId, [FromBody] SchoolViewModel school)
        {
            var updatedSchool = await _schoolService.UpdateSchoolAsync(new School
            {
                SchoolId           = school.SchoolId,
                DistrictId         = school.DistrictId,
                CurrentGradingTerm = school.CurrentGradingTerm,
                SchoolName         = school.SchoolName,
                LogoUrl            = school.LogoUrl,
                IconUrl            = school.IconUrl
            });
            return Ok(updatedSchool);
        }

        // [HttpDelete("school/{schoolId}")]
        private async Task<IActionResult> DeleteSchool([FromRoute] long schoolId)
        {
            var school = await _schoolService.GetSchoolById(schoolId);
            await _schoolService.DeleteSchool(school);
            return Ok(school);
        }
        #endregion

        #region Report
        [HttpGet("report")]
        public async Task<IActionResult> QuickReport([ModelBinder] string domain)
        {
            var district = await _districtService.GetByDomain(domain);
            if (district == null) return NotFound();
            var user = await GetCurrentUser();

            var report = await IsDistrictAdmin()
                ? await _districtService.GetQuickReport(district.DistrictId)
                : await _districtService.GetQuickReportForSchoolAdmin(district.DistrictId, user.Id);

            return Ok(report);
        }

        [HttpGet("report/school/{schoolId}/term/{gradingTermId}")]
        public async Task<IActionResult> GetSchoolReport([FromRoute]int schoolId, [FromRoute]int gradingTermId)
        {
            var report = await _districtService.GetSchoolReport(schoolId, gradingTermId);
            return Ok(report);
        }
        #endregion

        #region Student
        [HttpGet("district/{districtId}/student")]
        public async Task<IActionResult> GetStudent([FromRoute] long districtId)
        {
            var rs = await _studentService.GetStudentsOfDistrict(districtId).ToListAsync();
            var dto = GeneralRecord.ToGeneralDto(rs);
            return Ok(dto.ToObject());
        }

        [HttpGet("student/{studentId}")]
        public async Task<IActionResult> GetStudent([FromRoute] long studentId, [FromServices] IStudentService studentService)
        {
            var student = await studentService.GetStudentById(studentId);
            if (student == null) return NotFound(studentId);

            return Ok(new GeneralDto {
                Users = new[] {student.User},
                Students = new[]{student},
            }.ToObject());
        }

        [HttpPost("student")]
        public async Task<IActionResult> CreateStudent(
            [FromRoute] long studentId,
            [FromBody] CreateStudentRequest vm,
            [FromServices] IStudentService studentService,
            [FromServices] ISchoolService schoolService
            )
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var districtId = await GetRequestDistrictId();
            if (districtId < 1) return BadRequest("districtId");
            var user = vm.ToUser(districtId);

            School school = null;
            if (0 < vm.SchoolId) {
                school = await schoolService.GetSchoolById(vm.SchoolId);
                if (school == null) return BadRequest($"School({vm.SchoolId}) not found");
            }

            var createUserResult = await _userService.CreateUser(user);
            if (!createUserResult.Succeeded) return BadRequest(createUserResult);
            else {
                // TODO: use other template the ForgotPassword
                await _appMailService.NewUserSetPassword(user, Request.GetHostUrl());
            }

            await _userManager.AddToRoleAsync(user, ApplicationRoleValue.Student);

            var student = vm.ToStudent(user.Id);
            await studentService.CreateStudentAsync(student, vm.SchoolId);

            return Ok(StudentResponse.Create(student, user));
        }

        [HttpPut("student/{studentId}")]
        public async Task<IActionResult> UpdateStudent(
            [FromRoute] long studentId,
            [FromBody] UpdateStudentRequest vm,
            [FromServices] IStudentService studentService)
        {
            var student = await studentService.GetStudentById(studentId);
            if (student == null) return NotFound($"Student({studentId}) NOT FOUND");
            var user = student.User;
            if (user == null) return StatusCode(500, $"User for Student({studentId}) NOT FOUND");

            var originalEmail = user.Email;
            vm.WriteTo(user);
            var updateUserresult = await _userService.UpdateUser(user, originalEmail);
            if (!updateUserresult.Succeeded) return BadRequest(updateUserresult);

            vm.WriteTo(student);
            await studentService.UpdateStudentAsync(student);

            return Ok(StudentResponse.Create(student, user));
        }
        #endregion

        #region GradingTerm
        [HttpPost("schools/{schoolId}/grading-terms")]
        public async Task<IActionResult> CreateGradingTerm(
            [FromRoute] long schoolId,
            [FromBody] CreateGradingTermRequest vm,
            [FromServices] ISchoolService schoolService,
            [FromServices] IGradingTermService gradingTermService

        )
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var school = await schoolService.GetSchoolById(schoolId);
            if (school == null) return BadRequest($"School({schoolId}) not found");

            var term = await gradingTermService.SetGradingTerm(vm.ToGradingTerm(schoolId));

            return Ok(GradingTermResponse.Create(term));
        }

        [HttpPut("schools/{schoolId}/grading-terms/{termId}")]
        public async Task<IActionResult> UpdateGradingTerm(
            [FromRoute] long schoolId,
            [FromRoute] long termId,
            [FromBody] UpdateGradingTermRequest vm,
            [FromServices] ISchoolService schoolService,
            [FromServices] IGradingTermService gradingTermService
        )
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var term = await gradingTermService.GetGradingTerm(schoolId, termId);
            if (term == null) return NotFound($"GradingTerm({termId})");

            vm.WriteTo(term);
            var newTerm = await gradingTermService.SetGradingTerm(term);

            return Ok(GradingTermResponse.Create(newTerm));
        }

        [HttpDelete("schools/{schoolId}/grading-terms/{termId}")]
        public async Task<IActionResult> DeleteGradingTerm(
            [FromRoute] long schoolId,
            [FromRoute] long termId,
            [FromServices] ISchoolService schoolService,
            [FromServices] IGradingTermService gradingTermService
        )
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var term = await gradingTermService.GetGradingTerm(schoolId, termId);
            if (term == null) return NotFound($"GradingTerm({termId})");

            await gradingTermService.DeleteGradingTerm(term);

            return Ok();
        }
        #endregion

        #region Activity
        [HttpGet("school/{schoolId}/faculty/{facultyId}/activity")]
        public async Task<IActionResult> GetActivitiesOfFacultyInSchool([FromRoute]long schoolId, [FromRoute]long facultyId)
        {
            var rs = await _activityService.GetActivitiesOfFaculty(schoolId, facultyId)
                .Where(r => r.Activity.DateAssigned.HasValue && r.Activity.DateAssigned < DateTime.UtcNow)
                .ToListAsync();
            var dto = GeneralRecord.ToGeneralDto(rs);
            return Ok(dto.ToObject());
        }
        #endregion

        #region User
        [HttpPost("user/{userId}/impersonate")]
        public async Task<IActionResult> Impersonate([FromRoute]long userId, [ModelBinder] string domain)
        {
            // TODO: check permission

            var user = await _userService.GetUserById(userId);
            if (user == null) return NotFound();

            var roles = await _userManager.GetRolesAsync(user);

            var token = await _authService.CreateToken(user, roles, new[] {
                new Claim(AppConstants.Security.ClaimTypes.DistrictDomain, domain),
            });
            if (string.IsNullOrEmpty(token)) return StatusCode(500);

            return Ok(token);
        }
        #endregion

        private async Task<bool> IsDistrictAdmin()
        {
            var roles = await GetCurrentRoles();
            return roles.Contains(ApplicationRoleValue.Admin);
        }

        private string GetDistrictDomain()
        {
            var isCustomBinderUsed = _config.GetSection("CustomBinderValues:enabled").Get<bool>();

            var domain = isCustomBinderUsed ? _config.GetSection("CustomBinderValues:data:domain").Get<string>() :  this.HttpContext.GetDistrictDomain();

            var domainClaim = this.HttpContext.User.Claims.FirstOrDefault(c => c.Type == AppConstants.Security.ClaimTypes.DistrictDomain);
            if (domainClaim == null || !string.Equals(domainClaim.Value, domain, System.StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidCredentialException($"Expected {domainClaim.Value}. Get {domain}");
            }

            return domain;
        }
    }
}
