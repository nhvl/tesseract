using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tesseract.Common.Extensions;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Models;

namespace Tesseract.Web.Controllers.Api
{
    public partial class FacultyController
    {
        [HttpGet("class/{classId}/discussion")]
        public async Task<IActionResult> GetClassDiscussions([FromRoute]long schoolId, [FromRoute]long classId)
        {
            var discussions = (await _discussionService.GetDiscussionsOfClass(classId)).ToArray();
            var dto = await IncludeDataForDiscussions(discussions);
            return Ok(dto.ToObject());
        }

        [HttpGet("activity/{activityId}/discussion")]
        public async Task<IActionResult> GetDiscussionsOfActivity([FromRoute]long schoolId, [FromRoute]long activityId)
        {
            var discussions = (await _discussionService.GetDiscussionsOfActivity(activityId)).ToArray();
            var dto = await IncludeDataForDiscussions(discussions);
            return Ok(dto.ToObject());
        }

        [HttpGet("discussionActivity/{discussionActivityId}/discussion")]
        public async Task<IActionResult> GetDiscussionFromDiscussionActivity([FromRoute]long discussionActivityId)
        {
            return Ok(await _discussionService.GetDiscussionFromDiscussionActivity(discussionActivityId));
        }

        [HttpGet("class/{classId}/discussion/{discussionId}")]
        public async Task<IActionResult> GetDiscussion([FromRoute]long schoolId, [FromRoute]long classId, [FromRoute]long discussionId)
        {
            var discussion = await _discussionService.GetById(discussionId);
            var discussions = new[] {discussion};
            var users = await GetCreatedUsersOfDiscussions(discussions);
            var discussionActivities = new List<Activity>();
            if(discussion.DiscussionActivityId.HasValue && discussion.DiscussionActivityId > 0)
            {
                discussionActivities.Add(await _activityService.GetActivity(discussion.DiscussionActivityId.Value, false));
            }
            return Ok((new GeneralDto{
                Discussions = discussions,
                Activities = discussionActivities.ToArray(),
                Users = users
            }).ToObject());
        }

        [HttpPost("class/{classId}/discussion")]
        public async Task<IActionResult> CreateDiscussion([FromRoute]long schoolId, [FromRoute]long classId, [FromBody]CreateDiscussionRequest vm)
        {
            var user = await GetCurrentUser();
            if (user == null) return Unauthorized();
            var discussionActivity = vm.ToActivity(classId, user.Id);
            Activity updatedActivity;
            if (discussionActivity.ActivityId < 1)
            {
                updatedActivity = await _activityService.CreateActivity(discussionActivity);
            }
            else
            {
                updatedActivity = await _activityService.EditActivity(discussionActivity);
            }
            var item = vm.ToDiscussion(classId, user.Id, _discussionService.GenerateThreadId());
            item.DiscussionActivityId = updatedActivity.ActivityId;
            var discussion = await _discussionService.Create(item);
            return Ok((new GeneralDto
            {
                Discussions = new[] { discussion },
                Activities = new[] { updatedActivity },
            }).ToObject());
        }

        [HttpPut("class/{classId}/discussion/{discussionId}")]
        public async Task<IActionResult> UpdateDiscussion([FromRoute]long schoolId, [FromRoute]long classId, [FromRoute]long discussionId, [FromBody]UpdateDiscussionRequest vm)
        {
            var user = await GetCurrentUser();
            var item = await _discussionService.GetById(discussionId);
            var discussionActivity = item.DiscussionActivityId.HasValue ? await _activityService.GetActivity(item.DiscussionActivityId.Value) : new Activity {
                ClassId = item.ClassId,
                Type = ActivityType.Discussion,
                Title = vm.Title,
                IsGraded = vm.IsGraded,
                CreatedBy = user?.Id,
            };
            discussionActivity = discussionActivity.Clone();
            discussionActivity.ClassId = item.ClassId;
            if (item.DiscussionActivityId.HasValue)
            {
                discussionActivity.ActivityId = item.DiscussionActivityId.Value;
            }

            vm.WriteTo(item, discussionActivity, user?.Id);

            Activity updatedActivity;
            if (discussionActivity.ActivityId < 1)
            {
                updatedActivity = await _activityService.CreateActivity(discussionActivity);
            }
            else
            {
                updatedActivity = await _activityService.EditActivity(discussionActivity);
            }

            item.DiscussionActivityId = updatedActivity.ActivityId;

            var updatedItem = await _discussionService.Update(item);

            var activities = new List<Activity>();
            if (updatedItem.DiscussionActivityId.HasValue && updatedItem.DiscussionActivityId.Value > 0)
            {
                activities.Add(await _activityService.GetActivity(updatedItem.DiscussionActivityId.Value));
            }
            return Ok((new GeneralDto
            {
                Discussions = new[] { updatedItem },
                Activities = new[] { updatedActivity },
            }).ToObject());
        }

        private async Task<ApplicationUser[]> GetCreatedUsersOfDiscussions(IEnumerable<Discussion> discussions) {
            var userIds = discussions.SelectNotNull(d => d.CreatedBy).ToArray();
            return (await _userService.GetUsersById(userIds)).ToArray();
        }
        private async Task<GeneralDto> IncludeDataForDiscussions(Discussion[] discussions)
        {
            var threadIds = discussions.Select(d => d.ThreadId).ToArray();

            var pActivities = _activityService.GetActivitiesById(
                discussions.SelectNotNull(d => d.DiscussionActivityId).ToArray()
            ).ToArrayAsync();
            var pLastComments = _commentService.GetLastCommentOfThreads(threadIds);

            var user = await GetCurrentUser();
            var pThreadUnreads = (_commentService.CountNewComments(user.Id, threadIds));

            var lastComments = (await pLastComments).ToArray();
            var pUsers = _userService.GetUsersById(
                discussions.SelectNotNull(d => d.CreatedBy).Concat(
                    lastComments.SelectNotNull(c => c.CreatedBy)
                    )
                    .Distinct()
                    .ToArray());

            var activities = (await pActivities);
            var users = (await pUsers).ToArray();
            var threadUnreads = (await pThreadUnreads).ToArray();
            

            return new GeneralDto {
                Discussions = discussions,
                Activities = activities,
                Users = users,
                ThreadUnreads = threadUnreads,
                Comments = lastComments,
            };
        }
    }
}
