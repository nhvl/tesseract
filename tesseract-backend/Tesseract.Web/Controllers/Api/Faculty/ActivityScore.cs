﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Models;

namespace Tesseract.Web.Controllers.Api
{
    public partial class FacultyController : ABasicApiController
    {
        [HttpGet("class/{classId}/activityScore")]
        public async Task<IActionResult> GetActivityScoresOfClass(long classId)
        {
            var activityScores = await _activityScoreService.GetActivityScoreOfClass(classId);
            return Ok(activityScores.Select(ActivityScoreDto.Create));
        }

        [HttpGet("activity/{activityId}/ActivityScore")]
        public async Task<IActionResult> GetActivityScoreById([FromRoute]long activityId)
        {
            var activity = await _activityService.GetActivity(activityId);
            if (activity == null) return NotFound($"Activity({activityId}) not found");

            var records = await _studentService.GetStudentsByActivity(activityId, activity.ClassId).ToListAsync();
            await _activityService.EnsureStudentsHasActivityScoreWithThreadId(records, activityId);

            var dto = GeneralRecord.ToGeneralDto(records);
            dto.Activities = new[]{activity};
            if (activity.Class != null) dto.Classes = new []{activity.Class};
            return Ok(dto.ToObject());
        }

        [HttpGet("activity/{activityId}/student/{studentId}")]
        public async Task<IActionResult> GetActivityScore([FromRoute]long activityId, [FromRoute]long studentId)
        {
            var score = await _activityService.GetActivityScoreOrCreate(activityId, studentId);
            if (score == null) return NotFound();
            return Ok(score);
        }

        [HttpPost("activity/{activityId}/student/{studentId}/[action]")]
        public async Task<IActionResult> SetGrade(
            [FromRoute]long activityId, [FromRoute]long studentId, [FromBody]ScoreRequestViewModel activityScore)
        {
            return Ok(await _activityService.SetGrade(
                activityId,
                studentId,
                activityScore.Score,
                activityScore.Communication,
                activityScore.Collaboration,
                activityScore.Character,
                activityScore.Creativity,
                activityScore.CriticalThinking,
                activityScore.IsExclude
            ));
        }
    }
}
