using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Models;

namespace Tesseract.Web.Controllers.Api
{
    public partial class FacultyController
    {
        [HttpGet("GradingTerm/{gradingTerm}/OpenActivities")]
        public async Task<IActionResult> GetOpenActivities([FromRoute]long facultyId, [FromRoute]long gradingTerm)
        {
            var activities = await _activityService.GetActivitiesOfFaculty(facultyId, gradingTerm, Activity.IsOpenExpr);

            return Ok(activities.Where(x => x.Type == ActivityType.Assessment || x.Type == ActivityType.Assignment));
        }

        [HttpGet("GradingTerm/{gradingTerm}/ClosedActivities")]
        public async Task<IActionResult> GetClosedActivities([FromRoute]long facultyId, [FromRoute]long gradingTerm)
        {
            var activities = await _activityService.GetActivitiesOfFaculty(facultyId, gradingTerm, Activity.Not(Activity.IsOpenExpr));

            return Ok(activities.Where(x => x.Type == ActivityType.Assessment || x.Type == ActivityType.Assignment));
        }

        [HttpGet("GradingTerm/{gradingTerm}/OpenToGradeActivities")]
        public async Task<IActionResult> GetUngradeActivities([FromRoute]long facultyId, [FromRoute]long gradingTerm)
        {
            var activities = await _activityService.GetActivitiesOfFaculty(facultyId, gradingTerm, x => x.IsGraded && (x.Type == ActivityType.Assessment || x.Type == ActivityType.Assignment));
            var mapper = new MapperConfiguration(cfg => {
                cfg.CreateMap<Activity, ActivityDetailModelView>()
                    .ForMember(dest => dest.SubmisstionCount, opt => opt.MapFrom(activity => activity.Scores.Count(s => s.HasSubmission())))
                    .ForMember(dest => dest.UnGradeCount, opt => opt.MapFrom(activity => activity.Scores.Count(s => s.IsPendingScore())))
                    ;
                cfg.CreateMap<Class, ShortClassViewModel>();
            }).CreateMapper();

            var viewModals = mapper.Map<IEnumerable<ActivityDetailModelView>>(activities);

            return Ok(viewModals);
        }

        [HttpGet("class/{classId}/activity")]
        public async Task<IActionResult> GetActivitiesOfClass([FromRoute]long classId)
        {
            var activities = await _activityService.GetActivitiesOfClass(classId);
            return Ok(activities);
        }

        [HttpGet("activity/{activityId}")]
        public async Task<IActionResult> GetActivityById([FromRoute] long activityId)
        {
            var activity = await _activityService.GetActivity(activityId);
            if (activity == null) return NotFound();

            var eClass = await _classService.GetClassAsync(activity.ClassId);
            if (eClass == null) return NotFound($"Class({activity.ClassId}) not found.");

            var category = !activity.CategoryId.HasValue ? null :
                await _activityCategoryService.GetCategory(activity.CategoryId.Value);
            if (category == null)
            {
                activity.CategoryId = null;
            }
            else
            {
                activity.Color    = category.Color;
                activity.IsGraded = category.IsGraded;
                activity.MaxScore = category.MaxScore;
                activity.Weight   = category.Weight;
            }

            return Ok(ActivityDto.Create(activity));
        }

        [HttpPost("class/{classId}/activity")]
        public async Task<IActionResult> CreateActivity([FromRoute] long classId, [FromBody] CreateActivityRequest model)
        {
            var classItem = await _classService.GetClassAsync(classId);
            if (classItem == null) return NotFound($"Class({classId}");

            var user = await GetCurrentUser();

            ActivityCategory category = null;
            if (model.CategoryId.HasValue) {
                var categoryId = model.CategoryId.Value;
                category = await _activityCategoryService.GetCategory(categoryId);
                if (category == null) return NotFound($"Category({categoryId}) not found");
            }

            var activity = model.ToEntity(classId, user.Id);
            if (category != null) {
                activity.Color    = category.Color;
                activity.IsGraded = category.IsGraded;
                activity.MaxScore = category.MaxScore;
                activity.Weight   = category.Weight;
            }

            activity = await _activityService.CreateActivity(activity);

            return Ok(ActivityDto.Create(activity));
        }

        [HttpPut("activity/{activityId}")]
        public async Task<IActionResult> UpdateActivity([FromRoute] long activityId, [FromBody] EditActivityRequest model)
        {
            var user = await GetCurrentUser();

            var (activity, shouldRecalculate) = await UpdateActivity(activityId, model, user.Id, true);
            if (shouldRecalculate) {
                await _activityService.ReCalculateCumulativeGrade(activity.ClassId);
            }

            await _notificationDispatcherService.FacultyUpdateActivity(user, activity);

            return Ok(ActivityDto.Create(activity));
        }

        [HttpPut("activity")]
        public async Task<IActionResult> UpdateActivities([FromBody] BatchEditActivityRequest[] xs)
        {
            var user = await GetCurrentUser();

            var ys = await Task.WhenAll(xs.Select(x => UpdateActivity(x.ActivityId, x, user.Id, false)));
            await _mainDbContext.SaveChangesAsync();

            var shouldRecalculateClassIds = ys.Select((y => y.Item2 ? y.Item1.ClassId : -1)).Where(classId => classId > 1).Distinct();
            await Task.WhenAll(shouldRecalculateClassIds.Select(classId => _activityService.ReCalculateCumulativeGrade(classId)));

            return Ok(ys.Select(y => y.Item1));
        }

        private async Task<(Activity, bool)> UpdateActivity(long activityId, EditActivityRequest model, long userId, bool commit = true)
        {
            var activity = await _activityService.GetActivity(activityId, false);
            if (activity == null) return (null, false);

            var category = model.CategoryId.HasValue ? await _activityCategoryService.GetCategory(model.CategoryId.Value) : null;

            var shouldRecalculate = model.ShouldRecalculateGrade(activity);
            model.WriteTo(activity, category, userId);

            await _activityService.EditActivitySimple(activity, commit);
            return (activity, shouldRecalculate);
        }

        [HttpPut("activity/{activityId}/publish")]
        public async Task<IActionResult> EditPublishTime([FromRoute] long activityId, [FromBody] EditPublishRequest request)
        {
            var activity = await _activityService.GetActivity(activityId, false);
            if (activity == null) return NotFound();

            activity.PublishStartDate = request.Start;
            activity.PublishEndDate = request.End;
            await _activityService.EditActivitySimple(activity);

            return Ok(ActivityDto.Create(activity));
        }

        [HttpPost("activity/{activityId}/duplicate")]
        public async Task<IActionResult> DuplicateActivity([FromRoute] long activityId, [FromBody]long[] classIds)
        {
            return Ok(await _activityService.DuplicateActivity(activityId, classIds));
        }
    }
}
