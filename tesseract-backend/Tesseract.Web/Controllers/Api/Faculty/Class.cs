using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Authorization;
using Tesseract.Web.Models;

namespace Tesseract.Web.Controllers.Api
{
    public partial class FacultyController
    {
        [HttpGet("class")]
        public async Task<IActionResult> GetClassesOfFacultyInSchool([FromRoute] long schoolId, [FromRoute] long facultyId)
        {
            var asFacultyClassRecords = _classService.GetClassesOfFacultyInSchool(schoolId, facultyId).ToListAsync();
            var rs = (await asFacultyClassRecords);

            var dto = GeneralRecord.ToGeneralDto(rs);
            return Ok(dto.ToObject());
        }

        [HttpGet("gradingTerm/{gradingTermId}/class")]
        public async Task<IActionResult> GetClassesOfFacultyInSchool([FromRoute] long schoolId, [FromRoute] long facultyId, [FromRoute] long gradingTermId)
        {
            var asFacultyClassRecords = _classService.GetClassesOfFacultyInSchool(schoolId, facultyId, gradingTermId).ToListAsync();
            var rs = (await asFacultyClassRecords);

            var dto = GeneralRecord.ToGeneralDto(rs);
            return Ok(dto.ToObject());
        }

        [HttpGet("class/{classId}")]
        public async Task<IActionResult> GetClass([FromRoute] long classId)
        {
            var classItem = await _classService.GetClassAsync(classId);
            if (classItem == null) return NotFound($"Class({classId}");

            var classViewModel = ClassViewModel.FromEntity(classItem);

            // get default grading range if not set
            if (classItem.UseDefaultGradesRange || classItem.GradeRanges == null)
            {
                classViewModel.GradeRanges = (await _keyValueService.GetDefaultGradeRange(null, classItem.SchoolId));
            }

            return Ok(classViewModel);
        }

        [HttpPost("class")]
        public async Task<IActionResult> CreateClass([FromRoute] long facultyId, [FromBody] ClassViewModel classItem)
        {
            var user = await GetCurrentUser();
            if (user == null)
            {
                return Unauthorized();
            }

            var nClass = classItem.ToEntity();
            nClass.CreatedBy = user.Id;
            nClass.UpdatedBy = user.Id;

            var eClass = await _classService.CreateClassAsync(nClass, facultyId);
            return Ok(eClass);
        }

        [PermissionClaim(Permission.Cons.CanEditClass)]
        [HttpPut("class/{classId}")]
        public async Task<IActionResult> UpdateClass([FromRoute] long classId, [FromBody] ClassViewModel classItem)
        {
            var user = await GetCurrentUser();
            if (user == null) return Unauthorized();

            var eClass = await _classService.GetClassAsync(classId);
            if (eClass == null) return NotFound($"Class({classId}");

            classItem.WriteTo(eClass);
            eClass.UpdatedBy = user.Id;

            return Ok(await _classService.UpdateClassAsync(eClass));
        }

        [HttpPut("class")]
        public async Task<IActionResult> BatchUpdate([FromRoute] long schoolId, [FromRoute] long facultyId, [FromBody] FacultyUpdateClassRequest[] data)
        {
            // TODO: permission
            var classIds = data.Select(c => c.ClassId).ToArray();
            var rs = await _classService.GetClassesOfFacultyInSchool(schoolId, facultyId, classIds).ToListAsync();
            var dto = GeneralRecord.ToGeneralDto(rs);
            var classes = dto.Classes;
            var classFaculties = dto.ClassFaculties;
            var notFoundClassIds = classIds.Where(classId => classes.All(c => c.ClassId != classId)).ToArray();
            if (notFoundClassIds.Length > 0) return NotFound($"Class not found: {string.Join(", ", notFoundClassIds)}");

            var changedClasses = new List<Class>(classes.Length);
            var changedClassFaculties = new List<ClassFaculty>(classFaculties.Length);

            foreach (var vm in data)
            {
                var aClass = classes.FirstOrDefault(c => c.ClassId == vm.ClassId);
                if (aClass == null) return NotFound($"Class not found: {vm.ClassId}");

                var classFaculty = classFaculties.FirstOrDefault(c => c.ClassId == vm.ClassId);
                if (classFaculty == null) return Unauthorized();

                var isChanged = vm.WriteTo(aClass);
                if (isChanged) changedClasses.Add(aClass);

                isChanged = vm.WriteTo(classFaculty);
                if (isChanged) changedClassFaculties.Add(classFaculty);
            }

            await _classService.UpdateClasses(changedClasses, changedClassFaculties);

            return Ok(dto.ToObject());
        }


        [HttpGet("asStudentClass")]
        public async Task<IActionResult> GetAsStudentClasses([FromRoute] long schoolId, [FromRoute] long facultyId)
        {
            var asStudentClassRecords = _classService.GetClassesOfStudentInSchool(schoolId, facultyId).ToListAsync();
            var rs = (await asStudentClassRecords);

            var dto = GeneralRecord.ToGeneralDto(rs);
            return Ok(dto.ToObject());
        }
    }
}
