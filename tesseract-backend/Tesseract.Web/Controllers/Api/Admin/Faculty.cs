﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using AutoMapper;
using EasyCaching.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Common;
using Tesseract.Web.Exceptions;
using Tesseract.Web.Extensions;
using Tesseract.Web.Models;
using Tesseract.Web.Service;

namespace Tesseract.Web.Controllers.Api
{
    public partial class AdminController : ABasicApiController
    {
        [HttpGet("faculty")]
        public async Task<IActionResult> GetFaculties([ModelBinder] string domain)
        {
            var xs = await IsDistrictAdmin()
                ? await _districtService.GetFaculties((await _districtService.GetByDomain(domain)).DistrictId)
                : (await _districtService.GetFacultiesForSchoolAdmin((await GetCurrentUser()).Id))
                ;
            return Ok(_mapper.Map<IEnumerable<FacultyDistrictView>>(xs));
        }

        [HttpGet("faculty/{facultyId}")]
        public async Task<IActionResult> GetFacultyByFacultyId([FromRoute]long facultyId)
        {
            var xs = await _facultyService.GetFacultyAsAdmin(facultyId).ToListAsync();
            var dto = GeneralRecord.ToGeneralDto(xs);
            if ((dto.Faculties?.Length ?? 0) < 1) return NotFound();

            return Ok(dto.ToObject());
        }

        [HttpGet("school/{schoolId2}/faculty/{facultyId}")]
        public async Task<IActionResult> GetFacultyOfSchool([FromRoute]long schoolId2, [FromRoute]long facultyId)
        {
            var (faculty, user) = await _facultyService.GetFacultyOfSchool(schoolId2, facultyId);
            if (faculty == null) return NotFound();

            return Ok(FacultyDto.Create(user, faculty));
        }

        [HttpPost("faculty")]
        public async Task<IActionResult> Create([FromBody]CreateFacultyRequest vm)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (vm.Schools.Length < 1) return BadRequest(ErrorByCode.SchoolIsRequired);

            var districtId = await GetRequestDistrictId();

            var user = vm.ToUser(districtId);

            var createUserResult = await _userService.CreateUser(user);
            if (!createUserResult.Succeeded) return ToErrorCode(createUserResult);                

            await _appMailService.NewUserSetPassword(user, Request.Host.ToString());

            // add to default role
            await _userManager.AddToRoleAsync(user, ApplicationRoleValue.Faculty);

            // TODO: add selected roles. FOR NOW, ADD STUDENT MANAGER TEMPORARY
            await _userManager.AddToRoleAsync(user, ApplicationRoleValue.StudentManager);

            var faculty = vm.ToFaculty(user.Id);
            faculty = await _facultyService.CreateFaculty(faculty);

            foreach (var schoolFaculty in vm.ToSchoolFaculties(faculty.FacultyId))
            {
                await _mainDbContext.AddLinkEntityAsync(schoolFaculty, false);
            }
            await _mainDbContext.SaveChangesAsync();

            return Ok(FacultyDto.Create(user, faculty));
        }
        private BadRequestObjectResult ToErrorCode(IdentityResult errorResult)
        {
            //Username is unchanged and equals to Email. Bypass it.
            var errorItem = errorResult.Errors.Where(e => e.Code != nameof(ErrorByCode.DuplicateUserName)).FirstOrDefault();
            if (errorItem != null)
            {
                var errorCode = errorItem.Code;
                if (errorCode == nameof(ErrorByCode.DuplicateEmail)) return BadRequest(ErrorByCode.DuplicateEmail);
            }
            return BadRequest(errorResult);
        }

        [HttpPut("faculty/{facultyId}")]
        public async Task<IActionResult> Update([FromRoute]long facultyId, [FromBody]UpdateFacultyRequest vm)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (vm.Schools != null && vm.Schools.Length < 1) return BadRequest(ErrorByCode.SchoolIsRequired);

            var faculty = await _facultyService.GetFacultyAsync(facultyId);
            if (faculty == null) return NotFound();

            var user = await _userService.GetUserById(faculty.FacultyId);
            if (user == null) throw new ApplicationException($"User of Faculty({faculty.FacultyId}) NOT FOUND");

            var originalEmail = user.Email;
            vm.WriteTo(user);
            var updateUserResult = await _userService.UpdateUser(user, originalEmail);
            if (!updateUserResult.Succeeded) return ToErrorCode(updateUserResult);

            vm.WriteTo(faculty);
            await _mainDbContext.UpdateAsync(faculty, false);

            if (vm.Schools != null)
            {
                await _mainDbContext.DeleteLinkEntityAsync<SchoolFaculty>(sf => sf.FacultyId == facultyId, false);
                foreach (var sf in vm.ToSchoolFaculties(facultyId))
                {
                    await _mainDbContext.AddLinkEntityAsync(sf, false);
                }
            }

            await _mainDbContext.SaveChangesAsync();

            return Ok(FacultyDto.Create(user, faculty));
        }
    }
}
