namespace Tesseract.Web.Controllers.Api
{
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Tesseract.Core.Entities.Identity;
    using Tesseract.Web.Authorization;
    using Tesseract.Web.Models;

    /// <summary>
    /// This AdminController partial specific for Role
    /// </summary>
    public partial class AdminController : ABasicApiController
    {
        [HttpGet("permission")]
        public IActionResult GetPermissions()
        {
            return this.Ok(Permission.All.Select(p => p.Name));
        }

        [HttpGet("role")]
        public async Task<IActionResult> GetRoles()
        {
            var roles = await this._roleManager.Roles.Select(r => RoleViewModel.Create(r.Id, r.Name, r.IsSystemRole, r.Description)).ToListAsync();
            return Ok(roles);
        }

        [HttpGet("role/{roleId}")]
        public async Task<IActionResult> GetRole([FromRoute] long roleId)
        {
            var r = await this._roleManager.Roles.FirstOrDefaultAsync(role => role.Id == roleId);
            if (r == null)
            {
                return NotFound();
            }

            var cs = await _roleManager.GetClaimsAsync(r);

            return Ok(RoleViewModel.Create(r, cs));
        }

        [HttpPost("role")]
        public async Task<IActionResult> CreateRole([FromBody] CreateRoleRequest role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var r = new ApplicationRole(role.Name, false) { Description = role.Description };
            var result = await _roleManager.CreateAsync(r);
            if (!result.Succeeded)
            {
                return BadRequest(result);
            }

            var permissions = role.Permissions;
            if (permissions != null)
            {
                foreach (var permission in permissions)
                {
                    var createResult = await _roleManager.AddClaimAsync(r, new Claim(Permission.ClaimType, permission));
                    if (!createResult.Succeeded)
                    {
                        return BadRequest(createResult);
                    }
                }
            }

            return Ok(RoleViewModel.Create(r, permissions));
        }

        [HttpPut("role/{roleId}")]
        public async Task<IActionResult> UpdateRole([FromRoute] long roleId, [FromBody] UpdateRoleRequest role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var r = await _roleManager.Roles.FirstOrDefaultAsync(x => x.Id == roleId);
            if (r == null)
            {
                return NotFound();
            }

            role.WriteTo(r);
            var result = await _roleManager.UpdateAsync(r);
            if (!result.Succeeded)
            {
                return BadRequest(result);
            }

            var permissions = role.Permissions;
            if (permissions != null)
            {
                var currentClaims = await _roleManager.GetClaimsAsync(r);

                var newPermissions = role.Permissions.Where(permission => currentClaims.All(c => c.Value != permission));
                foreach (var permission in newPermissions)
                {
                    var createResult = await _roleManager.AddClaimAsync(r, new Claim(Permission.ClaimType, permission));
                    if (!createResult.Succeeded)
                    {
                        return BadRequest(createResult);
                    }
                }

                var removedClaims = currentClaims.Where(c => permissions.All(p => p != c.Value));
                foreach (var claim in removedClaims)
                {
                    var removeResult = await _roleManager.RemoveClaimAsync(r, claim);
                    if (!removeResult.Succeeded)
                    {
                        return BadRequest(removeResult);
                    }
                }
            }

            return Ok(RoleViewModel.Create(r, permissions));
        }

        [HttpDelete("role/{roleId}")]
        public async Task<IActionResult> DeleteRole([FromRoute] long roleId) {
            var r = await _roleManager.Roles.FirstOrDefaultAsync(x => x.Id == roleId);
            if (r == null)
            {
                return NotFound();
            }

            if (r.IsSystemRole)
            {
                return BadRequest();
            }

            var result = await _roleManager.DeleteAsync(r);
            if (!result.Succeeded)
            {
                return StatusCode(500, result.Errors);
            }

            return Ok(RoleViewModel.Create(r, (string[]) null));
        }

        [HttpGet("district/{districtId}/user/{userId}/role")]
        public async Task<IActionResult> GetUserRoles([FromRoute] long districtId, [FromRoute] long userId)
        {
            var user = await _userService.GetUserById(userId);
            if (user.DistrictId != districtId)
            {
                return NotFound();
            }

            var roles = await _userManager.GetRolesAsync(user);
            return Ok(roles);
        }

        [HttpPut("district/{districtId}/user/{userId}/role")]
        public async Task<IActionResult> UpdateUserRoles([FromRoute] long districtId, [FromRoute] long userId, [FromBody] string[] roles)
        {
            var user = await _userService.GetUserById(userId);
            if (user == null || user.DistrictId != districtId)
            {
                return NotFound($"User({userId}) not found");
            }

            var currentRoles = await _userService.GetUserCustomRoles(user);
            var newRoles = roles.Where(role => currentRoles.All(r => r.Name != role)).ToArray();
            var removedRoles = currentRoles.Select(role => role.Name).Where(role => !roles.Contains(role)).ToArray();

            if (newRoles.Length > 0)
            {
                var result = await _userManager.AddToSchoolRoleAsync(user, null, newRoles);
                if (!result.Succeeded)
                {
                    return BadRequest(result);
                }
            }

            if (removedRoles.Length > 0)
            {
                var result = await _userManager.RemoveFromSchoolRolesAsync(user, null, removedRoles);
                if (!result.Succeeded)
                {
                    return BadRequest(result);
                }
            }

            return Ok(roles);
        }

        [PermissionClaim(Permission.Cons.CanEditSchoolRole)]
        [HttpPost("school/{schoolId}/faculty/{facultyId}/roles")]
        public async Task<IActionResult> AddFacultyRoles([FromRoute]long schoolId, [FromRoute]long facultyId, [FromBody] CustomRoleRequest routeRequest)
        {
            var user = await GetFacultyUser(facultyId);
            var result = await _userManager.AddToSchoolRoleAsync(user, schoolId, routeRequest.CustomRoles);
            return result.Succeeded ? Ok(await _userManager.GetRolesAsync(user)) : BadRequest(result) as IActionResult;
        }

        [PermissionClaim(Permission.Cons.CanEditSchoolRole)]
        [HttpGet("school/{schoolId}/faculty/{facultyId}/roles")]
        public async Task<IActionResult> GetFacultyRoles([FromRoute]long schoolId, [FromRoute]long facultyId)
        {
            var user = await GetFacultyUser(facultyId);
            return Ok(await _userManager.GetRolesWithSchoolIdAsync(user, schoolId));
        }

        [PermissionClaim(Permission.Cons.CanEditSchoolRole)]
        [HttpDelete("school/{schoolId}/faculty/{facultyId}/roles")]
        public async Task<IActionResult> DeleteFacultyRoles([FromRoute]long schoolId, [FromRoute]long facultyId, [FromBody] CustomRoleRequest routeRequest)
        {
            var user = await GetFacultyUser(facultyId);

            var result = await _userManager.RemoveFromSchoolRolesAsync(user, schoolId, routeRequest.CustomRoles);
            return result.Succeeded ? Ok(await _userManager.GetRolesAsync(user)) : BadRequest(result) as IActionResult;
        }

        private async Task<ApplicationUser> GetFacultyUser(long facultyId)
        {
            return await _userManager.FindByIdAsync(facultyId.ToString());
        }
    }
}
