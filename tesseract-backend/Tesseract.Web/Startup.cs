using AutoMapper;
using EasyCaching.Core;
using EasyCaching.InMemory;
using EasyCaching.Redis;
using Hangfire;
using Hangfire.MemoryStorage;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Config;
using Tesseract.Core.Entities.Identity;
using Tesseract.Infrastructure.Configuration;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Data.NoSql;
using Tesseract.Infrastructure.Services.Impl;
using Tesseract.Web.Authorization;
using Tesseract.Web.Binders;
using Tesseract.Web.Common;
using Tesseract.Web.Extensions;
using Tesseract.Web.Mapping;
using Tesseract.Web.MiddleWare;
using Tesseract.Web.Service.Notification;
using RabbitMQ.Client;

namespace Tesseract.Web
{
    public class Startup
    {
        private const string DevCorsPolicy = "DevCorsPolicy";
        private const string DefaultConnectionString = "DefaultConnection";
        private readonly IConfiguration _configuration;

        public Startup( IConfiguration config)
        {
            _configuration = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var authTokenSettings = _configuration.GetSection("Api:TokenSettings").Get<AuthenticationSettings>();
            // decrypt sensitive information in config file
            //authTokenSettings.ValidateTokenKey = tripleDES.Decrypt(authTokenSettings.ValidateTokenKey);
            services.AddSingleton(authTokenSettings);
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.TokenValidationParameters =
                        new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = false,
                            ValidateIssuerSigningKey = true,

                            ValidIssuer = authTokenSettings.Issuer,
                            ValidAudience = authTokenSettings.Audience,

                            ClockSkew = TimeSpan.FromMinutes(authTokenSettings.DeltaMinutes),

                            RequireExpirationTime = false,
                            RequireSignedTokens = true,

                            IssuerSigningKey = new SymmetricSecurityKey(Convert.FromBase64String(authTokenSettings.ValidateTokenKey))
                        };

                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query[AppConstants.Notification.AccessToken]; // 2019-08-30 huyn: SignalR negotiation HTTP request don't inject Bearer token in HTTP header. We must read the token from query string.

                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) && (path.StartsWithSegments(AppConstants.Notification.HubUrl)))
                            {
                                context.Token = accessToken;
                            }

                            return Task.CompletedTask;
                        }
                    };
                })
                .AddCookie("Cookies")
                .AddGoogleOpenIdConnect("Google", options =>
                {
                    var clientInfo = (AnalyticsReportingSettings)services.First(x => x.ServiceType == typeof(AnalyticsReportingSettings)).ImplementationInstance;
                    options.ClientId = clientInfo.ClientId;
                    options.ClientSecret = clientInfo.ClientSecret;
                    options.SignedOutCallbackPath = new PathString("/signout-callback-oidc");
                });

            services.AddCors(options => options.AddPolicy(DevCorsPolicy, builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()));

            services.AddAuthorization(options =>
            {
                options.AddPolicy(AppConstants.Security.Policy.CrossedPortalAccess, policy => policy.Requirements.Add(new SufficientDistrictRequirement()));
                options.AddPolicy(AppConstants.Security.Policy.SchoolAccess, policy =>
                {
                    policy.Requirements.Add(new SufficientDistrictRequirement());
                    policy.Requirements.Add(new MatchedUserClaimsRequirement());
                    policy.Requirements.Add(new MatchedSchoolClaimsRequirement());
                    policy.Requirements.Add(new QueryPermissionRequirement());
                });

                options.AddPolicy(AppConstants.Security.Policy.GeneralAccess, policy =>
                {
                    policy.Requirements.Add(new SufficientDistrictRequirement());
                    policy.Requirements.Add(new MatchedUserClaimsRequirement());
                    policy.Requirements.Add(new QueryPermissionRequirement());
                });
            });

            services.AddDbContext<MainDbContext>(options => options.UseSqlServer(_configuration.GetConnectionString(DefaultConnectionString)));

            services.AddDefaultIdentity<ApplicationUser>()
                    .AddRoles<ApplicationRole>()
                    .AddEntityFrameworkStores<MainDbContext>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                // options.Password.RequireNonAlphanumeric = true;
                // options.Password.RequiredUniqueChars = 1;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 8;
                options.User.RequireUniqueEmail = true;
                options.SignIn.RequireConfirmedEmail = true;
            });

            services.Configure<JsonSerializerSettings>(settings => settings.AddTesseractConverters());

            services.AddMvc(opt =>
                        {
                            opt.ModelBinderProviders.Insert(0, new TesseractBinderProvider());
                            var binderDict = _configuration.GetCustomBinder();
                            if (binderDict != null)
                            {
                                opt.ModelBinderProviders.Insert(0, new PreconfigBinderProvider(binderDict));
                            }

                            if (opt.OutputFormatters.FirstOrDefault(fm => fm is JsonOutputFormatter) is JsonOutputFormatter jsonOutputFormatter)
                            {
                                opt.OutputFormatters.Insert(0, new CustomOutputFormatter(jsonOutputFormatter.PublicSerializerSettings, ArrayPool<char>.Create())); // TODO: 2019-08-30 this one is anti pattern. We should find a way to keep using the standard JsonOutputFormatter.
                            }

                            opt.CacheProfiles.Add(AppConstants.Cache.Profile.StaticApi, new CacheProfile() { Duration = 60 * 10 });
                        })

                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                    .AddJsonOptions(options => options.SerializerSettings.AddTesseractConverters())
                    .AddControllersAsServices();

            services.AddTesseract(_configuration);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(AppConstants.AppVersionString, new Info { Title = $"{AppConstants.AppName} {AppConstants.AppVersionString}", Version = AppConstants.AppVersionString });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "JWT Bearer", Name = "Authorization", Type = "apiKey" });
            });

            services.AddHttpContextAccessor();

            services.AddEasyCaching(option =>
            {
                var redisSectionName = "Caching:easycaching:inredis";
                var shouldUseRedis = _configuration.GetSection(redisSectionName).Exists();
                if (shouldUseRedis) {
                    option.UseRedis(_configuration, AppConstants.Cache.CacheDefault, redisSectionName);
                } else {
                    option.UseInMemory(_configuration, AppConstants.Cache.CacheDefault, "Caching:easycaching:inmemory");
                }
            });

            services.AddSpaStaticFiles(configuration => configuration.RootPath = _configuration.GetSection("FrontEnd:Root").Get<string>());

            var config = new MapperConfiguration(cfg => cfg.AddProfile<DefaultMappingProfile>());
            config.AssertConfigurationIsValid();
            services.AddSingleton(config.CreateMapper());

            services.AddResponseCompression(options => options.EnableForHttps = true );
            services.AddSignalR();

            services.AddHangfire(configuration =>
            {
                configuration.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                             .UseSimpleAssemblyNameTypeSerializer()
                             .UseRecommendedSerializerSettings();

                var hangFileConnectionString = _configuration.GetSection("Services:HangFireDbConnection").Get<string>();
                if (string.IsNullOrEmpty(hangFileConnectionString))
                {
                    configuration.UseMemoryStorage();
                    return;
                }

                configuration.UseSqlServerStorage(hangFileConnectionString, new SqlServerStorageOptions
                                {
                                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                                    QueuePollInterval = TimeSpan.Zero,
                                    UseRecommendedIsolationLevel = true,
                                    UsePageLocksOnDequeue = true,
                                    DisableGlobalLocks = true,
                                });
            });
            

            services.AddHangfireServer();

            services.AddApiVersioning();

            var rabbitMqUrl = _configuration.GetSection("Services:RabbitMqUrl").Get<string>();
            var rabbitMqUri = Uri.TryCreate(rabbitMqUrl, UriKind.Absolute, out var val) ? val : throw new InvalidOperationException();
            var factory = new ConnectionFactory() { Uri = rabbitMqUri };
            services.AddSingleton<IConnection>(factory.CreateConnection());
        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
                              IHostingEnvironment env,
                              RoleManager<ApplicationRole> roleManager,
                              UserManager<ApplicationUser> userManager,
                              ILoggerFactory logFactory,
                              MainDbContext mainDbContext,
                              IDocumentDBRepository repo)
        {
            var logger = logFactory.CreateLogger<Startup>();
            DocumentDBInitializer.Init(repo, logFactory.CreateLogger<IDocumentDBRepository>());
            
            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();

                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"/swagger/{AppConstants.AppVersionString}/swagger.json", $"{AppConstants.AppName} API {AppConstants.AppVersionString}");
                });

                app.UseCors(DevCorsPolicy);

                var useMigration = _configuration.GetSection("DatabaseSettings:AutoMigration").Get<bool>();
                if (useMigration)
                {
                    var firstMigration = "Init_Identity_Tables";
                    if (mainDbContext.Database.GetPendingMigrations().FirstOrDefault()?.EndsWith(firstMigration) == true)
                    {
                        mainDbContext.GetService<IMigrator>().Migrate(firstMigration);
                        TesseractDatabaseInitializer.SeedRoles(roleManager, logger).Wait();
                        TesseractDatabaseInitializer.SeedUsers(userManager as TesseractUserManager, logger).Wait();
                    }

                    mainDbContext.Database.Migrate();
                }

                app.UseHangfireDashboard();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            TesseractDatabaseInitializer.SeedRoles(roleManager, logger).Wait(); // Seed roles again to make sure all the permissions is correctly set.


            app.UseHangfireServer();

            app.UseStaticFiles(new StaticFileOptions() 
            { 
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), AppConstants.Path.Resources)),
                RequestPath = new PathString($"/{AppConstants.Path.Resources}")
            });
            //app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMiddleware<ErrorMiddleware>();
            app.UseMiddleware<EnrichLogContextMiddleware>();
            app.UseSpaStaticFiles();
            
            app.UseSignalR(routes =>
            {
                routes.MapHub<NotificationHub>(AppConstants.Notification.HubUrl);
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                var shouldUserProxy = _configuration.GetSection("FrontEnd:ShouldUseProxyForReact").Get<bool>();

                if (env.IsDevelopment() && shouldUserProxy)
                {
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:3000");
                }
            });
        }
    }
}