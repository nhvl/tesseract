﻿using System;
using System.IO;
using System.Net;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Serilog;
using Tesseract.Web.Extensions;

namespace Tesseract.Web.Extensions 
{ 
    public static class LoggingServiceConfig{
        public static NetworkCredential EmailCredential = new NetworkCredential();
    }
}
  
namespace Tesseract.Web
{
    public class Program
    {
        public static IConfiguration Configuration { get; } = ConfigBuilder(new ConfigurationBuilder())
            .Build(); 
   
        public static IConfigurationBuilder ConfigBuilder(IConfigurationBuilder builder)
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production";
            builder.SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env}.json", optional: true)
                    .AddJsonFile($"appsettings.{env}.local.json", optional: true);

            builder.AddEnvironmentVariables();
            return builder;
        }
 
        public static void Main(string[] args)
        {
            var logEmailConfig = Configuration.GetSection("Credentials:LogEmail");
            if (logEmailConfig.Exists()){
                LoggingServiceConfig.EmailCredential = new NetworkCredential(logEmailConfig["userName"], logEmailConfig["password"]);
            }

            var loggerConfig = new LoggerConfiguration().ReadFrom.Configuration(Configuration, "Logging:Serilog");
                    
            Log.Logger = loggerConfig.CreateLogger();
 
            try 
            {
                CreateWebHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally 
            {
                Log.CloseAndFlush();
            } 
        }
 
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration( (_, builder) => ConfigBuilder(builder))
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseSerilog();
    }
}
