﻿using System;
using System.Text.RegularExpressions;

namespace Tesseract.Web.Extensions
{
    public static class UriExtension
    {
        private const string YoutubeHostPattern = @"^((?:www|m)\.)?((?:youtube\.com|youtu.be))$";
        private const string SoundCloudHostPattern = @"^(soundcloud\.com|snd\.sc)$";
        private const string VimeoHostPattern = @"^(?:www\.|player\.)?vimeo.com$";
        public static bool IsYoutubeLink(this Uri uri)
        {
            return new Regex(YoutubeHostPattern).Match(uri.Host).Success;
        }

        public static bool IsVimeoLink(this Uri uri)
        {
            return new Regex(VimeoHostPattern).Match(uri.Host).Success;
        }

        public static bool IsSoundCloudLink(this Uri uri)
        {
            return new Regex(SoundCloudHostPattern).Match(uri.Host).Success;
        }

        public static string GetHostUrl(this Uri uri) => uri.GetLeftPart(UriPartial.Authority);
    }
}
