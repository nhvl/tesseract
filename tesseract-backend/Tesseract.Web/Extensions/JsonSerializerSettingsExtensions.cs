﻿using Tesseract.Web.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Tesseract.Web.Extensions
{
    public static class JsonSerializerSettingsExtensions
    {
        public static void AddTesseractConverters(this JsonSerializerSettings settings)
        {
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.Converters.Add(new DatetimeToNumberConverter());
            settings.Converters.Add(new XssSanitizedStringConverter());
            settings.Converters.Add(new ActItemConverter());
            settings.Converters.Add(new TimeSpanToNumberConverter());
        }
    }
}
