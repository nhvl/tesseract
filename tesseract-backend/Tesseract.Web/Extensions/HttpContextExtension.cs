﻿using Microsoft.AspNetCore.Http;
using System.Linq;

namespace Tesseract.Web.Extensions
{
    public static class HttpContextExtension
    {
        public static string GetDistrictDomain(this HttpContext context)
        {
            var subdomains = context.Request?.Host.Value?.Split('.');
            return subdomains?.Length <= 1 ? null : subdomains?.First();
        }
    }
}
