﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Tesseract.Core.Entities.Config;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Services.Implementation.LearningManagement;
using Tesseract.Core.Services.Interface.Authorization;
using Tesseract.Core.Services.Interface.LearningManagement;
using Tesseract.Core.Services.Interface.Repository;
using Tesseract.Infrastructure.Authorization;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Data.NoSql;
using Tesseract.Infrastructure.Services.Impl;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Web.Authorization;
using Tesseract.Web.Common;
using Tesseract.Web.Service;
using Tesseract.Web.Service.Background;
using Tesseract.Web.Service.Notification;
using Tesseract.Web.Service.GoogleApi;

namespace Tesseract.Web.Extensions
{

    public static class TesseractServicesExtensions
    {
        public static void AddTesseract(this IServiceCollection services, IConfiguration config)
        {
            // TODO Must be Merge AnalyticsReporting & GoogleDrive
            var gaApiSettings = config.GetSection("GoogleApis:AnalyticsReporting").Get<AnalyticsReportingSettings>();
            services.AddSingleton(gaApiSettings);

            var gaDriveSettings = config.GetSection("GoogleApis:GoogleDrive").Get<GoogleApiSettings>();
            services.AddSingleton(gaDriveSettings);

            var smtpSettings = config.GetSection("SMTPServer").Get<SMTPServerSettings>();
            services.AddSingleton(smtpSettings);

            services.AddTransient<ISchoolService, SchoolService>();
            services.AddTransient<IGradingTermService, GradingTermService>();
            services.AddTransient<IClassService, ClassService>();
            services.AddScoped<IClassTemplateService, ClassTemplateService>();
            services.AddTransient<IStudentService, StudentService>();
            services.AddTransient<ILocalizationService, LocalizationService>();
            services.AddTransient<IFacultyService, FacultyService>();
            services.AddTransient<IActivityCategoryService, ActivityCategoryService>();
            services.AddTransient<IKeyValueService, CachedKeyValueService>();
            services.AddTransient<IActivityService, ActivityService>();
            services.AddTransient<IActivityScoreService, ActivityScoreService>();
            services.AddTransient<IActDocService, ActDocService>();
            services.AddTransient<ISubmissionService, SubmissionService>();
            services.AddTransient<IMediaStorageService, MediaStorageService>();
            services.AddTransient<IDistrictService, CachedDistrictService>();
            services.AddTransient<IParentService, ParentService>();
            services.AddTransient<IDiscussionService, DiscussionService>();
            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<IEmailTemplateService, EmailTemplateService>();
            services.AddTransient<IAppMailService, AppMailService>();
            services.AddTransient<ILoginLogService, LoginLogService>();
            services.AddTransient<IEmailSenderService, EmailSenderService>();
            services.AddTransient<ISecurityService, SecurityService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IGoogleAuthService, GoogleAuthService>();
            services.AddTransient<IGoogleDriveService, GoogleDriveService>();
            services.AddTransient<IGoogleCalendarService, GoogleCalendarService>();
            services.AddTransient<IUserNotificationSettingService, UserNotificationSettingService>();
            services.AddTransient<INotificationDispatcherService, NotificationDispatcherService>();
            services.AddTransient<IRazorViewToStringRenderer, RazorViewToStringRenderer>();
            services.AddHostedService<DistrictReportRunner>();

            services.AddTransient<IAuthService, AuthService>();

            services.AddTransient<IClassBuilder, ClassBuilder>();
            services.AddTransient<IClassRepo, ClassRepo>();
            services.AddTransient<IGradingTermRepo, GradingTermRepo>();

            services.AddScoped<INotificationHubHelper, NotificationHubHelper>();
            services.AddScoped<IResouceAccessHandler, DefaultResouceAccessHandler>();
            services.AddScoped<UserManager<ApplicationUser>, TesseractUserManager>();

            services.AddSingleton<IAuthorizationHandler, SufficientDistrictHandler>();
            services.AddSingleton<IAuthorizationHandler, MatchedUserClaimsHandler>();
            services.AddSingleton<IAuthorizationHandler, QueryPermissionHandler>();



            // register cosmosdb services
            var azureConfig = config.GetSection("AzureCloud");

            var cosmosConfig = azureConfig.GetSection("CosmosDB");
            var docDbSettings = new DocumentDbSettings
            {
                DatabaseId = cosmosConfig.GetSection("DatabaseId").Value,
                EndpointUrl = cosmosConfig.GetSection("EndpointUrl").Value,
                MasterKey = cosmosConfig.GetSection("MasterKey").Value,
            };

            services.Configure<DocumentDbSettings>(Options => {
                Options.DatabaseId = docDbSettings.DatabaseId;
                Options.EndpointUrl = docDbSettings.EndpointUrl;
                Options.MasterKey = docDbSettings.MasterKey;
            });

            services.AddSingleton<IDocumentDBRepository, DocumentDBRepository>();

            var blobStorageConfig = azureConfig.GetSection("BlobStorage");
            var account = blobStorageConfig.GetSection("Account").Value;
            if (string.IsNullOrEmpty(account))
            {
                services.AddScoped<IImageUploader, LocalImageUploader>();
            }
            else
            {
                services.Configure<BlobStorageSetttings>(Options => {
                    Options.Account = account;
                    Options.Key = blobStorageConfig.GetSection("PrimaryKey").Value;
                    Options.ImageContainer = blobStorageConfig.GetSection("ImageContainer").Value;
                });
                services.AddScoped<IImageUploader, AzureImageUploader>();
            }


            //set time to expiration to 1 day when reset password.
            services.Configure<DataProtectionTokenProviderOptions>(options => options.TokenLifespan = TimeSpan.FromDays(1));
            //var client = new DocumentClient(new Uri(Configuration.GetSection("CosmosDB:EndpointUrl").Value), Configuration.GetSection("CosmosDB:MasterKey").Value);
            services.Configure<ServiceSettings>(config.GetSection("Services"));
        }
    }
}
