﻿// Copyright (c) Denovu. All rights reserved.

namespace Tesseract.Web.Extensions
{
    using System.Collections.Generic;
    using Microsoft.Extensions.Configuration;

    /// <summary>
    /// Defines the <see cref="IConfigurationExtension" />.
    /// </summary>
    internal static class IConfigurationExtension
    {
        /// <summary>
        /// The GetCustomBinder dictionary to replace binding from request url.
        /// </summary>
        /// <param name="configuration">The configuration<see cref="IConfiguration"/></param>
        /// <returns>The <see cref="Dictionary{string, object}"/>Dictionary to replace binding from request url.</returns>
        internal static Dictionary<string, object> GetCustomBinder(this IConfiguration configuration)
        {
            var binderSetting = configuration.GetSection("CustomBinderValues");
            if (!binderSetting.GetValue<bool>("enabled"))
            {
                return null;
            }

            return binderSetting.GetSection("data").Get<Dictionary<string, object>>();
        }
    }
}
