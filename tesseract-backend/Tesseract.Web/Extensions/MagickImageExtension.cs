﻿using ImageMagick;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Tesseract.Web.Extensions
{
    public static class MagickImageExtension
    {
        public static bool IsPng(this MagickImage magickImage) => MagickFormat.Png <= magickImage.Format && magickImage.Format <= MagickFormat.Png8;
        public static bool IsJpg(this MagickImage magickImage) => MagickFormat.Jpeg == magickImage.Format || MagickFormat.Jpg == magickImage.Format;
    }
}
