﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Tesseract.Web.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static T ResolveWith<T>(this IServiceProvider provider, params object[] parameters) where T : class =>
            ActivatorUtilities.CreateInstance<T>(provider, parameters);
    }
}
