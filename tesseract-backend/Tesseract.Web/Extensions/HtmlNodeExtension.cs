﻿using HtmlAgilityPack;
using System;
using System.ComponentModel;
using System.Linq;

namespace Tesseract.Web.Extensions
{
    public static class HtmlNodeExtension
    {
        public static string GetClosingTag(this HtmlNode node)
        {
            return $"</{node.Name}>";
        }

        public static string GetOpeningTag(this HtmlNode node)
        {
            return node.Attributes.Count == 0 ? $"<{node.Name}>" :
                                                $"<{node.Name} {string.Join(' ', node.Attributes.Select(att => $"{att.Name}=\"{att.Value}\""))}>";
        }
    }
}
