﻿using Microsoft.AspNetCore.Http;

namespace Tesseract.Web.Extensions
{
    public static class HttpRequestExtensions
    {
        public static string GetHostUrl(this HttpRequest request) => $"{request.Scheme}://{request.Host}";
    }
}
