﻿using Microsoft.AspNetCore.Http;
using Serilog.Context;
using System.Security.Claims;
using System.Threading.Tasks;
using Tesseract.Web.Common;

namespace Tesseract.Web.MiddleWare
{
    public class EnrichLogContextMiddleware
    {
        private readonly RequestDelegate next;

        public EnrichLogContextMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public Task Invoke(HttpContext context)
        {
            LogContext.PushProperty("UserId", context.User.FindFirstValue(ClaimTypes.Name));
            LogContext.PushProperty("DistrictDomain", context.User.FindFirstValue(AppConstants.Security.ClaimTypes.DistrictDomain));
            LogContext.PushProperty("SchoolId", context.User.FindFirstValue(AppConstants.Security.ClaimTypes.ActiveSchoolId));

            return next(context);
        }
    }
}