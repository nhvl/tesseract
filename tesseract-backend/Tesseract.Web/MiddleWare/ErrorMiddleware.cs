﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Tesseract.Core.Exceptions;
using Tesseract.Web.Common;
using Tesseract.Web.Exceptions;

namespace Tesseract.Web.MiddleWare
{
    public class ErrorMiddleware
    {
        private readonly RequestDelegate  _nextMiddleware;
        private readonly JsonSerializerSettings _jsonSerializerSettings;
        private readonly ILogger<ErrorMiddleware> _logger;
        private readonly IHostingEnvironment _env;

        public ErrorMiddleware(RequestDelegate nextMiddleware, ILogger<ErrorMiddleware> logger, IOptions<MvcJsonOptions> options, IHostingEnvironment env)
        {
            _jsonSerializerSettings = options.Value.SerializerSettings;
            _logger = logger;
            _nextMiddleware = nextMiddleware;
            _env = env;
        }

        public async Task Invoke(HttpContext httpContext)
        {

            try
            {
                await _nextMiddleware.Invoke(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                httpContext.Response.ContentType = "application/json";
                httpContext.Response.StatusCode = GetStatusCode(ex);

                string errorMessage = "System Error";
                if (ex is IUserFriendlyException friendlyEx)
                {
                    errorMessage = friendlyEx.FriendlyMessage;
                }

                var errResult = ResponseHelper.GetResponseObject(_env.IsDevelopment() ? ex : errorMessage as object, httpContext.Response.StatusCode);

                await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(errResult, _jsonSerializerSettings));
            }
        }

        private static int GetStatusCode(Exception ex) 
        {
            switch (ex)
            {
                case ResourceNotFoundException _: return (int)HttpStatusCode.NotFound;
                case ConflictResourceException _: return (int)HttpStatusCode.Conflict;
                case InvalidRequestException _: return (int)HttpStatusCode.BadRequest;
                default: return (int)HttpStatusCode.InternalServerError;
            }
        }
    }
}
