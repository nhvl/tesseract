﻿using System;
using System.Runtime.Serialization;

namespace Tesseract.Web.Exceptions
{
    [Serializable]
    public class UnsupportedMediaTypeException : Exception, IUserFriendlyException
    {

        public UnsupportedMediaTypeException()
        {
        }

        public UnsupportedMediaTypeException(string message) : base(message)
        {
            this.FriendlyMessage = message;
        }

        public UnsupportedMediaTypeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnsupportedMediaTypeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public string FriendlyMessage { get; } =  "Image type is not supported by browser"; // TODO: localizatoin
    }
}