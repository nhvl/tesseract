﻿namespace Tesseract.Web.Exceptions
{
    internal interface IUserFriendlyException
    {
        string FriendlyMessage { get;}
    }
}