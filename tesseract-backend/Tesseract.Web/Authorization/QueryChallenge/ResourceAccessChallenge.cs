﻿using Tesseract.Core.Services.Interface.Authorization;

namespace Tesseract.Web.Authorization.QueryChallenge
{
    public class ResourceAccessChallenge
    {
        public ResourceAccessChallenge(EntityIndex<long> identityIndex, EntityIndex<string> resourceIndex)
        {
            ResourceIndex = resourceIndex;
            IdentityIndex = identityIndex;
        }

        public EntityIndex<string> ResourceIndex { get; set; }
        public EntityIndex<long> IdentityIndex { get; set; }

        public override string ToString() => $"{IdentityIndex} --> {ResourceIndex}";
    }
}