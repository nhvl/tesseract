﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tesseract.Common.Extensions;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Web.Authorization.QueryChallenge
{
    public class UrlQueryNode
    {
        internal static Dictionary<string, Type> IdentityTypeMapper = new Dictionary<string, Type>
        {
            ["faculty"] = typeof(Faculty),
            ["student"] = typeof(Student),
        };

        internal static Dictionary<string, Type> ResouceTypeMapper = new Dictionary<string, Type>
        {
            ["class"] = typeof(Class),
            ["activity"] = typeof(Activity),
            ["school"] = typeof(School),
            ["gradingterm"] = typeof(GradingTerm),
        }.Concat(IdentityTypeMapper).ToDictionary(x => x.Key, y=> y.Value);

        public UrlQueryNode(string node, string key)
        {
            Node = node;
            Key = key;
            Is = new UrlQueryNodeChecking(this);
        }

        public string Node { get; set; }
        public string Key { get; set; }

        public UrlQueryNodeChecking Is;

        public Type GetNodeType()
        {
            if (ResouceTypeMapper.TryGetValue(Node, out Type resource)) return resource;

            return typeof(object);
        }
        public static UrlQueryNode[] BuildQueryTree(string urlPath)
        {
            var components = (urlPath ?? "")
                .Split("/")
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.Trim().ToLower())
                .ToArray();

            var count = components.Count();
            var list = new List<UrlQueryNode>();
            int i = 0;
            while (i < count)
            {
                var node = components[i];
                i += 1;
                if (node.IsLong()) continue;
                var nextNode = i < count ? components[i] : null;

                if (nextNode?.IsLong() == true)
                {
                    list.Add(new UrlQueryNode(node, nextNode));
                    i += 1;
                    continue;
                }

                list.Add(new UrlQueryNode(node, null));
            }

            return list.ToArray();
        }
    }

    public class UrlQueryNodeChecking
    {

        private readonly UrlQueryNode Owner;

        public UrlQueryNodeChecking(UrlQueryNode owner) => Owner = owner;

        public bool HavingKey() => !string.IsNullOrWhiteSpace(Owner.Key);
        public bool Identity() => HavingKey() && UrlQueryNode.IdentityTypeMapper.ContainsKey(Owner.Node);
        public bool Resource() => HavingKey() && UrlQueryNode.ResouceTypeMapper.ContainsKey(Owner.Node);
    }
}