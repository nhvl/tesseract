﻿using System;
using System.Linq;
using Tesseract.Common.Extensions;
using Tesseract.Core.Services.Interface.Authorization;

namespace Tesseract.Web.Authorization.QueryChallenge
{
    public static class ResourceChallengeHelper
    {
        public static ResourceAccessChallenge GetAccessChallenge(string requestPath)
        {
            var queryTree = UrlQueryNode.BuildQueryTree(requestPath);
            if (queryTree.Length < 2) return null;

            var entityNode = queryTree.FirstOrDefault(x => x.Is.Identity());
            var resourceNode = queryTree.LastOrDefault(x => x.Is.Resource());

            if (entityNode == null || resourceNode == null || entityNode == resourceNode)return null;

            return new ResourceAccessChallenge(
                    new EntityIndex<long>(entityNode.GetNodeType(), entityNode.Key.ToLong() ?? throw new InvalidOperationException()), 
                    new EntityIndex<string>(resourceNode.GetNodeType(), resourceNode.Key)
                );
        }
    }
}