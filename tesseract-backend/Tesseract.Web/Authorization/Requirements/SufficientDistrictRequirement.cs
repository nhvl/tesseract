﻿// Copyright (c) Denovu. All rights reserved.

namespace Tesseract.Web.Authorization
{
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// Defines the <see cref="SufficientDistrictRequirement" />.
    /// </summary>
    public class SufficientDistrictRequirement : IAuthorizationRequirement
    {
    }
}
