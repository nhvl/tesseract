﻿// Copyright (c) Denovu. All rights reserved.

namespace Tesseract.Web.Authorization
{
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Tesseract.Web.Common;
    using Tesseract.Web.Extensions;

    /// <summary>
    /// Defines the <see cref="SufficientDistrictHandler"/>.
    /// </summary>
    public class SufficientDistrictHandler : AuthorizationHandler<SufficientDistrictRequirement>
    {
        private readonly IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="SufficientDistrictHandler"/> class.
        /// </summary>
        /// <param name="contextAccessor">The contextAccessor<see cref="IHttpContextAccessor"/>.</param>
        public SufficientDistrictHandler(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        /// <summary>
        /// The HandleRequirementAsync.
        /// </summary>
        /// <param name="context">The context<see cref="AuthorizationHandlerContext"/>.</param>
        /// <param name="requirement">The requirement<see cref="SufficientDistrictRequirement"/>.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, SufficientDistrictRequirement requirement)
        {
            var request = _contextAccessor.HttpContext.Request;
            var config = _contextAccessor.HttpContext.RequestServices.GetService<IConfiguration>();
            var customBindingUsed = config.GetCustomBinder() != null;

            if (customBindingUsed)
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            var districtDomainClaim = context.User.Claims.FirstOrDefault(c => c.Type == AppConstants.Security.ClaimTypes.DistrictDomain);
            if (districtDomainClaim != null)
            {
                var districtDomain = _contextAccessor.HttpContext.GetDistrictDomain();
                if (string.Equals(districtDomainClaim.Value, districtDomain, System.StringComparison.OrdinalIgnoreCase))
                {
                    context.Succeed(requirement);
                }
            }

            return Task.CompletedTask;
        }
    }
}
