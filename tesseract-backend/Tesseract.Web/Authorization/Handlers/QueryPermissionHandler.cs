﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Tesseract.Core.Services.Interface.Authorization;
using Tesseract.Web.Authorization.QueryChallenge;

namespace Tesseract.Web.Authorization
{
    public class QueryPermissionHandler : AuthorizationHandler<QueryPermissionRequirement>
    {
        private readonly IHttpContextAccessor _contextAccessor;

        public QueryPermissionHandler(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, QueryPermissionRequirement requirement)
        {
            var request = _contextAccessor.HttpContext.Request;
            var logger = _contextAccessor.HttpContext.RequestServices.GetService<ILogger<QueryPermissionHandler>>();

            var challenge = ResourceChallengeHelper.GetAccessChallenge(request.Path);
            if (challenge == null)
            {
                logger.LogWarning($"{request.Method} {request.Path} can not resolve to challenge.");
                context.Succeed(requirement);
                return;
            } 

            var provider = _contextAccessor.HttpContext.RequestServices.GetService<IResouceAccessHandler>();
            try
            {
                if (!await provider.Identity(challenge.IdentityIndex).CanAccess(challenge.ResourceIndex))
                {
                    logger.LogInformation($"Failed to authorize access:  {challenge}.");
                    return;
                }
            }
            catch (AccessHandlerNotFoundException ex)
            {
                logger.LogWarning(ex, $"{request.Method} {request.Path} can not resolve to handler for: {challenge}. Please update your code.");
            }

            context.Succeed(requirement);
        }
    }
}

