﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;
using Tesseract.Web.Common;

namespace Tesseract.Web.Authorization
{
    public class MatchedUserClaimsHandler : IAuthorizationHandler
    {
        public Task HandleAsync(AuthorizationHandlerContext context)
        {
            var pendingRequirements = context.PendingRequirements.ToList();
            var request = _contextAccessor.HttpContext.Request;

            foreach (var requirement in pendingRequirements)
            {
                if (requirement is MatchedUserClaimsRequirement && IsValidUserClaim(context, request))
                {
                    context.Succeed(requirement);
                }
                else if (requirement is MatchedSchoolClaimsRequirement && IsValidSchoolIdClaim(context, request))
                {
                    context.Succeed(requirement);
                }
            }

            return Task.CompletedTask;
        }
    

    private readonly IHttpContextAccessor _contextAccessor;

        public MatchedUserClaimsHandler(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        private static readonly Dictionary<string, string> _roleClaimDict = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            [ApplicationRoleValue.Faculty] = AppConstants.Security.ClaimTypes.FacultyId,
            [ApplicationRoleValue.Student] = AppConstants.Security.ClaimTypes.StudentId,
            [ApplicationRoleValue.Parent] = AppConstants.Security.ClaimTypes.ParentId,
        };

        private static string _roleRegexPattern = string.Join("|", _roleClaimDict.Keys);

        private bool  IsValidSchoolIdClaim(AuthorizationHandlerContext context, HttpRequest request)
        {
            var schoolIdClaim = context.User.Claims.FirstOrDefault(c => c.Type == AppConstants.Security.ClaimTypes.ActiveSchoolId);
            if (schoolIdClaim == null) return false;

            return Regex.IsMatch(request.Path, $"^/{AppConstants.AppName}/v[0-9]+/({_roleRegexPattern})/\\d+/School/{schoolIdClaim.Value}", RegexOptions.IgnoreCase);
        }

        private bool  IsValidUserClaim(AuthorizationHandlerContext context, HttpRequest request)
        {
            var regexMatch = Regex.Match(request.Path, $"^/{AppConstants.AppName}/v[0-9]+/(?<role>{_roleRegexPattern})/(?<id>\\d+)/", RegexOptions.IgnoreCase);
            if (regexMatch != Match.Empty)
            {
                var urlSchoolId = regexMatch.Groups["id"].Value;
                var schoolIdClaimType = _roleClaimDict[regexMatch.Groups["role"].Value];

                return urlSchoolId == context.User.Claims.FirstOrDefault(c => c.Type == schoolIdClaimType)?.Value;
            }

            var ignoredUserClaim = Regex.IsMatch(request.Path, $"^/{AppConstants.AppName}/v[0-9]+/(account|thread|admin|schooladmin|image|config|localization|test)/", RegexOptions.IgnoreCase);
            if (ignoredUserClaim) return true;

            return false;
        }
    }
}
