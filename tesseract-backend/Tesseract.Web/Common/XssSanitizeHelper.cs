﻿using HtmlAgilityPack;
using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using Tesseract.Web.Extensions;

namespace Tesseract.Web.Common
{
    public static class XssSanitizeHelper
    {
        public static string XssSanitize(string value)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(value);
            return GetSanitizeValue(doc.DocumentNode);
        }

        private static string GetSanitizeValue(HtmlNode node)
        {
            var outputBuilder = new StringBuilder();
            foreach (var childNode in node.ChildNodes)
            {
                if (childNode.NodeType == HtmlNodeType.Text)
                {
                    outputBuilder.Append(childNode.InnerText);
                }
                else if (TryLoadWhitelistNode(childNode, out HtmlNode whitelistNode))
                {
                    outputBuilder.Append(whitelistNode.GetOpeningTag() + GetSanitizeValue(whitelistNode) + whitelistNode.GetClosingTag());
                }
                else
                {
                    outputBuilder.Append(GetSanitizeValue(childNode));
                }
            }

            return outputBuilder.ToString();
        }

        private static bool TryLoadWhitelistNode(HtmlNode node, out HtmlNode whitelistNode) {
            whitelistNode = null;
            var result =  WhitelistTags.Contains(node.Name);
            if (result)
            {
                whitelistNode = node.Clone();

                var invalidAttributes = whitelistNode.Attributes.Where(attr => !WhitelistAttributes.Contains(attr.Name)).ToList();
                foreach(var att in invalidAttributes)
                {
                    whitelistNode.Attributes.Remove(att);
                }

                var hrefAttr = whitelistNode.Attributes[HrefAttribute];
                if (hrefAttr != null)
                {
                    hrefAttr.Value = WebUtility.UrlEncode(hrefAttr.Value);
                }
            }

            return result;
        }

        public static string SanitizeUrl(string url)
        {
            return Regex.Replace(url, @"[^-A-Za-z0-9+&@#/%?=~_|!:,.;\(\)]", "");
        }


        const string HrefAttribute = "href";
        private static string[] WhitelistTags = new string[] { "a", "ul", "li", "i", "u", "b", "em", "p", "strong", "h1", "h2", "h3", "h4", "h5", "h6", "h7" };
        private static string[] WhitelistAttributes = new string[] { HrefAttribute, "target" };
    }
}