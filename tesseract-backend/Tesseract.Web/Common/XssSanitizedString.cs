﻿using HtmlAgilityPack;
using System;
using System.Linq;
using System.Text;

namespace Tesseract.Web.Common
{
    public class XssSanitizedString : IEquatable<XssSanitizedString>
    {
        private readonly string _value;

        public XssSanitizedString(string value)
        {
            _value = XssSanitizeHelper.XssSanitize(value ?? string.Empty);
        }

        public override string ToString() => _value ?? string.Empty;

        public bool Equals(XssSanitizedString other) => _value?.Equals(other.ToString()) ?? false;

        public static implicit operator XssSanitizedString(string value) => new XssSanitizedString(value);
    }
}