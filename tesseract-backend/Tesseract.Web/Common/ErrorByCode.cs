﻿using System;

namespace Tesseract.Web.Common
{
    public class ErrorByCode
    {
        public object Errors { get; set; }
        public string ErrorCode { get; }
        public string FriendlyMessage { get; }

        public static ErrorByCode EmailNotConfirm = new ErrorByCode(nameof(EmailNotConfirm), $"api.login.error.{nameof(EmailNotConfirm)}");
        public static ErrorByCode UserIsLockedOut = new ErrorByCode(nameof(UserIsLockedOut), $"api.login.error.{nameof(UserIsLockedOut)}");
        public static ErrorByCode UserIsNotAllowed = new ErrorByCode(nameof(UserIsNotAllowed), $"api.login.error.{nameof(UserIsNotAllowed)}");
        public static ErrorByCode LoginRequiresTwoFactor = new ErrorByCode(nameof(LoginRequiresTwoFactor), $"api.login.error.{nameof(LoginRequiresTwoFactor)}");
        public static ErrorByCode LoginInvalid = new ErrorByCode(nameof(LoginInvalid), $"api.login.error.{nameof(LoginInvalid)}");

        public static ErrorByCode StudentNotFound = new ErrorByCode(nameof(StudentNotFound), $"api.student.error.{nameof(StudentNotFound)}");
        public static ErrorByCode StudentNumberNotFound = new ErrorByCode(nameof(StudentNumberNotFound), $"api.student.error.{nameof(StudentNumberNotFound)}");
        public static ErrorByCode StudentPhoneNumberNotFound = new ErrorByCode(nameof(StudentPhoneNumberNotFound), $"api.student.error.{nameof(StudentPhoneNumberNotFound)}");
        public static ErrorByCode BatchUpdateStudentError = new ErrorByCode(nameof(BatchUpdateStudentError), $"api.student.error.{nameof(BatchUpdateStudentError)}");

        public static ErrorByCode SchoolIsRequired = new ErrorByCode(nameof(SchoolIsRequired), $"api.school.error.{nameof(SchoolIsRequired)}");

        public static ErrorByCode DuplicateUserName = new ErrorByCode(nameof(DuplicateUserName), $"api.user.error.{nameof(DuplicateUserName)}");
        public static ErrorByCode DuplicateEmail = new ErrorByCode(nameof(DuplicateEmail), $"api.user.error.{nameof(DuplicateEmail)}");
        private ErrorByCode(string errorCode, string friendlyMessage)
        {
            ErrorCode = errorCode ?? throw new ArgumentNullException(nameof(errorCode));
            FriendlyMessage = friendlyMessage;
        }
    }
}
