﻿namespace Tesseract.Web.Common
{
    public static class ResponseHelper
    {
        public static object GetResponseObject(object resultObject, int httpStatusCode)
        {
            var isSuccess = 200 <= httpStatusCode && httpStatusCode < 300;

            if (isSuccess)
            {
                return new { data = resultObject, isSuccess };
            }

            return new { error = new ErrorDetail(resultObject, httpStatusCode), isSuccess };
        }
    }
}
