﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Microsoft.AspNetCore.Http;

namespace Tesseract.Web.Common
{
    public static class CsvReadHelper
    {
        public static IEnumerable<T> ReaderFromFile<T>(IFormFile file)
        {
            using (var stream = file.OpenReadStream())
            {
                using (var textReader = new StreamReader(stream))
                {
                    using (var csv = new CsvReader(textReader))
                    {
                        // TODO validate record input
                        var result = csv.GetRecords<T>().ToList();
                        foreach(var item in result){
                            yield return item;
                        }
                    }
                }
            }
        }
        public static IEnumerable<T> ReaderFromFile<T>(string physicalPath)
        {
            using (var textReader = new StreamReader(physicalPath))
            {
                using (var csv = new CsvReader(textReader))
                {
                    // TODO validate record input
                    var result = csv.GetRecords<T>().ToList();
                    foreach (var item in result)
                    {
                        yield return item;
                    }
                }
            }
        }
    }
}
