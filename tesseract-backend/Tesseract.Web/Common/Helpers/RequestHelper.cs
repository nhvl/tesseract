﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using Serilog;
namespace Tesseract.Web.Common
{
    public static class RequestHelper
    {
        public static string GetFileName(IFormFile file)
        {
            return ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Replace(" ", "").Trim('"');
        }

        public static bool IsLocal(HttpRequest request)
        {
            return !request.Host.HasValue || request.Host.Value.Contains(AppConstants.Path.Localhost, StringComparison.InvariantCultureIgnoreCase);
        }

        public static JObject GetRequestBody(HttpRequest request)
        {
            try
            {
                var bodyStr = "";

                // Allows using several time the stream in ASP.Net Core
                request.EnableRewind();

                // Arguments: Stream, Encoding, detect encoding, buffer size 
                // AND, the most important: keep stream opened
                using (StreamReader reader = new StreamReader(request.Body, Encoding.UTF8, true, 1024, true))
                {
                    bodyStr = reader.ReadToEnd();
                }

                // Rewind, so the core is not lost when it looks the body for the request
                request.Body.Position = 0;

                return string.IsNullOrWhiteSpace(bodyStr)
                    ? null
                    : Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(bodyStr);
            }
            catch (Exception ex)
            {
                Log.Logger?.Error(ex, nameof(GetRequestBody) + " failed.");
                return null;
            }
        }
    }
}