﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Enums;
using Tesseract.Web.Extensions;

namespace Tesseract.Web.Common
{
    public static class MediaUrlHepler
    {
        public static readonly Func<Uri, Task<MediaInfo>>[] MediaInfoHandlers;

        static MediaUrlHepler()
        {
            MediaInfoHandlers = new Func<Uri, Task<MediaInfo>>[] { GetMediaFromSupportSide, GetMediaFromNameExtension, GetMediaFromHttpClient };
        }

        public static async Task<MediaInfo> GetMediaInfo(string url)
        {
            if (!Uri.TryCreate(url, UriKind.Absolute, out var uriResult))
            {
                throw new ArgumentException("Invalid url.");
            }

            var validUrl = new UriBuilder(url) { Scheme = Uri.UriSchemeHttps, Port = -1 }.Uri;

            foreach (var handler in MediaInfoHandlers)
            {
                var info = await handler(validUrl);
                if (info != null)
                {
                    return info;
                }
            }

            return null;
        }

        private static async Task<MediaInfo> GetMediaFromSupportSide(Uri url)
        {
            MediaInfo mediaInfo = null;
            if (url.IsYoutubeLink())
            {
                mediaInfo = new MediaInfo { Link = url.AbsoluteUri, MediaType = MediaTypeEnum.VideoLink };
            }
            else if (url.IsVimeoLink())
            {
                mediaInfo = new MediaInfo { Link = url.AbsoluteUri, MediaType = MediaTypeEnum.VideoLink };
            }
            else if (url.IsSoundCloudLink())
            {
                mediaInfo = new MediaInfo { Link = url.AbsoluteUri, MediaType = MediaTypeEnum.AudioLink };
            }

            return await Task.FromResult(mediaInfo);
        }

        private static async Task<MediaInfo> GetMediaFromNameExtension(Uri url)
        {
            MediaInfo mediaInfo = null;
            var extension = "." + url.AbsoluteUri.Split(".").Last();
            if (Mappings.TryGetValue(extension, out var contentType))
            {
                var mediaType = GetMediaFromContentType(contentType);
                if (mediaType != null)
                {
                    mediaInfo = new MediaInfo(url.AbsoluteUri, mediaType.Value, contentType);
                }
            }

            return await Task.FromResult(mediaInfo);
        }

        private static async Task<MediaInfo> GetMediaFromHttpClient(Uri url)
        {
            HttpClient httpClient = new HttpClient();
            foreach (var method in new HttpMethod [] { HttpMethod.Head, HttpMethod.Get})
            {
                HttpRequestMessage request = new HttpRequestMessage(method, url);

                var response = await httpClient.SendAsync(request);
                var contentType = response?.Content?.Headers?.ContentType.MediaType;
                var mediaType = GetMediaFromContentType(contentType);
                if (mediaType != null)
                {
                    return new MediaInfo(url.AbsoluteUri, mediaType.Value, contentType);
                }
            }

            return null;
        }

        private static MediaTypeEnum? GetMediaFromContentType(string contentType)
        {
            if (contentType == null)
            {
                return null;
            }

            if (contentType.StartsWith("audio/"))
            {
                return MediaTypeEnum.AudioLink;
            }
            else if (contentType.StartsWith("image/"))
            {
                return  MediaTypeEnum.ImageLink;
            }
            else if (contentType.StartsWith("video/"))
            {
                return  MediaTypeEnum.VideoLink;
            }

            return null;
        }

        private static Dictionary<string, string> Mappings = new Dictionary<string, string>{
                {".AAC", "audio/aac"},
                {".aiff", "audio/aiff"},
                {".snd", "audio/basic"},
                {".midi", "audio/mid"},
                {".wav", "audio/wav"},
                {".m4a", "audio/x-m4a"},
                {".m3u", "audio/x-mpegurl"},
                {".ra", "audio/x-pn-realaudio"},
                {".smd", "audio/x-smd"},
                {".bmp", "image/bmp"},
                {".jpg", "image/jpeg"},
                {".pic", "image/pict"},
                {".png", "image/png"},
                {".tiff", "image/tiff"},
                {".mac", "image/x-macpaint"},
                {".qti", "image/x-quicktime"},
                {".eml", "message/rfc822"},
                {".html", "text/html"},
                {".txt", "text/plain"},
                {".wsc", "text/scriptlet"},
                {".xml", "text/xml"},
                {".3gp", "video/3gpp"},
                {".3gp2", "video/3gpp2"},
                {".mp4", "video/mp4"},
                {".mpg", "video/mpeg"},
                {".mov", "video/quicktime"},
                {".m2t", "video/vnd.dlna.mpeg-tts"},
                {".dv", "video/x-dv"},
                {".lsf", "video/x-la-asf"},
                {".asf", "video/x-ms-asf"}
        };
    }
}