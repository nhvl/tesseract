﻿namespace Tesseract.Web.Common
{
    public static class AppConstants
    {
        public const string AppName = "Tesseract";
        public static double AppVersion = 1;
        public static string AppVersionString => $"v{AppVersion}";

        public static class Cache
        {
            public const string CacheDefault = "Default";
            public const string ContentTypeKey = "_contenttype";
            public const string StatusCodeKey = "_statuscode";
            public const string DefaultContentType = "application/json";
            public static class Profile
            {
                public const string StaticApi = "StaticApi";
            }
        }

        public static class Path
        {
            public const string Resources = "Resources";
            public const string Images = "Images";
            public const string Localhost = "localhost";
        }

        public static class Binder
        {
            public const string domain = "domain";
            public const string SchoolId = "schoolId";
        }

        public static class Notification
        {
            public const string AccessToken = "access_token";
            public const string HubUrl = "/notificationHub";
            public static class Message
            {
                public const string DiscussionChanged = "discussionChanged";
            }
        }

        public static class Security
        {
            public static class ClaimTypes
            {
                public const string ActiveSchoolId = "ActiveSchoolId";
                public const string FacultyId = "FacultyId";
                public const string StudentId = "StudentId";
                public const string DistrictDomain = "DistrictDomain";
                public const string ParentId = "ParentId";
            }

            public static class Policy
            {
                public const string SchoolAccess = "SchoolResourceAccess";
                public const string GeneralAccess = "GeneralResourceAccess";
                public const string CrossedPortalAccess = "CrossedPortalAccess";
            }
        }
    }
}