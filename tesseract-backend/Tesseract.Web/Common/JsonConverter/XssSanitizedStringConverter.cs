﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;

namespace Tesseract.Web.Common
{
    public class XssSanitizedStringConverter : JsonConverter<XssSanitizedString>
    {
        public override void WriteJson(JsonWriter writer, XssSanitizedString value, JsonSerializer serializer) => writer.WriteValue(value.ToString());
        public override XssSanitizedString ReadJson(JsonReader reader, Type objectType, XssSanitizedString existingValue, bool hasExistingValue, JsonSerializer serializer) => reader.TokenType == JsonToken.String ? new XssSanitizedString((string)reader.Value) : existingValue;
    }
}