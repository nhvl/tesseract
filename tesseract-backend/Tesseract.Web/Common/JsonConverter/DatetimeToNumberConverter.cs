﻿using Newtonsoft.Json;
using System;
using Serilog;

namespace Tesseract.Web.Common
{
    public class DatetimeToNumberConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
            }
            else if (value is DateTime dateValue)
            {
                try
                {
                    var dateOffset = new DateTimeOffset(dateValue, TimeSpan.Zero);
                    if (dateOffset == DateTimeOffset.MinValue)
                    {
                        Log.Debug("Unexpected DateTime minimum value in writing");
                        writer.WriteNull();
                    }
                    else
                    {
                        writer.WriteValue(dateOffset.ToUnixTimeMilliseconds());
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    writer.WriteNull();
                }

            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Integer)
            {
                var dateOffset = DateTimeOffset.FromUnixTimeMilliseconds((long)reader.Value);
                if(dateOffset.UtcDateTime == DateTimeOffset.MinValue)
                {
                    Log.Debug("Unexpected DateTime minimum value in reading");
                    return null;
                }

                return dateOffset.UtcDateTime;
            }
            else if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }

            return existingValue;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime) || objectType == typeof(DateTime?);
        }
    }
}