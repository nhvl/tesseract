﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Enums;

namespace Tesseract.Web.Common
{
    public class ActItemConverter : JsonConverter
    {
        public override bool CanWrite { get; } = false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => throw new NotImplementedException();

        public override bool CanConvert(Type objectType) => objectType == typeof(BaseActItem);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (!CanConvert(objectType)) return null;

            var versioningObject = serializer.Deserialize<JObject>(reader);
            var itemType = versioningObject.GetValue(nameof(BaseActItem.Type))?.Value<int>() ?? throw new ArgumentException($"Required {nameof(BaseActItem.Type)} property.");

            switch ((EActItemType)itemType)
            {
                case EActItemType.NumericQuestion:
                    return versioningObject.ToObject<NumberActItem>(serializer);
                case EActItemType.MatchQuiz:
                    return versioningObject.ToObject<MatchActItem>(serializer);
                case EActItemType.PollQuiz:
                    return versioningObject.ToObject<PollingActItem>(serializer);
                case EActItemType.ChoiceQuiz:
                    return versioningObject.ToObject<ChoiceActItem>(serializer);
                case EActItemType.Media:
                    return versioningObject.ToObject<MediaActItem>(serializer);
                case EActItemType.Embed:
                    return versioningObject.ToObject<EmbedActItem>(serializer);
                case EActItemType.Discussion:
                    return versioningObject.ToObject<DiscussionActItem>(serializer);
                default:
                    return versioningObject.ToObject<DefaultActItem>(serializer);
            }
        }
    }
}
