﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace Tesseract.Web.Common
{
    public class TimeSpanToNumberConverter : JsonConverter<TimeSpan>
    {
        public override TimeSpan ReadJson(JsonReader reader, Type objectType, TimeSpan existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            switch (reader.TokenType)
            {
                case JsonToken.Integer:
                    return TimeSpan.FromMilliseconds((long)reader.Value % (TimeSpan.FromDays(1).TotalMilliseconds));
                case JsonToken.Float:
                    return TimeSpan.FromMilliseconds((long)reader.Value % (24 * 60 * 60 * 1000));
                default:
                    throw new SerializationException($"Invalid TimeSpan {reader.Value}");
            }
        }


        public override void WriteJson(JsonWriter writer, TimeSpan value, JsonSerializer serializer)
        {
            writer.WriteValue(value.TotalMilliseconds);
        }
    }
}
