﻿namespace Tesseract.Web.Common
{
    public class ServiceSettings
    {
        public int ReportRunnerInSeconds { get; set; }
    }
}
