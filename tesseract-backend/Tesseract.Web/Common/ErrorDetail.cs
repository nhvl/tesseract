﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Tesseract.Web.Common
{
    public class ErrorDetail
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public string ErrorCode { get; set; } = "System.Error";
        public object ErrorObject { get; set; }

        public ErrorDetail(object objectResult, int statuscode)
        {
            StatusCode = statuscode;

            switch (objectResult)
            {
                case ErrorByCode e:
                    ErrorCode = e.ErrorCode;
                    this.Message = e.FriendlyMessage;
                    this.ErrorObject = e.Errors;
                    break;
                case SerializableError e:
                    this.ErrorCode = e.GetType().FullName;
                    this.ErrorObject = e;
                    this.Message = "Invalid request";
                    break;
                case IdentityResult e:
                    this.ErrorCode = e.GetType().FullName;
                    this.ErrorObject = e.Errors;
                    break;
                case Google.GoogleApiException e:
                    this.ErrorCode = e.GetType().FullName;
                    this.ErrorObject = e.Error.Errors;
                    break;
                case string errorMessage:
                    this.Message = errorMessage;
                    break;
                case Exception ex:
                    this.Message = "System Error";
                    this.ErrorObject = ex.ToString();
                    break;
                default:
                    this.ErrorCode = objectResult.GetType().FullName;
                    this.ErrorObject = objectResult;
                    break;
            }
        }

        public ErrorDetail()
        {
        }
    }
}
