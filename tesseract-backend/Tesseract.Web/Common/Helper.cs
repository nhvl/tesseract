﻿using System;

namespace Tesseract.Web.Common
{
    public class Helper
    {
        #region Safe type convert
        public static bool SafeBool(object o)
        {
            try
            {
                if (Convert.IsDBNull(o) || o == null)
                    return false;
                return Convert.ToBoolean(o);
            }
            catch
            {
                return false;
            }
        }
        
        public static byte SafeByte(object s)
        {
            byte result;
            byte.TryParse(SafeString(s), out result);
            return result;
        }
        
        public static int SafeInteger(object s)
        {
            int result;
            return int.TryParse(SafeString(s), out result) ? result : 0;
        }

        public static long SafeLong(object s)
        {
            long result;
            return long.TryParse(SafeString(s), out result) ? result : 0;
        }

        public static double SafeDouble(object s)
        {
            double result;
            return double.TryParse(SafeString(s), out result) ? result : 0.0;
        }
        
        public static decimal SafeDecimal(object s)
        {
            decimal result;
            return decimal.TryParse(SafeString(s), out result) ? result : decimal.Zero;
        }
        
        public static string SafeString(object s)
        {
            if (s == null || s is DBNull)
            {
                return "";
            }
            
            return Convert.ToString(s).Trim();
        }
        #endregion
    }
}