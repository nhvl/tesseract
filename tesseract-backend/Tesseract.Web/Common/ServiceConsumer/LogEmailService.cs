﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Config;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Web.Common.ServiceConsumer
{
    public class LogEmailService : IEmailSenderService
    {
        private readonly ILogger<LogEmailService> _logger;
        private readonly SMTPServerSettings _smtpServerSettings;

        public LogEmailService(ILogger<LogEmailService> logger,
            SMTPServerSettings smtpServerSettings) {
            _logger = logger;
            _smtpServerSettings = smtpServerSettings;
        }

        public Task SendEmailAsync(
                                   string subject,
                                   string body,
                                   string fromAddress,
                                   string fromName,
                                   string toAddress,
                                   string toName,
                                   string replyToAddress,
                                   string replyToName,
                                   IEnumerable<string> bcc,
                                   IEnumerable<string> cc,
                                   IEnumerable<string> attachmentFilePaths = null,
                                   string attachmentFileName = null,
                                   int attachedDownloadId = 0,
                                   IDictionary<string, string> headers = null)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"emailAccount: {_smtpServerSettings.Email}");
            builder.AppendLine($"subject: {subject}");
            builder.AppendLine($"body: {body}");
            builder.AppendLine($"fromAddress: {fromAddress}");
            builder.AppendLine($"fromName: {fromName}");
            builder.AppendLine($"toAddress: {toAddress}");
            builder.AppendLine($"toName: {toName}");
            builder.AppendLine($"replyToAddress: {replyToAddress}");
            builder.AppendLine($"replyToName: {replyToName}");
            builder.AppendLine($"bcc: {string.Join(",", bcc?? new[] { "null" })}");
            builder.AppendLine($"cc: {string.Join(",", cc ?? new[] { "null" })}");
            builder.AppendLine($"attachmentFilePath: {string.Join(",", attachmentFilePaths ?? new[] { "null" })}");
            builder.AppendLine($"attachmentFileName: {attachmentFileName}");
            builder.AppendLine($"attachedDownloadId: {attachedDownloadId}");
            builder.AppendLine($"headers: {string.Join(" | ", headers?.Select(kp => $"{kp.Key}: {kp.Value}") ?? new[] { "null" })}");
            _logger.LogInformation(builder.ToString());
            return Task.CompletedTask;
        }
    }
}
