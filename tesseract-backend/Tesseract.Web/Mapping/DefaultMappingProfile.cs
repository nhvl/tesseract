﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Enums;
using Tesseract.Web.Models;

namespace Tesseract.Web.Mapping
{
    public class DefaultMappingProfile : Profile
    {
        public DefaultMappingProfile()
        {
            CreateMapForActItem();
            CreateMapForActDoc();

            CreateMap<District, DistrictViewModel>();

            CreateMap<ApplicationUser, LoggedInUserViewModel>()
                .ForMember(x => x.Token, opt => opt.Ignore());

            CreateMap<ApplicationUser, DetailUserViewModel>()
                .ForMember(x => x.Role, opt => opt.Ignore())
                .ForMember(x => x.Schools, opt => opt.Ignore())
                .ForMember(x => x.SchoolList, opt => opt.Ignore())
                .ForMember(x => x.FacultyId, opt => opt.Ignore())
                .ForMember(x => x.StudentId, opt => opt.Ignore())
                .ForMember(x => x.StudentNumber, opt => opt.Ignore())
                .ForMember(x => x.ParentId, opt => opt.Ignore())
                .ForMember(x => x.Permissions, opt => opt.Ignore())
                ;

            CreateMap<ApplicationUser, UserViewModel>();

            CreateMap<Class, StudentClassViewModel>()
                .ForMember(x => x.GradeRanges, opt => opt.MapFrom(c => c.GradeRanges))
                .ForMember(x => x.Faculties, opt => opt.Ignore())
                .ForMember(x => x.Grade, opt => opt.Ignore())
                .ForMember(x => x.SortIndex, opt => opt.Ignore())
                ;

            CreateMap<Activity, StudyingActivityModelView>()
                .ForMember(x => x.Score, opt => opt.Ignore());

            CreateMap<ActOptionView, ActOption>();
            CreateMap<ActOption, ActOptionView>();

            CreateMap<PollItemView, PollQuizItem>();
            CreateMap<PollQuizItem, PollItemView>();
            CreateMap<PollQuizItem, PollItemPreview>()
                .ForMember(x => x.Votes, opt => opt.Ignore());

            CreateMap<(Faculty faculty, long schoolId, ActivePassiveCount actCount), FacultyDistrictView>()
                .ForMember(x => x.FacultyId    , opt => opt.MapFrom(req => req.faculty.FacultyId))
                .ForMember(x => x.SchoolId     , opt => opt.MapFrom(req => req.schoolId))
                .ForMember(x => x.FirstName    , opt => opt.MapFrom(req => req.faculty.User.FirstName))
                .ForMember(x => x.LastName     , opt => opt.MapFrom(req => req.faculty.User.LastName))
                .ForMember(x => x.Salutation   , opt => opt.MapFrom(req => req.faculty.User.Salutation))
                .ForMember(x => x.Nickname     , opt => opt.MapFrom(req => req.faculty.User.Nickname))
                .ForMember(x => x.ActivityCount, opt => opt.MapFrom(req => req.actCount));

            CreateMap<Student, StudentView>()
                .ForMember(x => x.FirstName       , opt => opt.MapFrom(s => s.User.FirstName ))
                .ForMember(x => x.LastName        , opt => opt.MapFrom(s => s.User.LastName  ))
                .ForMember(x => x.Salutation      , opt => opt.MapFrom(s => s.User.Salutation))
                .ForMember(x => x.Nickname        , opt => opt.MapFrom(s => s.User.Nickname  ))
                .ForMember(x => x.CumulativeGrade , opt => opt.Ignore ())
                .ForMember(x => x.LastAccessed    , opt => opt.Ignore ());

            CreateMap<Student, ParentStudentView>()
                .ForMember(x => x.Activities, opt => opt.Ignore())
                .IncludeBase<Student, StudentView>();

            CreateMap<RegisterViewModel, ApplicationUser>()
                .ForMember(x => x.FirstName  , opt => opt.AllowNull())
                .ForMember(x => x.LastName   , opt => opt.AllowNull())
                .ForMember(x => x.Salutation , opt => opt.AllowNull())
                .ForMember(x => x.Nickname   , opt => opt.AllowNull())
                .ForMember(x => x.Language   , opt => opt.AllowNull())
                .ForMember(x => x.Email      , opt => opt.AllowNull())
                .ForMember(x => x.PhoneNumber, opt => opt.AllowNull())
                .ForMember(x => x.Language   , opt => opt.AllowNull())
                .ForMember(x => x.UserName   , opt => opt.MapFrom(model => model.Email))
                .ForAllOtherMembers(opt => opt.Ignore());
        }

        private void CreateMapForActDoc()
        {

            CreateMap<ActDocRequest, ActDoc>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.DateCreated, opt => opt.Ignore())
                .ForMember(x => x.DateUpdated, opt => opt.Ignore())
                .ForMember(x => x.CreatedBy, opt => opt.Ignore())
                .ForMember(x => x.UpdatedBy, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.ETag, opt => opt.Ignore());


            CreateMap<ActDoc, ActDocView>();
            CreateMap<ActDoc, ActDocPreview>();

            Action<StudentActDoc, IEnumerable<StudentActItemRequest>> UpdateActDocItem = (src, items) =>
            {
                foreach (var item in items)
                {
                    item.FromTeacher = item.Id != null && (src.Answers?.Keys?.Contains(item.Id) ?? false);

                    if (item.FromTeacher)
                    {
                        var answerJsonString = src.Answers[item.Id];
                        if (string.IsNullOrEmpty(answerJsonString)) continue;
                        switch (item.Type)
                        {
                            case Core.Enums.EActItemType.PollQuiz:
                                item.PollItemsAnswer = JsonConvert.DeserializeObject<int>(answerJsonString);
                                break;
                            case Core.Enums.EActItemType.NumericQuestion:
                                item.NumberAnswer = JsonConvert.DeserializeObject<int>(answerJsonString);
                                break;
                            case Core.Enums.EActItemType.TextQuiz:
                                item.TextAnswer = JsonConvert.DeserializeObject<string>(answerJsonString);
                                break;
                            case Core.Enums.EActItemType.ChoiceQuiz:
                                item.OptionsAnswer = JsonConvert.DeserializeObject<ActOption[]>(answerJsonString);
                                break;
                            case Core.Enums.EActItemType.MatchQuiz:
                                item.MatchQuizzAnswer = JsonConvert.DeserializeObject<MatchQuizzItem[]>(answerJsonString);
                                break;
                            default:
                                continue;
                        }
                    }
                }
            };

            CreateMap<StudentActDoc, StudentActDocView>().AfterMap((src, des) => {
                if (des.Items == null) return;
                UpdateActDocItem(src, des.Items);
            });

            CreateMap<StudentActDoc, StudentActDocReview>().AfterMap((src, des) => {
                if (des.Items == null) return;
                UpdateActDocItem(src, des.Items);
            });

            CreateMap<StudentActDocRequest, StudentActDoc>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.DateCreated, opt => opt.Ignore())
                .ForMember(x => x.DateUpdated, opt => opt.Ignore())
                .ForMember(x => x.CreatedBy, opt => opt.Ignore())
                .ForMember(x => x.UpdatedBy, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore())
                .ForMember(x => x.ETag, opt => opt.Ignore())
                .ForMember(x => x.StudentId, opt => opt.Ignore())
                .ForMember(x => x.Answers, opt => opt.MapFrom(src => src.Items.ToDictionary(x => x.Id, y => y.TextAnswer)));
        }

        private void CreateMapForActItem()
        {
            Func<ActItemBaseModel, BaseActItem> ActItemConvertFunc = src => {
                BaseActItem result;
                switch (src.Type)
                {
                    case EActItemType.Media:
                        result = new MediaActItem { MediaInfo = new MediaInfo(src.MediaInfoLink, src.MediaInfoMediaType, src.MediaInfoContentType) };
                        break;
                    case EActItemType.Embed:
                        result = new EmbedActItem { EmbedInfo = new EmbedInfo(src.EmbedInfoLink, src.EmbedApp, src.EmbedId, src.EmbedInfoContentType, src.EmbedType, src.LastEditedUtc) };
                        break;
                    case EActItemType.ChoiceQuiz:
                        result = new ChoiceActItem {
                            IsMultipleAnswer = src.IsMultipleAnswer,
                            Options = src.Options?.Select(o => new ActOption { Label = o.Label?.ToString(), Image = o.Image, IsCorrect = o.IsCorrect }).ToArray() };
                        break;
                    case EActItemType.MatchQuiz:
                        result = new MatchActItem { MatchQuizzItems = src.matchQuizzItems.Select(m => new MatchQuizzItem { Left = m.Left, LeftImage = m.LeftImage, Right = m.Right, RightImage = m.RightImage }).ToArray() };
                        break;
                    case EActItemType.PollQuiz:
                        result = new PollingActItem { PollItems = src.PollItems.Select(p => new PollQuizItem { Label = p.Label.ToString(), Image = p.Image }).ToArray() };
                        break;
                    case EActItemType.NumericQuestion:
                        result = new NumberActItem { };
                        break;
                    case EActItemType.Discussion:
                        result = new DiscussionActItem { DiscussionId = src.DiscussionId ?? -1 };
                        break;
                    default:
                        result = new DefaultActItem { };
                        break;
                }

                result.Id = src.Id;
                result.Title = src.Title?.ToString();
                result.Image = src.Image;
                result.Content = src.Content?.ToString();
                result.Type = src.Type;
                return result;
            };

            CreateMap<ActItemBaseModel, BaseActItem>()
                .ConvertUsing(src => ActItemConvertFunc(src));

            CreateMap<ActItemRequest, BaseActItem>()
                .ConstructUsing(src => ActItemConvertFunc(src))
                .ForMember(item => item.FormatVersion, opt => opt.Ignore())
                .AfterMap( (request, item) => {
                    if (item.Type == EActItemType.NumericQuestion)
                    {
                        var numberItem = item as NumberActItem;
                        if (numberItem == null) return;
                        numberItem.NumericMax = request.NumericMax;
                        numberItem.NumericMin = request.NumericMin;
                    }
                });

            CreateMap<BaseActItem, ActItemPreview>()
                .IncludeBase<BaseActItem, ActItemBaseModel>()
                .ForMember(x => x.IsMultipleAnswer, o => o.MapFrom((item, _) => (item as ChoiceActItem)?.IsMultipleAnswer ?? true))
                .ForMember(x => x.Options, o => o.MapFrom((item, _) => {
                    var options = (item as ChoiceActItem)?.Options;
                    if (options == null) return new ActOption[0];

                    foreach (var opt in options)
                    {
                        opt.IsCorrect = false;
                    }

                    return options;
                }))
                .ForMember(x => x.PollItems, o => o.MapFrom((item, _) => {
                    var options = (item as PollingActItem)?.PollItems;
                    if (options == null) return new PollQuizItem[0];
                    return options;
                }))
                .ForMember(x => x.NumericMin, o => o.MapFrom((item, _) => (item as NumberActItem)?.NumericMin))
                .ForMember(x => x.NumericMax, o => o.MapFrom((item, _) => (item as NumberActItem)?.NumericMax))
                .ForMember(x => x.DiscussionId, o => o.MapFrom((item, _) => (item as DiscussionActItem)?.DiscussionId))
                .ForMember(x => x.matchQuizzItems, o => o.MapFrom((item, _) => (item as MatchActItem)?.MatchQuizzItems ?? new MatchQuizzItem[0]));

            CreateMap<BaseActItem, ActItemBaseModel>()
                .ForMember(x => x.IsMultipleAnswer, o => o.MapFrom((item, _) => (item as ChoiceActItem)?.IsMultipleAnswer ?? true))
                .ForMember(x => x.Options, o => o.MapFrom((item, _) => (item as ChoiceActItem)?.Options ?? new ActOption[0]))
                .ForMember(x => x.PollItems, o => o.MapFrom((item, _) => (item as PollingActItem)?.PollItems ?? new PollQuizItem[0]))
                .ForMember(x => x.MediaInfoLink, o => o.MapFrom((item, _) => (item as MediaActItem)?.MediaInfo?.Link))
                .ForMember(x => x.MediaInfoContentType, o => o.MapFrom((item, _) => (item as MediaActItem)?.MediaInfo?.ContentType))
                .ForMember(x => x.EmbedInfoLink, o => o.MapFrom((item, _) => (item as EmbedActItem)?.EmbedInfo?.Link))
                .ForMember(x => x.EmbedInfoContentType, o => o.MapFrom((item, _) => (item as EmbedActItem)?.EmbedInfo?.ContentType))
                .ForMember(x => x.EmbedId, o => o.MapFrom((item, _) => (item as EmbedActItem)?.EmbedInfo?.EmbedId))
                .ForMember(x => x.EmbedApp, o => o.MapFrom((item, _) => (item as EmbedActItem)?.EmbedInfo?.EmbedApp))
                .ForMember(x => x.EmbedType, o => o.MapFrom((item, _) => (item as EmbedActItem)?.EmbedInfo?.Type))
                .ForMember(x => x.LastEditedUtc, o => o.MapFrom((item, _) => (item as EmbedActItem)?.EmbedInfo?.LastEditedUtc))
                .ForMember(x => x.MediaInfoMediaType, o => o.MapFrom((item, _) => (item as MediaActItem)?.MediaInfo?.MediaType ?? MediaTypeEnum.AudioFile))
                .ForMember(x => x.matchQuizzItems, o => o.MapFrom((item, _) => (item as MatchActItem)?.MatchQuizzItems ?? new MatchQuizzItem[0]))
                .ForMember(x => x.NumericMin, o => o.MapFrom((item, _) => (item as NumberActItem)?.NumericMin))
                .ForMember(x => x.NumericMax, o => o.MapFrom((item, _) => (item as NumberActItem)?.NumericMax))
                .ForMember(x => x.DiscussionId, o => o.MapFrom((item, _) => (item as DiscussionActItem)?.DiscussionId))
                ;

            CreateMap<BaseActItem, ActItemRequest>()
                .IncludeBase<BaseActItem, ActItemBaseModel>()
                .ForMember(x => x.IsMultipleAnswer, o => o.MapFrom((item, _) => (item as ChoiceActItem)?.IsMultipleAnswer ?? true))
                .ForMember(x => x.NumericMin, o => o.MapFrom((item, _) => (item as NumberActItem)?.NumericMin))
                .ForMember(x => x.NumericMax, o => o.MapFrom((item, _) => (item as NumberActItem)?.NumericMax))
                ;

            CreateMap<BaseActItem, StudentActItemRequest>()
                .IncludeBase<BaseActItem, ActItemBaseModel>()
                .ForMember(x => x.FromTeacher, opt => opt.Ignore())
                .ForMember(x => x.TextAnswer, opt => opt.Ignore())
                .ForMember(x => x.MatchQuizzAnswer, opt => opt.Ignore())
                .ForMember(x => x.OptionsAnswer, opt => opt.Ignore())
                .ForMember(x => x.PollItemsAnswer, opt => opt.Ignore())
                .ForMember(x => x.NumberAnswer, opt => opt.Ignore())
                .ForMember(x => x.matchQuizzItems, o => o.MapFrom((item, _) => (item as MatchActItem)?.MatchQuizzItems ?? new MatchQuizzItem[0]));

            CreateMap<BaseActItem, StudentActItemReview>()
                .IncludeBase<BaseActItem, StudentActItemRequest>()
                .ForMember(x => x.NumericMin, o => o.MapFrom((item, _) => (item as NumberActItem)?.NumericMin))
                .ForMember(x => x.NumericMax, o => o.MapFrom((item, _) => (item as NumberActItem)?.NumericMax))
                ;

            CreateMap<StudentActItemRequest, BaseActItem>()
            .ConstructUsing(src => ActItemConvertFunc(src))
            .ForMember(item => item.StartDate, opt => opt.Ignore())
            .ForMember(item => item.EndDate, opt => opt.Ignore())
            .ForMember(item => item.FormatVersion, opt => opt.Ignore());
        }
    }
}
