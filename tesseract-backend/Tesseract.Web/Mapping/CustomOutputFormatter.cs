﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json;
using System.Buffers;
using System.Text;
using System.Threading.Tasks;
using Tesseract.Web.Common;

namespace Tesseract.Web.Mapping
{
    public class CustomOutputFormatter : JsonOutputFormatter
    {
        public CustomOutputFormatter(JsonSerializerSettings serializerSettings, ArrayPool<char> charPool):base(serializerSettings, charPool)
        {
        }

        public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
        {
            var response = context.HttpContext.Response;
            await response.WriteAsync(JsonConvert.SerializeObject(ResponseHelper.GetResponseObject(context.Object, response.StatusCode), SerializerSettings));
        }
    }
}
