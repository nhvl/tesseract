﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using Tesseract.Web.Common;

namespace Tesseract.Web.Binders
{
    public class TesseractBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            var meta = context?.Metadata ?? throw new ArgumentNullException(nameof(context));

            if (meta.Name == AppConstants.Binder.domain && meta.ModelType == typeof(string))
            {
                return new DistrictValueBinder();
            }

            return null;
        }
    }
}
