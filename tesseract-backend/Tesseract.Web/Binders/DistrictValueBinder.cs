﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Threading.Tasks;
using Tesseract.Web.Common;
using Tesseract.Web.Extensions;

namespace Tesseract.Web.Binders
{
    public class DistrictValueBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var districtDomain = bindingContext?.HttpContext?.GetDistrictDomain() ?? throw new ArgumentNullException(nameof(bindingContext));
            bindingContext.Result = !string.IsNullOrEmpty(districtDomain) ? ModelBindingResult.Success(districtDomain) : throw new ArgumentException();
            return Task.CompletedTask;
        }
    }
}
