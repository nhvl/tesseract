﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Web.Common;

namespace Tesseract.Web.Binders
{
    public class PreconfigBinderProvider : IModelBinderProvider
    {
        private readonly PreconfigBinder _binder;

        public PreconfigBinderProvider(Dictionary<string, object> binderValues)
        {
            _binder = new PreconfigBinder(binderValues);
        }

        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (_binder.CanBind(context))
            {
                return _binder;
            }

            return null;
        }
    }

    public class PreconfigBinder: IModelBinder
    {
        private readonly Dictionary<string, object> _binderValues;

        public PreconfigBinder(Dictionary<string, object> binderValues)
        {
            _binderValues = binderValues;
        }

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            bindingContext.Result = _binderValues.TryGetValue(bindingContext.ModelMetadata.Name, out object value) 
                ? ModelBindingResult.Success(value) 
                : ModelBindingResult.Failed();

            return Task.CompletedTask;
        }

        public bool CanBind(ModelBinderProviderContext context)
        {
            return _binderValues.ContainsKey(context.Metadata.Name);
        }
    }
}
