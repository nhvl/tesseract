﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IUserService
    {
        Task<IdentityResult> CreateUser(ApplicationUser user);
        Task<IdentityResult> UpdateUser(ApplicationUser user, string originalEmail);
        Task<ApplicationUser> GetUserById(long userId);
        Task<List<ApplicationUser>> GetUsersById(long[] userIds);

        Task<ApplicationRole[]> GetUserRoles(ApplicationUser user);
        Task<ApplicationRole[]> GetUserCustomRoles(ApplicationUser user);
        Task<Claim[]> GetRoleClaims(ApplicationRole[] roles);
        Task<Claim[]> GetUserClaims(ApplicationUser user);
    }
}
