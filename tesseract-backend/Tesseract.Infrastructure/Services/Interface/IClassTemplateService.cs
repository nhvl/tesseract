﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public class ApplyTemplateConfig
    {
        public string ClassName { get; set; }
        public long GradingTermId { get; set; }
    }

    public interface IClassTemplateService
    {
        Task CreateTemplate(long classId, long facultyId);
        Task DeleteTemplate(long templateId);
        Task<IEnumerable<Class>> GetTemplates(long facultyId);
        Task<Class> ApplyTemplate(long templateId, ApplyTemplateConfig config);
    }
}
