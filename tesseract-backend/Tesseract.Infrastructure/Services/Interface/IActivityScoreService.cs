﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IActivityScoreService
    {
        Task<ActivityScore> Get(long activityId, long studentId);
        Task<IEnumerable<ActivityScore>> GetActivityScoreOfClass(long classId);
    }
}
