﻿using System.Threading.Tasks;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IRazorViewToStringRenderer
    {
        Task<string> RenderViewToStringAsync<TModel>(string viewName, TModel model);
    }
}
