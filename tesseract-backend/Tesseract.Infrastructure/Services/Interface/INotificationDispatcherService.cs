﻿using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface INotificationDispatcherService
    {
        Task FacultyUpdateActivity(ApplicationUser user, Activity activity);
    }
}
