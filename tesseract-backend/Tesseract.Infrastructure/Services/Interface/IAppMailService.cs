﻿using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IAppMailService
    {
        /// <param name="resetPasswordLinkTemplate">The reset password URL template with placeholder {0} for token</param>
        Task<bool> ForgotPassword(string email, string resetPasswordLinkTemplate);

        /// <param name="resetPasswordLinkTemplate">The reset password URL template with placeholder {0} for token</param>
        Task<bool> ForgotPassword(ApplicationUser user, string resetPasswordLinkTemplate);

        Task<bool> NewUserSetPassword(ApplicationUser user, string resetPasswordLinkTemplate);

        Task<bool> ConfirmEmail(ApplicationUser user, string ConfirmEmailLinkTemplate);

        Task<bool> NotifyStudentForNewParent(ApplicationUser student, ApplicationUser parent);
    }
}
