﻿namespace Tesseract.Infrastructure.Services.Interface
{
    public interface ISecurityService
    {
        string RandomPassword();
    }
}