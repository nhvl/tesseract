﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IUserNotificationSettingService
    {
        Task<List<UserNotificationSetting>> GetUserNotificationSettings(long userId);
        Task<UserNotificationSetting> GetUserNotificationSettings(long userId, NotificationType type);
        Task<UserNotificationSetting[]> SaveUserNotificationSettings(long userId, UserNotificationSetting[] settings);
        Task<UserNotificationSetting> SaveUserNotificationSetting(long userId, NotificationType type, UserNotificationSetting setting);
    }
}
