﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IFacultyService
    {
        Task<Faculty> GetFacultyAsync(long facultyId);
        Task<Faculty> GetFacultyByUserIdAsync(long userId);
        IQueryable<GeneralRecord> GetFacultyAsAdmin(long facultyId);

        Task<(Faculty, ApplicationUser)> GetFacultyOfSchool(long schoolId, long facultyId);

        Task<IList<Faculty>> GetFacultiesOfClass(long classId);
        Task<IList<Faculty>> GetFacultiesOfSchool(long schoolId);

        Task<Faculty> CreateFaculty(Faculty faculty);
        Task UpdateFacultyAsync(Faculty faculty);
        Task DeleteFacultyAsync(Faculty faculty);

        IQueryable<GeneralRecord> GetFacultiesOfClasses(long[] classId);
    }
}
