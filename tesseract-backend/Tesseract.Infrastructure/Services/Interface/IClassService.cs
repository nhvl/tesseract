﻿#nullable enable

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IClassService
    {
        Task<Class?> GetClassAsync(long classId);
        Task<IList<Class>> GetClassesById(long[] classIds);
        Task<IList<Class>> GetClassOfSchool(long schoolId);
        IQueryable<GeneralRecord> GetClassesOfFacultyInSchool(long schoolId, long facultyId);
        IQueryable<GeneralRecord> GetClassesOfFacultyInSchool(long schoolId, long facultyId, long gradingTermId);
        IQueryable<GeneralRecord> GetClassesOfFacultyInSchool(long schoolId, long facultyId, long[] classIds);
        IQueryable<GeneralRecord> GetClassesOfStudentInSchool(long schoolId, long studentId);
        IQueryable<GeneralRecord> GetClassesOfStudentInSchool(long schoolId, long studentId, long[] classIds);
        Task<IQueryable<GeneralRecord>> GetClassesOfStudent(long studentId, long schoolId);
        IQueryable<GeneralRecord> GetCurrentClassesOfStudent(long studentId);

        Task<Class> CreateClassAsync(Class classItem, long facultyId);

        Task<Class?> UpdateClassAsync(Class classItem);
        Task UpdateClasses(IEnumerable<Class> classes, IEnumerable<ClassFaculty> classFaculties);
        Task UpdateClassStudents(IEnumerable<ClassStudent> classStudents);

        Task DeleteClassAsync(Class classItem);

        Task RegisterStudentAsync(long classId, long studentId);
        Task RegisterFacultyAsync(long classId, long facultyId);
    }
}
