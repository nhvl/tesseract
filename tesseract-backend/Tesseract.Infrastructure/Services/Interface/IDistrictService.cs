﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Enums;
using Tesseract.Infrastructure.Models;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IDistrictService
    {
        Task<District> GetByDomain(string districtDomain);
        Task<District> GetById(long districtId);

        Task<District> Update(District district);

        Task<DistrictReport> GetQuickReport(long districtId);
        Task<DistrictReport> GetQuickReportForSchoolAdmin(long districtId, long userId);
        Task<SchoolReport> GetSchoolReport(long schoolId, long termId);
        Task CreateQuickReports();

        Task<IEnumerable<(Faculty, long, ActivePassiveCount)>> GetFaculties(long districtId);

        Task<IEnumerable<(Faculty, long, ActivePassiveCount)>> GetFacultiesForSchoolAdmin(long userId);

        Task<IEnumerable<School>> GetSchools(long districtId);

        Task<PowerSearchResult> PowerSearch(long userId, string userRole, string searchText, int maxItems, long? schoolId);
    }

}
