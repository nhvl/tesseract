﻿using System.Threading.Tasks;
using Tesseract.Core.Entities.NoSql;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface ISubmissionService
    {
        Task<Submission> Get(long activityId, long studentId);
        Task<Submission> CreateOrUpdate(Submission doc, long? userId);
        Task<Submission> Create(Submission doc);
        Task<Submission> Update(Submission doc);
        Task<Submission> Delete(long activityId, long studentId);
    }
}
