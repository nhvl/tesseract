﻿using System.Threading.Tasks;
using Tesseract.Core.Entities.NoSql;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IActDocService
    {
        Task<ActDoc> Get(long activityId);
        Task<ActDoc> Create(ActDoc doc);
        Task<ActDoc> Update(ActDoc doc);
        Task<ActDoc> DeleteByActivityId(long activityId);

        Task<StudentActDoc> GetStudentDoc(long activityId, long studentId);
        Task<StudentActDoc> GetOrCreateStudentDoc(long activityId, long studentId);
        Task<StudentActDoc> CreateStudentDoc(StudentActDoc doc);
        Task<StudentActDoc> UpdateStudentDoc(StudentActDoc doc);
        Task<StudentActDoc> DeleteStudentDocByActivityId(long activityId, long studentId);

    }
}
