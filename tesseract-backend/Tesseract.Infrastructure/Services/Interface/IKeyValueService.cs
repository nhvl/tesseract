﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Models;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IKeyValueService
    {
        Task<string> GetValueFromKey(long? districtId, long? schoolId, string keyName);
        Task<IEnumerable<string>> GetValuesFromKey(long? districtId, long? schoolId, string keyName);

        Task<GradeRange[]> GetDefaultGradeRange(long? districtId, long? schoolId);
        Task<GradeRange[]> SetDefaultGradeRange(long? districtId, long? schoolId, GradeRange[] gradeRange);
        Task<ScoreBadge> GetScoreBadge(long? districtId, long? schoolId);
        Task<string[]> GetActivityCategoryColors(long? districtId, long? schoolId);
        Task<string> GetGoogleTagManagerId(long? districtId, long? schoolId);
    }

    public class ScoreBadge
    {
        public string CommunicationColor { get; set; }
        public string CollaborationColor { get; set; }
        public string CharacterColor { get; set; }
        public string CreativityColor { get; set; }
        public string CriticalThinkingColor { get; set; }
    }
}
