﻿using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IParentService
    {
        Task<Parent> GetParentAsync(long parentId);
        Task<Parent> CreateParentAsync(Parent parent);
        Task UpdateParentAsync(Parent parent);
        Task DeleteParentAsync(Parent parent);
        Task<Parent> GetParentByUserIdAsync(long id);

        Task AddStudentForParent(long parentId, long studentId);
    }
}
