﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IEmailTemplateService
    {
        Task<EmailTemplate> GetEmailTemplateByNameAsync(string name);
    }
}
