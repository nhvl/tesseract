﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public partial interface IEmailSenderService
    {
        /// <summary>
        /// Sends an email asynchronous
        /// </summary>
        /// <param name="subject">Subject</param>
        /// <param name="body">Body</param>
        /// <param name="fromAddress">From address</param>
        /// <param name="fromName">From display name</param>
        /// <param name="toAddress">To address</param>
        /// <param name="toName">To display name</param>
        /// <param name="replyToAddress">ReplyTo address</param>
        /// <param name="replyToName">ReplyTo display name</param>
        /// <param name="bcc">BCC addresses list</param>
        /// <param name="cc">CC addresses list</param>
        /// <param name="attachmentFilePaths">Attachment file paths</param>
        /// <param name="attachmentFileName">Attachment file name. If specified, then this file name will be sent to a recipient. Otherwise, "AttachmentFilePath" name will be used.</param>
        /// <param name="attachedDownloadId">Attachment download ID (another attachment)</param>
        /// <param name="headers">Headers</param>
        Task SendEmailAsync(string subject, string body,
            string fromAddress, string fromName, string toAddress, string toName,
             string replyToAddress = null, string replyToName = null,
            IEnumerable<string> bcc = null, IEnumerable<string> cc = null,
            IEnumerable<string> attachmentFilePaths = null, string attachmentFileName = null,
            int attachedDownloadId = 0, IDictionary<string, string> headers = null);
    }
}
