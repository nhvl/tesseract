﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IDiscussionService
    {
        Task<Discussion> Create(Discussion item);
        Task<Discussion> Update(Discussion item);

        Task Delete(Discussion item);
        Task<Discussion> Delete(long dicussionId);

        Task<Discussion> GetById(long dicussionId);
        Task<IList<Discussion>> GetDiscussionsOfClass(long classId);
        Task<IList<Discussion>> GetDiscussionsOfActivity(long activityId);
        Task<Discussion> GetDiscussionFromDiscussionActivity(long discussionActivityId);
        Task<IList<Discussion>> GetDiscussionsOfClassAsStudent(long classId);
        Task<IList<Discussion>> GetDiscussionsOfActivityAsStudent(long activityId);

        Task<ActivityCountDiscussion[]> CountDiscussionOfActivity(long[] activityIds);

        string GenerateThreadId();
    }
}
