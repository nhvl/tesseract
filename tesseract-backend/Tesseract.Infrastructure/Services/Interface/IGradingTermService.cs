﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IGradingTermService
    {
        Task<IEnumerable<GradingTerm>> GetGradingTerms(long schoolId);
        Task<GradingTerm[]> GetGradingTerms(long[] schoolIds);
        Task<GradingTerm> GetGradingTerm(long schoolId, long termId);

        Task<GradingTerm> SetGradingTerm(GradingTerm term);

        Task DeleteGradingTerm(GradingTerm term);
    }
}
