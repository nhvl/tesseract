﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public class GeneralRecord
    {
        public ApplicationUser User          { get; set; }
        public School          School        { get; set; }
        public GradingTerm     GradingTerm   { get; set; }
        public Class           Class         { get; set; }
        public Student         Student       { get; set; }
        public Faculty         Faculty       { get; set; }
        public SchoolStudent   SchoolStudent { get; set; }
        public SchoolFaculty   SchoolFaculty { get; set; }
        public ClassStudent    ClassStudent  { get; set; }
        public ClassFaculty    ClassFaculty  { get; set; }
        public Activity        Activity      { get; set; }
        public ActivityScore   ActivityScore { get; set; }

        public static GeneralDto ToGeneralDto(IEnumerable<GeneralRecord> items)
        {
            return new GeneralDto
            {
                Users           = items.Select(i => i.User                ).Where(i => i != null).Distinct().ToArray(),
                Schools         = items.Select(i => i.School              ).Where(i => i != null).Distinct().ToArray(),
                GradingTerms    = items.Select(i => i.GradingTerm         ).Where(i => i != null).Distinct().ToArray(),
                Classes         = items.Select(i => i.Class               ).Where(i => i != null).Distinct().ToArray(),
                Faculties       = items.Select(i => i.Faculty             ).Where(i => i != null).Distinct().ToArray(),
                Students        = items.Select(i => i.Student             ).Where(i => i != null).Distinct().ToArray(),
                SchoolFaculties = items.Select(i => i.SchoolFaculty       ).Where(i => i != null).Distinct().ToArray(),
                SchoolStudents  = items.Select(i => i.SchoolStudent       ).Where(i => i != null).Distinct().ToArray(),
                ClassStudents   = items.Select(i => i.ClassStudent        ).Where(i => i != null).Distinct().ToArray(),
                ClassFaculties  = items.Select(i => i.ClassFaculty        ).Where(i => i != null).Distinct().ToArray(),
                Activities      = items.Select(i => i.Activity            ).Where(i => i != null).Distinct().ToArray(),
                ActivityScores  = items.Select(i => i.ActivityScore       ).Where(i => i != null).Distinct().ToArray(),
            };
        }
    }

    public class UserDto {
        public long?     UserId      { get; set; }
        public string    Avatar      { get; set; }
        public string    FirstName   { get; set; }
        public string    LastName    { get; set; }
        public string    Salutation  { get; set; }
        public string    Nickname    { get; set; }
        public string    Language    { get; set; }
        public string    PhoneNumber { get; set; }
        public string    Email       { get; set; }
        public string    Dob         { get; set; }
        public DateTime? LastActive  { get; set; }
        public long      DistrictId  { get; set; }

        public UserDto(ApplicationUser user)
        {
            if (user == null) return;
            UserId      = user.Id;
            Avatar      = user.Avatar;
            FirstName   = user.FirstName;
            LastName    = user.LastName;
            Salutation  = user.Salutation;
            Nickname    = user.Nickname;
            Language    = user.Language;
            PhoneNumber = user.PhoneNumber;
            Email       = user.Email;
            Dob         = user.Dob?.ToShortDateString();
            LastActive  = user.LastActive;
            DistrictId  = user.DistrictId;
        }

        public static UserDto Create(ApplicationUser user) {
            return new UserDto(user);
        }
    }
    public class FacultyDto: UserDto {
        public long   FacultyId  { get; set; }
        public string ExternalId { get; set; }

        public FacultyDto(ApplicationUser user, Faculty faculty): base(user)
        {
            FacultyId  = faculty.FacultyId;
            ExternalId = faculty.ExternalId;
            UserId     = faculty.FacultyId;
        }

        public static FacultyDto Create(ApplicationUser user, Faculty faculty) {
            return new FacultyDto(user, faculty);
        }
    }
    public class StudentDto: UserDto {
        public long   StudentId     { get; set; }
        public string ExternalId    { get; set; }
        public string StudentNumber { get; set; }
        public short  Grade         { get; set; }

        public StudentDto(ApplicationUser user, Student student): base(user)
        {
            StudentId     = student.StudentId;
            ExternalId    = student.ExternalId;
            UserId        = student.StudentId;
            StudentNumber = student.StudentNumber;
            Grade         = student.Grade;
        }

        public static StudentDto Create(ApplicationUser user, Student student) {
            return new StudentDto(user, student);
        }
    }

    public class ClassDto {
        public long          SchoolId             { get; set; }
        public long          GradingTerm          { get; set; }
        public long          ClassId              { get; set; }
        public string        Period               { get; set; }
        public string        ClassName            { get; set; }
        public string        Description          { get; set; }
        public bool         UseDefaultGradesRange { get; set; }
        public GradeRange[] GradeRanges           { get; set; }
        public bool         IsTemplate            { get; set; }

        public static ClassDto Create(Class c) {
            return new ClassDto {
                SchoolId = c.SchoolId,
                GradingTerm = c.GradingTerm,
                ClassId = c.ClassId,
                Period = c.Period,
                ClassName = c.ClassName,
                Description = c.Description,
                IsTemplate = c.IsTemplate,
                UseDefaultGradesRange = c.UseDefaultGradesRange,
                GradeRanges = c.UseDefaultGradesRange ? null : c.GradeRanges
            };
        }
    }

    public class ActivityDto {
        public long         ClassId          { get; set; }
        public long         ActivityId       { get; set; }
        public ActivityType Type             { get; set; }
        public string       Title            { get; set; }
        public string       Description      { get; set; }
        public DateTime?    DateDue          { get; set; }
        public DateTime?    DateAssigned     { get; set; }
        public DateTime?    DateCutoff       { get; set; }
        public int          SortIndex        { get; set; }
        public long?        ParentActivityId { get; set; }
        public DateTime?    PublishStartDate { get; set; }
        public DateTime?    PublishEndDate   { get; set; }
        public long?        CategoryId       { get; set; }
        public string       Color            { get; set; }
        public bool         IsGraded         { get; set; }
        public decimal      MaxScore         { get; set; }
        public decimal      Weight           { get; set; }
        public bool         IsCredit         { get; set; }

        public static ActivityDto Create(Activity a) {
            return new ActivityDto {
                ClassId          = a.ClassId,
                ActivityId       = a.ActivityId,
                Type             = a.Type,
                Title            = a.Title,
                Description      = a.Description,
                DateAssigned     = a.DateAssigned,
                DateDue          = a.DateDue,
                DateCutoff       = a.DateCutoff,
                ParentActivityId = a.ParentActivityId,
                SortIndex        = a.SortIndex,
                PublishStartDate = a.PublishStartDate,
                PublishEndDate   = a.PublishEndDate,
                IsGraded         = a.IsGraded,
                Color            = a.Color,
                MaxScore         = a.MaxScore,
                Weight           = a.Weight,
                IsCredit         = a.IsCredit,
            };
        }
    }

    public class ActivityScoreDto {
        public long      StudentId        { get; set; }
        public long      ActivityId       { get; set; }
        public string    ThreadId         { get; set; }
        public bool      IsExclude        { get; set; }
        public bool      IsSubmitted      { get; set; }
        public DateTime? SavedDate        { get; set; }
        public DateTime? GradeDate        { get; set; }
        public decimal?  Score            { get; set; }
        public byte?     Communication    { get; set; }
        public byte?     Collaboration    { get; set; }
        public byte?     Character        { get; set; }
        public byte?     Creativity       { get; set; }
        public byte?     CriticalThinking { get; set; }

        public static ActivityScoreDto Create(ActivityScore score) {
            return new ActivityScoreDto {
                StudentId        = score.StudentId,
                ActivityId       = score.ActivityId,
                GradeDate        = score.GradeDate,
                IsExclude        = score.IsExclude,
                Score            = score.Score,
                Communication    = score.Communication,
                Collaboration    = score.Collaboration,
                Character        = score.Character,
                Creativity       = score.Creativity,
                CriticalThinking = score.CriticalThinking,
                SavedDate        = score.SavedDate,
                IsSubmitted      = score.IsSubmitted,
                ThreadId         = score.ThreadId,
            };
        }
    }

    public class ThreadUnread {
        public string ThreadId { get; set; }
        public int    Unread { get; set; }
    }

    public class ActivityCountDiscussion {
        public long ActivityId      { get; set; }
        public long DiscussionCount { get; set; }
    }

    public class GeneralDto
    {
        public ApplicationUser[] Users                 { get; set; }
        public Faculty        [] Faculties             { get; set; }
        public Student        [] Students              { get; set; }
        public School         [] Schools               { get; set; }
        public GradingTerm    [] GradingTerms          { get; set; }
        public Class          [] Classes               { get; set; }
        public SchoolFaculty  [] SchoolFaculties       { get; set; }
        public SchoolStudent  [] SchoolStudents        { get; set; }
        public ClassFaculty   [] ClassFaculties        { get; set; }
        public ClassStudent   [] ClassStudents         { get; set; }
        public Activity       [] Activities            { get; set; }
        public ActivityScore  [] ActivityScores        { get; set; }
        public Discussion     [] Discussions           { get; set; }
        public Comment        [] Comments              { get; set; }
        public CommentSeen    [] CommentSeens          { get; set; }

        public ThreadUnread[] ThreadUnreads         { get; set; }
        public ActivityCountDiscussion[] ActivityCountDiscussions { get; set; }

        public object ToObject()
        {
            var id2User = this.Users?.ToDictionary(u => u.Id) ?? new Dictionary<long, ApplicationUser>();
            return (new
            {
                users = this.Users?.Where(u => u != null &&
                        (this.Students?.All(s => s.StudentId != u.Id) ?? true) &&
                        (this.Faculties?.All(s => s.FacultyId != u.Id) ?? true)
                    )
                    .Select(UserDto.Create),
                faculties = this.Faculties?.Select(faculty => FacultyDto.Create(id2User.ContainsKey(faculty.FacultyId) ? id2User[faculty.FacultyId] : null, faculty)),
                students = this.Students?.Select(student => new StudentDto(id2User.ContainsKey(student.StudentId) ? id2User[student.StudentId] : null, student)),
                schools = this.Schools?.Select(school => new
                {
                    school.DistrictId,
                    school.SchoolId,
                    school.CurrentGradingTerm,
                    school.IconUrl,
                    school.LogoUrl,
                    school.SchoolName,
                }),
                gradingTerms = this.GradingTerms?.Select(t => new { t.GradingTermId, t.Name, t.SchoolId, t.StartDate, t.EndDate }),
                classes = this.Classes?.Select(ClassDto.Create),
                SchoolFaculties = this.SchoolFaculties?.Select(sf => new
                {
                    sf.SchoolId,
                    sf.FacultyId,
                }),
                schoolStudents = this.SchoolStudents?.Select(ss => new
                {
                    ss.SchoolId,
                    ss.StudentId,
                    ss.GradingPeriod,
                    ss.Grade,
                }),
                classFaculties = this.ClassFaculties?.Select(cf => new
                {
                    cf.ClassId,
                    cf.FacultyId,
                    cf.SortIndex,
                }),
                classStudents = this.ClassStudents?.Select(cs => new
                {
                    cs.StudentId,
                    cs.ClassId,
                    cs.CumulativeGrade,
                    cs.SortIndex,
                    cs.LastAccessed,
                }),
                activities = this.Activities?.Select(ActivityDto.Create),
                activityScores = this.ActivityScores?.Select(ActivityScoreDto.Create),
                discussions   = this.Discussions,
                comments      = this.Comments,
                commentSeens  = this.CommentSeens,
                threadUnreads = this.ThreadUnreads,
                activityCountDiscussion = this.ActivityCountDiscussions?.ToDictionary(x => x.ActivityId, x => x.DiscussionCount),
            });
        }
    }
}

