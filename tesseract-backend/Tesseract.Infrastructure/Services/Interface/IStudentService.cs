﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IStudentService
    {
        Task<Student> GetStudentById(long studentId);
        Task<Student> GetStudentByUserId(long userId);
        Task<Student> GetStudentByStudentNumber(string studentNumber);
        Task<Student> GetStudentByStudentNumber(string studentNumber, string phoneNumber);

        IQueryable<GeneralRecord> GetStudentsOfDistrict(long districtId);
        Task<GeneralDto> GetStudentsOfSchool(long schoolId
            , int pageIndex = 0
            , int pageSize = -1
            , long excludedClassId = -1
            , long filteredClassId = -1
            , string searchText = ""
            , string sortColumnKey = ""
            , string sortOrder = "");
        Task<GeneralDto> GetAllStudentsOfTeacherAsync(long facultyId, long? classId);
        IQueryable<GeneralRecord> GetStudentsByActivity(long activityId, long classId);
        IQueryable<GeneralRecord> GetStudentsInClass(long classId, bool includeUserInfo = false);

        IQueryable<GeneralRecord> GetStudentsOfParent(long parentId);

        Task<GeneralDto> GetStudentDetailForTeacherAsync(long facultyId, long studentId, long? classId);

        Task<Student> CreateStudentAsync(Student student, long schoolId);
        Task UpdateStudentAsync(Student student);
        Task DeleteStudentAsync(Student student);
        Task<IEnumerable<Activity>> GetOpenActivities(long studentId);
        Task<IEnumerable<ActivityScore>> GetActivityScores(long studentId);
    }
}

