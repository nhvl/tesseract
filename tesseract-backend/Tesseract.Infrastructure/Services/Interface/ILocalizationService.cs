﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface ILocalizationService
    {
        Task<List<string>> GetAllLocales();
        Task<IList<Localization>> GetLocale(string language);
        Task<int> UpdateLocale(string language, IEnumerable<Localization> records);
    }
}
