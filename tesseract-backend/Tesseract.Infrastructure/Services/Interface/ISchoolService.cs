﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface ISchoolService
    {
        Task<School> GetSchoolById(long schoolId);
        Task<School> GetSchoolInDistrict(long districtId, long schoolId);
        Task<School> GetSchoolOfSchoolAdmin(long userId, long schoolId);

        IQueryable<School> GetSchoolsInDistrict(long districtId);
        IQueryable<School> GetSchoolsOfSchoolAdmin(long userId);
        IQueryable<School> GetSchoolsOfSchoolAdmin(long userId, long districtId);
        Task<IList<School>> GetSchoolsOfFaculty(long facultyId);
        Task<IList<School>> GetSchoolsOfStudent(long studentId);

        Task<School> GetSchoolOfFaculty(long facultyId, long schoolId);

        IQueryable<GeneralRecord> GetCurrentSchoolsOfStudent(long studentId);

        Task<School> CreateSchool(School school, GradingTerm currentGradingTerm);
        Task<School> UpdateSchoolAsync(School school);
        Task DeleteSchool(School school);

        Task RegisterStudentAsync(SchoolStudent schoolStudent);
        Task RegisterFacultyAsync(long schoolId, long facultyId);
    }
}

