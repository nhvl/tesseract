﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface IActivityCategoryService
    {
        Task<IEnumerable<ActivityCategory>> GetCategories(long facultyId);
        Task<ActivityCategory> GetCategory(long categoryId);
        Task<ActivityCategory> SetCategory(ActivityCategory category);
        Task DeleteCategory(ActivityCategory category);
    }
}
