﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Entities.NoSql;

namespace Tesseract.Infrastructure.Services.Interface
{
    public enum ActivityStatusEnum: byte
    {
        All,
        Overdue,
        Pending,
        Done
    }

    public interface IActivityService
    {
        Task<Activity> GetActivity(long activityId, bool includeClass = true);
        Task<GeneralRecord> GetActivityWithScore(long studentId, long activityId);
        IQueryable<Activity> GetActivitiesById(long[] activityIds);

        Task<IEnumerable<Activity>> GetActivitiesOfClass(long classId);
        Task<IList<GeneralRecord>> GetActivitiesOfStudent(long studentId, long schoolId, long? classId, ActivityType? type);
        Task<IList<GeneralRecord>> GetActivitiesOfStudentInClass(long studentId, long classId);

        Task<Activity> CreateActivity(Activity activity);
        Task<Activity> EditActivity(Activity activity);
        Task EditActivitySimple(Activity activity, bool commit = true);
        Task<int> DeleteActivity(long activityId);

        Task<IEnumerable<long>> DuplicateActivity(long activityId, IEnumerable<long> toClassIds);

        Task<Submission> MakeSubmission(long activityId, long studentId, string content, SubmissionAttachment[] attachments, long? userId, bool isSubmitted);
        Task<decimal?> SetGrade(long activityId, long studentId, decimal? grade, byte? communication = null, byte? collaboration = null, byte? character = null, byte? creativity = null, byte? criticalThinking = null, bool? isExclude = null);

        IQueryable<GeneralRecord> GetActivitiesOfFaculty(long schoolId, long facultyId);
        IQueryable<GeneralRecord> GetActivitiesOfFaculty(long schoolId, long gradingTermId, long facultyId);
        IQueryable<Activity> GetActivitiesOfFacultyInClass(long classId, long facultyId);
        Task<IEnumerable<Activity>> GetActivitiesOfFaculty(long facultyId, long gradingTermId, Expression<Func<Activity, bool>> p);

        Task<ActivityScore> GetActivityScore(long activityId, long studentId);
        Task<ActivityScore> GetActivityScoreOrCreate(long activityId, long studentId, bool commit = true);
        Task<ActivityScore> GetActivityScoreEnsureThreadId(long activityId, long studentId, bool commit = true);
        Task EnsureActivityScoreHasThreadId(ActivityScore s, bool commit = true);
        Task EnsureStudentsHasActivityScoreWithThreadId(IEnumerable<GeneralRecord> records, long activityId);
        Task EnsureActivitiesHasActivityScoreWithThreadId(IEnumerable<GeneralRecord> records, long studentId);

        IQueryable<GeneralRecord> GetActivitiesOfStudentAsParent(long studentId, long[] schoolIds);

        Task<int> GetPollingCount(long activityId, string pollQuizId, byte voteContent);
        Task<Poll> SetPolling(long activityId, long studentId, string pollQuizId, byte voteContent);
        Task<byte?> GetStudentVote(long activityId, long studentId, string pollQuizId);

        Task<IEnumerable<Activity>> GetActivitiesByCategory(long activityCategoryId);

        Task ReCalculateCumulativeGrade(long classId, long? studentId = null);

    }
}
