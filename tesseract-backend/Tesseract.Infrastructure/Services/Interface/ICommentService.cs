﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface ICommentService
    {
        Task<Comment> Create(Comment item);
        Task Delete(Comment item);
        Task<Comment> Delete(long commentId);

        Task<Comment> GetCommentById(long commentId);
        Task<IList<Comment>> GetCommentsOfThread(string threadId);
        Task<IList<Comment>> GetReplies(long commentId);
        Task<Comment> GetLastCommentOfThread(string threadId);
        Task<List<Comment>> GetLastCommentOfThreads(string[] threadIds);

        Task<int> CountCommentsOfThread(string threadId);
        Task<int> CountReplies(long commentId);
        Task<int> CountNewComments(string threadId, DateTime from);
        Task<int> CountNewComments(string threadId, long userId);
        Task<int> CountNewReplies(long commentId, DateTime from);

        Task<IEnumerable<long>> GetUserIdsOfThread(string threadId);

        Task<CommentSeen> GetCommentSeen(string threadId, long userId);
        Task<CommentSeen> SetCommentSeen(string threadId, long userId, DateTime lastSeen);
        Task<List<ThreadUnread>> CountNewComments(long userId, string[] threadIds);
    }
}
