﻿using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Infrastructure.Services.Interface
{
    public interface ILoginLogService
    {
        Task CreateLoginAsync(LoginLog login);
    }
}
