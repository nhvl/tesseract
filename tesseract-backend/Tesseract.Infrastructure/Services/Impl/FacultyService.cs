﻿#nullable enable

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class FacultyService : IFacultyService
    {
        private readonly MainDbContext _mainDbContext;

        public FacultyService(MainDbContext mainDbContext)
        {
            _mainDbContext = mainDbContext;
        }

        public Task<Faculty> CreateFaculty(Faculty faculty)
        {
            return _mainDbContext.AddAsync(faculty, true);
        }

        public async Task DeleteFacultyAsync(Faculty faculty)
        {
            await _mainDbContext.DeleteAsync(faculty, true);
        }

        public async Task<Faculty> GetFacultyAsync(long facultyId)
        {
            return await _mainDbContext.Faculties.FirstOrDefaultAsync(f => f.FacultyId == facultyId);
        }

        public IQueryable<GeneralRecord> GetFacultyAsAdmin(long facultyId)
        {
            return (
                from f in _mainDbContext.Faculties
                join u in _mainDbContext.Users on f.FacultyId equals u.Id
                join sf in _mainDbContext.SchoolFaculties on f.FacultyId equals sf.FacultyId
                where f.FacultyId == facultyId
                select new GeneralRecord{ Faculty = f, User = u, SchoolFaculty = sf }
            );
        }

        public async Task<(Faculty, ApplicationUser)> GetFacultyOfSchool(long schoolId, long facultyId)
        {
            var r = await (
                from f in _mainDbContext.Faculties
                join sf in _mainDbContext.SchoolFaculties on f.FacultyId equals sf.FacultyId
                join u in _mainDbContext.Users on f.FacultyId equals u.Id
                where f.FacultyId == facultyId
                   && sf.SchoolId == schoolId
                select new GeneralRecord{ Faculty = f, User = u }
            ).FirstOrDefaultAsync();
            return (r.Faculty, r.User);
        }

        public async Task<Faculty?> GetFacultyByUserIdAsync(long userId)
        {
            var r = await (
                from f in _mainDbContext.Faculties
                join u in _mainDbContext.Users on f.FacultyId equals u.Id
                where u.Id == userId
                select new GeneralRecord { Faculty = f, User = u }
            ).FirstOrDefaultAsync();

            if (r == null) return null;

            var faculty = r.Faculty;
            if (faculty.User == null) faculty.User = r.User;

            return faculty;
        }

        public async Task<IList<Faculty>> GetFacultiesOfClass(long classId)
        {
            return await (
                from cf in _mainDbContext.ClassFaculties
                join f in _mainDbContext.Faculties on cf.FacultyId equals f.FacultyId
                where cf.ClassId == classId
                select f
            ).ToListAsync();
        }

        public async Task<IList<Faculty>> GetFacultiesOfSchool(long schoolId)
        {
            return await (
                from sf in _mainDbContext.SchoolFaculties
                join f in _mainDbContext.Faculties on sf.FacultyId equals f.FacultyId
                where sf.SchoolId == schoolId
                select f
            ).ToListAsync();
        }

        public async Task UpdateFacultyAsync(Faculty faculty)
        {
            var s = await GetFacultyAsync(faculty.FacultyId);
            await _mainDbContext.UpdateAsync(s, true);
        }

        public IQueryable<GeneralRecord> GetFacultiesOfClasses(long[] classId) {
            return (
                from cf in _mainDbContext.ClassFaculties
                join f in _mainDbContext.Faculties on cf.FacultyId equals f.FacultyId
                join u in _mainDbContext.Users on f.FacultyId equals u.Id
                where classId.Contains(cf.ClassId)
                select new GeneralRecord { Faculty = f, ClassFaculty = cf, User = u }
            );
        }
    }
}
