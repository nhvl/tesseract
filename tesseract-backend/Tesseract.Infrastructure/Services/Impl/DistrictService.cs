﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Data.NoSql;
using Tesseract.Infrastructure.Models;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class DistrictService : IDistrictService
    {
        protected readonly MainDbContext _mainDbContext;
        protected readonly IStudentService _studentService;
        protected readonly ISchoolService _schoolService;
        protected readonly IActivityService _activityService;
        protected readonly IDocumentDBRepository _documentDbRepository;

        public DistrictService(
            MainDbContext mainDbContext,
            IStudentService studentService,
            ISchoolService schoolService,
            IActivityService activityService,
            IDocumentDBRepository documentDbRepository
        )
        {
            _mainDbContext = mainDbContext;
            _studentService = studentService;
            _schoolService = schoolService;
            _activityService = activityService;
            _documentDbRepository = documentDbRepository;
        }

        public async Task<District> Update(District district)
        {
            if (district == null) return null;

            var d = await _mainDbContext.Districts.FirstOrDefaultAsync(x => x.DistrictId == district.DistrictId);
            if (d == null) return null;

            if (!string.IsNullOrEmpty(district.DistrictName)) d.DistrictName = district.DistrictName;
            if (!string.IsNullOrEmpty(district.LogoUrl)) d.LogoUrl = district.LogoUrl;

            await _mainDbContext.UpdateAsync(d);
            return d;
        }

        public async Task<District> GetByDomain(string districtDomain)
        {
            return await _mainDbContext.Districts.FirstOrDefaultAsync(x => x.Domain.ToLower() == districtDomain.ToLower());
        }

        public async Task<District> GetById(long districtId)
        {
            return await _mainDbContext.Districts.FirstOrDefaultAsync(x => x.DistrictId == districtId);
        }

        public async Task<DistrictReport> GetQuickReportForSchoolAdmin(long districtId, long userId)
        {
            var schools = (await _schoolService.GetSchoolsOfSchoolAdmin(userId, districtId).ToArrayAsync());

            var schoolReports = await GetSchoolReports(schools);

            return new DistrictReport { DistrictId = districtId, SchoolReports = schoolReports };
        }

        public async Task<DistrictReport> GetQuickReport(long districtId)
        {
            var schools = await _mainDbContext.Schools
                .Where(s => s.DistrictId == districtId)
                .ToArrayAsync();

            var schoolReports = await GetSchoolReports(schools);

            return new DistrictReport { DistrictId = districtId, SchoolReports = schoolReports };
        }

        private async Task<SchoolReport[]> GetSchoolReports(School[] schools)
        {
            return await Task.WhenAll(schools.Select(s => GetSchoolReport(s.SchoolId, s.CurrentGradingTerm)));
        }

        public Task<SchoolReport> GetSchoolReport(long schoolId, long termId) =>
            _documentDbRepository.GetOne<SchoolReport>(sr => sr.SchoolId == schoolId && sr.GradingTerm == termId);

        public async Task CreateQuickReports()
        {
            var currentGradingTerms = await _mainDbContext.Schools
                .Select(s => new { s.SchoolId, s.CurrentGradingTerm, s.DistrictId })
                .ToListAsync();

            bool IsCurrentTerm(long schoolId, long termId) => currentGradingTerms.Any(ct => ct.SchoolId == schoolId && ct.CurrentGradingTerm == termId);

            var gradingTerms = await _mainDbContext.GradingTerms
                .Select(s => new { s.SchoolId, s.GradingTermId })
                .ToListAsync();
            var schoolIds = gradingTerms.Select(t => t.SchoolId).ToArray();
            var termIds = gradingTerms.Select(t => t.GradingTermId).ToArray();

            var reports = await _documentDbRepository.GetManyAsync<SchoolReport>(sr => schoolIds.Contains(sr.SchoolId) && termIds.Contains(sr.GradingTerm));

            var xFacultyIds = await _mainDbContext.SchoolFaculties
                                        .GroupBy(sf => new { sf.FacultyId })
                                        .Select(g => new { g.Key.FacultyId, Count = g.Count() })
                                        .Where(g => g.Count > 1)
                                        .Select(g => g.FacultyId)
                                        .ToListAsync();
            var xFacultyLookup = (await _mainDbContext.SchoolFaculties
                                        .Where(sf => xFacultyIds.Contains(sf.FacultyId))
                                        .ToArrayAsync())
                                       .ToLookup(sf => sf.SchoolId, sf => sf.FacultyId);

            var tasks = gradingTerms.Select(async term =>
            {
                var currentReport = reports.FirstOrDefault(rp => rp.SchoolId == term.SchoolId && rp.GradingTerm == term.GradingTermId);

                if (currentReport != null && !IsCurrentTerm(term.SchoolId, term.GradingTermId))
                {
                    return;
                }

                var report = currentReport ?? new SchoolReport(term.SchoolId, term.GradingTermId) { DateCreated = DateTime.UtcNow };
                report.DateUpdated = DateTime.UtcNow;

                await UpdateSchoolReport(report, xFacultyLookup[report.SchoolId]);
                await _documentDbRepository.UpsertAsync(report);
            }).ToArray();

            Task.WaitAll(tasks);
        }

        private async Task UpdateSchoolReport(SchoolReport report, IEnumerable<long> xschoolFaculties)
        {
            var inTermClassIds = await _mainDbContext.Classes
                .Where(cl => cl.SchoolId == report.SchoolId && cl.GradingTerm == report.GradingTerm && !cl.IsTemplate)
                .Select(cl => cl.ClassId)
                .ToListAsync();

            var facultyIds = await _mainDbContext.ClassFaculties
                .Where(cf => inTermClassIds.Contains(cf.ClassId))
                .Select(cf => cf.FacultyId)
                .ToListAsync();

            var activities = await _mainDbContext.Activities
                .Where(act => inTermClassIds.Contains(act.ClassId))
                .Select(act => new { act.ActivityId, act.ClassId, IsOpen = act.IsOpen() })
                .ToListAsync();

            var activeClassIds = activities.Select(s => s.ClassId).Distinct().ToHashSet();
            var activityIds = activities.Select(s => s.ActivityId).ToHashSet();
            report.ActivityCount = new ActivePassiveCount(activityIds.Count, activities.Count(a => a.IsOpen));

            var activeFaculties = await _mainDbContext.ClassFaculties
                .Where(cf => activeClassIds.Contains(cf.ClassId))
                .Select(cf => cf.FacultyId)
                .Distinct()
                .ToArrayAsync();

            report.FacultyCount = new ActivePassiveCount(facultyIds.Count, activeFaculties.Count());

            var activeXFaculties = xschoolFaculties.Where(facId => xschoolFaculties.Contains(facId)).ToArray();
            report.XSchoolFaculties = (activeXFaculties, xschoolFaculties.Except(activeXFaculties).ToArray());

            var studentCount = await _mainDbContext.SchoolStudents
                .Where(ss => ss.SchoolId == report.SchoolId)
                .Select(ss => ss.StudentId)
                .Distinct().CountAsync();
            report.StudentCount = new ActivePassiveCount(studentCount, studentCount);

            var scores = await _mainDbContext.ActivityScores
                .Where(asc => activityIds.Contains(asc.ActivityId))
                .ToListAsync();

            report.FiveCScore = new FiveCScore(scores);
        }

        internal async Task<IEnumerable<(Faculty, long, ActivePassiveCount)>> GetFacultiesBySchool(long[] schoolIds)
        {
            var fs = await (
                from sf in _mainDbContext.SchoolFaculties
                join f in _mainDbContext.Faculties on sf.FacultyId equals f.FacultyId
                join u in _mainDbContext.Users on f.FacultyId equals u.Id
                join s in _mainDbContext.Schools on sf.SchoolId equals s.SchoolId
                where schoolIds.Contains(sf.SchoolId)
                select new GeneralRecord { SchoolFaculty = sf, Faculty = f, User = u, School = s }
            ).ToListAsync();

            var rs = await Task.WhenAll(fs.Select(async r => (
                r.Faculty,
                r.SchoolFaculty.SchoolId,
                await GetActivityStatsOfFaculty(r.SchoolFaculty.FacultyId, r.SchoolFaculty.SchoolId, r.School.CurrentGradingTerm))));
            return rs;
        }

        internal async Task<ActivePassiveCount> GetActivityStatsOfFaculty(long facultyId, long schoolId, long gradingTermId)
        {
            var openActivities = (await _activityService.GetActivitiesOfFaculty(facultyId, gradingTermId, Activity.IsOpenExpr)).Where(x => x.Type == ActivityType.Assessment || x.Type == ActivityType.Assignment);
            var closedActivities = (await _activityService.GetActivitiesOfFaculty(facultyId, gradingTermId, Activity.Not(Activity.IsOpenExpr))).Where(x => x.Type == ActivityType.Assessment || x.Type == ActivityType.Assignment);

            return new ActivePassiveCount(openActivities.Count()+closedActivities.Count(), openActivities.Count());
        }

        public async Task<IEnumerable<(Faculty, long, ActivePassiveCount)>> GetFacultiesForSchoolAdmin(long userId)
        {
            var schoolIds = await _schoolService.GetSchoolsOfSchoolAdmin(userId).Select(sch => sch.SchoolId).ToArrayAsync();
            return (await GetFacultiesBySchool(schoolIds));
        }

        public async Task<IEnumerable<(Faculty, long, ActivePassiveCount)>> GetFaculties(long districtId)
        {
            var schoolIds = await _schoolService.GetSchoolsInDistrict(districtId).Select(sch => sch.SchoolId).ToArrayAsync();
            return (await GetFacultiesBySchool(schoolIds));
        }

        public async Task<IEnumerable<School>> GetSchools(long districtId)
        {
            return await _mainDbContext.Schools.Where(sch => sch.DistrictId == districtId).ToArrayAsync();
        }

        public async Task<PowerSearchResult> PowerSearch(long userId, string userRole, string searchText, int maxItems, long? schoolId)
        {
            var searchResult = new PowerSearchResult();
            IQueryable<Class> allClasses;
            IQueryable<Student> allStudents;
            IQueryable<School> allSchools;
            IQueryable<Faculty> allFaculties;

            long? currentGradingTerm = !schoolId.HasValue ? (long?) null : (
                await _mainDbContext.Schools.Where(s => s.SchoolId == schoolId.Value).Select(s => s.CurrentGradingTerm).FirstOrDefaultAsync()
            );

            switch (userRole)
            {
                case ApplicationRoleValue.Admin:
                    var districtId = await _mainDbContext.Users.Where(u => u.Id == userId).Select(u => u.DistrictId).FirstOrDefaultAsync();
                    allSchools = _mainDbContext.Schools.Where(s => s.DistrictId == districtId);

                    allFaculties = _mainDbContext.Faculties
                            .Join(_mainDbContext.SchoolFaculties
                                .Where(sf => allSchools.Any(s => s.SchoolId == sf.SchoolId)), f => f.FacultyId, sf => sf.FacultyId, (f, sf) => f).Distinct();

                    searchResult.Schools = await allSchools.Where(s => s.SchoolName.Contains(searchText, StringComparison.CurrentCultureIgnoreCase))
                                            .Take(maxItems).ToListAsync();

                    searchResult.Faculties = await allFaculties.Join(_mainDbContext.Users
                                            .Where(x => x.FullName.IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) != -1)
                                        , faculty => faculty.FacultyId, user => user.Id
                                        , (faculty, user) => (new Tuple<Faculty, ApplicationUser>(faculty, user)).ToValueTuple()).Take(maxItems).ToListAsync();

                    (await _studentService.GetStudentsOfDistrict(districtId)
                        .Where(r => r.User.FullName.Contains(searchText, StringComparison.InvariantCultureIgnoreCase))
                        .Select(r => new GeneralRecord { Student = r.Student, User = r.User })
                        .Take(maxItems)
                        .ToListAsync()
                        ).Select(r => (r.Student, r.User));

                    searchResult.Students = (await _studentService.GetStudentsOfDistrict(districtId)
                        .Where(r => r.User.FullName.Contains(searchText, StringComparison.InvariantCultureIgnoreCase))
                        .Select(r => new GeneralRecord { Student = r.Student, User = r.User })
                        .Take(maxItems)
                        .ToListAsync()
                        ).Select(r => (r.Student, r.User));
                    break;
                case ApplicationRoleValue.SchoolAdmin:
                    var schoolList = _mainDbContext.SchoolAdmins.Where(u => u.UserId == userId);

                    allFaculties = _mainDbContext.Faculties
                            .Join(_mainDbContext.SchoolFaculties
                                .Where(sf => schoolList.Any(s => s.SchoolId == sf.SchoolId)), f => f.FacultyId, sf => sf.FacultyId, (f, sf) => f).Distinct();

                    allStudents = _mainDbContext.Students
                                    .Join(_mainDbContext.SchoolStudents
                                    .Where(ss => schoolList.Any(s => s.SchoolId == ss.SchoolId)), s => s.StudentId, ss => ss.StudentId, (s, ss) => s).Distinct();

                    searchResult.Schools = await _mainDbContext.Schools
                        .Where(s => schoolList.Any(x => x.SchoolId == s.SchoolId)
                                && s.SchoolName.Contains(searchText, StringComparison.CurrentCultureIgnoreCase))
                        .Take(maxItems).ToListAsync();

                    searchResult.Faculties = await allFaculties.Join(_mainDbContext.Users
                                            .Where(x => x.FullName.IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) != -1)
                                        , faculty => faculty.FacultyId, user => user.Id
                                        , (faculty, user) => (new Tuple<Faculty, ApplicationUser>(faculty, user)).ToValueTuple()).Take(maxItems).ToListAsync();

                    searchResult.Students = await allStudents.Join(_mainDbContext.Users
                                           .Where(x => x.FullName.IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) != -1)
                                       , student => student.StudentId, user => user.Id
                                       , (student, user) => (new Tuple<Student, ApplicationUser>(student, user)).ToValueTuple()).Take(maxItems).ToListAsync();
                    break;
                case ApplicationRoleValue.Faculty:
                    if (schoolId == null || currentGradingTerm == null) break;
                    var facultyId = await _mainDbContext.Faculties.Where(x => x.FacultyId == userId).Select(x => x.FacultyId).FirstOrDefaultAsync();
                    allClasses = _mainDbContext.Classes.Where(c => c.SchoolId == schoolId && c.GradingTerm==currentGradingTerm)
                        .Join(_mainDbContext.ClassFaculties.Where(cf => cf.FacultyId == facultyId), c => c.ClassId, cf => cf.ClassId, (c, cf) => c)
                        .Distinct();

                    allStudents = _mainDbContext.Students
                        .Join(_mainDbContext.ClassStudents.Where(f => allClasses.Any(y => y.ClassId == f.ClassId)),
                                student => student.StudentId,
                                classStudent => classStudent.StudentId,
                                (student, classStudent) => student).Distinct();

                    searchResult.Students = await allStudents.Join(_mainDbContext.Users
                                            .Where(x => x.FullName.IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) != -1)
                                        , student => student.StudentId, user => user.Id
                                        , (student, user) => (new Tuple<Student, ApplicationUser>(student, user)).ToValueTuple()).Take(maxItems).ToListAsync();

                    searchResult.Classes = await allClasses.Where(x => x.ClassName.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)).Take(maxItems).ToListAsync();

                    searchResult.Activities = await _mainDbContext.Activities.Where(x =>
                        allClasses.Any(y => y.ClassId == x.ClassId) &&
                        x.CreatedBy == userId &&
                        x.Title.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)
                        ).Take(maxItems).ToListAsync();
                    break;
                case ApplicationRoleValue.Parent:
                    var parentId = userId;

                    var students = (await
                        _studentService.GetStudentsOfParent(parentId)
                        .Where(r => r.User.FullName.Contains(searchText, StringComparison.InvariantCultureIgnoreCase))
                        .Take(maxItems)
                        .ToListAsync()
                    ).Select(r => (r.Student, r.User));

                    searchResult.Students = students;

                    break;
                case ApplicationRoleValue.Student:
                    if (schoolId == null || currentGradingTerm == null) break;
                    var studentId = userId;
                    allClasses = _mainDbContext.Classes.Where(c => c.SchoolId == schoolId && c.GradingTerm == currentGradingTerm)
                        .Join(_mainDbContext.ClassStudents.Where(cf => cf.StudentId == studentId), c => c.ClassId, cf => cf.ClassId, (c, cf) => c)
                        .Distinct();

                    searchResult.Classes = await allClasses.Where(x => x.ClassName.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)).Take(maxItems).ToListAsync();

                    searchResult.Activities = await _mainDbContext.Activities.Where(a =>
                        allClasses.Any(y => y.ClassId == a.ClassId) &&
                        a.IsPublic() &&
                        a.Title.Contains(searchText, StringComparison.CurrentCultureIgnoreCase)
                        ).Take(maxItems).ToListAsync();
                    break;
                default:
                    throw new InvalidEnumArgumentException();
            }
            return searchResult;
        }
    }
}
