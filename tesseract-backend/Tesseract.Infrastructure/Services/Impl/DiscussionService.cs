﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class DiscussionService : IDiscussionService
    {
        private readonly MainDbContext _mainDbContext;
        private readonly ILogger<DiscussionService> _logger;

        public DiscussionService(
            MainDbContext mainDbContext,
            ILogger<DiscussionService> logger)
        {
            _mainDbContext = mainDbContext;
            _logger = logger;
        }

        public async Task<Discussion> Create(Discussion item)
        {
            return await _mainDbContext.AddAsync(item, true);
        }

        public async Task<Discussion> Update(Discussion item)
        {
            await _mainDbContext.UpdateAsync(item, true);
            return item;
        }

        public async Task Delete(Discussion item)
        {
            await _mainDbContext.DeleteAsync(item, true);
        }

        public async Task<Discussion> Delete(long dicussionId)
        {
            var item = await GetById(dicussionId);
            if (item == null) return item;

            await Delete(item);
            return item;
        }

        public async Task<Discussion> GetById(long dicussionId)
        {
            return await _mainDbContext.Discussions
                .FirstOrDefaultAsync(e => e.DiscussionId == dicussionId);
        }


        public async Task<IList<Discussion>> GetDiscussionsOfClass(long classId)
        {
            return await _mainDbContext.Discussions
                .Where(e => e.ClassId == classId)
                .OrderBy(e => e.DateCreated)
                .ToListAsync();
        }

        public async Task<IList<Discussion>> GetDiscussionsOfActivity(long activityId)
        {
            return await _mainDbContext.Discussions
                .Where(e => e.ActivityId == activityId)
                .OrderBy(e => e.DateCreated)
                .ToListAsync();
        }

        public async Task<IList<Discussion>> GetDiscussionsOfClassAsStudent(long classId)
        {
            return await _mainDbContext.Discussions
                .Where(e => e.ClassId == classId)
                .Where(IsPublishForStudent)
                .OrderBy(e => e.DateCreated)
                .ToListAsync();
        }

        public async Task<IList<Discussion>> GetDiscussionsOfActivityAsStudent(long activityId)
        {
            return await _mainDbContext.Discussions
                .Where(e => e.ActivityId == activityId)
                .Where(IsPublishForStudent)
                .OrderBy(e => e.DateCreated)
                .ToListAsync();
        }

        public async Task<Discussion> GetDiscussionFromDiscussionActivity(long discussionActivityId)
        {
            return await _mainDbContext.Discussions.FirstOrDefaultAsync(x => x.DiscussionActivityId == discussionActivityId);
        }

        public Task<ActivityCountDiscussion[]> CountDiscussionOfActivity(long[] activityIds) {
            return (
                from d in _mainDbContext.Discussions
                where activityIds.Contains(d.ActivityId)
                group d by d.ActivityId into g
                select new ActivityCountDiscussion { ActivityId = g.Key, DiscussionCount = g.Count() }
            ).ToArrayAsync();
        }

        static readonly Expression<Func<Discussion, bool>> IsPublishForStudent = (e => e.StartTime.HasValue && e.StartTime.Value < DateTime.UtcNow);

        public string GenerateThreadId()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}
