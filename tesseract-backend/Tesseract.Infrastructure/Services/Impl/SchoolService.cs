﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class SchoolService : ISchoolService
    {
        private readonly MainDbContext _mainDbContext;

        public SchoolService(MainDbContext mainDbContext)
        {
            _mainDbContext = mainDbContext;
        }

        public async Task<School> CreateSchool(School school, GradingTerm currentGradingTerm)
        {
            var newSchool= await _mainDbContext.AddAsync(school, false);
            var newGradingTerm = await _mainDbContext.AddAsync(currentGradingTerm, false);
            await _mainDbContext.SaveChangesAsync();

            newSchool.CurrentGradingTerm = newGradingTerm.GradingTermId;
            newGradingTerm.SchoolId = newSchool.SchoolId;

            await _mainDbContext.UpdateAsync(newSchool, false);
            await _mainDbContext.UpdateAsync(newGradingTerm, false);
            await _mainDbContext.SaveChangesAsync();
            return newSchool;
        }

        public async Task DeleteSchool(School school)
        {
            //TODO: Remove all data related to School.
            var classItems = await _mainDbContext.Classes.Where(t => t.SchoolId == school.SchoolId).ToListAsync();
            foreach ( var c in classItems)
            {
                await _mainDbContext.DeleteAsync(c, false);
            }

            await _mainDbContext.DeleteAsync(school, false);
            await _mainDbContext.SaveChangesAsync();
        }

        public async Task<School> GetSchoolById(long schoolId)
        {
            var s = await _mainDbContext.Schools.FirstOrDefaultAsync(t => t.SchoolId == schoolId);
            if (s == null) throw new Exception($"School ID: {schoolId} not found.");
            return s;
        }

        public Task<School> GetSchoolInDistrict(long districtId, long schoolId)
        {
            return _mainDbContext.Schools.FirstOrDefaultAsync(t => t.SchoolId == schoolId && t.DistrictId == districtId);
        }

        public Task<School> GetSchoolOfSchoolAdmin(long userId, long schoolId)
        {
            return GetSchoolsOfSchoolAdmin(userId).FirstOrDefaultAsync(t => t.SchoolId == schoolId);
        }

        public IQueryable<School> GetSchoolsInDistrict(long districtId)
        {
            return _mainDbContext.Schools.Where(t => t.DistrictId == districtId);
        }

        public async Task<IList<School>> GetSchoolsOfFaculty(long facultyId)
        {
            return (await QuerySchoolsOfFaculty(facultyId).Distinct().ToListAsync());
        }
        public Task<School> GetSchoolOfFaculty(long facultyId, long schoolId)
        {
            return QuerySchoolsOfFaculty(facultyId).FirstOrDefaultAsync(s => s.SchoolId == schoolId);
        }
        private IQueryable<School> QuerySchoolsOfFaculty(long facultyId)
        {
            return (
                from sf in _mainDbContext.SchoolFaculties
                join s in _mainDbContext.Schools on sf.SchoolId equals s.SchoolId
                where sf.FacultyId == facultyId
                select s
            );
        }

        public async Task<IList<School>> GetSchoolsOfStudent(long studentId)
        {
            return (await (
                from ss in _mainDbContext.SchoolStudents
                join s in _mainDbContext.Schools on ss.SchoolId equals s.SchoolId
                where ss.StudentId == studentId
                select s
            ).Distinct().ToListAsync());
        }

        public IQueryable<GeneralRecord> GetCurrentSchoolsOfStudent(long studentId)
        {
            return (
                from ss in _mainDbContext.SchoolStudents
                join school in _mainDbContext.Schools on ss.SchoolId equals school.SchoolId
                where ss.StudentId == studentId &&
                      school.CurrentGradingTerm == ss.GradingPeriod
                select new GeneralRecord { School = school, SchoolStudent = ss }
            );
        }

        public async Task RegisterFacultyAsync(long schoolId, long facultyId)
        {
            await GetSchoolById(schoolId);

            var f = await _mainDbContext.Faculties.FirstOrDefaultAsync(c => c.FacultyId == facultyId);
            if (f == null)
            {
                throw new Exception($"Faculty ID: {facultyId} not found.");
            }

            await _mainDbContext.AddLinkEntityAsync(new SchoolFaculty() { SchoolId = schoolId, FacultyId = facultyId }, true);
        }

        public async Task RegisterStudentAsync(SchoolStudent schoolStudent)
        {
            await GetSchoolById(schoolStudent.SchoolId);

            var student = await _mainDbContext.Students.FirstOrDefaultAsync(c => c.StudentId == schoolStudent.StudentId);
            if (student == null)
            {
                throw new Exception($"Student ID: {schoolStudent.StudentId} not found.");
            }
            await _mainDbContext.AddLinkEntityAsync(schoolStudent, true);
        }

        public async Task<School> UpdateSchoolAsync(School school)
        {
            var s = await GetSchoolById(school.SchoolId);

            s.SchoolName = school.SchoolName;
            s.CurrentGradingTerm = school.CurrentGradingTerm;
            s.LogoUrl = school.LogoUrl;
            s.IconUrl = school.IconUrl;

            await _mainDbContext.UpdateAsync(s, true);
            return s;
        }

        public IQueryable<School> GetSchoolsOfSchoolAdmin(long userId)
        {
            return (
                from sa in _mainDbContext.SchoolAdmins
                join s in _mainDbContext.Schools on sa.SchoolId equals s.SchoolId
                where sa.UserId == userId
                select s
            );
        }
        public IQueryable<School> GetSchoolsOfSchoolAdmin(long userId, long districtId)
        {
            return (GetSchoolsOfSchoolAdmin(userId).Where(s => s.DistrictId == districtId));
        }
    }
}
