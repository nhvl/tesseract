﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Config;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class FileEmailSenderService : EmailSenderService
    {
        private const string MailDirectory = @"C:\tesseract-mail\";

        public FileEmailSenderService(SMTPServerSettings smtpServerSettings) : base(smtpServerSettings){
        }

        public override async Task SendEmailAsync(
            string subject,
            string body,
            string fromAddress,
            string fromName,
            string toAddress,
            string toName,
            string replyToAddress = null,
            string replyToName = null,
            IEnumerable<string> bcc = null,
            IEnumerable<string> cc = null,
            IEnumerable<string> attachmentFilePaths = null,
            string attachmentFileName = null,
            int attachedDownloadId = 0,
            IDictionary<string, string> headers = null)
        {
            var message = ToMessage(
                subject,
                body,
                fromAddress,
                fromName,
                toAddress,
                toName,
                replyToAddress,
                replyToName,
                bcc,
                cc,
                headers
            );

            System.IO.Directory.CreateDirectory(MailDirectory);

            using (var smtpClient = new SmtpClient
            {
                DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory,
                PickupDirectoryLocation = MailDirectory,
            })
            {
                await smtpClient.SendMailAsync(message);
            }
        }
    }
}
