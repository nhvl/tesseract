﻿using System;
using System.Data.Common;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Data.NoSql;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class SubmissionService: ISubmissionService
    {
        private readonly MainDbContext _mainDbContext;
        private readonly IDocumentDBRepository _documentDbRepository;
        private readonly ILogger<SubmissionService> _logger;

        public SubmissionService(
            MainDbContext mainDbContext,
            IDocumentDBRepository documentDbRepository,
            ILogger<SubmissionService> logger
            )
        {
            _mainDbContext = mainDbContext;
            _documentDbRepository = documentDbRepository;
            _logger = logger;
        }

        public async Task<Submission> Get(long activityId, long studentId)
        {
            var doc = await _documentDbRepository.GetOne<Submission>(d => d.ActivityId == activityId && d.StudentId == studentId);
            return doc;
        }

        public async Task<Submission> CreateOrUpdate(Submission doc, long? userId)
        {
            var s = await Get(doc.ActivityId, doc.StudentId);
            if (s == null)
            {
                doc.CreatedBy = userId;
                doc.UpdatedBy = userId;
                return await Create(doc);
            }
            s.Content = doc.Content;
            s.Attachments = doc.Attachments;
            s.UpdatedBy = userId;
            return await Update(s);
        }

        public async Task<Submission> Create(Submission doc)
        {
            if (doc.ActivityId < 1 || doc.StudentId < 1) throw new ArgumentException($"ActivityId or StudentId is required.");
            doc.DateCreated = DateTime.UtcNow;
            doc.DateUpdated = DateTime.UtcNow;

            var document = await _documentDbRepository.InsertAsync(doc);
            Submission d2 = (dynamic) (document);
            return d2;
        }

        public async Task<Submission> Update(Submission doc)
        {
            doc.DateUpdated = DateTime.UtcNow;
            var document = await _documentDbRepository.UpsertAsync(doc);
            Submission d2 = (dynamic)(document);
            return d2;
        }

        public async Task<Submission> Delete(long activityId, long studentId)
        {
            var doc = await Get(activityId, studentId);
            if (doc == null) return null;

            await _documentDbRepository.DeleteAsync(doc);
            return doc;
        }
    }
}
