﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class EmailTemplateService : IEmailTemplateService
    {
        private readonly MainDbContext _mainDbContext;

        public EmailTemplateService(MainDbContext mainDbContext)
        {
            _mainDbContext = mainDbContext;
        }
        public async Task<EmailTemplate> GetEmailTemplateByNameAsync(string name)
        {
            return await _mainDbContext.EmailTemplates.FirstOrDefaultAsync(x => x.Name == name);
        }
    }
}
