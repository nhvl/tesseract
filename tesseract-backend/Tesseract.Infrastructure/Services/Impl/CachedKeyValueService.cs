﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Tesseract.Infrastructure.Cryptography.Md5HashExtension;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Infrastructure.Models;
using Tesseract.Infrastructure.Configuration;
using Tesseract.Core.Exceptions;
using System;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class CachedKeyValueService : IKeyValueService
    {
        private readonly MainDbContext _mainDbContext;
        private readonly IDistributedCache _cache;

        public CachedKeyValueService(MainDbContext mainDbContext, IDistributedCache cache)
        {
            _mainDbContext = mainDbContext;
            _cache = cache;
        }

        public async Task<string> GetValueFromKey(long? districtId, long? schoolId, string keyName)
        {
            return await _cache.GetAsync(
                GetCacheKey2(districtId, schoolId, keyName),
                () => GetValueFromKeyNoCache(districtId, schoolId, keyName));
        }
        public async Task<IEnumerable<string>> GetValuesFromKey(long? districtId, long? schoolId, string keyName)
        {
            return await _cache.GetAsync(
                GetCacheKey(districtId, schoolId, keyName),
                () => GetValuesFromKeyNoCache(districtId, schoolId, keyName));
        }

        private Task<List<string>> GetValuesFromKeyNoCache(long? districtId, long? schoolId, string keyName)
        {
            return _mainDbContext.KeyValues
                .Where(x => x.DistrictID == districtId &&
                            x.SchoolID == schoolId &&
                            x.KeyName == keyName)
                .OrderBy(x => x.OrderIndex)
                .Select(x => x.TextValue)
                .ToListAsync();
        }
        private Task<string> GetValueFromKeyNoCache(long? districtId, long? schoolId, string keyName)
        {
            return _mainDbContext.KeyValues
                .Where(x => x.DistrictID == districtId &&
                            x.SchoolID == schoolId &&
                            x.KeyName == keyName)
                .OrderBy(x => x.OrderIndex)
                .Select(x => x.TextValue)
                .FirstOrDefaultAsync();
        }
        private static string GetCacheKey(long? districtId, long? schoolId, string keyName)
        {
            return $"{districtId}-{schoolId}-{keyName}".ToMD5Hash();
        }
        private static string GetCacheKey2(long? districtId, long? schoolId, string keyName)
        {
            return $"{districtId}-{schoolId}-{keyName}-2".ToMD5Hash();
        }

        public async Task<GradeRange[]> GetDefaultGradeRange(long? districtId, long? schoolId)
        {
            var defaultRange = new List<GradeRange>();
            if (schoolId.HasValue)
            {
                defaultRange = new List<GradeRange>();
                var school = await _mainDbContext.Schools.FirstOrDefaultAsync(s => s.SchoolId == schoolId.Value);
                if (school != null)
                {
                    foreach(var gradeRange in Enum.GetValues(typeof(E_DefaultGradeRange)).Cast<E_DefaultGradeRange>())
                    {
                        var percentGrade = await this.GetValueFromKey(null, schoolId, EnumExtensions.GetDescriptionFromEnumValue(gradeRange));
                        if (string.IsNullOrWhiteSpace(percentGrade))
                        {
                            percentGrade = await this.GetValueFromKey(school.DistrictId, null, EnumExtensions.GetDescriptionFromEnumValue(gradeRange));
                        }
                        if (string.IsNullOrWhiteSpace(percentGrade))
                        {
                            percentGrade = gradeRange.ToString("D");
                        }
                        defaultRange.Add(new GradeRange
                        {
                            LetterGrade = EnumExtensions.GetDescriptionFromEnumValue(gradeRange),
                            PercentGrade = decimal.Parse(percentGrade.Trim())
                        });
                    }
                }
            }

            if (districtId.HasValue)
            {
                defaultRange = new List<GradeRange>();
                foreach (var gradeRange in Enum.GetValues(typeof(E_DefaultGradeRange)).Cast<E_DefaultGradeRange>())
                {
                    var percentGrade = await this.GetValueFromKey(districtId, null, EnumExtensions.GetDescriptionFromEnumValue(gradeRange));
                    if (string.IsNullOrWhiteSpace(percentGrade))
                    {
                        percentGrade = gradeRange.ToString("D");
                    }
                    defaultRange.Add(new GradeRange
                    {
                        LetterGrade = EnumExtensions.GetDescriptionFromEnumValue(gradeRange),
                        PercentGrade = decimal.Parse(percentGrade.Trim())
                    });
                }
            }

            return defaultRange.OrderByDescending(x => x.PercentGrade).ToArray();
        }
        public async Task<GradeRange[]> SetDefaultGradeRange(long? districtId, long? schoolId, GradeRange[] gradeRange)
        {
            _mainDbContext.KeyValues.RemoveRange(_mainDbContext.KeyValues.Where(x => x.DistrictID == x.DistrictID && x.SchoolID == schoolId));
            await _mainDbContext.SaveChangesAsync();
            await _mainDbContext.KeyValues.AddRangeAsync(gradeRange.Select((g, index) => new Core.Entities.Config.KeyValue
            {
                DistrictID = districtId,
                SchoolID = schoolId,
                KeyName = g.LetterGrade,
                TextValue = g.PercentGrade.ToString(),
                OrderIndex = index
            }));
            await _mainDbContext.SaveChangesAsync();
            return await GetDefaultGradeRange(districtId, schoolId);
        }
        public async Task<ScoreBadge> GetScoreBadge(long? districtId, long? schoolId) {
            var scoreBadge = new ScoreBadge();
            var colorValues = new List<string>();
            if (schoolId.HasValue)
            {
                colorValues = new List<string>();
                var school = await _mainDbContext.Schools.FirstOrDefaultAsync(s => s.SchoolId == schoolId.Value);
                if (school != null)
                {
                    foreach (var skill in Enum.GetValues(typeof(E_DefaultFiveCSkillsColor)).Cast<E_DefaultFiveCSkillsColor>())
                    {
                        var color = await this.GetValueFromKey(null, schoolId, EnumExtensions.GetDescriptionFromEnumValue(skill));
                        if (string.IsNullOrWhiteSpace(color))
                        {
                            color = await this.GetValueFromKey(school.DistrictId, null, EnumExtensions.GetDescriptionFromEnumValue(skill));
                        }
                        if (string.IsNullOrWhiteSpace(color))
                        {
                            color = EnumExtensions.GetColor(skill);
                        }
                        switch (skill)
                        {
                            case E_DefaultFiveCSkillsColor.Character: scoreBadge.CharacterColor = color; break;
                            case E_DefaultFiveCSkillsColor.Collaboration: scoreBadge.CollaborationColor = color; break;
                            case E_DefaultFiveCSkillsColor.Communication: scoreBadge.CommunicationColor = color; break;
                            case E_DefaultFiveCSkillsColor.Creativity: scoreBadge.CreativityColor = color; break;
                            case E_DefaultFiveCSkillsColor.CriticalThinking: scoreBadge.CriticalThinkingColor = color; break;
                        }
                    }
                }
            }

            if (districtId.HasValue)
            {
                colorValues = new List<string>();
                foreach (var skill in Enum.GetValues(typeof(E_DefaultFiveCSkillsColor)).Cast<E_DefaultFiveCSkillsColor>())
                {
                    var color = await this.GetValueFromKey(districtId, null, EnumExtensions.GetDescriptionFromEnumValue(skill));
                    if (string.IsNullOrWhiteSpace(color))
                    {
                        color = EnumExtensions.GetColor(skill);
                    }
                    switch (skill)
                    {
                        case E_DefaultFiveCSkillsColor.Character: scoreBadge.CharacterColor = color; break;
                        case E_DefaultFiveCSkillsColor.Collaboration: scoreBadge.CollaborationColor = color; break;
                        case E_DefaultFiveCSkillsColor.Communication: scoreBadge.CommunicationColor = color; break;
                        case E_DefaultFiveCSkillsColor.Creativity: scoreBadge.CreativityColor = color; break;
                        case E_DefaultFiveCSkillsColor.CriticalThinking: scoreBadge.CriticalThinkingColor = color; break;
                    }
                }
            }

            return scoreBadge;
        }

        public async Task<string[]> GetActivityCategoryColors(long? districtId, long? schoolId)
        {
            var colorValues = new List<string>();
            if (schoolId.HasValue)
            {
                colorValues = new List<string>();
                var school = await _mainDbContext.Schools.FirstOrDefaultAsync(s => s.SchoolId == schoolId.Value);
                if (school != null)
                {
                    foreach (var skill in Enum.GetValues(typeof(E_DefaultCategoryColor)).Cast<E_DefaultCategoryColor>())
                    {
                        var color = await this.GetValueFromKey(null, schoolId, EnumExtensions.GetDescriptionFromEnumValue(skill));
                        if (string.IsNullOrWhiteSpace(color))
                        {
                            color = await this.GetValueFromKey(school.DistrictId, null, EnumExtensions.GetDescriptionFromEnumValue(skill));
                        }
                        if (string.IsNullOrWhiteSpace(color))
                        {
                            color = EnumExtensions.GetColor(skill);
                        }
                        colorValues.Add(color);
                    }
                }
            }

            if (districtId.HasValue)
            {
                colorValues = new List<string>();
                foreach (var skill in Enum.GetValues(typeof(E_DefaultCategoryColor)).Cast<E_DefaultCategoryColor>())
                {
                    var color = await this.GetValueFromKey(districtId, null, EnumExtensions.GetDescriptionFromEnumValue(skill));
                    if (string.IsNullOrWhiteSpace(color))
                    {
                        color = EnumExtensions.GetColor(skill);
                    }
                    colorValues.Add(color);
                }
            }

            return colorValues.ToArray();
        }

        public async Task<string> GetGoogleTagManagerId(long? districtId, long? schoolId) {
            return (await GetValueFromKey(districtId, schoolId, KeyValueNames.GoogleTagManager));
        }
    }
}
