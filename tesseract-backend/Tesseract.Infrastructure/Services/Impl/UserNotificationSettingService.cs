﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class UserNotificationSettingService : IUserNotificationSettingService
    {
        private readonly MainDbContext _mainDbContext;

        public UserNotificationSettingService(MainDbContext mainDbContext)
        {
            _mainDbContext = mainDbContext;
        }

        public Task<List<UserNotificationSetting>> GetUserNotificationSettings(long userId)
        {
            return _mainDbContext.UserNotificationSettings.Where(s => s.UserId == userId).ToListAsync();
        }

        public Task<UserNotificationSetting> GetUserNotificationSettings(long userId, NotificationType type)
        {
            return _mainDbContext.UserNotificationSettings.Where(s => s.UserId == userId && s.Type == type).FirstOrDefaultAsync();
        }

        public async Task<UserNotificationSetting[]> SaveUserNotificationSettings(long userId, UserNotificationSetting[] settings)
        {
            var type2Settings = settings.ToDictionary(s => s.Type, s => s);

            var oldSettings = await GetUserNotificationSettings(userId);
            var type2OldSetting = oldSettings.ToDictionary(s => s.Type, s => s);

            foreach (var setting in oldSettings)
            {
                if (!type2Settings.ContainsKey(setting.Type))
                {
                    setting.SendType = NotificationSendType.DontSend;
                }
                else
                {
                    var newSetting = type2Settings[setting.Type];
                    setting.SendType = newSetting.SendType;
                    setting.SendTime = newSetting.SendTime;
                    setting.WeeklyDay = newSetting.WeeklyDay;
                }

                await _mainDbContext.UpdateAsync(setting, false);
            }

            var newSettings = settings.Where(s => type2OldSetting.ContainsKey(s.Type)).ToArray();
            foreach (var setting in newSettings)
            {
                setting.UserId = userId;
                await _mainDbContext.AddAsync(setting, false);
            }

            await _mainDbContext.SaveChangesAsync();

            return oldSettings.Concat(newSettings).ToArray();
        }

        public async Task<UserNotificationSetting> SaveUserNotificationSetting(long userId, NotificationType type, UserNotificationSetting setting)
        {
            var oldSettings = await GetUserNotificationSettings(userId, type);
            if (oldSettings == null)
            {
                setting.UserId = userId;
                setting.Type = type;
                await _mainDbContext.AddAsync(setting, true);
                return setting;
            }

            oldSettings.SendType = setting.SendType;
            oldSettings.SendTime = setting.SendTime;
            oldSettings.WeeklyDay = setting.WeeklyDay;
            await _mainDbContext.UpdateAsync(oldSettings, true);
            return oldSettings;
        }
    }
}
