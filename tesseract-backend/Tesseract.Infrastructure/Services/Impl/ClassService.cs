﻿#nullable enable

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class ClassService : IClassService
    {
        private readonly MainDbContext _mainDbContext;
        private readonly ILogger<ClassService> _logger;

        public ClassService(
            MainDbContext mainDbContext,
            ILogger<ClassService> logger)
        {
            _mainDbContext = mainDbContext;
            _logger = logger;
        }

        public async Task<Class> CreateClassAsync(Class classItem, long facultyId)
        {
            var schoolId = classItem.SchoolId;
            var school = await _mainDbContext.Schools.Where(t => t.SchoolId == schoolId)
                .Join(_mainDbContext.SchoolFaculties.Where(sf => sf.FacultyId == facultyId), s => s.SchoolId, sf => sf.SchoolId, (s, sf) => s)
                .FirstOrDefaultAsync();
            if (school == null)
            {
                throw new Exception($"School ID: {schoolId} not found.");
            }

            var maxSortIndex = await _mainDbContext.Classes
                                                   .Where(c => c.SchoolId == schoolId && c.GradingTerm == classItem.GradingTerm)
                                                   .Join(_mainDbContext.ClassFaculties.Where(cf => cf.FacultyId == facultyId),
                                                                                             c => c.ClassId,
                                                                                             cf => cf.ClassId,
                                                                                             (c, cf) => (int?)cf.SortIndex)
                                                   .MaxAsync() ?? 0;

            await _mainDbContext.AddAsync(classItem, false);

            await _mainDbContext.AddLinkEntityAsync(new ClassFaculty
            {
                ClassId = classItem.ClassId,
                FacultyId = facultyId,
                SortIndex = maxSortIndex + 1,
            }, false);

            await _mainDbContext.SaveChangesAsync();

            return classItem;
        }

        public async Task DeleteClassAsync(Class classItem)
        {
            //TODO: Remove all data related to Class.
            await _mainDbContext.DeleteAsync(classItem, true);
        }

        public async Task<Class?> UpdateClassAsync(Class classItem)
        {
            var s = await GetClassAsync(classItem.ClassId);
            if (s == null) return null;

            s.ClassName   = classItem.ClassName;
            s.Period      = classItem.Period;
            s.GradingTerm = classItem.GradingTerm;
            s.Description = classItem.Description;
            s.GradeRanges = classItem.GradeRanges;

            await _mainDbContext.UpdateAsync(s, true);

            return s;
        }

        public async Task UpdateClasses(IEnumerable<Class> classes, IEnumerable<ClassFaculty> classFaculties)
        {
            foreach (var c in classes)
            {
                await _mainDbContext.UpdateAsync(c, false);
            }

            foreach (var cf in classFaculties)
            {
                await _mainDbContext.UpdateLinkEntityAsync(cf, false);
            }

            await _mainDbContext.SaveChangesAsync();
        }

        public async Task UpdateClassStudents(IEnumerable<ClassStudent> classStudents)
        {
            foreach (var entity in classStudents)
            {
                await _mainDbContext.UpdateLinkEntityAsync(entity, false);
            }

            await _mainDbContext.SaveChangesAsync();
        }

        public Task<Class?> GetClassAsync(long classId)
        {
            return _mainDbContext.Classes.FirstOrDefaultAsync(t => t.ClassId == classId);
        }

        public async Task<IQueryable<GeneralRecord>> GetClassesOfStudent(long studentId, long schoolId)
        {
            var school = await _mainDbContext.Schools.FirstOrDefaultAsync(x => x.SchoolId == schoolId);
            if (school == null) throw new ArgumentException();
            var gradingTerm = school.CurrentGradingTerm;

            return (
                from cs in _mainDbContext.ClassStudents
                join s in _mainDbContext.Students on cs.StudentId equals s.StudentId
                join c in _mainDbContext.Classes on cs.ClassId equals c.ClassId
                where c.SchoolId == schoolId && c.GradingTerm == gradingTerm &&
                    s.StudentId == studentId
                select new GeneralRecord { ClassStudent = cs, Student = s, Class = c }
            );
        }

        public IQueryable<GeneralRecord> GetClassesOfFacultyInSchool(long schoolId, long facultyId)
        {
            return (
                from c in _mainDbContext.Classes
                join cf in _mainDbContext.ClassFaculties on c.ClassId equals cf.ClassId
                where c.SchoolId == schoolId
                   && cf.FacultyId == facultyId
                select new GeneralRecord { Class = c, ClassFaculty = cf }
            );
        }

        public IQueryable<GeneralRecord> GetClassesOfFacultyInSchool(long schoolId, long facultyId, long gradingTermId)
        {
            return GetClassesOfFacultyInSchool(schoolId, facultyId).Where(r => r.Class.GradingTerm == gradingTermId);
        }

        public IQueryable<GeneralRecord> GetClassesOfFacultyInSchool(long schoolId, long facultyId, long[] classIds)
        {
            return GetClassesOfFacultyInSchool(schoolId, facultyId).Where(r => classIds.Contains(r.Class.ClassId));
        }

        public IQueryable<GeneralRecord> GetClassesOfStudentInSchool(long schoolId, long studentId)
        {
            return (
                from c in _mainDbContext.Classes
                join cs in _mainDbContext.ClassStudents on c.ClassId equals cs.ClassId
                where c.SchoolId == schoolId
                   && cs.StudentId == studentId
                select new GeneralRecord { Class = c, ClassStudent = cs }
            );
        }
        public IQueryable<GeneralRecord> GetClassesOfStudentInSchool(long schoolId, long studentId, long gradingTermId)
        {
            return GetClassesOfStudentInSchool(schoolId, studentId).Where(r => r.Class.GradingTerm == gradingTermId);
        }
        public IQueryable<GeneralRecord> GetClassesOfStudentInSchool(long schoolId, long studentId, long[] classIds)
        {
            return GetClassesOfStudentInSchool(schoolId, studentId).Where(r => classIds.Contains(r.Class.ClassId));
        }

        public async Task<IList<Class>> GetClassOfSchool(long schoolId)
        {
            return await _mainDbContext.Classes.Where(t => t.SchoolId == schoolId).ToListAsync();
        }

        public async Task<IList<Class>> GetClassesById(long[] classIds) {
            var ps = await _mainDbContext.Classes
                .Where(c => classIds.Contains(c.ClassId))
                .ToListAsync();
            return ps;
        }

        public IQueryable<GeneralRecord> GetCurrentClassesOfStudent(long studentId)
        {
            return (
                from ss in _mainDbContext.SchoolStudents.Where(ss => ss.StudentId == studentId)
                join school in _mainDbContext.Schools on ss.SchoolId equals school.SchoolId
                join c in _mainDbContext.Classes on school.SchoolId equals c.SchoolId
                join cs in _mainDbContext.ClassStudents.Where(cs => cs.StudentId == studentId) on c.ClassId equals cs.ClassId
                where school.CurrentGradingTerm == ss.GradingPeriod
                select new GeneralRecord { School = school, SchoolStudent = ss, Class = c, ClassStudent = cs }
            );
        }

        public async Task RegisterFacultyAsync(long classId, long facultyId)
        {
            var classItem = await GetClassAsync(classId);
            if (classItem == null) throw new ApplicationException($"Class({classId}) not found");

            var schoolFaculty = await _mainDbContext.SchoolFaculties.FirstOrDefaultAsync(c => c.SchoolId == classItem.SchoolId && c.FacultyId == facultyId);
            if (schoolFaculty == null)
            {
                throw new Exception($"Faculty with FacultyId: {facultyId} does not belong to SchoolID: {classItem.SchoolId}.");
            }

            await _mainDbContext.AddLinkEntityAsync(new ClassFaculty() { ClassId = classId, FacultyId = facultyId }, true);
        }

        public async Task RegisterStudentAsync(long classId, long studentId)
        {
            var classItem = await GetClassAsync(classId);
            if (classItem == null) throw new ApplicationException($"Class({classId}) not found");

            var schoolStudent = await _mainDbContext.SchoolStudents.FirstOrDefaultAsync(c => c.SchoolId == classItem.SchoolId && c.StudentId == studentId);
            if (schoolStudent == null)
            {
                throw new Exception($"Student with StudentID: {studentId} does not belong to SchoolID: {classItem.SchoolId}.");
            }

            await _mainDbContext.AddLinkEntityAsync(new ClassStudent() { ClassId = classId, StudentId = studentId }, true);
        }
    }
}
