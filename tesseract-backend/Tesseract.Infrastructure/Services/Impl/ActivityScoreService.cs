﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class ActivityScoreService : IActivityScoreService
    {
        private readonly MainDbContext _mainDbContext;

        public ActivityScoreService(
            MainDbContext mainDbContext
            )
        {
            _mainDbContext = mainDbContext;
        }

        public async Task<ActivityScore> Create(ActivityScore score)
        {
            return await _mainDbContext.AddLinkEntityAsync(score, true);
        }

        public async Task<ActivityScore> Get(long activityId, long studentId)
        {
            var score = await _mainDbContext.ActivityScores
                .Where(s => s.ActivityId == activityId && s.StudentId == studentId)
                .FirstOrDefaultAsync();
            return score;
        }

        public async Task<IEnumerable<ActivityScore>> GetActivityScoreOfClass(long classId)
        {
            return await
                _mainDbContext.ActivityScores.Where(s =>
                    _mainDbContext.Activities
                        .Where(a => a.ClassId == classId)
                        .Select(a => a.ActivityId).Contains(s.ActivityId)
                )
                .ToListAsync();
        }
    }
}
