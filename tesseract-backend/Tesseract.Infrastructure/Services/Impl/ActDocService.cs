﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Infrastructure.Data.NoSql;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class ActDocService: IActDocService
    {
        private readonly IDocumentDBRepository _documentDbRepository;

        public ActDocService(IDocumentDBRepository documentDbRepository)
        {
            _documentDbRepository = documentDbRepository;
        }

        public async Task<ActDoc> Get(long activityId) => await _documentDbRepository.GetOne<ActDoc>(d => d.ActivityId == activityId);

        public async Task<ActDoc> Create(ActDoc doc)
        {
            Refine(doc);
            doc.DateCreated = doc.DateUpdated = DateTime.UtcNow;

            var document = await _documentDbRepository.InsertAsync(doc);
            return (dynamic) (document);
        }

        public async Task<ActDoc> Update(ActDoc doc)
        {
            Refine(doc);
            doc.DateUpdated = DateTime.UtcNow;

            dynamic document = await _documentDbRepository.UpsertAsync(doc);
            doc = document;

            return doc;
        }

        public async Task<ActDoc> DeleteByActivityId(long activityId)
        {
            ActDoc doc = await Get(activityId);
            if (doc == null) return null;

            await _documentDbRepository.DeleteAsync(doc);
            return doc;
        }

        public async  Task<StudentActDoc> GetOrCreateStudentDoc(long activityId, long studentId){
            var doc = await GetStudentDoc(activityId, studentId);

            if (doc != null) return doc;

            var facultyDoc = await Get(activityId);
            if (facultyDoc == null) return null;

            return await CreateStudentDoc(new StudentActDoc(facultyDoc)
            {
                StudentId = studentId, 
                ActivityId = activityId
            });
        }
        public async Task<StudentActDoc> GetStudentDoc(long activityId, long studentId) => await _documentDbRepository.GetOne<StudentActDoc>(d => d.ActivityId == activityId && d.StudentId == studentId);

        public async Task<StudentActDoc> CreateStudentDoc(StudentActDoc doc)
        {
            doc.DateCreated = doc.DateUpdated = DateTime.UtcNow;

            var document = await _documentDbRepository.InsertAsync(doc);
            return (dynamic)(document);
        }

        public async Task<StudentActDoc> UpdateStudentDoc(StudentActDoc doc)
        {
            doc.DateUpdated = DateTime.UtcNow;

            dynamic document = await _documentDbRepository.UpsertAsync(doc);
            return document;
        }

        public async Task<StudentActDoc> DeleteStudentDocByActivityId(long activityId, long studentId)
        {
            var doc = await GetStudentDoc(activityId, studentId);
            if (doc == null) return null;

            await _documentDbRepository.DeleteAsync(doc);
            return doc;
        }

        private static void Refine(ActDoc doc)
        {
            foreach (var item in doc.Items)
            {
                switch (item)
                {
                   case ChoiceActItem choiceItem:
                        choiceItem.Options = choiceItem.Options.Where(x => !string.IsNullOrWhiteSpace(x.Label) || !string.IsNullOrWhiteSpace(x.Image)).ToArray();
                        break;
                    case MatchActItem matchItem:
                        matchItem.MatchQuizzItems = matchItem.MatchQuizzItems
                                    .Where(x => (!string.IsNullOrWhiteSpace(x.Left) || !string.IsNullOrWhiteSpace(x.LeftImage))
                                        && (!string.IsNullOrWhiteSpace(x.Right) || !string.IsNullOrWhiteSpace(x.RightImage))).ToArray();
                        break;
                    case PollingActItem pollItem:
                        pollItem.PollItems = pollItem.PollItems.Where(x => !string.IsNullOrWhiteSpace(x.Label) || !string.IsNullOrWhiteSpace(x.Image)).ToArray();
                        break;
                }
            }
        }
    }
}
