﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Tesseract.Core.Entities.Config;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Infrastructure.Views.Emails;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class AppMailService : IAppMailService
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSenderService _emailSenderService;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly IDistrictService _districtService;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;
        private readonly ILogger<AppMailService> _logger;
        private readonly SMTPServerSettings _smtpServerSettings;

        public AppMailService(
            SignInManager<ApplicationUser> signInManager,
            IEmailSenderService emailSenderService,
            IEmailTemplateService emailTemplateService,
            IDistrictService districtService,
            IRazorViewToStringRenderer razorViewToStringRenderer,
            ILogger<AppMailService> logger,
            SMTPServerSettings smtpServerSettings
        )
        {
            _signInManager = signInManager;
            _emailSenderService = emailSenderService;
            _emailTemplateService = emailTemplateService;
            _districtService = districtService;
            _razorViewToStringRenderer = razorViewToStringRenderer;
            _logger = logger;
            _smtpServerSettings = smtpServerSettings;
        }

        public async Task<bool> NotifyStudentForNewParent(ApplicationUser student, ApplicationUser parent)
        {
            if (student == null) {
                _logger.LogError("{0}", new System.ArgumentNullException(nameof(student)));
                return false;
            }

            var template = await _emailTemplateService.GetEmailTemplateByNameAsync("Account.NotifyStudent");
            if (template == null) {
                _logger.LogError("{0}", new System.ArgumentNullException(nameof(template)));
                return false;
            }

            var mail = FillTemplate(template, new Dictionary<string, string> {
                { "{{STUDENT_NAME}}", student.FullName },
                { "{{PARENT_NAME}}", parent.FullName },
                { "{{PARENT_EMAIL}}", parent.Email },
            });

            return await SendMailToUser(student, mail.Subject, mail.Body);
        }

        public async Task<bool> ConfirmEmail(ApplicationUser user, string confirmEmailLink)
        {
            if (user == null) return false;

            var district = await _districtService.GetById(user.DistrictId);
            if (district == null) {
                _logger.LogError($"User District ({user.DistrictId}) not found");
                return false;
            }

            var url = new Uri(confirmEmailLink);
            var homeUrl = $"{url.Scheme}://{url.Host}:{url.Port}";

            const string subject = "Confirm your Account";
            var body = await _razorViewToStringRenderer.RenderViewToStringAsync("/Views/Emails/ConfirmEmail/ConfirmEmail.cshtml",
                new ConfirmEmailViewModel {
                    ConfirmEmailUrl = confirmEmailLink,
                    DistrictLogo    = district.LogoUrl,
                    HomeUrl         = homeUrl,
                });

            return await SendMailToUser(user, subject, body);
        }

        public async Task<bool> ForgotPassword(string email, string host)
        {
            var user = await _signInManager.UserManager.FindByNameAsync(email);
            return await ForgotPassword(user, host);
        }

        public async Task<bool> ForgotPassword(ApplicationUser user, string host)
        {
            if (user == null) return false;

            var district = await _districtService.GetById(user.DistrictId);
            if (district == null) {
                _logger.LogError($"User District ({user.DistrictId}) not found");
                return false;
            }

            var token = await _signInManager.UserManager.GeneratePasswordResetTokenAsync(user);

            host = host.StartsWith("http") ? host : $"https://{host}";
            var resetLink = $"{host}/reset-password?code={HttpUtility.UrlEncode(token)}";

            var subject = $"{district.DistrictName} - Your password reset instructions";
            var body = await _razorViewToStringRenderer.RenderViewToStringAsync("/Views/Emails/ForgotPassword/index.cshtml",
                new ForgotPasswordViewModel {
                    ResetPasswordUrl = resetLink,
                    DistrictName     = district.DistrictName,
                    DistrictLogo     = district.LogoUrl,
                    HomeUrl          = host,
                });

            return await SendMailToUser(user, subject, body);
        }

        public async Task<bool> NewUserSetPassword(ApplicationUser user, string host)
        {
            if (user == null) return false;

            var district = await _districtService.GetById(user.DistrictId);
            if (district == null) {
                _logger.LogError($"User District ({user.DistrictId}) not found");
                return false;
            }

            var token = await _signInManager.UserManager.GeneratePasswordResetTokenAsync(user);
            token = HttpUtility.UrlEncode(token);

            var resetLink = $"{host}/reset-password?code={token}";

            var subject = $"{district.DistrictName} - Account created";
            var body = await _razorViewToStringRenderer.RenderViewToStringAsync("/Views/Emails/NewUserSetPassword/index.cshtml",
                new NewUserSetPasswordViewModel {
                    ResetPasswordUrl = resetLink,
                    DistrictName = district.DistrictName,
                });

            return await SendMailToUser(user, subject, body);
        }

        private async Task<bool> SendMailToUser(ApplicationUser user, string subject, string body)
        {
            if (user == null) return false;

            await _emailSenderService.SendEmailAsync(
                subject, body,
                _smtpServerSettings.Email,
                _smtpServerSettings.FriendlyName,
                user.Email,
                user.FullName
            );
            return true;
        }

        private async Task<bool> SendMail(string toAddress, string toName,
            string subject, string body, 
            IEnumerable<string> attachmentFilePaths = null)
        {

            await _emailSenderService.SendEmailAsync(
                subject: subject, 
                body: body,
                fromAddress: _smtpServerSettings.Email,
                fromName: _smtpServerSettings.FriendlyName,
                toAddress: toAddress,
                toName: toName,
                attachmentFilePaths: attachmentFilePaths
            );
            return true;
        }

        private static EmailTemplate FillTemplate(EmailTemplate template, IDictionary<string, string> vm) {
            var body = template.Body;
            var subject = template.Subject;
            foreach(var (key, value) in vm) {
                body = body.Replace(key, value);
                subject = subject.Replace(key, value);
            }
            return new EmailTemplate {
                Body = body,
                Subject = subject,
            };
        }
    }
}
