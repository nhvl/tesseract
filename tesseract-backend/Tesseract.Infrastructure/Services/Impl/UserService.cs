﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Tesseract.Core.Entities.Identity;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class UserService : IUserService
    {
        private readonly MainDbContext _mainDbContext;
        private readonly ISecurityService _securityService;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly TesseractUserManager _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public UserService(
            MainDbContext mainDbContext,
            SignInManager<ApplicationUser> signInManager,
            ISecurityService securityService,
            RoleManager<ApplicationRole> roleManager
        )
        {
            _mainDbContext = mainDbContext;
            _signInManager = signInManager;
            _userManager = signInManager.UserManager as TesseractUserManager;
            _securityService = securityService;
            _roleManager = roleManager;
        }

        public async Task<IdentityResult> CreateUser(ApplicationUser user)
        {
            var password = _securityService.RandomPassword();
            return await _signInManager.UserManager.CreateAsync(user, password);
        }

        public async Task<IdentityResult> UpdateUser(
            ApplicationUser user,
            string originalEmail
        )
        {
            if (string.IsNullOrEmpty(originalEmail) || originalEmail != user.Email) {
                var changeEmailResult = await _signInManager.UserManager.SetEmailAsync(user, user.Email);
                if (!changeEmailResult.Succeeded) return changeEmailResult;
            }

            return await _signInManager.UserManager.UpdateAsync(user);
        }

        public Task<ApplicationUser> GetUserById(long userId)
        {
            return _mainDbContext.Users.FirstOrDefaultAsync(u => u.Id == userId);
        }

        public Task<List<ApplicationUser>> GetUsersById(long[] userIds)
        {
            return _mainDbContext.Users.Where(u => userIds.Contains(u.Id)).ToListAsync();
        }

        private Task<IList<string>> GetUserStringRoles(ApplicationUser user) {
            return _userManager.GetRolesAsync(user);
        }

        public async Task<ApplicationRole[]> GetUserRoles(ApplicationUser user) {
            var roles = await GetUserStringRoles(user);
            return await Task.WhenAll(roles.Select(r => _roleManager.FindByNameAsync(r)));
        }

        public async Task<ApplicationRole[]> GetUserCustomRoles(ApplicationUser user) {
            return (await GetUserRoles(user)).Where(r => !r.IsSystemRole).ToArray();
        }

        public async Task<Claim[]> GetUserClaims(ApplicationUser user) {
            var pUserClaims = _userManager.GetClaimsAsync(user);
            var roles = await GetUserRoles(user);
            var roleClaims = await GetRoleClaims(roles);

            var claims = (await pUserClaims).Concat(roleClaims)
                .Where(x => x.Type == Permission.ClaimType)
                .ToArray();

            return claims;
        }

        public async Task<Claim[]> GetRoleClaims(ApplicationRole[] roles) {
            return (await Task.WhenAll(roles.Select(r => _roleManager.GetClaimsAsync(r))))
                .SelectMany(xs => xs)
                .ToArray();
        }
    }
}
