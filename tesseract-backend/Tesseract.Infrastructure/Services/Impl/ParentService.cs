﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class ParentService : IParentService
    {
        private readonly MainDbContext _mainDbContext;

        public ParentService(MainDbContext mainDbContext)
        {
            _mainDbContext = mainDbContext;
        }

        public async Task<Parent> CreateParentAsync(Parent parent)
        {
            return (await _mainDbContext.AddAsync(parent, true));
        }

        public async Task DeleteParentAsync(Parent parent)
        {
            await _mainDbContext.DeleteAsync(parent, true);
        }

        public async Task<Parent> GetParentAsync(long parentId)
        {
            return await _mainDbContext.Parents.FirstOrDefaultAsync(f => f.ParentId == parentId);
        }

        public Task<Parent> GetParentByUserIdAsync(long userId)
        {
            return GetParentAsync(userId);
        }

        public async Task AddStudentForParent(long parentId, long studentId)
        {
            await _mainDbContext.ParentStudents.AddAsync(new ParentStudent { ParentId = parentId, StudentId = studentId });
        }

        public async Task UpdateParentAsync(Parent parent) => await _mainDbContext.UpdateAsync(parent);
    }
}
