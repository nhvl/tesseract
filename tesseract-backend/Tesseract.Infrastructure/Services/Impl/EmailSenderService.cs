﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Config;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class EmailSenderService : IEmailSenderService
    {
        private readonly SMTPServerSettings _smtpServerSettings;
        public EmailSenderService(SMTPServerSettings smtpServerSettings) => _smtpServerSettings = smtpServerSettings;

        public virtual async Task SendEmailAsync(
            string subject,
            string body,
            string fromAddress,
            string fromName,
            string toAddress,
            string toName,
            string replyToAddress = null,
            string replyToName = null,
            IEnumerable<string> bcc = null,
            IEnumerable<string> cc = null,
            IEnumerable<string> attachmentFilePaths = null,
            string attachmentFileName = null,
            int attachedDownloadId = 0,
            IDictionary<string, string> headers = null)
        {
            //only check if smtp server is sandbox test
            if(_smtpServerSettings.IsSandBox)
            {
                toAddress = _smtpServerSettings.ToValidSandBoxEmail;
            }
            var message = ToMessage(
                subject,
                body,
                fromAddress,
                fromName,
                toAddress,
                toName,
                replyToAddress,
                replyToName,
                bcc,
                cc,
                headers,
                attachmentFilePaths
            );

            using (var smtpClient = new SmtpClient
            {
                UseDefaultCredentials = _smtpServerSettings.UseDefaultCredentials,
                Host = _smtpServerSettings.Host,
                Port = _smtpServerSettings.Port,
                EnableSsl = _smtpServerSettings.EnableSsl,
                Credentials = _smtpServerSettings.UseDefaultCredentials ?
                    CredentialCache.DefaultNetworkCredentials :
                    new NetworkCredential(_smtpServerSettings.Username, _smtpServerSettings.Password)
            })
            {
                await smtpClient.SendMailAsync(message);
            }
        }

        protected static MailMessage ToMessage(
            string subject,
            string body,
            string fromAddress,
            string fromName,
            string toAddress,
            string toName,
            string replyToAddress = null,
            string replyToName = null,
            IEnumerable<string> bcc = null,
            IEnumerable<string> cc = null,
            IDictionary<string, string> headers = null,
            IEnumerable<string> attachmentFilePaths = null)
        {
            var message = new MailMessage
            {
                From = new MailAddress(fromAddress, fromName),
                To = {new MailAddress(toAddress, toName)},
                Subject = subject,
                Body = body,
                IsBodyHtml = true,
            };
            if (!string.IsNullOrEmpty(replyToAddress))
            {
                message.ReplyToList.Add(new MailAddress(replyToAddress, replyToName));
            }

            //BCC
            if (bcc != null)
            {
                foreach (var address in bcc.Where(bccValue => !string.IsNullOrWhiteSpace(bccValue)))
                {
                    message.Bcc.Add(address.Trim());
                }
            }

            //CC
            if (cc != null)
            {
                foreach (var address in cc.Where(ccValue => !string.IsNullOrWhiteSpace(ccValue)))
                {
                    message.CC.Add(address.Trim());
                }
            }

            //headers
            if (headers != null)
            {
                foreach (var (key, value) in headers)
                {
                    message.Headers.Add(key, value);
                }
            }
            
            //Attachments
            if (attachmentFilePaths != null)
            {
                foreach (var attachPath in attachmentFilePaths.Where(attachValue => !string.IsNullOrWhiteSpace(attachValue)))
                {
                    var attachment = new System.Net.Mail.Attachment(attachPath);
                    message.Attachments.Add(attachment);
                }
            }

            return message;
        }
    }
}
