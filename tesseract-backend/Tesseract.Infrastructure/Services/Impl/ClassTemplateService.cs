﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Exceptions;
using Tesseract.Core.Services.Interface.LearningManagement;
using Tesseract.Core.Services.Interface.Repository;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{

    public class ClassTemplateService : IClassTemplateService
    {
        private readonly ILogger<ClassService> _logger;
        private readonly IClassBuilder _classBuilder;
        private readonly IClassRepo _classRepo;
        private readonly IGradingTermRepo  _termRepo;

        public ClassTemplateService(ILogger<ClassService> logger, IClassBuilder classBuilder, IClassRepo classRepo, IGradingTermRepo termRepo)
        {
            _logger = logger;
            _classBuilder = classBuilder;
            _classRepo = classRepo;
            _termRepo = termRepo;
        }

        public async Task<Class> ApplyTemplate(long templateId, ApplyTemplateConfig config)
        {
            var classTemplate = await _classRepo.GetTemplate(templateId) ??  throw new ResourceNotFoundException($"{nameof(templateId)} #{templateId}");

            var facultyId = classTemplate.CreatedBy;
            if (facultyId == null) throw new ResourceNotFoundException($"{nameof(classTemplate.CreatedBy)}");

            var gradingTerm = await _termRepo.GetTerm(config.GradingTermId) ?? throw new ResourceNotFoundException($"{nameof(config.GradingTermId)} #{config.GradingTermId}");

            var newClass = await _classBuilder.BuildFromtemplate(classTemplate, gradingTerm, facultyId.Value, config.ClassName);

            return await _classRepo.AddClass(newClass, facultyId.Value);
        }

        public async Task CreateTemplate(long classId, long facultyId)
        {
            var classItem = await _classRepo.GetClass(classId);
            if (classItem == null)
            {
                _logger.LogWarning($"Can not create class template. Class #${classId} not existed.");
                return;
            };

            var templateClass = await _classBuilder.ExportTemplate(classItem, facultyId);

            await _classRepo.AddTemplate(templateClass);
        }

        public async Task DeleteTemplate(long templateId)
        {
            await _classRepo.DeleteTemplate(templateId);
        }

        public async Task<IEnumerable<Class>> GetTemplates(long facultyId) => await _classRepo.GetTemplates(facultyId);
    }
}
