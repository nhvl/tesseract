﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Services.Interface.Repository;
using Tesseract.Infrastructure.Data;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class GradingTermRepo : IGradingTermRepo
    {
        private readonly MainDbContext _mainDbContext;

        public GradingTermRepo(MainDbContext mainDbContext)
        {
            _mainDbContext = mainDbContext;
        }

        public async Task<GradingTerm> GetTerm(long GradingTermId)
        {
            return await _mainDbContext.GradingTerms.FirstOrDefaultAsync(term => term.GradingTermId == GradingTermId);
        }
    }
}
