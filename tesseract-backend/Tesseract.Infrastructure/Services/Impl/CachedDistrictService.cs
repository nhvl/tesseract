﻿using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Data.NoSql;
using Microsoft.Extensions.Caching.Distributed;
using Tesseract.Infrastructure.Cryptography.Md5HashExtension;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class CachedDistrictService : DistrictService
    {
        private readonly IDistributedCache _cache;

        public CachedDistrictService(
            MainDbContext mainDbContext, 
            IStudentService studentService,
            ISchoolService schoolService,
            IActivityService activityService,
            IDocumentDBRepository documentDbRepository, 
            IDistributedCache cache)
        : base(mainDbContext, studentService, schoolService, activityService, documentDbRepository)
        {
            _cache = cache;
        }

        static string GetDisctrictIdKey(long districtId) => $"district_districtId_{districtId}".ToMD5Hash();
        static string GetDisctrictDomainKey(string domain) => $"district_domain_{domain}".ToMD5Hash();
        private async Task Cache(District district) {
            if (district == null) return;
            var a = _cache.SetJson(GetDisctrictIdKey(district.DistrictId), district);
            var b = _cache.SetJson(GetDisctrictDomainKey(district.Domain), district);
            await Task.WhenAll(new[]{a, b});
        }
        private async Task InvalidateCache(District district) {
            if (district == null) return;
            var a = _cache.RemoveAsync(GetDisctrictIdKey(district.DistrictId));
            var b = _cache.RemoveAsync(GetDisctrictDomainKey(district.Domain));
            await Task.WhenAll(new[]{a, b});
        }

        public new async Task<District> Update(District district)
        {
            _ = InvalidateCache(district);
            var d = await base.Update(district);
            _ = Cache(d);
            return d;
        }

        public new async Task<District> GetByDomain(string districtDomain)
        {
            var d = await _cache.GetJson<District>(GetDisctrictDomainKey(districtDomain));
            if (d != null) return d;
            d = await base.GetByDomain(districtDomain);
            _ = Cache(d);
            return d;
        }

        public new async Task<District> GetById(long districtId)
        {
            var d = await _cache.GetJson<District>(GetDisctrictIdKey(districtId));
            if (d != null) return d;
            d = await base.GetById(districtId);
            _ = Cache(d);
            return d;
        }
    }
}
