﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class CommentService : ICommentService
    {
        private readonly MainDbContext _mainDbContext;
        private readonly ILogger<CommentService> _logger;

        public CommentService(
            MainDbContext mainDbContext,
            ILogger<CommentService> logger)
        {
            _mainDbContext = mainDbContext;
            _logger = logger;
        }

        public async Task<Comment> Create(Comment item)
        {
            return await _mainDbContext.AddAsync(item, true);
        }

        public async Task Delete(Comment comment)
        {
            await _mainDbContext.DeleteAsync(comment, true);
        }

        public async Task<Comment> Delete(long commentId)
        {
            var comment = await GetCommentById(commentId);
            if (comment == null) return comment;

            await Delete(comment);
            return comment;
        }

        public async Task<Comment> GetCommentById(long commentId)
        {
            return await _mainDbContext.Comments
                .FirstOrDefaultAsync(e => e.CommentId == commentId);
        }

        public async Task<IList<Comment>> GetCommentsOfThread(string threadId)
        {
            return await _mainDbContext.Comments
                .Where(e => e.ThreadId == threadId)
                .OrderBy(e => e.DateCreated)
                .ToListAsync();
        }
        public Task<Comment> GetLastCommentOfThread(string threadId)
        {
            return _mainDbContext.Comments
                .Where(e => e.ThreadId == threadId)
                .OrderByDescending(e => e.DateCreated)
                .Take(1)
                .FirstOrDefaultAsync();
        }
        public Task<List<Comment>> GetLastCommentOfThreads(string[] threadIds)
        {
            return _mainDbContext.Comments
                .Where(e => threadIds.Contains(e.ThreadId))
                .GroupBy(e => e.ThreadId)
                .Select(g => g.OrderByDescending(e => e.DateCreated).Take(1))
                .SelectMany(e => e)
                .ToListAsync();
        }

        public async Task<IList<Comment>> GetReplies(long commentId)
        {
            return await _mainDbContext.Comments
                .Where(e => e.ReplyTo == commentId)
                .OrderBy(e => e.DateCreated)
                .ToListAsync();
        }

        public async Task<int> CountCommentsOfThread(string threadId)
        {
            return await _mainDbContext.Comments
                .CountAsync(e => e.ThreadId == threadId);
        }

        public async Task<int> CountReplies(long commentId)
        {
            return await _mainDbContext.Comments
                .CountAsync(e => e.ReplyTo == commentId);
        }

        public async Task<int> CountNewComments(string threadId, DateTime from)
        {
            return await _mainDbContext.Comments
                .CountAsync(e => e.ThreadId == threadId &&
                                 e.DateCreated < from);
        }

        public async Task<int> CountNewComments(string threadId, long userId)
        {
            var cs = await GetCommentSeen(threadId, userId);
            if (cs == null) return await _mainDbContext.Comments.CountAsync(e => e.ThreadId == threadId && e.CreatedBy != userId);
            return await _mainDbContext.Comments.CountAsync(e => e.ThreadId == threadId && e.CreatedBy != userId && e.DateCreated < cs.LastSeen);
        }
        public Task<List<ThreadUnread>> CountNewComments(long userId, string[] threadIds)
        {
            return (
                from c in _mainDbContext.Comments.Where(c => threadIds.Contains(c.ThreadId) && c.CreatedBy != userId)
                join cs in _mainDbContext.CommentSeens.Where(cs => cs.UserId == userId)
                    on c.ThreadId equals cs.ThreadId into css
                from cs in css.DefaultIfEmpty()
                where (cs == null || (c.DateCreated.HasValue && cs.LastSeen < c.DateCreated.Value))
                group c by c.ThreadId into g
                select new ThreadUnread { ThreadId = g.Key, Unread = g.Count() }
            ).ToListAsync();
        }

        public async Task<int> CountNewReplies(long commentId, DateTime from)
        {
            return await _mainDbContext.Comments
                .CountAsync(e => e.ReplyTo == commentId &&
                                 from < e.DateCreated);
        }

        public async Task<IEnumerable<long>> GetUserIdsOfThread(string threadId)
        {
            return await _mainDbContext.Comments
                .Where(e => e.ThreadId == threadId && e.CreatedBy != null)
                .Select(e => e.CreatedBy.Value)
                .Distinct()
                .ToListAsync();
        }

        public async Task<CommentSeen> GetCommentSeen(string threadId, long userId)
        {
            return await _mainDbContext.CommentSeens
                .FirstOrDefaultAsync(e => e.ThreadId == threadId && e.UserId == userId);
        }

        public async Task<CommentSeen> GetCommentSeens(long userId, string[] threadIds)
        {
            return await _mainDbContext.CommentSeens
                .FirstOrDefaultAsync(e => threadIds.Contains(e.ThreadId) && e.UserId == userId);
        }

        public async Task<CommentSeen> SetCommentSeen(string threadId, long userId, DateTime lastSeen)
        {
            var cs = await GetCommentSeen(threadId, userId);
            if (cs == null) {
                return await _mainDbContext.AddAsync(new CommentSeen { ThreadId = threadId, UserId = userId, LastSeen = lastSeen }, true);
            }
            if (cs.LastSeen >= lastSeen) return cs;
            cs.LastSeen = lastSeen;
            await _mainDbContext.UpdateAsync(cs, true);
            return cs;
        }
    }
}
