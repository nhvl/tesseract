﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Core.Exceptions;
using Tesseract.Common.Extensions;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Data.NoSql;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Infrastructure.Utils;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class ActivityService : IActivityService
    {
        private readonly MainDbContext _mainDbContext;
        private readonly IDocumentDBRepository _documentDbRepository;
        private readonly IActDocService _actDocService;
        private readonly ISubmissionService _submissionService;
        private readonly IUserService _userService;
        private readonly ILogger<ActivityService> _logger;

        public ActivityService(
            MainDbContext mainDbContext,
            ISubmissionService submissionService,
            IDocumentDBRepository documentDbRepository,
            IActDocService actDocService,
            IUserService userService,
            ILogger<ActivityService> logger
            )
        {
            _mainDbContext = mainDbContext;
            _submissionService = submissionService;
            _documentDbRepository = documentDbRepository;
            _actDocService = actDocService;
            _userService = userService;
            _logger = logger;
        }

        public async Task<Activity> CreateActivity(Activity activity)
        {
            var newActivity = await _mainDbContext.AddAsync(activity, true);
            if (newActivity.IsGraded)
            {
                await ReCalculateCumulativeGrade(newActivity.ClassId);
            }
            return newActivity;
        }

        public Task<int> DeleteActivity(long activityId)
        {
            return _mainDbContext.DeleteAsync<Activity>(x => x.ActivityId == activityId);
        }

        public async Task<IEnumerable<long>> DuplicateActivity(long activityId, IEnumerable<long> toClassIds)
        {
            var activity = await _mainDbContext.Activities.AsNoTracking().Where(x => x.ActivityId == activityId).FirstOrDefaultAsync() ?? throw new ResourceNotFoundException($"{nameof(activityId)}: {activityId}");
            var newActIds = new List<long>();

            foreach (var classId in toClassIds)
            {
                var _ = await _mainDbContext.Classes.FindAsync(classId) ?? throw new ResourceNotFoundException($"{nameof(classId)}: {classId}");
                var clonedActivity = GetActivityClone(activity, classId);
                var act = await _mainDbContext.Activities.AddAsync(clonedActivity);
                await _mainDbContext.SaveChangesAsync();
                newActIds.Add(act.Entity.ActivityId);
            }

            var actDoc = await _actDocService.Get(activityId);

            if (actDoc != null)
            {
                await Task.WhenAll(newActIds.Select(async actId => await _actDocService.Create(actDoc.Clone(actId))));
            }

            return newActIds;
        }

        public async Task EditActivitySimple(Activity activity, bool commit = true)
        {
            await _mainDbContext.UpdateAsync(activity, commit);
        }
        public async Task<Activity> EditActivity(Activity activity)
        {
            var currentActivity = _mainDbContext.Activities.FirstOrDefault(x => x.ActivityId == activity.ActivityId);
            if (currentActivity == null) return null;

            //Check if new updates need recalculate student grades or not.
            var isRecalculateGrade = (
                (currentActivity.DateAssigned != activity.DateAssigned) ||
                (currentActivity.IsGraded != activity.IsGraded) ||
                (currentActivity.IsGraded && (
                    currentActivity.MaxScore != activity.MaxScore ||
                    currentActivity.Weight != activity.Weight
                )) ||
                (currentActivity.IsCredit != activity.IsCredit)
            );

            // TODO: do we allow chang type???
            currentActivity.Type             = activity.Type;
            currentActivity.Title            = activity.Title;
            currentActivity.Description      = activity.Description;

            currentActivity.DateAssigned     = activity.DateAssigned;
            currentActivity.DateDue          = activity.DateDue;
            currentActivity.DateCutoff       = activity.DateCutoff;

            currentActivity.ParentActivityId = activity.ParentActivityId;
            currentActivity.SortIndex        = activity.SortIndex;
            currentActivity.PublishStartDate = activity.PublishStartDate;
            currentActivity.PublishEndDate   = activity.PublishEndDate;

            currentActivity.IsGraded         = activity.IsGraded;
            currentActivity.MaxScore         = activity.MaxScore;
            currentActivity.Weight           = activity.Weight;
            currentActivity.IsCredit         = activity.IsCredit;

            await _mainDbContext.UpdateAsync(currentActivity, true);

            if (isRecalculateGrade)
            {
                await ReCalculateCumulativeGrade(currentActivity.ClassId);
            }

            return currentActivity;
        }

        public async Task<IEnumerable<Activity>> GetActivitiesOfClass(long classId)
        {
            return await _mainDbContext.Activities.Where(x => x.ClassId == classId).ToListAsync();
        }

        public Task<Activity> GetActivity(long activityId, bool includeClass = true)
        {
            var activities = includeClass
                ? _mainDbContext.Activities.Include(x => x.Class)
                : (IQueryable<Activity>) _mainDbContext.Activities;
            return activities.FirstOrDefaultAsync(x => x.ActivityId == activityId);
        }

        public IQueryable<Activity> GetActivitiesById(long[] activityIds)
        {
            return _mainDbContext.Activities.Where(x => activityIds.Contains(x.ActivityId));
        }

        public Task<ActivityScore> GetActivityScore(long activityId, long studentId)
        {
            return _mainDbContext.ActivityScores.FirstOrDefaultAsync(s => s.ActivityId == activityId && s.StudentId == studentId);
        }
        public async Task<ActivityScore> GetActivityScoreOrCreate(long activityId, long studentId, bool commit = true)
        {
            var s = await GetActivityScore(activityId, studentId);
            if (s != null) {
                if (!commit) await _mainDbContext.UpdateLinkEntityAsync(s, false);
                return s;
            }
            s = new ActivityScore
            {
                ActivityId = activityId,
                StudentId = studentId,
                ThreadId = GenerateThreadId(),
            };
            return await _mainDbContext.AddLinkEntityAsync(s, commit);
        }
        public async Task<ActivityScore> GetActivityScoreEnsureThreadId(long activityId, long studentId, bool commit = true)
        {
            var s = await GetActivityScoreOrCreate(activityId, studentId, false);
            if (string.IsNullOrEmpty(s.ThreadId)) s.ThreadId = GenerateThreadId();
            await _mainDbContext.SaveChangesAsync();
            return s;
        }
        public async Task EnsureActivityScoreHasThreadId(ActivityScore s, bool commit = true)
        {
            if (string.IsNullOrEmpty(s.ThreadId))
            {
                s.ThreadId = GenerateThreadId();
                await _mainDbContext.UpdateLinkEntityAsync(s, commit);
            }
        }
        public async Task EnsureStudentsHasActivityScoreWithThreadId(IEnumerable<GeneralRecord> records, long activityId)
        {
            foreach(var record in records)
            {
                if (record.ActivityScore == null) {
                    record.ActivityScore = await GetActivityScoreOrCreate(activityId, record.Student.StudentId, false);
                } else {
                    await EnsureActivityScoreHasThreadId(record.ActivityScore);
                }
            }
            await _mainDbContext.SaveChangesAsync();
        }
        public async Task EnsureActivitiesHasActivityScoreWithThreadId(IEnumerable<GeneralRecord> records, long studentId)
        {
            foreach(var record in records)
            {
                if (record.ActivityScore == null) {
                    record.ActivityScore = await GetActivityScoreOrCreate(record.Activity.ActivityId, studentId, false);
                } else {
                    await EnsureActivityScoreHasThreadId(record.ActivityScore);
                }
            }
            await _mainDbContext.SaveChangesAsync();
        }

        public async Task<Submission> MakeSubmission(long activityId, long studentId, string content, SubmissionAttachment[] attachments, long? userId, bool isSubmitted)
        {
            var activity = await _mainDbContext.Activities.FirstOrDefaultAsync(t => t.ActivityId == activityId);
            if (activity == null) throw new Exception($"Activity with ActivityID: {activityId} not found.");
            if (activity.DateCutoff != null && activity.DateCutoff < DateTime.UtcNow) throw new Exception($"Cannot submit to ActivityID: {activityId} any more.");

            var activityScore = await GetActivityScoreOrCreate(activityId, studentId, false);
            activityScore.SavedDate = DateTime.UtcNow;
            activityScore.IsSubmitted = activityScore.IsSubmitted || isSubmitted;
            await _mainDbContext.SaveChangesAsync();

            return await _submissionService.CreateOrUpdate(new Submission
            {
                ActivityId = activityId,
                StudentId = studentId,
                Content = content,
                Attachments = attachments,
            }, userId);
        }

        public async Task<decimal?> SetGrade(
            long activityId, long studentId,
            decimal? grade, byte? communication = null, byte? collaboration = null,
            byte? character = null, byte? creativity = null, byte? criticalThinking = null,
            bool? isExclude  = null)
        {
            var activity = await _mainDbContext.Activities.FirstOrDefaultAsync(t => t.ActivityId == activityId);
            if (activity == null) throw new ResourceNotFoundException($"Activity with ActivityID: {activityId} not found.");

            if (!activity.DateAssigned.HasValue || activity.DateAssigned > DateTime.UtcNow)
                throw new InvalidRequestException($"Activity  with ActivityID: {activityId} can't be graded because DateAssigned is {activity.DateAssigned}");

            if (grade < 0 || grade > activity.MaxScore * 2) throw new InvalidRequestException($"Activity  with ActivityID: {activityId} can't be graded because Grade {grade} is invalid.");

            var activityScore = await GetActivityScoreOrCreate(activityId, studentId, false);

            var isRecalculateGrade = (activityScore.Score != grade || activityScore.IsExclude != isExclude);
            if (isRecalculateGrade)
            {
                activityScore.Score = grade;
                activityScore.GradeDate = DateTime.UtcNow;
            }

            if (communication != null && activityScore.Communication != communication)
            {
                activityScore.Communication = communication;
                activityScore.GradeDate = DateTime.UtcNow;
            }
            if (collaboration != null && activityScore.Collaboration != collaboration)
            {
                activityScore.Collaboration = collaboration;
                activityScore.GradeDate = DateTime.UtcNow;
            }
            if (character != null && activityScore.Character != character)
            {
                activityScore.Character = character;
                activityScore.GradeDate = DateTime.UtcNow;
            }
            if (creativity != null && activityScore.Creativity != creativity)
            {
                activityScore.Creativity = creativity;
                activityScore.GradeDate = DateTime.UtcNow;
            }
            if (criticalThinking != null && activityScore.CriticalThinking != criticalThinking)
            {
                activityScore.CriticalThinking = criticalThinking;
                activityScore.GradeDate = DateTime.UtcNow;
            }
            if (isExclude != null && activityScore.IsExclude != isExclude) {
                activityScore.IsExclude = isExclude.Value;
                activityScore.GradeDate = DateTime.UtcNow;
            }

            await _mainDbContext.SaveChangesAsync();

            if (isRecalculateGrade)
            {
                await ReCalculateCumulativeGrade(activity.ClassId, studentId);
            }

            var classStudent = await _mainDbContext.ClassStudents.FirstOrDefaultAsync(x => x.StudentId == studentId && x.ClassId == activity.ClassId);

            return classStudent.CumulativeGrade;
        }

        public async Task ReCalculateCumulativeGrade(long classId, long? studentId = null)
        {
            var studentList = (studentId.HasValue) ? new List<long>() { studentId.Value } :
                (await _mainDbContext.ClassStudents.Where(cs => cs.ClassId == classId).Select(cs => cs.StudentId).ToListAsync());

            foreach (var sId in studentList)
            {
                //Set grade in ClassStudent table
                var activityScoreItems = await
                    _mainDbContext.ActivityScores.Where(x => x.Score != null && x.StudentId == sId)
                    .Join(_mainDbContext.Activities.Where(x => x.ClassId == classId && x.IsGraded && x.Type != ActivityType.Activity && x.DateAssigned.HasValue && x.DateAssigned <= DateTime.UtcNow),
                            actScore => actScore.ActivityId,
                            act => act.ActivityId,
                            (actScore, act) => new { actScore, act }).ToListAsync();
                decimal? cumulativeGrade = null;
                if (activityScoreItems.Count != 0)
                {
                    var scoreData = activityScoreItems.Select(x => (
                        Score    : (decimal)x.actScore.Score,
                        MaxScore : x.act.MaxScore,
                        Weight   : x.act.Weight,
                        IsExclude: x.actScore.IsExclude,
                        IsCredit : x.act.IsCredit
                    ));
                    cumulativeGrade = ScoreHepler.CalculateCumulativeGrade(scoreData);
                }

                var classStudent = await _mainDbContext.ClassStudents.FirstOrDefaultAsync(x => x.StudentId == sId && x.ClassId == classId);
                var isCreateNew = false;
                if (classStudent == null)
                {
                    isCreateNew = true;
                    classStudent = new ClassStudent()
                    {
                        StudentId = sId,
                        ClassId = classId
                    };
                }
                classStudent.CumulativeGrade = cumulativeGrade;
                if (isCreateNew)
                {
                    await _mainDbContext.AddLinkEntityAsync(classStudent, true);
                }
                else
                {
                    await _mainDbContext.UpdateLinkEntityAsync(classStudent, true);
                }

                //Set grade in SchoolStudent table
                var currentGradingTerm = (await _mainDbContext.Classes.FindAsync(classStudent.ClassId)).GradingTerm;
                var allClassOfStudent = await _mainDbContext.ClassStudents.Where(x => x.StudentId == sId && x.CumulativeGrade != null &&
                                _mainDbContext.Classes.Any(y => y.GradingTerm == currentGradingTerm && y.ClassId == x.ClassId)).ToListAsync();
                decimal? averageGrade = null;
                if (allClassOfStudent.Count != 0)
                {
                    averageGrade = allClassOfStudent.Average(x => x.CumulativeGrade);
                }

                var schoolStudent = await _mainDbContext.SchoolStudents.FirstOrDefaultAsync(x => x.StudentId == sId && x.GradingPeriod == currentGradingTerm);
                isCreateNew = false;
                if (schoolStudent == null)
                {
                    isCreateNew = true;
                    schoolStudent = new SchoolStudent()
                    {
                        StudentId = sId,
                        GradingPeriod = currentGradingTerm,
                        SchoolId = (await _mainDbContext.Classes.FindAsync(classStudent.ClassId)).SchoolId
                    };
                }
                schoolStudent.Grade = averageGrade;
                if (isCreateNew)
                {
                    await _mainDbContext.AddLinkEntityAsync(schoolStudent, true);
                }
                else
                {
                    await _mainDbContext.UpdateLinkEntityAsync(schoolStudent, true);
                }
            };
        }

        public IQueryable<GeneralRecord> GetActivitiesOfFaculty(long schoolId, long facultyId)
        {
            return (
                from cf in _mainDbContext.ClassFaculties
                join c in _mainDbContext.Classes on cf.ClassId equals c.ClassId
                join a in _mainDbContext.Activities on c.ClassId equals a.ActivityId
                join f in _mainDbContext.Faculties on cf.FacultyId equals f.FacultyId
                where cf.FacultyId == facultyId &&
                      c.SchoolId == schoolId
                select new GeneralRecord { ClassFaculty = cf, Class = c, Activity = a, Faculty = f }
            );
        }

        public IQueryable<GeneralRecord> GetActivitiesOfFaculty(long schoolId, long gradingTermId, long facultyId)
        {
            return (
                from r in GetActivitiesOfFaculty(schoolId, facultyId)
                let c = r.Class
                where c.GradingTerm == gradingTermId
                select r
            );
        }

        public async Task<IEnumerable<Activity>> GetActivitiesOfFaculty(long facultyId, long gradingTermId, Expression<Func<Activity, bool>> p)
        {
            var activities = await (
                    from cf in _mainDbContext.ClassFaculties
                    join c in _mainDbContext.Classes on cf.ClassId equals c.ClassId
                    join a in _mainDbContext.Activities on c.ClassId equals a.ClassId
                    where cf.FacultyId == facultyId &&
                        c.GradingTerm == gradingTermId
                    select a
                ).Where(p)
                //.Include(x => x.Class)
                //.Include(x => x.Scores)
                .ToListAsync();
            foreach(var activity in activities)
            {
                activity.Class = await _mainDbContext.Classes.FirstOrDefaultAsync(c => c.ClassId == activity.ClassId);
                activity.Scores = await _mainDbContext.ActivityScores.Where(a => a.ActivityId == activity.ActivityId).ToListAsync();
            }
            return activities;
        }

        public IQueryable<Activity> GetActivitiesOfFacultyInClass(long classId, long facultyId)
        {
            return (from c in _mainDbContext.Classes
                    join cf in _mainDbContext.ClassFaculties on c.ClassId equals cf.ClassId
                    join f in _mainDbContext.Faculties on cf.FacultyId equals f.FacultyId
                    join a in _mainDbContext.Activities on c.ClassId equals a.ClassId
                    where c.ClassId == classId &&
                          f.FacultyId == facultyId
                    select a);
        }

        private async Task<IQueryable<GeneralRecord>> GetActivityAsStudent(long studentId, long? schoolId, long? classId, long? activityId)
        {
            var school = schoolId.HasValue ? (await _mainDbContext.Schools.FirstOrDefaultAsync(x => x.SchoolId == schoolId)) : null;
            if (schoolId.HasValue && school == null) throw new ArgumentException();
            var currentGradingTerm = school?.CurrentGradingTerm;

            return (
                from cs in _mainDbContext.ClassStudents
                join c in _mainDbContext.Classes on cs.ClassId equals c.ClassId
                join a in _mainDbContext.Activities.Where(Activity.IsPublicExpr) on c.ClassId equals a.ClassId
                    into aG from a in aG.DefaultIfEmpty()
                join score in _mainDbContext.ActivityScores on a.ActivityId equals score.ActivityId
                    into scoreG from score in scoreG.Where(s => s.StudentId == studentId).DefaultIfEmpty()
                where cs.StudentId == studentId &&
                      (!schoolId.HasValue || (c.SchoolId == schoolId.Value && c.GradingTerm == currentGradingTerm)) &&
                      (!classId.HasValue || c.ClassId == classId.Value) &&
                      (!activityId.HasValue || a.ActivityId == activityId.Value)
                select new GeneralRecord
                {
                    ClassStudent = cs,
                    Class = c,
                    Activity = a,
                    ActivityScore = score,
                }
            );
        }

        public IQueryable<GeneralRecord> GetActivitiesOfStudentAsParent(long studentId, long[] schoolIds)
        {
            return (
                from cs in _mainDbContext.ClassStudents
                join c in _mainDbContext.Classes on cs.ClassId equals c.ClassId
                join t in _mainDbContext.GradingTerms on c.GradingTerm  equals t.GradingTermId
                join cf in _mainDbContext.ClassFaculties on cs.ClassId  equals cf.ClassId
                join f in _mainDbContext.Faculties on cf.FacultyId equals f.FacultyId
                join u in _mainDbContext.Users on f.FacultyId equals u.Id
                join a in _mainDbContext.Activities.Where(Activity.IsPublicExpr)
                    on c.ClassId equals a.ClassId
                    // into aG from a in aG.DefaultIfEmpty()
                join score in _mainDbContext.ActivityScores.Where(s => s.StudentId == studentId)
                    on a.ActivityId equals score.ActivityId
                    into scoreG from score in scoreG.DefaultIfEmpty()
                where cs.StudentId == studentId
                    && schoolIds.Contains(c.SchoolId)
                select new GeneralRecord
                {
                    ClassStudent  = cs,
                    Class         = c,
                    Activity      = a,
                    ActivityScore = score,
                    GradingTerm   = t,
                    Faculty       = f,
                    ClassFaculty  = cf,
                    User          = u,
                }
            );
        }

        public async Task<IList<GeneralRecord>> GetActivitiesOfStudent(long studentId, long schoolId, long? classId, ActivityType? type)
        {
            var data =
                (await
                    (await GetActivityAsStudent(studentId, schoolId, classId, null))
                    .Where(r => r.Activity != null && (!type.HasValue || r.Activity.Type == type.Value))
                .ToListAsync());
            return data;
        }

        public async Task<IList<GeneralRecord>> GetActivitiesOfStudentInClass(long studentId, long classId)
        {
            var data = await (await GetActivityAsStudent(studentId, null, classId, null)).ToListAsync();
            return data;
        }

        public async Task<GeneralRecord> GetActivityWithScore(long studentId, long activityId)
        {
            var row = await (await GetActivityAsStudent(studentId, null, null, activityId)).FirstOrDefaultAsync();
            return row;
        }

        private Activity GetActivityClone(Activity activity, long newClassId)
        {
            _mainDbContext.Entry(activity).State = EntityState.Detached;
            activity.ClassId = newClassId;
            activity.ActivityId = 0;
            activity.DateDue = null;
            activity.DateCutoff = null;
            activity.DateAssigned = null;
            return activity;
        }

        #region Polling
        public async Task<int> GetPollingCount(long activityId, string pollQuizId, byte voteContent)
        {
            var activity = await _mainDbContext.Activities.FirstOrDefaultAsync(t => t.ActivityId == activityId);
            if (activity == null)
            {
                throw new Exception($"Activity with ActivityID: {activityId} not found.");
            }

            var pollItem = await _mainDbContext.Polls.Where(x => x.ActivityId == activityId && x.PollQuizId == pollQuizId && x.VoteContent == voteContent).CountAsync();
            return pollItem;
        }

        public async Task<Poll> SetPolling(long activityId, long studentId, string pollQuizId, byte voteContent)
        {
            var pollItem = await _mainDbContext.Polls.Where(x => x.ActivityId == activityId && x.PollQuizId == pollQuizId && x.StudentId == studentId).FirstOrDefaultAsync();
            if (pollItem == null)
            {
                var newPoll = new Poll
                {
                    ActivityId = activityId,
                    StudentId = studentId,
                    PollQuizId = pollQuizId,
                    VoteContent = voteContent
                };
                return await _mainDbContext.AddAsync(newPoll, true);
            }

            pollItem.VoteContent = voteContent;
            await _mainDbContext.UpdateAsync(pollItem);
            await _mainDbContext.SaveChangesAsync();
            return pollItem;
        }

        public async Task<byte?> GetStudentVote(long activityId, long studentId, string pollQuizId)
        {
            var pollItem = await _mainDbContext.Polls.Where(x => x.ActivityId == activityId && x.StudentId == studentId && x.PollQuizId == pollQuizId).FirstOrDefaultAsync();
            return pollItem?.VoteContent;
        }
        #endregion


        internal static string GenerateThreadId()
        {
            return Guid.NewGuid().ToString("N");
        }

        public async Task<IEnumerable<Activity>> GetActivitiesByCategory(long activityCategoryId)
        {
            return await _mainDbContext.Activities.Where(x => x.CategoryId == activityCategoryId).ToListAsync();
        }
    }
}
