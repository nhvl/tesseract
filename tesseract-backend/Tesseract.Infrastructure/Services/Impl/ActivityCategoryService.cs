﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class ActivityCategoryService : IActivityCategoryService
    {
        private readonly MainDbContext _mainDbContext;

        public ActivityCategoryService(MainDbContext mainDbContext)
        {
            _mainDbContext = mainDbContext;
        }

        public async Task DeleteCategory(ActivityCategory category)
        {
            await _mainDbContext.Activities.Where(a => a.CategoryId == category.ActivityCategoryId).ForEachAsync(x => x.CategoryId = null);
            await _mainDbContext.DeleteAsync(category, false);
            await _mainDbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<ActivityCategory>> GetCategories(long facultyId)
        {
            return await _mainDbContext.ActivityCategories.Where(x => x.FacultyId == facultyId).OrderByDescending(x => x.ActivityCategoryId).ToListAsync();
        }

        public async Task<ActivityCategory> GetCategory(long categoryId)
        {
            return await _mainDbContext.ActivityCategories.Where(x => x.ActivityCategoryId == categoryId).FirstOrDefaultAsync();
        }

        public async Task<ActivityCategory> SetCategory(ActivityCategory category)
        {
            if (string.IsNullOrWhiteSpace(category.Name)) throw new System.Exception("Category Name is required."); // TODO: localization
            if (string.IsNullOrWhiteSpace(category.Color)) throw new System.Exception("Color is required.");
            var faculty = await _mainDbContext.Faculties.Where(x => x.FacultyId == category.FacultyId).FirstOrDefaultAsync();
            if (faculty == null) throw new System.Exception("FacultyId is invalid.");
            if (category.ActivityCategoryId < 1)
            {
                var newCategory = await _mainDbContext.AddAsync(category, true);
                return newCategory;
            }

            var categoryItem = await _mainDbContext.ActivityCategories.Where(x => x.ActivityCategoryId == category.ActivityCategoryId).FirstOrDefaultAsync();
            if (categoryItem == null) return null;

            categoryItem.Name = category.Name;
            categoryItem.Color = category.Color;
            categoryItem.FacultyId = category.FacultyId;
            categoryItem.IsGraded = category.IsGraded;
            categoryItem.MaxScore = category.MaxScore;
            categoryItem.Weight = category.Weight;
            await _mainDbContext.UpdateAsync(categoryItem);

            await _mainDbContext.Activities.Where(a => a.CategoryId == categoryItem.ActivityCategoryId).ForEachAsync(x =>
            {
                x.Color = categoryItem.Color;
                x.IsGraded = categoryItem.IsGraded;
                x.MaxScore = categoryItem.MaxScore;
                x.Weight = categoryItem.Weight;
            });
            await _mainDbContext.SaveChangesAsync();

            return categoryItem;
        }
    }
}
