﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Cryptography.Md5HashExtension;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class GradingTermService : IGradingTermService
    {
        private readonly MainDbContext _mainDbContext;
        private readonly IDistributedCache _cache;
        public GradingTermService(
            MainDbContext mainDbContext
            , IDistributedCache cache)
        {
            _mainDbContext = mainDbContext;
            _cache = cache;
        }

        public async Task<IEnumerable<GradingTerm>> GetGradingTerms(long schoolId)
        {
            return await _cache.GetAsync(
                GetCacheKey(schoolId),
                () => GetGradingTermsNoCache(schoolId));
        }

        public async Task<GradingTerm[]> GetGradingTerms(long[] schoolIds)
        {
            var xs = await Task.WhenAll(schoolIds.Select(async SchoolId => new {
                SchoolId,
                GradingTerms = await _cache.GetJson<GradingTerm[]>(GetCacheKey(SchoolId))
            }));
            var ys = xs.Where(x => x.GradingTerms == null).Select(x => x.SchoolId).ToArray();
            var ts = await GetGradingTermsNoCache(ys);
            foreach (var x in xs)
            {
                if (x.GradingTerms == null) {
                    var xts = ts.Where(t => t.SchoolId == x.SchoolId).ToArray();
                    await _cache.SetJson(GetCacheKey(x.SchoolId), xts);
                }
            }
            return xs.SelectMany(x => x.GradingTerms == null ? new GradingTerm[0] : x.GradingTerms)
                .Concat(ts)
                .ToArray();
        }

        public Task<List<GradingTerm>> GetGradingTermsNoCache(long schoolId)
        {
            return _mainDbContext.GradingTerms.Where(x => x.SchoolId == schoolId).ToListAsync();
        }

        public Task<List<GradingTerm>> GetGradingTermsNoCache(long[] schoolIds)
        {
            return _mainDbContext.GradingTerms.Where(x => schoolIds.Contains(x.SchoolId)).ToListAsync();
        }

        public async Task<GradingTerm> GetGradingTerm(long schoolId, long termId)
        {
            return await _mainDbContext.GradingTerms.Where(x => x.SchoolId == schoolId && x.GradingTermId == termId).FirstOrDefaultAsync();
        }

        private static string GetCacheKey(long schoolId)
        {
            return $"{schoolId}-gradingTerm-entries".ToMD5Hash();
        }

        public async Task<GradingTerm> SetGradingTerm(GradingTerm term)
        {
            if (string.IsNullOrWhiteSpace(term.Name)) throw new System.Exception("Grading Term Name is required."); // TODO: localization
            if (term.StartDate == null) throw new System.Exception("Start Date is required.");
            if (term.EndDate == null) throw new System.Exception("End Date is required.");
            if (term.StartDate >= term.EndDate) throw new System.Exception("End Date should be greater than Start Date.");

            if (term.GradingTermId < 1)
            {
                var newTerm = await _mainDbContext.AddAsync(term, true);
                await _cache.RemoveAsync(GetCacheKey(term.SchoolId));
                return newTerm;
            }

            var termItem = await _mainDbContext.GradingTerms.Where(x => x.GradingTermId == term.GradingTermId).FirstOrDefaultAsync();
            if (termItem == null) return null;

            termItem.Name = term.Name;
            termItem.SchoolId = term.SchoolId;
            termItem.StartDate = term.StartDate;
            termItem.EndDate = term.EndDate;

            await _mainDbContext.UpdateAsync(termItem);
            await _cache.RemoveAsync(GetCacheKey(term.SchoolId));
            return termItem;
        }

        public async Task DeleteGradingTerm(GradingTerm term)
        {
            await _mainDbContext.DeleteAsync(term);
            await _cache.RemoveAsync(GetCacheKey(term.SchoolId));
        }
    }
}
