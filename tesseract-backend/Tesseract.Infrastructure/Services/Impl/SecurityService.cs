﻿using System;
using System.Security.Cryptography;
using System.Web;
using Tesseract.Infrastructure.Services.Interface;
using System.Collections.Generic;
using System.Linq;
using Tesseract.Common.Extensions;
namespace Tesseract.Infrastructure.Services.Impl
{
    public class SecurityService : ISecurityService
    {
        private static readonly char[] Punctuations = "!@#$%^&*()_-+=[{]};:>|./?".ToCharArray();
        private static readonly char[] Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".ToCharArray();
        private static readonly char[] Digits = "0123456789".ToCharArray();
        private Random rnd = new Random();

        public string RandomPassword()
        {
            var passLength = rnd.Next(12, 16);
            var numberLength = rnd.Next(3, 5);
            var punctuationLength = rnd.Next(3, 5);
            var letterLength = passLength - numberLength - punctuationLength;
            return Generate(letterLength, numberLength, punctuationLength);
        }

        private string Generate(int letterLength, int numberLength, int punctuationLength)
        {

            var digits = Enumerable.Range(1, numberLength).Select(i => Punctuations[rnd.Next(0, Punctuations.Length)]);
            var letters = Enumerable.Range(1, letterLength).Select(i => Chars[rnd.Next(0, Chars.Length)]);
            var punctuations = Enumerable.Range(1, punctuationLength).Select(i => Digits[rnd.Next(0, Digits.Length)]);
            return new string(digits.Concat(letters).Concat(punctuations).ToList().Shuffle().ToArray());
        }
    }
}