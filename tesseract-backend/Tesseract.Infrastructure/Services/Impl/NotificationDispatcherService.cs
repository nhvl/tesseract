﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class NotificationDispatcherService : INotificationDispatcherService, IDisposable
    {
        private readonly IStudentService _studentService;
        private readonly IModel _channel;

        private static readonly string TaskQueue = "NotifyEvent";

        public NotificationDispatcherService(IStudentService studentService, IConnection connection)
        {
            _studentService = studentService;
            _channel = connection.CreateModel();
            _channel.QueueDeclare(queue: TaskQueue,
                                durable: false,
                                exclusive: false,
                                autoDelete: false);

        }

        public async Task FacultyUpdateActivity(ApplicationUser user, Activity activity) {
            var students = await _studentService.GetStudentsInClass(activity.ClassId, includeUserInfo: true)
                .Where(r => r.User.Id != user.Id)
                .ToListAsync();

            var data = new {
                activity,
                updatedAt = DateTime.Now,
                updateBy = user,
                users = students.Select(x => new { x.User.Id, x.User.Email } ).ToArray()
            };

            _channel.BasicPublish(exchange: "",
                                    routingKey: TaskQueue,
                                    basicProperties: null,
                                    body: Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data)));
        }

        public void Dispose()
        {
            _channel?.Close();
        }
    }
}
