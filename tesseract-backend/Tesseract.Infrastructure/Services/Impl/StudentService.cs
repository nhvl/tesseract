﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Exceptions;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class StudentService : IStudentService
    {
        private readonly MainDbContext _mainDbContext;
        private readonly IUserService _userService;

        public StudentService(
            MainDbContext mainDbContext,
            IUserService userService
        )
        {
            _mainDbContext = mainDbContext;
            _userService = userService;
        }

        public async Task<Student> CreateStudentAsync(Student student, long schoolId)
        {
            if (await GetStudentByStudentNumber(student.StudentNumber) != null) throw new ConflictResourceException("Duplicated student number.");

            var storedStudent = await _mainDbContext.AddAsync(student, true);
            var school = await _mainDbContext.Schools.FirstOrDefaultAsync(t => t.SchoolId == schoolId) ?? throw new ResourceNotFoundException();
            var schoolStudent = new SchoolStudent
            {
                SchoolId = school.SchoolId,
                StudentId = student.StudentId,
                GradingPeriod = school.CurrentGradingTerm
            };

            await _mainDbContext.AddLinkEntityAsync(schoolStudent, true);
            return storedStudent;
        }

        public Task DeleteStudentAsync(Student student)
        {
            throw new NotImplementedException();
        }

        public async Task<Student> GetStudentById(long studentId)
        {
            var r = await (
                from s in _mainDbContext.Students
                join u in _mainDbContext.Users on s.StudentId equals u.Id
                where s.StudentId == studentId
                select new GeneralRecord { Student = s, User = u }
            ).FirstOrDefaultAsync();

            return ToStudent(r);
        }

        public async Task<Student> GetStudentByStudentNumber(string studentNumber)
        {
            if (string.IsNullOrEmpty(studentNumber)) return null;

            studentNumber = studentNumber.ToLower();

            var r = await (
                from s in _mainDbContext.Students
                join u in _mainDbContext.Users on s.StudentId equals u.Id
                where s.StudentNumber.ToLower() == studentNumber
                select new GeneralRecord { Student = s, User = u }
            ).FirstOrDefaultAsync();

            return ToStudent(r);
        }

        public async Task<Student> GetStudentByStudentNumber(string studentNumber, string phoneNumber)
        {
            if (string.IsNullOrEmpty(studentNumber)) return null;

            studentNumber = studentNumber.ToLower();

            var r = await (
                from s in _mainDbContext.Students
                join u in _mainDbContext.Users on s.StudentId equals u.Id
                where s.StudentNumber.ToLower() == studentNumber
                   && u.PhoneNumber == phoneNumber
                select new GeneralRecord { Student = s, User = u }
            ).FirstOrDefaultAsync();

            return ToStudent(r);
        }

        private Student ToStudent(GeneralRecord record)
        {
            if (record == null) return null;
            var student = record.Student;
            if (student == null) return null;
            if (student.User == null) student.User = record.User;
            return student;
        }

        public Task<Student> GetStudentByUserId(long userId)
        {
            return GetStudentById(userId);
        }

        public async Task<GeneralDto> GetAllStudentsOfTeacherAsync(long facultyId, long? classId)
        {
            var tClasses = (classId == null
                ? _mainDbContext.Classes
                : _mainDbContext.Classes.Where(c => c.ClassId == classId));

            var ps = (await tClasses
                .Join(_mainDbContext.ClassFaculties.Where(f => f.FacultyId == facultyId), c => c.ClassId, cf => cf.ClassId,
                    (c, cf) => new { c, cf })
                .Join(_mainDbContext.ClassStudents, p => p.cf.ClassId, cs => cs.ClassId,
                    (p, cs) => new { p.c, p.cf, cs })
                .Join(_mainDbContext.Students, p => p.cs.StudentId, s => s.StudentId,
                    (p, s) => new { p.c, p.cf, p.cs, s })
                .Join(_mainDbContext.Users, p => p.s.StudentId, u => u.Id,
                    (p, u) => new { p.c, p.cf, p.cs, p.s, u })
                .ToListAsync());

            var classes        = ps.Select(p => p.c).Distinct().ToArray();
            var classFaculties = ps.Select(p => p.cf).Distinct().ToArray();
            var classStudents  = ps.Select(p => p.cs).Distinct().ToArray();
            var students       = ps.Select(p => p.s).Distinct().ToArray();
            var users          = ps.Select(p => p.u).Distinct().ToArray();

            return new GeneralDto
            {
                Students       = students,
                Users          = users,
                Classes        = classes,
                ClassStudents  = classStudents,
                ClassFaculties = classFaculties,
            };
        }

        public IQueryable<GeneralRecord> GetStudentsByActivity(long activityId, long classId)
        {
            return (
                from student in _mainDbContext.Students
                join classStudent in _mainDbContext.ClassStudents.Where(x => x.ClassId == classId) on student.StudentId equals classStudent.StudentId
                join score in _mainDbContext.ActivityScores.Where(a => a.ActivityId == activityId) on student.StudentId equals score.StudentId
                    into ss from score in ss.DefaultIfEmpty()
                join user in _mainDbContext.Users on student.StudentId equals user.Id
                select new GeneralRecord { Student = student, ActivityScore = score, User = user, ClassStudent = classStudent }
            );
        }

        public async Task<GeneralDto> GetStudentDetailForTeacherAsync(long facultyId, long studentId, long? classId)
        {
            var student = await _mainDbContext.Students
                .FirstOrDefaultAsync(t => t.StudentId == studentId);
            if (student == null) return null;

            var tClass = classId.HasValue
                ? _mainDbContext.Classes.Where(c => c.ClassId == classId)
                : _mainDbContext.Classes;

            var ps = (await tClass
                .Join(_mainDbContext.ClassFaculties.Where(f => f.FacultyId == facultyId), c => c.ClassId, cf => cf.ClassId, (c, cf) => new { c, cf })
                .Join(_mainDbContext.ClassStudents.Where(f => f.StudentId == studentId), p => p.c.ClassId, cs => cs.ClassId, (p, cs) => new { p.c, p.cf, cs })
                .ToListAsync());
            if (ps.Count < 1) return null;

            var classes = ps.Select(p => p.c).Distinct().ToArray();
            var classIds = classes.Select(c => c.ClassId).ToArray();

            var activities = await _mainDbContext.Activities.Where(a => classIds.Contains(a.ClassId)).ToArrayAsync();
            var activityIds = activities.Select(a => a.ActivityId).ToArray();

            var activityScores = await _mainDbContext.ActivityScores
                .Where(_as => activityIds.Contains(_as.ActivityId) && _as.StudentId == studentId)
                .ToArrayAsync();

            var user = await _userService.GetUserById(student.StudentId);

            return new GeneralDto
            {
                Students       = new[] {student},
                Users          = new[] {user},
                Classes        = classes,
                ClassFaculties = ps.Select(p => p.cf).Distinct().ToArray(),
                ClassStudents  = ps.Select(p => p.cs).Distinct().ToArray(),
                Activities     = activities,
                ActivityScores = activityScores,
            };
        }
        public async Task<GeneralDto> GetStudentsOfSchool(long schoolId
            , int pageIndex
            , int pageSize
            , long excludedClassId
            , long filteredClassId
            , string searchText
            , string sortColumnKey
            , string sortOrder
            )
        {
            var records = await (from student in _mainDbContext.Students
                                 join ss in _mainDbContext.SchoolStudents on student.StudentId equals ss.StudentId
                                 join cs in _mainDbContext.ClassStudents on ss.StudentId equals cs.StudentId into scs
                                 from cs in scs.DefaultIfEmpty()
                                 join user in _mainDbContext.Users on student.StudentId equals user.Id
                                 where ss.SchoolId == schoolId
                                     && !(from c in _mainDbContext.ClassStudents where c.ClassId == excludedClassId select c.StudentId).Contains(student.StudentId)
                                     && (filteredClassId != -1 ? cs.ClassId == filteredClassId : true)
                                     && (!string.IsNullOrWhiteSpace(searchText) ? (user.FullName.IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) != -1
                                     || student.StudentNumber.IndexOf(searchText, StringComparison.CurrentCultureIgnoreCase) != -1) : true)
                                 select new GeneralRecord
                                 {
                                     Student = student,
                                     User = user,
                                     SchoolStudent = ss,
                                     ClassStudent = cs
                                 }).ToListAsync();
            var dto = GeneralRecord.ToGeneralDto(records);
            if (pageSize != -1)
            {
                if (string.IsNullOrWhiteSpace(sortColumnKey) || string.IsNullOrWhiteSpace(sortOrder))
                {
                    dto.Students = dto.Students.Skip(pageSize * pageIndex).Take(pageSize).ToArray();
                }
                else
                {
                    if (sortColumnKey == "fullName")
                    {
                        if (sortOrder == "ascend")
                        {
                            dto.Users = dto.Users.OrderBy(u => u.FullName).Skip(pageSize * pageIndex).Take(pageSize).ToArray();
                        }
                        else
                        {
                            dto.Users = dto.Users.OrderByDescending(u => u.FullName).Skip(pageSize * pageIndex).Take(pageSize).ToArray();
                        }
                        dto.Students = dto.Students.Where(s => dto.Users.Any(u => u.Id == s.StudentId)).ToArray();
                    }
                    else
                    {
                        if (sortOrder == "ascend")
                        {
                            dto.Students = dto.Students.OrderBy(s=>s.StudentNumber).Skip(pageSize * pageIndex).Take(pageSize).ToArray();
                        }
                        else
                        {
                            dto.Students = dto.Students.OrderByDescending(s => s.StudentNumber).Skip(pageSize * pageIndex).Take(pageSize).ToArray();
                        }
                    }
                }

            }
            return dto;
        }

        public IQueryable<GeneralRecord> GetStudentsOfDistrict(long districtId)
        {
            return (
                from student in _mainDbContext.Students
                join ss in _mainDbContext.SchoolStudents on student.StudentId equals ss.StudentId
                join school in _mainDbContext.Schools on ss.SchoolId equals school.SchoolId
                join user in _mainDbContext.Users on student.StudentId equals user.Id
                where school.CurrentGradingTerm == ss.GradingPeriod &&
                        school.DistrictId == districtId
                select new GeneralRecord{ Student = student, User = user, School = school, SchoolStudent = ss }
            );
        }

        public IQueryable<GeneralRecord> GetStudentsInClass(long classId, bool includeUserInfo = false)
        {
            return includeUserInfo ? (
                    from s in _mainDbContext.Students
                    join cs in _mainDbContext.ClassStudents.Where(f => f.ClassId == classId) on s.StudentId equals cs.StudentId
                    join u in _mainDbContext.Users on s.StudentId equals u.Id
                    select new GeneralRecord{ Student = s, ClassStudent = cs, User = u}
                ) : (
                    from s in _mainDbContext.Students
                    join cs in _mainDbContext.ClassStudents.Where(f => f.ClassId == classId) on s.StudentId equals  cs.StudentId
                    select new GeneralRecord{ Student = s, ClassStudent = cs}
                );
        }

        public IQueryable<GeneralRecord> GetStudentsOfParent(long parentId)
        {
            return (
                from p in _mainDbContext.ParentStudents
                join s in _mainDbContext.Students on p.StudentId equals s.StudentId
                join u in _mainDbContext.Users on s.StudentId equals u.Id
                where p.ParentId == parentId
                select new GeneralRecord { Student = s, User = u }
            );
        }

        public async Task UpdateStudentAsync(Student student)
        {
            var s = await GetStudentById(student.StudentId);

            s.ExternalId = student.ExternalId;

            await _mainDbContext.UpdateAsync(s, true);
        }

        public async Task<IEnumerable<Activity>> GetOpenActivities(long studentId)
        {
            return await _mainDbContext.ClassStudents
               .Where(cs => cs.StudentId == studentId)
               .SelectMany(cs => cs.Class.Activities)
               .Where(Activity.IsOpenExpr)
               .ToArrayAsync();
        }

        public async Task<IEnumerable<ActivityScore>> GetActivityScores(long studentId)
        {
            return await _mainDbContext.ActivityScores
               .Where(cs => cs.StudentId == studentId)
               .ToArrayAsync();
        }
    }
}

