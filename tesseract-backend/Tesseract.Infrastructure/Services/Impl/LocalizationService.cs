﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class LocalizationService : ILocalizationService
    {
        private readonly MainDbContext _mainDbContext;

        public LocalizationService(MainDbContext mainDbContext)
        {
            _mainDbContext = mainDbContext;
        }

        public async Task<List<string>> GetAllLocales()
        {
            return await _mainDbContext.Localizations.Select(r => r.Locale).Distinct().ToListAsync();
        }

        public async Task<IList<Localization>> GetLocale(string language)
        {
            return await _mainDbContext.Localizations.Where(r => r.Locale == language).ToListAsync();
        }

        public async Task<int> UpdateLocale(string language, IEnumerable<Localization> records)
        {
            _mainDbContext.Localizations.RemoveRange(_mainDbContext.Localizations.Where(r => r.Locale == language));
            _mainDbContext.Localizations.AddRange(records);
            return await _mainDbContext.SaveChangesAsync();
        }
    }
}
