﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Exceptions;
using Tesseract.Core.Services.Interface.Repository;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Data.NoSql;
using Tesseract.Infrastructure.Services.Interface;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class ClassRepo : IClassRepo
    {
        private readonly MainDbContext _mainDbContext;
        private readonly IActDocService _actDocService;

        public ClassRepo(MainDbContext mainDbContext, IActDocService actDocService)
        {
            _mainDbContext = mainDbContext;
            _actDocService = actDocService;
        }

        public async Task<Class> AddClass(Class classItem, long facultyId)
        {
            var item = await AddTemplate(classItem);
            await _mainDbContext.AddLinkEntityAsync(new ClassFaculty() { ClassId = item.ClassId, FacultyId = facultyId }, true);
            return item;
        }

        public async Task<Class> AddTemplate(Class template)
        {
            var storedTemplate =  await _mainDbContext.AddAsync(template, true);
            if (storedTemplate?.Activities == null) throw new ArgumentNullException();

            foreach (var activity in storedTemplate.Activities)
            {
                if (activity.Document != null)
                {
                    activity.Document.ActivityId = activity.ActivityId;
                    activity.Document.Id = null;
                    activity.Document = await _actDocService.Create(activity.Document);
                }
                
                if (activity.Parent?.ActivityId > 0)
                {
                    activity.ParentActivityId = activity.Parent.ActivityId;
                }
            }

            await _mainDbContext.UpdateAsync(storedTemplate, true);

            return storedTemplate;
        }

        public async Task DeleteTemplate(long templateId)
        {
            var template = await GetTemplate(templateId);
            if (template == null) throw new ResourceNotFoundException();

            foreach(var activity in template.Activities){
                if (activity.Document != null){
                    await _actDocService.DeleteByActivityId(activity.ActivityId);
                }
                await _mainDbContext.DeleteAsync(activity);
            }

            await _mainDbContext.DeleteAsync(template);
        }

        public async Task<Class> GetClass(long classId)
        {
            var classItem = await _mainDbContext.Classes.FirstOrDefaultAsync(t => t.ClassId == classId);
            if (classItem == null) return null;
            await _mainDbContext.Entry(classItem).Collection(cls => cls.Activities).LoadAsync();

            foreach (var activity in classItem.Activities)
            {
                activity.Document = await _actDocService.Get(activity.ActivityId);
                activity.Parent = activity.ParentActivityId == null ? null : classItem.Activities.FirstOrDefault(act => act.ActivityId == activity.ParentActivityId);
            }

            return classItem;
        }

        public async Task<Class> GetTemplate(long templateId)
        {
            var template =  await _mainDbContext.Classes.FirstOrDefaultAsync(cl => cl.ClassId == templateId && cl.IsTemplate && !cl.IsDeleted);
            if (template == null) return null;
            await _mainDbContext.Entry(template).Collection(cls => cls.Activities).LoadAsync();
            await _mainDbContext.Entry(template).Reference(cls => cls.Term).LoadAsync();
            foreach (var activity in template.Activities)
            {
                activity.Document = await _actDocService.Get(activity.ActivityId);
                activity.Parent = activity.ParentActivityId == null ? null : template.Activities.FirstOrDefault(act => act.ActivityId == activity.ParentActivityId);
            }

            return template;
        }

        public async Task<IEnumerable<Class>> GetTemplates(long facultyId)
        {
            return await _mainDbContext.Classes
                .IgnoreQueryFilters()
                .Where(cl => cl.CreatedBy == facultyId && cl.IsTemplate && !cl.IsDeleted)
                .ToArrayAsync();
        }
    }
}
