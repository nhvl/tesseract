﻿using System.Threading.Tasks;
using Tesseract.Infrastructure.Data;
using Tesseract.Infrastructure.Services.Interface;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Infrastructure.Services.Impl
{
    public class LoginLogService: ILoginLogService{
        private readonly MainDbContext _mainDbContext;

        public LoginLogService(MainDbContext mainDbContext)
        {
            _mainDbContext = mainDbContext;
        }

        public async Task CreateLoginAsync(LoginLog login)
        {
            await _mainDbContext.AddLinkEntityAsync(login, true);
        }
    }
}
