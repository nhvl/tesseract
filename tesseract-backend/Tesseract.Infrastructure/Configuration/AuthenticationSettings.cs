﻿namespace Tesseract.Infrastructure.Configuration
{
    public class AuthenticationSettings
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string ValidateTokenKey { get; set; }
        public int DeltaMinutes { get; set; }
        public int ExpiryHours { get; set; }
    }
}
