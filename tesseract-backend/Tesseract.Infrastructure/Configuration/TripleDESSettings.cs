﻿namespace Tesseract.Infrastructure.Configuration
{
    public sealed class TripleDESSettings
    {
        public string Key { get; set; }
        public string IV { get; set; }
    }
}
