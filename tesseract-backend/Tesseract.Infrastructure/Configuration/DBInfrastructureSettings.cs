﻿namespace Tesseract.Infrastructure.Configuration
{
    public sealed class DBInfrastructureSettings
    {
        public string MainConnection { get; set; }
        public string LogConnection { get; set; }
        public string MongoDbConnection { get; set; }
    }
}
