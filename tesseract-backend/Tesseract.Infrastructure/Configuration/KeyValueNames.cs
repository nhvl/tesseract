﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tesseract.Infrastructure.Configuration
{
    public class KeyValueNames
    {
        public static string CommunicationColor = "CommunicationColor";
        public static string CollaborationColor = "CollaborationColor";
        public static string CharacterColor = "CharacterColor";
        public static string CreativityColor = "CreativityColor";
        public static string CriticalThinkingColor = "CriticalThinkingColor";

        public static string ActivityCategoryColorPrefix = "ActivityCategory_";
        public static readonly int ActivityCategoryColorMaxAmount = 10;

        public static string GoogleTagManager = "GoogleTagManager";
    }
}
