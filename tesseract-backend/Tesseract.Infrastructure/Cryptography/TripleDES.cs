﻿using System;
using System.Security.Cryptography;
using System.Text;
using Tesseract.Infrastructure.Configuration;

namespace Tesseract.Infrastructure.Cryptography
{
    public class TripleDES
    {
        private readonly TripleDESSettings _settings;

        protected byte[] Key { get; private set; }
        protected byte[] IV { get; private set; }

        public TripleDES(TripleDESSettings settings)
        {
            _settings = settings;

            if (!string.IsNullOrEmpty(_settings.Key))
                Key = Convert.FromBase64String(_settings.Key);

            if (!string.IsNullOrEmpty(_settings.IV))
                IV = Convert.FromBase64String(_settings.IV);
        }

        public string Encrypt(string plaintext)
        {
            // do not encrypt in development environment
            if (string.IsNullOrEmpty(_settings.Key))
                return plaintext;

            if (string.IsNullOrEmpty(plaintext))
                return string.Empty;

            using (TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider())
            using (ICryptoTransform encryptor = tripleDES.CreateEncryptor(Key, IV))
            {
                byte[] plainbytes = Encoding.Unicode.GetBytes(plaintext),
                encrypted = encryptor.TransformFinalBlock(plainbytes, 0, plainbytes.Length);

                return Convert.ToBase64String(encrypted);
            }
        }

        public string Decrypt(string cipher)
        {
            // do not decrypt in development environment
            if (string.IsNullOrEmpty(_settings.Key))
                return cipher;

            if (string.IsNullOrEmpty(cipher))
                return string.Empty;

            using (TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider())
            using (ICryptoTransform decryptor = tripleDES.CreateDecryptor(Key, IV))
            {
                byte[] cipherBytes = Convert.FromBase64String(cipher),
                    decrypted = decryptor.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length);

                return Encoding.Unicode.GetString(decrypted);
            }
        }
    }
}
