﻿using System.Threading.Tasks;
using Tesseract.Core.Services.Interface.Authorization;
using Tesseract.Infrastructure.Data;

namespace Tesseract.Infrastructure.Authorization
{
    public class DefaultResouceAccessHandler : IResouceAccessHandler
    {
        private readonly MainDbContext _mainDbContext;

        public DefaultResouceAccessHandler(MainDbContext mainDbContext)
        {
            _mainDbContext = mainDbContext;
        }

        public IEntityHandler Identity(EntityIndex<long> identity) => new DefaultAccessHandler(_mainDbContext, identity);

        private class DefaultAccessHandler : IEntityHandler
        {
            private readonly MainDbContext _mainDbContext;
            private readonly EntityIndex<long> _identityIndex;

            public DefaultAccessHandler(MainDbContext mainDbContext, EntityIndex<long> identityIndex)
            {
                _mainDbContext = mainDbContext;
                _identityIndex = identityIndex;
            }

            public async Task<bool> CanAccess(EntityIndex<string> resource) => await UtilityPermisionHandler.Create(_mainDbContext, _identityIndex.EntityType, resource.EntityType).CheckAccessPermission(_identityIndex.Key, resource.Key);
        }
    }
}
