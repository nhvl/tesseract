﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Tesseract.Common.Extensions;
using Tesseract.Core.Entities.Main;
using Tesseract.Core.Services.Interface.Authorization;
using Tesseract.Infrastructure.Data;

namespace Tesseract.Infrastructure.Authorization
{
    public partial class UtilityPermisionHandler
    {
        private readonly MainDbContext _mainDbContext;
        private MethodInfo _registeredMethod;

        private static Dictionary<(Type, Type), string> MethodMapper = new Dictionary<(Type, Type), string>
        {
            [(typeof(Faculty), typeof(Class))] = nameof(FacultyToClass),
            [(typeof(Faculty), typeof(Activity))] = nameof(FacultyToActivity),
            [(typeof(Faculty), typeof(School))] = nameof(FacultyToSchool),
            [(typeof(Faculty), typeof(GradingTerm))] = nameof(FacultyToGradingTerm),
            [(typeof(Student), typeof(Class))] = nameof(StudentToClass),
            [(typeof(Student), typeof(Activity))] = nameof(StudentToActivity),
            [(typeof(Student), typeof(School))] = nameof(StudentToSchool),
            [(typeof(Student), typeof(GradingTerm))] = nameof(StudentToGradingTerm),
        };

        private UtilityPermisionHandler(MainDbContext mainDbContext, string methodName)
        {
            _mainDbContext = mainDbContext;
            _registeredMethod = this.GetType().GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (_registeredMethod?.GetParameters().Length != 2)
            {
                throw new AccessHandlerNotFoundException(($"Method {_registeredMethod.Name ?? "<Null>"} must have 2 parameter"));
            }
        }

        public static UtilityPermisionHandler Create(MainDbContext mainDbContext, Type identityType, Type resourceType)
        {
            if (MethodMapper.TryGetValue((identityType, resourceType), out string methodName))
            {
                return new UtilityPermisionHandler(mainDbContext, methodName);
            }

            throw new AccessHandlerNotFoundException();
        }

        public Task<bool> CheckAccessPermission(long id, string resourceId)
        {
            var resourceParam = _registeredMethod.GetParameters()[1];
            var refinedResourceId = resourceParam.ParameterType == typeof(long) ? resourceId.ToLong() : resourceId as object;
            return _registeredMethod.Invoke(this, new object[] { id, refinedResourceId }) as Task<bool>;
        }
    }

    public partial class UtilityPermisionHandler
    {

        private async Task<bool> FacultyToClass(long id, long classId)
        {
            return await _mainDbContext.ClassFaculties.AnyAsync(x => x.ClassId == classId && x.FacultyId == id)
                    || await _mainDbContext.Classes.AnyAsync(cls => cls.ClassId == classId && cls.IsTemplate && cls.CreatedBy == id);
                            
        }

        private async Task<bool> FacultyToActivity(long id, long activityId)
        {
            var classId = await _mainDbContext.Activities.Where(act => act.ActivityId == activityId).Select(x => x.ClassId).FirstOrDefaultAsync();
            return await FacultyToClass(id, classId);
        }

        private async Task<bool> FacultyToSchool(long id, long schoolId)
        {
            return await _mainDbContext.SchoolFaculties.AnyAsync(sf => sf.SchoolId == schoolId && sf.FacultyId == id);
        }

        private async Task<bool> FacultyToGradingTerm(long id, long gradingTermId)
        {
            return await _mainDbContext.GradingTerms
                            .Where(gt => gt.GradingTermId == gradingTermId)
                            .Join(_mainDbContext.SchoolFaculties.Where(sf => sf.FacultyId == id), gt => gt.SchoolId, sf => sf.SchoolId, (_, __) => true)
                            .AnyAsync();
        }
    }

    
    public partial class UtilityPermisionHandler
    {

        private async Task<bool> StudentToClass(long id, long classId)
        {
            return await _mainDbContext.ClassStudents.AnyAsync(x => x.ClassId == classId && x.StudentId == id);                   
        }

        private async Task<bool> StudentToActivity(long id, long activityId)
        {
            var classId = await _mainDbContext.Activities.Where(act => act.ActivityId == activityId).Select(x => x.ClassId).FirstOrDefaultAsync();
            return await StudentToClass(id, classId);
        }

        private async Task<bool> StudentToSchool(long id, long schoolId)
        {
            return await _mainDbContext.SchoolStudents.AnyAsync(sf => sf.SchoolId == schoolId && sf.StudentId == id);
        }

        private async Task<bool> StudentToGradingTerm(long id, long gradingTermId)
        {
            return await _mainDbContext.GradingTerms
                            .Where(gt => gt.GradingTermId == gradingTermId)
                            .Join(_mainDbContext.SchoolStudents.Where(sf => sf.StudentId == id), gt => gt.SchoolId, sf => sf.SchoolId, (_, __) => true)
                            .AnyAsync();
        }
    }

}
