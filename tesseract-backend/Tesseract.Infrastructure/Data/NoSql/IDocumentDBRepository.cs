﻿using Microsoft.Azure.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tesseract.Core.Entities.NoSql;

namespace Tesseract.Infrastructure.Data.NoSql
{
    public interface IDocumentDBRepository : IDisposable
    {
        Task CreateDatabaseIfNotExistsAsync();
        Task CreateCollectionIfNotExistsSimple<T>() where T : BaseDocument;
        Task CreateCollectionIfNotExistsSimple(Type objectType);
        Task CreateCollectionIfNotExistsAsync<T>() where T : BaseDocument;
        Task CreateCollectionIfNotExistsAsync<T>(string partitionKey) where T : BaseDocument;


        T GetById<T>(string id) where T : BaseDocument;
        Task<T> GetByIdAsync<T>(string id) where T : BaseDocument;
        Task<T> GetByIdAsync<T>(string partitionId, string id) where T : BaseDocument;
        Task<T> GetById2Async<T>(string id) where T : BaseDocument;


        Task<T> GetOne<T>(Expression<Func<T, bool>> predicate) where T : BaseDocument;
        Task<T> GetOne<T>(Func<IOrderedQueryable<T>, IQueryable<T>> queryBuilder) where T : BaseDocument;

        Task<IEnumerable<T>> GetAll<T>() where T : BaseDocument;
        IEnumerable<T> GetMany<T>(Expression<Func<T, bool>> predicate) where T : BaseDocument;
        Task<IEnumerable<T>> GetManyAsync<T>(Expression<Func<T, bool>> predicate) where T : BaseDocument;
        Task<IEnumerable<T>> GetManyAsync<T>(Func<IOrderedQueryable<T>, IQueryable<T>> queryBuilder) where T : BaseDocument;

        Task<int> Count<T>(Func<IOrderedQueryable<T>, IQueryable<T>> queryBuilder) where T : BaseDocument;


        Task<Document> InsertAsync<T>(T entity) where T : BaseDocument;
        Task InsertAsync<T>(IEnumerable<T> entities) where T : BaseDocument;

        Task<Document> UpsertAsync<T>(T entity) where T : BaseDocument;

        Task<Document> UpdateAsync<T>(T entity) where T : BaseDocument;
        Task<Document> UpdateAsync<T>(T entity, string partitionId) where T : BaseDocument;

        Task DeleteAsync<T>(string id) where T : BaseDocument;
        Task DeleteAsync<T>(string partitionId, string id) where T : BaseDocument;
        Task DeleteAsync<T>(T entity) where T : BaseDocument;

        Uri GetDocumentCollectionUri<T>();
        Uri GetDocumentUri<T>(string id);
    }
}