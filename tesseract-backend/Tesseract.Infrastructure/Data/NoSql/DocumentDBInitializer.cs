﻿using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.NoSql;
using Tesseract.Infrastructure.Data.NoSql;

namespace Tesseract.Infrastructure.Data
{
    public static class DocumentDBInitializer
    {
        public static async void Init(IDocumentDBRepository repo, ILogger<IDocumentDBRepository> logger)
        {
            await repo.CreateDatabaseIfNotExistsAsync();
            await repo.CreateCollectionIfNotExistsSimple(typeof(StudentActDoc));
            await Task.WhenAll(GetNoSqlDataTypes().Select(repo.CreateCollectionIfNotExistsSimple));

            // HACK: huyn 2019-05-17 following https://stackoverflow.com/questions/33088927/documentdb-performance-issues DocumentDB always takes first request longer than others. So I call the first request here to avoid user encoutering as slow peformance.
            try
            {
                repo.GetById<SchoolReport>("1");
            }
            catch(Exception ex)
            {
                logger.LogError(ex, ex.Message);
            }
        }

        public static Type[] GetNoSqlDataTypes()
        {
            return typeof(ActDoc).Assembly.GetTypes()
                                          .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(BaseDocument)))
                                          .ToArray();
        }
    }
}
