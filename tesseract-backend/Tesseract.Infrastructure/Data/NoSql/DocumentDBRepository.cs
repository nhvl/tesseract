﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tesseract.Core.Entities.NoSql;
using Microsoft.Extensions.Options;
using Tesseract.Core.Entities.Config;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ObjectsComparer;

namespace Tesseract.Infrastructure.Data.NoSql
{
    public class DocumentDBRepository : IDocumentDBRepository
    {
        private readonly DocumentClient _client;
        private readonly ILogger<DocumentDBRepository> _logger;
        private readonly string _databaseId;
        private bool _disposed;
        
        public DocumentDBRepository(IOptions<DocumentDbSettings> settings, ILogger<DocumentDBRepository> logger, IOptions<JsonSerializerSettings> serializerSettings)
        {
            try
            {
                _logger = logger;
                _databaseId = settings.Value.DatabaseId;

                var jsonSettings = serializerSettings?.Value ?? new JsonSerializerSettings();
                jsonSettings.ContractResolver = new DefaultContractResolver(); // CosmosDB creates query using the correct property name. So we avoid converting these names into camel case.
                _client = new DocumentClient(new Uri(settings.Value.EndpointUrl), settings.Value.MasterKey, jsonSettings);
                _client.CreateDatabaseIfNotExistsAsync(new Database { Id = _databaseId });
            }
            catch (Exception ex)
            {
                throw new Exception("Can not access to DocumentDb server.", ex);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            lock (this)
            {
                if (this._disposed)
                {
                    return;
                }

                if (disposing)
                {
                    _client?.Dispose();
                }

                this._disposed = true;
            }
        }

        public async Task CreateDatabaseIfNotExistsAsync()
        {
            await _client.CreateDatabaseIfNotExistsAsync(new Database { Id = _databaseId });
        }
        public async Task CreateCollectionIfNotExistsSimple<T>() where T : BaseDocument
        {
            await CreateCollectionIfNotExistsSimple(typeof(T));
        }

        public async Task CreateCollectionIfNotExistsSimple(Type objectType)
        {
            var collectionId = objectType.Name;

            var collection = new DocumentCollection { Id = collectionId };

            await _client.CreateDocumentCollectionIfNotExistsAsync(
                GetDatabaseUri(), collection);
        }

        public async Task CreateCollectionIfNotExistsAsync<T>() where T : BaseDocument
        {
            var collectionId = typeof(T).Name;

            var collection = new DocumentCollection { Id = collectionId };
            {
                collection.PartitionKey.Paths.Add("/id");
                // Disable indexing if it's just KV lookup
                collection.IndexingPolicy.Automatic = false;
                collection.IndexingPolicy.IndexingMode = IndexingMode.None;
                collection.IndexingPolicy.IncludedPaths.Clear();
                collection.IndexingPolicy.ExcludedPaths.Clear();
            }

            await _client.CreateDocumentCollectionIfNotExistsAsync(
                GetDatabaseUri(), collection,
                new RequestOptions { OfferThroughput = 10000 });
        }

        public async Task CreateCollectionIfNotExistsAsync<T>(string partitionKey) where T : BaseDocument
        {
            var collectionId = typeof(T).Name;

            var collection = new DocumentCollection { Id = collectionId };
            {
                collection.PartitionKey.Paths.Add($"/{partitionKey}");

                collection.IndexingPolicy.Automatic = true;
                collection.IndexingPolicy.IndexingMode = IndexingMode.Consistent;

                var path = new IncludedPath { Path = $"/{partitionKey}/?" };
                {
                    path.Indexes.Add(new RangeIndex(DataType.String) { Precision = -1 });
                }
                collection.IndexingPolicy.IncludedPaths.Clear();
                collection.IndexingPolicy.IncludedPaths.Add(path);

                collection.IndexingPolicy.ExcludedPaths.Clear();
                collection.IndexingPolicy.ExcludedPaths.Add(new ExcludedPath { Path = "/*" });
            }

            await _client.CreateDocumentCollectionIfNotExistsAsync(
                GetDatabaseUri(), collection,
                new RequestOptions { OfferThroughput = 10000 });
        }

        public async Task<T> PerformWithStopwatchAsync<T>(string functionName, Func<Task<T>> func){
            var watch = new Stopwatch();
            watch.Start();
            var result = await func();
            _logger?.LogDebug($"Finished {functionName} in {watch.ElapsedMilliseconds} ms ");
            watch.Stop();
 
            return result;
        }

        public async Task PerformWithStopwatchAsync(string functionName, Func<Task> func){
            var watch = new Stopwatch();
            watch.Start();
            await func();
            _logger?.LogDebug($"Finished {functionName} in {watch.ElapsedMilliseconds} ms ");
            watch.Stop();
        }

        public T GetById<T>(string id) where T : BaseDocument
        {
            return _client.CreateDocumentQuery<T>(GetDocumentCollectionUri<T>())
                .Where(x => x.Id == id).ToList().FirstOrDefault();
        }

        public async Task<T> GetByIdAsync<T>(string id) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(GetByIdAsync), async () => await _client.ReadDocumentAsync<T>(GetDocumentUri<T>(id),
                new RequestOptions { PartitionKey = new PartitionKey(id) }) );
        }

        public async Task<T> GetByIdAsync<T>(string partitionId, string id) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(GetByIdAsync), async () => await _client.ReadDocumentAsync<T>(GetDocumentUri<T>(id),
                new RequestOptions { PartitionKey = new PartitionKey(partitionId) }));
        }

        public async Task<T> GetById2Async<T>(string id) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(GetById2Async), async () => 
            {
                var feedOptions = new FeedOptions
                {
                    EnableCrossPartitionQuery = true,
                    MaxDegreeOfParallelism = -1
                };

                var query = 
                    _client.CreateDocumentQuery<T>(GetDocumentCollectionUri<T>(), feedOptions)
                    .Where(g => g.Id == id)
                    .AsDocumentQuery();

                while (query.HasMoreResults)
                {
                    var response = await query.ExecuteNextAsync<T>();
                    if (response.Count > 0)
                    {
                        return response.First();
                    }
                }

                return null;
            });
        }

        public async Task<T> GetOne<T>(Expression<Func<T, bool>> predicate) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(GetOne), async () => 
            {
                var query = _client.CreateDocumentQuery<T>(GetDocumentCollectionUri<T>())
                    .Where(predicate)
                    .AsDocumentQuery();

                while (query.HasMoreResults)
                {
                    var response = await query.ExecuteNextAsync<T>();
                    if (response.Count > 0)
                    {
                        return response.First();
                    }
                }

                return null;
            });
        }

        public async Task<T> GetOne<T>(Func<IOrderedQueryable<T>, IQueryable<T>> queryBuilder) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(GetOne), async () => 
            {
                var query = queryBuilder(_client.CreateDocumentQuery<T>(GetDocumentCollectionUri<T>()))
                    .AsDocumentQuery();

                while (query.HasMoreResults)
                {
                    var response = await query.ExecuteNextAsync<T>();
                    if (response.Count > 0)
                    {
                        return response.First();
                    }
                }

                return null;
            });
        }



        public IEnumerable<T> GetMany<T>(Expression<Func<T, bool>> predicate) where T : BaseDocument
        {
            return _client.CreateDocumentQuery<T>(GetDocumentCollectionUri<T>())
                .Where(predicate);
        }

        public async Task<IEnumerable<T>> GetAll<T>() where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(GetAll), async () => 
            {
                var query = _client.CreateDocumentQuery<T>(GetDocumentCollectionUri<T>())
                .AsDocumentQuery();

                var items = new List<T>();
                while (query.HasMoreResults)
                {
                    var response = await query.ExecuteNextAsync<T>();
                    items.AddRange(response);
                }

                return items;
            });
        }

        public async Task<IEnumerable<T>> GetManyAsync<T>(Expression<Func<T, bool>> predicate) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(GetManyAsync), async () => 
            {
                var query = _client.CreateDocumentQuery<T>(GetDocumentCollectionUri<T>())
                .Where(predicate)
                .AsDocumentQuery();

                var items = new List<T>();
                while (query.HasMoreResults)
                {
                    var response = await query.ExecuteNextAsync<T>();
                    items.AddRange(response);
                }

                return items;
            });
        }

        public async Task<IEnumerable<T>> GetManyAsync<T>(Func<IOrderedQueryable<T>, IQueryable<T>> queryBuilder) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(GetManyAsync), async () => 
            {
                var query = queryBuilder(_client.CreateDocumentQuery<T>(GetDocumentCollectionUri<T>()))
                .AsDocumentQuery();

                var items = new List<T>();
                while (query.HasMoreResults)
                {
                    var response = await query.ExecuteNextAsync<T>();
                    items.AddRange(response);
                }

                return items;
            });
        }


        public async Task<int> Count<T>(Func<IOrderedQueryable<T>, IQueryable<T>> queryBuilder) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(Count), async () => await queryBuilder(_client.CreateDocumentQuery<T>(GetDocumentCollectionUri<T>()))
                .CountAsync());
        }


        public async Task<Document> InsertAsync<T>(T entity) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(InsertAsync), async () => {
                if (entity == null)
                {
                    throw new ArgumentNullException(nameof(entity));
                }

                var doc = await _client.CreateDocumentAsync(GetDocumentCollectionUri<T>(), entity);

                // audit log
                entity.Id = doc.Resource.Id;
                entity.ETag = doc.Resource.ETag;

                _logger?.LogInformation("{entityName} was Inserted: {@entity}", typeof(T).Name, entity);

                return doc;
            });            
        }

        public async Task InsertAsync<T>(IEnumerable<T> entities) where T : BaseDocument
        {
            await PerformWithStopwatchAsync(nameof(InsertAsync), async () => {
                foreach (var entity in entities)
                {
                    if (entity == null) continue;

                    var doc = await _client.CreateDocumentAsync(GetDocumentCollectionUri<T>(), entity);

                    entity.Id = doc.Resource.Id;
                    entity.ETag = doc.Resource.ETag;
                }

                _logger?.LogInformation("{entityName}(s) was Inserted: {@entities}", typeof(T).Name, entities);
            });
        }

        public async Task<Document> UpsertAsync<T>(T entity) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(UpsertAsync), async () => {
                if (entity == null)
                {
                    throw new ArgumentNullException(nameof(entity));
                }

                var doc = await _client.UpsertDocumentAsync(GetDocumentCollectionUri<T>(), entity);

                // audit log
                entity.Id = doc.Resource.Id;
                entity.ETag = doc.Resource.ETag;

                _logger?.LogInformation("{entityName} was Upserted: {@entity}", typeof(T).Name, entity);

                return doc;
            });
            
        }

        public async Task<Document> UpdateAsync<T>(T entity) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(UpdateAsync), async () => {
                if (entity == null)
                {
                    throw new ArgumentNullException(nameof(entity));
                }

                // Use conditional update with ETag
                var documentUri = GetDocumentUri<T>(entity.Id);
                var orig = await GetByIdAsync<T>(entity.Id);

                var res = await _client.ReplaceDocumentAsync(documentUri, entity,
                    new RequestOptions
                    {
                        AccessCondition = new AccessCondition
                        {
                            Condition = orig.ETag,
                            Type = AccessConditionType.IfMatch
                        }
                    });

                if (orig != null)
                {
                    var comparer = new ObjectsComparer.Comparer<T>();

                    //Compare objects
                    IEnumerable<Difference> differences;
                    var isEqual = comparer.Compare(orig, entity, out differences);

                    if (differences != null && differences.Count() > 0)
                    {
                        _logger?.LogInformation("{entityName} - ID: {id} was Updated: {@differences}", typeof(T).Name, orig.Id, differences);
                    }
                }

                return res;
            });
        }

        public async Task<Document> UpdateAsync<T>(T entity, string partitionId) where T : BaseDocument
        {
            return await PerformWithStopwatchAsync(nameof(UpdateAsync), async () => {
                if (entity == null)
                {
                    throw new ArgumentNullException(nameof(entity));
                }

                // Use conditional update with ETag
                var documentUri = GetDocumentUri<T>(entity.Id);
                var orig = await GetByIdAsync<T>(partitionId, entity.Id);

                var res = await _client.ReplaceDocumentAsync(documentUri, entity,
                    new RequestOptions
                    {
                        AccessCondition = new AccessCondition
                        {
                            Condition = orig.ETag,
                            Type = AccessConditionType.IfMatch
                        }
                    });

                if (orig != null)
                {
                    var comparer = new ObjectsComparer.Comparer<T>();

                    //Compare objects
                    IEnumerable<Difference> differences;
                    var isEqual = comparer.Compare(orig, entity, out differences);

                    if (differences != null && differences.Count() > 0)
                    {
                        _logger?.LogInformation("{entityName} - ID: {id} was Updated: {@differences}", typeof(T).Name, orig.Id, differences);
                    }
                }

                return res;
            });
        }

        public async Task DeleteAsync<T>(string id) where T : BaseDocument
        {
            await PerformWithStopwatchAsync(nameof(DeleteAsync), async () => {
                await _client.DeleteDocumentAsync(GetDocumentUri<T>(id));

                _logger?.LogInformation("{entityName} - ID: {id} was Deleted", typeof(T).Name, id);
            });
        }

        public async Task DeleteAsync<T>(string partitionId, string id) where T : BaseDocument
        {
            await PerformWithStopwatchAsync(nameof(DeleteAsync), async () => {
                await _client.DeleteDocumentAsync(GetDocumentUri<T>(id),
                new RequestOptions { PartitionKey = new PartitionKey(partitionId) });

                _logger?.LogInformation("{entityName} - ID: {id} was Deleted", typeof(T).Name, id);
            });
        }

        public async Task DeleteAsync<T>(T entity) where T : BaseDocument
        {
            await PerformWithStopwatchAsync(nameof(DeleteAsync), async () => {
                await _client.DeleteDocumentAsync(GetDocumentUri<T>(entity.Id));

                _logger?.LogInformation("{entityName} - ID: {id} was Deleted", typeof(T).Name, entity.Id);
            });
            
        }

        public Uri GetDatabaseUri()
        {
            return UriFactory.CreateDatabaseUri(_databaseId);
        }
        public Uri GetDocumentCollectionUri<T>()
        {
            var collectionId = typeof(T).Name;
            return UriFactory.CreateDocumentCollectionUri(_databaseId, collectionId);
        }
        public Uri GetDocumentUri<T>(string id)
        {
            var collectionId = typeof(T).Name;
            return UriFactory.CreateDocumentUri(_databaseId, collectionId, id);
        }
    }
}