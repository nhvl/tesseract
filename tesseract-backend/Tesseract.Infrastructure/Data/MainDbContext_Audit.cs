﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Log;
using Tesseract.Core.Enums;

namespace Tesseract.Infrastructure.Data
{
    public partial class MainDbContext
    {
        private static readonly string[] SKIP_PROPERTIES = { "updatedby", "dateupdated" };

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            // get changes
            var changes = GetChangedValues();

            var result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);

            // save log
            if (result > 0)
            {
                SaveAuditLog(changes);
            }

            return result;
        }

        private List<AuditChange> GetChangedValues()
        {
            var changes = new List<AuditChange>();
            try
            {
                ChangeTracker.DetectChanges();

                foreach (var entry in ChangeTracker.Entries())
                {
                    if (entry.Entity is AuditChange ||
                        entry.Entity is LoginLog ||
                        entry.State == EntityState.Detached ||
                        entry.State == EntityState.Unchanged)
                        continue;

                    var audit = new AuditChange();
                    audit.TableName = entry.Metadata.Relational().TableName;
                    audit.AuditAction = GetAuditActionType(entry.State);

                    audit.PrimaryKey = entry.Metadata.FindPrimaryKey()
                        .Properties
                        .Select(p => new KeyValuePair<string, object>(p.Name, entry.Property(p.Name).CurrentValue))
                        .ToList();

                    foreach (var property in entry.Properties)
                    {
                        if (SKIP_PROPERTIES.Contains(property.Metadata.Name.ToLower()))
                            continue;

                        var oldValue = entry.State == EntityState.Added || property.OriginalValue == null ? "null" : property.OriginalValue.ToString();
                        var newValue = entry.State == EntityState.Deleted || property.CurrentValue == null ? "null" : property.CurrentValue.ToString();

                        if (property.IsTemporary)
                        {
                            audit.TemporaryProperties.Add(property);
                        }

                        if (oldValue != newValue)
                        {
                            var delta = new AuditDelta();
                            delta.FieldName = property.Metadata.Name;
                            delta.ValueBefore = oldValue;
                            delta.ValueAfter = newValue;

                            audit.Changes.Add(delta);
                        }
                    }

                    if (audit.Changes.Count > 0)
                    {
                        changes.Add(audit);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error while GetChangedValues", ex);
            }

            return changes;
        }

        private AuditActionType GetAuditActionType(EntityState state)
        {
            switch (state)
            {
                case EntityState.Added:
                    return AuditActionType.Create;
                case EntityState.Modified:
                    return AuditActionType.Update;
                case EntityState.Deleted:
                    return AuditActionType.Delete;
                default:
                    return AuditActionType.Unknown;
            }
        }

        private void SaveAuditLog(List<AuditChange> changes)
        {
            try
            {
                if (changes == null || changes.Count == 0)
                    return;

                var tempProps = new List<TemporaryProperty>();

                foreach (var audit in changes)
                {
                    foreach (var tempProp in audit.TemporaryProperties)
                    {
                        var prop = audit.Changes.FirstOrDefault(c => c.FieldName == tempProp.Metadata.Name);
                        if (prop != null)
                        {
                            // store for use later in other changes
                            tempProps.Add(new TemporaryProperty()
                            {
                                Name = tempProp.Metadata.Name,
                                TempValue = prop.ValueAfter,
                                NewValue = tempProp.CurrentValue
                            });

                            // update to new value
                            prop.ValueAfter = tempProp.CurrentValue?.ToString();
                        }
                    }

                    // restore temp value in link entity
                    foreach (var t in tempProps)
                    {
                        var p = audit.Changes.FirstOrDefault(c => c.FieldName == t.Name && c.ValueAfter == t.TempValue?.ToString());
                        if (p != null)
                        {
                            // update to new value
                            p.ValueAfter = t.NewValue?.ToString();
                        }
                    }

                    _logger.LogInformation("{DataModel} - ID: {KeyFieldID} was {Action}d: {@Changes}", audit.TableName, GetPrimaryKey(audit.PrimaryKey), audit.AuditAction, audit.Changes);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error while SaveAuditLog", ex);
            }
        }

        private string GetPrimaryKey(List<KeyValuePair<string, object>> pk)
        {
            if (pk == null || pk.Count == 0) return string.Empty;

            if (pk.Count == 1) return pk[0].Value.ToString();

            return $"(${string.Join(",", pk.Select(k => k.Value))})";
        }

        private struct TemporaryProperty
        {
            public string Name { get; set; }
            public object TempValue { get; set; }
            public object NewValue { get; set; }
        }
    }
}