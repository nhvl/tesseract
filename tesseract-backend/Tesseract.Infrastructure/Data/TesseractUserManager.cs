﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Infrastructure.Data
{
    public class TesseractUserManager : UserManager<ApplicationUser>
    {
        private readonly MainDbContext _mainDbContext;
        public TesseractUserManager(MainDbContext mainDbContext, IUserStore<ApplicationUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ApplicationUser> passwordHasher, IEnumerable<IUserValidator<ApplicationUser>> userValidators, IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ApplicationUser>> logger) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _mainDbContext = mainDbContext;
        }

        public override Task<IList<string>> GetRolesAsync(ApplicationUser user) => GetRolesWithSchoolIdAsync(user, user?.ActiveSchoolId);
        public override Task<IdentityResult> AddToRolesAsync(ApplicationUser user, IEnumerable<string> roles) => AddToSchoolRoleAsync(user,null, roles?.ToArray());
        public override Task<IdentityResult> AddToRoleAsync(ApplicationUser user, string role)  => AddToSchoolRoleAsync(user,null, role);
        public override Task<IdentityResult> RemoveFromRoleAsync(ApplicationUser user, string role) => RemoveFromSchoolRolesAsync(user, null, role);
        public override Task<IdentityResult> RemoveFromRolesAsync(ApplicationUser user, IEnumerable<string> roles) => RemoveFromSchoolRolesAsync(user, null, roles?.ToArray()) ;
        public async Task<IList<string>> GetRolesWithSchoolIdAsync(ApplicationUser user, long? schoolId)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            var query = from qr in _mainDbContext.UserRoles
                        join rl in _mainDbContext.Roles on qr.RoleId equals rl.Id
                        where qr.UserId == user.Id && (qr.SchoolId == null || qr.SchoolId == user.ActiveSchoolId)
                        select rl.Name;

            return await query.ToListAsync();
        }

        public async Task<IdentityResult> RemoveFromSchoolRolesAsync(ApplicationUser user, long? schoolId, params string[]roles)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            if (roles == null) throw new ArgumentNullException(nameof(roles));

            foreach (var role in roles)
            {
                var normalizedRole = this.KeyNormalizer.Normalize(role);
                var matchedRole = await _mainDbContext.Roles.FirstOrDefaultAsync(rl => rl.NormalizedName == normalizedRole);
                if (matchedRole == null) continue;

                var userRole = await _mainDbContext.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == user.Id && ur.RoleId == matchedRole.Id && ur.SchoolId == schoolId);
                if (userRole == null) continue;

                _mainDbContext.Remove(userRole);
            }
            await _mainDbContext.SaveChangesAsync();

            return await UpdateUserAsync(user);
        }

        public async Task<IdentityResult> AddToSchoolRoleAsync(ApplicationUser user, long? schoolId, params string[] roles)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));
            if (roles == null) throw new ArgumentNullException(nameof(roles));

            foreach (var role in roles)
            {
                var normalizedRole = this.KeyNormalizer.Normalize(role);
                var matchedRole = await _mainDbContext.Roles.FirstOrDefaultAsync(rl => rl.NormalizedName == normalizedRole);
                if (matchedRole == null) continue;

                var existed = await _mainDbContext.UserRoles.AnyAsync(ur => ur.UserId == user.Id && ur.RoleId == matchedRole.Id && ur.SchoolId == schoolId);
                if (existed) continue;

                await _mainDbContext.AddAsync(new ApplicationUserRole { RoleId = matchedRole.Id, UserId = user.Id, SchoolId = schoolId });
            }

            await _mainDbContext.SaveChangesAsync();
            return await UpdateUserAsync(user);
        }

    }
}
