﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Tesseract.Infrastructure.Data
{
    public static class RoleManagerExtension
    {
        public static async Task<IdentityResult> AddClaimIfNotExistsAsync<TRole>(this RoleManager<TRole> roleManager, TRole role, Claim claim) where TRole : class
        {
            var claims = await roleManager.GetClaimsAsync(role);
            if (claims.Any(cl => cl.Type == claim.Type && cl.Value == claim.Value)) return IdentityResult.Success;


            return await roleManager.AddClaimAsync(role, claim); ;
        }

        public static async Task<IList<Claim>> GetClaimsAsync<TRole>(this RoleManager<TRole> roleManager, IEnumerable<string> roleNames) where TRole : class
        {
            var claims = new List<Claim>();
            foreach (var role in roleNames)
            {
                var appRole = await roleManager.FindByNameAsync(role);
                if (appRole != null)
                {
                    claims.AddRange(await roleManager.GetClaimsAsync(appRole));
                }
            }

            return claims;
        }
    }
}