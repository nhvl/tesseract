﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Tesseract.Core.Entities.Config;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Configuration;
using Tesseract.Infrastructure.Models;

namespace Tesseract.Infrastructure.Data
{
    public partial class MainDbContext
    {
        private void SeedTestingData(ModelBuilder modelBuilder)
        {
            this.SeedDistrictAndSchool(modelBuilder);
            this.SeedGradingTerms(modelBuilder);
            this.SeedClass(modelBuilder);
            this.SeedStudent(modelBuilder);
            this.SeedLanguage(modelBuilder);
            this.SeedKeyValue(modelBuilder);
            this.SeedEmailAcount(modelBuilder);
            this.SeedEmailTemplate(modelBuilder);
        }

        private void SeedEmailAcount(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmailAccount>().HasData(new EmailAccount { Port = 587, DisplayName = "Tesseract", Email = "info@tesseract.com", EnableSsl = true, IsDeleted = false, Username = "maxkingsouth2016", Password = "lenguyen@894047", Host = "smtp.sendgrid.net", UseDefaultCredentials = false, EmailAccountId = 1, CreatedBy = 1, DateCreated = new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc) });
        }

        private void SeedEmailTemplate(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmailTemplate>().HasData(new EmailTemplate { Body = "<p>If you did not request to reset your password for your {{SCHOOL_DISTRICT_NAME}} portal, you may ignore this email.</p><p>To reset your password, <a href='{{link}}'>[Click here]</a> or copy and paste this link into your browser:{{link}}</p>", CreatedBy = 1, DateCreated = new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), EmailTemplateId = 1, IsDeleted = false, Name = "Account.ForgotPassword", Subject = "{{SCHOOL_DISTRICT_NAME}} - Your password reset instructions", });
            modelBuilder.Entity<EmailTemplate>().HasData(new EmailTemplate { Body = "<p>Welcome to Tesseract!</p><p>To active your account, <a href='{{link}}'>[Click here]</a> or copy and paste this link into your browser:{{link}}</p>", CreatedBy = 1, DateCreated = new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), EmailTemplateId = 2, IsDeleted = false, Name = "Account.ConfirmEmail", Subject = "Welcome to Tesseract", });
            modelBuilder.Entity<EmailTemplate>().HasData(new EmailTemplate { Body = "<p>{{STUDENT_NAME}}</p><p>{{PARENT_NAME}} ({{PARENT_EMAIL}}) has registered as your parent.</p><p>If this is a mistake, please contact your administrator.</p>", CreatedBy = 1, DateCreated = new DateTime(2019, 6, 12, 0, 0, 0, 0, DateTimeKind.Utc), EmailTemplateId = 3, IsDeleted = false, Name = "Account.NotifyStudent", Subject = "{{PARENT_NAME}} has registered as your parent", });
        }

        private void MakeForeignKeys(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Faculty>().Property(et => et.FacultyId).ValueGeneratedNever();
            modelBuilder.Entity<Student>().Property(et => et.StudentId).ValueGeneratedNever();
            modelBuilder.Entity<Parent>().Property(et => et.ParentId).ValueGeneratedNever();

            modelBuilder.Entity<ApplicationUser>().HasOne(a => a.Parent).WithOne(b => b.User).HasForeignKey<Parent>(u => u.ParentId);
            modelBuilder.Entity<ApplicationUser>().HasOne(a => a.Faculty).WithOne(b => b.User).HasForeignKey<Faculty>(u => u.FacultyId);
            modelBuilder.Entity<ApplicationUser>().HasOne(a => a.Student).WithOne(b => b.User).HasForeignKey<Student>(u => u.StudentId);

            modelBuilder.Entity<SchoolFaculty>().HasKey(c => new { c.SchoolId, c.FacultyId });
            modelBuilder.Entity<SchoolFaculty>().HasOne(pt => pt.School).WithMany("SchoolFaculties").HasForeignKey(pt => pt.SchoolId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<SchoolFaculty>().HasOne(pt => pt.Faculty).WithMany("SchoolFaculties").HasForeignKey(pt => pt.FacultyId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<SchoolStudent>().HasKey(c => new { c.SchoolId, c.StudentId, c.GradingPeriod });
            modelBuilder.Entity<SchoolStudent>().HasOne(pt => pt.School).WithMany("SchoolStudents").HasForeignKey(pt => pt.SchoolId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<SchoolStudent>().HasOne(pt => pt.Student).WithMany("SchoolStudents").HasForeignKey(pt => pt.StudentId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ParentStudent>().HasKey(c => new { c.ParentId, c.StudentId });
            modelBuilder.Entity<ParentStudent>().HasOne(pt => pt.Parent).WithMany("ParentsOfStudent").HasForeignKey(pt => pt.ParentId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ParentStudent>().HasOne(pt => pt.Student).WithMany("StudentsOfParent").HasForeignKey(pt => pt.StudentId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ClassFaculty>().HasKey(c => new { c.ClassId, c.FacultyId });
            modelBuilder.Entity<ClassFaculty>().HasOne(pt => pt.Class).WithMany("ClassFaculties").HasForeignKey(pt => pt.ClassId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ClassFaculty>().HasOne(pt => pt.Faculty).WithMany("ClassFaculties").HasForeignKey(pt => pt.FacultyId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ClassStudent>().HasKey(c => new { c.ClassId, c.StudentId });
            modelBuilder.Entity<ClassStudent>().HasOne(pt => pt.Class).WithMany("ClassStudents").HasForeignKey(pt => pt.ClassId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ClassStudent>().HasOne(pt => pt.Student).WithMany("ClassStudents").HasForeignKey(pt => pt.StudentId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Activity>().HasOne(act => act.Class).WithMany("Activities").HasForeignKey(act => act.ClassId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ActivityScore>().HasKey(c => new { c.ActivityId, c.StudentId });
            modelBuilder.Entity<ActivityScore>().HasOne(aS => aS.Activity).WithMany(a => a.Scores).HasForeignKey(score => score.ActivityId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ActivityScore>().HasOne(aS => aS.Student).WithMany(a => a.Scores).HasForeignKey(score => score.StudentId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<SchoolAdmin>().HasKey(c => new { c.UserId, c.SchoolId });
            modelBuilder.Entity<SchoolAdmin>().HasOne(pt => pt.School).WithMany("SchoolAdmins").HasForeignKey(pt => pt.SchoolId).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<District>().HasIndex(ds => ds.Domain).IsUnique();

            modelBuilder.Entity<Class>().HasOne(ds => ds.Term).WithMany("Classes").HasForeignKey(cl => cl.GradingTerm ).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Class>().HasOne(ds => ds.School).WithMany("Classes").HasForeignKey(cl => cl.SchoolId).OnDelete(DeleteBehavior.Restrict);

            
        }

        private void SeedDistrictAndSchool(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<District>().HasData(new District() { DistrictId = 1, DistrictName = "Test District", LogoUrl = "", Domain = "district1" });

            modelBuilder.Entity<District>().HasData(new District() { DistrictId = 2, DistrictName = "Test District", LogoUrl = "", Domain = "district2" });

            modelBuilder.Entity<School>().HasData(new School() { SchoolId = 1, SchoolName = "Test School", DistrictId = 1, CurrentGradingTerm = 2, LogoUrl = "", IconUrl = "" });

            modelBuilder.Entity<School>().HasData(new School() { SchoolId = 2, SchoolName = "Test School 2", DistrictId = 2, CurrentGradingTerm = 2, LogoUrl = "", IconUrl = "" });

            modelBuilder.Entity<Faculty>().HasData(new Faculty() { FacultyId = 5, ExternalId = "Test External Teacher" });

            modelBuilder.Entity<Faculty>().HasData(new Faculty() { FacultyId = 6, ExternalId = "Test External Teacher 2" });

            modelBuilder.Entity<SchoolFaculty>().HasData(new SchoolFaculty() { FacultyId = 5, SchoolId = 1 });
            modelBuilder.Entity<SchoolFaculty>().HasData(new SchoolFaculty() { FacultyId = 6, SchoolId = 2 });
        }

        private void SeedGradingTerms(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GradingTerm>().HasData(new GradingTerm() { GradingTermId = 1, SchoolId = 1, Name = "Spring 2019", StartDate = new DateTime(2019, 1, 1, 0, 0, 0, DateTimeKind.Utc), EndDate = new DateTime(2019, 3, 31, 0, 0, 0, DateTimeKind.Utc) });
            modelBuilder.Entity<GradingTerm>().HasData(new GradingTerm() { GradingTermId = 2, SchoolId = 1, Name = "Summer 2019", StartDate = new DateTime(2019, 4, 1, 0, 0, 0, DateTimeKind.Utc), EndDate = new DateTime(2019, 6, 30, 0, 0, 0, DateTimeKind.Utc) });
            modelBuilder.Entity<GradingTerm>().HasData(new GradingTerm() { GradingTermId = 3, SchoolId = 1, Name = "Fall 2019", StartDate = new DateTime(2019, 7, 1, 0, 0, 0, DateTimeKind.Utc), EndDate = new DateTime(2019, 9, 30, 0, 0, 0, DateTimeKind.Utc) });
            modelBuilder.Entity<GradingTerm>().HasData(new GradingTerm() { GradingTermId = 4, SchoolId = 1, Name = "Winter 2019", StartDate = new DateTime(2019, 10, 1, 0, 0, 0, DateTimeKind.Utc), EndDate = new DateTime(2019, 12, 31, 0, 0, 0, DateTimeKind.Utc) });
        }

        private void SeedClass(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Class>().HasData(new Class() { ClassId = 1, ClassName = "Class 1", Period = "1", GradingTerm = 1, SchoolId = 1, Description = "No Desc", UseDefaultGradesRange = true });
            modelBuilder.Entity<Class>().HasData(new Class() { ClassId = 2, ClassName = "Class 2", Period = "2", GradingTerm = 2, SchoolId = 1, Description = "No Desc", UseDefaultGradesRange = true });
            modelBuilder.Entity<Class>().HasData(new Class() { ClassId = 3, ClassName = "Class 3", Period = "3", GradingTerm = 3, SchoolId = 1, Description = "Desc", UseDefaultGradesRange = true });

            modelBuilder.Entity<ClassFaculty>().HasData(new ClassFaculty() { ClassId = 1, FacultyId = 5 });
            modelBuilder.Entity<ClassFaculty>().HasData(new ClassFaculty() { ClassId = 2, FacultyId = 5 });
            modelBuilder.Entity<ClassFaculty>().HasData(new ClassFaculty() { ClassId = 3, FacultyId = 5 });

            modelBuilder.Entity<Activity>().HasData(new Activity() { ActivityId = 1, ClassId = 1, CreatedBy = 4, Color= "#FFFFFF", IsGraded = false, UpdatedBy = 4, Title = "Activity 1", DateAssigned = new DateTime(2019, 3, 20), DateCreated = new DateTime(2019, 3, 19), DateDue = new DateTime(2019, 3, 31), DateUpdated = new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), Description = "Description activity", MaxScore = 100, Weight = 1 });
            modelBuilder.Entity<Activity>().HasData(new Activity() { ActivityId = 2, ClassId = 1, CreatedBy = 4, Color = "#FFFFFF", IsGraded = false, UpdatedBy = 4, Title = "Activity 2", DateAssigned = new DateTime(2019, 3, 21), DateCreated = new DateTime(2019, 3, 20), DateDue = new DateTime(2019, 3, 26), DateUpdated = new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), Description = "Description activity", MaxScore = 100, Weight = 2 });
            modelBuilder.Entity<Activity>().HasData(new Activity() { ActivityId = 3, ClassId = 2, CreatedBy = 4, Color = "#FFFFFF", IsGraded = true, UpdatedBy = 4, Title = "Activity 2", DateAssigned = new DateTime(2019, 3, 20), DateCreated = new DateTime(2019, 3, 19), DateDue = new DateTime(2019, 3, 29), DateUpdated = new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), Description = "Description activity", MaxScore = 10, Weight = 1 });
            
            modelBuilder.Entity<ActivityCategory>().HasData(new ActivityCategory() { ActivityCategoryId  = 1, FacultyId = 5, Name = "Daily homework", Color= "#e6b8af", IsGraded = true, MaxScore = 10, Weight = 1 });
            modelBuilder.Entity<ActivityCategory>().HasData(new ActivityCategory() { ActivityCategoryId  = 2, FacultyId = 5, Name = "Reading Assignment", Color= "#f4cccc", IsGraded = false, MaxScore = 10, Weight = 1 });
            modelBuilder.Entity<ActivityCategory>().HasData(new ActivityCategory() { ActivityCategoryId  = 3, FacultyId = 5, Name = "Big Project", Color= "#fce5cd", IsGraded = true, MaxScore = 100, Weight = 1 });
        }

        private void SeedStudent(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 3, ExternalId = "0001", StudentNumber = "0001" });
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 8, ExternalId = "0002", StudentNumber = "0002" });
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 9, ExternalId = "0003", StudentNumber = "0003" });
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 10, ExternalId = "0004", StudentNumber = "0004" });
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 11, ExternalId = "0005", StudentNumber = "0005" });
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 12, ExternalId = "0006", StudentNumber = "0006" });
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 13, ExternalId = "0007", StudentNumber = "0007" });
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 14, ExternalId = "0008", StudentNumber = "0008" });
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 15, ExternalId = "0009", StudentNumber = "0009" });
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 16, ExternalId = "0010", StudentNumber = "0010" });
            modelBuilder.Entity<Student>().HasData(new Student() { StudentId = 17, ExternalId = "0011", StudentNumber = "0011" });

            modelBuilder.Entity<Parent>().HasData(new Parent() { ParentId = 4 });
            modelBuilder.Entity<ParentStudent>().HasData(new ParentStudent() { StudentId = 3, ParentId = 4 });
            modelBuilder.Entity<ParentStudent>().HasData(new ParentStudent() { StudentId = 8, ParentId = 4 });

            modelBuilder.Entity<SchoolStudent>().HasData(new SchoolStudent() { SchoolId = 1, StudentId = 3, GradingPeriod = 1, Grade = 0.5m });
            modelBuilder.Entity<SchoolStudent>().HasData(new SchoolStudent() { SchoolId = 1, StudentId = 8, GradingPeriod = 1, Grade = 0.4m });
            modelBuilder.Entity<SchoolStudent>().HasData(new SchoolStudent() { SchoolId = 1, StudentId = 9, GradingPeriod = 1, Grade = 0.3m });
            modelBuilder.Entity<SchoolStudent>().HasData(new SchoolStudent() { SchoolId = 1, StudentId = 10, GradingPeriod = 1, Grade = 0.2m });
            modelBuilder.Entity<SchoolStudent>().HasData(new SchoolStudent() { SchoolId = 1, StudentId = 11, GradingPeriod = 1, Grade = 0.1m });
            modelBuilder.Entity<SchoolStudent>().HasData(new SchoolStudent() { SchoolId = 1, StudentId = 12, GradingPeriod = 1, Grade = 0.1m });
            modelBuilder.Entity<SchoolStudent>().HasData(new SchoolStudent() { SchoolId = 1, StudentId = 13, GradingPeriod = 1, Grade = 0.1m });
            modelBuilder.Entity<SchoolStudent>().HasData(new SchoolStudent() { SchoolId = 1, StudentId = 14, GradingPeriod = 1, Grade = 0.1m });
            modelBuilder.Entity<SchoolStudent>().HasData(new SchoolStudent() { SchoolId = 1, StudentId = 15, GradingPeriod = 1, Grade = null });
            modelBuilder.Entity<SchoolStudent>().HasData(new SchoolStudent() { SchoolId = 1, StudentId = 16, GradingPeriod = 1, Grade = 0.1m });

            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 1, StudentId = 3 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 1, StudentId = 8 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 1, StudentId = 9 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 1, StudentId = 10 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 1, StudentId = 11 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 1, StudentId = 12 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 1, StudentId = 13 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 1, StudentId = 14 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 1, StudentId = 15 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 1, StudentId = 16 });

            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 2, StudentId = 3 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 2, StudentId = 9 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 2, StudentId = 11 });

            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 3, StudentId = 3 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 3, StudentId = 8 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 3, StudentId = 9 });
            modelBuilder.Entity<ClassStudent>().HasData(new ClassStudent() { ClassId = 3, StudentId = 10 });

            modelBuilder.Entity<ActivityScore>().HasData(new ActivityScore() { ActivityId = 1, StudentId = 3, Score = 4, SavedDate = new DateTime(2019, 3, 1) });
            modelBuilder.Entity<ActivityScore>().HasData(new ActivityScore() { ActivityId = 1, StudentId = 8, Score = 3, SavedDate = new DateTime(2019, 3, 10) });
            modelBuilder.Entity<ActivityScore>().HasData(new ActivityScore() { ActivityId = 1, StudentId = 9, Score = 0, });
            modelBuilder.Entity<ActivityScore>().HasData(new ActivityScore() { ActivityId = 1, StudentId = 10, Score = null });
            modelBuilder.Entity<ActivityScore>().HasData(new ActivityScore() { ActivityId = 1, StudentId = 11 });

        }

        private void SeedKeyValue(ModelBuilder modelBuilder)
        {
            long index = 1;

            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = KeyValueNames.CommunicationColor, TextValue = 0xFF7D00.ToString("X") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = KeyValueNames.CollaborationColor, TextValue = 0x41C4FF.ToString("X") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = KeyValueNames.CharacterColor, TextValue = 0x3DD94A.ToString("X") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = KeyValueNames.CreativityColor, TextValue = 0x9574EA.ToString("X") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = KeyValueNames.CriticalThinkingColor, TextValue = 0xFCEE21.ToString("X") });

            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = 1, SchoolID = null, KeyName = KeyValueNames.CommunicationColor, TextValue = 0xFF7D00.ToString("X") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = 1, SchoolID = null, KeyName = KeyValueNames.CollaborationColor, TextValue = 0x41C4FF.ToString("X") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = 1, SchoolID = null, KeyName = KeyValueNames.CharacterColor, TextValue = 0x3DD94A.ToString("X") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = 1, SchoolID = null, KeyName = KeyValueNames.CreativityColor, TextValue = 0x9574EA.ToString("X") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = 1, SchoolID = null, KeyName = KeyValueNames.CriticalThinkingColor, TextValue = 0xFCEE21.ToString("X") });

            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = KeyValueNames.GoogleTagManager, TextValue = "GTM-TM4ZXV5" });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = 1, SchoolID = null, KeyName = KeyValueNames.GoogleTagManager, TextValue = "GTM-N3QFKRR" });

            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.APlus), TextValue = E_DefaultGradeRange.APlus.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.A), TextValue = E_DefaultGradeRange.A.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.AMinus), TextValue = E_DefaultGradeRange.AMinus.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.BPlus), TextValue = E_DefaultGradeRange.BPlus.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.B), TextValue = E_DefaultGradeRange.B.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.BMinus), TextValue = E_DefaultGradeRange.BMinus.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.CPlus), TextValue = E_DefaultGradeRange.CPlus.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.C), TextValue = E_DefaultGradeRange.C.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.CMinus), TextValue = E_DefaultGradeRange.CMinus.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.DPlus), TextValue = E_DefaultGradeRange.DPlus.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.D), TextValue = E_DefaultGradeRange.D.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.DMinus), TextValue = E_DefaultGradeRange.DMinus.ToString("D") });
            modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = EnumExtensions.GetDescriptionFromEnumValue(E_DefaultGradeRange.F), TextValue = E_DefaultGradeRange.F.ToString("D") });

            var categoryColors = new[] {
                "#e6b8af",
                "#f4cccc",
                "#fce5cd",
                "#fff2cc",
                "#d9ead3",
                "#d0e0e3",
                "#c9daf8",
                "#cfe2f3",
                "#d9d2e9",
                "#ead1dc" };
            for(var i = 0; i < categoryColors.Length; i++)
            {
                modelBuilder.Entity<KeyValue>().HasData(new KeyValue() { KeyValueID = index++, OrderIndex = (int)index, DistrictID = null, SchoolID = 1, KeyName = KeyValueNames.ActivityCategoryColorPrefix + (i + 1).ToString(), TextValue = categoryColors[i] });
            }
            
        }

        private void SeedLanguage(ModelBuilder modelBuilder)
        {
            var index = 1;
            foreach (var keyValPair in GetLanguageDict)
            {
                modelBuilder.Entity<Localization>().HasData(new Localization { RecordId = index++, Locale = "en-US", Key = keyValPair.Key, Value = keyValPair.Value });
            }

            foreach (var pair in new Dictionary<string, string>
            {
                { "app.classes.addClass"        , "Añadir clase" },
                { "app.classes.editClass"       , "clase de edición" },
                { "app.classes.period"          , "Período" },
                { "app.classes.className"       , "Nombre de la clase" },
                { "app.classes.gradingTerm"     , "Término de calificación" },
                { "app.classes.classDesc"       , "Descripción de la clase" },
                { "app.classes.list.className"  , "Nombre de la clase" },
                { "app.classes.list.period"     , "Período" },
                { "app.classes.list.gradingTerm", "Término de calificación" },
            })
            {
                modelBuilder.Entity<Localization>().HasData(new Localization { RecordId = index++, Locale = "es", Key = pair.Key, Value = pair.Value });
            }

            foreach (var pair in new Dictionary<string, string>
            {
                { "app.classes.addClass"        , "Thêm lớp" },
                { "app.classes.editClass"       , "Sửa lớp" },
                { "app.classes.period"          , "Tiết" },
                { "app.classes.className"       , "Tên lớp" },
                { "app.classes.gradingTerm"     , "Học kỳ" },
                { "app.classes.classDesc"       , "Tổng quan" },
                { "app.classes.list.className"  , "Lớp" },
                { "app.classes.list.period"     , "Tiết" },
                { "app.classes.list.gradingTerm", "Học kỳ" },
            })
            {
                modelBuilder.Entity<Localization>().HasData(new Localization { RecordId = index++, Locale = "vi", Key = pair.Key, Value = pair.Value });
            }


            foreach (var pair in new Dictionary<string, string>
            {
                { "api.login.error.emailnotconfirm",  "El correo electrónico no está confirmado. Por favor revise el correo electrónico para confirmación e intente iniciar sesión nuevamente."},
                { "api.login.error.userislockedout",  "Su cuenta está bloqueada. Póngase en contacto con su administrador."},
                { "api.login.error.userisnotallowed",  "Este usuario ahora tiene permitido iniciar sesión."},
                { "api.login.error.loginrequirestwofactor",  "Se requiere autenticación de dos factores."},
                { "api.login.error.logininvalid",  "Usuario o contraseña invalido. Por favor, compruebe su nombre de usuario y contraseña y vuelva a intentarlo."},
                { "api.student.error.studentnotfound",  "El estudiante no fue encontrado."},
                { "api.student.error.studentnumbernotfound",  "El número de estudiante no fue encontrado."},
                { "api.student.error.studentphonenumbernotfound",  "El número de teléfono del estudiante no fue encontrado."},
            })
            {
                modelBuilder.Entity<Localization>().HasData(new Localization { RecordId = index++, Locale = "es", Key = pair.Key, Value = pair.Value });
            }
        }

        private static Dictionary<string, string> GetLanguageDict => new Dictionary<string, string>
        {
            ["navBar.lang"] = "Languages",
            ["layout.user.link.help"] = "Help",
            ["layout.user.link.privacy"] = "Privacy",
            ["layout.user.link.terms"] = "Terms",
            ["app.home.introduce"] = "introduce",
            ["app.forms.basic.title"] = "Basic form",
            ["app.forms.basic.description"] = "Form pages are used to collect or verify information to users, and basic forms are common in scenarios where there are fewer data items.",
            ["app.analysis.test"] = "Gongzhuan No.{no} shop",
            ["app.analysis.introduce"] = "Introduce",
            ["app.analysis.total-sales"] = "Total Sales",
            ["app.analysis.day-sales"] = "Daily Sales",
            ["app.analysis.visits"] = "Visits",
            ["app.analysis.visits-trend"] = "Visits Trend",
            ["app.analysis.visits-ranking"] = "Visits Ranking",
            ["app.analysis.day-visits"] = "Daily Visits",
            ["app.analysis.week"] = "WoW Change",
            ["app.analysis.day"] = "DoD Change",
            ["app.analysis.payments"] = "Payments",
            ["app.analysis.conversion-rate"] = "Conversion Rate",
            ["app.analysis.operational-effect"] = "Operational Effect",
            ["app.analysis.sales-trend"] = "Stores Sales Trend",
            ["app.analysis.sales-ranking"] = "Sales Ranking",
            ["app.analysis.all-year"] = "All Year",
            ["app.analysis.all-month"] = "All Month",
            ["app.analysis.all-week"] = "All Week",
            ["app.analysis.all-day"] = "All day",
            ["app.analysis.search-users"] = "Search Users",
            ["app.analysis.per-capita-search"] = "Per Capita Search",
            ["app.analysis.online-top-search"] = "Online Top Search",
            ["app.analysis.the-proportion-of-sales"] = "The Proportion Of Sales",
            ["app.analysis.channel.all"] = "ALL",
            ["app.analysis.channel.online"] = "Online",
            ["app.analysis.channel.stores"] = "Stores",
            ["app.analysis.sales"] = "Sales",
            ["app.analysis.traffic"] = "Traffic",
            ["app.analysis.table.rank"] = "Rank",
            ["app.analysis.table.search-keyword"] = "Keyword",
            ["app.analysis.table.users"] = "Users",
            ["app.analysis.table.weekly-range"] = "Weekly Range",
            ["app.exception.back"] = "Back to home",
            ["app.exception.description.403"] = "Sorry, you don't have access to this page",
            ["app.exception.description.404"] = "Sorry, the page you visited does not exist",
            ["app.exception.description.500"] = "Sorry, the server is reporting an error",
            ["form.get-captcha"] = "Get Captcha",
            ["form.captcha.second"] = "sec",
            ["form.optional"] = " (optional) ",
            ["form.submit"] = "Submit",
            ["form.save"] = "Save",
            ["form.firstName.placeholder"] = "First Name",
            ["form.lastName.placeholder"] = "Last Name",
            ["form.email.placeholder"] = "Email",
            ["form.password.placeholder"] = "Password",
            ["form.confirm-password.placeholder"] = "Confirm password",
            ["form.phone-number.placeholder"] = "Phone number",
            ["form.verification-code.placeholder"] = "Verification code",
            ["form.title.label"] = "Title",
            ["form.title.placeholder"] = "Give the target a name",
            ["form.date.label"] = "Start and end date",
            ["form.date.placeholder.start"] = "Start date",
            ["form.date.placeholder.end"] = "End date",
            ["form.goal.label"] = "Goal description",
            ["form.goal.placeholder"] = "Please enter your work goals",
            ["form.standard.label"] = "Metrics",
            ["form.standard.placeholder"] = "Please enter a metric",
            ["form.client.label"] = "Client",
            ["form.client.label.tooltip"] = "Target service object",
            ["form.client.placeholder"] = "Please describe your customer service, internal customers directly @ Name / job number",
            ["form.invites.label"] = "Inviting critics",
            ["form.invites.placeholder"] = "Please direct @ Name / job number, you can invite up to 5 people",
            ["form.weight.label"] = "Weight",
            ["form.weight.placeholder"] = "Please enter weight",
            ["form.public.label"] = "Target disclosure",
            ["form.public.label.help"] = "Customers and invitees are shared by default",
            ["form.public.radio.public"] = "Public",
            ["form.public.radio.partially-public"] = "Partially public",
            ["form.public.radio.private"] = "Private",
            ["form.publicUsers.placeholder"] = "Open to",
            ["form.publicUsers.option.A"] = "Colleague A",
            ["form.publicUsers.option.B"] = "Colleague B",
            ["form.publicUsers.option.C"] = "Colleague C",
            ["component.globalHeader.search"] = "Search",
            ["component.globalHeader.search.example1"] = "Search example 1",
            ["component.globalHeader.search.example2"] = "Search example 2",
            ["component.globalHeader.search.example3"] = "Search example 3",
            ["component.globalHeader.help"] = "Help",
            ["component.globalHeader.notification"] = "Notification",
            ["component.globalHeader.notification.empty"] = "You have viewed all notifications.",
            ["component.globalHeader.message"] = "Message",
            ["component.globalHeader.message.empty"] = "You have viewed all messsages.",
            ["component.globalHeader.event"] = "Event",
            ["component.globalHeader.event.empty"] = "You have viewed all events.",
            ["component.noticeIcon.clear"] = "Clear",
            ["component.noticeIcon.cleared"] = "Cleared",
            ["component.noticeIcon.empty"] = "No notifications",
            ["component.noticeIcon.view-more"] = "View more",
            ["app.login.userName"] = "userName",
            ["app.login.password"] = "password",
            ["app.login.message-invalid-credentials"] = "Invalid username or password（admin/ant.design）",
            ["app.login.message-invalid-verification-code"] = "Invalid verification code",
            ["app.login.tab-login-credentials"] = "Credentials",
            ["app.login.tab-login-mobile"] = "Mobile number",
            ["app.login.remember-me"] = "Remember me",
            ["app.login.forgot-password"] = "Forgot your password?",
            ["app.login.sign-in-with"] = "Sign in with",
            ["app.login.signup"] = "Sign up",
            ["app.login.login"] = "Login",
            ["app.register.register"] = "Register",
            ["app.register.get-verification-code"] = "Get code",
            ["app.register.sign-in"] = "Already have an account?",
            ["app.register-result.msg"] = "Account：registered at {email}",
            ["app.register-result.activation-email"] = "The activation email has been sent to your email address and is valid for 24 hours. Please log in to the email in time and click on the link in the email to activate the account.",
            ["app.register-result.back-home"] = "Back to home",
            ["app.register-result.view-mailbox"] = "View mailbox",
            ["validation.email.required"] = "Please enter your email!",
            ["validation.email.wrong-format"] = "The email address is in the wrong format!",
            ["validation.userName.required"] = "Please enter your userName!",
            ["validation.password.required"] = "Please enter your password!",
            ["validation.password.twice"] = "The passwords entered twice do not match!",
            ["validation.password.strength.msg"] = "Please enter at least 6 characters and don't use passwords that are easy to guess.",
            ["validation.password.strength.strong"] = "Strength] = strong",
            ["validation.password.strength.medium"] = "Strength] = medium",
            ["validation.password.strength.short"] = "Strength] = too short",
            ["validation.confirm-password.required"] = "Please confirm your password!",
            ["validation.phone-number.required"] = "Please enter your phone number!",
            ["validation.phone-number.wrong-format"] = "Malformed phone number!",
            ["validation.verification-code.required"] = "Please enter the verification code!",
            ["validation.title.required"] = "Please enter a title",
            ["validation.date.required"] = "Please select the start and end date",
            ["validation.goal.required"] = "Please enter a description of the goal",
            ["validation.standard.required"] = "Please enter a metric",
            ["menu.home"] = "Home",
            ["menu.login"] = "Login",
            ["menu.register"] = "Register",
            ["menu.register.result"] = "Register Result",
            ["menu.dashboard"] = "Dashboard",
            ["menu.dashboard.analysis"] = "Analysis",
            ["menu.dashboard.monitor"] = "Monitor",
            ["menu.dashboard.workplace"] = "Workplace",
            ["menu.form"] = "Form",
            ["menu.form.basicform"] = "Basic Form",
            ["menu.form.stepform"] = "Step Form",
            ["menu.form.stepform.info"] = "Step Form(write transfer information)",
            ["menu.form.stepform.confirm"] = "Step Form(confirm transfer information)",
            ["menu.form.stepform.result"] = "Step Form(finished)",
            ["menu.form.advancedform"] = "Advanced Form",
            ["menu.list"] = "List",
            ["menu.list.searchtable"] = "Search Table",
            ["menu.list.basiclist"] = "Basic List",
            ["menu.list.cardlist"] = "Card List",
            ["menu.list.searchlist"] = "Search List",
            ["menu.list.searchlist.articles"] = "Search List(articles)",
            ["menu.list.searchlist.projects"] = "Search List(projects)",
            ["menu.list.searchlist.applications"] = "Search List(applications)",
            ["menu.profile"] = "Profile",
            ["menu.profile.basic"] = "Basic Profile",
            ["menu.profile.advanced"] = "Advanced Profile",
            ["menu.result"] = "Result",
            ["menu.result.success"] = "Success",
            ["menu.result.fail"] = "Fail",
            ["menu.exception"] = "Exception",
            ["menu.exception.not-permission"] = "403",
            ["menu.exception.not-find"] = "404",
            ["menu.exception.server-error"] = "500",
            ["menu.exception.trigger"] = "Trigger",
            ["menu.account"] = "Account",
            ["menu.account.center"] = "Account Center",
            ["menu.account.settings"] = "Account Settings",
            ["menu.account.trigger"] = "Trigger Error",
            ["menu.account.logout"] = "Logout",
            ["app.monitor.trading-activity"] = "Real-Time Trading Activity",
            ["app.monitor.total-transactions"] = "Total transactions today",
            ["app.monitor.sales-target"] = "Sales target completion rate",
            ["app.monitor.remaining-time"] = "Remaining time of activity",
            ["app.monitor.total-transactions-per-second"] = "Total transactions per second",
            ["app.monitor.activity-forecast"] = "Activity forecast",
            ["app.monitor.efficiency"] = "Efficiency",
            ["app.monitor.ratio"] = "Ratio",
            ["app.monitor.proportion-per-category"] = "Proportion Per Category",
            ["app.monitor.fast-food"] = "Fast food",
            ["app.monitor.western-food"] = "Western food",
            ["app.monitor.hot-pot"] = "Hot pot",
            ["app.monitor.waiting-for-implementation"] = "Waiting for implementation",
            ["app.monitor.popular-searches"] = "Popular Searches",
            ["app.monitor.resource-surplus"] = "Resource Surplus",
            ["app.monitor.fund-surplus"] = "Fund Surplus",
            ["app.result.error.title"] = "Submission Failed",
            ["app.result.error.description"] = "Please check and modify the following information before resubmitting.",
            ["app.result.error.hint-title"] = "The content you submitted has the following error] =",
            ["app.result.error.hint-text1"] = "Your account has been frozen",
            ["app.result.error.hint-btn1"] = "Thaw immediately",
            ["app.result.error.hint-text2"] = "Your account is not yet eligible to apply",
            ["app.result.error.hint-btn2"] = "Upgrade immediately",
            ["app.result.error.btn-text"] = "Return to modify",
            ["app.result.success.title"] = "Submission Success",
            ["app.result.success.description"] = "The submission results page is used to feed back the results of a series of operational tasks. If it is a simple operation, use the Message global prompt feedback. This text area can show a simple supplementary explanation. If there is a similar requirement for displaying “documents”, the following gray area can present more complicated content.",
            ["app.result.success.operate-title"] = "Project Name",
            ["app.result.success.operate-id"] = "Project ID：",
            ["app.result.success.principal"] = "Principal：",
            ["app.result.success.operate-time"] = "Effective time：",
            ["app.result.success.step1-title"] = "Create project",
            ["app.result.success.step1-operator"] = "Qu Lili",
            ["app.result.success.step2-title"] = "Departmental preliminary review",
            ["app.result.success.step2-operator"] = "Zhou Maomao",
            ["app.result.success.step2-extra"] = "Urge",
            ["app.result.success.step3-title"] = "Financial review",
            ["app.result.success.step4-title"] = "Finish",
            ["app.result.success.btn-return"] = "Back to list",
            ["app.result.success.btn-project"] = "View project",
            ["app.result.success.btn-print"] = "Print",
            ["app.setting.pagestyle"] = "Page style setting",
            ["app.setting.pagestyle.dark"] = "Dark style",
            ["app.setting.pagestyle.light"] = "Light style",
            ["app.setting.content-width"] = "Content Width",
            ["app.setting.content-width.fixed"] = "Fixed",
            ["app.setting.content-width.fluid"] = "Fluid",
            ["app.setting.themecolor"] = "Theme Color",
            ["app.setting.themecolor.dust"] = "Dust Red",
            ["app.setting.themecolor.volcano"] = "Volcano",
            ["app.setting.themecolor.sunset"] = "Sunset Orange",
            ["app.setting.themecolor.cyan"] = "Cyan",
            ["app.setting.themecolor.green"] = "Polar Green",
            ["app.setting.themecolor.daybreak"] = "Daybreak Blue (default)",
            ["app.setting.themecolor.geekblue"] = "Geek Glue",
            ["app.setting.themecolor.purple"] = "Golden Purple",
            ["app.setting.navigationmode"] = "Navigation Mode",
            ["app.setting.sidemenu"] = "Side Menu Layout",
            ["app.setting.topmenu"] = "Top Menu Layout",
            ["app.setting.fixedheader"] = "Fixed Header",
            ["app.setting.fixedsidebar"] = "Fixed Sidebar",
            ["app.setting.fixedsidebar.hint"] = "Works on Side Menu Layout",
            ["app.setting.hideheader"] = "Hidden Header when scrolling",
            ["app.setting.hideheader.hint"] = "Works when Hidden Header is enabled",
            ["app.setting.othersettings"] = "Other Settings",
            ["app.setting.weakmode"] = "Weak Mode",
            ["app.setting.copy"] = "Copy Setting",
            ["app.setting.copyinfo"] = "copy success，please replace defaultSettings in src/models/setting.js",
            ["app.setting.production.hint"] = "Setting panel shows in development environment only, please manually modify",
            ["app.settings.menuMap.basic"] = "Basic Settings",
            ["app.settings.menuMap.security"] = "Security Settings",
            ["app.settings.menuMap.binding"] = "Account Binding",
            ["app.settings.menuMap.notification"] = "New Message Notification",
            ["app.settings.basic.avatar"] = "Avatar",
            ["app.settings.basic.change-avatar"] = "Change avatar",
            ["app.settings.basic.email"] = "Email",
            ["app.settings.basic.email-message"] = "Please input your email!",
            ["app.settings.basic.nickname"] = "Nickname",
            ["app.settings.basic.nickname-message"] = "Please input your Nickname!",
            ["app.settings.basic.profile"] = "Personal profile",
            ["app.settings.basic.profile-message"] = "Please input your personal profile!",
            ["app.settings.basic.profile-placeholder"] = "Brief introduction to yourself",
            ["app.settings.basic.country"] = "Country/Region",
            ["app.settings.basic.country-message"] = "Please input your country!",
            ["app.settings.basic.geographic"] = "Province or city",
            ["app.settings.basic.geographic-message"] = "Please input your geographic info!",
            ["app.settings.basic.address"] = "Street Address",
            ["app.settings.basic.address-message"] = "Please input your address!",
            ["app.settings.basic.phone"] = "Phone Number",
            ["app.settings.basic.phone-message"] = "Please input your phone!",
            ["app.settings.basic.update"] = "Update Information",
            ["app.settings.security.strong"] = "Strong",
            ["app.settings.security.medium"] = "Medium",
            ["app.settings.security.weak"] = "Weak",
            ["app.settings.security.password"] = "Account Password",
            ["app.settings.security.password-description"] = "Current password strength",
            ["app.settings.security.phone"] = "Security Phone",
            ["app.settings.security.phone-description"] = "Bound phone",
            ["app.settings.security.question"] = "Security Question",
            ["app.settings.security.question-description"] = "The security question is not set, and the security policy can effectively protect the account security",
            ["app.settings.security.email"] = "Backup Email",
            ["app.settings.security.email-description"] = "Bound Email",
            ["app.settings.security.mfa"] = "MFA Device",
            ["app.settings.security.mfa-description"] = "Unbound MFA device, after binding, can be confirmed twice",
            ["app.settings.security.modify"] = "Modify",
            ["app.settings.security.set"] = "Set",
            ["app.settings.security.bind"] = "Bind",
            ["app.settings.binding.taobao"] = "Binding Taobao",
            ["app.settings.binding.taobao-description"] = "Currently unbound Taobao account",
            ["app.settings.binding.alipay"] = "Binding Alipay",
            ["app.settings.binding.alipay-description"] = "Currently unbound Alipay account",
            ["app.settings.binding.dingding"] = "Binding DingTalk",
            ["app.settings.binding.dingding-description"] = "Currently unbound DingTalk account",
            ["app.settings.binding.bind"] = "Bind",
            ["app.settings.notification.password"] = "Account Password",
            ["app.settings.notification.password-description"] = "Messages from other users will be notified in the form of a station letter",
            ["app.settings.notification.messages"] = "System Messages",
            ["app.settings.notification.messages-description"] = "System messages will be notified in the form of a station letter",
            ["app.settings.notification.todo"] = "To-do Notification",
            ["app.settings.notification.todo-description"] = "The to-do list will be notified in the form of a letter from the station",
            ["app.settings.open"] = "Open",
            ["app.settings.close"] = "Close",
            ["app.pwa.offline"] = "You are offline now",
            ["app.pwa.serviceworker.updated"] = "New content is available",
            ["app.pwa.serviceworker.updated.hint"] = "Please press the \"Refresh\" button to reload current page",
            ["app.pwa.serviceworker.updated.ok"] = "Refresh",
            ["component.tagSelect.expand"] = "Expand",
            ["component.tagSelect.collapse"] = "Collapse",
            ["component.tagSelect.all"] = "All"
        };
    }
}
