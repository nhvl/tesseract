﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using Tesseract.Core.Entities.Config;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Data
{
    public partial class MainDbContext : TesseractIdentityDbContext
    {
        private readonly ILogger _logger;

        public MainDbContext(DbContextOptions<MainDbContext> options, ILoggerFactory loggerFactory) : base(options)
        {
            _logger = loggerFactory.CreateLogger<MainDbContext>();
        }

        public DbSet<District> Districts { get; set; }
        public DbSet<GradingTerm> GradingTerms { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<SchoolAdmin> SchoolAdmins { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<ActivityCategory> ActivityCategories { get; set; }
        public DbSet<ActivityScore> ActivityScores { get; set; }
        public DbSet<SchoolFaculty> SchoolFaculties { get; set; }
        public DbSet<SchoolStudent> SchoolStudents { get; set; }
        public DbSet<ParentStudent> ParentStudents { get; set; }
        public DbSet<ClassFaculty> ClassFaculties { get; set; }
        public DbSet<ClassStudent> ClassStudents { get; set; }
        public DbSet<Localization> Localizations { get; set; }
        public DbSet<KeyValue> KeyValues { get; set; }
        public DbSet<ChangeScript> ChangeScripts { get; set; }
        public DbSet<EmailAccount> EmailAccounts { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<LoginLog> LoginLogs { get; set; }
        public DbSet<Poll> Polls { get; set; }
        public DbSet<Discussion> Discussions { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<CommentSeen> CommentSeens { get; set; }
        public DbSet<UserNotificationSetting> UserNotificationSettings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Ignore<District>();
            //modelBuilder.Ignore<GradingTerm>();
            //modelBuilder.Ignore<School>();
            //modelBuilder.Ignore<SchoolAdmin>();
            //modelBuilder.Ignore<Class>();
            //modelBuilder.Ignore<Student>();
            //modelBuilder.Ignore<Parent>();
            //modelBuilder.Ignore<Faculty>();
            //modelBuilder.Ignore<Activity>();
            //modelBuilder.Ignore<ActivityCategory>();
            //modelBuilder.Ignore<ActivityScore>();
            //modelBuilder.Ignore<SchoolFaculty>();
            //modelBuilder.Ignore<SchoolStudent>();
            //modelBuilder.Ignore<ParentStudent>();
            //modelBuilder.Ignore<ClassFaculty>();
            //modelBuilder.Ignore<ClassStudent>();
            //modelBuilder.Ignore<Localization>();
            //modelBuilder.Ignore<KeyValue>();
            //modelBuilder.Ignore<ChangeScript>();
            //modelBuilder.Ignore<EmailAccount>();
            //modelBuilder.Ignore<EmailTemplate>();
            //modelBuilder.Ignore<LoginLog>();
            //modelBuilder.Ignore<Poll>();
            //modelBuilder.Ignore<Discussion>();
            //modelBuilder.Ignore<Comment>();
            //modelBuilder.Ignore<CommentSeen>();
            //modelBuilder.Ignore<UserNotificationSetting>();

            // filter soft deleted entities
            modelBuilder.Entity<District>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<School>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<GradingTerm>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<Class>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<Student>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<Parent>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<Faculty>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<Activity>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<ActivityCategory>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<Poll>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<Discussion>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<Comment>().HasQueryFilter(e => !e.IsDeleted);

            modelBuilder.Entity<Discussion>().HasIndex(e => e.ThreadId).IsUnique();
            modelBuilder.Entity<Comment>().HasIndex(e => e.ThreadId);

            modelBuilder.Entity<CommentSeen>().HasKey(e => new { e.ThreadId, e.UserId });

            modelBuilder.Entity<UserNotificationSetting>().HasKey(e => new { e.UserId, e.Type });
            modelBuilder.Entity<UserNotificationSetting>().HasOne(e => e.User);

            Func<string, GradeRange[]> DeserelizeGradeRange = (json) =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<GradeRange[]>(json);
                }
                catch (JsonSerializationException ex)
                {
                    _logger.LogError(ex, json);
                }

                return null;
            };

            modelBuilder.Entity<Class>().Property(x => x.GradeRanges)
                                        .HasColumnName("GradeRangesJson")
                                        .HasConversion(
                                            ranges => JsonConvert.SerializeObject(ranges),
                                            json => DeserelizeGradeRange(json)
                                        );

            base.OnModelCreating(modelBuilder);
            this.MakeForeignKeys(modelBuilder);

            this.SeedTestingData(modelBuilder);
        }
    }
}
