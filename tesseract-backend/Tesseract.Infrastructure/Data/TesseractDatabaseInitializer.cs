﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesseract.Common.Extensions;
using Tesseract.Core.Entities.Identity;

namespace Tesseract.Infrastructure.Data
{
    public class TesseractDatabaseInitializer
    {
        private struct SeedAccount
        {
            public string Email { get; set; }
            public string Password { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Role { get; set; }
            public long District { get; set; }

            public SeedAccount(string email, string password, string firstName, string lastName, string role, long district)
            {
                Email = email;
                Password = password;
                FirstName = firstName;
                LastName = lastName;
                Role = role;
                District = district;
            }

            public ApplicationUser ToApplicantUser() => new ApplicationUser
            {
                FirstName = this.FirstName,
                LastName = this.LastName,
                UserName = this.Email,
                Email = this.Email,
                DistrictId = this.District
            };
        }

        public static async Task SeedUsers(TesseractUserManager userManager, ILogger logger)
        {
            const string password = "Asd123!!";
            const long defaultDistrictId = 1;
            var accounts = new SeedAccount[]
            {
                new SeedAccount("admin@tesseract.com", password, "Super", "Admin", "Admin", defaultDistrictId),
                new SeedAccount("admin2@tesseract.com", password, "Super", "Admin", "Admin", 2),
                new SeedAccount("student@tesseract.com", password, "Test", "Student", "Student", defaultDistrictId),
                new SeedAccount("parent@tesseract.com", password, "Test", "Parent", "Parent", defaultDistrictId),
                new SeedAccount("faculty@tesseract.com", password, "Test", "Faculty", "Faculty", defaultDistrictId),
                new SeedAccount("faculty2@tesseract.com", password, "Test", "Faculty", "Faculty", 2),
                new SeedAccount("schooladmin@tesseract.com", password, "School", "Admin", "SchoolAdmin", defaultDistrictId),
            }.ToList();

            accounts.InsertRange(7, Enumerable.Range(1, 10).Select(i => new SeedAccount($"student{i}@tesseract.com", password, "Test", $"Student{i}", "Student", defaultDistrictId)));
            await accounts.ForEachAsync(account => CreateAccount(account, userManager, logger));
        }

        private static async Task CreateAccount(SeedAccount account, TesseractUserManager userManager, ILogger logger)
        {
            try
            {
                if (await userManager.FindByEmailAsync(account.Email) != null) return;

                var user = account.ToApplicantUser();
                if ((await userManager.CreateAsync(user, account.Password)).Succeeded)
                {
                    await userManager.AddToSchoolRoleAsync(user, null, account.Role);
                }
            }
            catch (Exception ex)
            {
                logger.LogCritical(new EventId(), ex, ex.Message);
            }
        } 

        public static async Task SeedRoles(RoleManager<ApplicationRole> roleManager, ILogger logger)
        {
            var roleDict = new Dictionary<string, ApplicationRole>();

            foreach (var roleItem in ApplicationRoleValue.All())
            {
                if (!await roleManager.RoleExistsAsync(roleItem.Name))
                {
                    await roleManager.CreateAsync(new ApplicationRole(roleItem.Name, roleItem.IsSystemRole));
                }

                roleDict[roleItem.Name] = await roleManager.FindByNameAsync(roleItem.Name);
            }

            await roleManager.AddClaimIfNotExistsAsync(roleDict[ApplicationRoleValue.Admin], Permission.CanEditClass.ToClaim());
            await roleManager.AddClaimIfNotExistsAsync(roleDict[ApplicationRoleValue.Admin], Permission.CanEditSchoolRole.ToClaim());
            await roleManager.AddClaimIfNotExistsAsync(roleDict[ApplicationRoleValue.Admin], Permission.CanAddStudent.ToClaim());
            await roleManager.AddClaimIfNotExistsAsync(roleDict[ApplicationRoleValue.Faculty], Permission.CanEditClass.ToClaim());
            await roleManager.AddClaimIfNotExistsAsync(roleDict[ApplicationRoleValue.StudentManager], Permission.CanAddStudent.ToClaim());
        }
    }
}