﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tesseract.Infrastructure.Migrations
{
    public partial class Activity_Add_Weigth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsExclude",
                table: "ActivityScores",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsCredit",
                table: "Activities",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsExclude",
                table: "ActivityScores");

            migrationBuilder.DropColumn(
                name: "IsCredit",
                table: "Activities");
        }
    }
}
