﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Tesseract.Infrastructure.Migrations
{
    public partial class Seed_Testing_Data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "ActivityCategories",
                columns: new[] { "ActivityCategoryId", "Color", "CreatedBy", "DateCreated", "DateUpdated", "FacultyId", "IsDeleted", "IsGraded", "MaxScore", "Name", "UpdatedBy", "Weight" },
                values: new object[,]
                {
                    { 3L, "#fce5cd", null, null, null, 5L, false, true, 100m, "Big Project", null, 1m },
                    { 1L, "#e6b8af", null, null, null, 5L, false, true, 10m, "Daily homework", null, 1m },
                    { 2L, "#f4cccc", null, null, null, 5L, false, false, 10m, "Reading Assignment", null, 1m }
                });

            migrationBuilder.InsertData(
                table: "Districts",
                columns: new[] { "DistrictId", "CreatedBy", "DateCreated", "DateUpdated", "DistrictName", "Domain", "IsDeleted", "LogoUrl", "UpdatedBy" },
                values: new object[,]
                {
                    { 1L, null, null, null, "Test District", "district1", false, "", null },
                    { 2L, null, null, null, "Test District", "district2", false, "", null }
                });

            migrationBuilder.InsertData(
                table: "EmailAccounts",
                columns: new[] { "EmailAccountId", "CreatedBy", "DateCreated", "DateUpdated", "DisplayName", "Email", "EnableSsl", "Host", "IsDeleted", "Password", "Port", "UpdatedBy", "UseDefaultCredentials", "Username" },
                values: new object[] { 1, 1L, new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), null, "Tesseract", "info@tesseract.com", true, "smtp.sendgrid.net", false, "lenguyen@894047", 587, null, false, "maxkingsouth2016" });

            migrationBuilder.InsertData(
                table: "EmailTemplates",
                columns: new[] { "EmailTemplateId", "BccEmailAddresses", "Body", "CreatedBy", "DateCreated", "DateUpdated", "IsDeleted", "Name", "Subject", "UpdatedBy" },
                values: new object[,]
                {
                    { 1, null, "<p>If you did not request to reset your password for your {{SCHOOL_DISTRICT_NAME}} portal, you may ignore this email.</p><p>To reset your password, <a href='{{link}}'>[Click here]</a> or copy and paste this link into your browser:{{link}}</p>", 1L, new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), null, false, "Account.ForgotPassword", "{{SCHOOL_DISTRICT_NAME}} - Your password reset instructions", null },
                    { 3, null, "<p>{{STUDENT_NAME}}</p><p>{{PARENT_NAME}} ({{PARENT_EMAIL}}) has registered as your parent.</p><p>If this is a mistake, please contact your administrator.</p>", 1L, new DateTime(2019, 6, 12, 0, 0, 0, 0, DateTimeKind.Utc), null, false, "Account.NotifyStudent", "{{PARENT_NAME}} has registered as your parent", null },
                    { 2, null, "<p>Welcome to Tesseract!</p><p>To active your account, <a href='{{link}}'>[Click here]</a> or copy and paste this link into your browser:{{link}}</p>", 1L, new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), null, false, "Account.ConfirmEmail", "Welcome to Tesseract", null }
                });

            migrationBuilder.InsertData(
                table: "Faculty",
                columns: new[] { "FacultyId", "CreatedBy", "DateCreated", "DateUpdated", "ExternalId", "IsDeleted", "UpdatedBy" },
                values: new object[,]
                {
                    { 5L, null, null, null, "Test External Teacher", false, null },
                    { 6L, null, null, null, "Test External Teacher 2", false, null }
                });

            migrationBuilder.InsertData(
                table: "GradingTerms",
                columns: new[] { "GradingTermId", "CreatedBy", "DateCreated", "DateUpdated", "EndDate", "IsDeleted", "Name", "SchoolId", "StartDate", "UpdatedBy" },
                values: new object[,]
                {
                    { 1L, null, null, null, new DateTime(2019, 3, 31, 0, 0, 0, 0, DateTimeKind.Utc), false, "Spring 2019", 1L, new DateTime(2019, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc), null },
                    { 2L, null, null, null, new DateTime(2019, 6, 30, 0, 0, 0, 0, DateTimeKind.Utc), false, "Summer 2019", 1L, new DateTime(2019, 4, 1, 0, 0, 0, 0, DateTimeKind.Utc), null },
                    { 3L, null, null, null, new DateTime(2019, 9, 30, 0, 0, 0, 0, DateTimeKind.Utc), false, "Fall 2019", 1L, new DateTime(2019, 7, 1, 0, 0, 0, 0, DateTimeKind.Utc), null },
                    { 4L, null, null, null, new DateTime(2019, 12, 31, 0, 0, 0, 0, DateTimeKind.Utc), false, "Winter 2019", 1L, new DateTime(2019, 10, 1, 0, 0, 0, 0, DateTimeKind.Utc), null }
                });

            migrationBuilder.InsertData(
                table: "KeyValues",
                columns: new[] { "KeyValueID", "DistrictID", "IsActive", "KeyName", "OrderIndex", "SchoolID", "TextValue" },
                values: new object[,]
                {
                    { 28L, null, false, "ActivityCategory_3", 29, 1L, "#fce5cd" },
                    { 29L, null, false, "ActivityCategory_4", 30, 1L, "#fff2cc" },
                    { 30L, null, false, "ActivityCategory_5", 31, 1L, "#d9ead3" },
                    { 31L, null, false, "ActivityCategory_6", 32, 1L, "#d0e0e3" },
                    { 34L, null, false, "ActivityCategory_9", 35, 1L, "#d9d2e9" },
                    { 33L, null, false, "ActivityCategory_8", 34, 1L, "#cfe2f3" },
                    { 35L, null, false, "ActivityCategory_10", 36, 1L, "#ead1dc" },
                    { 27L, null, false, "ActivityCategory_2", 28, 1L, "#f4cccc" },
                    { 32L, null, false, "ActivityCategory_7", 33, 1L, "#c9daf8" },
                    { 26L, null, false, "ActivityCategory_1", 27, 1L, "#e6b8af" },
                    { 1L, null, false, "CommunicationColor", 2, 1L, "FF7D00" },
                    { 24L, null, false, "D-", 25, 1L, "62" },
                    { 25L, null, false, "F", 26, 1L, "60" },
                    { 2L, null, false, "CollaborationColor", 3, 1L, "41C4FF" },
                    { 3L, null, false, "CharacterColor", 4, 1L, "3DD94A" },
                    { 4L, null, false, "CreativityColor", 5, 1L, "9574EA" },
                    { 6L, 1L, false, "CommunicationColor", 7, null, "FF7D00" },
                    { 7L, 1L, false, "CollaborationColor", 8, null, "41C4FF" },
                    { 8L, 1L, false, "CharacterColor", 9, null, "3DD94A" },
                    { 9L, 1L, false, "CreativityColor", 10, null, "9574EA" },
                    { 10L, 1L, false, "CriticalThinkingColor", 11, null, "FCEE21" },
                    { 11L, null, false, "GoogleTagManager", 12, 1L, "GTM-TM4ZXV5" },
                    { 12L, 1L, false, "GoogleTagManager", 13, null, "GTM-N3QFKRR" },
                    { 5L, null, false, "CriticalThinkingColor", 6, 1L, "FCEE21" },
                    { 14L, null, false, "A", 15, 1L, "96" },
                    { 13L, null, false, "A+", 14, 1L, "100" },
                    { 22L, null, false, "D+", 23, 1L, "69" },
                    { 23L, null, false, "D", 24, 1L, "66" },
                    { 20L, null, false, "C", 21, 1L, "76" },
                    { 19L, null, false, "C+", 20, 1L, "79" },
                    { 21L, null, false, "C-", 22, 1L, "72" },
                    { 17L, null, false, "B", 18, 1L, "86" },
                    { 16L, null, false, "B+", 17, 1L, "89" },
                    { 15L, null, false, "A-", 16, 1L, "92" },
                    { 18L, null, false, "B-", 19, 1L, "82" }
                });

            migrationBuilder.InsertData(
                table: "Localizations",
                columns: new[] { "RecordId", "Key", "Locale", "Value" },
                values: new object[,]
                {
                    { 220L, "app.setting.themecolor.cyan", "en-US", "Cyan" },
                    { 221L, "app.setting.themecolor.green", "en-US", "Polar Green" },
                    { 219L, "app.setting.themecolor.sunset", "en-US", "Sunset Orange" },
                    { 218L, "app.setting.themecolor.volcano", "en-US", "Volcano" },
                    { 217L, "app.setting.themecolor.dust", "en-US", "Dust Red" },
                    { 216L, "app.setting.themecolor", "en-US", "Theme Color" },
                    { 215L, "app.setting.content-width.fluid", "en-US", "Fluid" },
                    { 214L, "app.setting.content-width.fixed", "en-US", "Fixed" },
                    { 210L, "app.setting.pagestyle", "en-US", "Page style setting" },
                    { 212L, "app.setting.pagestyle.light", "en-US", "Light style" },
                    { 211L, "app.setting.pagestyle.dark", "en-US", "Dark style" },
                    { 209L, "app.result.success.btn-print", "en-US", "Print" },
                    { 208L, "app.result.success.btn-project", "en-US", "View project" },
                    { 207L, "app.result.success.btn-return", "en-US", "Back to list" },
                    { 222L, "app.setting.themecolor.daybreak", "en-US", "Daybreak Blue (default)" },
                    { 206L, "app.result.success.step4-title", "en-US", "Finish" },
                    { 213L, "app.setting.content-width", "en-US", "Content Width" },
                    { 223L, "app.setting.themecolor.geekblue", "en-US", "Geek Glue" },
                    { 235L, "app.setting.copy", "en-US", "Copy Setting" },
                    { 225L, "app.setting.navigationmode", "en-US", "Navigation Mode" },
                    { 205L, "app.result.success.step3-title", "en-US", "Financial review" },
                    { 241L, "app.settings.menuMap.notification", "en-US", "New Message Notification" },
                    { 240L, "app.settings.menuMap.binding", "en-US", "Account Binding" },
                    { 239L, "app.settings.menuMap.security", "en-US", "Security Settings" },
                    { 238L, "app.settings.menuMap.basic", "en-US", "Basic Settings" },
                    { 237L, "app.setting.production.hint", "en-US", "Setting panel shows in development environment only, please manually modify" },
                    { 236L, "app.setting.copyinfo", "en-US", "copy success，please replace defaultSettings in src/models/setting.js" },
                    { 224L, "app.setting.themecolor.purple", "en-US", "Golden Purple" },
                    { 234L, "app.setting.weakmode", "en-US", "Weak Mode" },
                    { 232L, "app.setting.hideheader.hint", "en-US", "Works when Hidden Header is enabled" },
                    { 231L, "app.setting.hideheader", "en-US", "Hidden Header when scrolling" },
                    { 230L, "app.setting.fixedsidebar.hint", "en-US", "Works on Side Menu Layout" },
                    { 229L, "app.setting.fixedsidebar", "en-US", "Fixed Sidebar" },
                    { 228L, "app.setting.fixedheader", "en-US", "Fixed Header" },
                    { 227L, "app.setting.topmenu", "en-US", "Top Menu Layout" },
                    { 226L, "app.setting.sidemenu", "en-US", "Side Menu Layout" },
                    { 233L, "app.setting.othersettings", "en-US", "Other Settings" },
                    { 204L, "app.result.success.step2-extra", "en-US", "Urge" },
                    { 179L, "app.monitor.fast-food", "en-US", "Fast food" },
                    { 202L, "app.result.success.step2-title", "en-US", "Departmental preliminary review" },
                    { 180L, "app.monitor.western-food", "en-US", "Western food" },
                    { 242L, "app.settings.basic.avatar", "en-US", "Avatar" },
                    { 178L, "app.monitor.proportion-per-category", "en-US", "Proportion Per Category" },
                    { 177L, "app.monitor.ratio", "en-US", "Ratio" },
                    { 176L, "app.monitor.efficiency", "en-US", "Efficiency" },
                    { 175L, "app.monitor.activity-forecast", "en-US", "Activity forecast" },
                    { 174L, "app.monitor.total-transactions-per-second", "en-US", "Total transactions per second" },
                    { 181L, "app.monitor.hot-pot", "en-US", "Hot pot" },
                    { 173L, "app.monitor.remaining-time", "en-US", "Remaining time of activity" },
                    { 171L, "app.monitor.total-transactions", "en-US", "Total transactions today" },
                    { 170L, "app.monitor.trading-activity", "en-US", "Real-Time Trading Activity" },
                    { 169L, "menu.account.logout", "en-US", "Logout" },
                    { 168L, "menu.account.trigger", "en-US", "Trigger Error" },
                    { 167L, "menu.account.settings", "en-US", "Account Settings" },
                    { 166L, "menu.account.center", "en-US", "Account Center" },
                    { 165L, "menu.account", "en-US", "Account" },
                    { 172L, "app.monitor.sales-target", "en-US", "Sales target completion rate" },
                    { 182L, "app.monitor.waiting-for-implementation", "en-US", "Waiting for implementation" },
                    { 183L, "app.monitor.popular-searches", "en-US", "Popular Searches" },
                    { 184L, "app.monitor.resource-surplus", "en-US", "Resource Surplus" },
                    { 201L, "app.result.success.step1-operator", "en-US", "Qu Lili" },
                    { 200L, "app.result.success.step1-title", "en-US", "Create project" },
                    { 199L, "app.result.success.operate-time", "en-US", "Effective time：" },
                    { 198L, "app.result.success.principal", "en-US", "Principal：" },
                    { 197L, "app.result.success.operate-id", "en-US", "Project ID：" },
                    { 196L, "app.result.success.operate-title", "en-US", "Project Name" },
                    { 195L, "app.result.success.description", "en-US", "The submission results page is used to feed back the results of a series of operational tasks. If it is a simple operation, use the Message global prompt feedback. This text area can show a simple supplementary explanation. If there is a similar requirement for displaying “documents”, the following gray area can present more complicated content." },
                    { 194L, "app.result.success.title", "en-US", "Submission Success" },
                    { 193L, "app.result.error.btn-text", "en-US", "Return to modify" },
                    { 192L, "app.result.error.hint-btn2", "en-US", "Upgrade immediately" },
                    { 191L, "app.result.error.hint-text2", "en-US", "Your account is not yet eligible to apply" },
                    { 190L, "app.result.error.hint-btn1", "en-US", "Thaw immediately" },
                    { 189L, "app.result.error.hint-text1", "en-US", "Your account has been frozen" },
                    { 188L, "app.result.error.hint-title", "en-US", "The content you submitted has the following error] =" },
                    { 187L, "app.result.error.description", "en-US", "Please check and modify the following information before resubmitting." },
                    { 186L, "app.result.error.title", "en-US", "Submission Failed" },
                    { 185L, "app.monitor.fund-surplus", "en-US", "Fund Surplus" },
                    { 203L, "app.result.success.step2-operator", "en-US", "Zhou Maomao" },
                    { 243L, "app.settings.basic.change-avatar", "en-US", "Change avatar" },
                    { 268L, "app.settings.security.question-description", "en-US", "The security question is not set, and the security policy can effectively protect the account security" },
                    { 245L, "app.settings.basic.email-message", "en-US", "Please input your email!" },
                    { 302L, "app.classes.gradingTerm", "es", "Término de calificación" },
                    { 301L, "app.classes.className", "es", "Nombre de la clase" },
                    { 300L, "app.classes.period", "es", "Período" },
                    { 299L, "app.classes.editClass", "es", "clase de edición" },
                    { 298L, "app.classes.addClass", "es", "Añadir clase" },
                    { 297L, "component.tagSelect.all", "en-US", "All" },
                    { 296L, "component.tagSelect.collapse", "en-US", "Collapse" },
                    { 303L, "app.classes.classDesc", "es", "Descripción de la clase" },
                    { 295L, "component.tagSelect.expand", "en-US", "Expand" },
                    { 293L, "app.pwa.serviceworker.updated.hint", "en-US", "Please press the \"Refresh\" button to reload current page" },
                    { 292L, "app.pwa.serviceworker.updated", "en-US", "New content is available" },
                    { 291L, "app.pwa.offline", "en-US", "You are offline now" },
                    { 290L, "app.settings.close", "en-US", "Close" },
                    { 289L, "app.settings.open", "en-US", "Open" },
                    { 288L, "app.settings.notification.todo-description", "en-US", "The to-do list will be notified in the form of a letter from the station" },
                    { 287L, "app.settings.notification.todo", "en-US", "To-do Notification" },
                    { 294L, "app.pwa.serviceworker.updated.ok", "en-US", "Refresh" },
                    { 304L, "app.classes.list.className", "es", "Nombre de la clase" },
                    { 305L, "app.classes.list.period", "es", "Período" },
                    { 306L, "app.classes.list.gradingTerm", "es", "Término de calificación" },
                    { 323L, "api.student.error.studentphonenumbernotfound", "es", "El número de teléfono del estudiante no fue encontrado." },
                    { 322L, "api.student.error.studentnumbernotfound", "es", "El número de estudiante no fue encontrado." },
                    { 321L, "api.student.error.studentnotfound", "es", "El estudiante no fue encontrado." },
                    { 320L, "api.login.error.logininvalid", "es", "Usuario o contraseña invalido. Por favor, compruebe su nombre de usuario y contraseña y vuelva a intentarlo." },
                    { 319L, "api.login.error.loginrequirestwofactor", "es", "Se requiere autenticación de dos factores." },
                    { 318L, "api.login.error.userisnotallowed", "es", "Este usuario ahora tiene permitido iniciar sesión." },
                    { 317L, "api.login.error.userislockedout", "es", "Su cuenta está bloqueada. Póngase en contacto con su administrador." },
                    { 316L, "api.login.error.emailnotconfirm", "es", "El correo electrónico no está confirmado. Por favor revise el correo electrónico para confirmación e intente iniciar sesión nuevamente." },
                    { 315L, "app.classes.list.gradingTerm", "vi", "Học kỳ" },
                    { 314L, "app.classes.list.period", "vi", "Tiết" },
                    { 313L, "app.classes.list.className", "vi", "Lớp" },
                    { 312L, "app.classes.classDesc", "vi", "Tổng quan" },
                    { 311L, "app.classes.gradingTerm", "vi", "Học kỳ" },
                    { 310L, "app.classes.className", "vi", "Tên lớp" },
                    { 309L, "app.classes.period", "vi", "Tiết" },
                    { 308L, "app.classes.editClass", "vi", "Sửa lớp" },
                    { 307L, "app.classes.addClass", "vi", "Thêm lớp" },
                    { 286L, "app.settings.notification.messages-description", "en-US", "System messages will be notified in the form of a station letter" },
                    { 244L, "app.settings.basic.email", "en-US", "Email" },
                    { 285L, "app.settings.notification.messages", "en-US", "System Messages" },
                    { 283L, "app.settings.notification.password", "en-US", "Account Password" },
                    { 261L, "app.settings.security.medium", "en-US", "Medium" },
                    { 260L, "app.settings.security.strong", "en-US", "Strong" },
                    { 259L, "app.settings.basic.update", "en-US", "Update Information" },
                    { 258L, "app.settings.basic.phone-message", "en-US", "Please input your phone!" },
                    { 257L, "app.settings.basic.phone", "en-US", "Phone Number" },
                    { 256L, "app.settings.basic.address-message", "en-US", "Please input your address!" },
                    { 255L, "app.settings.basic.address", "en-US", "Street Address" },
                    { 262L, "app.settings.security.weak", "en-US", "Weak" },
                    { 254L, "app.settings.basic.geographic-message", "en-US", "Please input your geographic info!" },
                    { 252L, "app.settings.basic.country-message", "en-US", "Please input your country!" },
                    { 251L, "app.settings.basic.country", "en-US", "Country/Region" },
                    { 250L, "app.settings.basic.profile-placeholder", "en-US", "Brief introduction to yourself" },
                    { 249L, "app.settings.basic.profile-message", "en-US", "Please input your personal profile!" },
                    { 248L, "app.settings.basic.profile", "en-US", "Personal profile" },
                    { 247L, "app.settings.basic.nickname-message", "en-US", "Please input your Nickname!" },
                    { 246L, "app.settings.basic.nickname", "en-US", "Nickname" },
                    { 253L, "app.settings.basic.geographic", "en-US", "Province or city" },
                    { 263L, "app.settings.security.password", "en-US", "Account Password" },
                    { 264L, "app.settings.security.password-description", "en-US", "Current password strength" },
                    { 265L, "app.settings.security.phone", "en-US", "Security Phone" },
                    { 282L, "app.settings.binding.bind", "en-US", "Bind" },
                    { 281L, "app.settings.binding.dingding-description", "en-US", "Currently unbound DingTalk account" },
                    { 280L, "app.settings.binding.dingding", "en-US", "Binding DingTalk" },
                    { 279L, "app.settings.binding.alipay-description", "en-US", "Currently unbound Alipay account" },
                    { 278L, "app.settings.binding.alipay", "en-US", "Binding Alipay" },
                    { 277L, "app.settings.binding.taobao-description", "en-US", "Currently unbound Taobao account" },
                    { 276L, "app.settings.binding.taobao", "en-US", "Binding Taobao" },
                    { 275L, "app.settings.security.bind", "en-US", "Bind" },
                    { 274L, "app.settings.security.set", "en-US", "Set" },
                    { 273L, "app.settings.security.modify", "en-US", "Modify" },
                    { 272L, "app.settings.security.mfa-description", "en-US", "Unbound MFA device, after binding, can be confirmed twice" },
                    { 271L, "app.settings.security.mfa", "en-US", "MFA Device" },
                    { 270L, "app.settings.security.email-description", "en-US", "Bound Email" },
                    { 269L, "app.settings.security.email", "en-US", "Backup Email" },
                    { 164L, "menu.exception.trigger", "en-US", "Trigger" },
                    { 267L, "app.settings.security.question", "en-US", "Security Question" },
                    { 266L, "app.settings.security.phone-description", "en-US", "Bound phone" },
                    { 284L, "app.settings.notification.password-description", "en-US", "Messages from other users will be notified in the form of a station letter" },
                    { 163L, "menu.exception.server-error", "en-US", "500" },
                    { 144L, "menu.form.stepform.result", "en-US", "Step Form(finished)" },
                    { 161L, "menu.exception.not-permission", "en-US", "403" },
                    { 57L, "form.title.placeholder", "en-US", "Give the target a name" },
                    { 56L, "form.title.label", "en-US", "Title" },
                    { 55L, "form.verification-code.placeholder", "en-US", "Verification code" },
                    { 54L, "form.phone-number.placeholder", "en-US", "Phone number" },
                    { 53L, "form.confirm-password.placeholder", "en-US", "Confirm password" },
                    { 52L, "form.password.placeholder", "en-US", "Password" },
                    { 51L, "form.email.placeholder", "en-US", "Email" },
                    { 58L, "form.date.label", "en-US", "Start and end date" },
                    { 50L, "form.lastName.placeholder", "en-US", "Last Name" },
                    { 48L, "form.save", "en-US", "Save" },
                    { 47L, "form.submit", "en-US", "Submit" },
                    { 46L, "form.optional", "en-US", " (optional) " },
                    { 45L, "form.captcha.second", "en-US", "sec" },
                    { 44L, "form.get-captcha", "en-US", "Get Captcha" },
                    { 43L, "app.exception.description.500", "en-US", "Sorry, the server is reporting an error" },
                    { 42L, "app.exception.description.404", "en-US", "Sorry, the page you visited does not exist" },
                    { 49L, "form.firstName.placeholder", "en-US", "First Name" },
                    { 59L, "form.date.placeholder.start", "en-US", "Start date" },
                    { 60L, "form.date.placeholder.end", "en-US", "End date" },
                    { 61L, "form.goal.label", "en-US", "Goal description" },
                    { 78L, "form.publicUsers.option.A", "en-US", "Colleague A" },
                    { 77L, "form.publicUsers.placeholder", "en-US", "Open to" },
                    { 76L, "form.public.radio.private", "en-US", "Private" },
                    { 75L, "form.public.radio.partially-public", "en-US", "Partially public" },
                    { 162L, "menu.exception.not-find", "en-US", "404" },
                    { 73L, "form.public.label.help", "en-US", "Customers and invitees are shared by default" },
                    { 72L, "form.public.label", "en-US", "Target disclosure" },
                    { 71L, "form.weight.placeholder", "en-US", "Please enter weight" },
                    { 70L, "form.weight.label", "en-US", "Weight" },
                    { 69L, "form.invites.placeholder", "en-US", "Please direct @ Name / job number, you can invite up to 5 people" },
                    { 68L, "form.invites.label", "en-US", "Inviting critics" },
                    { 67L, "form.client.placeholder", "en-US", "Please describe your customer service, internal customers directly @ Name / job number" },
                    { 66L, "form.client.label.tooltip", "en-US", "Target service object" },
                    { 65L, "form.client.label", "en-US", "Client" },
                    { 64L, "form.standard.placeholder", "en-US", "Please enter a metric" },
                    { 63L, "form.standard.label", "en-US", "Metrics" },
                    { 62L, "form.goal.placeholder", "en-US", "Please enter your work goals" },
                    { 41L, "app.exception.description.403", "en-US", "Sorry, you don't have access to this page" },
                    { 79L, "form.publicUsers.option.B", "en-US", "Colleague B" },
                    { 40L, "app.exception.back", "en-US", "Back to home" },
                    { 38L, "app.analysis.table.users", "en-US", "Users" },
                    { 16L, "app.analysis.week", "en-US", "WoW Change" },
                    { 15L, "app.analysis.day-visits", "en-US", "Daily Visits" },
                    { 14L, "app.analysis.visits-ranking", "en-US", "Visits Ranking" },
                    { 13L, "app.analysis.visits-trend", "en-US", "Visits Trend" },
                    { 12L, "app.analysis.visits", "en-US", "Visits" },
                    { 11L, "app.analysis.day-sales", "en-US", "Daily Sales" },
                    { 10L, "app.analysis.total-sales", "en-US", "Total Sales" },
                    { 17L, "app.analysis.day", "en-US", "DoD Change" },
                    { 9L, "app.analysis.introduce", "en-US", "Introduce" },
                    { 7L, "app.forms.basic.description", "en-US", "Form pages are used to collect or verify information to users, and basic forms are common in scenarios where there are fewer data items." },
                    { 6L, "app.forms.basic.title", "en-US", "Basic form" },
                    { 5L, "app.home.introduce", "en-US", "introduce" },
                    { 4L, "layout.user.link.terms", "en-US", "Terms" },
                    { 3L, "layout.user.link.privacy", "en-US", "Privacy" },
                    { 2L, "layout.user.link.help", "en-US", "Help" },
                    { 1L, "navBar.lang", "en-US", "Languages" },
                    { 8L, "app.analysis.test", "en-US", "Gongzhuan No.{no} shop" },
                    { 18L, "app.analysis.payments", "en-US", "Payments" },
                    { 19L, "app.analysis.conversion-rate", "en-US", "Conversion Rate" },
                    { 20L, "app.analysis.operational-effect", "en-US", "Operational Effect" },
                    { 37L, "app.analysis.table.search-keyword", "en-US", "Keyword" },
                    { 36L, "app.analysis.table.rank", "en-US", "Rank" },
                    { 35L, "app.analysis.traffic", "en-US", "Traffic" },
                    { 34L, "app.analysis.sales", "en-US", "Sales" },
                    { 33L, "app.analysis.channel.stores", "en-US", "Stores" },
                    { 32L, "app.analysis.channel.online", "en-US", "Online" },
                    { 31L, "app.analysis.channel.all", "en-US", "ALL" },
                    { 30L, "app.analysis.the-proportion-of-sales", "en-US", "The Proportion Of Sales" },
                    { 29L, "app.analysis.online-top-search", "en-US", "Online Top Search" },
                    { 28L, "app.analysis.per-capita-search", "en-US", "Per Capita Search" },
                    { 27L, "app.analysis.search-users", "en-US", "Search Users" },
                    { 26L, "app.analysis.all-day", "en-US", "All day" },
                    { 25L, "app.analysis.all-week", "en-US", "All Week" },
                    { 24L, "app.analysis.all-month", "en-US", "All Month" },
                    { 23L, "app.analysis.all-year", "en-US", "All Year" },
                    { 22L, "app.analysis.sales-ranking", "en-US", "Sales Ranking" },
                    { 21L, "app.analysis.sales-trend", "en-US", "Stores Sales Trend" },
                    { 39L, "app.analysis.table.weekly-range", "en-US", "Weekly Range" },
                    { 80L, "form.publicUsers.option.C", "en-US", "Colleague C" },
                    { 74L, "form.public.radio.public", "en-US", "Public" },
                    { 82L, "component.globalHeader.search.example1", "en-US", "Search example 1" },
                    { 139L, "menu.form", "en-US", "Form" },
                    { 138L, "menu.dashboard.workplace", "en-US", "Workplace" },
                    { 137L, "menu.dashboard.monitor", "en-US", "Monitor" },
                    { 136L, "menu.dashboard.analysis", "en-US", "Analysis" },
                    { 135L, "menu.dashboard", "en-US", "Dashboard" },
                    { 134L, "menu.register.result", "en-US", "Register Result" },
                    { 133L, "menu.register", "en-US", "Register" },
                    { 140L, "menu.form.basicform", "en-US", "Basic Form" },
                    { 132L, "menu.login", "en-US", "Login" },
                    { 130L, "validation.standard.required", "en-US", "Please enter a metric" },
                    { 129L, "validation.goal.required", "en-US", "Please enter a description of the goal" },
                    { 81L, "component.globalHeader.search", "en-US", "Search" },
                    { 127L, "validation.title.required", "en-US", "Please enter a title" },
                    { 126L, "validation.verification-code.required", "en-US", "Please enter the verification code!" },
                    { 125L, "validation.phone-number.wrong-format", "en-US", "Malformed phone number!" },
                    { 124L, "validation.phone-number.required", "en-US", "Please enter your phone number!" },
                    { 131L, "menu.home", "en-US", "Home" },
                    { 123L, "validation.confirm-password.required", "en-US", "Please confirm your password!" },
                    { 141L, "menu.form.stepform", "en-US", "Step Form" },
                    { 143L, "menu.form.stepform.confirm", "en-US", "Step Form(confirm transfer information)" },
                    { 160L, "menu.exception", "en-US", "Exception" },
                    { 159L, "menu.result.fail", "en-US", "Fail" },
                    { 158L, "menu.result.success", "en-US", "Success" },
                    { 157L, "menu.result", "en-US", "Result" },
                    { 156L, "menu.profile.advanced", "en-US", "Advanced Profile" },
                    { 155L, "menu.profile.basic", "en-US", "Basic Profile" },
                    { 154L, "menu.profile", "en-US", "Profile" },
                    { 142L, "menu.form.stepform.info", "en-US", "Step Form(write transfer information)" },
                    { 153L, "menu.list.searchlist.applications", "en-US", "Search List(applications)" },
                    { 151L, "menu.list.searchlist.articles", "en-US", "Search List(articles)" },
                    { 150L, "menu.list.searchlist", "en-US", "Search List" },
                    { 149L, "menu.list.cardlist", "en-US", "Card List" },
                    { 148L, "menu.list.basiclist", "en-US", "Basic List" },
                    { 147L, "menu.list.searchtable", "en-US", "Search Table" },
                    { 146L, "menu.list", "en-US", "List" },
                    { 145L, "menu.form.advancedform", "en-US", "Advanced Form" },
                    { 152L, "menu.list.searchlist.projects", "en-US", "Search List(projects)" },
                    { 122L, "validation.password.strength.short", "en-US", "Strength] = too short" },
                    { 128L, "validation.date.required", "en-US", "Please select the start and end date" },
                    { 120L, "validation.password.strength.strong", "en-US", "Strength] = strong" },
                    { 98L, "app.login.message-invalid-credentials", "en-US", "Invalid username or password（admin/ant.design）" },
                    { 97L, "app.login.password", "en-US", "password" },
                    { 96L, "app.login.userName", "en-US", "userName" },
                    { 121L, "validation.password.strength.medium", "en-US", "Strength] = medium" },
                    { 94L, "component.noticeIcon.empty", "en-US", "No notifications" },
                    { 93L, "component.noticeIcon.cleared", "en-US", "Cleared" },
                    { 92L, "component.noticeIcon.clear", "en-US", "Clear" },
                    { 99L, "app.login.message-invalid-verification-code", "en-US", "Invalid verification code" },
                    { 91L, "component.globalHeader.event.empty", "en-US", "You have viewed all events." },
                    { 89L, "component.globalHeader.message.empty", "en-US", "You have viewed all messsages." },
                    { 88L, "component.globalHeader.message", "en-US", "Message" },
                    { 87L, "component.globalHeader.notification.empty", "en-US", "You have viewed all notifications." },
                    { 86L, "component.globalHeader.notification", "en-US", "Notification" },
                    { 85L, "component.globalHeader.help", "en-US", "Help" },
                    { 84L, "component.globalHeader.search.example3", "en-US", "Search example 3" },
                    { 83L, "component.globalHeader.search.example2", "en-US", "Search example 2" },
                    { 90L, "component.globalHeader.event", "en-US", "Event" },
                    { 100L, "app.login.tab-login-credentials", "en-US", "Credentials" },
                    { 95L, "component.noticeIcon.view-more", "en-US", "View more" },
                    { 102L, "app.login.remember-me", "en-US", "Remember me" },
                    { 119L, "validation.password.strength.msg", "en-US", "Please enter at least 6 characters and don't use passwords that are easy to guess." },
                    { 118L, "validation.password.twice", "en-US", "The passwords entered twice do not match!" },
                    { 101L, "app.login.tab-login-mobile", "en-US", "Mobile number" },
                    { 116L, "validation.userName.required", "en-US", "Please enter your userName!" },
                    { 115L, "validation.email.wrong-format", "en-US", "The email address is in the wrong format!" },
                    { 114L, "validation.email.required", "en-US", "Please enter your email!" },
                    { 113L, "app.register-result.view-mailbox", "en-US", "View mailbox" },
                    { 112L, "app.register-result.back-home", "en-US", "Back to home" },
                    { 111L, "app.register-result.activation-email", "en-US", "The activation email has been sent to your email address and is valid for 24 hours. Please log in to the email in time and click on the link in the email to activate the account." },
                    { 117L, "validation.password.required", "en-US", "Please enter your password!" },
                    { 109L, "app.register.sign-in", "en-US", "Already have an account?" },
                    { 108L, "app.register.get-verification-code", "en-US", "Get code" },
                    { 107L, "app.register.register", "en-US", "Register" },
                    { 106L, "app.login.login", "en-US", "Login" },
                    { 105L, "app.login.signup", "en-US", "Sign up" },
                    { 104L, "app.login.sign-in-with", "en-US", "Sign in with" },
                    { 110L, "app.register-result.msg", "en-US", "Account：registered at {email}" },
                    { 103L, "app.login.forgot-password", "en-US", "Forgot your password?" }
                });

            migrationBuilder.InsertData(
                table: "ParentStudent",
                columns: new[] { "ParentId", "StudentId" },
                values: new object[,]
                {
                    { 4L, 3L },
                    { 4L, 8L }
                });

            migrationBuilder.InsertData(
                table: "Parents",
                columns: new[] { "ParentId", "CreatedBy", "DateCreated", "DateUpdated", "IsDeleted", "UpdatedBy" },
                values: new object[] { 4L, null, null, null, false, null });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "StudentId", "CreatedBy", "DateCreated", "DateUpdated", "ExternalId", "IsDeleted", "StudentNumber", "UpdatedBy" },
                values: new object[,]
                {
                    { 16L, null, null, null, "0010", false, "0010", null },
                    { 3L, null, null, null, "0001", false, "0001", null },
                    { 8L, null, null, null, "0002", false, "0002", null },
                    { 9L, null, null, null, "0003", false, "0003", null },
                    { 10L, null, null, null, "0004", false, "0004", null },
                    { 11L, null, null, null, "0005", false, "0005", null },
                    { 12L, null, null, null, "0006", false, "0006", null },
                    { 13L, null, null, null, "0007", false, "0007", null },
                    { 14L, null, null, null, "0008", false, "0008", null },
                    { 15L, null, null, null, "0009", false, "0009", null },
                    { 17L, null, null, null, "0011", false, "0011", null }
                });

            migrationBuilder.InsertData(
                table: "Schools",
                columns: new[] { "SchoolId", "CreatedBy", "CurrentGradingTerm", "DateCreated", "DateUpdated", "DistrictId", "IconUrl", "IsDeleted", "LogoUrl", "SchoolName", "UpdatedBy" },
                values: new object[] { 1L, null, 2L, null, null, 1L, "", false, "", "Test School", null });

            migrationBuilder.InsertData(
                table: "Schools",
                columns: new[] { "SchoolId", "CreatedBy", "CurrentGradingTerm", "DateCreated", "DateUpdated", "DistrictId", "IconUrl", "IsDeleted", "LogoUrl", "SchoolName", "UpdatedBy" },
                values: new object[] { 2L, null, 2L, null, null, 2L, "", false, "", "Test School 2", null });

            migrationBuilder.InsertData(
                table: "Classes",
                columns: new[] { "ClassId", "ClassName", "CreatedBy", "DateCreated", "DateUpdated", "Description", "GradeRangesJson", "GradingTerm", "IsDeleted", "IsTemplate", "Period", "SchoolId", "UpdatedBy", "UseDefaultGradesRange" },
                values: new object[,]
                {
                    { 1L, "Class 1", null, null, null, "No Desc", null, 1L, false, false, "1", 1L, null, true },
                    { 2L, "Class 2", null, null, null, "No Desc", null, 2L, false, false, "2", 1L, null, true },
                    { 3L, "Class 3", null, null, null, "Desc", null, 3L, false, false, "3", 1L, null, true }
                });

            migrationBuilder.InsertData(
                table: "SchoolFaculty",
                columns: new[] { "SchoolId", "FacultyId" },
                values: new object[,]
                {
                    { 1L, 5L },
                    { 2L, 6L }
                });

            migrationBuilder.InsertData(
                table: "SchoolStudent",
                columns: new[] { "SchoolId", "StudentId", "GradingPeriod", "Grade" },
                values: new object[,]
                {
                    { 1L, 3L, 1L, 0.5m },
                    { 1L, 8L, 1L, 0.4m },
                    { 1L, 9L, 1L, 0.3m },
                    { 1L, 10L, 1L, 0.2m },
                    { 1L, 11L, 1L, 0.1m },
                    { 1L, 12L, 1L, 0.1m },
                    { 1L, 13L, 1L, 0.1m },
                    { 1L, 14L, 1L, 0.1m },
                    { 1L, 15L, 1L, null },
                    { 1L, 16L, 1L, 0.1m }
                });

            migrationBuilder.InsertData(
                table: "Activities",
                columns: new[] { "ActivityId", "CategoryId", "ClassId", "Color", "CreatedBy", "DateAssigned", "DateCreated", "DateCutoff", "DateDue", "DateUpdated", "Description", "IsDeleted", "IsGraded", "MaxScore", "ParentActivityId", "PublishEndDate", "PublishStartDate", "SortIndex", "Title", "Type", "UpdatedBy", "Weight" },
                values: new object[,]
                {
                    { 1L, null, 1L, "#FFFFFF", 4L, new DateTime(2019, 3, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 3, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new DateTime(2019, 3, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), "Description activity", false, false, 100m, null, null, null, 0, "Activity 1", 0, 4L, 1m },
                    { 2L, null, 1L, "#FFFFFF", 4L, new DateTime(2019, 3, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 3, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new DateTime(2019, 3, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), "Description activity", false, false, 100m, null, null, null, 0, "Activity 2", 0, 4L, 2m },
                    { 3L, null, 2L, "#FFFFFF", 4L, new DateTime(2019, 3, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 3, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new DateTime(2019, 3, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 4, 24, 6, 31, 51, 825, DateTimeKind.Utc), "Description activity", false, true, 10m, null, null, null, 0, "Activity 2", 0, 4L, 1m }
                });

            migrationBuilder.InsertData(
                table: "ClassFaculty",
                columns: new[] { "ClassId", "FacultyId", "SortIndex" },
                values: new object[,]
                {
                    { 1L, 5L, 10000 },
                    { 3L, 5L, 10000 },
                    { 2L, 5L, 10000 }
                });

            migrationBuilder.InsertData(
                table: "ClassStudent",
                columns: new[] { "ClassId", "StudentId", "CumulativeGrade", "LastAccessed", "SortIndex" },
                values: new object[,]
                {
                    { 3L, 8L, null, null, 10000 },
                    { 3L, 3L, null, null, 10000 },
                    { 2L, 11L, null, null, 10000 },
                    { 2L, 9L, null, null, 10000 },
                    { 2L, 3L, null, null, 10000 },
                    { 1L, 16L, null, null, 10000 },
                    { 1L, 15L, null, null, 10000 },
                    { 1L, 14L, null, null, 10000 },
                    { 1L, 13L, null, null, 10000 },
                    { 1L, 12L, null, null, 10000 },
                    { 1L, 11L, null, null, 10000 },
                    { 1L, 10L, null, null, 10000 },
                    { 1L, 9L, null, null, 10000 },
                    { 1L, 8L, null, null, 10000 },
                    { 1L, 3L, null, null, 10000 },
                    { 3L, 9L, null, null, 10000 },
                    { 3L, 10L, null, null, 10000 }
                });

            migrationBuilder.InsertData(
                table: "ActivityScores",
                columns: new[] { "ActivityId", "StudentId", "Character", "Collaboration", "Communication", "Creativity", "CriticalThinking", "GradeDate", "IsSubmitted", "SavedDate", "Score", "ThreadId" },
                values: new object[,]
                {
                    { 1L, 3L, null, null, null, null, null, null, false, new DateTime(2019, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4m, null },
                    { 1L, 8L, null, null, null, null, null, null, false, new DateTime(2019, 3, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), 3m, null },
                    { 1L, 9L, null, null, null, null, null, null, false, null, 0m, null },
                    { 1L, 10L, null, null, null, null, null, null, false, null, null, null },
                    { 1L, 11L, null, null, null, null, null, null, false, null, null, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "ActivityId",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "ActivityId",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "ActivityCategories",
                keyColumn: "ActivityCategoryId",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "ActivityCategories",
                keyColumn: "ActivityCategoryId",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "ActivityCategories",
                keyColumn: "ActivityCategoryId",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "ActivityScores",
                keyColumns: new[] { "ActivityId", "StudentId" },
                keyValues: new object[] { 1L, 3L });

            migrationBuilder.DeleteData(
                table: "ActivityScores",
                keyColumns: new[] { "ActivityId", "StudentId" },
                keyValues: new object[] { 1L, 8L });

            migrationBuilder.DeleteData(
                table: "ActivityScores",
                keyColumns: new[] { "ActivityId", "StudentId" },
                keyValues: new object[] { 1L, 9L });

            migrationBuilder.DeleteData(
                table: "ActivityScores",
                keyColumns: new[] { "ActivityId", "StudentId" },
                keyValues: new object[] { 1L, 10L });

            migrationBuilder.DeleteData(
                table: "ActivityScores",
                keyColumns: new[] { "ActivityId", "StudentId" },
                keyValues: new object[] { 1L, 11L });

            migrationBuilder.DeleteData(
                table: "ClassFaculty",
                keyColumns: new[] { "ClassId", "FacultyId" },
                keyValues: new object[] { 1L, 5L });

            migrationBuilder.DeleteData(
                table: "ClassFaculty",
                keyColumns: new[] { "ClassId", "FacultyId" },
                keyValues: new object[] { 2L, 5L });

            migrationBuilder.DeleteData(
                table: "ClassFaculty",
                keyColumns: new[] { "ClassId", "FacultyId" },
                keyValues: new object[] { 3L, 5L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 1L, 3L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 1L, 8L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 1L, 9L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 1L, 10L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 1L, 11L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 1L, 12L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 1L, 13L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 1L, 14L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 1L, 15L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 1L, 16L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 2L, 3L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 2L, 9L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 2L, 11L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 3L, 3L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 3L, 8L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 3L, 9L });

            migrationBuilder.DeleteData(
                table: "ClassStudent",
                keyColumns: new[] { "ClassId", "StudentId" },
                keyValues: new object[] { 3L, 10L });

            migrationBuilder.DeleteData(
                table: "EmailAccounts",
                keyColumn: "EmailAccountId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "EmailTemplates",
                keyColumn: "EmailTemplateId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "EmailTemplates",
                keyColumn: "EmailTemplateId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "EmailTemplates",
                keyColumn: "EmailTemplateId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Faculty",
                keyColumn: "FacultyId",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "Faculty",
                keyColumn: "FacultyId",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "GradingTerms",
                keyColumn: "GradingTermId",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 18L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 19L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 20L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 21L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 22L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 23L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 24L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 25L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 26L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 27L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 28L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 29L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 30L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 31L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 32L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 33L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 34L);

            migrationBuilder.DeleteData(
                table: "KeyValues",
                keyColumn: "KeyValueID",
                keyValue: 35L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 5L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 6L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 7L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 18L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 19L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 20L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 21L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 22L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 23L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 24L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 25L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 26L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 27L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 28L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 29L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 30L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 31L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 32L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 33L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 34L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 35L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 36L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 37L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 38L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 39L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 40L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 41L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 42L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 43L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 44L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 45L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 46L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 47L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 48L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 49L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 50L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 51L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 52L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 53L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 54L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 55L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 56L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 57L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 58L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 59L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 60L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 61L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 62L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 63L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 64L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 65L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 66L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 67L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 68L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 69L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 70L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 71L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 72L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 73L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 74L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 75L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 76L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 77L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 78L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 79L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 80L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 81L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 82L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 83L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 84L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 85L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 86L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 87L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 88L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 89L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 90L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 91L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 92L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 93L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 94L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 95L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 96L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 97L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 98L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 99L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 100L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 101L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 102L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 103L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 104L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 105L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 106L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 107L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 108L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 109L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 110L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 111L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 112L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 113L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 114L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 115L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 116L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 117L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 118L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 119L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 120L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 121L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 122L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 123L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 124L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 125L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 126L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 127L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 128L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 129L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 130L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 131L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 132L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 133L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 134L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 135L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 136L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 137L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 138L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 139L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 140L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 141L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 142L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 143L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 144L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 145L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 146L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 147L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 148L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 149L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 150L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 151L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 152L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 153L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 154L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 155L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 156L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 157L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 158L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 159L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 160L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 161L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 162L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 163L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 164L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 165L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 166L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 167L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 168L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 169L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 170L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 171L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 172L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 173L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 174L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 175L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 176L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 177L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 178L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 179L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 180L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 181L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 182L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 183L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 184L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 185L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 186L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 187L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 188L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 189L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 190L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 191L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 192L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 193L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 194L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 195L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 196L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 197L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 198L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 199L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 200L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 201L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 202L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 203L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 204L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 205L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 206L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 207L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 208L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 209L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 210L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 211L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 212L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 213L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 214L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 215L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 216L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 217L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 218L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 219L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 220L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 221L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 222L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 223L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 224L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 225L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 226L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 227L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 228L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 229L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 230L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 231L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 232L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 233L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 234L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 235L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 236L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 237L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 238L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 239L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 240L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 241L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 242L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 243L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 244L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 245L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 246L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 247L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 248L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 249L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 250L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 251L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 252L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 253L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 254L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 255L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 256L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 257L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 258L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 259L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 260L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 261L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 262L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 263L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 264L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 265L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 266L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 267L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 268L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 269L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 270L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 271L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 272L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 273L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 274L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 275L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 276L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 277L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 278L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 279L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 280L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 281L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 282L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 283L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 284L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 285L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 286L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 287L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 288L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 289L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 290L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 291L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 292L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 293L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 294L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 295L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 296L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 297L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 298L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 299L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 300L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 301L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 302L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 303L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 304L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 305L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 306L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 307L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 308L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 309L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 310L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 311L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 312L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 313L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 314L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 315L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 316L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 317L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 318L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 319L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 320L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 321L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 322L);

            migrationBuilder.DeleteData(
                table: "Localizations",
                keyColumn: "RecordId",
                keyValue: 323L);

            migrationBuilder.DeleteData(
                table: "ParentStudent",
                keyColumns: new[] { "ParentId", "StudentId" },
                keyValues: new object[] { 4L, 3L });

            migrationBuilder.DeleteData(
                table: "ParentStudent",
                keyColumns: new[] { "ParentId", "StudentId" },
                keyValues: new object[] { 4L, 8L });

            migrationBuilder.DeleteData(
                table: "Parents",
                keyColumn: "ParentId",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "SchoolFaculty",
                keyColumns: new[] { "SchoolId", "FacultyId" },
                keyValues: new object[] { 1L, 5L });

            migrationBuilder.DeleteData(
                table: "SchoolFaculty",
                keyColumns: new[] { "SchoolId", "FacultyId" },
                keyValues: new object[] { 2L, 6L });

            migrationBuilder.DeleteData(
                table: "SchoolStudent",
                keyColumns: new[] { "SchoolId", "StudentId", "GradingPeriod" },
                keyValues: new object[] { 1L, 3L, 1L });

            migrationBuilder.DeleteData(
                table: "SchoolStudent",
                keyColumns: new[] { "SchoolId", "StudentId", "GradingPeriod" },
                keyValues: new object[] { 1L, 8L, 1L });

            migrationBuilder.DeleteData(
                table: "SchoolStudent",
                keyColumns: new[] { "SchoolId", "StudentId", "GradingPeriod" },
                keyValues: new object[] { 1L, 9L, 1L });

            migrationBuilder.DeleteData(
                table: "SchoolStudent",
                keyColumns: new[] { "SchoolId", "StudentId", "GradingPeriod" },
                keyValues: new object[] { 1L, 10L, 1L });

            migrationBuilder.DeleteData(
                table: "SchoolStudent",
                keyColumns: new[] { "SchoolId", "StudentId", "GradingPeriod" },
                keyValues: new object[] { 1L, 11L, 1L });

            migrationBuilder.DeleteData(
                table: "SchoolStudent",
                keyColumns: new[] { "SchoolId", "StudentId", "GradingPeriod" },
                keyValues: new object[] { 1L, 12L, 1L });

            migrationBuilder.DeleteData(
                table: "SchoolStudent",
                keyColumns: new[] { "SchoolId", "StudentId", "GradingPeriod" },
                keyValues: new object[] { 1L, 13L, 1L });

            migrationBuilder.DeleteData(
                table: "SchoolStudent",
                keyColumns: new[] { "SchoolId", "StudentId", "GradingPeriod" },
                keyValues: new object[] { 1L, 14L, 1L });

            migrationBuilder.DeleteData(
                table: "SchoolStudent",
                keyColumns: new[] { "SchoolId", "StudentId", "GradingPeriod" },
                keyValues: new object[] { 1L, 15L, 1L });

            migrationBuilder.DeleteData(
                table: "SchoolStudent",
                keyColumns: new[] { "SchoolId", "StudentId", "GradingPeriod" },
                keyValues: new object[] { 1L, 16L, 1L });

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 8L);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 9L);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 10L);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 11L);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 12L);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 13L);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 14L);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 15L);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 16L);

            migrationBuilder.DeleteData(
                table: "Students",
                keyColumn: "StudentId",
                keyValue: 17L);

            migrationBuilder.DeleteData(
                table: "Activities",
                keyColumn: "ActivityId",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "ClassId",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "ClassId",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Schools",
                keyColumn: "SchoolId",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "Classes",
                keyColumn: "ClassId",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Districts",
                keyColumn: "DistrictId",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "GradingTerms",
                keyColumn: "GradingTermId",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "GradingTerms",
                keyColumn: "GradingTermId",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "GradingTerms",
                keyColumn: "GradingTermId",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Schools",
                keyColumn: "SchoolId",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Districts",
                keyColumn: "DistrictId",
                keyValue: 1L);
        }
    }
}
