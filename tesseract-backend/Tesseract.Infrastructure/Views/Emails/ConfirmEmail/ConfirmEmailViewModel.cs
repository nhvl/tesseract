﻿using Tesseract.Infrastructure.Views.Shared;

namespace Tesseract.Infrastructure.Views.Emails
{
    public class ConfirmEmailViewModel: EmailLayoutViewModel
    {
        public string ConfirmEmailUrl { get; set; }
    }
}
