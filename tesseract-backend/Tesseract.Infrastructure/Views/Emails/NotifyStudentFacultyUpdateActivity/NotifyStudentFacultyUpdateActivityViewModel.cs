﻿using System;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;
using Tesseract.Infrastructure.Views.Shared;

namespace Tesseract.Infrastructure.Views.Emails
{
    public class NotifyStudentFacultyUpdateActivityViewModel: EmailLayoutViewModel
    {
        public Activity Activity { get; set; }
        public ApplicationUser UpdateBy { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
