﻿using Tesseract.Infrastructure.Views.Shared;

namespace Tesseract.Infrastructure.Views.Emails
{
    public class ForgotPasswordViewModel: EmailLayoutViewModel
    {
        public string ResetPasswordUrl { get; set; }
        public string DistrictName { get; set; }
    }
}
