﻿using Tesseract.Infrastructure.Views.Shared;

namespace Tesseract.Infrastructure.Views.Emails
{
    public class NewUserSetPasswordViewModel: EmailLayoutViewModel
    {
        public string ResetPasswordUrl { get; set; }
        public string DistrictName { get; set; }
    }
}
