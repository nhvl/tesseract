﻿namespace Tesseract.Infrastructure.Views.Shared
{
    public class EmailLayoutViewModel
    {
        public string HomeUrl { get; set; }
        public string DistrictLogo { get; set; }
    }
}
