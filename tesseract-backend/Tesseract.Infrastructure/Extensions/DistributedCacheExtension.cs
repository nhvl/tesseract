﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tesseract.Infrastructure.Data
{
    public static class DistributedCacheExtension
    {
        public static async Task<T> GetAsync<T>(this IDistributedCache cache, string cacheKey, Func<Task<T>> f, CancellationToken token = default)
        {
            T value = await cache.GetJson<T>(cacheKey, token);
            if (value != null) return value;

            value = await f();
            if (value == null) return value;

            await cache.SetJson(cacheKey, value, token);
            return value;
        }

        public static async Task<T> SetJson<T>(this IDistributedCache cache, string cacheKey, T value, CancellationToken token = default)
        {
            await cache.SetAsync(cacheKey, Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(value)), token);
            return value;
        }

        public static async Task<T> GetJson<T>(this IDistributedCache cache, string cacheKey, CancellationToken token = default)
        {
            byte[] value = await cache.GetAsync(cacheKey, token);
            if (value == null) return default;
            return JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(value));
        }
    }
}
