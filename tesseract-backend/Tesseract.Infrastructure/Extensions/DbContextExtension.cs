﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Data
{
    public static class DbContextExtension
    {
        public static async Task<T> AddAsync<T>(this DbContext dbContext, T entity, bool commit = true) where T : BaseEntity
        {
            entity.DateCreated = DateTime.UtcNow;
            entity.DateUpdated = DateTime.UtcNow;

            var dbSet = dbContext.Set<T>();
            var e = await dbSet.AddAsync(entity);

            if (commit) await dbContext.SaveChangesAsync();

            return e.Entity;
        }

        public static async Task<T> AddLinkEntityAsync<T>(this DbContext dbContext, T entity, bool commit = true) where T : class
        {
            var dbSet = dbContext.Set<T>();
            var e = await dbSet.AddAsync(entity);

            if (commit) await dbContext.SaveChangesAsync();
            return e.Entity;
        }

        public static async Task UpdateAsync<T>(this DbContext dbContext, T entity, bool commit = true) where T : BaseEntity
        {
            entity.DateUpdated = DateTime.UtcNow;

            var dbSet = dbContext.Set<T>();
            dbSet.Attach(entity);
            dbContext.Entry(entity).State = EntityState.Modified;

            if (commit) await dbContext.SaveChangesAsync();
        }

        public static async Task UpdateLinkEntityAsync<T>(this DbContext dbContext, T entity, bool commit = true) where T : class
        {
            var dbSet = dbContext.Set<T>();
            dbSet.Attach(entity);
            dbContext.Entry(entity).State = EntityState.Modified;

            if (commit) await dbContext.SaveChangesAsync();
        }

        public static async Task DeleteAsync<T>(this DbContext dbContext, T entity, bool commit = true) where T : BaseEntity
        {
            // set deleted flag
            entity.IsDeleted = true;
            entity.DateUpdated = DateTime.UtcNow;

            var dbSet = dbContext.Set<T>();
            dbSet.Attach(entity);
            dbContext.Entry(entity).State = EntityState.Modified;

            if (commit) await dbContext.SaveChangesAsync();
        }

        public static async Task<int> DeleteAsync<T>(this DbContext dbContext, Expression<Func<T, bool>> where, bool commit = true) where T : BaseEntity
        {
            var dbSet = dbContext.Set<T>();
            var objects = dbSet.Where(where).AsEnumerable();
            var removeCount = objects.Count();

            foreach (var obj in objects)
            {
                // set deleted flag
                obj.IsDeleted = true;

                dbSet.Attach(obj);
                dbContext.Entry(obj).State = EntityState.Modified;
            }

            if (commit) await dbContext.SaveChangesAsync();

            return removeCount;
        }

        public static async Task<int> DeleteLinkEntityAsync<T>(this DbContext dbContext, Expression<Func<T, bool>> where, bool commit = true) where T : class
        {
            var dbSet = dbContext.Set<T>();
            var objects = await dbSet.Where(where).ToListAsync();
            var removeCount = objects.Count;

            foreach (var obj in objects)
            {
                dbSet.Attach(obj);
                dbContext.Entry(obj).State = EntityState.Deleted;
            }

            if (commit) await dbContext.SaveChangesAsync();

            return removeCount;
        }

        public static async Task<T> GetByIdAsync<T>(this DbContext dbContext, int id) where T : BaseEntity
        {
            var dbSet = dbContext.Set<T>();
            var item = await dbSet.FindAsync(id);

            return item.IsDeleted ? null : item;
        }

        public static async Task<IEnumerable<T>> GetAllAsync<T>(this DbContext dbContext) where T : BaseEntity
        {
            var dbSet = dbContext.Set<T>();
            return await dbSet.ToListAsync();
        }

        public static async Task<IEnumerable<T>> GetManyAsync<T>(this DbContext dbContext, Expression<Func<T, bool>> where) where T : BaseEntity
        {
            var dbSet = dbContext.Set<T>();
            return await dbSet.Where(where).ToListAsync();
        }

        public static async Task<T> GetAsync<T>(this DbContext dbContext, Expression<Func<T, bool>> where) where T : BaseEntity
        {
            var dbSet = dbContext.Set<T>();
            return await dbSet.Where(where).FirstOrDefaultAsync();
        }

        public static IQueryable<T> GetTable<T>(this DbContext dbContext) where T : BaseEntity
        {
            var dbSet = dbContext.Set<T>();
            return dbSet;
        }

        public static IQueryable<T> GetTableUntracked<T>(this DbContext dbContext) where T : BaseEntity
        {
            var dbSet = dbContext.Set<T>();
            return dbSet.AsNoTracking();
        }
    }
}