﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Tesseract.Infrastructure.Data
{
    public static class EnumExtensions
    {
        public static string GetDescriptionFromEnumValue(Enum value)
        {
            DescriptionAttribute attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .SingleOrDefault() as DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;
        }
        public static string GetColor(Enum value)
        {
            var str = value.ToString("X").TrimStart(new Char[] { '0' });
            if (!str.StartsWith("#"))
            {
                str = "#" + str;
            }
            return str;
        }
    }
}
