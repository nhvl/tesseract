﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Tesseract.Core.Entities.Identity;
using Tesseract.Infrastructure.Configuration;

namespace Tesseract.Infrastructure.Utils
{
    public static class TokenHelper
    {
        public static string IssueToken(AuthenticationSettings authTokenSettings, ApplicationUser user, IList<string> roles, IList<Claim> claims)
        {
            DateTime issueDate = DateTime.UtcNow;

            var identityClaims = GenerateIdentityClaims(user, roles, claims);
            var key = new SymmetricSecurityKey(Convert.FromBase64String(authTokenSettings.ValidateTokenKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var jwtToken = new JwtSecurityToken(
                authTokenSettings.Issuer,
                authTokenSettings.Audience,
                identityClaims,
                issueDate,
                issueDate.AddHours(authTokenSettings.ExpiryHours),
                creds);

            return new JwtSecurityTokenHandler().WriteToken(jwtToken);
        }

        private static List<Claim> GenerateIdentityClaims(ApplicationUser user, IList<string> roles, IList<Claim> claims)
        {
            var result = new List<Claim>
            {
                new Claim(ClaimTypes.PrimarySid, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Id.ToString()) // for 2FA
            };

            if (!string.IsNullOrEmpty(user.Email))
            {
                result.Add(new Claim(ClaimTypes.Email, user.Email));
            }

            if (roles != null && roles.Count > 0)
            {
                foreach (string r in roles)
                    result.Add(new Claim(ClaimTypes.Role, r));
            }

            if (claims != null && claims.Count > 0)
                result.AddRange(claims);

            return result;
        }
    }
}
