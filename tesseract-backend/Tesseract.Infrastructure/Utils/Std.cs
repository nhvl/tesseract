﻿using Microsoft.AspNetCore.Http;

namespace Tesseract.Infrastructure.Utils
{
    public class Std
    {
        public static string ParseLocalUrl(HttpRequest request, string path)
        {
            if (string.IsNullOrEmpty(path) || path.StartsWith("http") || path.StartsWith("aigapp") || request == null)
                return path;

            if (path.StartsWith('/'))
                return $"{request.Scheme}://{request.Host}{path}";

            return $"{request.Scheme}://{request.Host}/{path}";
        }

        public static string GetClientIp(HttpRequest request)
        {
            return request.HttpContext.Connection.RemoteIpAddress.ToString();
        }
    }
}
