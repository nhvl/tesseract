﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Tesseract.Infrastructure.Utils
{
    public static class ScoreHepler
    {
        /// <summary>
        /// Calculate Cumulative Grade.
        /// Formula: TES-28
        /// </summary>
        /// <param name="scoreData">The score data.</param>
        /// <returns>Cumulative Score.</returns>
        public static decimal CalculateCumulativeGrade(IEnumerable<(decimal Score, decimal MaxScore, decimal Weight, bool IsExclude, bool IsCredit)> scoreData)
        {
            var invalidScore = scoreData.Any(x => x.MaxScore <= 0);
            if (invalidScore)
            {
                throw new DivideByZeroException("MaxScore is invalid. Cannot calculate cumulative grade");
            }

            var weightSum = scoreData.Sum(x =>
                x.Weight
                * (x.IsExclude ? 0 : 1)
                * (x.IsCredit ? 0 : 1)
            );
            if (weightSum == 0)
            {
                return 1;
            }

            var weightedValueSum = scoreData.Sum(x =>
                Math.Min(1, (x.Score / x.MaxScore))
                * x.Weight
                * (x.IsExclude ? 0 : 1)
            );

            return weightedValueSum / weightSum;
        }
    }
}
