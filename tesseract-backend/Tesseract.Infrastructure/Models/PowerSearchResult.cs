﻿using System.Collections.Generic;
using Tesseract.Core.Entities.Identity;
using Tesseract.Core.Entities.Main;

namespace Tesseract.Infrastructure.Models
{
    public class PowerSearchResult
    {
        public IEnumerable<(Student, ApplicationUser)> Students;
        public IEnumerable<(Faculty, ApplicationUser)> Faculties;
        public IEnumerable<(Parent, ApplicationUser)> Parents;
        public IEnumerable<School> Schools;
        public IEnumerable<Class> Classes;
        public IEnumerable<Activity> Activities;
        public PowerSearchResult()
        {
            Students = new List<(Student, ApplicationUser)>();
            Faculties = new List<(Faculty, ApplicationUser)>();
            Parents = new List<(Parent, ApplicationUser)>();
            Schools = new List<School>();
            Classes = new List<Class>();
            Activities = new List<Activity>();
        }
    }
}
