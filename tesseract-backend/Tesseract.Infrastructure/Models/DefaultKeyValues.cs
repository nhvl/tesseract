﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Tesseract.Infrastructure.Models
{
    public enum E_DefaultGradeRange
    {
        [Description("A+")]
        APlus = 100,
        [Description("A")]
        A = 96,
        [Description("A-")]
        AMinus = 92,
        [Description("B+")]
        BPlus = 89,
        [Description("B")]
        B = 86,
        [Description("B-")]
        BMinus = 82,
        [Description("C+")]
        CPlus = 79,
        [Description("C")]
        C = 76,
        [Description("C-")]
        CMinus = 72,
        [Description("D+")]
        DPlus = 69,
        [Description("D")]
        D = 66,
        [Description("D-")]
        DMinus = 62,
        [Description("F")]
        F = 60
    }

    public enum E_DefaultFiveCSkillsColor
    {
        Communication = 0xFF7D00,
        Collaboration = 0x41C4FF,
        Character = 0x3DD94A,
        Creativity = 0x9574EA,
        CriticalThinking = 0xFCEE21,
    }

    public enum E_DefaultCategoryColor
    {
        ActivityCategory_1 = 0xE6B8AF,
        ActivityCategory_2 = 0xF4CCCC,
        ActivityCategory_3 = 0xFCE5CD,
        ActivityCategory_4 = 0xFFF2CC,
        ActivityCategory_5 = 0xD9EAD3,
        ActivityCategory_6 = 0xD0E0E3,
        ActivityCategory_7 = 0xC9DAF8,
        ActivityCategory_8 = 0xCFE2F3,
        ActivityCategory_9 = 0xD9D2E9,
        ActivityCategory_10 = 0xEAD1DC,
    }
}
