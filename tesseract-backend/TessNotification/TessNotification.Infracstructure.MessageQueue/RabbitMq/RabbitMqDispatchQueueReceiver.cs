﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;

namespace TessNotification.Infracstructure.MessageQueue.RabbitMq
{
    public class RabbitMqDispatchQueueReceiver : IDispatchQueueReceiver, IDisposable
    {
        private readonly IModel _channel;
        private readonly ILogger<RabbitMqDispatchQueueReceiver> _logger;
        private readonly IUserService _userService;

        public RabbitMqDispatchQueueReceiver(IConnection connection, ILogger<RabbitMqDispatchQueueReceiver> logger, IUserService userService)
        {
            _channel = connection.CreateModel();
            _channel.QueueDeclare(queue: RabbitMqConstants.TaskQueue,
                                durable: false,
                                exclusive: false,
                                autoDelete: false);

            _channel.QueueDeclare(queue: RabbitMqConstants.NotifyQueue,
                                durable: false,
                                exclusive: false,
                                autoDelete: false);

            _logger = logger;
            _userService = userService;
        }

        public void Dispose()
        {
            _channel?.Close();
        }
    

        public async Task OnReceive(Func<long, Task> jobHandler, TimeSpan waitingTime)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (model, ea) =>
            {
                var message = Encoding.UTF8.GetString(ea.Body);
                if (!long.TryParse(message, out long userId))
                {
                    _logger.LogError($"Invalid user id from queue{RabbitMqConstants.TaskQueue} : {message}");
                    return;
                }

                await jobHandler(userId);
            };

            _channel.BasicConsume(queue: RabbitMqConstants.TaskQueue,
                                    autoAck: true,
                                    consumer: consumer);
            await Task.Delay(waitingTime);
        }

        public async Task OnReceiverNotification(Func<NotificationData, long[], Task> notificationHandler, TimeSpan waitingTime)
        {
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (model, ea) =>
            {
                // send email
                var message = Encoding.UTF8.GetString(ea.Body);
                var data = JsonConvert.DeserializeObject<NotificationModel>(message, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                if (data == null)
                {
                    _logger.LogError($"Invalid user id from queue{RabbitMqConstants.TaskQueue} : {message}");
                    return;
                }

                foreach (var item in data.Users)
                {
                    await _userService.Upsert(item.Id, item.Email);
                }

                await notificationHandler(new NotificationData(RabbitMqConstants.NotifyQueue, "", data.Activity), data.Users.Select(x => x.Id).ToArray());
            };

            _channel.BasicConsume(queue: RabbitMqConstants.NotifyQueue,
                                    autoAck: true,
                                    consumer: consumer);

            await Task.Delay(waitingTime);
        }
    }

    public class NotificationModel
    {
        public Dictionary<string, object> Activity { get; set; }
        public Notificationusers[] Users { get; set; }
    }

    public class Notificationusers
    {
        public long Id { get; set; }
        public string Email { get; set; }
    }
}