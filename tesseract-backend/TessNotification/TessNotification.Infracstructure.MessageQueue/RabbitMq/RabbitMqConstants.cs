﻿namespace TessNotification.Infracstructure.MessageQueue.RabbitMq
{
    internal static class RabbitMqConstants
    {
        internal static readonly string TaskQueue = "TaskDispatchedEvent";
        internal static readonly string NotifyQueue = "NotifyEvent";
    }
}