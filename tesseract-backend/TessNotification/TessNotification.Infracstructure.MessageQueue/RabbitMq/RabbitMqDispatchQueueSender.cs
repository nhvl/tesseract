﻿using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;

namespace TessNotification.Infracstructure.MessageQueue.RabbitMq
{
    public class RabbitMqDispatchQueueSender : IDispatchQueueSender, IDisposable
    {
        private readonly IModel _channel;
        private readonly ILogger<RabbitMqDispatchQueueSender> _logger;

        public RabbitMqDispatchQueueSender(IConnection connection, ILogger<RabbitMqDispatchQueueSender> logger)
        {
            _channel = connection.CreateModel();
            _channel.QueueDeclare(queue: RabbitMqConstants.TaskQueue,
                                durable: false,
                                exclusive: false,
                                autoDelete: false);

            _logger = logger;
        }

        public void Dispose()
        {
           _channel?.Close();
        }

        public void Send(long jobId)
        {
            _channel.BasicPublish(exchange: "",
                                         routingKey: RabbitMqConstants.TaskQueue,
                                         basicProperties: null,
                                         body: Encoding.UTF8.GetBytes(jobId.ToString()));
            _logger.LogInformation($"Add notification job {jobId} into queue {RabbitMqConstants.TaskQueue}");
        }
    }
}