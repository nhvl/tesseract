﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;

namespace TessNotification.Infracstructure.MessageQueue.Memory
{
    public class MemoryDispatchQueueReceiver : IDispatchQueueReceiver
    {
        internal static Func<NotificationJob, Task> SharedHandler = null;
        public async Task OnReceive(Func<long, Task> jobHandler, TimeSpan _)
        {
            var jobs = MemoryDispatchQueueSender.Jobs.ToArray();
            MemoryDispatchQueueSender.Jobs.Clear();
            foreach (var job in jobs)
            {
                await jobHandler(job);
            }
        }

        public Task OnReceiverNotification(Func<NotificationData, long[], Task> notification, TimeSpan timeSpan)
        {
            throw new NotImplementedException();
        }
    }
}