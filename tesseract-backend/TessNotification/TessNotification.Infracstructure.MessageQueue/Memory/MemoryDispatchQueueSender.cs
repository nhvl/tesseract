﻿using System.Collections.Generic;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;

namespace TessNotification.Infracstructure.MessageQueue.Memory
{
    public class MemoryDispatchQueueSender : IDispatchQueueSender
    {
        internal static IList<long> Jobs = new List<long>();
        public void Send(long job)
        {
            Jobs.Add(job);
        }
    }
}