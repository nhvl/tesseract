﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;

namespace TessNotification.Infracstructure.Data.Memory
{
    public class MemoryDispatchJobRepo : IDispatchJobRepo
    {
        private static readonly List<NotificationJob> JobList = new List<NotificationJob>();
        private static long JobCounter = 1;
        public Task<NotificationJob> Add(NotificationJob job)
        {
            JobList.Add(job);
            job.NotificationJobId = JobCounter++;
            return Task.FromResult(job);
        }

        public Task<NotificationJob> GetJob(long jobId)
        {
            var job = JobList.FirstOrDefault(j => j.NotificationJobId == jobId);
            return Task.FromResult(job);
        }

        public Task<IEnumerable<NotificationJob>> GetReadyJobs()
        {
            var now = DateTime.Now;
            IEnumerable<NotificationJob> jobs = JobList.Where(job => job.DispatchTime > now).ToArray();
            return Task.FromResult(jobs);
        }

        public Task Remove(NotificationJob job)
        {
            JobList.Remove(job);
            return Task.CompletedTask;
        }
    }
}
