﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;

namespace TessNotification.Infracstructure.Data.Memory
{

    public class MemorySubscriptionRepo : ISubscriptionRepo
    {
        private static readonly IList<Subscription> SubsList = new List<Subscription>();
        public Task<Subscription> Create(Subscription subscription)
        {
            SubsList.Add(subscription);
            return Task.FromResult(subscription);
        }

        public async Task<Subscription> Delete(long userId, long channelId)
        {
            var item = await Get(userId, channelId);
            if (item != null)
            {
                SubsList.Remove(item);
            }

            return item;
        }

        public Task<Subscription> Get(long userId, long channelId)
        {
            return Task.FromResult(SubsList.FirstOrDefault(x => x.UserId == userId && x.ChannelId == channelId));
        }

        public Task<IEnumerable<Subscription>> GetByUser(long userId)
        {
            return Task.FromResult(SubsList.Where(x => x.UserId == userId));
        }

        public async Task<Subscription> Update(Subscription subscription)
        {
            var item = await Get(subscription.UserId, subscription.ChannelId);
            if (item == null) throw new InvalidOperationException();
            item.SendTime = subscription.SendTime;
            item.WeeklyDay = subscription.WeeklyDay;
            item.SendType = subscription.SendType;
            return item;
        }
    }
}