﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;

namespace TessNotification.Infracstructure.Data.Memory
{
    public class MemoryUserRepo : IUserRepo
    {
        private static readonly IList<User> UserList = new List<User>();

        public Task<User> Create(User user)
        {
            UserList.Add(user);
            return Task.FromResult(user);
        }

        public async Task<User> Delete(long userId)
        {
             var item = await Get(userId);
            if (item != null)
            {
                UserList.Remove(item);
            }

            return item;
        }

        public Task<User> Get(long userId)
        {
            return Task.FromResult(UserList.FirstOrDefault(x => x.UserId == userId));
        }

        public async Task<User> Update(User user)
        {
            var item = await Get(user.UserId);
            if (item == null) throw new InvalidOperationException();
            item.Email = user.Email;
            return item;
        }
    }
}