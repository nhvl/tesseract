﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;


namespace TessNotification.Infracstructure.Data.Memory
{

    public class MemoryChannelRepo : IChannelRepo
    {
        private static readonly IEnumerable<Channel> Channels = new[] { new Channel(1, "Email", "Send Email") };
        public Task<IEnumerable<Channel>> GetChannels()
        {
            return Task.FromResult(Channels);
        }
    }
}