﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TessNotification.Infracstructure.Data.Migrations
{
    public partial class changesubscription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SendCrontab",
                table: "Subscription");

            migrationBuilder.AddColumn<TimeSpan>(
                name: "SendTime",
                table: "Subscription",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<byte>(
                name: "WeeklyDay",
                table: "Subscription",
                nullable: false,
                defaultValue: (byte)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SendTime",
                table: "Subscription");

            migrationBuilder.DropColumn(
                name: "WeeklyDay",
                table: "Subscription");

            migrationBuilder.AddColumn<string>(
                name: "SendCrontab",
                table: "Subscription",
                nullable: true);
        }
    }
}
