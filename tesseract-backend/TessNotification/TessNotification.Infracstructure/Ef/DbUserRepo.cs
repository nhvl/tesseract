﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;


namespace TessNotification.Infracstructure.Data.Ef
{
    public class DbUserRepo : IUserRepo
    {
        private readonly MainDbContext _dbContext;

        public DbUserRepo(MainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<User> Create(User user)
        {
            var data = await _dbContext.AddAsync(user, true);
            return data;

        }

        public async Task<User> Delete(long userId)
        {
            var user = await Get(userId);
            if (user != null)
            {
                await _dbContext.DeleteAsync(user, true);
            }

            return user;
        }

        public Task<User> Get(long userId)
        {
            return _dbContext.GetAsync<User>(user => user.UserId == userId);
        }

        public Task<User> Update(User user)
        {
            return _dbContext.UpdateAsync(user, true);
        }
    }
}