﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;


namespace TessNotification.Infracstructure.Data.Ef
{
    public class DbDispatchJobRepo : IDispatchJobRepo
    {
        private readonly MainDbContext _dbContext;

        public DbDispatchJobRepo(MainDbContext dbContext) => _dbContext = dbContext;


        public Task<NotificationJob> GetJob(long jobId)
        {
            return _dbContext.GetAsync<NotificationJob>(job => job.NotificationJobId == jobId);
        }

        public async Task<IEnumerable<NotificationJob>> GetReadyJobs()
        {
            var now = DateTime.Now;
            return await _dbContext.GetManyAsync<NotificationJob>(job => job.DispatchTime > now);
        }

        public Task Remove(NotificationJob job)
        {
            return _dbContext.DeleteAsync(job, true);
        }

        public Task<NotificationJob> Add(NotificationJob job)
        {
            return _dbContext.AddAsync(job, true);
        }
    }
}