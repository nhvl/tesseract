﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.Linq;
using TessNotification.Domain.Entities;


namespace TessNotification.Infracstructure.Data.Ef
{

    public class MainDbContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Channel>().ToTable("Channels").HasKey(e => e.ChannelId);
            modelBuilder.Entity<Channel>().HasData(new Channel(1, "Email", "Email"));
            modelBuilder.Entity<Channel>().HasData(new Channel(2, "SMS", "SMS"));
            modelBuilder.Entity<Channel>().HasData(new Channel(3, "SignalR", "SignalR"));

            modelBuilder.Entity<NotificationJob>().ToTable("NotificationJobs").HasKey(e => e.NotificationJobId);
            modelBuilder.Entity<NotificationJob>().Property(e => e.NotificationJobId).ValueGeneratedOnAdd();

            modelBuilder.Entity<NotificationJob>().HasQueryFilter(e => !e.IsDeleted);
            modelBuilder.Entity<NotificationJob>().Property(e => e.Data).HasJsonConversion();

            modelBuilder.Entity<User>().ToTable("Users").HasKey(e => e.UserId);
            modelBuilder.Entity<User>().Property(e => e.UserId).ValueGeneratedOnAdd();
            modelBuilder.Entity<User>().HasQueryFilter(e => !e.IsDeleted);

            modelBuilder.Entity<Subscription>().ToTable("Subscription").HasKey(e => new { e.UserId, e.ChannelId });
            modelBuilder.Entity<Subscription>().HasQueryFilter(e => !e.IsDeleted);

            base.OnModelCreating(modelBuilder);
        }

        public MainDbContext(DbContextOptions<MainDbContext> options) : base(options) { }
    }

    public static class PropertyBuilderExtension
    {
        public static PropertyBuilder<TProperty> HasJsonConversion<TProperty>(this PropertyBuilder<TProperty> builder) where TProperty : class
        {
            var setting = new JsonSerializerSettings
            {
                TypeNameHandling = typeof(TProperty).IsInterface ? TypeNameHandling.All : TypeNameHandling.Auto,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            return builder.HasConversion(
                v => JsonConvert.SerializeObject(v, Formatting.Indented, setting),
                v => JsonConvert.DeserializeObject<TProperty>(v, setting)
            );
        }
    }
}