﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;


namespace TessNotification.Infracstructure.Data.Ef
{
    public class DbChannelRepo : IChannelRepo
    {
        private readonly MainDbContext _dbContext;

        public DbChannelRepo(MainDbContext dbContext) => _dbContext = dbContext;

        public async Task<IEnumerable<Channel>> GetChannels() => await _dbContext.Set<Channel>().ToArrayAsync();
    }
}