﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;


namespace TessNotification.Infracstructure.Data.Ef
{
    public class DbSubscriptionRepo : ISubscriptionRepo
    {
        private readonly MainDbContext _dbContext;

        public DbSubscriptionRepo(MainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<Subscription> Create(Subscription subscription)
        {
            return _dbContext.AddAsync(subscription, true);
        }

        public async Task<Subscription> Delete(long userId, long channelId)
        {
            var subs = await Get(userId, channelId);
            if (subs != null)
            {
                await _dbContext.DeleteAsync(subs, true);
            }

            return subs;
        }

        public Task<Subscription> Get(long userId, long channelId)
        {
            return _dbContext.GetAsync<Subscription>(x => x.UserId == userId && x.ChannelId == channelId);
        }

        public Task<IEnumerable<Subscription>> GetByUser(long userId)
        {
            return _dbContext.GetManyAsync<Subscription>(x => x.UserId == userId);
        }

        public Task<Subscription> Update(Subscription subscription)
        {
            return _dbContext.UpdateAsync(subscription, true);
        }
    }
}