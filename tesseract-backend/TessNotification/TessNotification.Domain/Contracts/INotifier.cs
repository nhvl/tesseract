﻿using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Contracts
{
    public interface INotifier
    {
        bool CanNotify(NotificationJob userNotifications);
        Task<UserNotificationState> SendNotificationsAsync(NotificationJob userNotifications);
    }
}
