﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Contracts
{
    public interface ISubscriptionService
    {
        Task<IEnumerable<Channel>> GetChannels();
        Task<IEnumerable<Subscription>> GetSubscriptions(long UserId);
        Task DeleteSubscription(long userId, long channelId);
        Task<Subscription> CreateSubscription(long userId, long channelId, SendTypeEnum sendType, System.TimeSpan sendTime, byte weeklyDay);
        Task<Subscription> UpsertSubscription(long userId, long channelId, SendTypeEnum sendType, System.TimeSpan sendTime, byte weeklyDay);
    }
}
