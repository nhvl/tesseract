﻿using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Contracts
{
    public interface IPublisher
    {
        Task Pusblish(string notificationName, NotificationData data = null, params long[] userIds);
    }
}
