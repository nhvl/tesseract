﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Contracts
{
    public interface IDistributer
    {
        Task<IEnumerable<NotificationJob>> Distribute(NotificationData notification, long[] userIds);
    }

}
