﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Contracts
{
    public interface IDispatcher
    {
        Task<IEnumerable<INotifier>> Dispatch(NotificationJob job);
    }

}
