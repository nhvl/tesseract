﻿using System.Threading.Tasks;

namespace TessNotification.Domain.Contracts
{
    public interface IBackgroundWorker
    {
        Task Run();
    }

}
