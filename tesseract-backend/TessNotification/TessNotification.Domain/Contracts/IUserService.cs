﻿using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Contracts
{
    public interface IUserService
    {
        Task<User> Create(long userId, string email);
        Task Delete(long userId);
        Task<User> GetUser(long userId);
        Task<User> Update(long userId, string email);
        Task<User> Upsert(long userId, string email);
    }
}
