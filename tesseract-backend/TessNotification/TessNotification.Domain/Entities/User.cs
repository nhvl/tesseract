﻿using System;

namespace TessNotification.Domain.Entities
{
    public class User : BaseEntity
    {
        public User(long externalId, string email)
        {
            ExternalId = externalId;
            Email = email;
        }

        public long UserId { get; set; }
        public string Email { get; set; }
        public long ExternalId { get; set; }

        public override bool Equals(object obj)  => obj is User user &&
                   UserId == user.UserId &&
                   Email == user.Email && 
                   ExternalId == user.ExternalId;

        public override int GetHashCode() => HashCode.Combine(UserId, Email);
    }
}
