﻿namespace TessNotification.Domain.Entities
{
    public enum SendTypeEnum
    {
        DontSend = 1,
        Imediately = 2,
        Daily = 3,
        Weekly = 4,
    }
}
