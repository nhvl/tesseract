﻿using System;
using System.Collections.Generic;

namespace TessNotification.Domain.Entities
{
    public class Subscription : BaseEntity
    {
        private const byte Sunday = 0b01000000;
        private const byte Monday = 0b00100000;
        private const byte Tuesday = 0b00010000;
        private const byte Wednesday = 0b00001000;
        private const byte Thursday = 0b00000100;
        private const byte Friday = 0b00000010;
        private const byte Saturday = 0b00000001;

        public Subscription(long userId, long channelId, SendTypeEnum sendType, TimeSpan sendTime, byte weeklyDay)
        {
            UserId = userId;
            ChannelId = channelId;
            SendType = sendType;
            SendTime = sendTime;
            WeeklyDay = weeklyDay;
        }

        public long UserId { get; }
        public long ChannelId { get; }
        public SendTypeEnum SendType { get; set; }
        public TimeSpan SendTime { get; set; }
        public byte WeeklyDay { get; set; }

        public DateTime? GetNextDispatchTime(DateTime? now = null)
        {
            var nowTime = now ?? DateTime.Now;
            switch (this.SendType)
            {
                case SendTypeEnum.DontSend: return null;
                case SendTypeEnum.Imediately: return DateTime.MinValue;
                case SendTypeEnum.Daily: return GetNextDate(nowTime, this.SendTime);
                case SendTypeEnum.Weekly: return GetNextDate(nowTime, ByteToDayOfWeeks(this.WeeklyDay), this.SendTime);
                default: return null;
            }
        }

        static List<DayOfWeek> ByteToDayOfWeeks(byte days)
        {
            var list = new List<DayOfWeek>(7);
            if ((days & Sunday) == Sunday) list.Add(DayOfWeek.Sunday);
            if ((days & Monday) == Monday) list.Add(DayOfWeek.Monday);
            if ((days & Tuesday) == Tuesday) list.Add(DayOfWeek.Tuesday);
            if ((days & Wednesday) == Wednesday) list.Add(DayOfWeek.Wednesday);
            if ((days & Thursday) == Thursday) list.Add(DayOfWeek.Thursday);
            if ((days & Friday) == Friday) list.Add(DayOfWeek.Friday);
            if ((days & Saturday) == Saturday) list.Add(DayOfWeek.Saturday);
            return list;
        }

        static DateTime GetNextDate(DateTime now, TimeSpan time)
        {
            var timeOfDay = now.TimeOfDay;
            return timeOfDay < time
                ? now.Date + time
                : now.Date + TimeSpan.FromDays(1) + time;
        }

        static DateTime? GetNextDate(DateTime now, ICollection<DayOfWeek> days, TimeSpan time)
        {
            if (days.Count < 1) return null;
            var sendTime = GetNextDate(now, time);
            while (!days.Contains(sendTime.DayOfWeek))
            {
                sendTime = sendTime.AddDays(1);
            }

            return sendTime;
        }
    }
}
