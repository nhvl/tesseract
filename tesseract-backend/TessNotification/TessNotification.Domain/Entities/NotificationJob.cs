﻿using System;


namespace TessNotification.Domain.Entities
{
    public class NotificationJob : BaseEntity
    {
        public NotificationJob()
        {
        }

        public NotificationJob(long userId, long channelId, DateTime dispatchTime, NotificationData data)
        {
            UserId = userId;
            ChannelId = channelId;
            DispatchTime = dispatchTime;
            Data = data;
        }

        public long NotificationJobId { get; set; }
        public long UserId { get; set; }
        public long ChannelId { get; set; }
        public DateTime DispatchTime { get; set; }
        public NotificationData Data { get; set; }
        public UserNotificationState State { get; set; }
    }
}
