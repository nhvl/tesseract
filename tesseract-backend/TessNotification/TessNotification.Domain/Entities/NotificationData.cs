﻿using System.Collections.Generic;

namespace TessNotification.Domain.Entities
{
    public class NotificationData
    {
        public NotificationData(string notificationName, string message, Dictionary<string, object> properties = null)
        {
            NotificationName = notificationName;
            Message = message;
            Properties = properties;
        }

        public string NotificationName { get; }
        public string Message { get; }
        public Dictionary<string, object> Properties{ get; }

      
    }
}
