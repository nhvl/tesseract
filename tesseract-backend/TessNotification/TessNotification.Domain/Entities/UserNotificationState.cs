﻿namespace TessNotification.Domain.Entities
{
    public enum UserNotificationState
    {
        Pending,
        Assigned,
        Sent,
        Read,
        Error
    }
}
