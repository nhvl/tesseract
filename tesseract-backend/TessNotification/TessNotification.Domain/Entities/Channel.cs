﻿namespace TessNotification.Domain.Entities
{
    public class Channel
    {
        public Channel(long channelId, string name, string desc)
        {
            ChannelId = channelId;
            Name = name;
            Desc = desc;
        }

        public long ChannelId { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
    }
}
