﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Repo
{
    public interface ISubscriptionRepo
    {
        Task<Subscription> Create(Subscription subscription);
        Task<IEnumerable<Subscription>> GetByUser(long UserId);
        Task<Subscription> Get(long userId, long channelId);
        Task<Subscription> Update(Subscription subscription);
        Task<Subscription> Delete(long userId, long channelId);
    }
}
