﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Repo
{
    public interface IDispatchJobRepo
    {
        Task<NotificationJob> Add(NotificationJob job);
        Task<IEnumerable<NotificationJob>> GetReadyJobs();
        Task Remove(NotificationJob job);
        Task<NotificationJob> GetJob(long jobId);
    }
}
