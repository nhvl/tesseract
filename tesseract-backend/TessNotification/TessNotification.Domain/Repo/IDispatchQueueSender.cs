﻿using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Repo
{
    public interface IDispatchQueueSender
    {
        void Send(long jobId);
    }
}
