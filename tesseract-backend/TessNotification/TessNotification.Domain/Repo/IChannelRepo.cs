﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Repo
{
    public interface IChannelRepo
    {
        Task<IEnumerable<Channel>> GetChannels();
    }
}
