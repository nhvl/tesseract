﻿using System;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Repo
{
    public interface IDispatchQueueReceiver
    {
        Task OnReceive(Func<long, Task> job, TimeSpan timeSpan);
        Task OnReceiverNotification(Func<NotificationData, long[], Task> notification, TimeSpan timeSpan);
    }
}
