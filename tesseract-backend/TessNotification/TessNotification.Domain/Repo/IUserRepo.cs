﻿using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Domain.Repo
{
    public interface IUserRepo
    {
        Task<User> Create(User user);
        Task<User> Get(long userId);
        Task<User> Update(User user);
        Task<User> Delete(long userId);
    }
}
