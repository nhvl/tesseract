﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace TessNotification.Common
{
    public static class EnumerableAsyncExtension
    {
        public static async Task<int> CountAsync<T>(this Task<IEnumerable<T>> self) => (await self).Count();
        public static async Task<T> FirstOrDefaultAsync<T>(this Task<IEnumerable<T>> self) => (await self).FirstOrDefault();
        public static async Task<IEnumerable<U>> SelectManyAsync<T, U>(this IEnumerable<T> self, Func<T, Task<IEnumerable<U>>> selector) => (await Task.WhenAll(self.Select(selector))).SelectMany(x => x);
    }
}
