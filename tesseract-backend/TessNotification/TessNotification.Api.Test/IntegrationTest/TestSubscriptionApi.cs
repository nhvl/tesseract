using Bogus;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TessNotification.Api.Models;
using TessNotification.Domain.Entities;
using Xunit;

namespace TessNotification.Api.Test.IntegrationTest
{
    public class TestSubscriptionApi : IClassFixture<WebApplicationFactory<Startup>>, IDisposable
    {
        private const string BaseUri = "/api/v1/subscription";
        private readonly HttpClient _client;
        private readonly Faker<SubscriptionModifyRequest> _faker = new Faker<SubscriptionModifyRequest>()
                                                                    .RuleFor(x => x.SendType, f => f.PickRandom<SendTypeEnum>())
                                                                    .RuleFor(x => x.WeeklyDay, f => f.Random.Byte())
                                                                    .RuleFor(x => x.SendTime, f => new TimeSpan(f.Random.Int(0,23), f.Random.Int(0, 59), f.Random.Int(0, 59)).Ticks);
        private IEnumerable<Subscription> CreatedSubscriptions;

        public TestSubscriptionApi(WebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            if (CreatedSubscriptions == null)
            {
                return;
            }

            foreach (var subscription in CreatedSubscriptions)
            {
                DeleteSubscription(subscription).Wait();
            }
        }

        [Fact]
        public async Task<Channel[]> CanGetAccount()
        {
            var response = await _client.GetAsync(BaseUri + "/channel");
            var channels = await response.JsonParse<Channel[]>();
            Assert.NotEmpty(channels);

            return channels;
        }

        [Fact]
        public async Task<IEnumerable<Subscription>> CanCreateSubscription()
        {
            var userId = new Randomizer().Long(min: 1001, max: 2001);
            var channels = await CanGetAccount();
            var createdSubscriptionsList = new List<Subscription>();
            foreach (var channel in channels)
            {
                var request = _faker.Generate();
                var subscription = await _client.PostJsonAsync<Subscription>($"{BaseUri}/{userId}/channel/{channel.ChannelId}", request);

                Assert.Equal(userId, subscription.UserId);
                Assert.Equal(request.SendType, subscription.SendType);
                Assert.Equal(request.WeeklyDay, subscription.WeeklyDay);
                Assert.Equal(request.SendTime, subscription.SendTime.Ticks);

                createdSubscriptionsList.Add(subscription);
            }

            CreatedSubscriptions = createdSubscriptionsList.ToArray();
            return CreatedSubscriptions;
        }

        [Fact]
        public async Task CanGetSubscription()
        {
            var subs = await CanCreateSubscription();
            var userid = subs.First().UserId;

            var response = await _client.GetAsync($"{BaseUri}/{userid}");
            var readSubs = await response.JsonParse<Subscription[]>();
            Assert.Equal(subs.Count(), readSubs.Length);
        }

        [Fact]
        public async Task CanPutSubscription()
        {
            var sub = (await CanCreateSubscription()).First();
            var updatedRequest = _faker.Generate();

            var subscription = await _client.PutJsonAsync<Subscription>($"{BaseUri}/{sub.UserId}/channel/{sub.ChannelId}", updatedRequest);

            Assert.Equal(updatedRequest.SendType, subscription.SendType);
            Assert.Equal(updatedRequest.WeeklyDay, subscription.WeeklyDay);
            Assert.Equal(updatedRequest.SendTime, subscription.SendTime.Ticks);
        }

        private async Task DeleteSubscription(Subscription sups)
        {
            var response = await _client.DeleteAsync($"{BaseUri}/{sups.UserId}/channel/{sups.ChannelId}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);
        }
    }
}
