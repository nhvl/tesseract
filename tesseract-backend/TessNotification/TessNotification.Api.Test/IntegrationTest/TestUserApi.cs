using Bogus;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TessNotification.Api.Models;
using TessNotification.Domain.Entities;
using Xunit;

namespace TessNotification.Api.Test.IntegrationTest
{

    public static class HttpResponseMessageExtension
    {
        public static async Task<T> JsonParse<T>(this HttpResponseMessage self)
        {
            Assert.True(self.StatusCode == System.Net.HttpStatusCode.OK);
            var data = await self.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(data);
        }
    }

    public class TestUserApi : IClassFixture<WebApplicationFactory<Startup>>, IDisposable
    {
        private const string BaseUri = "/api/v1/user";
        private readonly HttpClient _client;
        private readonly Faker<CreateUserRequest> _faker = new Faker<CreateUserRequest>()
                                                    .RuleFor(x => x.UserId, f => f.Random.Long(min: 999))
                                                    .RuleFor(x => x.Email, f => f.Internet.Email());
        private IEnumerable<User> CreatedUsers;

        public TestUserApi(WebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            if (CreatedUsers != null)
            {
                Task.WaitAll(CreatedUsers.Select(DeleteUser).ToArray());
            }
        }
        
        private async Task<User> CanSendPostRequest(CreateUserRequest request)
        {
            var user = await _client.PostJsonAsync<User>($"{BaseUri}", request);
            AssertAreEqual(request, user);

            return user;
        }

        private static void AssertAreEqual(CreateUserRequest request, User user)
        {
            Assert.Equal(request.UserId, user.ExternalId);
            Assert.Equal(request.Email, user.Email);
        }

        [Fact]
        public async Task<IEnumerable<User>> CanCreateUser()
        {
            var users = new List<User>();
            var userIdSeed = new Randomizer().Long(min: 100000, max: 100100);

            this.CreatedUsers = await Task.WhenAll( _faker.Generate(10).Select(async x => await CanSendPostRequest(x)));
            return CreatedUsers.ToArray();
        }

        [Fact]
        public async Task CanGetUser()
        {
            var users = await CanCreateUser();
            foreach (var user in users)
            {
                var response = await _client.GetAsync($"{BaseUri}/{user.UserId}");
                var downloadedUser = await response.JsonParse<User>();

                Assert.Equal(user, downloadedUser);
            }
        }

        [Fact]
        public async Task CanPutUser()
        {
            var users = await CanCreateUser();
            foreach (var user in users)
            {
                EditUserRequest newUpdateRequest= _faker.Generate();
                var updatedUser = await _client.PutJsonAsync<User>($"{BaseUri}/{user.UserId}", newUpdateRequest);
                Assert.Equal(newUpdateRequest.Email, updatedUser.Email);
            }
        }

        private async Task DeleteUser(User user)
        {
            var response = await _client.DeleteAsync($"{BaseUri}/{user.UserId}");
            Assert.True(response.StatusCode == System.Net.HttpStatusCode.OK);
        }
    }
}
