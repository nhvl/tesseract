﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TessNotification.Api.Test.IntegrationTest
{
    public static class HttpClientExtension
    {
        public static  Task<T> PutJsonAsync<T>(this HttpClient self, string url, object request) where T : class => SendJsonAsync<T>(self, HttpMethod.Put, url, request);
        public static  Task<T> PostJsonAsync<T>(this HttpClient self, string url, object request) where T : class => SendJsonAsync<T>(self, HttpMethod.Post, url, request);

        private static async Task<T> SendJsonAsync<T>(this HttpClient self, HttpMethod method, string url, object request) where T : class
        {
            var data = JsonConvert.SerializeObject(request);
            var httpRequest = new HttpRequestMessage(method, url);
            httpRequest.Content = new StringContent(data, Encoding.UTF8, "application/json");
            var response = await self.SendAsync(httpRequest);
            return await response.JsonParse<T>();
        }
    }
}
