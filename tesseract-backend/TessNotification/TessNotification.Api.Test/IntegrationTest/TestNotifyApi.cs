using Bogus;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TessNotification.Api.Models;
using Xunit;

namespace TessNotification.Api.Test.IntegrationTest
{
    public class TestNotifyApi : IClassFixture<WebApplicationFactory<Startup>>, IDisposable
    {
        private const string BaseUri = "/api/v1/notification";
        private readonly HttpClient _client;
        private readonly Faker<NotifyRequest> _faker = new Faker<NotifyRequest>()
                                                        .RuleFor(x => x.Name, f => f.System.Exception().ToString())
                                                        .RuleFor(x => x.Message, f => f.System.Exception().Message);
        private readonly TestSubscriptionApi _testSubsApi;

        public TestNotifyApi(WebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
            _testSubsApi = new TestSubscriptionApi(factory);
        }

        [Fact]
        public async Task CanNotify()
        {
            var subs = await _testSubsApi.CanCreateSubscription();

            var tryCount = 60 * 2 / 5;
            while(tryCount-- > 0)
            {
                var request = _faker.Generate();
                request.UserIds = subs.Select(x => x.UserId).Distinct().ToArray();

                var data = JsonConvert.SerializeObject(request);
                var content = new StringContent(data, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync(BaseUri, content);
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
                await Task.Delay(1000 * 5);
            }
        }

        public void Dispose()
        {
            _testSubsApi.Dispose();
        }
    }
}
