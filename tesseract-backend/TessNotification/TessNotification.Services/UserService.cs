﻿using System.Threading.Tasks;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;

namespace TessNotification.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepo _userRepo;

        public UserService(IUserRepo userRepo)
        {
            _userRepo = userRepo;
        }

        public Task<User> Create(long userId, string email)
        {
           return _userRepo.Create(new User(userId, email));
        }

        public Task Delete(long userId)
        {
            return _userRepo.Delete(userId);
        }

        public Task<User> GetUser(long userId)
        {
            return _userRepo.Get(userId);
        }

        public async Task<User> Update(long userId, string email)
        {
            var user = await _userRepo.Get(userId) ?? throw new ResourceNotFoundException();

            user.Email = email;
            return await _userRepo.Update(user);
        }

        public async Task<User> Upsert(long userId, string email)
        {
            if (await GetUser(userId) != null)
            {
                return await this.Update(userId, email);
            }
            else
            {
                return await this.Create(userId, email);
            }
        }
    }
}
