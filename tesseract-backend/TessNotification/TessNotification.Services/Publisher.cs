﻿using System;
using System.Threading.Tasks;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;


namespace TessNotification.Services
{
    public class Publisher : IPublisher
    {
        private readonly IDistributer _distributer;
        private readonly IDispatchQueueSender _sender;
        private readonly IDispatchJobRepo _dispatchRepo;

        public Publisher(IDistributer distributer, IDispatchQueueSender sender, IDispatchJobRepo dispatchRepo)
        {
            _distributer = distributer;
            _sender = sender;
            _dispatchRepo = dispatchRepo;
        }

        public async Task Pusblish(string notificationName, NotificationData data = null, params long[] userIds)
        {
            var now = DateTime.UtcNow;
            var jobs = await _distributer.Distribute(data, userIds);
            foreach (var job in jobs)
            {
                var addJob = await _dispatchRepo.Add(job);

                if (addJob.DispatchTime < now)
                {
                    _sender.Send(addJob.NotificationJobId);
                }

            }
        }
    }
}
