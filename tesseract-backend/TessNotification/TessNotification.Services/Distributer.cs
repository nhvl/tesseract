﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TessNotification.Common;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Entities;


namespace TessNotification.Services
{
    public class Distributer : IDistributer
    {
        private readonly ILogger<Distributer> _logger;
        private readonly ISubscriptionService _subscriptionService;

        public Distributer(ILogger<Distributer> logger, ISubscriptionService subscriptionService)
        {
            _logger = logger;
            _subscriptionService = subscriptionService;
        }

        public async Task<IEnumerable<NotificationJob>> Distribute(NotificationData notification, long[] userIds)
        {
            var subscriptions = await userIds.SelectManyAsync(userId => _subscriptionService.GetSubscriptions(userId));
            var jobs = subscriptions.Where(subs => subs.GetNextDispatchTime() != null)
                                                        .Select(subs => new NotificationJob(subs.UserId, subs.ChannelId, subs.GetNextDispatchTime().Value, notification))
                                                        .ToArray();
            _logger.LogTrace($"Distributed notification {notification.NotificationName} with user IDs {string.Join(',', userIds)} to {jobs.Count()} jobs.");
            return jobs;
        }
    }
}
