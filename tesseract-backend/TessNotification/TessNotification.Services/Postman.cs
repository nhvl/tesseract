﻿using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;


namespace TessNotification.Services
{
    public class Postman : IPostman
    {
        private readonly IDispatcher _dispatcher;
        private readonly IDispatchQueueReceiver _receiver;
        private readonly IDispatchJobRepo _jobRepo;
        private readonly ILogger<Postman> _logger;

        public Postman(IDispatcher dispatcher, IDispatchQueueReceiver receiver, IDispatchJobRepo jobRepo, ILogger<Postman> logger)
        {
            _dispatcher = dispatcher;
            _receiver = receiver;
            _jobRepo = jobRepo;
            _logger = logger;
        }

        public async Task Run()
        {
            await _receiver.OnReceive(async jobId =>
            {
                var job = await _jobRepo.GetJob(jobId);
                if (job == null)
                {
                    _logger.LogWarning($"Can not find job {jobId}");
                    return;
                }

                foreach (var notifier in await _dispatcher.Dispatch(job))
                {
                    await notifier.SendNotificationsAsync(job);
                }
            }, TimeSpan.FromMinutes(1));
        }
    }
}
