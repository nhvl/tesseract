﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Entities;


namespace TessNotification.Services
{
    public class Dispatcher : IDispatcher
    {
        private readonly IEnumerable<INotifier> _notifiers;

        public Dispatcher(IEnumerable<INotifier> notifiers)
        {
            _notifiers = notifiers;
        }

        public Task<IEnumerable<INotifier>> Dispatch(NotificationJob job)
        {
            IEnumerable<INotifier> results = _notifiers.Where(notifier => notifier.CanNotify(job));
            return Task.FromResult(results);
        }
    }
}
