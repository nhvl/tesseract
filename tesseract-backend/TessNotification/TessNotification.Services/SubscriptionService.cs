﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;



namespace TessNotification.Services
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly ISubscriptionRepo _subsRepo;
        private readonly IChannelRepo _channelRepo;

        public SubscriptionService(ISubscriptionRepo subsRepo, IChannelRepo channelRepo)
        {
            _subsRepo = subsRepo;
            _channelRepo = channelRepo;
        }

        public Task<Subscription> CreateSubscription(long userId, long channelId, SendTypeEnum sendType, TimeSpan sendTime, byte weeklyDay)
        {
            // TODO: check duplicated pair(userId, channelId)
            return _subsRepo.Create(new Subscription(userId, channelId, sendType, sendTime, weeklyDay));
        }

        public Task DeleteSubscription(long userId, long channelId)
        {
            return _subsRepo.Delete(userId, channelId);
        }

        public Task<IEnumerable<Channel>> GetChannels()
        {
            return _channelRepo.GetChannels();
        }

        public Task<IEnumerable<Subscription>> GetSubscriptions(long UserId)
        {
            return _subsRepo.GetByUser(UserId);
        }

        public async Task<Subscription> UpsertSubscription(long userId, long channelId, SendTypeEnum sendType, TimeSpan sendTime, byte weeklyDay)
        {
            var subs = await _subsRepo.Get(userId, channelId);
            if (subs == null)
            {
                return await _subsRepo.Create(new Subscription(userId, channelId, sendType, sendTime, weeklyDay));
            }

            subs.SendType = sendType;
            subs.SendTime = sendTime;
            subs.WeeklyDay = weeklyDay;
            return await _subsRepo.Update(subs);
        }
    }
}
