﻿using System;

namespace TessNotification.Services
{
    public class ResourceNotFoundException : Exception { }
}