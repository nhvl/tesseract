﻿using System;
using System.Threading.Tasks;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Repo;


namespace TessNotification.Services
{
    public class ScheduleRunner : IScheduleRunner, IBackgroundWorker
    {
        private readonly IDispatchJobRepo _dispatchRepo;
        private readonly IDispatchQueueSender _sender;
        private readonly IDispatchQueueReceiver _receiver;
        private readonly IPublisher _publisher;

        public ScheduleRunner(IDispatchJobRepo dispatchRepo, IDispatchQueueSender sender, IDispatchQueueReceiver receiver, IPublisher publisher)
        {
            _dispatchRepo = dispatchRepo;
            _sender = sender;
            _receiver = receiver;
            _publisher = publisher;
        }

        public async Task Run()
        {
            foreach (var job in await _dispatchRepo.GetReadyJobs())
            {
                await _dispatchRepo.Remove(job);
                _sender.Send(job.NotificationJobId);
            }

            await _receiver.OnReceiverNotification(async (notification, userIds) =>
            {
                await _publisher.Pusblish(notification.NotificationName, notification, userIds);
                return;
            }, TimeSpan.FromMinutes(1));
        }
    }
}
