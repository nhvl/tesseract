﻿using Microsoft.Extensions.Logging;
using RazorLight;
using System.Threading.Tasks;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Entities;

namespace TessNotification.Services
{
    public class EmailNotifer : INotifier
    {
        private const string MockTemplate = @" < p >
                                                    @Model.UserId updated @Model.Title
                                                </p>

                                                <br />

                                                <br />
                                                ";
        public bool CanNotify(NotificationJob userNotifications) => userNotifications.ChannelId == 1;

        public async Task<UserNotificationState> SendNotificationsAsync(NotificationJob userNotifications)
        {
            var engine = new RazorLightEngineBuilder()
              .UseMemoryCachingProvider()
              .Build();

            string template = MockTemplate;
            userNotifications.Data.Properties["UserId"] = userNotifications.UserId;
            var model = new { userNotifications.UserId, Title = userNotifications.Data.Properties["Title"] };

            var result = await engine.CompileRenderAsync("templatekey", template, model);
            return string.IsNullOrEmpty(result) ? UserNotificationState.Error : UserNotificationState.Sent;
        }
    }

    public class LoggingNotifer : INotifier
    {
        private readonly ILogger<LoggingNotifer> _logger;

        public LoggingNotifer(ILogger<LoggingNotifer> logger)
        {
            _logger = logger;
        }

        public bool CanNotify(NotificationJob userNotifications)
        {
            return true;
        }

        public Task<UserNotificationState> SendNotificationsAsync(NotificationJob userNotification)
        {
            var message = nameof(LoggingNotifer) + userNotification.Data.NotificationName + userNotification.Data.Message;
            _logger.LogInformation(message);
            return Task.FromResult(UserNotificationState.Sent);
        }
    }
}
