﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TessNotification.Api.Models;
using TessNotification.Domain.Contracts;

namespace TessNotification.Api.Controllers
{
    [Route("api/v{v:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        [HttpPost()]
        public async Task<IActionResult> Post(int userId, [FromBody] NotifyRequest request, [FromServices] IPublisher publisher)
        {
            await publisher.Pusblish(request.Name, null, request.UserIds);
            return Ok();
        }
    }
}