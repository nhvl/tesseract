﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TessNotification.Api.Models;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;

namespace TessNotification.Api.Controllers
{
    [Route("api/v{v:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> Get(long userId)
        {
            var user = await _userService.GetUser(userId);
            return Ok(user);
        }

        [HttpPut("{userId}")]
        public async Task<IActionResult> Put(long userId, [FromBody]EditUserRequest request)
        {
            var user = await _userService.Update(userId, request.Email);
            return Ok(user);
        }

        [HttpDelete("{userId}")]
        public async Task<IActionResult> Delete(long userId)
        {
            await _userService.Delete(userId);
            return Ok();
        }

        [HttpPost()]
        public async Task<IActionResult> Post(long userId, [FromBody]CreateUserRequest request)
        {
            var user = await _userService.Create(request.UserId, request.Email);
            return Ok(user);
        }
    }
}