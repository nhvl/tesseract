﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TessNotification.Api.Models;
using TessNotification.Domain.Contracts;
using TessNotification.Services;

namespace TessNotification.Api.Controllers
{
    [Route("api/v{v:apiVersion}/[controller]")]
    [ApiVersion("1")]
    [ApiController]
    public class SubscriptionController : ControllerBase
    {
        private readonly ISubscriptionService _subsService;

        public SubscriptionController(ISubscriptionService subsService) => _subsService = subsService;

        [HttpGet("channel")]
        public async Task<IActionResult> GetChannels() => Ok(await _subsService.GetChannels());

        [HttpGet("{userId}")]
        public async Task<IActionResult> Get(long userId)
        {
            var subscription = await _subsService.GetSubscriptions(userId);
            return subscription != null ? Ok(new { data = subscription } ) : NotFound() as IActionResult;
        }

        [HttpPut("{userId}/channel/{channelId}")]
        public async Task<IActionResult> Put(long userId, long channelId, [FromBody]SubscriptionModifyRequest request)
        {
            var subscription = await _subsService.UpsertSubscription(userId, channelId, request.SendType, new TimeSpan(request.SendTime), request.WeeklyDay);
            return Ok(subscription);
        }

        [HttpDelete("{userId}/channel/{channelId}")]
        public async Task<IActionResult> Delete(long userId, long channelId)
        {
            await _subsService.DeleteSubscription(userId, channelId);
            return Ok();
        }

        [HttpPost("{userId}/channel/{channelId}")]
        public async Task<IActionResult> Post(long userId, long channelId, [FromBody]SubscriptionModifyRequest request)
        {
            var subscription = await _subsService.CreateSubscription(userId, channelId, request.SendType, new TimeSpan(request.SendTime), request.WeeklyDay);
            return Ok(subscription);
        }
    }
}