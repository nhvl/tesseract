﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;
using System;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Repo;
using TessNotification.Infracstructure.Data.Ef;
using TessNotification.Infracstructure.MessageQueue.Memory;
using TessNotification.Infracstructure.MessageQueue.RabbitMq;
using TessNotification.Services;

namespace TessNotification.Api
{
    public static class NotificationServicesExtensions
    {
        public static void AddNotifications(this IServiceCollection services, IConfiguration config)
        {

            services.AddTransient<ISubscriptionService, SubscriptionService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IPublisher, Publisher>();
            services.AddTransient<IDistributer, Distributer>();
            services.AddTransient<IDispatcher, Dispatcher>();
            services.AddTransient<INotifier, EmailNotifer>();
            services.AddTransient<IPostman, Postman>();
            services.AddTransient<IScheduleRunner, ScheduleRunner>();

            
            services.AddTransient<IDispatchJobRepo, DbDispatchJobRepo>();
            services.AddTransient<ISubscriptionRepo, DbSubscriptionRepo>();
            services.AddTransient<IChannelRepo, DbChannelRepo>();
            services.AddTransient<IUserRepo, DbUserRepo>();

            var rabbitMqUrl = config.GetSection("Services:RabbitMqUrl").Get<string>();
            var rabbitMqUri = Uri.TryCreate(rabbitMqUrl, UriKind.Absolute, out var val) ? val : null;
            if (rabbitMqUri != null){
                var factory = new ConnectionFactory() { Uri = rabbitMqUri };
                services.AddSingleton<IConnection>(factory.CreateConnection());
                services.AddScoped<IDispatchQueueSender, RabbitMqDispatchQueueSender>();
                services.AddScoped<IDispatchQueueReceiver, RabbitMqDispatchQueueReceiver>();
            } 
            else 
            {
                services.AddScoped<IDispatchQueueSender, MemoryDispatchQueueSender>();
                services.AddScoped<IDispatchQueueReceiver, MemoryDispatchQueueReceiver>();
            }
            
        }
    }
}
