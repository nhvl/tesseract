﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TessNotification.Domain.Entities;

namespace TessNotification.Api.Models
{
    public class SubscriptionModifyRequest 
    {
        public SendTypeEnum SendType { get; set; }
        public long SendTime { get; set; }
        public byte WeeklyDay { get; set; }
    }
}