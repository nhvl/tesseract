﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TessNotification.Api.Models;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Entities;
using TessNotification.Domain.Repo;

namespace TessNotification.Api.Models
{
    public class CreateUserRequest: EditUserRequest
    {
        public long UserId { get; set; }
    }
}
