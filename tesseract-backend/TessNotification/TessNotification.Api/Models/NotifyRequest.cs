﻿namespace TessNotification.Api.Models
{
    public class NotifyRequest
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public long[] UserIds { get; set; }
    }
}