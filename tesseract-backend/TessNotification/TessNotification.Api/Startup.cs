﻿using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using TessNotification.Domain.Contracts;
using TessNotification.Domain.Repo;
using TessNotification.Infracstructure.Data.Ef;
using TessNotification.Services;

namespace TessNotification.Api
{
    public class Startup
    {
        private const string DefaultConnectionString = "DefaultConnection";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MainDbContext>(options =>
            {
                var connectionString = Configuration.GetConnectionString(DefaultConnectionString);
                if (!string.IsNullOrEmpty(connectionString))
                {
                    options.UseSqlServer(connectionString);
                }
                else
                {
                    options.UseInMemoryDatabase("TessNotificationTesting");
                }
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(Properties.Resources.AppVersion, new Info { Title = $"{Properties.Resources.AppName}", Version = Properties.Resources.AppVersion });
            });

            services.AddApiVersioning();
            services.AddNotifications(Configuration);
            services.AddHangfire(configuration => configuration
                                                    .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                                                    .UseSimpleAssemblyNameTypeSerializer()
                                                    .UseRecommendedSerializerSettings()
                                                    .UseMemoryStorage()
                                                    );
            services.AddHealthChecks();

            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(Configuration, "Services:Serilog").CreateLogger();
        }

        public void Configure(IApplicationBuilder app,
                              IHostingEnvironment env,
                              IRecurringJobManager recurringJobManager,
                              MainDbContext dbContext)
        {
            if (!dbContext.Database.IsInMemory())
            {
                dbContext.Database.Migrate();
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"/swagger/{Properties.Resources.AppVersion}/swagger.json", $"{Properties.Resources.AppName} API {Properties.Resources.AppVersion}");
                });
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseHealthChecks("/hc");
            app.UseMvc();

            app.UseHangfireServer(new BackgroundJobServerOptions { WorkerCount = 2 });
            app.UseHangfireDashboard();
            recurringJobManager.AddOrUpdate<IScheduleRunner>(nameof(IScheduleRunner), x => x.Run(), "* * * * *");
            recurringJobManager.AddOrUpdate<IPostman>(nameof(IPostman), x => x.Run(), "* * * * *");
        }
    }
}
