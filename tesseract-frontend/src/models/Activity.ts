import {observable, action, computed} from "mobx";

import { DefaultId, DbIdentity } from "./types";
import {Class} from "./Class";

import { aFetch } from "../services/api/fetch";
import { GeneralDto, parseGeneralViewModel } from "./GeneralViewModel";
import { map, groupBy } from "lodash-es";

export enum ActivityType {
    Activity   = 1,
    Assignment = 2,
    Assessment = 3,
    Discussion = 4,
}
export enum PublishStatusEnum {
    Published = 1,
    Pending,
    Unpublished,
}

export class Activity {

    /* */       activityId  : DbIdentity  = DefaultId;
    /* */       classId     : DbIdentity  = DefaultId;

    @observable type        : ActivityType = ActivityType.Activity;
    @observable title       : string  = "";
    @observable description : string  = "";

    @observable dateDue     : number|undefined  = undefined;
    @observable dateAssigned: number|undefined  = undefined;
    @observable dateCutoff  : number|undefined  = undefined;

    @observable publishStartDate: number|undefined  = undefined;
    @observable publishEndDate  : number|undefined  = undefined;

    @observable categoryId? : DbIdentity = undefined;

    @observable color       : string  = "#ffffff"  ;
    @observable isGraded    : boolean = true       ;
    @observable maxScore    : number  = 10         ;
    @observable weight      : number  = 1          ;
    @observable isCredit    : boolean = false      ;

    @observable parentActivityId?: DbIdentity = undefined;
    @observable sortIndex   : number = -1;

    @observable dateCreated : number  = Date.now();

    @observable.ref class   : Class = new Class();

    constructor(data?:any) {
        if (data != null) {
            Object.assign(this, data);
        }
        if(this.color == null || this.color == "") this.color = "#ffffff";
    }

    @action set_classId          = (v : number      ) => { this.classId          = v; }
    @action set_activityId       = (v : number      ) => { this.activityId       = v; }
    @action set_type             = (v : ActivityType) => { this.type             = v; }
    @action set_title            = (v : string      ) => { this.title            = v; }
    @action set_description      = (v : string      ) => { this.description      = v; }
    @action set_dateDue          = (v?: number      ) => { this.dateDue          = v; }
    @action set_dateAssigned     = (v?: number      ) => { this.dateAssigned     = v; }
    @action set_dateCutoff       = (v?: number      ) => { this.dateCutoff       = v; }
    @action set_publishStartDate = (v?: number      ) => { this.publishStartDate = v; }
    @action set_publishEndDate   = (v?: number      ) => { this.publishEndDate   = v; }
    @action set_categoryId       = (v?: DbIdentity  ) => { this.categoryId       = v; }
    @action set_color            = (v : string      ) => { this.color            = v; }
    @action set_isGraded         = (v : boolean     ) => { this.isGraded         = v; }
    @action set_maxScore         = (v : number      ) => { this.maxScore         = v; }
    @action set_weight           = (v : number      ) => { this.weight           = v; }
    @action set_isCredit         = (v : boolean     ) => { this.isCredit         = v; }
    @action set_isExclude = (v: boolean) => {
        if (v) {
            this.weight = 0;
        } else {
            if (this.weight <= 0) this.weight = 1.;
        }
    }

    @computed get params() { return ({activityId:String(this.activityId), classId:String(this.classId)}) }
    @computed get publishStatus(){
        const now =  Date.now();
        if (this.publishStartDate == null) return PublishStatusEnum.Unpublished;
        if (this.publishStartDate > now) return PublishStatusEnum.Pending;

        if (this.publishEndDate != null && this.publishEndDate < now) return PublishStatusEnum.Unpublished;
        return PublishStatusEnum.Published;
    };

    toJS() {
        return ({
            activityId      : this.activityId,
            type            : this.type,
            title           : this.title,
            description     : this.description,
            dateAssigned    : this.dateAssigned,
            dateDue         : this.dateDue,
            dateCutoff      : this.dateCutoff,
            categoryId      : this.categoryId,
            color           : this.color,
            isGraded        : this.isGraded,
            isCredit        : this.isCredit,
            maxScore        : this.maxScore,
            weight          : this.weight,
            parentActivityId: this.parentActivityId,
            sortIndex       : this.sortIndex,
        });
    }

    async save(schoolId:DbIdentity, facultyId:DbIdentity) {
        if (this.activityId < 1) {
            const [err, data] = await aFetch<{}>("POST", `/faculty/${facultyId}/school/${schoolId}/class/${this.classId}/activity`, this.toJS());
            return [err, (err ? undefined : new Activity(data))!] as const;
        }

        const [err, data] = await aFetch<{}>("PUT", `/faculty/${facultyId}/school/${schoolId}/activity/${this.activityId}`, this.toJS());
        return [err, (err ? undefined : new Activity(data))!] as const;
    }

    static async fetchActivityAsFaculty({schoolId, facultyId, activityId}:{schoolId:DbIdentity, facultyId:DbIdentity, activityId:DbIdentity}) {
        const [err, data] = await aFetch<{}>("GET" , `/faculty/${facultyId}/school/${schoolId}/activity/${activityId}`);
        return [err, (err ? undefined : new Activity(data))!] as const;
    }

    static async fetchActivitiesOfClassAsFaculty(schoolId:DbIdentity, facultyId:DbIdentity, classId:DbIdentity) {
        const [err, data] = await aFetch<{}[]>("GET", `/faculty/${facultyId}/school/${schoolId}/class/${classId}/activity`);
        return [err, err ? [] : data.map(a => new Activity(a))] as const;
    }

    static async getOpenActivities(schoolId:DbIdentity, facultyId:DbIdentity, gradingTerm: DbIdentity): Promise<[Error|undefined, Activity[]]> {
        const [err, data] = await aFetch<{}[]>("GET" , `/faculty/${facultyId}/school/${schoolId}/GradingTerm/${gradingTerm}/OpenActivities`);
        return [err, err ? [] : data.map(a => new Activity(a))];
    }

    static async getClosedActivities(schoolId:DbIdentity, facultyId:DbIdentity, gradingTerm: DbIdentity): Promise<[Error|undefined, Activity[]]> {
        const [err, data] = await aFetch<{}[]>("GET" , `/faculty/${facultyId}/school/${schoolId}/GradingTerm/${gradingTerm}/ClosedActivities`);
        return [err, err ? [] : data.map(a => new Activity(a))];
    }


    static async getActivityOfFacultyInSchoolAsAdmin({schoolId, facultyId}:{schoolId:DbIdentity, facultyId:DbIdentity}) {
        const [err, dto] = await aFetch<GeneralDto>("GET" , `/admin/school/${schoolId}/faculty/${facultyId}/activity`);
        return [err, (err ? undefined : parseGeneralViewModel(dto))!] as const;
    }

    static async updateAsFaculty(xs:Activity[], {schoolId, facultyId}:{schoolId:DbIdentity, facultyId:DbIdentity}) {
        const [err, ys] = await aFetch<{}[]>("PUT", `/faculty/${facultyId}/school/${schoolId}/activity`, xs.map(x => x.toJS()));
        return [err, (err ? [] : ys.map(y => new Activity(y)))] as const;
    }

    static async setPublish(schoolId:DbIdentity, facultyId:DbIdentity, activityId:DbIdentity, start: number | undefined, end: number| undefined) {
        const [err, data] = await aFetch<{}>("PUT", `/faculty/${facultyId}/school/${schoolId}/activity/${activityId}/publish`, {"start": start, "end": end });
        return [err, (err ? undefined : new Activity(data))!] as const;
    }

    static async duplicate(schoolId:DbIdentity, facultyId:DbIdentity, activityId: DbIdentity, classIds:DbIdentity[]) {
        const [err, data] = await aFetch<DbIdentity[]>("POST", `/faculty/${facultyId}/school/${schoolId}/activity/${activityId}/duplicate`,  classIds);
        return [err, err ? [] : data] as const;
    }

    static sorter = {
        dateDue     : (a: Activity, b: Activity) => ((a.dateDue || 0) - (b.dateDue || 0)),
        dateAssigned: (a: Activity, b: Activity) => ((a.dateAssigned || 0) - (b.dateAssigned || 0)),
        dateCreated : (a: Activity, b: Activity) => ((a.dateCreated || 0) - (b.dateCreated || 0)),
        title       : (a: Activity, b: Activity) => (a.title.localeCompare(b.title)),
        sortIndex   : (a: Activity, b: Activity) => (a.sortIndex - b.sortIndex),
        weight      : (a: Activity, b: Activity) => (a.weight    - b.weight   ),
    };
}

export class ActivityDetail extends Activity {
    @observable submisstionCount: number = 0;
    @observable unGradeCount: number = 0;
    constructor(data?:any) {
        super(data);
        this.submisstionCount = 0;
        this.unGradeCount = 0;
        if (data != null)
        {
            Object.assign(this, data);
        }
    }
    static async getActivitiesToGrade(schoolId:DbIdentity, facultyId:DbIdentity, gradingTerm: number): Promise<[Error|undefined, ActivityDetail[]]> {
        const [err, data] = await aFetch<{}[]>("GET" , `/faculty/${facultyId}/school/${schoolId}/GradingTerm/${gradingTerm}/OpenToGradeActivities`);
        return [err, err ? [] : data.map(a => new ActivityDetail(a))];
    }
}

export function flatPages<T extends Activity>(activities: T[]) {
    const pages = activities.filter(a => a.type == ActivityType.Activity || a.type == 0);

    const parentId2Pages = new Map(
        map(
            groupBy(pages.filter(a => a.parentActivityId != null), a => a.parentActivityId!),
            xs => [xs[0].parentActivityId!, xs.sort(Activity.sorter.sortIndex)]
        )
    );

    return pages.filter(p => p.parentActivityId == null).sort(Activity.sorter.sortIndex)
        .flatMap(p => flatTree(p, parentId2Pages));
}

function flatTree<T extends Activity>(p: T, parentId2Pages:Map<DbIdentity, T[]>): T[] {
    const cs = parentId2Pages.get(p.activityId);
    if (cs == null || cs.length < 1) return [p];
    return [p, ...cs.flatMap(c => flatTree(c, parentId2Pages))];
}
