import {observable, action, computed, toJS} from "mobx";

import {DbIdentity, DefaultId} from "./types"
import { aFetch } from "../services/api/fetch";
import { IUser } from "./User";
import { GeneralDto, parseGeneralViewModel } from "./GeneralViewModel";

export class Faculty implements IUser {
                facultyId      : DbIdentity;
    @observable externalId     : string;

    @observable userId         : DbIdentity;
    @observable email          : string = "";
    @observable firstName      : string = "";
    @observable lastName       : string = "";
    @observable salutation     : string = "";
    @observable nickname       : string = "";
    @observable phoneNumber    : string = "";
    @observable avatar         : string = "";
    // @observable districtId     : DbIdentity;

    constructor(data?:{}) {
        this.facultyId       = DefaultId;
        this.externalId      = "";
        // this.districtId      = 0;
        this.userId          = DefaultId;

        if (data != null) Object.assign(this, data);
    }

    extends(x:Faculty) {
        Object.assign(this, x);
    }

    toJS() {
        return toJS(this);
    }

    @action set_externalId  = (v: string) => { this.externalId  = v }
    @action set_email       = (v: string) => { this.email       = v }
    @action set_firstName   = (v: string) => { this.firstName   = v }
    @action set_lastName    = (v: string) => { this.lastName    = v }
    @action set_phoneNumber = (v: string) => { this.phoneNumber = v }

    @computed get fullName() { return this.nickname || `${this.salutation || ""} ${this.firstName || ""} ${this.lastName || ""}`.trim(); }

    async save(schools:DbIdentity[]) {
        const body = {...this.toJS(), schools};
        if (this.facultyId < 1) {
            const [err, x] = await aFetch<{}[]>("POST", `/admin/faculty`, body);
            return [err, (err ? undefined : new Faculty(x))!] as const;
        }

        const [err, x] = await aFetch<{}[]>("PUT", `/admin/faculty/${this.facultyId}`, body);
        return [err, (err ? undefined : new Faculty(x))!] as const;
    }

    static async getFacultiesAsAdmin() {
        const [err, xs] = await aFetch<{}[]>("GET", `/admin/faculty`);
        return [err, err ? [] : xs.map(x => new Faculty(x))] as const;
    }

    static async getFacultyAsAdmin({facultyId}:{facultyId: DbIdentity}) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/admin/faculty/${facultyId}`);
        return [err, (err ? undefined : parseGeneralViewModel(dto))!] as const;
    }

    static async getFacultyOfSchoolAsAdmin({schoolId, facultyId}:{schoolId: DbIdentity, facultyId: DbIdentity}) {
        const [err, x] = await aFetch<{}>("GET", `/admin/school/${schoolId}/faculty/${facultyId}`);
        return [err, (err ? undefined : new Faculty(x))!] as const;
    }
    static async addCustomRoles({schoolId, facultyId, roles}:{schoolId: DbIdentity, facultyId: DbIdentity, roles: string[]}) {
        const [err, x] =  await aFetch<{}, {[key:string]:string[]}>("POST", `admin/school/${schoolId}/faculty/${facultyId}/roles`, { customRoles: roles});
        return [err, (err ? undefined : x)!] as const;
    }

    static sorter = {
        firstName: (a:Faculty, b:Faculty) => (a.firstName.localeCompare(b.firstName)),
        lastName: (a:Faculty, b:Faculty) => (a.lastName.localeCompare(b.lastName)),
    };
}
