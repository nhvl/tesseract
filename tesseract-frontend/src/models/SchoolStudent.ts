import {observable} from "mobx";

import {DbIdentity, DefaultId} from "./types"

export interface ISchoolStudent {
    schoolId      : DbIdentity;
    studentId     : DbIdentity;
    gradingPeriod : DbIdentity;
    grade        ?: number;
}

export class SchoolStudent {
                schoolId      : DbIdentity;
                studentId     : DbIdentity;
    @observable GradingPeriod : DbIdentity;
    @observable Grade        ?: number;

    constructor(data?:{}) {
        this.schoolId      = DefaultId;
        this.studentId     = DefaultId;
        this.GradingPeriod = DefaultId;
        this.Grade         = undefined;

        if (data != null) Object.assign(this, data);
    }
}
