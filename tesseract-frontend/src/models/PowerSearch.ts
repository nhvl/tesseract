import {observable, action} from "mobx";
import { DbIdentity, DefaultId, EUserRole } from "../models/types";
import { aFetch } from "../services/api/fetch";
export class PowerSearch {
    @observable students    :SearchView[]=[];
    @observable classes     :SearchView[]=[];
    @observable pages       :SearchView[]=[];
    @observable assignments :SearchView[]=[];
    @observable assessments :SearchView[]=[];
    @observable schools     :SearchView[]=[];
    @observable faculties   :SearchView[]=[];
    @observable parents     :SearchView[]=[];

    constructor(data?:any) {
        this.students    = [];
        this.classes     = [];
        this.pages       = [];
        this.assessments = [];
        this.assignments = [];
        this.schools     = [];
        this.faculties   = [];
        this.parents     = [];

        if (data != null)
        {
            Object.assign(this, data);
        }
    }

    static async search({role, ...query}:{role:EUserRole, q:String, schoolId?:DbIdentity}): Promise<[Error|null, PowerSearch]> {
        const [err, data] = await aFetch<{}>("GET", `/district/${role}/PowerSearch`, query);
        if (err) return [err, null as any];
        return [null, new PowerSearch(data)];
    }
}

export class SearchView {
    @observable id    :DbIdentity;
    @observable name  :String;
    constructor(data?:any) {
        this.id = DefaultId;
        this.name = "";
        if (data != null)
        {
            Object.assign(this, data);
        }
    }
}
