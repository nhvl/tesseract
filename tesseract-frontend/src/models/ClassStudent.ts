import { DbIdentity } from "./types";

export interface IClassStudent {
    classId         : DbIdentity;
    studentId       : DbIdentity;
    sortIndex       : number;
    cumulativeGrade?: number;
    lastAccessed   ?: number;
}
