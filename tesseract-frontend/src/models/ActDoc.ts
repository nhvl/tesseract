import {observable, action, computed} from "mobx";

import { DocId, DbIdentity, UrlString, HtmlString, DefaultId, NumberDate } from "./types";
import { MediaTypeEnum } from "./MediaTypeEnum";

import { Discussion } from "./Discussion";

import { UploadFile } from "antd/lib/upload/interface";

import { message } from "antd";
import nanoid from "nanoid";
import { createError } from "../services/api/AppError";
import { aFetch } from "../services/api/fetch";
import { GoogleDocItem } from "./GoogleDrive";

export enum EActItemType {
    Heading         = 1,
    Text            = 2,
    Media           = 3,
    Embed           = 5,
    Discussion      = 6,
    NumericQuestion = 7,
    ChoiceQuiz      = 8,
    MatchQuiz       = 9,
    TextQuiz        = 10,
    PollQuiz        = 11,
}

export enum EEmbedAppType {
    GoogleDrive         = "Google.Drive",
}

export interface IActOption {
    label    : HtmlString,
    image    : UrlString,
    isCorrect: boolean
}
interface IPollQuizItem {
    label    : HtmlString,
    image    : UrlString,
}

export interface IMatchQuizItem {
    left       : string;
    leftImage  : UrlString;
    right      : string;
    rightImage : UrlString;
}

export interface IActItem {
    id             : string,
    type           : EActItemType,
    title          : string;
    content        : HtmlString;
    image          : UrlString;
    startDate     ?: number;
    endDate       ?: number;

    link           : UrlString;
    contentType    : string;
    mediaType      : MediaTypeEnum;

    embedLink           : UrlString;
    embedContentType    : string;
    embedId             : string;
    embedApp            : string;
    embedType            : string;
    embedLastEdited     : number; 

    matchQuizzItems: IMatchQuizItem[];

    numericMin    ?: number;
    numericMax    ?: number;

    isMultipleAnswer?: boolean;
    options       ?: IActOption[];

    pollItems     ?: IPollQuizItem[];

    discussionId  ?: DbIdentity;
}

interface IActDoc {
    id        : DocId;
    activityId: DbIdentity;

    title     : string;
    banner    : UrlString;
    summary   : HtmlString;

    items     : IActItem[];

    // comments: DocId[];
}

export class ActDoc {
    /* */               id         : DocId      = "";
    /* */               activityId : DbIdentity = DefaultId;
    @observable         title      : string     = "";
    @observable         banner     : UrlString  = "";
    @observable         summary    : HtmlString = "";
    @observable.shallow items      : ActItem[]  = [];
    @observable.shallow comments   : DocId[]    = [];
    /* */               dateUpdated: NumberDate = 0;

    @observable totalStudent      : number = 0;

    constructor(data:any) {
        if (data != null) {
            const {items, ...pData} = data;
            if (Array.isArray(items)) this.items = items.map(item => new ActItem(item))
            Object.assign(this, pData);
        }
        if(this.title != "" && !/<[a-z][\s\S]*>/i.test(this.title)) {
            this.title = "<p>" + this.title + "</p>";
        }
    }

    toJS(): IActDoc {
        return ({
            id        : this.id,
            activityId: this.activityId,
            title     : this.title,
            banner    : this.banner,
            summary   : this.summary,
            items     : this.items.map(item => item.toJS()),
            // comments  : this.comments,
        });
    }

    @action set_title   = (v: string    ) => { this.title   = v }
    @action set_banner  = (v: UrlString ) => { this.banner  = v }
    @action set_summary = (v: HtmlString) => { this.summary = v }
    @action set_totalStudent = (v:number) => {this.totalStudent = v }
    async save(schoolId:DbIdentity, facultyId:DbIdentity) {
        if (this.activityId < 1) return [createError(new Error(`Invalid activityId`), 400), this] as const;
        if (!this.id) {
            const [err, x] = await aFetch<IActDoc>("POST", `/faculty/${facultyId}/school/${schoolId}/activity/${this.activityId}/doc`, this.toJS())
            return [err, (err ? undefined : new ActDoc(x))!] as const;
        }
        const [err, x] = await aFetch<IActDoc>("PUT", `/faculty/${facultyId}/school/${schoolId}/activity/${this.activityId}/doc`, this.toJS())
        return [err, (err ? undefined : new ActDoc(x))!] as const;
    }

    static async fetchAsFaculty(schoolId:DbIdentity, facultyId:DbIdentity, activityId:DbIdentity) {
        const [err, x] = await aFetch<IActDoc>("GET", `/faculty/${facultyId}/school/${schoolId}/activity/${activityId}/doc`)
        const d = (err ? undefined : new ActDoc(x))!
        return [err, d] as [typeof err, typeof d];
    }

    static async fetchAsStudent({schoolId, activityId, studentId}:{schoolId:DbIdentity, activityId:DbIdentity, studentId:DbIdentity}) {
        const [err, x] = await aFetch<IActDoc>("GET", `/student/${studentId}/school/${schoolId}/activity/${activityId}/faculty-doc`);
        const d = (err ? undefined : new ActDoc(x))!
        return [err, d] as [typeof err, typeof d];
    }
}

export class ActItem {
    @observable.ref     id             : string                = "";
    @observable         type           : EActItemType          = EActItemType.Text;
    @observable         title          : string                = "";
    @observable         image          : UrlString             = "";
    @observable         content        : HtmlString            = "";
    @observable         startDate     ?: number                = undefined;
    @observable         endDate       ?: number                = undefined;

    @observable         link           : UrlString             = "";
    @observable         contentType    : string                = "";
    @observable         embedLink           : UrlString             = "";
    @observable         embedId             : string             = "";
    @observable         embedApp            : string             = "";
    @observable         embedType           : string             = "";
    @observable         embedContentType    : string                = "";
    @observable         embedLastEdited     : number                = 0;
    @observable         mediaType      : IActItem["mediaType"] = 0;
    @observable         matchQuizzItems: MatchQuizzItem[]      = [];

    @observable         numericMin     : number|undefined      = undefined;
    @observable         numericMax     : number|undefined      = undefined;
    @observable         isMultipleAnswer?: boolean             = true;
    @observable         options       ?: ActOption[]           = [];

    @observable         pollValue  ?: number                   = undefined;
    @observable         pollItems  ?: PollQuizItem[]           = [];

    @observable         discussionId ?: DbIdentity;
    @observable.ref     discussion ?: Discussion;

    //@observable.shallow comments : DocId[] = [];

    origLink:UrlString = "";

    constructor(data:any) {
        if (data != null) {
            const {options, matchQuizzItems, pollItems, ...pData} = data;
            if (Array.isArray(options)) this.options = options.map(opt => new ActOption(opt));
            if (Array.isArray(matchQuizzItems)) this.matchQuizzItems = matchQuizzItems.map(i => new MatchQuizzItem(i));
            if (Array.isArray(pollItems)) this.pollItems = pollItems.map(i => new PollQuizItem(i));
            Object.assign(this, pData);
        }

        if(this.type == EActItemType.ChoiceQuiz) {
            if (this.options == undefined) {
                this.options = [];
            }
            if (this.options.length == 0) {
                this.options.push(new ActOption({isCorrect:true}));
                this.options.push(new ActOption());
                this.options.push(new ActOption());
                this.options.push(new ActOption());
            }
        }

        if(this.type == EActItemType.PollQuiz) {
            if (this.pollItems == undefined) {
                this.pollItems = [];
            }
            if (this.pollItems.length == 0) {
                this.pollItems.push(new PollQuizItem());
                this.pollItems.push(new PollQuizItem());
            }
        }

        if (!this.id) { this.id = nanoid(); }

        this.origLink = this.link;
    }

    @action set_type      = (v : EActItemType) => { this.type      = v }
    @action set_title     = (v : string      ) => { this.title     = v }
    @action set_image     = (v : UrlString   ) => { this.image     = v }
    @action set_content   = (v : HtmlString  ) => { this.content   = v }
    @action set_startDate = (v?: number  )     => { this.startDate = v }
    @action set_endDate   = (v?: number  )     => { this.endDate   = v }

    @action set_discussionId = (v?:DbIdentity) => {this.discussionId = v}
    @action set_discussion = (v?:Discussion) => {this.discussion = v}

    isEmpty = (str:any) => {
        return (str==undefined || 0 === str.length);
    }
    @action set_numericMin = (v:number|undefined)   => { this.numericMin = v; }
    @action populateMaxValue = () => {
        if(!this.isEmpty(this.numericMin) && (this.isEmpty(this.numericMax) || this.numericMax! < this.numericMin!)) {
            this.numericMax = this.numericMin;
        }
    }
    @action set_numericMax = (v:number|undefined)   => { this.numericMax = v; }
    @action populateMinValue = () => {
        if(!this.isEmpty(this.numericMin) && !this.isEmpty(this.numericMax) && this.numericMin! > this.numericMax!){
            this.numericMin = this.numericMax;
        }
    }

    @observable.ref imageFile?:UploadFile;
    @action set_imageFile = async (v:UploadFile|undefined) => { this.imageFile = v }

    @action removeMathQuizzItem = (v:MatchQuizzItem) => {
        this.matchQuizzItems = this.matchQuizzItems.filter(i=>i!=v);
    }

    @action set_link    = (v: UrlString        ) => { this.link    = v }

    @action set_embedLink    = (v: UrlString        ) => { this.embedLink  = v }

    @action set_mediaType   = (v: MediaTypeEnum)     => { this.mediaType  = v }

    @observable.ref mediaFile?:UploadFile;
    @action set_mediaFile = async (v:UploadFile|undefined) => {
        if (v != null) {
            const file: File = (v.originFileObj || v) as any;
            if (file.type.startsWith("video/")) {
                v.url = URL.createObjectURL(file);
                this.mediaType = MediaTypeEnum.VideoFile;
            } else if (file.type.startsWith("image/")) {
                v.thumbUrl = v.url = URL.createObjectURL(file);
                this.mediaType = MediaTypeEnum.ImageFile;
            } else if (file.type.startsWith("audio/")) {
                v.url = URL.createObjectURL(file);
                this.mediaType = MediaTypeEnum.AudioFile;
            } else {
                message.warning(`Unsupported file: ${file.name}`);
                return;
            }

            this.link = "";
            this.contentType = file.type;
        }
        this.mediaFile = v;
    }

    @observable.ref googleFile?:GoogleDocItem;
    @action set_googleFile = async (v:GoogleDocItem|undefined) => {
        if (v) {
            this.embedContentType = v.mimeType;
            this.embedId = v.id;
            this.embedApp = EEmbedAppType.GoogleDrive;
            this.embedType = v.type;
            this.embedLastEdited = v.lastEditedUtc;
            this.set_embedLink("");
        }
        this.googleFile = v;
    }
    @action clearEmbedContent() {
        this.embedContentType = "";
        this.embedId = "";
        this.embedType = "";
        this.embedLastEdited = 0;
    }

    @action clearMediaType() {
        this.contentType = "";
        this.mediaType = MediaTypeEnum.Unknown;
    }

    @computed get typeString() {
        switch (this.type) {
            case EActItemType.Heading        : return "Heading";
            case EActItemType.Text           : return "Text Content";
            case EActItemType.Media          : return "Multimedia";
            case EActItemType.Embed          : return "Google Drive";
            case EActItemType.ChoiceQuiz     : return "Multiple Choice";
            case EActItemType.TextQuiz       : return "Text Question";
            case EActItemType.NumericQuestion: return "Numeric";
            case EActItemType.MatchQuiz      : return "Matching Options";
            case EActItemType.PollQuiz       : return "Polling";
            case EActItemType.Discussion     : return "Discussion";
            default: return "";
        }
    }

    toJS(): IActItem {
        return ({
            id             : this.id,
            type           : this.type,
            title          : this.title,
            image          : this.image,
            content        : this.content,
            startDate      : this.startDate,
            endDate        : this.endDate,
            link           : this.link,
            contentType    : this.contentType,
            mediaType      : this.mediaType,

            embedLink      : this.embedLink,
            embedContentType: this.embedContentType,
            embedId         : this.embedId,
            embedApp        : this.embedApp,
            embedType       : this.embedType,
            embedLastEdited : this.embedLastEdited,
            
            matchQuizzItems: this.matchQuizzItems.map(i => i.toJS()),
            numericMin     : this.numericMin,
            numericMax     : this.numericMax,
            isMultipleAnswer: this.isMultipleAnswer,
            options        : this.options == undefined ? undefined: this.options.map(opt => opt.toJS()),
            pollItems      : this.pollItems == undefined ? undefined : this.pollItems.map(opt=>opt.toJS()),
            discussionId   : this.discussionId,
        });
    }

    @action set_pollValue    = (v: number) => { this.pollValue = v }
    @computed get totalPoll(){
        return (this.pollItems==null||this.pollItems.length==0) ? 0 :
        this.pollItems.map(x=>x.votes).reduce((a,b)=> { return a + b; });
    }

    @action set_isMultipleAnswer = (v:boolean)   => {
        this.isMultipleAnswer = v;
        if(!v) {
            if (this.options == undefined) this.options = [];
            const nCorrectOption = this.options.filter(o => o.isCorrect).length;
            if(nCorrectOption > 1) {
                const selectedIndex = this.options.findIndex(o=>o.isCorrect);
                this.options.map((o,index) => {
                    if(selectedIndex != index){
                        o.isCorrect = false;
                    }
                    return o;
                })
            }
        }
    }

    @action set_multipleChoice_correctAnswers(isCorrect: boolean, index: number){
        if (this.options == undefined) this.options = [];
        const nCorrectOption = this.options.filter(o => o.isCorrect).length;
        // At least 1 correct answer
        if (nCorrectOption <= 1 && !isCorrect) return;
        //If 1 answer type
        if(!this.isMultipleAnswer && isCorrect && nCorrectOption>=1) {
            this.options = this.options.map(o=>{o.isCorrect = false; return o;});
        }
        this.options[index].set_isCorrect(isCorrect);
    }
    is_multipleChoice_deleteAble(index: number): boolean {
        if (this.options == undefined) return false;
        const nCorrectOption = this.options.filter(o => o.isCorrect).length;
        return !(nCorrectOption <= 1 && this.options[index].isCorrect);
    }
}

export class PollQuizItem implements IPollQuizItem {
    @observable label     : HtmlString = ""    ;
    @observable image     : UrlString  = ""    ;

    @observable votes     : number     = 0     ;

    constructor(data?:{}) {
        if (data != null) Object.assign(this, data);
    }

    @action set_label     = (v: HtmlString ) => { this.label     = v }
    @action set_image     = (v: UrlString  ) => { this.image     = v }

    @observable.ref imageFile?:UploadFile;
    @action clear_image   = ()               => { this.imageFile = undefined; this.image = ""; }
    @action set_imageFile = async (v:UploadFile|undefined) => { this.imageFile = v }
    @computed get isRequired(): boolean|undefined {
        return !(this.label  != "" || this.imageFile != undefined || this.image  != "");
    }

    static calculatePercent(votes: number, totalStudent:number) {
        if(totalStudent==0) return 0;
        return Math.round(votes * 100 / totalStudent);
    }

    static async fetchAsStudent(schoolId:DbIdentity, activityId:DbIdentity, studentId:DbIdentity, pollQuizId: string) {
        const [err, x] = await aFetch<number>("GET", `/student/${studentId}/school/${schoolId}/activity/${activityId}/pollQuizId/${pollQuizId}`);
        const d = (err ? undefined : x)!
        return [err, d] as const;
    }

    static async savePolling(schoolId:DbIdentity, activityId:DbIdentity, studentId:DbIdentity, pollQuizId: string, votes: number ){
        const [err, x] = await aFetch<PollQuizItem>("PUT", `/student/${studentId}/school/${schoolId}/activity/${activityId}/pollQuizId/${pollQuizId}/vote/${votes}`);
        return err;
    }

    toJS(): IPollQuizItem {
        return ({
            label     : this.label,
            image     : this.image
        });
    }
}

export class ActOption implements IActOption  {
    @observable label     : HtmlString = ""    ;
    @observable image     : UrlString  = ""    ;
    @observable isCorrect : boolean    = false ;

    constructor(data?:{}) {
        if (data != null) Object.assign(this, data);
    }

    @action set_label     = (v: HtmlString ) => { this.label     = v }
    @action set_isCorrect = (v: boolean    ) => { this.isCorrect = v }

    @action set_image     = (v: UrlString  ) => { this.image     = v }

    @observable.ref imageFile?:UploadFile;
    @action clear_image   = ()               => { this.imageFile = undefined; this.image = ""; }
    @action set_imageFile = async (v:UploadFile|undefined) => { this.imageFile = v }
    @computed get isRequired(): boolean|undefined {
        if(this.isCorrect){
            return !(this.label  != "" || this.imageFile != undefined || this.image  != "");
        }

        return false;
    }
    isEqual(item: ActOption): boolean{
        return (this.label == item.label && this.image == item.image);
    }
    toJS(): IActOption {
        return ({
            label     : this.label,
            image     : this.image,
            isCorrect : this.isCorrect
        });
    }
}

export class MatchQuizzItem implements IMatchQuizItem {
    @observable     left    : string = "";
    @observable     right   : string = "";

    @observable leftImage   : UrlString  = ""    ;
    @observable rightImage  : UrlString  = ""    ;

    constructor(data?:{}) {
        if (data != null) Object.assign(this, data);
    }

    @action set_left  = (v: string) => {this.left  = v}
    @action set_right = (v: string) => {this.right = v}

    @action set_leftImage   = (v: UrlString) => { this.leftImage  = v }
    @action set_rightImage  = (v: UrlString) => { this.rightImage = v }

    @observable.ref leftImageFile?:UploadFile;
    @observable.ref rightImageFile?:UploadFile;

    @action clear_leftImage  = () => { this.leftImageFile  = undefined; this.leftImage  = ""; }
    @action clear_rightImage = () => { this.rightImageFile = undefined; this.rightImage = ""; }

    @action set_leftImageFile  = async (v:UploadFile|undefined) => { this.leftImageFile  = v }
    @action set_rightImageFile = async (v:UploadFile|undefined) => { this.rightImageFile = v }
    @computed get hasLeftValue()  { return this.left  != "" || this.leftImageFile != undefined || this.leftImage  != ""; }
    @computed get hasRightValue() { return this.right != "" || this.rightImageFile!= undefined || this.rightImage != ""; }
    @computed get isLeftRequired()  { return !this.hasLeftValue && this.hasRightValue; }
    @computed get isRightRequired() { return !this.hasRightValue && this.hasLeftValue; }
    isEqual(item: IMatchQuizItem){
        return (this.left == item.left) && this.right == item.right && this.leftImage == item.leftImage && this.rightImage == item.rightImage;

    }
    toJS(): IMatchQuizItem {
        return ({
            left       : this.left,
            leftImage  : this.leftImage,
            right      : this.right,
            rightImage : this.rightImage
        });
    }

}
