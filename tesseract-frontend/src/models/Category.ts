import { observable, action, toJS } from "mobx";

import { DefaultId, DbIdentity } from "./types";

import { aFetch } from "../services/api/fetch";

export class Category {
    @observable activityCategoryId: DbIdentity = DefaultId;
    @observable name              : string     = "";
    @observable color             : string     = "#FFFFFF";
    @observable isGraded          : boolean    = true;
    @observable maxScore          : number     = 10;
    @observable weight            : number     = 1;

    constructor(data?: any) {
        if (data != null) {
            Object.assign(this, data);
        }
    }

    @action set_name     = (v: string)  => { this.name     = v; }
    @action set_color    = (v: string)  => { this.color    = v; }
    @action set_isGraded = (v: boolean) => { this.isGraded = v; }
    @action set_maxScore = (v: number)  => { this.maxScore = v; }
    @action set_weight   = (v: number)  => { this.weight   = v; }

    toJS() {
        return toJS(this);
    }

    static isValidCategory(categoryId: DbIdentity|undefined, categories: Category[]){
        if(categoryId){
            for(var i = 0;i < categories.length; i++){
                if(categories[i].activityCategoryId == categoryId) return true;
            }
        }
        return false;
    }

    static async fetch({schoolId, facultyId}: {schoolId: DbIdentity, facultyId: DbIdentity}) {
        const [err, data] = await aFetch<{}[]>("GET", `/faculty/${facultyId}/school/${schoolId}/category`);
        return [err, err ? [] : data.map(a => new Category(a))] as const;
    }

    static async update({schoolId, facultyId}: {schoolId: DbIdentity, facultyId: DbIdentity}, category: Category) {
        if (category.activityCategoryId == DefaultId) {
            const [err, data] = await aFetch<{}>("POST", `/faculty/${facultyId}/school/${schoolId}/category`, category.toJS());
            return [err, (err ? undefined : new Category(data))!] as const;
        }

        const [err, data] = await aFetch<{}>("PUT", `/faculty/${facultyId}/school/${schoolId}/category`, category.toJS());
        return [err, (err ? undefined : new Category(data))!] as const;
    }

    static async delete({schoolId, facultyId}: {schoolId: DbIdentity, facultyId: DbIdentity}, category: Category) {
        const [err, data] = await aFetch<{}>("DELETE", `/faculty/${facultyId}/school/${schoolId}/category/${category.activityCategoryId}`);
        return [err, data] as const;
    }

    static async getDisctrictDefaultColors(districtId: DbIdentity){
        const [err, x] = await aFetch<string[]>("GET", `/config/district/${districtId}/ActivityCategoryColor`);
        return [err, (err ? undefined : x)!] as const;
    }
    static async getSchoolDefaultColors(schoolId: DbIdentity){
        const [err, x] = await aFetch<string[]>("GET", `/config/school/${schoolId}/ActivityCategoryColor`);
        return [err, (err ? undefined : x)!] as const;
    }
}
