import {observable} from "mobx";

import {DbIdentity, DefaultId} from "./types"
import { aFetch, uploadFile } from "../services/api/fetch";

export class District {
                districtId    : DbIdentity = DefaultId;
    @observable districtName  : string = "";
    @observable domain        : string = "";
    @observable logoUrl       : string = "";

    constructor(data?:{}) {
        if (data != null) Object.assign(this, data);
    }

    static async getDistrict() {
        const [error, data]  = await aFetch("GET", `/district`);
        return [error, (error ? undefined : new District(data))!] as const;
    }

    async updateLogo(logo:File) {
        const [err, x] = await uploadFile<{}>("PUT", `/admin/district/${this.districtId}/logo`, logo);
        return [err, (err ? undefined : new District(x))!] as const;
    }
}
