import {observable, action, toJS, computed} from "mobx";

import {DbIdentity, DefaultId} from "./types";
import { aFetch } from "../services/api/fetch";

export class Role {
    @observable         roleId     : DbIdentity;
    @observable         name       : string;
    @observable         isSystemRole: boolean;
    @observable         description: string;
    @observable.shallow permissions: string[];

    constructor(data?:{}) {
        this.roleId      = DefaultId;
        this.name        = "";
        this.description = "";
        this.isSystemRole = false;
        this.permissions = [];

        if (data != null) Object.assign(this, data);
    }

    @action set_name        = (v: string  ) => { this.name        = v }
    @action set_description = (v: string  ) => { this.description = v }
    @action set_permissions = (v: string[]) => { this.permissions = v }

    @computed get params() { return ({roleId: String(this.roleId)}) }

    toJS() {
        const v = toJS(this);
        return v;
    }

    clone() {
        return new Role(this.toJS())
    }

    async save() {
        if (this.roleId < 1) {
            const [err, x] = await aFetch<{}>("POST", `/admin/role`, this.toJS());
            return [err, (err ? undefined : new Role(x))!] as const;
        }
        const [err, x] = await aFetch<{}>("PUT", `/admin/role/${this.roleId}`, this.toJS());
        return [err, (err ? undefined : new Role(x))!] as const;
    }

    static async fetchAll() {
        const [err, xs] = await aFetch<{}[]>("GET", `/admin/role`);
        return [err, (err ? [] : xs.map(x => new Role(x)))] as const;
    }

    static async fetch({roleId}:{roleId:DbIdentity}) {
        const [err, x] = await aFetch<{}[]>("GET", `/admin/role/${roleId}`);
        return [err, (err ? undefined : new Role(x))!] as const;
    }

    static async fetchUserRole({districtId, userId}:{districtId:DbIdentity, userId:DbIdentity}) {
        const [err, xs] = await aFetch<string[]>("GET", `/admin/district/${districtId}/user/${userId}/role`);
        return [err, (err ? [] : xs)] as const;
    }

    static async saveUserRole({districtId, userId}:{districtId:DbIdentity, userId:DbIdentity}, roles:string[]) {
        const [err, xs] = await aFetch<string[]>("PUT", `/admin/district/${districtId}/user/${userId}/role`, roles);
        return [err, (err ? [] : xs)] as const;
    }

    static async deleteRole({roleId}:{roleId:DbIdentity}) {
        const [err, x] = await aFetch<{}[]>("DELETE", `/admin/role/${roleId}`);
        return [err, (err ? undefined : new Role(x))!] as const;
    }
}
