import {observable, action, computed} from "mobx";

import { aFetch } from "../services/api/fetch";

import {DbIdentity, DefaultId, } from "./types";
import {GeneralDto, parseGeneralViewModel} from "./GeneralViewModel";
import { Activity } from "./Activity";
import { ActivityScore } from './ActivityScore';

interface IClassStudents {
    classId         : DbIdentity;
    studentId       : DbIdentity;
    cumulativeGrade?: number;
}

export class Student {
                studentId       : DbIdentity;
    @observable externalId      : string;
    @observable studentNumber   : string;
    @observable grade          ?: number;

    @observable userId          : DbIdentity;
    @observable email           : string;
    @observable firstName       : string;
    @observable lastName        : string;
    @observable salutation      : string;
    @observable nickname        : string;
    @observable phoneNumber     : string;
    @observable avatar          : string;
    @observable dob            ?: number;
    @observable lastActive     ?: number;

    @observable cumulativeGrade?: number;

    @observable.shallow classes: DbIdentity[] = [];

    @observable.shallow tActivityScore: ActivityScore[] = [];
    @computed get mActivitiScore() { return observable.map(this.tActivityScore.map<[DbIdentity, ActivityScore]>(s => [s.activityId, s])) }

    @observable.shallow tClassStudent: IClassStudents[] = [];
    @computed get mClassStudent() { return observable.map(this.tClassStudent.map<[DbIdentity, IClassStudents]>(s => [s.classId, s])) }

    constructor(data?:{}) {
        this.studentId       = DefaultId;
        this.externalId      = "";
        this.studentNumber   = "";
        this.grade           = undefined;

        this.userId          = DefaultId;
        this.email           = "";
        this.firstName       = "";
        this.lastName        = "";
        this.salutation      = "";
        this.nickname        = "";
        this.phoneNumber     = "";
        this.avatar          = "";
        this.dob             = undefined;
        this.lastActive      = undefined;

        this.cumulativeGrade = undefined;

        if (data != null) {
            Object.assign(this, data);
        };
    }

    toJS() {
        return ({
            studentId      : this.studentId,
            externalId     : this.externalId,
            studentNumber  : this.studentNumber,
            grade          : this.grade,

            userId         : this.userId,
            email          : this.email,
            phoneNumber    : this.phoneNumber,
            firstName      : this.firstName.trim(),
            lastName       : this.lastName.trim(),
            avatar         : this.avatar,
            dob            : this.dob,
            lastActive     : this.lastActive,

            cumulativeGrade: this.cumulativeGrade,
        });
    }

    @action extends(c: Student) {
        Object.assign(this, c.toJS());
        if (c.classes.length > 0) this.classes = c.classes;
        if (c.tClassStudent.length > 0) this.tClassStudent = c.tClassStudent;
        if (c.tActivityScore.length > 0) this.tActivityScore = c.tActivityScore;
    }

    @computed get params() { return ({studentId: String(this.studentId)})}

    @computed get fullName() { return `${this.lastName}, ${this.firstName}`}

    @computed get getCumulativeGrade() {
        return Student.getCumulativeGradePercentage(this.cumulativeGrade);
    }

    @action set_cumulativeGrade = (v: number|undefined) => { this.cumulativeGrade     = v }

    @action set_studentId     = (v: DbIdentity) => { this.studentId     = v }
    @action set_externalId    = (v: string    ) => { this.externalId    = v }
    @action set_studentNumber = (v: string    ) => { this.studentNumber = v }
    @action set_grade         = (v?: number   ) => { this.grade         = v }
    @action set_firstName     = (v: string    ) => { this.firstName     = v }
    @action set_lastName      = (v: string    ) => { this.lastName      = v }
    @action set_phoneNumber   = (v: string    ) => { this.phoneNumber   = v }
    @action set_email         = (v: string    ) => { this.email         = v }
    @action set_avatar        = (v: string    ) => { this.avatar        = v }

    calculateMissingAssigment(tActivity:Activity[]) {
        return (
            tActivity.filter(a => this.classes.includes(a.classId))
            .filter(a => {
                if (a.dateDue == null || Date.now() < a.dateDue) return false;
                const ac = this.mActivitiScore.get(a.activityId);
                return ac == null || ac.savedDate == null;
            })
        );
    }

    async save() {
        const [err, x] = await aFetch<{}>("PUT", `/admin/student/${this.studentId}`, this.toJS());
        return [err, (err ? undefined : new Student(x))!] as const;
    }

    async create(schoolId:DbIdentity) {
        const [err, x] = await aFetch<{}>("POST", `/admin/student`, {...this.toJS(), schoolId});
        return [err, (err ? undefined : new Student(x))!] as const;
    }

    static getCumulativeGradePercentage(cumulativeGrade?:number){
        return cumulativeGrade && cumulativeGrade.toLocaleString("en", {style: "percent", minimumFractionDigits: 3});
    }

    static async fetchStudentsInDistrictAsAdmin(districtId:DbIdentity) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/admin/district/${districtId}/student`);
        return [err, (err ? undefined : parseGeneralViewModel(dto))!] as const;
    }

    static async fetchStudentsInSchoolAsFaculty(schoolId: DbIdentity, facultyId:DbIdentity) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/faculty/${facultyId}/school/${schoolId}/schoolStudent`);
        return [err, (err ? undefined : parseGeneralViewModel(dto))!] as const;
    }
    static async fetchStudentsInSchoolPaginationAsFaculty(schoolId: DbIdentity
        , facultyId:DbIdentity
        , pageIndex:number
        , pageSize:number
        , excludedClassId:DbIdentity
        , filteredClassId: DbIdentity
        , searchText:string
        , sortColumnKey: string|undefined
        , sortOrder: boolean|"ascend"|"descend"|undefined
        ) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/faculty/${facultyId}/school/${schoolId}/page/${pageIndex}/size/${pageSize}/schoolStudent`,{
            excludedClassId: excludedClassId,
            filteredClassId: filteredClassId,
            searchText:searchText,
            sortColumnKey: sortColumnKey ? sortColumnKey : "",
            sortOrder: sortOrder!=undefined ? sortOrder : "",
        });
        return [err, (err ? undefined : parseGeneralViewModel(dto))!] as const;
    }
    static async fetchStudentsInSchoolTotalPaginationAsFaculty(schoolId: DbIdentity
        , facultyId:DbIdentity
        , excludedClassId:DbIdentity
        , filteredClassId: DbIdentity
        , searchText:string) {
        const [err, dto] = await aFetch<number>("GET", `/faculty/${facultyId}/school/${schoolId}/totalSchoolStudent`,{
            excludedClassId: excludedClassId,
            filteredClassId: filteredClassId,
            searchText:searchText
        });
        return [err, (err ? undefined : dto)!] as const;
    }

    static async fetchStudentsOfClassAsFaculty(schoolId: DbIdentity, facultyId:DbIdentity, classId:DbIdentity) {
        const [err, xs] = await aFetch<{}[]>("GET", `/faculty/${facultyId}/school/${schoolId}/class/${classId}/student`);
        return [err, (err ? [] : xs.map(x => new Student(x)))] as const;
    }

    async createStudentsAsFaculty(schoolId: DbIdentity, facultyId:DbIdentity, classId:DbIdentity) {
        const [err, data] = await aFetch<{}>("POST", `/faculty/${facultyId}/school/${schoolId}/class/${classId}/student`, {...this.toJS()});
        return [err, (err ? undefined : new Student(data))!] as const;
    }

    async updateStudentsAsFaculty(schoolId: DbIdentity, facultyId:DbIdentity, classId:DbIdentity) {
        const [err, data] = await aFetch<{}>("PUT", `/faculty/${facultyId}/school/${schoolId}/class/${classId}/student/${this.studentId}`, {...this.toJS()});
        return [err, (err ? undefined : new Student(data))!] as const;
    }

    static async fetchStudentsAsFaculty(schoolId: DbIdentity, facultyId:DbIdentity) {
        const [err, data] = await aFetch<GeneralDto>("GET", `/faculty/${facultyId}/school/${schoolId}/student`);
        return [err, (err ? undefined : parseGeneralViewModel(data))!] as const;
    }

    static async fetchStudentAsFaculty(schoolId: DbIdentity, facultyId:DbIdentity, studentId:DbIdentity) {
        const [err, data] = await aFetch<GeneralDto>("GET", `/faculty/${facultyId}/school/${schoolId}/student/${studentId}`);
        const vm = err ? undefined : parseGeneralViewModel(data);

        return [err, (vm == null ? undefined : vm.students[0])!] as const;
    }

    static async fetchTeacherView({schoolId, facultyId, studentId, classId}:{schoolId:DbIdentity, facultyId:DbIdentity, studentId:DbIdentity, classId?:DbIdentity}) {
        const [err, data] = await aFetch<GeneralDto>("GET", `/faculty/${facultyId}/school/${schoolId}/student/${studentId}/teacher-view`, {classId});
        return [err, (err ? undefined : parseGeneralViewModel(data))!] as const;
    }

    static async fetchAsAdmin(studentId?:DbIdentity) {
        const [err, data] = await aFetch<GeneralDto>("GET", `/admin/student/${studentId}`);
        const vm = err ? undefined : parseGeneralViewModel(data);

        return [err, (vm == null ? undefined : vm.students[0])!] as const;
    }

    static sorter = {
        fullName     : (a?: Student, b?: Student) => (a ? a.fullName     : "").localeCompare(b ? b.fullName     : ""),
        firstName    : (a?: Student, b?: Student) => (a ? a.firstName.trim()    : "").localeCompare(b ? b.firstName.trim()    : ""),
        lastName     : (a?: Student, b?: Student) => (a ? a.lastName.trim()     : "").localeCompare(b ? b.lastName.trim()     : ""),
        studentNumber: (a?: Student, b?: Student) => (a ? a.studentNumber: "").localeCompare(b ? b.studentNumber: ""),
        grade        : (a?: Student, b?: Student) => (a ? a.grade || 0 : 0) - (b ? b.grade || 0 : 0),
        cumulativeGrade        : (a?: Student, b?: Student) => (a ? a.cumulativeGrade || 0 : 0) - (b ? b.cumulativeGrade || 0 : 0),
    }
}
