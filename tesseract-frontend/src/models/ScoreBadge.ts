import { observable } from "mobx";
import { DbIdentity, ColorString } from "./types";
import { aFetch } from "../services/api/fetch";
import { darken, lighten } from "../utils/color";
import { FiveC, FiveCValues } from "./ActivityScore";
import { TFunction } from 'i18next';

export class ScoreBadge {
    @observable communicationColor   : ColorString;
    @observable collaborationColor   : ColorString;
    @observable characterColor       : ColorString;
    @observable creativityColor      : ColorString;
    @observable criticalThinkingColor: ColorString;

    constructor(data?: {}) {
        this.communicationColor    = "";
        this.collaborationColor    = "";
        this.characterColor        = "";
        this.creativityColor       = "";
        this.criticalThinkingColor = "";

        if (data != null) Object.assign(this, data);
    }

    getBaseColor(type:FiveC): ColorString {
        switch (type) {
            case FiveC.Communication   : return this.communicationColor   ;
            case FiveC.Collaboration   : return this.collaborationColor   ;
            case FiveC.Character       : return this.characterColor       ;
            case FiveC.Creativity      : return this.creativityColor      ;
            case FiveC.CriticalThinking: return this.criticalThinkingColor;
        }
        return "";
    }
    getBadgeColor(c: FiveC, v: FiveCValues) {
        const color = this.getBaseColor(c);
        return !color ? color : ScoreBadge.getColor(color, v);
    }

    static getColor(color: string, score: FiveCValues) {
        const lightenPercentage: number = 20;
        const darkenPercentage: number = 20;
        switch (score) {
            case FiveCValues.Low   : return lighten(color, lightenPercentage);
            case FiveCValues.Medium: return color;
            case FiveCValues.High  : return darken(color, darkenPercentage);
            default: return "";
        }
    }

    static getTextColorFromBackground(hexBackground:ColorString){
        //https://codepen.io/DevillersJerome/pen/bpLPGe
        const rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexBackground);
        if(rgb) {
            const r = parseInt(rgb[1], 16);
            const g = parseInt(rgb[2], 16);
            const b = parseInt(rgb[3], 16);
            var hsp = Math.sqrt(
                0.299 * (r * r) +
                0.587 * (g * g) +
                0.114 * (b * b)
            );
            if (hsp > 127.5){
                return "#000000";
            }
        }
        return "#ffffff";
    }

    static getLevel(score: FiveCValues) {
        switch (score) {
            case 1: return "Low";
            case 2: return "Medium";
            case 3: return "High";
        }
        return "";
    }

    static async getSchoolScoreBadge(schoolId: DbIdentity) {
        const [err, x] = await aFetch<{ id: DbIdentity, value: string }[]>("GET", `/config/school/${schoolId}/BadgeColor`);
        return [err, (err ? undefined : new ScoreBadge(x))!] as const;
    }

    static async getDistrictScoreBadge(districtId: DbIdentity) {
        const [err, x] = await aFetch<{ id: DbIdentity, value: string }[]>("GET", `/config/district/${districtId}/BadgeColor`);
        return [err, (err ? undefined : new ScoreBadge(x))!] as const;
    }
}
