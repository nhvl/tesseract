import {observable, action, toJS, computed} from "mobx";

import {DbIdentity, EUserRole, DefaultId} from "./types";
import { aFetch } from "../services/api/fetch";

export class Permission {
    /* @observable */ type : string;
    /* @observable */ value: string;

    constructor(data?:{}) {
        this.type  = "";
        this.value = "";

        if (data != null) Object.assign(this, data);
    }

    static async fetchAll() {
        const [err, xs] = await aFetch<string[]>("GET", `/admin/permission`);
        return [err, (err ? [] : xs)] as const;
    }
}
