import {observable, action, toJS} from "mobx";

import {DbIdentity, DefaultId} from "./types"
import { aFetch } from "../services/api/fetch";

export enum ENotificationType {
    Email   = 1,
    SMS     = 2,
    PwaPush = 3,
}

export enum ENotificationSendType {
    DontSend   = 1,
    Imediately = 2,
    Daily      = 3,
    Weekly     = 4,
}

export class UserNotificationSetting {
    /* */       userId   : DbIdentity;
    @observable channelId: ENotificationType;
    @observable sendType : ENotificationSendType;
    @observable sendTime : number;
    @observable weeklyDay: number;

    constructor(data?:{}) {
        this.userId    = DefaultId;
        this.channelId = 0;
        this.sendType  = 0;
        this.sendTime  = 0;
        this.weeklyDay = 0;

        if (data != null) Object.assign(this, data);
    }

    toJS() {
        return toJS(this);
    }

    @action set_type      = (v: ENotificationType    ) => { this.channelId     = v };
    @action set_sendType  = (v: ENotificationSendType) => { this.sendType = v };
    @action set_sendTime  = (v: number               ) => { this.sendTime = v };
    @action set_weeklyDay = (v: number               ) => { this.weeklyDay= v };

    async save() {
        const [err, x] = await aFetch<{}>("PUT", `/notification/subscription/${this.userId}/channel/${this.channelId}`, this.toJS());
        return [err, (err ? undefined : new UserNotificationSetting(x))!] as const;
    }

    static async fetch(userId: DbIdentity) {
        const [err, xs] = await aFetch<{}[]>("GET", `/notification/subscription/${userId}`);
        return [err, err ? [] : xs.map(x => new UserNotificationSetting(x))] as const;
    }

    static async save(settings: UserNotificationSetting[]) {
        // TODO huyn 2019-9-25 currently we don't support batch save notification yet
        const rs = await Promise.all(settings.map(async st =>  await st.save()));
        const data = rs.map(rs => rs[1]);
        const error = rs.map(rs => rs[0]).find(err => err !=undefined);

        return [error, data] as const;
    }
}
