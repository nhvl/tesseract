import {observable, action, computed, toJS} from "mobx";
import { aFetch } from "../services/api/fetch";

import {DbIdentity, DefaultId} from "./types"

export interface IActivityScore {
    activityId: DbIdentity;
    score     : number;
}

export class SubmissionAttachment {
    @observable contentType: string = "";
    @observable url      : string = "";
    @observable fileName : string = "";
    constructor(data?:{}) {
        if (data != null) {
            Object.assign(this, data);
        }
    }
}

export class Submission {
                        studentId     : DbIdentity = DefaultId;
                        activityId    : DbIdentity = DefaultId;
    @observable         content       : string = "";
    @observable.shallow attachments   : SubmissionAttachment[] = [];
    @observable         createdBy     : DbIdentity = DefaultId;
    @observable         updatedBy     : DbIdentity = DefaultId;
    @observable         dateCreated   : number = 0;
    @observable         dateUpdated   : number = 0;

    constructor(data?:{studentId?:DbIdentity, activityId?:DbIdentity, attachments?:{}[]}) {
        if (data != null) {
            const {attachments, ...pData} = data;
            Object.assign(this, pData);
            if (Array.isArray(attachments)) this.attachments = attachments.map(a => new SubmissionAttachment(a));
        }
    }

    toJS() {
        return toJS(this);
    }

    @action set_content    = (v: string) => { this.content    = v }

    async update(isSubmitted:boolean, schoolId:DbIdentity) {
        const [err, x] = await aFetch<{}>("PUT", `/student/${this.studentId}/school/${schoolId}/activity/${this.activityId}/submission?isSubmitted=${isSubmitted}`, this.toJS());
        return [err, (err ? undefined : new Submission(x))!] as const;
    }

    static async getAsFaculty({schoolId, facultyId, activityId, studentId}:{schoolId:DbIdentity, facultyId:DbIdentity, activityId:DbIdentity, studentId:DbIdentity}) {
        const [err, x] = await aFetch<object>("GET", `/faculty/${facultyId}/school/${schoolId}/activity/${activityId}/student/${studentId}/submission`);
        return [err, (err ? undefined : new Submission(x))!] as const;
    }

    static async getAsStudent({schoolId, activityId, studentId}:{schoolId:DbIdentity, activityId:DbIdentity, studentId:DbIdentity}) {
        const [err, x] = await aFetch<object>("GET", `/student/${studentId}/school/${schoolId}/activity/${activityId}/submission`);
        return [err, ((err || x == null) ? undefined : new Submission(x))] as const;
    }
}
