import {observable, action, toJS} from "mobx";
import { aFetch } from "../services/api/fetch";

export class GoogleDocItem{
    @observable id          : string = "";
    @observable name        ?: string | undefined;
    @observable mimeType    : string = "";
    @observable type           : string = "";
    @observable embedUrl           ?: string | undefined;
    @observable iconUrl           ?: string | undefined;
    @observable url           ?: string | undefined;
    @observable description           ?: string | undefined;
    @observable isShared           ?: string | undefined;
    @observable lastEditedUtc        : number = 0;
    @observable serviceId           ?: string | undefined;
    @observable sizeBytes           : number = 0;
}

export class ClientInfo{
    @observable clientId            : string = "";
    @observable developerKey        : string = "";
    @observable redirectUri         : string  = "";
}

export class FilePermission{
    @observable allowFileDiscovery  : string = "";
    @observable deleted             : string = "";
    @observable displayName         : string = "";
    @observable domain              : string = "";
    @observable eTag                : string = "";
    @observable emailAddress        : string = "";
    @observable expirationTime      : string = "";
    @observable id                  : string = "";
    @observable kind                : string = "";
    @observable permissionDetails   : string = "";
    @observable photoLink           : string = "";
    @observable role                : string = "";
    @observable teamDrivePermissionDetails: string = "";
    @observable type                : string = "";
}

export class GoogleDrive {
    
    static async downloadGoogleFile(doc: GoogleDocItem) {
        const [err, url] = await aFetch<string>("POST", `/GoogleApi/drive/download`, toJS(doc));
        return [err, (err ? undefined : url)!] as const;
    }

    static async getClientInfo() {
        const [err, result] = await aFetch<ClientInfo>("GET", `/GoogleApi/auth/clientInfo`);
        return [err, (err ? undefined : result)!] as const;
    }

    static async exchangeAuthorizationCode(offlineCode: string) {
        const [err, result] = await aFetch<string>("GET", `/GoogleApi/auth/exchangeAuthenticationCode?code=${offlineCode}`);
        return [err, (err ? undefined : result)!] as const;
    }

    static async revokeToken() {
        const [err, result] = await aFetch<string>("GET", `/GoogleApi/auth/revoke`);
        return [err, (err ? undefined : result)!] as const;
    }
    
    static async getAccessToken() {
        const [err, result] = await aFetch<string>("GET", `/GoogleApi/auth/accessToken`);
        return [err, (err ? undefined : result)!] as const;
    }

    static async shareFile(fileId: string) {
        const [err, result] = await aFetch<any>("GET", `/GoogleApi/drive/sharefile?fileId=${fileId}`);
        return [err, (err ? undefined : result)!] as const;
    }

    static async getFilePermissionInfo(fileId: string) {
        const [err, result] = await aFetch<FilePermission>("GET", `/GoogleApi/drive/filePermissionInfo?fileId=${fileId}`);
        return [err, (err ? undefined : result)!] as const;
    }
}
