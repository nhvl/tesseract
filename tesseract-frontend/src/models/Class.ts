import {observable, action, computed} from "mobx";

import {DbIdentity, DefaultId} from "./types"
import { GradeRange } from "./GradeRange";

import { aFetch } from "../services/api/fetch";

export interface IClassFaculty {
    classId   : DbIdentity;
    facultyId : DbIdentity;
    sortIndex : number;
}

export class Class {
                        classId              : DbIdentity;
    @observable         schoolId             : DbIdentity;
    @observable         period               : string;
    @observable         gradingTerm          : DbIdentity;
    @observable         className            : string;
    @observable         description          : string;
    @observable         isTemplate           : boolean;
    @observable         useDefaultGradesRange: boolean;
    @observable.shallow gradeRanges          : GradeRange[];


    constructor(data?:any) {
        this.classId               = DefaultId;
        this.schoolId              = DefaultId;
        this.period                = "";
        this.gradingTerm           = DefaultId;
        this.className             = "";
        this.description           = "";
        this.useDefaultGradesRange = true;
        this.isTemplate            = false;
        this.gradeRanges           = [];

        if (data != null) {
            const {gradeRanges, ...pData} = data;
            Object.assign(this, pData);
            if (Array.isArray(gradeRanges)) this.gradeRanges = gradeRanges.map(x => new GradeRange(x));
        }
    }

    @action set_schoolId              = (v: DbIdentity) => { this.schoolId              = v }
    @action set_period                = (v: string    ) => { this.period                = v }
    @action set_gradingTerm           = (v: DbIdentity) => { this.gradingTerm           = v }
    @action set_className             = (v: string    ) => { this.className             = v }
    @action set_description           = (v: string    ) => { this.description           = v }
    @action set_useDefaultGradesRange = (v: boolean   ) => { this.useDefaultGradesRange = v }

    @computed get params() { return ({classId:String(this.classId), schoolId:String(this.schoolId)}) }

    extends(data:any) {
        Object.assign(this, data);
    }

    toJS() {
        return ({
            classId              : this.classId,
            schoolId             : this.schoolId,
            period               : this.period,
            gradingTerm          : this.gradingTerm,
            className            : this.className,
            description          : this.description,
            useDefaultGradesRange: this.useDefaultGradesRange,
            gradeRanges          : this.gradeRanges.map(i => i.toJS()),
        });
    }

    async save(facultyId:DbIdentity) {
        const body = this.toJS();

        if (this.classId < 1) {
            const [err, x] = await aFetch<{}>("POST", `/faculty/${facultyId}/school/${this.schoolId}/class`, body);
            return [err, (err ? undefined : new Class(x))!] as const;
        }

        const [err, x] = await aFetch<{}>("PUT", `/faculty/${facultyId}/school/${this.schoolId}/class/${this.classId}`, body);
        return [err, (err ? undefined : new Class(x))!] as const;
    }

    static async fetchClassAsFaculty({schoolId, facultyId, classId}:{schoolId:DbIdentity, facultyId: DbIdentity, classId:DbIdentity}) {
        const [err, x] = await aFetch<{}[]>("GET", `/faculty/${facultyId}/school/${schoolId}/class/${classId}`);
        return [err, (err ? undefined : new Class(x))!] as const;
    }

    static async createTempate({schoolId, facultyId, classId}:{schoolId:DbIdentity, facultyId: DbIdentity, classId:DbIdentity}) {
        const [err, _] = await aFetch<{}[]>("POST", `/faculty/${facultyId}/school/${schoolId}/class/${classId}/template`);
        return err;
    }

    static async getTemplates({schoolId, facultyId}:{schoolId:DbIdentity, facultyId: DbIdentity}) {
        const [err, data] = await aFetch<{}[]>("GET", `/faculty/${facultyId}/school/${schoolId}/class_templates`);
        return [err, (err ? [] : data.map(dt => new Class(dt)))] as const;
    }

    static async applyTemplate({schoolId, facultyId}:{schoolId:DbIdentity, facultyId: DbIdentity}, data:{templateId: DbIdentity, gradingTermId: DbIdentity, className: string}) {
        const [err] = await aFetch<void>("POST", `/faculty/${facultyId}/school/${schoolId}/class/create_from_template`, data);
        return err;
    }

    static async deleteTemplate({schoolId, facultyId, templateId}:{schoolId:DbIdentity, facultyId: DbIdentity, templateId: DbIdentity}) {
        return await aFetch<void>("DELETE", `/faculty/${facultyId}/school/${schoolId}/template/${templateId}`);
    }

    static sorter = {
        period   : (a?: Class, b?: Class) => ((a ? a.period   : "").localeCompare(b ? b.period: "")),
        className: (a?: Class, b?: Class) => ((a ? a.className: "").localeCompare(b ? b.className : "")),
    }
}
