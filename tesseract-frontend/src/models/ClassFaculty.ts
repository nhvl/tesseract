import {observable, action} from "mobx";

import {DbIdentity, NumberDate, DefaultId, DefaultNumberDate} from "./types"

export interface IClassFalcuty {
    classId     : DbIdentity;
    facultyId   : DbIdentity;
    sortIndex   : number;
}

export class ClassFaculty {
                classId     : DbIdentity;
                facultyId   : DbIdentity;
    @observable sortIndex   : number;

    constructor(data?:{}) {
        this.classId    = DefaultId;
        this.facultyId  = DefaultId;
        this.sortIndex  = 10000;

        if (data != null) Object.assign(this, data);
    }
}
