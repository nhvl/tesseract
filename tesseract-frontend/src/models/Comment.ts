import {observable, action, computed, toJS} from "mobx";

import {DbIdentity, DefaultId, NumberDate} from "./types"
import { aFetch } from "../services/api/fetch";
import { GeneralDto, parseGeneralViewModel, IGeneralViewModel } from "./GeneralViewModel";

export class Comment {
                commentId   : DbIdentity;
                threadId    : string;
    @observable replyTo    ?: DbIdentity;
    @observable content     : string;
    @observable createdBy  ?: DbIdentity;
    @observable dateCreated?: NumberDate;

    constructor(data?:{}) {
        this.commentId   = DefaultId;
        this.threadId    = "";
        this.replyTo     = undefined;
        this.content     = "";
        this.createdBy   = undefined;
        this.dateCreated = undefined;

        if (data != null) Object.assign(this, data);
    }

    @action set_content      = (v: string    ) => { this.content      = v }

    @computed get params() { return ({threadId:this.threadId, commentId:this.commentId}) }

    toJS() {
        return toJS(this);
    }

    async save() {
        if (this.commentId < 1) {
            const [err, x] = await aFetch<{}>("POST", `/thread/${this.threadId}/comments`, this.toJS());
            return [err, (err ? undefined : new Comment(x))!] as const;
        }

        const [err, x] = await aFetch<{}>("PUT", `/thread/${this.threadId}/comments/${this.commentId}`, this.toJS());
        return [err, (err ? undefined : new Comment(x))!] as const;
    }

    static async getCommentsOfThread(threadId:string) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/thread/${threadId}/comments`);
        return [err, (err ? undefined : parseGeneralViewModel(dto))!] as const;
    }

    static async setSeen({threadId, lastSeen}:{threadId:string, lastSeen?:number}) {
        const [err, x] = await aFetch<{}>("POST", `/thread/${threadId}/seen`, lastSeen);
        return [err, (err ? undefined : x)!] as const;
    }

    static sorter = {
        dateCreated: (a:Comment, b:Comment) => (a.dateCreated || -1) - (b.dateCreated || -1),
    }
}
