
import { DbIdentity } from "./types";
import { IUser, User } from "./User";
import { Student } from "./Student";
import { Faculty } from "./Faculty";
import { School } from "./School";
import { Class, IClassFaculty } from "./Class";
import { IClassStudent } from "./ClassStudent";
import { Activity } from "./Activity";
import { ISchoolStudent } from "./SchoolStudent";
import { IActivityScore, ActivityScore } from "./ActivityScore";
import { Discussion } from "./Discussion";
import { Comment } from "./Comment";
import { CommentSeen } from "./CommentSeen";
import { GradingTerm } from "./GradingTerm";

interface IActivity {
    activityId: DbIdentity
}

interface ISchoolFaculty {
    schoolId : DbIdentity;
    facultyId: DbIdentity;
}

export interface GeneralDto {
    users                  : IUser[],
    faculties              : {}[],
    students               : {}[],
    schools                : {}[],
    gradingTerms           : {}[],
    classes                : {}[],
    activities             : IActivity[],
    schoolFaculties        : ISchoolFaculty[],
    schoolStudents         : ISchoolStudent[],
    classStudents          : IClassStudent[],
    classFaculties         : IClassFaculty[],
    activityScores         : IActivityScore[];
    discussions            : {}[];
    comments               : {}[];
    commentSeens           : {}[];
    threadUnreads          : { threadId:string, unread:number }[];
    activityCountDiscussion: { [activityId:number]: number },
}

export interface IGeneralViewModel {
    users                  : User[],
    faculties              : Faculty[],
    students               : Student[],
    schools                : School[],
    gradingTerms           : GradingTerm[],
    classes                : Class[],
    activities             : Activity[],
    activitiesScores       : ActivityScore[],
    schoolFaculties        : ISchoolFaculty[],
    schoolStudents         : ISchoolStudent[],
    classStudents          : IClassStudent[],
    classFaculties         : IClassFaculty[],
    discussions            : Discussion[],
    comments               : Comment[],
    commentSeens           : CommentSeen[],
    threadUnreads          : { threadId: string, unread: number }[],
    activityCountDiscussion: { [activityId: number]: number },
}

export function parseGeneralViewModel(data:GeneralDto): IGeneralViewModel {
    const users            = Array.isArray(data.users                ) ? data.users                .map(u => new User                (u)) : [];
    const faculties        = Array.isArray(data.faculties            ) ? data.faculties            .map(s => new Faculty             (s)) : [];
    const students         = Array.isArray(data.students             ) ? data.students             .map(s => new Student             (s)) : [];
    const schools          = Array.isArray(data.schools              ) ? data.schools              .map(c => new School              (c)) : [];
    const gradingTerms     = Array.isArray(data.gradingTerms         ) ? data.gradingTerms         .map(c => new GradingTerm         (c)) : [];
    const classes          = Array.isArray(data.classes              ) ? data.classes              .map(c => new Class               (c)) : [];
    const activities       = Array.isArray(data.activities           ) ? data.activities           .map(a => new Activity            (a)) : [];
    const activitiesScores = Array.isArray(data.activityScores       ) ? data.activityScores       .map(s => new ActivityScore       (s)) : [];
    const discussions      = Array.isArray(data.discussions          ) ? data.discussions          .map(s => new Discussion          (s)) : [];

    const a2ac = new Map(activitiesScores.map(s => [s.activityId, s]));

    students.forEach(s => {
        const classStudents = Array.isArray(data.classStudents) ? data.classStudents.filter(cs => cs.studentId == s.studentId) : [];
        const classIds = classStudents.map(cs => cs.classId);
        s.classes = classIds;
        s.tClassStudent = classStudents;
        s.tActivityScore = activities.filter(a => classIds.includes(a.classId))
                            .map(a =>  a2ac.get(a.activityId)!).filter(Boolean)
                            ;
    });

    return ({
        users                  : users,
        faculties              : faculties,
        students               : students,
        schools                : schools,
        gradingTerms           : gradingTerms,
        classes                : classes,
        activities             : activities,
        activitiesScores       : activitiesScores,
        schoolFaculties        : data.schoolFaculties || [],
        schoolStudents         : data.schoolStudents || [],
        classFaculties         : data.classFaculties || [],
        classStudents          : data.classStudents || [],
        comments               : (data.comments || []).map(x => new Comment(x)),
        discussions            : discussions,
        commentSeens           : (data.commentSeens || []).map(x => new CommentSeen(x)),
        threadUnreads          : (data.threadUnreads || []),
        activityCountDiscussion: (data.activityCountDiscussion || {}),
    });
}
