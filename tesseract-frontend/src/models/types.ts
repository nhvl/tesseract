export type NumberDate = number;
export interface StringDate    extends String {};
export interface JSONString<T> extends String {};
export interface HtmlString    extends String {};
export interface UrlString     extends String {};
export interface ColorString   extends String {};

export type DbIdentity = number;
export interface DocId      extends String {};

export const DefaultId:DbIdentity = -1;
export const DefaultNumberDate:NumberDate = -1;

export enum EUserRole {
    Unknow      = "",
    Faculty     = "Faculty",
    Admin       = "Admin",
    Student     = "Student",
    Parent      = "Parent",
    SchoolAdmin = "SchoolAdmin"
}

export enum EPermission {
    CanEditClass      = "CanEditClass",
    CanEditSchoolRole = "CanEditSchoolRole",
    CanAddStudent     = "CanAddStudent",
}

