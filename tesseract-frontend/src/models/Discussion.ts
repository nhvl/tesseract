import {observable, action, computed, toJS} from "mobx";

import {DbIdentity, DefaultId, NumberDate, UrlString} from "./types";
import {Activity} from "./Activity";
import { aFetch, uploadFile } from "../services/api/fetch";
import { GeneralDto, parseGeneralViewModel } from "./GeneralViewModel";
import { UploadFile } from "antd/lib/upload/interface";
import { MediaTypeEnum } from "./MediaTypeEnum";
import { message } from "antd";

export class Discussion {
    /* */       classId     : DbIdentity;
    /* */       activityId  : DbIdentity;
    /* */       discussionId: DbIdentity;
    @observable title       : string;
    @observable content     : string;
    @observable link        : UrlString;
    @observable contentType : string;
    @observable mediaType   : MediaTypeEnum;
    @observable startTime  ?: NumberDate;
    @observable endTime    ?: NumberDate;
    @observable threadId    : string;
    @observable createdBy  ?: DbIdentity;
    @observable dateCreated?: NumberDate;

    @observable isGraded    : boolean;
    @observable discussionActivityId : DbIdentity;
    @observable color       : string  = "#ffffff"  ;
    @observable maxScore    : number  = 10         ;
    @observable weight      : number  = 1          ;

    constructor(data?:any) {
        this.classId      = DefaultId;
        this.activityId   = DefaultId;
        this.discussionId = DefaultId;
        this.title        = "";
        this.content      = "";
        this.link         = "";
        this.contentType  = "";
        this.mediaType    = MediaTypeEnum.Unknown;
        this.startTime    = undefined;
        this.endTime      = undefined;
        this.threadId     = "";
        this.createdBy    = undefined;
        this.dateCreated  = undefined;

        this.isGraded     = false;

        this.discussionActivityId = DefaultId;

        if (data != null) {
            Object.assign(this, data);
        }
    }

    toJS() {
        const {origLink, mediaFile, ...data} = toJS(this);
        return data;
    }
    clone() { return new Discussion(this.toJS()) }

    @action set_title     = (v : string    ) => { this.title     = v }
    @action set_content   = (v : string    ) => { this.content   = v }
    @action set_startTime = (v?: NumberDate) => { this.startTime = v }
    @action set_endTime   = (v?: NumberDate) => { this.endTime   = v }
    @action set_classId   = (v : DbIdentity) => { this.classId   = v }
    @action set_threadId  = (v : string    ) => { this.threadId  = v }
    @action set_link      = (v : string    ) => { this.link      = v }

    @action set_color            = (v: string           ) => { this.color            = v; }
    @action set_isGraded         = (v: boolean          ) => { this.isGraded         = v; }
    @action set_maxScore         = (v: number           ) => { this.maxScore         = v; }
    @action set_weight           = (v: number           ) => { this.weight           = v; }

    @observable origLink:string = "";
    @observable.ref mediaFile?: UploadFile;
    @action set_mediaFile = (v?:UploadFile) => {
        if (v != null) {
            const file: File = (v.originFileObj || v) as any;
            if (file.type.startsWith("video/")) {
                v.url = URL.createObjectURL(file);
                this.mediaType = MediaTypeEnum.VideoFile;
            } else if (file.type.startsWith("image/")) {
                v.thumbUrl = v.url = URL.createObjectURL(file);
                this.mediaType = MediaTypeEnum.ImageFile;
            } else if (file.type.startsWith("audio/")) {
                v.url = URL.createObjectURL(file);
                this.mediaType = MediaTypeEnum.AudioFile;
            } else {
                message.warning(`Unsupported file: ${file.name} (${file.type})`);
                return;
            }

            this.link = "";
            this.contentType = file.type;
        }
        this.mediaFile = v;
    }
    @action clearMediaType() {
        this.contentType = "";
        this.mediaType = MediaTypeEnum.Unknown;
    }

    @computed get isPublish() { return (this.startTime != null && this.startTime < Date.now()) }
    @computed get isClosed () { return (this.endTime   != null && this.endTime   < Date.now()) }

    @computed get params() { return ({
        classId     : String(this.classId),
        discussionId: String(this.discussionId),
        threadId    : String(this.threadId),
    }) }

    async save(schoolId:DbIdentity, facultyId:DbIdentity) {
        if (this.mediaFile != null) {
            const f = this.mediaFile.originFileObj || (this.mediaFile as any as File);
            const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
            if (err) return [err, undefined!] as const;
            this.set_mediaFile(undefined);
            this.set_link(url);
        }

        if (this.discussionId == DefaultId) {
            const [err, dto] = await aFetch<GeneralDto>("POST", `/faculty/${facultyId}/school/${schoolId}/class/${this.classId}/discussion`, this.toJS());
            const vm = err ? undefined : parseGeneralViewModel(dto);
            return [err, vm!] as const;
        }

        const [err, dto] = await aFetch<GeneralDto>("PUT", `/faculty/${facultyId}/school/${schoolId}/class/${this.classId}/discussion/${this.discussionId}`, this.toJS());
        const vm = err ? undefined : parseGeneralViewModel(dto);
        return [err, vm!] as const;
    }

    //#region API AsFaculty
    static async getDiscussionsOfClassAsFaculty({schoolId, facultyId, classId}:{schoolId:DbIdentity, facultyId:DbIdentity, classId:DbIdentity}) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/faculty/${facultyId}/school/${schoolId}/class/${classId}/discussion`);
        const vm = err ? undefined : parseGeneralViewModel(dto);
        return [err, vm!] as const;
    }

    static async getDiscussionsOfActivityAsFaculty({schoolId, facultyId, activityId}:{schoolId:DbIdentity, facultyId:DbIdentity, activityId:DbIdentity}) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/faculty/${facultyId}/school/${schoolId}/activity/${activityId}/discussion`);
        const vm = err ? undefined : parseGeneralViewModel(dto);
        return [err, vm!] as const;
    }

    static async getDiscussionFromDiscussionActivityAsFaculty({schoolId, facultyId, discussionActivityId}:{schoolId:DbIdentity, facultyId:DbIdentity, discussionActivityId:DbIdentity}){
        const [err, x] = await aFetch<Discussion>("GET", `/faculty/${facultyId}/school/${schoolId}/discussionActivity/${discussionActivityId}/discussion`);
        return [err, (err ? undefined : new Discussion(x))!] as const;
    }

    static async getDiscussionAsFaculty({schoolId, facultyId, classId, discussionId}:{schoolId:DbIdentity, facultyId:DbIdentity, classId:DbIdentity, discussionId:DbIdentity}) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/faculty/${facultyId}/school/${schoolId}/class/${classId}/discussion/${discussionId}`);
        const vm = err ? undefined : parseGeneralViewModel(dto);
        return [err, vm!] as const;
    }
    //#endregion

    //#region API AsStudent
    static async getDiscussionsOfClassAsStudent({schoolId, studentId, classId}:{schoolId:DbIdentity, studentId:DbIdentity, classId:DbIdentity}) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/student/${studentId}/school/${schoolId}/class/${classId}/discussion`);
        const vm = err ? undefined : parseGeneralViewModel(dto);
        return [err, vm!] as const;
    }

    static async getDiscussionsOfActivityAsStudent({schoolId, studentId, activityId}:{schoolId:DbIdentity, studentId:DbIdentity, activityId:DbIdentity}) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/student/${studentId}/school/${schoolId}/activity/${activityId}/discussion`);
        const vm = err ? undefined : parseGeneralViewModel(dto);
        return [err, vm!] as const;
    }

    static async getDiscussionAsStudent({schoolId, studentId, classId, discussionId}:{schoolId:DbIdentity, studentId:DbIdentity, classId:DbIdentity, discussionId:DbIdentity}) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/student/${studentId}/school/${schoolId}/class/${classId}/discussion/${discussionId}`);
        const vm = err ? undefined : parseGeneralViewModel(dto);
        return [err, vm!] as const;
    }
    //#endregion

    static sorter = {
        title      : (a:Discussion, b:Discussion) => (a.title.localeCompare(b.title)),
        dateCreated: (a:Discussion, b:Discussion) => (a.dateCreated || -1) - (b.dateCreated || -1),
    }
}
