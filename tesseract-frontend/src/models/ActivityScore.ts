import {observable, action, computed, toJS} from "mobx";
import { aFetch } from "../services/api/fetch";

import {DbIdentity, DefaultId} from "./types"
import { GeneralDto, parseGeneralViewModel } from "./GeneralViewModel";
import { TFunction } from 'i18next';

export enum FiveC {
    Communication    = "Communication",
    Collaboration    = "Collaboration",
    Character        = "Character",
    Creativity       = "Creativity",
    CriticalThinking = "Critical Thinking",
}

export enum FiveCValues {
    Unknown = 0,
    Low     = 1,
    Medium  = 2,
    High    = 3,
}

export interface IActivityScore {
    activityId: DbIdentity;
    score     : number;
}

export class ActivityScore {
    /* */       studentId       : DbIdentity;
    /* */       activityId      : DbIdentity;
    @observable score          ?: number;
    @observable isExclude       : boolean;
    @observable savedDate      ?: number;
    @observable gradeDate      ?: number;
    @observable isSubmitted     : boolean;
    @observable communication   : FiveCValues;
    @observable collaboration   : FiveCValues;
    @observable character       : FiveCValues;
    @observable creativity      : FiveCValues;
    @observable criticalThinking: FiveCValues;
    @observable threadId        : string;

    constructor(data?:{}) {
        this.studentId        = DefaultId;
        this.activityId       = DefaultId;
        this.gradeDate        = undefined;
        this.score            = undefined;
        this.isExclude        = false;
        this.savedDate        = undefined;
        this.isSubmitted      = false;

        this.communication    = FiveCValues.Unknown;
        this.collaboration    = FiveCValues.Unknown;
        this.character        = FiveCValues.Unknown;
        this.creativity       = FiveCValues.Unknown;
        this.criticalThinking = FiveCValues.Unknown;

        this.threadId         = "";

        if (data != null) Object.assign(this, data);
    }

    toJS() {
        return toJS(this);
    }
    clone() {
        return new ActivityScore(this.toJS());
    }
    @computed get emptyBadges(): FiveC[] {
        let result: FiveC[] = [];
        if(this.communication == null || this.communication < 1){
            result.push(FiveC.Communication);
        }
        if(this.collaboration == null || this.collaboration < 1){
            result.push(FiveC.Collaboration);
        }
        if(this.character == null || this.character < 1){
            result.push(FiveC.Character);
        }
        if(this.creativity == null || this.creativity < 1){
            result.push(FiveC.Creativity);
        }
        if(this.criticalThinking == null || this.criticalThinking < 1){
            result.push(FiveC.CriticalThinking);
        }
        return result;
    }

    scoreBadge(cType:FiveC) {
        switch (cType) {
            case FiveC.Communication:
                return this.communication;
            case FiveC.Collaboration:
                return this.collaboration;
            case FiveC.Character:
                return this.character;
            case FiveC.Creativity:
                return this.creativity;
            case FiveC.CriticalThinking:
                return this.criticalThinking;
            default:
                return FiveCValues.Unknown;
        }
    }

    static getFiveCLocale(fiveC: FiveC | string, t:TFunction) {
        switch (fiveC) {
            case FiveC.Character:       return t('app.activities.score.Character');
            case FiveC.Communication:   return t('app.activities.score.Communication');
            case FiveC.Collaboration:   return t('app.activities.score.Collaboration');
            case FiveC.Creativity:      return t('app.activities.score.Creativity');
            case FiveC.CriticalThinking:return t('app.activities.score.CriticalThinking');
        }
        return "";
    }

    @action set_scoreBadge = (v: number, cType: FiveC) => {
        switch (cType) {
            case FiveC.Communication:
                this.set_communication(v);
                break;
            case FiveC.Collaboration:
                this.set_collaboration(v);
                break;
            case FiveC.Character:
                this.set_character(v);
                break;
            case FiveC.Creativity:
                this.set_creativity(v);
                break;
            case FiveC.CriticalThinking:
                this.set_criticalThinking(v);
                break;
            default:
                return;
        }
    }

    @action set_score            = (v?: number ) => { this.score = v ? Number(v.toFixed(2)) : v }
    @action set_isExclude        = (v : boolean) => { this.isExclude = v }
    @action set_communication    = (v : number ) => { this.communication    = v }
    @action set_collaboration    = (v : number ) => { this.collaboration    = v }
    @action set_character        = (v : number ) => { this.character        = v }
    @action set_creativity       = (v : number ) => { this.creativity       = v }
    @action set_criticalThinking = (v : number ) => { this.criticalThinking = v }

    async update(schoolId:DbIdentity, facultyId:DbIdentity) {
        const [err, data] = await aFetch<number|undefined>("POST", `/faculty/${facultyId}/school/${schoolId}/activity/${this.activityId}/student/${this.studentId}/SetGrade`, this.toJS());
        return [err, (err ? undefined : data)!] as const;
    }

    static async fetchAsFaculty(schoolId:DbIdentity, facultyId:DbIdentity, activityId:DbIdentity, studentId:DbIdentity) {
        const [err, x] = await aFetch<{}>("GET", `/faculty/${facultyId}/school/${schoolId}/activity/${activityId}/student/${studentId}`);
        return [err, (err ? undefined : new ActivityScore(x))!] as const;
    }

    static async fetchActivityScoresOfClassAsFaculty(schoolId:DbIdentity, facultyId:DbIdentity, classId:DbIdentity) {
        const [err, xs] = await aFetch<{}[]>("GET", `/faculty/${facultyId}/school/${schoolId}/class/${classId}/activityScore`);
        return [err, err ? [] : xs.map(a => new ActivityScore(a))] as const;
    }

    static async fetchActivityScoresOfActivityAsFaculty(schoolId:DbIdentity, facultyId:DbIdentity, activityId:number) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/faculty/${facultyId}/school/${schoolId}/activity/${activityId}/ActivityScore`);
        return [err, (err ? undefined : parseGeneralViewModel(dto))!] as const;
    }

    static sorter = {
        score: <T extends ActivityScore>(a:T, b:T) => ((a.score || -1) - (b.score || -1)),
    };
}

export class ActivityScoreInfo extends ActivityScore {
    @observable fullName     : string;
    @observable externalId   : string;
    @observable isLate       : boolean;
    constructor(data?:{}) {
        super(data);
        this.isLate = false;
        this.fullName = "";
        this.externalId = "";
        if (data != null)
        {
            Object.assign(this, data);
        }
    }

    clone() {
        return new ActivityScoreInfo(this.toJS());
    }

    static sorter = {
        ...ActivityScore.sorter,
        fullName: (a:ActivityScoreInfo, b:ActivityScoreInfo) => (a.fullName.localeCompare(b.fullName)),
        externalId: (a:ActivityScoreInfo, b:ActivityScoreInfo) => (a.externalId.localeCompare(b.externalId)),
    }
}
