import {observable, action} from "mobx";

import { DbIdentity } from "./types";

import { aFetch } from "../services/api/fetch";

export class GradeRange {
    @observable letterGrade: string = "";
    @observable percentGrade: number = 0;

    constructor(data?:{}) {
        if (data != null) Object.assign(this, data);
    }

    @action set_letterGrade  = (v: string) => { this.letterGrade  = v; }
    @action set_percentGrade = (v: number) => { if(v <= 0) v = 1; this.percentGrade = v; }

    static async getDistrictGradeRange(districtId:DbIdentity) {
        const [err, data] = await aFetch<GradeRange[]>("GET", `/config/district/${districtId}/GradeRange`);
        return [err, (err ? [] : data.map(a => new GradeRange(a)))] as const;
    }

    static async getSchoolGradeRange(schoolId:DbIdentity) {
        const [err, data] = await aFetch<GradeRange[]>("GET", `/config/school/${schoolId}/GradeRange`);
        return [err, (err ? [] : data.map(a => new GradeRange(a)))] as const;
    }
    static async setSchoolGradeRange(schoolId:DbIdentity, gradeRange:GradeRange[]){
        const [err, data] = await aFetch<GradeRange[]>("POST", `/config/school/${schoolId}/GradeRange`, gradeRange.map(g=>g.toJS()));
        return [err, (err ? [] : data.map(a => new GradeRange(a)))] as const;
    }

    static retrieveLetterGrade(gradeRanges: GradeRange[], percentGrade: number | undefined) {
        if (percentGrade == null || gradeRanges.length < 1) return "";

        percentGrade = percentGrade * 100;

        if(percentGrade >= gradeRanges[0].percentGrade) return gradeRanges[0].letterGrade;

        for (var i = 1; i < gradeRanges.length; i++) {
            const [a, b] = [gradeRanges[i], gradeRanges[i - 1]]
            if (a.percentGrade < percentGrade && percentGrade <= b.percentGrade)
                return b.letterGrade;
        }
        return gradeRanges[gradeRanges.length - 1].letterGrade;
    }

    toJS() {
        return ({
            letterGrade    : this.letterGrade,
            percentGrade   : this.percentGrade
        });
    }
}
