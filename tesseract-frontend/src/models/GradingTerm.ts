import {observable, action, runInAction, toJS} from "mobx";

import {DbIdentity, DefaultId, NumberDate} from "./types"
import { aFetch } from "../services/api/fetch";

export class GradingTerm {
    /* */       gradingTermId: DbIdentity;
    @observable schoolId     : DbIdentity;
    @observable name         : string;
    @observable startDate    : NumberDate;
    @observable endDate      : NumberDate;

    constructor(data?:{}) {
        this.gradingTermId = DefaultId;
        this.schoolId      = DefaultId;
        this.name          = "";
        this.startDate     = 0;
        this.endDate       = 0;

        if (data != null) Object.assign(this, data);
    }

    toJS() {
        return toJS(this);
    }

    clone() {
        return new GradingTerm(this.toJS());
    }

    @action set_name      = (v: string    ) => { this.name      = v }
    @action set_startDate = (v: NumberDate) => { this.startDate = v }
    @action set_endDate   = (v: NumberDate) => { this.endDate   = v }

    async save() {
        if (this.gradingTermId < 1) {
            const [err, x] = await aFetch<GradingTerm>("POST", `/admin/schools/${this.schoolId}/grading-terms`, this.toJS());
            return [err, (err ? undefined : new GradingTerm(x))!] as const;
        }

        const [err, x] = await aFetch<GradingTerm>("PUT", `/admin/schools/${this.schoolId}/grading-terms/${this.gradingTermId}`, this.toJS());
        return [err, (err ? undefined : new GradingTerm(x))!] as const;
    }

    async remove() {
        const [err,] = await aFetch<GradingTerm>("DELETE", `/admin/schools/${this.schoolId}/grading-terms/${this.gradingTermId}`);
        return [err, this] as const;
    }

    static sorter = {
        name     : (a: GradingTerm, b: GradingTerm) => a.name.localeCompare(b.name),
        startDate: (a: GradingTerm, b: GradingTerm) => ((a.startDate || -1) - (b.startDate || -1)),
        endDate  : (a: GradingTerm, b: GradingTerm) => ((a.endDate || -1) - (b.endDate || -1)),
    }
}
