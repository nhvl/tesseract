import {observable, action, computed, toJS} from "mobx";

import {DbIdentity, DefaultId, NumberDate} from "./types"

export class CommentSeen {
    /* */       threadId    : string;
    /* */       userId      : DbIdentity;
    @observable lastSeen    : NumberDate;

    constructor(data?:{}) {
        this.threadId    = "";
        this.userId      = DefaultId;
        this.lastSeen    = -1;

        if (data != null) Object.assign(this, data);
    }

    toJS() {
        return toJS(this);
    }
}
