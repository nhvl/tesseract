import {observable, action, runInAction, toJS, computed} from "mobx";
import {ActItem, IActItem, IMatchQuizItem, IActOption, MatchQuizzItem, ActOption, PollQuizItem} from './ActDoc';
import { DocId, DbIdentity, UrlString, HtmlString, DefaultId, NumberDate } from "./types";
import { aFetch } from "../services/api/fetch";
import { createError } from "../services/api/AppError";

interface IStudentActItem extends IActItem {
    fromTeacher    : boolean;
    textAnswer     : string;
    matchQuizzAnswer: IMatchQuizItem[];
    optionsAnswer   : IActOption[];
    pollItemsAnswer?: number|undefined;
    numberAnswer?  : number|undefined;
}

interface IStudentActDoc {
    id        : DocId;
    activityId: DbIdentity;

    title     : string;
    banner    : UrlString;
    summary   : HtmlString;

    studentId : number;
    items     : IStudentActItem[];
}

export class StudentActItem extends ActItem implements IStudentActItem {
    @observable fromTeacher      : boolean          = true;
    @observable textAnswer       : string           = "";
    @observable matchQuizzAnswer : MatchQuizzItem[] = [];
    @observable optionsAnswer    : ActOption[]      = [];
    @observable pollItemsAnswer ?: number|undefined = undefined;
    @observable numberAnswer    ?: number|undefined = undefined;


    constructor(data:any) {
        super(data);
        if (data != null) {
            const {
                matchQuizzAnswer, optionsAnswer,
                fromTeacher, textAnswer, pollItemsAnswer, numberAnswer,
            } = data;
            this.matchQuizzAnswer = (Array.isArray(matchQuizzAnswer) && matchQuizzAnswer.length == this.matchQuizzItems.length)
                ? matchQuizzAnswer.map(opt => new MatchQuizzItem(opt))
                : this.matchQuizzItems.map(opt => new MatchQuizzItem(opt));

            if (Array.isArray(optionsAnswer)) this.optionsAnswer = optionsAnswer.map(i => new ActOption(i));
            Object.assign(this, {fromTeacher, textAnswer, pollItemsAnswer, numberAnswer});
        }
    }
    update_matchQuizzAnswer(matchAnswer: MatchQuizzItem[]) {
        for(var i = 0; i < matchAnswer.length; i++) {
            this.matchQuizzAnswer[i].right = matchAnswer[i].right;
            this.matchQuizzAnswer[i].rightImage = matchAnswer[i].rightImage;
        }
    }
    get_optionsAnswer(item: ActOption){
        for(var i = 0; i < this.optionsAnswer.length; i++){
            if(this.optionsAnswer[i].isEqual(item)){
                return true;
            }
        }
        return false;
    }
    @action set_optionsAnswer = (item: ActOption, v: boolean) => {
        for(var i = 0; i < this.optionsAnswer.length; i++){
            if(this.optionsAnswer[i].isEqual(item)) {
                if(!v) this.optionsAnswer.splice(i, 1);
                return;
            }
        }
        if(!this.isMultipleAnswer) {
            this.optionsAnswer = [];
        }
        this.optionsAnswer.push(new ActOption({label:item.label, image:item.image}));
    }
    @action set_pollItemsAnswer = (index: number) => { this.pollItemsAnswer = index; }
    @action set_numericAnswer = (v: number|undefined) => { this.numberAnswer = v; }
    @action set_textAnswer = (v: string) => { this.textAnswer = v; }
    toJS() {
        return ({
            id             : this.id,
            type           : this.type,
            title          : this.title,
            image          : this.image,
            content        : this.content,
            startDate      : this.startDate,
            endDate        : this.endDate,
            link           : this.link,
            contentType    : this.contentType,
            mediaType      : this.mediaType,
            embedLink       : this.embedLink,
            embedContentType : this.embedContentType,
            embedId         : this.embedId,
            embedApp        : this.embedApp,
            matchQuizzItems: this.matchQuizzItems.map(i => i.toJS()),
            numericMin     : this.numericMin,
            numericMax     : this.numericMax,
            isMultipleAnswer: this.isMultipleAnswer,
            options        : this.options == undefined ? undefined : this.options.map(opt => new ActOption(opt).toJS()),
            pollItems      : this.pollItems == undefined ? undefined : this.pollItems.map(opt => new PollQuizItem(opt).toJS()),
            fromTeacher    : this.fromTeacher,
            textAnswer     : this.textAnswer,
            matchQuizzAnswer: this.matchQuizzAnswer.map(opt=>opt.toJS()),
            optionsAnswer  : this.optionsAnswer.map(opt => opt.toJS()),
            pollItemsAnswer: this.pollItemsAnswer,
            numberAnswer   : this.numberAnswer,
        });
    }

}

export class StudentActDoc implements IStudentActDoc {
                        id         : DocId            = "";
                        activityId : DbIdentity       = DefaultId;
                        studentId  : DbIdentity       = DefaultId;
    @observable         title      : string           = "";
    @observable         banner     : UrlString        = "";
    @observable         summary    : HtmlString       = "";
    @observable.shallow items      : StudentActItem[] = [];
    @observable.shallow comments   : DocId[]          = [];
    /* */               dateCreated: NumberDate       = 0;

    constructor(data:any) {
        if (data != null) {
            const {items, ...pData} = data;
            if (Array.isArray(items)) this.items = items.map(item => new StudentActItem(item))
            Object.assign(this, pData);
        }
    }
    toJS(): IStudentActDoc {
        return ({
            id        : this.id,
            activityId: this.activityId,
            studentId : this.studentId,
            title     : this.title,
            banner    : this.banner,
            summary   : this.summary,
            items     : this.items.map(item => item.toJS()),
            // comments  : this.comments,
        });
    }
    async save(schoolId: DbIdentity) {
        if (this.activityId < 1) return [createError(new Error(`Invalid activityId`), 400), this] as const;
        const [err, x] = await aFetch<IStudentActDoc>("PUT", `/student/${this.studentId}/school/${schoolId}/activity/${this.activityId}/doc`, this.toJS())
        return [err, (err ? undefined : new StudentActDoc(x))!] as const;
    }

    static async fetchForFaculty({schoolId, facultyId, activityId, studentId}:{schoolId: DbIdentity, facultyId: DbIdentity, activityId:DbIdentity, studentId:DbIdentity}) {
        const [err, x] = await aFetch<IStudentActDoc>("GET", `/faculty/${facultyId}/school/${schoolId}/student/${studentId}/activity/${activityId}/doc`);
        const d = ((err || !x) ? undefined : new StudentActDoc(x));
        return [err, (err ? undefined : new StudentActDoc(x))!] as const;
    }

    static async fetchForStudent(schoolId: DbIdentity, activityId:DbIdentity, studentId:DbIdentity) {
        const [err, x] = await aFetch<IStudentActDoc>("GET", `/student/${studentId}/school/${schoolId}/activity/${activityId}/doc`);
        return [err, (err ? undefined : new StudentActDoc(x))!] as const;
    }

    static async pullAsStudent({schoolId, activityId, studentId}:{schoolId: DbIdentity, activityId:DbIdentity, studentId:DbIdentity}) {
        const [err, x] = await aFetch<IStudentActDoc>("POST", `/student/${studentId}/school/${schoolId}/activity/${activityId}/doc/pull`);
        return [err, (err ? undefined : new StudentActDoc(x))!] as const;
    }
}
