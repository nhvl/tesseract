
export enum MediaTypeEnum {
    Unsupport = -1,
    Unknown   = 0,
    ImageFile = 1,
    VideoFile = 2,
    AudioFile = 3,
    VideoLink = 4,
    AudioLink = 5,
    ImageLink = 6,
    DocumentFile = 7,
    DocumentLink = 7,
}
