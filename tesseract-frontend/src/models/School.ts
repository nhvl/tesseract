import {observable, action, runInAction, computed} from "mobx";

import { DbIdentity, DefaultId } from "./types";
import { GradingTerm } from "./GradingTerm";

import { aFetch } from "../services/api/fetch";

import { UploadFile } from 'antd/lib/upload/interface';

import logo from '../logo.png';

export class School {
                schoolId            : DbIdentity;
    @observable schoolName          : string;
    @observable currentGradingTerm  : DbIdentity;
    @observable districtId          : DbIdentity;
    @observable logoUrl             : string;
    @observable iconUrl             : string;

    @observable.shallow tGradingTerm: GradingTerm[] = [];

    constructor(data?:any) {
        this.schoolId           = DefaultId;
        this.schoolName         = "";
        this.currentGradingTerm = DefaultId;
        this.districtId         = DefaultId;
        this.logoUrl            = logo;
        this.iconUrl            = "";

        if (data != null) {
            const {tGradingTerm, ...pData} = data;
            Object.assign(this, pData);
            if (Array.isArray(tGradingTerm)) this.tGradingTerm = tGradingTerm.map(x => new GradingTerm(x));
        }

        if(!this.logoUrl) this.logoUrl = logo;
    }

    @action set_logoUrl = (v: string) => { this.logoUrl = v; }
    @action set_iconUrl = (v: string) => { this.iconUrl = v; }

    @computed get currentGradingTermObject(){
        return this.tGradingTerm.find(c => c.gradingTermId==this.currentGradingTerm);
    }

    toJS() {
        return ({
            schoolId           : this.schoolId,
            schoolName         : this.schoolName,
            currentGradingTerm : this.currentGradingTerm,
            districtId         : this.districtId,
            logoUrl            : this.logoUrl,
            iconUrl            : this.iconUrl,
        });
    }

    @computed get params() { return ({schoolId:String(this.schoolId), districtId:String(this.districtId)}) }

    async save(currentGradingTerm?: GradingTerm) {
        const body = {...this.toJS()};
        if (this.schoolId < 1) {
            const [err, x] = await aFetch<{}[]>("POST", `/admin/school`, {
                schoolName      : this.schoolName,
                logoUrl         : this.logoUrl,
                iconUrl         : this.iconUrl,
                gradingTermName : currentGradingTerm!.name,
                startDate       : currentGradingTerm!.startDate.valueOf(),
                endDate         : currentGradingTerm!.endDate.valueOf()
            });
            return [err, (err ? undefined : new School(x))!] as const;
        }

        const [err, x] = await aFetch<{}[]>("PUT", `/admin/school/${this.schoolId}`, body);
        return [err, (err ? undefined : new School(x))!] as const;
    }

    static async getSchool(schoolId:DbIdentity) {
        const [err, data] = await aFetch("GET", `/School/${schoolId}`);
        const school = (err ? undefined : new School(data));
        return [err, school!] as const;
    }

    static async getSchoolsAsFaculty(facultyId:DbIdentity) {
        const [err, xs] = await aFetch<{}[]>("GET", `/faculty/${facultyId}/school`);
        return [err, err ? [] : xs.map(x => new School(x))] as const;
    }

    static async getSchoolAsStudent(studentId:DbIdentity, schoolId:DbIdentity) {
        const [err, x] = await aFetch<{}>("GET", `/student/${studentId}/school/${schoolId}`);
        return [err, (err ? undefined : new School(x))!] as const;
    }

    static async getSchoolsOfDistrictAsAdmin(districtId:DbIdentity) {
        const [err, xs] = await aFetch<{}[]>("GET", `/admin/district/${districtId}/school`);
        return [err, err ? [] : xs.map(x => new School(x))] as const;
    }
    static async getSchoolAsAdmin({districtId, schoolId}:{districtId:DbIdentity, schoolId:DbIdentity}) {
        const [err, x] = await aFetch<{}>("GET", `/admin/district/${districtId}/school/${schoolId}`);
        return [err, (err ? undefined : new School(x))!] as const;
    }

    static sorter = {
        schoolName: (a?:School, b?:School) => (a ? a.schoolName : "").localeCompare(b ? b.schoolName : ""),
    }
}

export class EditableSchool extends School {
    @observable.ref logoFile?: UploadFile|undefined;
    @action set_logoFile = (v?:UploadFile|undefined) => { this.logoFile = v; if(!v) this.logoUrl = ""; }
    @action clear_logo = () => { this.logoUrl = ""; this.logoFile = undefined; }

    @observable.ref iconFile?: UploadFile|undefined;
    @action set_iconFile = (v?:UploadFile|undefined) => { this.iconFile = v }
    @action clear_icon = () => { this.iconUrl = ""; this.iconFile = undefined; }

    @action set_schoolName          = (v: string    ) => { this.schoolName         = v }
    @action set_currentGradingTerm = (v: DbIdentity) => { this.currentGradingTerm = v }

    static from(school:School) {
        return new EditableSchool({
            ...school.toJS(),
            tGradingTerm:school.tGradingTerm.map(t => t.toJS()),
        });
    }

    toSchool() {
        return new School({
            ...this.toJS(),
            tGradingTerm:this.tGradingTerm.map(t => t.toJS()),
        });
    }
}
