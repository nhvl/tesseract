import { DbIdentity } from "./types"
import { aFetch } from "../services/api/fetch";
import { FiveCValues } from "./ActivityScore";

export interface IFiveCScore {
    communication   : { [key:number]: number },
    collaboration   : { [key:number]: number },
    character       : { [key:number]: number },
    creativity      : { [key:number]: number },
    criticalThinking: { [key:number]: number },
}

export interface ISchoolReport {
    schoolId     : DbIdentity,
    gradingTerm  : DbIdentity,
    activityCount: { "active": number, "passive": number },
    facultyCount : { "active": number, "passive": number },
    studentCount : { "active": number, "passive": number },
    fiveCScore   : IFiveCScore,
}

export interface IDistrictReport {
    districtId   : DbIdentity,
    schoolReports: ISchoolReport[],
    facultyCount : { "active": number, "passive": number },
}

export async function getSchoolReport({schoolId, gradingTermId}:{schoolId: DbIdentity, gradingTermId:DbIdentity}) {
    const [error, data]  = await aFetch<ISchoolReport>("GET", `/admin/report/school/${schoolId}/term/${gradingTermId}`);
    return [error, (error ? undefined : data)!] as const;
}

export async function getReport() {
    const [error, data]  = await aFetch<IDistrictReport>("GET", `/admin/report`);
    return [error, (error ? undefined : data)!] as const;
}

export async function getDistrictReport() {
    const [err, report] = await getReport();
    const stats = (err ? undefined : toStatistic(report))!;
    return [err, stats] as const;
}

const fiveCValues = [FiveCValues.High, FiveCValues.Medium, FiveCValues.Low,];

export function toStatistic(report: IDistrictReport) {
    const nActiveSchools = report.schoolReports.filter(s => (s.activityCount.active + s.activityCount.passive) > 0).length;
    return ({
        nActiveSchools  : nActiveSchools,
        nInactiveSchools: report.schoolReports.length - nActiveSchools,
        nOpenAssignments: report.schoolReports.map(s => s.activityCount.active).reduce((s, i) => s + i, 0),
        nActiveTeachers : report.facultyCount.active,
        nActiveStudents : report.schoolReports.map(s => s.studentCount .active).reduce((s, i) => s + i, 0),
        collaboration   : fiveCValues.reduce((m, v) => ({...m, [v]: report.schoolReports.map(s => s.fiveCScore.collaboration   [v] || 0).reduce((s, i) => s + i, 0) }), {}),
        communication   : fiveCValues.reduce((m, v) => ({...m, [v]: report.schoolReports.map(s => s.fiveCScore.communication   [v] || 0).reduce((s, i) => s + i, 0) }), {}),
        creativity      : fiveCValues.reduce((m, v) => ({...m, [v]: report.schoolReports.map(s => s.fiveCScore.creativity      [v] || 0).reduce((s, i) => s + i, 0) }), {}),
        character       : fiveCValues.reduce((m, v) => ({...m, [v]: report.schoolReports.map(s => s.fiveCScore.character       [v] || 0).reduce((s, i) => s + i, 0) }), {}),
        criticalThinking: fiveCValues.reduce((m, v) => ({...m, [v]: report.schoolReports.map(s => s.fiveCScore.criticalThinking[v] || 0).reduce((s, i) => s + i, 0) }), {}),
    });
}

export type IDistrictStatistic = ReturnType<typeof toStatistic>;



