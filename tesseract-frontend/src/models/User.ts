import {observable, action, toJS, computed} from "mobx";

import {DbIdentity, EUserRole, DefaultId, EPermission} from "./types";
import { School } from "./School";
import { aFetch } from "../services/api/fetch";

export interface IUser {
    userId     : DbIdentity;
    email      : string;
    firstName  : string;
    lastName   : string;
    nickname   : string;
    salutation : string;
    phoneNumber: string;
    avatar     : string;
    // districtId : DbIdentity;
}

export class User {
    @observable userId     : DbIdentity;
    @observable email      : string;
    @observable firstName  : string;
    @observable lastName   : string;
    @observable salutation : string;
    @observable nickname   : string;
    @observable phoneNumber: string;
    @observable language   : string;
    @observable avatar     : string;
    @observable role       : EUserRole;
    @observable permissions: EPermission[];
    @observable schools    : DbIdentity[];
    @observable schoolList : School[];
    @observable districtId : DbIdentity;

    constructor(data?:{}) {
        this.userId      = DefaultId;
        this.firstName   = "";
        this.lastName    = "";
        this.salutation  = "";
        this.nickname    = "";
        this.phoneNumber = "";
        this.language    = "";
        this.avatar      = "";
        this.email       = "";
        this.role        = EUserRole.Unknow;
        this.permissions = [];
        this.schools     = [];
        this.schoolList  = [];
        this.districtId  = DefaultId;

        if (data != null) Object.assign(this, data);
    }

    @computed get fullName() { return this.nickname || `${this.salutation || ""} ${this.firstName || ""} ${this.lastName || ""}`.trim(); }

    @action set_email       = (v: string) => { this.email       = v }
    @action set_firstName   = (v: string) => { this.firstName   = v }
    @action set_lastName    = (v: string) => { this.lastName    = v }
    @action set_salutation  = (v: string) => { this.salutation  = v }
    @action set_nickname    = (v: string) => { this.nickname    = v }
    @action set_phoneNumber = (v: string) => { this.phoneNumber = v }
    @action set_language    = (v: string) => { this.language    = v }
    @action set_avatar      = (v: string) => { this.avatar      = v }

    toJS() {
        const v = toJS(this);
        return v;
    }

    clone() {
        return new FacultyUser(this.toJS())
    }

    async updateProfile() {
        const [err] = await aFetch<{}, {[key:string]:string[]}>("PUT", "/Account/UpdateProfile", {
            firstName  : this.firstName,
            lastName   : this.lastName,
            salutation : this.salutation,
            nickname   : this.nickname,
            phoneNumber: this.phoneNumber,
            language   : this.language,
            avatar     : this.avatar,
        });
        return err;
    }

    static async changePassword(oldPassword:string, newPassword:string) {
        const [err] = await aFetch<{}, {code:string, description:string}[]>("PUT", "/Account/ChangePassword", {
            password   : oldPassword,
            newPassword: newPassword,
        });
        return err;
    }

    static async updateActiveSchool(schoolId:DbIdentity) {
        const [err, data] = await aFetch<string, {code:string, description:string}[]>("PUT", "/Account/UpdateActiveSchool", {
            schoolId   : schoolId
        });

        return [err, data] as [typeof err, typeof data];
    }

    static async impersonate(userId:DbIdentity) {
        const [err, data] = await aFetch<string>("POST", `/admin/user/${userId}/impersonate`);

        return [err, data] as [typeof err, typeof data];
    }
}

export class FacultyUser extends User {
    @observable facultyId: number;

    constructor(data?:{}) {
        super(data);
        this.facultyId   = -1;
        if (data != null) Object.assign(this, data);
    }
}

export class ParentUser extends User {
    @observable parentId: number;

    constructor(data?:{}) {
        super(data);
        this.parentId = -1;
        if (data != null) Object.assign(this, data);
    }
}

export class StudentUser extends User {
    @observable studentId: number;
    @observable studentNumber: string;
    constructor(data?:{}) {
        super(data);
        this.studentId = -1;
        this.studentNumber = "";
        if (data != null) Object.assign(this, data);
    }
}
