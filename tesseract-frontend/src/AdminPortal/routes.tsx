import React, { ReactNode } from 'react';

import { RouterState, Route, StringMap, TransitionHook } from 'mobx-state-router';
import {mapValues, toPairs} from "lodash-es";

import { Store } from './stores/Store';
export const homeRoute = new RouterState("home");
export const notFound = new RouterState('notFound');
const notFoundComp = (<div><h1>404</h1><div><a href="/">back to home</a></div></div>);

const checkForUserSignedIn: TransitionHook = async (fromState, toState, routerStore) => {
    const store:Store = routerStore.rootStore;
    const isLogin = await store.checkLogin();
    if (!isLogin) location.reload();
};

import { HomePage              } from "./pages/home/HomePage"                          ;
import { DistrictPage          } from "./pages/Setting/DistrictPage"                   ;
import { RolesPage             } from "./pages/Role/RolesPage"                         ;
import { EditRolePage          } from "./pages/Role/EditRole"                          ;
import { SchoolListPage        } from "./pages/SchoolList/SchoolListPage"              ;
import { FacultyListPage       } from "./pages/faculties/FacultyListPage"              ;
import { EditFacultyPage       } from "./pages/editFaculty/EditFacultyPage"            ;
import { EditSchoolPage        } from "./pages/EditSchool/EditSchoolPage"              ;
import { GradingTermPage       } from "./pages/GradingTerms/GradingTermPage"           ;
import { AccountSettingsPage   } from "./pages/Account/Settings/AccountSettingsPage"   ;
import { FacultyActivitiesPage } from "./pages/FacultyActivities/FacultyActivitiesPage";
import { StudentListPage       } from "./pages/StudentList/StudentListPage"            ;
import { EditStudentPage       } from "./pages/EditStudent/EditStudentPage"            ;
import { PowerSearchDetail     } from './pages/home/PowerSearchDetail'                 ;

export const routeConfig:{[key:string]: {pattern:string, comp:ReactNode, allowAnonymous?:boolean}} = {
    notFound           : ({pattern:"/404"                        , comp: ( notFoundComp          ), allowAnonymous:true }),
    home               : ({pattern:"/"                           , comp: (<HomePage           /> ),                     }),
    districtSetting    : ({pattern:"/district-setting"           , comp: (<DistrictPage       /> ),                     }),
    roles              : ({pattern:"/role"                       , comp: (<RolesPage          /> ),                     }),
    editRole           : ({pattern:"/role/:roleId"               , comp: (<EditRolePage       /> ),                     }),
    schools            : ({pattern:"/schools"                    , comp: (<SchoolListPage     /> ),                     }),
    createSchool       : ({pattern:"/schools/new"                , comp: (<EditSchoolPage     /> ),                     }),
    editSchool         : ({pattern:"/schools/:schoolId/edit"     , comp: (<EditSchoolPage     /> ),                     }),
    facultiesInSchool  : ({pattern:"/schools/:schoolId/faculties", comp: (<FacultyListPage    /> ),                     }),
    faculties          : ({pattern:"/faculties"                  , comp: (<FacultyListPage    /> ),                     }),
    createFaculty      : ({pattern:"/faculties/new"              , comp: (<EditFacultyPage    /> ),                     }),
    editFaculty        : ({pattern:"/faculties/:facultyId/edit"  , comp: (<EditFacultyPage    /> ),                     }),
    facultyActivities  : ({pattern:"/schools/:schoolId/faculties/:facultyId/activities", comp: (<FacultyActivitiesPage /> )}),
    students           : ({pattern:"/students"                   , comp: (<StudentListPage    /> ),                     }),
    createStudent      : ({pattern:"/students/new"               , comp: (<EditStudentPage    /> ),                     }),
    editStudent        : ({pattern:"/students/:studentId/edit"   , comp: (<EditStudentPage    /> ),                     }),
    gradingTerms       : ({pattern:"/grading-terms"              , comp: (<GradingTermPage    /> ),                     }),
    accountSettings    : ({pattern:"/account-settings"           , comp: (<AccountSettingsPage/> ),                     }),
    powerSearchDetail  : ({pattern:"/powerSearch"                , comp: (<PowerSearchDetail  /> ),                     }),
};


export const appViewMap = mapValues(routeConfig, c => c.comp);
export const routes = toPairs(routeConfig).map<Route>(([name, c]) => ({
    name, pattern:c.pattern,
    beforeEnter: !!c.allowAnonymous ? undefined :checkForUserSignedIn
}));

function safeFromState(state: RouterState) {
    return state.routeName === "__initial__" ? state : homeRoute
}

function redirect(routeName:string, params?: StringMap, queryParams?: Object) {
    return () => Promise.reject(new RouterState(routeName, params, queryParams))
}
