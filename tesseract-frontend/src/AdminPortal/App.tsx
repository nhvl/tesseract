import React, { Component } from 'react';

import { StoreContext, store } from './stores';
import { RouterView } from '../components/router/RouterView';

import 'react-quill/dist/quill.snow.css';
import { appViewMap } from './routes';


export class App extends Component {
  render() {
    return (
      <StoreContext.Provider value={store}>
        <RouterView viewMap={appViewMap} />
      </StoreContext.Provider>
    );
  }
}

export default App;
