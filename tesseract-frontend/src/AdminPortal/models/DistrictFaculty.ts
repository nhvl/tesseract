import {observable, computed} from "mobx";

import { DbIdentity, DefaultId } from "../../models/types";
import { aFetch } from "../../services/api/fetch";
import { Faculty } from "../../models/Faculty";


export class DistrictFaculty extends Faculty {
    @observable schoolId: DbIdentity = DefaultId;
    @observable activityCount: { "active": number, "passive": number } = { "active": 0, "passive": 0 };
    constructor(data?:{}) {
        super(data);

        if (data != null) Object.assign(this, data);
    }

    @computed get params() {
        return ({
            schoolId: String(this.schoolId),
            facultyId: String(this.facultyId),
        })
    }

    static async getFaculties() {
        const [err, xs] = await aFetch<{}[]>("GET" , `/admin/faculty`);
        const ys = err ? [] : xs.map(x => new DistrictFaculty(x));
        return [err, ys] as [typeof err, typeof ys];
    }
}
