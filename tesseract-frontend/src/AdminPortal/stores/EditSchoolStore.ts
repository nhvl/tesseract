import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../models/types";
import { EditableSchool, School } from '../../models/School';
import { GradeRange } from '../../models/GradeRange';
import { GradingTerm } from '../../models/GradingTerm';

import { uploadFile } from "../../services/api/fetch";

import { Store } from "./Store";

export class EditSchoolStore {
    constructor(private store:Store) {
    }

    @observable schoolId: DbIdentity = DefaultId;

    @observable.ref school   ?: EditableSchool;

    @action async init({schoolId}:{schoolId: DbIdentity}) {
        this.schoolId = schoolId;
        this.loading = false;

        if (schoolId < 1) {
            const [gErr, _gradeRange] = await GradeRange.getDistrictGradeRange(this.store.districtId);
            if(!gErr) {
                this.defaultGradeRange = _gradeRange;
            }
            this.school = new EditableSchool();
            return;
        }
        const [gErr, _gradeRange] = await GradeRange.getSchoolGradeRange(schoolId);
        if(!gErr) {
            this.defaultGradeRange = _gradeRange;
        }

        const [err, school] = await School.getSchoolAsAdmin({districtId:this.store.districtId, schoolId});
        if (this.schoolId != schoolId) return;
        if (!err) runInAction(() => {
            this.school = EditableSchool.from(school);
        });
        return err;
    }

    @observable defaultGradeRange: GradeRange[] = [];
    @observable useDefaultGradeRange : boolean = true;
    @observable isOpenGradeRangeModal: boolean = false;
    @observable set_isOpenGradeRangeModal = (v: boolean) => { this.isOpenGradeRangeModal = v; }

    @observable loading = false;
    @action async save(gradingTerm: GradingTerm) {
        if (this.school == null) return;
        this.loading = true;
        if (this.school.logoFile != null) {
            const f = this.school.logoFile.originFileObj || (this.school.logoFile as any as File);
            const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
            if (err) { this.loading = false; return err }
            this.school.set_logoFile(undefined);
            this.school.set_logoUrl(url);
        }

        if (this.school.iconFile != null) {
            const f = this.school.iconFile.originFileObj || (this.school.iconFile as any as File);
            const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
            if (err) { this.loading = false; return err }
            this.school.set_iconFile(undefined);
            this.school.set_iconUrl(url);
        }


        const [err, s] = await this.school.save(gradingTerm);

        runInAction(() => {
            if(!err) {
                this.school = EditableSchool.from(s);
                GradeRange.setSchoolGradeRange(s.schoolId, this.defaultGradeRange)
            }
            this.loading = false;
        });
        return err;
    }

    @computed get isCreateNew() { return this.schoolId == null || this.schoolId < 1 }
}
