import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../models/types";
import { Role } from "../../models/Role";
import { Permission } from "../../models/Permission";

import { Store } from "./Store";

export class EditRoleStore {
    constructor(private store:Store) {
    }

    @observable.ref permissions: string[] = [];

    private roleId: DbIdentity = DefaultId;
    @observable.ref item?: Role;

    @action async init({roleId}:{roleId:DbIdentity}) {
        this.item = undefined;

        const pPermission = this.permissions.length < 1 ? this.fetchPermission() : Promise.resolve(undefined);

        this.roleId = roleId;
        if (roleId < 1) {
            this.item = new Role({});
            return;
        }

        const [err, x] = await Role.fetch({roleId});
        if (this.roleId != roleId) return;
        if (!err) runInAction(() => {
            this.item = x;
        });

        const pErr = await pPermission;
        return err || pErr;
    }

    @action async save() {
        const {item, roleId} = this;
        if (item == null) return;
        const [err, role] = await item.save();
        if (roleId != this.roleId) return;
        if (!err) this.item = role;
        return err;
    }

    async fetchPermission() {
        const [err, ps ] = await Permission.fetchAll();
        if (!err) runInAction(() => {
            this.permissions = ps;
        });
        return err;
    }
}
