import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../models/types";
import { Role } from "../../models/Role";

import { Store } from "./Store";

export class RoleListStore {
    constructor(private store:Store) {
    }

    @observable.shallow items : Role[] = [];

    @action async init() {
        const [err, xs] = await Role.fetchAll();
        if (!err) runInAction(() => {
            this.items = xs;
        });
        return err;
    }

    @action async deleteRole(roleId:DbIdentity) {
        const [err] = await Role.deleteRole({roleId});
        if (!err) runInAction(() => {
            this.items = this.items.filter(r => r.roleId != roleId);
        });
        return err;
    }
}
