import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../models/types";
import { DistrictFaculty } from "../models/DistrictFaculty";

import { Store } from "./Store";

export class FacultyStore {
    constructor(private store:Store) {
    }

    @observable.shallow items : DistrictFaculty[] = [];

    @observable chosenSchoolId : DbIdentity = DefaultId;
    @action set_school(schoolId:DbIdentity) { this.chosenSchoolId = schoolId; }

    @computed get chosenItems() { return this.chosenSchoolId == DefaultId ? this.items : this.items.filter(c => c.schoolId == this.chosenSchoolId)}

    @action async init() {
        const [err, xs] = await DistrictFaculty.getFaculties();
        if (!err) runInAction(() => {
            this.items = xs;
        });
        return err;
    }
}
