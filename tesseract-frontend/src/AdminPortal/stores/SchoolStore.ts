import { observable, action, runInAction, computed, ObservableMap } from "mobx";

import { DbIdentity } from "../../models/types";
import { School } from '../../models/School';
import { EditableSchool } from '../../models/School';
import { ISchoolReport, getSchoolReport } from "../../models/SchoolReport";

import { Store } from "./Store";

export class SchoolStore {
    constructor(private store:Store) {
    }

    @observable.shallow items : School[] = [];
    @computed get mSchool() { return observable.map(this.items.map(c => [c.schoolId, c])) }

    @observable.ref school   ?: EditableSchool;
    @observable gradingTerm  ?: DbIdentity;

    @observable.ref reportCache = observable.map<DbIdentity, ObservableMap<DbIdentity, ISchoolReport>>();
    @observable.ref report ?: ISchoolReport;

    @action async init(districtId: DbIdentity) {
        this.school = undefined;
        this.gradingTerm = undefined;

        const err = await this.refreshSchool(districtId);
        if (err) return err;
        this.items.map(school => this.getSchoolReport({schoolId:school.schoolId,  gradingTermId:school.currentGradingTerm}));

        return err;
    }

    @action async refreshSchool(districtId: DbIdentity) {
        const [err, xs] = await School.getSchoolsOfDistrictAsAdmin(districtId);
        if (!err) runInAction(() => {
            this.items = xs;
        });
        return err;
    }

    @action set_school(item:School) {
        this.school = EditableSchool.from(item);
        this.gradingTerm = item.currentGradingTerm;
        return this.getMainSchoolReport({ schoolId: item.schoolId, gradingTermId:item.currentGradingTerm });
    }

    @action changeGradingTerm = (gradingTermId: DbIdentity) => {
        this.gradingTerm = gradingTermId;
        if (this.school == null) return;
        const {schoolId} = this.school;

        return this.getMainSchoolReport({ schoolId: schoolId, gradingTermId });
    }

    @action async getMainSchoolReport({schoolId, gradingTermId}:{schoolId:DbIdentity, gradingTermId:DbIdentity}) {
        const [err, report] = await this.getSchoolReport({ schoolId: schoolId, gradingTermId });
        if (gradingTermId != this.gradingTerm || this.school == null || this.school.schoolId != schoolId) return [undefined, report];
        if (!err) runInAction(() => this.report = report);
        return err;
    }

    async getSchoolReport({schoolId, gradingTermId}:{schoolId:DbIdentity, gradingTermId:DbIdentity}) {
        const cachedReport = this.getCachedReport({schoolId, gradingTermId});
        if (cachedReport !== undefined) return [undefined, cachedReport] as const;

        const [err, report] = await getSchoolReport({
            schoolId     : schoolId,
            gradingTermId: gradingTermId,
        });
        if (!err) {
            this.cacheReport({schoolId, gradingTermId}, report || null);
        }
        return [err, report] as const;
    }

    @action cacheReport({schoolId, gradingTermId}:{schoolId:DbIdentity, gradingTermId:DbIdentity}, report: ISchoolReport) {
        const schoolCache = this.reportCache.get(schoolId);
        if (schoolCache == null) {
            this.reportCache.set(schoolId, observable.map([[gradingTermId, report]]));
            return;
        };
        schoolCache.set(gradingTermId, report);
    }

    getCachedReport({schoolId, gradingTermId}:{schoolId:DbIdentity, gradingTermId:DbIdentity}) {
        const schoolCache = this.reportCache.get(schoolId);
        if (schoolCache == null) return undefined;
        const report = schoolCache.get(gradingTermId);
        return report;
    }
}
