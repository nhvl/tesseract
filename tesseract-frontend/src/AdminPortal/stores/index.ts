import { createContext, useContext, Context } from "react";
import { Store } from "./Store";

import { BaseStore } from "../../stores/BaseStore";
import {setBaseStoreContext} from "../../stores/useBaseStore";

import { AuthorizedStore } from "../../stores/AuthorizedStore";
import {setAuthorizedStoreContext} from "../../stores/useAuthorizedStore";

export const store = new Store();

export const StoreContext = createContext(store)
setBaseStoreContext(StoreContext as any as Context<BaseStore>);
setAuthorizedStoreContext(StoreContext as any as Context<AuthorizedStore>);

export function useStore() {
    return useContext(StoreContext);
}

// @ts-ignore
window.store = store;
