import { observable, action, computed, reaction } from "mobx";

import { history } from "../../services/history";
import { routes, notFound } from "../routes";
import { RouterStore, HistoryAdapter } from "mobx-state-router";
import { groupBy, map } from "lodash-es";

import { DbIdentity } from "../../models/types";
import { District } from "../../models/District";
import { GradingTerm } from "../../models/GradingTerm";
import { School } from "../../models/School";
import { ScoreBadge } from "../../models/ScoreBadge";

import { AuthorizedStore } from "../../stores/AuthorizedStore";
import { GTMStore } from "../../stores/GTMStore";
import { SchoolStore } from "./SchoolStore";
import { FacultyStore } from "./FacultyStore";
import { EditFacultyStore } from "../pages/editFaculty/EditFacultyStore";
import { EditSchoolStore } from "./EditSchoolStore";
import { RoleListStore } from "./RoleListStore";
import { EditRoleStore } from "./EditRoleStore";
import { FacultyActivitiesStore } from "../pages/FacultyActivities/FacultyActivitiesStore";
import { StudentListStore } from "../pages/StudentList/StudentListStore";
import { EditStudentStore } from "../pages/EditStudent/EditStudentStore";
import { AccountSettingStore } from "../../components/Account/Settings/AccountSettingStore";

export class Store extends AuthorizedStore {
    @observable.ref currentDistrict?: District;

    @observable.shallow tSchool: School[] = [];
    @computed get mSchool() { return observable.map(this.tSchool.map<[DbIdentity, School]>(c => [c.schoolId, c])) }

    @observable.shallow tGradingTerm: GradingTerm[] = [];
    @computed get mGradingTermsOfSchool() { return observable.map(
        map(groupBy(this.tGradingTerm, c => c.schoolId), (ts) => [ts[0].schoolId, ts] as [DbIdentity, GradingTerm[]])
    )}

    @computed get districtId(): DbIdentity {
        return (
            this.currentDistrict != null ? this.currentDistrict.districtId : (
            this.currentUser != null ? (this.currentUser as any).districtId : (
            -1
        )));
    }

    constructor() {
        super();

        this.routerStore = new RouterStore(this, routes, notFound);
        const historyAdapter = new HistoryAdapter(this.routerStore, history);
        historyAdapter.observeRouterStateChanges();
        this.sLeftNav.menuOpenKeys = ["home", "config"];
        this.sLeftNav.expandedMenuOpenKeys = ["home", "config"];

        reaction(() => this.routerStore.routerState, routerState => {

            switch(routerState.routeName) {
                case "createFaculty":
                case "editFaculty":
                case "facultiesInSchool": this.sLeftNav.set_selectedKey("faculties"); break;
                case "createStudent":
                case "editStudent": this.sLeftNav.set_selectedKey("students"); break;
                case "createSchool":
                case "editSchool": this.sLeftNav.set_selectedKey("schools"); break;
                default: this.sLeftNav.set_selectedKey(routerState.routeName);
            }

            this.sGTagManager.run();
        });
    }

    sSchool            = new SchoolStore(this);
    sGTagManager       = new GTMStore(this);
    sFaculty           = new FacultyStore(this);
    sEditFaculty       = new EditFacultyStore(this);
    sFacultyActivities = new FacultyActivitiesStore(this);
    sStudentList       = new StudentListStore(this);
    sEditStudent       = new EditStudentStore(this);
    sAccountSetting    = new AccountSettingStore(this);
    sEditSchool        = new EditSchoolStore(this);
    sRoleList          = new RoleListStore(this);
    sEditRole          = new EditRoleStore(this);

    @action async setToken(token:string) {
        const [err, data] = await super.setToken(token);
        if (err) return [err, data] as const;

        await this.refresh();

        return [err, data] as const;
    }

    @action async refresh() {
        if (this.currentUser == null) { console.error("currentUser is null"); return; }
        this.refreshScoreBadge();
        this.sGTagManager.initWithDistrict(this.districtId);
        const [err, district] = await District.getDistrict();
        if (err) {
            console.error(err);
            this.logout();
        } else {
            this.currentDistrict = district;
        }
    }

    @observable scoreBadge: ScoreBadge = new ScoreBadge();
    @action set_ScoreBadge(scoreColor : ScoreBadge){
        this.scoreBadge = scoreColor;
    }
    async refreshScoreBadge(){
        if (this.currentUser == null) return;
        const [err,data] = await ScoreBadge.getDistrictScoreBadge(this.districtId);
        if (err) throw err;
        this.set_ScoreBadge(data);
    }
}
