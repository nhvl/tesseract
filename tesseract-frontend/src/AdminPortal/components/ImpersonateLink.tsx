import React, { FC,  useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DbIdentity } from "../../models/types";
import { User } from "../../models/User";

import {showError} from "../../services/api/ShowError";
import { storeJwtToken } from "../../LoginPortal/stores/loginUtil";

export const ImpersonateLink: FC<{userId:DbIdentity}> = observer(({userId, children}) => {
    const { t } = useTranslation();

    const onClick = useCallback(async () => {
        const [err, token] = await User.impersonate(userId);
        if (err) { showError(err, t); return; }
        storeJwtToken(token, "auto");
        location.assign("/");
    }, []);

    return (<a onClick={onClick}>{children}</a>)
});
