import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { DbIdentity } from '../../models/types';
import { School } from '../../models/School';

import { Select } from 'antd';
import { SelectProps } from 'antd/lib/select';

interface ISchoolSelectProps extends SelectProps<DbIdentity> {
    options: School[];
}

export const SchoolSelect: FC<ISchoolSelectProps> = observer(({options, children, ...props}) => {
    return (
        <Select {...props}>
            {children}
            {options.map((school) => (
                <Select.Option key={school.schoolId} value={school.schoolId}>{school.schoolName}</Select.Option>
            ))}
        </Select>
    );
});
