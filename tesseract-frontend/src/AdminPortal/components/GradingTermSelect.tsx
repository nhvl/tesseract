import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { DbIdentity } from '../../models/types';
import { GradingTerm } from '../../models/GradingTerm';

import { Select } from 'antd';
import { SelectProps } from 'antd/lib/select';

interface IGradingTermSelectProps extends SelectProps<DbIdentity> {
    options: GradingTerm[];
}

export const GradingTermSelect: FC<IGradingTermSelectProps> = observer(({options, ...props}) => {
    return (
        <Select {...props} className="select-min-w mb-lg">
            {options.map((term) => (
                <Select.Option key={term.gradingTermId} value={term.gradingTermId}>{term.name}</Select.Option>
            ))}
        </Select>
    );
});
