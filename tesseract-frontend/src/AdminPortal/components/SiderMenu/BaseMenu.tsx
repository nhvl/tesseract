import React, { FC, CSSProperties, useMemo, useCallback, useState, useEffect } from "react";
import { observer } from "mobx-react";

import { Menu, Icon, message } from "antd";
const { SubMenu } = Menu;

import { useStore } from "../../stores";

import { Link } from "../../../components/router/Links";
import { useTranslation } from "react-i18next";

import { EUserRole } from "../../../models/types";
import { subMenu } from "../../../components/inputs/antd/subMenu";


const style: CSSProperties = { padding: "0 0 16px 0", width: "100%" };

export const BaseMenu: FC<{}> = observer(() => {
    const [scrollMenuStyle, set_ScrollMenuStyle] = useState(undefined as CSSProperties|undefined);
    const store = useStore();
    const {sLeftNav} = store;
    const {t} = useTranslation();
    const selectedKeys = useMemo(() => [sLeftNav.selectedKey], [sLeftNav.selectedKey]);
    const openAeries = useCallback(() => message.info(t("menu.openaeries.open")), [t]);

    useEffect(() => {
        updateScrollMenu();
    }, []);
    const updateScrollMenu = useCallback(()=>{
        const item: CSSProperties = {height: "100%", maxHeight:(window.innerHeight - sLeftNav.nonMenuHeight), overflowY: "auto" }
        set_ScrollMenuStyle(item);
    },[window.innerHeight]);

    const isSysAdmin = (store.currentUser && store.currentUser.role == EUserRole.Admin);

    return (<div style={scrollMenuStyle}>
        <Menu theme="dark" mode="inline"
            selectedKeys={selectedKeys}
            openKeys = {sLeftNav.menuOpenKeys}
            onOpenChange = {sLeftNav.set_menuOpenKeys}
            style={style}
            inlineIndent={18}
            className="main-nav"
            >

            {...subMenu({
                collapsed:sLeftNav.collapsed,
                subMenuClassName: "level-1",
                key:"home",
                subMenuTitle:(
                    <Link routeName="home"><Icon type="home" /><span>{t("menu.home")}</span></Link>
                ),
                menuItems:[
                    (<Menu.Item key="schools"><Link routeName="schools"><span>{t("menu.schools")}</span></Link></Menu.Item>),
                    (<Menu.Item key="faculties"><Link routeName="faculties"><span>{t("menu.faculty")}</span></Link></Menu.Item>),
                    (<Menu.Item key="students"><Link routeName="students"><span>{t("menu.students")}</span></Link></Menu.Item>),
                ],
            })}

            <SubMenu className="level-1-admin" key="config" title={(<span><Icon type="setting" /><span>{t("menu.configuration")}</span></span>)}>
                <Menu.Item key="gradingTerms"><Link routeName="gradingTerms"><span>{t("menu.gradingterms")}</span></Link></Menu.Item>
                {isSysAdmin && (
                    <Menu.Item key="districtSetting"><Link routeName="districtSetting"><span>{t("menu.district.setting")}</span></Link></Menu.Item>
                )}
                {isSysAdmin && (
                    <Menu.Item key="roles"><Link routeName="roles"><span>{t("menu.district.roles")}</span></Link></Menu.Item>
                )}
            </SubMenu>

            <Menu.Item key="aeries">
                <a onClick={openAeries}>
                    <img className="anticon" src="https://ps.orangeusd.org/favicon.ico" alt={t("menu.openaeries")} />
                <span>{t("menu.openaeries")}</span>
                </a>
            </Menu.Item>
        </Menu>
    </div>)
})
