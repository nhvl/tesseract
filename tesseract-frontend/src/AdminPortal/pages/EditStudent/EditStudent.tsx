import React, { FC,  useEffect, useCallback, FormEvent } from "react";
import { observer } from "mobx-react";

import { DefaultId } from "../../../models/types";

import { useStore } from "../../stores";

import { Card, Skeleton, Button, Form, Select, Breadcrumb, Row, Col, Divider, } from "antd";

import { Input } from "../../../components/inputs/antd/Input";

import { Link } from "../../../components/router/Links";
import { SchoolSelect } from "../../components/SchoolSelect";
import { InputNumber } from "../../../components/inputs/antd/InputNumber";
import { useTranslation } from "react-i18next";
import { Modal } from "../../../components/Modal/ModalAntd";

export const EditStudent: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const {sEditStudent, routerStore} = useStore();

    const {studentId} = routerStore.routerState.params;
    let sId = !studentId ? DefaultId : Number(studentId);
    if (Number.isNaN(sId)) sId = DefaultId;
    useEffect(() => {
        sEditStudent.init({studentId:sId}).then(err => {
            if (err) Modal.error({content: err.message});
        })
    }, [sId]);

    const onSubmit = useCallback(async (event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (sEditStudent.student == null) return;
        if (sEditStudent.student.studentId < 1 && sEditStudent.schoolId < 1) {
            Modal.error({ content: t('app.students.edit.error.pleasSelectASchool') });
            return;
        }
        const err = await sEditStudent.save();
        if (err) Modal.error({content: err.message});
        else routerStore.goTo("students");
    }, []);

    const {student, loading, schoolId, set_schoolId, schools} = sEditStudent;

    if (student == null) return (<Card><Skeleton /></Card>);

    return (<>
        <div className="breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link routeName="students">{t('menu.students')}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    {sEditStudent.isCreateNew ? t('app.students.edit.createTitle') : t('app.students.edit.editTitle')}
                </Breadcrumb.Item>
            </Breadcrumb>
        </div>
        <Card>
            <Form onSubmit={onSubmit}>
                <fieldset disabled={loading}>
                    {student.studentId < 1 && (
                        <Row gutter={16}>
                            <Col xs={24} xxl={16}>
                                <Form.Item label={t('app.students.edit.schoolName')}>
                                    <SchoolSelect value={schoolId} onChange={set_schoolId} options={schools}>
                                        <Select.Option value={DefaultId}>{t('app.students.edit.selectASchool')}</Select.Option>
                                    </SchoolSelect>
                                </Form.Item>
                            </Col>
                        </Row>
                    )}
                    <Row gutter={16}>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.studentNumber')}>
                                <Input value={student.studentNumber} onChange={student.set_studentNumber}
                                    placeholder={t('app.students.edit.studentNumber')} />
                            </Form.Item>
                        </Col>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.grade')}>
                                <InputNumber value={student.grade} onChange={student.set_grade}
                                    placeholder={t('app.students.edit.grade')}
                                    min={0} max={12} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.firstName')}>
                                <Input value={student.firstName} onChange={student.set_firstName}
                                    placeholder={t('app.students.edit.firstName')} />
                            </Form.Item>
                        </Col>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.lastName')}>
                                <Input value={student.lastName} onChange={student.set_lastName}
                                    required
                                    placeholder={t('app.students.edit.lastName')} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} xxl={16}>
                            <Form.Item label={t('app.students.edit.email')}>
                                <Input value={student.email} onChange={student.set_email}
                                    type="email" required
                                    placeholder={t('app.students.edit.email')} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.phoneNumber')}>
                                <Input value={student.phoneNumber} onChange={student.set_phoneNumber}
                                    type="tel"
                                    placeholder={t('app.students.edit.phoneNumber')}/>
                            </Form.Item>
                        </Col>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.externalID')}>
                                <Input value={student.externalId} onChange={student.set_externalId}
                                    placeholder={t('app.students.edit.externalID')} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Divider />
                    <Button className="mr-sm"><Link routeName="students">{t('form.cancel')}</Link></Button>
                    <Button type="primary" htmlType="submit" loading={loading}>{t('form.save')}</Button>
                </fieldset>
            </Form>
        </Card>
    </>);
});
