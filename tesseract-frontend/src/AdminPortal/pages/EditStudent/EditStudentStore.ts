import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../../models/types";
import { School } from "../../../models/School";
import { Student } from "../../../models/Student";

import { Store } from "../../stores/Store";
import { IErrorData } from "../../../services/api/AppError";

export class EditStudentStore {
    constructor(private store:Store) {
    }

    @observable studentId: DbIdentity = DefaultId;

    @observable.ref student   ?: Student;
    @observable schoolId : DbIdentity = DefaultId;

    @observable.shallow schools   : School[] = [];

    @action async init({studentId}:{studentId: DbIdentity}) {
        this.studentId = studentId;
        this.loading = false;

        if (studentId < 1) {
            this.student = new Student();
        } else {
            const [fErr, student] = await Student.fetchAsAdmin(studentId);
            if (this.studentId != studentId) return;
            if (fErr) return fErr;
            runInAction(() => {
                this.student = student;
            });
        }

        const [sErr, schools] = await School.getSchoolsOfDistrictAsAdmin(this.store.currentDistrict!.districtId);
        if (this.studentId != studentId) return;
        if (sErr) return sErr;
        runInAction(() => {
            this.schools = schools
        });

        return;
    }

    @observable loading = false;
    @action async save() {
        if (this.student == null) return;
        this.loading = true;

        let err: IErrorData<{}>|undefined;
        if (this.student.studentId < 1) {
            [err, ] = await this.student.create(this.schoolId);
        } else {
            [err, ] = await this.student.save();
        }
        runInAction(() => {
            this.loading = false;
        });
        return err;
    }

    @action set_schoolId = (v:DbIdentity) => { this.schoolId = v }

    @computed get isCreateNew() { return this.studentId == null || this.studentId < 1 }
}
