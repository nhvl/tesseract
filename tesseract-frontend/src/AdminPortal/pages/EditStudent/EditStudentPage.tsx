import React, { FC } from "react";
import { observer } from "mobx-react";

import { BasicLayout } from "../../layouts/BasicLayout";
import { EditStudent } from "./EditStudent";

export const EditStudentPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <EditStudent />
    </BasicLayout>);
})


