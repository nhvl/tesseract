import React, { FC, useState, useMemo, FormEvent, useCallback, useEffect } from "react";
import { runInAction } from "mobx";
import {observer} from "mobx-react";
import { useTranslation } from "react-i18next";

import { EUserRole } from "../../../models/types";

import { useStore } from "../../stores";

import { Button, Form, Skeleton, Card, Typography, Icon, Input, Row, Col, Divider, } from "antd";
const { Title } = Typography;
import { UploadFile } from "antd/lib/upload/interface";

import { BasicLayout } from "../../layouts/BasicLayout";
import { DumbUpload } from "../../../components/inputs/antd/Upload";

import styles from "./DistrictPage.module.less";
import { Modal } from "../../../components/Modal/ModalAntd";

const DistrictSetting: FC<{}> = observer(() => {
  const { t } = useTranslation();
  const { routerStore } = useStore();
  const store = useStore();
  useEffect(() => {
    if(store.currentUser && store.currentUser.role != EUserRole.Admin){
      routerStore.goTo("home");
    }
  }, []);

  const { currentDistrict } = store;

  const [avatarFile, setAvatarFile] = useState<UploadFile|undefined>(undefined);
  const [submitting, setSubmitting] = useState(false);

  const src = useMemo(() => {
    return avatarFile ? avatarFile.thumbUrl : (currentDistrict ? currentDistrict.logoUrl : "");
  }, [avatarFile, currentDistrict])

  const onSubmit = useCallback(async (event:FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!avatarFile) return;
    if (currentDistrict == null) return;

    setSubmitting(true);
    const [err, district] = await currentDistrict.updateLogo(avatarFile as any as File);
    setSubmitting(false);
    if (err) Modal.error({content:err.message || t("component.modal.error.systemError")});
    else runInAction(() => {
      store.currentDistrict = district;
      setAvatarFile(undefined);
    });
  }, [avatarFile]);

  const handleClose = useCallback(async () => {
    if (history.length > 1) {
        history.back();
    } else {
        routerStore.goTo("");
    }
}, []);

  if (currentDistrict == null) return (<Skeleton />);

  return (
    <Card>
        <Row gutter={16}>
            <Col xs={24} xxl={16}>
              <Form.Item label={t("app.settings.district.logo")}>
                <div className="flex">
                  <div className={styles.avatar}><img src={src} alt={t("app.settings.district.logo")} /></div>
                  <div className="flex flex-col justify-center">
                    <DumbUpload accept="image/*" onChange={setAvatarFile} createThumb>
                        <span className="paddingRight-xs">{t("app.settings.basic.change-logo")}</span>
                        <Icon type="upload" className="icon-lg" />
                    </DumbUpload>
                  </div>
                </div>
              </Form.Item>
            </Col>
        </Row>
        <Form onSubmit={onSubmit}>
          <Row gutter={16}>
            <Col xs={24} md={12} xxl={8}>
              <Form.Item  label={t("app.settings.district.domain")}>
                <Input value={currentDistrict.domain} disabled />
              </Form.Item>
            </Col>
            <Col xs={24} md={12} xxl={8}>
              <Form.Item label={t("app.settings.district.name")}>
                  <Input value={currentDistrict.districtName} disabled />
              </Form.Item>
            </Col>
          </Row>
          <Divider />
          <Button onClick={handleClose} className="mr-sm">{t("form.cancel")}</Button>
          <Button loading={submitting} type="primary" htmlType="submit" disabled={avatarFile == null}>{t("form.save")}</Button>
        </Form>
    </Card>

  );
});

export const DistrictPage: FC<{}> = observer(() => {
  const {t} = useTranslation();
  return (
    <BasicLayout>
      <div className="header">
        <Title level={3}>{t("app.district.settings")}</Title>
      </div>
      <DistrictSetting/>
    </BasicLayout>
  );
});
