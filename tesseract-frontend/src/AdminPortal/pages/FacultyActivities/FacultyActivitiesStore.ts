import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../../models/types";
import { School } from '../../../models/School';
import { GradingTerm } from "../../../models/GradingTerm";
import { Class } from "../../../models/Class";
import { Faculty } from "../../../models/Faculty";
import { Activity } from "../../../models/Activity";

import { Store } from "../../stores/Store";

export class FacultyActivitiesStore {
    constructor(private store:Store) {
    }

    @observable schoolId: DbIdentity = DefaultId;
    @observable facultyId: DbIdentity = DefaultId;

    @observable.ref     school              ?: School;
    @observable.shallow gradingTerms         : GradingTerm[] = [];
    @observable         selectedGradingTerms : DbIdentity = DefaultId;
    @observable.ref     faculty             ?: Faculty;
    @observable.shallow activities           : Activity[] = [];
    @observable.shallow classes              : Class[] = [];
    @computed get mClass() { return observable.map(this.classes.map(c => [c.classId, c])) }

    @action async init({schoolId, facultyId}:{schoolId: DbIdentity, facultyId: DbIdentity}) {
        this.schoolId = schoolId;
        this.facultyId = facultyId;

        const pS = School.getSchoolAsAdmin({schoolId, districtId:this.store.districtId});
        const pF = Faculty.getFacultyOfSchoolAsAdmin({schoolId, facultyId});
        const pA = Activity.getActivityOfFacultyInSchoolAsAdmin({schoolId, facultyId});

        const [sErr, school ] = await pS;
        const [fErr, faculty] = await pF;
        const [aErr, vm     ] = await pA;

        runInAction(() => {
            if (!sErr) {
                this.school = school;
                this.gradingTerms = school.tGradingTerm;
                this.selectedGradingTerms = school.currentGradingTerm;
            }
            if (!fErr) this.faculty = faculty;
            if (!aErr) {
                this.activities = vm.activities;
                this.classes = vm.classes;
            }
        });

        return (
            sErr ||
            fErr ||
            aErr ||
            undefined
        );
    }

    @computed get openAssignments() {
        return this.activities.map(activity => ({
            activity,
            aClass: this.mClass.get(activity.classId),
        })).filter(x => x.activity.dateDue == null || Date.now() < x.activity.dateDue)
    }

    @computed get completedAssignments() {
        return this.activities.map(activity => ({
            activity,
            aClass: this.mClass.get(activity.classId),
        })).filter(x =>
            x.aClass != null && x.aClass.gradingTerm == this.selectedGradingTerms &&
            x.activity.dateDue != null && x.activity.dateDue < Date.now())
    }

    @action setSelectedGradingTerms = (v: DbIdentity) => { this.selectedGradingTerms = v; }

}

export interface IFacultyAssignment {
    activity : Activity,
    aClass  ?: Class,
}
