import React, { FC,  useEffect, useRef, useMemo } from 'react';
import { observer } from 'mobx-react';

import { DbIdentity } from '../../../models/types';
import { Activity } from '../../../models/Activity';

import { IFacultyAssignment } from './FacultyActivitiesStore';
import { useStore } from '../../stores';

import { Spin, Breadcrumb, Form, Skeleton, Card } from 'antd';
import { ColumnProps } from 'antd/lib/table';

import { ResponsiveTable } from '../../../components/ResponsiveTable/ResponsiveTable';
import { StyledDateTime } from '../../../components/DateTime';
import { Link } from '../../../components/router/Links';
import { GradingTermSelect } from '../../components/GradingTermSelect';
import { Class } from '../../../models/Class';
import { useTranslation } from 'react-i18next';

export const FacultyActivities: FC<{schoolId:DbIdentity, facultyId:DbIdentity}> = observer(({schoolId, facultyId}) => {
    const {t} = useTranslation();
    const store = useStore();
    const {sFacultyActivities, isMobile} = store;
    const params = useMemo(() => ({
        schoolId: String(schoolId),
        facultyId: String(facultyId),
    }), [schoolId, facultyId]);
    useEffect(() => {
        sFacultyActivities.init({schoolId, facultyId})
    }, [schoolId, facultyId]);

    const {school, faculty, openAssignments, completedAssignments} = sFacultyActivities;

    const columns = useMemo<Array<ColumnProps<IFacultyAssignment>>>(() => [
        { title: t('app.activities.listpage.table.period'), key: "Period", render:(_, item) => (item.aClass ? item.aClass.period    : ""), sorter:(a, b) => Class.sorter.period(a.aClass, b.aClass)},
        { title: t('app.activities.listpage.table.class') , key: "Class" , render:(_, item) => (item.aClass ? item.aClass.className : ""), sorter:(a, b) => Class.sorter.className(a.aClass, b.aClass)},
        {
            title: t('app.activities.listpage.table.assignment'),
            key: "title"  ,
            render:(_, item) => (<a>{item.activity.title}</a>),
            sorter:(a, b) => Activity.sorter.title(a.activity, b.activity),
            className: "whitespace-normal min-w"
        },
        { title: t('app.activities.listpage.table.duedate'), key: "dateDue", render:(_, item) => (<StyledDateTime value={item.activity.dateDue}/>), sorter:(a, b) => Activity.sorter.dateDue(a.activity, b.activity) },
        { title: "", key: "actions", render:(_, item) => (item.activity.isGraded ? (<a>{t('app.activities.listpage.table.grades')}</a>) : null) },
    ], []);

    if (school == null) return (<Card><Skeleton/></Card>);

    return (<>
        <div className="breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item><Link routeName="facultiesInSchool" params={params}>
                    {school == null ? (<Spin />) : school.schoolName}
                </Link></Breadcrumb.Item>
                <Breadcrumb.Item>{faculty == null ? (<Spin />) : `${faculty.fullName}`}</Breadcrumb.Item>
                <Breadcrumb.Item>{t('app.activities.listpage.title')}</Breadcrumb.Item>
            </Breadcrumb>
        </div>
        <Card title={t('app.activities.listpage.openAssignments')} className="mb-lg">
            <ResponsiveTable
                dataSource={openAssignments}
                columns={columns}
                pagination={false}
                rowKey={rowKey}
            />
        </Card>
        <Card title={t('app.activities.listpage.completedAssignments')} className="mb-lg">
            <div>
                <label className="text-black font-medium mr-xs">{t('app.activities.listpage.gradingPeriod')}:</label>
                <GradingTermSelect value={sFacultyActivities.selectedGradingTerms} onChange={sFacultyActivities.setSelectedGradingTerms} options={sFacultyActivities.gradingTerms} />
            </div>
            <ResponsiveTable
                dataSource={completedAssignments}
                columns={columns}
                pagination={false}
                rowKey={rowKey}
        />
        </Card>
    </>);
});

function rowKey(record: Activity) { return String(record.activityId) }
