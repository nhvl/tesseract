import React, { FC } from "react";
import { observer } from "mobx-react";

import { useStore } from "../../stores";

import { BasicLayout } from "../../layouts/BasicLayout";
import { FacultyActivities } from "./FacultyActivities";

export const FacultyActivitiesPage: FC<{}> = observer(({}) => {
    const { routerStore } = useStore();
    const {schoolId, facultyId} = routerStore.routerState.params;
    const sId = !schoolId ? NaN : Number(schoolId);
    const fId = !facultyId ? NaN : Number(facultyId);
    if (Number.isNaN(sId) && Number.isNaN(fId)) {
        routerStore.goTo("home");
        return null;
    }

    return (<BasicLayout>
        <FacultyActivities schoolId={sId} facultyId={fId} />
    </BasicLayout>);
});
