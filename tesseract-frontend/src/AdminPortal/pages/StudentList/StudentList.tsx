import React, { FC,  useEffect, useMemo } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DefaultId } from "../../../models/types";
import { School } from "../../../models/School";
import { Student } from "../../../models/Student";

import { useStore } from "../../stores";

import { Select, Typography, Card, Button, Tooltip, Icon, } from "antd";
const { Title } = Typography;
import { ColumnProps } from "antd/lib/table";

import { ResponsiveTable } from "../../../components/ResponsiveTable/ResponsiveTable";
import { SchoolSelect } from "../../components/SchoolSelect"
import { Link } from "../../../components/router/Links";
import {ImpersonateLink} from "../../components/ImpersonateLink";

export const StudentList: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const store = useStore();
    const {sStudentList} = store;
    useEffect(() => {
        sStudentList.init();
    }, []);

    const columns = useMemo<Array<ColumnProps<{school?:School, student?:Student}>>>(() => [
        { title: t('app.students.list.firstName') , key: "firstName", render:(_, item) => (item.student ? item.student.firstName : ""), sorter:(a,b) => Student.sorter.firstName (a.student, b.student) },
        { title: t('app.students.list.lastName') , key: "lastName" , render:(_, item) => (item.student ? item.student.lastName  : ""), sorter:(a,b) => Student.sorter.lastName  (a.student, b.student), defaultSortOrder:"ascend" },
        { title: t('app.students.list.school') , key: "school"   , render:(_, item) => (item.school  ? item.school .schoolName: ""), sorter:(a,b) => School .sorter.schoolName(a.school , b.school ) },
        { title: t('app.students.list.grade') , key: "grade"    , render:(_, item) => (item.student ? item.student.grade     : ""), sorter:(a,b) => Student.sorter.grade     (a.student, b.student) },
        { key: "actions", render:(_, item) => (item.student ? (<Link routeName="editStudent" params={item.student.params}><Tooltip title={t('app.students.list.edit')}><Icon type="edit" className="icon-sm" /></Tooltip></Link>) : null) },
        { key: "impersonate", render:(_, item) => (item.student ? (<ImpersonateLink userId={item.student.studentId}>{t("app.district.faculty.table.impersonate")}</ImpersonateLink>) : null) },
    ], []);

    const displayColumns = useMemo(() => columns.filter(({key}) => (key != "school" || sStudentList.schoolId < 1)), [columns, sStudentList.schoolId]);

    return (<>
        <div className="header">
            <Title level={3}>{t('app.students.list.students')}</Title>
        </div>
        <div className="mb-sm">
            <label className="text-black font-medium mr-xs">{t('app.students.list.selectLabel')}:</label>
            <SchoolSelect value={sStudentList.schoolId} onChange={sStudentList.setSchoolId} options={sStudentList.schools} className="select-min-w">
                <Select.Option value={DefaultId}>{t('app.students.list.selectSchool')}</Select.Option>
            </SchoolSelect>
        </div>
        <Card>
            <div className="mb-sm">
                <Link routeName="createStudent"><Button type="primary">{t('app.students.list.add')}</Button></Link>
            </div>
            <ResponsiveTable
                dataSource={sStudentList.displayStudents}
                columns={displayColumns}
                pagination={false}
                rowKey={rowKey}
            />
        </Card>
    </>);
});

function rowKey(record: {school?:School, student?:Student}) { return String(record.student!.studentId) }
