import { observable, action, runInAction, computed } from "mobx";
import { map, groupBy } from "lodash-es";

import { DbIdentity, DefaultId } from "../../../models/types";
import { School } from '../../../models/School';

import { Store } from "../../stores/Store";
import { Student } from "../../../models/Student";
import { Class } from "../../../models/Class";
import { Activity } from "../../../models/Activity";
import { ISchoolStudent } from "../../../models/SchoolStudent";

export class StudentListStore {
    constructor(private store:Store) {
    }

    @observable         schoolId: DbIdentity = DefaultId;
    @action setSchoolId = (v:DbIdentity) => { this.schoolId = v }

    @observable.shallow students: Student[] = [];
    @observable.shallow schools: School[] = [];
    @observable.shallow schoolStudents: ISchoolStudent[] = [];
    @computed get mSchool() { return observable.map(this.schools.map(x => [x.schoolId, x])) }
    @computed get mStudent() { return observable.map(this.students.map(x => [x.studentId, x])) }
    @computed get mSchool2Students() {
        return observable.map(map(
            groupBy(this.schoolStudents, i => i.schoolId),
            xs => [xs[0].schoolId, xs.map(x => this.mStudent.get(x.studentId)!).filter(Boolean)]
        ));
    }

    @action async init() {
        this.schoolId = DefaultId;
        const [err, vm] = await Student.fetchStudentsInDistrictAsAdmin(this.store.currentDistrict!.districtId);

        runInAction(() => {
            if (!err) {
                this.students = vm.students;
                this.schools = vm.schools;
                this.schoolStudents = vm.schoolStudents;
            }
        });

        return (
            err
        );
    }

    @computed get displayStudents() {
        return (
            (this.schoolId < 1
                ? this.schoolStudents
                : this.schoolStudents.filter(ss => ss.schoolId == this.schoolId))
            .map(ss => ({school:this.mSchool.get(ss.schoolId), student:this.mStudent.get(ss.studentId)}))
        );
    }
}

export interface IFacultyAssignment {
    activity : Activity,
    aClass  ?: Class,
}
