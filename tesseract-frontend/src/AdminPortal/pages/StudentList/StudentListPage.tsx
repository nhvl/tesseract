import React, { FC } from "react";
import { observer } from "mobx-react";

import { BasicLayout } from "../../layouts/BasicLayout";
import { StudentList } from "./StudentList";

export const StudentListPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <StudentList />
    </BasicLayout>);
});
