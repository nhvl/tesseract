import React, { FC } from "react";
import { observer } from "mobx-react";

import { BasicLayout } from "../../layouts/BasicLayout";
import { SchoolGradingTermList } from "./GradingTermList";

export const GradingTermPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <SchoolGradingTermList />
    </BasicLayout>);
})
