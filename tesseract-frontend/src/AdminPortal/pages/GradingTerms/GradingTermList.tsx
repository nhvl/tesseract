import React, { FC, useCallback, useState, useEffect, useMemo, FormEvent } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DefaultId } from "../../../models/types";
import { School } from "../../../models/School";
import { GradingTerm } from "../../../models/GradingTerm";

import {GradingTermListStore} from "./GradingTermListStore";
import { useStore } from "../../stores";

import { Modal as AntdModal, Avatar, Tabs, Form, Button, message, Card, Icon, Table, Typography, Tooltip, Row, Col } from "antd";
const { Title } = Typography;
import { ColumnProps } from "antd/lib/table";


import { Input } from "../../../components/inputs/antd/Input";
import { DatePicker } from "../../../components/inputs/antd/DatePicker";
import { StyledDateTime } from "../../../components/DateTime";
import { RangePicker } from "../../../components/inputs/antd/RangePicker";
import { SchoolSelect } from "../../components/SchoolSelect";

import { showError } from "../../../services/api/ShowError";
import { Modal } from "../../../components/Modal/ModalAntd";

import styles from "./GradingTermList.module.less";

export const SchoolGradingTermList: FC<{}> = observer(({ }) => {
    const {t} = useTranslation();
    const {currentDistrict, sSchool} = useStore();
    const [schoolId, set_schoolId] = useState(DefaultId);
    const selectSchool = useCallback((sId:string) => set_schoolId(Number(sId)), [set_schoolId]);

    useEffect(() => {
        if (!currentDistrict) return;
        sSchool.refreshSchool(currentDistrict.districtId).then((err) => {
            if (err) Modal.error({content:err.message})
            else {
                const s = sSchool.items[0];
                if (s != null && schoolId < 1) set_schoolId(s.schoolId);
            }
        });
    }, [currentDistrict]);


    return (<>
        <div className="header">
            <Title level={3}>{t("admin.gradingterm.list.GradingTerm")}</Title>
        </div>
        <Card className={styles.gradingTermList}>
            <div className={styles.filterSchool}>
                <label className="block font-bold mb-xs">Filter by School:</label>
                <SchoolSelect value={schoolId} onChange={set_schoolId}
                    options={sSchool.items} className="w-full mb-lg" />
            </div>
            <Tabs activeKey={String(schoolId)} onChange={selectSchool}
                tabPosition="left" className={styles.tabs}>
                {sSchool.items.map((school) => (
                    <Tabs.TabPane key={String(school.schoolId)}
                        tab={(<>
                            {(school.iconUrl || school.logoUrl) && (
                                <Avatar src={school.logoUrl || school.iconUrl}
                                    alt={school.schoolName} />
                            )}
                            {" "}
                            {school.schoolName}
                        </>)}>
                        {school.schoolId == schoolId && (
                            <GradingTermList school={school} />
                        )}
                    </Tabs.TabPane>
                ))}
            </Tabs>
        </Card>
    </>);
});

import {contextFactory} from "../../../utils/contextFactory";
const {
    useStore: useGradingTermListStore,
    withStore:withGradingTermListStore,
} = contextFactory(function() {
    const store = useStore();
    const [sGtList] = useState(() => new GradingTermListStore(store));
    return sGtList;
});

const GradingTermList = withGradingTermListStore<{school:School}>(observer((props) => {
    const {t} = useTranslation();
    const sGtList = useGradingTermListStore();
    const {school, term} = sGtList;

    useEffect(() => { sGtList.init(props.school); }, [props.school]);

    const rowClassName = useCallback((item:GradingTerm) => (
        (term && item.gradingTermId == term.gradingTermId) ? "ant-table-row-selected" : ""
    ), [term]);

    const columns = useMemo<ColumnProps<GradingTerm>[]>(() => [
        {key: "name"     , title : t("admin.gradingterm.list.GradingTerm"), dataIndex: "name", sorter: GradingTerm.sorter.name, className: "whitespace-normal min-w" },
        {key: "startDate", title : t("admin.gradingterm.list.StartDate")  , render: (_, term) => (<StyledDateTime value = {term.startDate} />), sorter: GradingTerm.sorter.startDate },
        {key: "endDate"  , title : t("admin.gradingterm.list.EndDate")    , render: (_, term) => (<StyledDateTime value = {term.endDate} />), sorter  : GradingTerm.sorter.endDate },
        {key: "action"   ,                         render: (_, term) => (<GradingTermActions term = {term} />)},
    ], []);

    if (school == null) return null;

    return (<>
        <Button onClick={sGtList.addNewGradingTerm} type="primary" className="mb-sm">{t("admin.gradingterm.list.AddGradingTerm")}</Button>
        <div className="overflow-y-auto">
            <Table dataSource={school.tGradingTerm}
                columns={columns}
                pagination={{hideOnSinglePage:true}}
                rowKey={rowKey}
                rowClassName={rowClassName}
            />
        </div>
        {term && (
            <GradingTermEdit term={term} />
        )}
    </>);
}));

function rowKey(item:GradingTerm){ return String(item.gradingTermId) }

const GradingTermActions: FC<{term:GradingTerm}> = observer(({term}) => {
    const {t} = useTranslation();
    const gsStore = useGradingTermListStore();

    const beginEdit = useCallback(() => {
        gsStore.selectTerm(term)
    }, [gsStore, term]);

    const removeTerm = useCallback(() => {
        gsStore.removeTerm(term);
    }, [gsStore, term]);

    return (
        <a onClick={beginEdit}><Tooltip title={t('form.edit')}><Icon type="edit" className="icon-sm" /></Tooltip></a>
    );
});

const GradingTermEdit: FC<{term:GradingTerm}> = observer(({term}) => {
    const {t} = useTranslation();
    const [loading, setLoading] = useState(false);
    const sGtList = useGradingTermListStore();


    const onCancel = useCallback(() => { sGtList.selectTerm(undefined); }, [sGtList]);

    const onSubmit = useCallback(async (event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        //Check validation
        if(term.name == ""){
            message.error(t("admin.gradingterm.form.TermNameRequired"));
            return;
        }
        if(term.startDate == null){
            message.error(t('admin.gradingterm.form.StartDateRequired'));
            return;
        }
        if(term.endDate == null){
            message.error(t('admin.gradingterm.form.EndDateRequired'));
            return;
        }
        if(term.endDate <= term.startDate){
            message.error(t('admin.gradingterm.form.InvalidDateRange'));
            return;
        }

        setLoading(true);
        const err = await sGtList.save();
        if (err) showError(err, t);
        else message.success(t('form.saved'));
    }, [sGtList]);

    const disabledStartDate = useCallback((startValue)=>{
        const endValue = term.endDate;
        if (!startValue || !endValue) return false;
        return startValue.valueOf() > endValue.valueOf();
    }, [term.endDate]);

    const disabledEndDate = useCallback((endValue) => {
        const startValue = term.startDate;
        if (!endValue || !startValue) return false;
        return endValue.valueOf() <= startValue.valueOf();
    },[term.startDate]);

    return (
        <AntdModal visible={true} key = {term.gradingTermId}
            confirmLoading={loading}
            onCancel={onCancel}
            title={t("admin.gradingterm.list.GradingTerm")}
            footer={[
                <Button key="back" onClick={onCancel}>{t('form.cancel')}</Button>,
                <Button key="submit" type="primary" htmlType="submit" loading={loading} form="GradingTermEditor">{t('form.save')}</Button>
              ]}
            >
            <Form onSubmit={onSubmit} id="GradingTermEditor">
                <Form.Item label={t("admin.gradingterm.list.Name")}>
                    <Input value={term.name} onChange={term.set_name} required />
                </Form.Item>
                <div hidden><Form.Item label={t('admin.gradingterm.list.Period')}>
                    <RangePicker
                        from={term.startDate} onChangeFrom={(v) => v && term.set_startDate(v)}
                        to={term.endDate} onChangeTo={(v) => v && term.set_endDate(v)}
                        size="large" className={styles.periodField}
                        format="YYYY-MM-DD HH:mm" showTime={{format: "HH:mm"}}
                        />
                </Form.Item></div>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={t("admin.gradingterm.list.StartDate")}>
                            <DatePicker value={term.startDate} onChange={(v) => v && term.set_startDate(v)}
                                size="large" className="w-full" allowClear={false}
                                format="YYYY-MM-DD HH:mm" showTime={{format: "HH:mm"}}
                                disabledDate={disabledStartDate}
                                />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={t("admin.gradingterm.list.EndDate")}>
                            <DatePicker value={term.endDate} onChange={(v) => v && term.set_endDate(v)}
                                size="large" className="w-full" allowClear={false}
                                format="YYYY-MM-DD HH:mm" showTime={{format: "HH:mm"}}
                                disabledDate={disabledEndDate}
                                />
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </AntdModal>
    );
});
