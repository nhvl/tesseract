import { observable, action, runInAction } from "mobx";
import moment from "moment";

import { School, EditableSchool } from "../../../models/School";
import { GradingTerm } from "../../../models/GradingTerm";

import { Store } from "../../stores/Store";

export class GradingTermListStore {
    constructor(private store:Store) {
    }

    @observable.ref school!: EditableSchool;
    @observable.ref term ?: GradingTerm;

    @action async init(school:School) {
        this.school = EditableSchool.from(school);
    }

    @action selectTerm = (term:GradingTerm|undefined) => {
        this.term = term ? term.clone() : term;
    }

    @action addNewGradingTerm = () => {
        const {school} = this;
        if (school == null) return;
        const lastTerm = school.tGradingTerm.length < 1 ? null : school.tGradingTerm[school.tGradingTerm.length - 1];
        const startDate = ((lastTerm && lastTerm.endDate) ? moment(lastTerm.endDate).add(1, "d") : moment().startOf("d"));
        this.term = new GradingTerm({
            gradingTermId: -Date.now(), // lastTerm ? -(Math.abs(lastTerm.gradingTermId) + 1) : -1,
            schoolId     : school.schoolId,
            name         : startDate.format("YYYY"),
            startDate    : startDate.valueOf(),
            endDate      : moment(startDate).add("3", "month").valueOf(),
        });
    }

    @action save = async () => {
        const {term} = this;
        if (term == null) return;
        const [err, x] = await term.save();
        if (!err) runInAction(() => {
            this.selectTerm(undefined);
            const {school} = this;
            if (school == null) return;
            if (term.gradingTermId < 1) school.tGradingTerm.push(x);
            else school.tGradingTerm = school.tGradingTerm.map(y => y.gradingTermId == x.gradingTermId ? x : y);
            this.pushChange();
        });
        return err;
    }

    @action removeTerm = async (term:GradingTerm) => {
        const [err] = await term.remove();
        if (!err) runInAction(() => {
            const {school} = this;
            if (school == null) return;
            school.tGradingTerm = school.tGradingTerm.filter(x => x.gradingTermId != term.gradingTermId);
            this.pushChange();
        });
        return err;
    }

    @action pushChange() {
        const {school} = this;
        if (school == null) return;
        this.store.sSchool.items = this.store.sSchool.items.map(s => s.schoolId == school.schoolId ? school.toSchool() : s);
    }
}
