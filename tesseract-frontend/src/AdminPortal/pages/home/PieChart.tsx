import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { ResponsivePie, PieSvgProps } from '@nivo/pie';

const defaultProps: PieSvgProps = {
    data                            : [],
    margin                          : { top: 24, right: 24, bottom: 54, left: 24 },
    innerRadius                     : 0.5,
    padAngle                        : 0,
    cornerRadius                    : 0,
    fit                             : false,
    colors                          : { scheme: 'category10' },
    borderColor                     : { from  : 'color', modifiers: [ [ 'darker', 0.2 ] ] },
    radialLabelsSkipAngle           : 0,
    radialLabelsTextXOffset         : 4,
    radialLabelsTextColor           : "#000000",
    radialLabelsLinkOffset          : 0,
    radialLabelsLinkDiagonalLength  : 12,
    radialLabelsLinkHorizontalLength: 12,
    radialLabelsLinkStrokeWidth     : 2,
    radialLabelsLinkColor           : "black",
    slicesLabelsSkipAngle           : 0,
    slicesLabelsTextColor           : "#ffffff",
    animate                         : true,
    motionStiffness                 : 90,
    motionDamping                   : 15,
    defs                            : [
        { id: 'dots', type: 'patternDots', background: 'inherit', color: 'rgba(255, 255, 255, 0.3)', size: 4, padding: 1, stagger: true },
        { id: 'lines', type: 'patternLines', background: 'inherit', color: 'rgba(255, 255, 255, 0.3)', rotation: -45, lineWidth: 6, spacing: 10 },
    ],
    fill                            : [
        { match: { id: 'ruby' }, id: 'dots' },
        { match: { id: 'c' }, id: 'dots' },
        { match: { id: 'go' }, id: 'dots' },
        { match: { id: 'python' }, id: 'dots' },
        { match: { id: 'scala' }, id: 'lines' },
        { match: { id: 'lisp' }, id: 'lines' },
        { match: { id: 'elixir' }, id: 'lines' },
        { match: { id: 'javascript' }, id: 'lines' },
    ],
    legends                         : [
        {
            anchor: 'bottom',
            direction: 'row',
            translateX: 0,
            translateY: 54,
            itemWidth: 80,
            itemHeight: 20,
            itemDirection: 'left-to-right',
            itemOpacity: 0.85,
            symbolSize: 14,
            effects: [
                {
                    on: 'hover',
                    style: {
                        itemOpacity: 1
                    },
                }
            ]
        }
    ],
}

export const PieChart: FC<PieSvgProps> = observer((props) => {
    return (<ResponsivePie
        {...defaultProps}
        {...props}
    />);
});



