import React, { FC, useEffect, useState } from 'react';
import { observer } from 'mobx-react';

import { getDistrictReport, IDistrictStatistic } from '../../../models/SchoolReport';

import { Typography, Card, Statistic, Skeleton } from "antd";
const { Title } = Typography;

import { BasicLayout } from '../../layouts/BasicLayout';
import { FiveCChart } from './../../../components/FiveCChart';
import { PieChart } from './PieChart';

import { useStore } from "../../stores";
import { useTranslation } from 'react-i18next';
import { Modal } from '../../../components/Modal/ModalAntd';

export const HomePage: FC<{}> = observer(({ }) => {
    const {t} = useTranslation();
    const [report, setReport] = useState<IDistrictStatistic>();
    const {scoreBadge} = useStore();
    useEffect(() => {
        (async () => {
            const [err, stats] = await getDistrictReport();
            if (err) Modal.error({ content: err.message });
            else setReport(stats);
        })();
    }, []);

    return (<BasicLayout>
        <div className="header">
            <Title level={3}>{t('app.admin.dashboard')}</Title>
        </div>
        {(report == null) ? (<Skeleton />) : (<>
            <Card title={t('app.admin.dashboard.adoptionAndParticipation')} className="mb-lg">
                <div className="flex">
                    <div className="flex flex-col self-center">
                        <Statistic className="mt-lg ml-lg" value={report.nOpenAssignments} title={t('app.admin.dashboard.adoptionAndParticipation')} />
                        <Statistic className="mt-lg ml-lg" value={report.nActiveTeachers} title={t('app.admin.dashboard.adoptionAndParticipation.teachersParticipating')} />
                        <Statistic className="mt-lg ml-lg" value={report.nActiveStudents} title={t('app.admin.dashboard.adoptionAndParticipation.studentsParticipating')} />
                    </div>
                    <div className="w-4/5">
                        <div className="header-sub text-center">
                            <Title level={4}>{t('app.admin.dashboard.adoptionAndParticipation.activeSchools')}</Title>
                        </div>
                        <div className="chart-wrap">
                            <PieChart
                                data={[
                                    {   id: `${t('app.admin.dashboard.adoptionAndParticipation.pie.inActive')}`  , 
                                        label: `${t('app.admin.dashboard.adoptionAndParticipation.pie.inActive')}`, 
                                        value: report.nActiveSchools 
                                    },
                                    {   id: `${t('app.admin.dashboard.adoptionAndParticipation.pie.active')}`, 
                                        label: `${t('app.admin.dashboard.adoptionAndParticipation.pie.active')}`, 
                                        value: report.nInactiveSchools 
                                    },
                                ]}
                            />
                        </div>
                    </div>
                </div>
            </Card>
            <Card title={t('app.admin.dashboard.studentAchievements')}>
                <div className="header-sub text-center">
                    <Title level={4}>{t('app.admin.dashboard.studentAchievements.students5C')}</Title>
                </div>
                <div className="chart-wrap">
                    <FiveCChart report={report} scoreBadge={scoreBadge} />
                </div>
            </Card>
        </>)}
    </BasicLayout>);
})


