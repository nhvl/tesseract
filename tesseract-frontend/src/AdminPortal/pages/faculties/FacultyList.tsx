import React, { FC,  useEffect, useMemo, useCallback } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { DefaultId, DbIdentity } from '../../../models/types';
import { DistrictFaculty } from '../../models/DistrictFaculty';

import { useStore } from '../../stores';

import { Select, Card, Typography, Table, Icon, Button, Tooltip, } from 'antd';
const { Title } = Typography;
import { ColumnProps } from 'antd/lib/table';

import { Link } from '../../../components/router/Links';
import { SchoolSelect } from "../../components/SchoolSelect";
import { Faculty } from '../../../models/Faculty';
import { Modal } from '../../../components/Modal/ModalAntd';
import { User } from '../../../models/User';
import { showError } from '../../../services/api/ShowError';
import { storeJwtToken } from '../../../LoginPortal/stores/loginUtil';
import { ImpersonateLink } from '../../components/ImpersonateLink';

export const FacultyList: FC<{}> = observer(({}) => {
    const { t } = useTranslation();
    const {sFaculty, sSchool, currentDistrict, routerStore} = useStore();
    const districtId = currentDistrict == null ? undefined : currentDistrict.districtId;

    useEffect(() => {
        if (districtId == null) return;
        sSchool.init(districtId).then(err => {
            if (err) Modal.error({content: err.message});
        });
    }, [districtId]);

    useEffect(() => {
        sFaculty.init().then(err => {
            if (err) Modal.error({content: err.message});
        });
    }, []);

    const {schoolId} = routerStore.routerState.params;
    const sId = !schoolId ? NaN : Number(schoolId);
    useEffect(() => {
        sFaculty.set_school(Number.isNaN(sId) ? DefaultId : sId);
    }, [sId]);

    const onSchoolChange = useCallback((v:DbIdentity) => {
        if (v == null || v < 1) routerStore.goTo("faculties")
        else routerStore.goTo("facultiesInSchool", {schoolId: String(v)})
        sFaculty.set_school(v);
    }, []);

    return (<>
        <div className="header">
            <Title level={3}>{t('app.faculty')}</Title>
        </div>
        <div className="flex justify-between items-center mb-sm">
            <div>
                <label className="text-black font-medium mr-xs">{t("app.district.faculty.filter")}</label>
                <SchoolSelect
                    value={sFaculty.chosenSchoolId}
                    onChange={onSchoolChange}
                    options={sSchool.items}
                    size="large"
                    className="select-min-w">
                    <Select.Option key={DefaultId} value={DefaultId}>{t("app.district.faculty.filter.selectSchool")}</Select.Option>
                </SchoolSelect>
            </div>
            <div>
                {/* <Checkbox  /> {t("app.district.faculty.includeInactive")} -- huyn 2019-05-22: skip Include Inactive for now */}
            </div>
        </div>
        <Card>
            <div className="mb-sm">
                <Link routeName="createFaculty"><Button type="primary">{t('app.faculty.edit.addFaculty')}</Button></Link>
            </div>
            <FacultyTable />
        </Card>
    </>);
});

export const FacultyTable: FC<{}> = observer(({}) => {
    const { t } = useTranslation();
    const {sFaculty, sSchool} = useStore();

    const getSchoolName = useCallback((item:DistrictFaculty) => {
        const school = sSchool.mSchool.get(item.schoolId);
        return school == null ? "" : school.schoolName;
    }, []);

    const columns = useMemo<ColumnProps<DistrictFaculty>[]>(() => [
        {title:t("app.district.faculty.table.col.firstName" ), key:"firstName" , dataIndex:"firstName" , render:(_, item) => item.firstName, sortDirections: ["descend", "ascend"], sorter: Faculty.sorter.firstName, },
        {title:t("app.district.faculty.table.col.lastName"  ), key:"lastName"  , dataIndex:"lastName"  , render:(_, item) => item.lastName , sortDirections: ["descend", "ascend"], sorter: Faculty.sorter.lastName , },
        {title:t("app.district.faculty.table.col.school"    ), key:"school"    , dataIndex:"school"    , render:(_, item) => getSchoolName(item), },
        {title:t("app.district.faculty.table.col.open"      ), key:"open"      , dataIndex:"open"      , render:(_, item) => item.activityCount.active  > 0 ? (<Link routeName="facultyActivities" params={item.params}>{item.activityCount.active}</Link>) : 0,},
        {title:t("app.district.faculty.table.col.completed" ), key:"completed" , dataIndex:"completed" , render:(_, item) => item.activityCount.passive > 0 ? (<Link routeName="facultyActivities" params={item.params}>{item.activityCount.passive}</Link>) : 0,},
        {key:"actions", render:(_, item) => (<Link routeName="editFaculty" params={item.params}><Tooltip title="Edit"><Icon type="edit" className="icon-sm" /></Tooltip></Link>)},
        {key:"impersonate", render:(_, item) => (<ImpersonateLink userId={item.facultyId}>{t("app.district.faculty.table.impersonate")}</ImpersonateLink>)},
    ], []);

    return (<div className="overflow-y-auto">
        <Table columns={columns} dataSource={sFaculty.chosenItems} pagination={false} rowKey={rowKey}/>
    </div>);
});

function rowKey(record: DistrictFaculty) {return `${record.schoolId}/${record.facultyId}` }
