import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { BasicLayout } from '../../layouts/BasicLayout';
import { FacultyList } from './FacultyList';

export const FacultyListPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <FacultyList />
    </BasicLayout>);
})


