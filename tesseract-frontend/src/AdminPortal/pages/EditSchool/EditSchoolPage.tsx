import React, { FC } from "react";
import { observer } from "mobx-react";

import { BasicLayout } from "../../layouts/BasicLayout";
import { EditSchool } from "./EditSchool";

export const EditSchoolPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <EditSchool />
    </BasicLayout>);
})


