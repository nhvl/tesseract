import React, { FC,  useEffect, useCallback, FormEvent, useState } from "react";
import { observer } from "mobx-react";

import { DefaultId } from "../../../models/types";

import { useStore } from "../../stores";

import { Card, Skeleton, Button, Form, Breadcrumb, Row, Col, Divider, DatePicker, message, Icon, Typography, Checkbox } from "antd";
const { Title } = Typography;

import { Input } from "../../../components/inputs/antd/Input";
import { ImageInput } from "../../../components/ImageInput";

import { Link } from "../../../components/router/Links";
import { GradingTerm } from "../../../models/GradingTerm";
import { GradingTermSelect } from "../../components/GradingTermSelect";
import moment from "moment";
import styles from "./EditSchool.module.less";
import { useTranslation } from "react-i18next";
import { Modal } from "../../../components/Modal/ModalAntd";
import { GradeRangeModal } from "../../../components/GradeRangeModal";

export const EditSchool: FC<{}> = observer(({}) => {
    const {t} = useTranslation()
    const {sEditSchool, routerStore} = useStore();
    const [termName, setTermName] = useState("");
    const [termBegin, setTermBegin] = useState(moment(new Date()));
    const [termEnd, setTermEnd] = useState(moment(new Date()).add("3", "month"));
    const {schoolId} = routerStore.routerState.params;
    let sId = !schoolId ? DefaultId : Number(schoolId);
    if (Number.isNaN(sId)) sId = DefaultId;
    useEffect(() => {
        sEditSchool.init({schoolId:sId}).then(err => {
            if (err) Modal.error({content: err.message});
        })
    }, [sId]);

    const onSubmit = useCallback(async (event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        //Check validation
        if(sEditSchool.school!.schoolName == ""){
            message.error(t('app.school.edit.school.error.required'));
            return;
        }
        if(sEditSchool.school!.logoFile == null && sEditSchool.school!.logoUrl == ""){
            message.error(t('app.school.edit.logo.error.required'));
            return;
        }
        if(sEditSchool.school!.iconFile == null && sEditSchool.school!.iconUrl == ""){
            message.error(t('app.school.edit.icon.error.required'));
            return;
        }
        if(termName == "" && sEditSchool.isCreateNew){
            message.error(t('app.school.edit.term.error.required'));
            return;
        }
        if(termBegin == null && sEditSchool.isCreateNew){
            message.error(t('app.school.edit.startDate.error.required'));
            return;
        }
        if(termEnd == null && sEditSchool.isCreateNew){
            message.error(t('app.school.edit.endDate.error.required'));
            return;
        }

        const err = await sEditSchool.save(new GradingTerm(
            {
                name: termName,
                startDate: termBegin.valueOf(),
                endDate : termEnd.valueOf()
            }));
        if (err) Modal.error({content: err.message});
        else routerStore.goTo("schools");
    }, [termName,termBegin,termEnd]);

    const disabledStartDate = useCallback((startValue)=>{
        const endValue = termEnd;
        if (!startValue || !endValue) return false;
        return startValue.valueOf() > endValue.valueOf();
    }, [termEnd]);

    const disabledEndDate = useCallback((endValue) => {
        const startValue = termBegin;
        if (!endValue || !startValue) return false;
        return endValue.valueOf() <= startValue.valueOf();
    },[termBegin]);

    const onCancelGradeRangeModal = useCallback(() => { sEditSchool.set_isOpenGradeRangeModal(false); }, []);
    const onSubmitGradeRangeModal = useCallback(() => { sEditSchool.set_isOpenGradeRangeModal(false); }, []);

    const {school, loading } = sEditSchool;

    if (school == null) return (<Card><Skeleton /></Card>);

    return (<>
        <div className="breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link routeName="schools">{t('menu.schools')}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    {sEditSchool.isCreateNew ? t('app.school.edit.createTitle') : t('app.school.edit.editTitle')}
                </Breadcrumb.Item>
            </Breadcrumb>
        </div>
        <Card>
            <Form onSubmit={onSubmit}>
                <fieldset disabled={loading}>
                    <div className="mb-lg">
                        <Title level={4} className="paddingBottom-sm">{t('app.school.edit.schoolInformation')}</Title>
                        <Row gutter={16}>
                            <Col xs={24} xxl={16}>
                                <Form.Item label={t('app.school.edit.school')}>
                                    <Input value={school.schoolName} onChange={school.set_schoolName}
                                        type="text" required
                                        placeholder={t('app.school.edit.school')} />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col xs={24} md={12} xxl={8}>
                                <Form.Item label={t('app.school.edit.logo')}>
                                    <div className={styles.imgUpload}>
                                        <ImageInput className="imgFile-sm" image={school.logoUrl} imageFile={school.logoFile} onChange={school.set_logoFile} placeholder={t('app.school.edit.logo.clickToAddLogo')}/>
                                        {(!!school.logoUrl || school.logoFile) && (
                                            <a onClick={school.clear_logo}><Icon type="delete" className="icon-sm" /></a>
                                        )}
                                    </div>
                                </Form.Item>
                            </Col>
                            <Col xs={24} md={12} xxl={8}>
                                <Form.Item label={t('app.school.edit.icon')}>
                                    <div className={styles.imgUpload}>
                                        <ImageInput className="imgFile-sm" image={school.iconUrl} imageFile={school.iconFile} onChange={school.set_iconFile} placeholder={t('app.school.edit.icon.clickToAddIcon')}/>
                                        {(!!school.iconUrl || school.iconFile) && (
                                            <a onClick={school.clear_icon}><Icon type="delete" className="icon-sm" /></a>
                                        )}
                                    </div>
                                </Form.Item>
                            </Col>
                        </Row>
                    </div>
                    <Row gutter={16}>
                        <Col xs={24} md={24} lg={24} xl={24} xxl={16}>
                            <Form.Item label={t("app.classes.gradingRanges")}>
                                {/*TODO: allow edit default grade range*/}
                                <a onClick={() => sEditSchool.set_isOpenGradeRangeModal(true)}>{t("app.classes.gradeRange.linkDefault")}</a>
                            </Form.Item>
                        </Col>
                    </Row>
                    <div>
                        <Title level={4} className="paddingBottom-sm">{t('app.school.edit.currentGradingTerm')}</Title>
                        {sEditSchool.isCreateNew
                        ? (<>
                            <Row gutter={16}>
                                <Col xs={24} xxl={16}>
                                    <Form.Item label={t('app.school.edit.term')}>
                                        <Input value={termName} onChange={setTermName} required
                                        placeholder={t('app.school.edit.term')} />
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={16}>
                                <Col xs={24} md={12} xxl={8}>
                                    <Form.Item label={t('app.school.edit.startDate')} validateStatus = { termBegin ? "" : "error"}>
                                        <DatePicker value={termBegin} onChange={setTermBegin}
                                        size="large" className="w-full" allowClear={false}
                                        format="YYYY-MM-DD HH:mm" showTime={{format: "HH:mm"}}
                                        disabledDate={disabledStartDate}
                                        />
                                    </Form.Item>
                                </Col>
                                <Col xs={24} md={12} xxl={8}>
                                    <Form.Item label={t('app.school.edit.endDate')} validateStatus = { termEnd ? "" : "error"}>
                                        <DatePicker value={termEnd} onChange={setTermEnd}
                                            size="large" className="w-full" allowClear={false}
                                            format="YYYY-MM-DD HH:mm" showTime={{format: "HH:mm"}}
                                            disabledDate={disabledEndDate}
                                            />
                                    </Form.Item>
                                </Col>
                            </Row>
                        </>)
                        : (<>
                            <Row gutter={16}>
                                <Col xs={24} xxl={16}>
                                    <Form.Item label={t('app.school.edit.term')} className={styles.gradingTerm}>
                                        <GradingTermSelect value={school.currentGradingTerm} onChange={school.set_currentGradingTerm} options={school.tGradingTerm} />
                                    </Form.Item>
                                </Col>
                            </Row>
                            {school.currentGradingTermObject &&
                                (<Row gutter={16}>
                                    <Col xs={24} md={12} xxl={8}>
                                        <Form.Item label={t('app.school.edit.startDate')}>
                                            <DatePicker value={moment(school.currentGradingTermObject.startDate)} disabled={true}
                                            size="large" className="w-full"
                                            format="YYYY-MM-DD HH:mm" showTime={{format: "HH:mm"}}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} md={12} xxl={8}>
                                        <Form.Item label={t('app.school.edit.endDate')}>
                                            <DatePicker value={moment(school.currentGradingTermObject.endDate)} disabled={true}
                                                size="large" className="w-full"
                                                format="YYYY-MM-DD HH:mm" showTime={{format: "HH:mm"}}
                                                />
                                        </Form.Item>
                                    </Col>
                               </Row>)}
                        </>)}
                    </div>
                    <Divider />
                    <Button className="mr-sm"><Link routeName="schools">{t('app.school.edit.cancel')}</Link></Button>
                    <Button type="primary" htmlType="submit" loading={loading}>{t('app.school.edit.save')}</Button>
                </fieldset>
            </Form>
            <GradeRangeModal 
                useDefaultGradesRange={sEditSchool.useDefaultGradeRange}
                onCancel = {onCancelGradeRangeModal}
                onSubmit = {onSubmitGradeRangeModal}
                modalGradeRange = {sEditSchool.defaultGradeRange}
                visible = {sEditSchool.isOpenGradeRangeModal}
                />
        </Card>
    </>);
});
