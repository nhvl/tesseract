import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../../models/types";
import { School } from '../../../models/School';
import { Faculty } from "../../../models/Faculty";

import { Store } from "../../stores/Store";
import { Role } from "../../../models/Role";

export class EditFacultyStore {
    constructor(private store:Store) {
    }

    @observable facultyId: DbIdentity = DefaultId;

    @observable.ref faculty   ?: Faculty;
    @observable.shallow schoolIds : DbIdentity[] = [];
    @observable.shallow userRoles : string[] = [];

    @observable.shallow schools   : School[] = [];
    @observable.shallow roles   : Role[] = [];
    @observable.shallow systemRoles   : Role[] = [];

    @action async init({facultyId}:{facultyId: DbIdentity}) {
        this.facultyId = facultyId;
        this.faculty   = undefined;
        this.schoolIds = [];
        this.userRoles = [];
        this.loading   = false;

        const pSchool = this.fetchSchools();
        const pRole = this.fetchRoles();

        if (facultyId < 1) {
            this.faculty = new Faculty();
        } else {
            const [fErr, vm] = await Faculty.getFacultyAsAdmin({facultyId});
            if (this.facultyId != facultyId) return;
            if (fErr) return fErr;
            runInAction(() => {
                this.faculty = vm.faculties[0];
                this.schoolIds = vm.schoolFaculties.filter(sf => sf != null && sf.facultyId == facultyId).map(sf => sf.schoolId);
            });
            const [err, userRoles] = await Role.fetchUserRole({districtId: this.store.currentDistrict!.districtId, userId:this.faculty!.userId});
            if (err) return err;

            const rErr = await pRole;
            if (rErr) return rErr;

            runInAction(() => {
                this.userRoles = userRoles.filter(roleName => this.systemRoles.every(r => r.name != roleName));
            });
        }

        const sErr = await pSchool;
        const rErr = await pRole;
        if (this.facultyId != facultyId) return;
        return sErr || rErr;
    }

    @observable loading = false;
    @action async save() {
        if (this.faculty == null) return;
        this.loading = true;
        const [err, faculty] = await this.faculty.save(this.schoolIds);
        if (err) {
            this.loading = false;
            return err;
        }
        const [rErr] = await Role.saveUserRole({districtId: this.store.currentDistrict!.districtId, userId: faculty.userId}, this.userRoles);
        runInAction(() => {
            this.loading = false;
        });
        return err || rErr;
    }

    @action set_schoolIds = (v:DbIdentity[]) => { this.schoolIds = v }
    @action set_roleIds = (v:string[]) => { this.userRoles = v }

    @computed get isCreateNew() { return this.facultyId == null || this.facultyId < 1 }

    private async fetchRoles() {
        const [err, rs] = await Role.fetchAll();
        if (!err) runInAction(() => {
            this.roles = rs.filter(r => !r.isSystemRole);
            this.systemRoles = rs.filter(r => r.isSystemRole);
        });
        return err;
    }

    private async fetchSchools() {
        const [err, ss] = await School.getSchoolsOfDistrictAsAdmin(this.store.currentDistrict!.districtId);
        if (!err) runInAction(() => {
            this.schools = ss.sort((a,b)=>(a ? a.schoolName : "").localeCompare(b ? b.schoolName : ""));
        });
        return err;
    }
}
