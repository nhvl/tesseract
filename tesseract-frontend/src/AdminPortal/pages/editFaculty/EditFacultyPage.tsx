import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { BasicLayout } from '../../layouts/BasicLayout';
import { EditFaculty } from './EditFaculty';

export const EditFacultyPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <EditFaculty />
    </BasicLayout>);
})


