import React, { FC } from "react";
import { observer } from "mobx-react";

import { Role } from "../../../models/Role";

import { Select } from "antd";

export const RolesSelect: FC<{value:string[], onChange:(_:string[]) => void, options:Role[]}> = observer(({value, onChange, options}) => {
    return (
        <Select mode="multiple"
            value={value}
            onChange={onChange}
            >{options.map(role => (
            <Select.Option key={role.roleId} value={role.name}>{role.name}</Select.Option>
        ))}</Select>
    )
})
