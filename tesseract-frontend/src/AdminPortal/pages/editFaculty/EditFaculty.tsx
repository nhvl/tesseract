import React, { FC,  useEffect, useCallback, FormEvent } from "react";
import { observer } from "mobx-react";

import { DefaultId } from "../../../models/types";

import { useStore } from "../../stores";
import { ErrorCode } from '../../../services/api/AppError';

import { Card, Skeleton, Button, Form, Select, Breadcrumb, Row, Col, Divider, } from "antd";

import { Input } from "../../../components/inputs/antd/Input";
const FormItem = Form.Item;

import { Link } from "../../../components/router/Links";
import { useTranslation } from "react-i18next";
import { Modal } from "../../../components/Modal/ModalAntd";
import { RolesSelect } from "./RolesSelect";

export const EditFaculty: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const {sEditFaculty, routerStore} = useStore();

    const {facultyId} = routerStore.routerState.params;
    let fId = !facultyId ? DefaultId : Number(facultyId);
    if (Number.isNaN(fId)) fId = DefaultId;
    useEffect(() => {
        sEditFaculty.init({facultyId:fId}).then(err => {
            if (err) Modal.error({content: err.message});
        })
    }, [fId]);

    const onSubmit = useCallback(async (event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (sEditFaculty.schoolIds.length < 1) {
            Modal.info({content: t("app.faculty.edit.schoolName.error.selectAtLeast1")});
            return;
        }
        const err = await sEditFaculty.save();
        if (err) {
            if(!err.error) {
                Modal.error({content: err.message});
            }
            else{
                switch (err.error.errorCode) {
                    case ErrorCode.DuplicateEmail:
                        Modal.error({content: t(err.error.message, {email:sEditFaculty.faculty ? sEditFaculty.faculty.email : ""})});
                        break;
                    case ErrorCode.DuplicateUserName:
                        Modal.error({content: t(err.error.message)});
                        break;
                }    
            }
        }
        else routerStore.goTo("faculties");
    }, []);

    const {faculty, schoolIds, loading, set_schoolIds, schools} = sEditFaculty;

    if (faculty == null) return (<Card><Skeleton /></Card>);

    return (<>
        <div className="breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link routeName="faculties">{t("menu.faculty")}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    {sEditFaculty.isCreateNew ? t("app.faculty.edit.createTitle") : t("app.faculty.edit.editTitle")}
                </Breadcrumb.Item>
            </Breadcrumb>
        </div>
        <Card>
            <Form onSubmit={onSubmit}>
                <fieldset disabled={loading}>
                    <Row gutter={16}>
                        <Col xs={24} xxl={16}>
                            <FormItem label={t("app.faculty.edit.schoolName")}>
                                <Select mode="multiple"
                                    value={schoolIds}
                                    onChange={set_schoolIds}
                                    filterOption={true}
                                    optionFilterProp="title"
                                    >{schools.map(school => (
                                    <Select.Option key={school.schoolId} value={school.schoolId} title={school.schoolName}>{school.schoolName}</Select.Option>
                                ))}</Select>
                            </FormItem>
                        </Col>

                        <Col xs={24} xxl={16}>
                            <FormItem label={t("app.faculty.edit.roles")}>
                                <RolesSelect
                                    value={sEditFaculty.userRoles}
                                    onChange={sEditFaculty.set_roleIds}
                                    options={sEditFaculty.roles}
                                    />
                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} md={12} xxl={8}>
                            <FormItem label={t("app.faculty.edit.firstName")}>
                                <Input value={faculty.firstName} onChange={faculty.set_firstName}
                                    placeholder={t("app.faculty.edit.firstName")} />
                            </FormItem>
                        </Col>
                        <Col xs={24} md={12} xxl={8}>
                            <FormItem label={t("app.faculty.edit.lastName")}>
                                <Input value={faculty.lastName} onChange={faculty.set_lastName}
                                    required
                                    placeholder={t("app.faculty.edit.lastName")} />
                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} xxl={16}>
                            <FormItem label={t("app.faculty.edit.email")}>
                                <Input value={faculty.email} onChange={faculty.set_email}
                                    type="email" required
                                    placeholder={t("app.faculty.edit.email")} />
                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} md={12} xxl={8}>
                            <FormItem label={t("app.faculty.edit.phoneNumber")}>
                                <Input value={faculty.phoneNumber} onChange={faculty.set_phoneNumber}
                                    type="tel"
                                    placeholder={t("app.faculty.edit.phoneNumber")}/>
                            </FormItem>
                        </Col>
                        <Col xs={24} md={12} xxl={8}>
                            <FormItem label={t("app.faculty.edit.externalID")}>
                                <Input value={faculty.externalId} onChange={faculty.set_externalId}
                                    placeholder={t("app.faculty.edit.externalID")} />
                            </FormItem>
                        </Col>
                    </Row>
                    <Divider />
                    <Button className="mr-sm"><Link routeName="faculties">{t("app.faculty.edit.cancel")}</Link></Button>
                    <Button type="primary" htmlType="submit" loading={loading}>{t("app.faculty.edit.save")}</Button>
                </fieldset>
            </Form>
        </Card>
    </>);
});
