import React, { FC } from "react";
import { observer } from "mobx-react";

import { useStore } from "../../stores";

import { Statistic, Skeleton, Card, Typography, Icon, Tooltip, Empty } from 'antd';
const { Title } = Typography;

import { School5cChart } from "../../../components/FiveCChart";
import { GradingTermSelect } from "../../components/GradingTermSelect";

import { Link } from '../../../components/router/Links';

export const SchoolStatistic: FC<{}> = observer(({}) => {
    const {sSchool} = useStore();
    const {school, report, gradingTerm, changeGradingTerm} = sSchool;

    if (school == null) return null;

    return (
        <Card className="mt-lg">
            <div className="flex">
                <div className="header">
                    <Title level={2}>{school.schoolName}</Title>
                </div>
                <Link className="mt-2 ml-2" routeName="editSchool" params={({schoolId:String(school.schoolId)})}><Tooltip title="Edit School"><Icon type="edit" className="icon-md" /></Tooltip></Link>
            </div>
            <div>
                <label className="text-black font-medium mr-xs">Grading Period:</label>
                <GradingTermSelect value={gradingTerm} onChange={changeGradingTerm} options={school.tGradingTerm} />
            </div>
            {report === undefined ? (<Skeleton />) : (
             report === null ? (<Empty/>) : (<>
                <Card title="Adoption and Participation" className="none-border mb-lg">
                    <div className="flex justify-between">
                        <Statistic value={report.activityCount.active} title="Open Assignments" />
                        <Statistic value={report.facultyCount.active} title="Teachers Participating" />
                        <Statistic value={report.studentCount.active} title="Students Participating" />
                    </div>
                </Card>

                <Card title="Student Achievements" className="none-border">
                    <div className="header-sub text-center">
                        <Title level={4}>Student's 5C</Title>
                    </div>
                    <div className="chart-wrap">
                        <School5cChart schoolId={report.schoolId} report={report.fiveCScore} />
                    </div>
                </Card>
            </>))}
        </Card>
    );
});
