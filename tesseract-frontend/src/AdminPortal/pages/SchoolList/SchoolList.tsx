import React, { FC, useEffect, useMemo, useCallback } from "react";
import { observer } from "mobx-react";

import { School } from "../../../models/School";

import { useStore } from "../../stores";

import { Table, Avatar, Card, Spin, Button } from "antd";
import { ColumnProps } from "antd/lib/table";

import { Link } from "../../../components/router/Links";
import { Tiny5cChart } from "../../../components/FiveCChart";
import { SchoolStatistic } from "./SchoolStatistic";
import { useTranslation } from "react-i18next";
import { Modal } from "../../../components/Modal/ModalAntd";

export const SchoolList: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const {sSchool, currentDistrict} = useStore();
    const districtId = currentDistrict == null ? undefined : currentDistrict.districtId;
    useEffect(() => {
        if (districtId == null) return;
        sSchool.init(districtId).then(err => {
            if (err) Modal.error({content: err.message});
        });
    }, [districtId]);

    return (<>
        <SchoolTable items={sSchool.items} />
        <SchoolStatistic />
    </>);
});

const SchoolTable: FC<{items:School[]}> = observer(({items: items}) => {
    const {t} = useTranslation();
    const {sSchool} = useStore();
    const columns = useMemo<ColumnProps<School>[]>(() => [
        {title: t('app.school.list.logo')        , key:"logoUrl"   , dataIndex:"logoUrl"   , render:(_, item) => (<Avatar src={item.logoUrl} />) },
        {title: t('app.school.list.school')       , key:"schoolName", dataIndex:"schoolName", render:(_, item) => (item.schoolName)},
        {title: t('app.school.list.teacher')      , key:"teachers"  , dataIndex:"schoolId"  , render:(_, item) => (<Link routeName="facultiesInSchool" params={item.params}>{t('app.school.list.teacher')}</Link>) },
        {title: t('app.school.list.progressBar') , key:"progress"  , dataIndex:"schoolId"  , render:(_, item) => (<SchoolProgress school={item} />), className: "chart-max-w" },
    ], []);

    const onRow = useCallback((record:School) => ({
        onClick: () => sSchool.set_school(record),
    }), []);

    // for observer to observer
    (sSchool.school != null && sSchool.school.schoolId);

    const rowClassName = useCallback((record:School) => (
        (sSchool.school != null && record.schoolId == sSchool.school.schoolId) ? "ant-table-row-selected" : ""
    ), []);


    return (
        <Card>
            <div className="mb-sm">
                <Link routeName="createSchool"><Button type="primary">{t('app.school.list.addSchool')}</Button></Link>
            </div>
            <div className="responsiveTable">
                <Table columns={columns} dataSource={items}
                    pagination={false}
                    rowKey={rowKey} onRow={onRow}
                    rowClassName={rowClassName} />
            </div>
        </Card>
    );
});

const SchoolProgress: FC<{school:School}> = observer(({school}) => {
    const {sSchool} = useStore();
    const report = sSchool.getCachedReport({schoolId:school.schoolId, gradingTermId:school.currentGradingTerm});
    if (report === undefined) return (<Spin />);
    if (report == null) return (null);

    return (<div className="chart-wrap-sm">
        <Tiny5cChart schoolId={report.schoolId} report={report.fiveCScore} />
    </div>);
});

function rowKey(item:School) { return String(item.schoolId) }
