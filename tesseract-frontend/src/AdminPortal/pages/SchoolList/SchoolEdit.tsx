import React, { FC } from "react";
import { observer } from "mobx-react";

import { useStore } from "../../stores";

import { Card, Form, Button } from "antd";

import { Input } from "../../../components/inputs/antd/Input";
import { ImageInput } from "../../../components/ImageInput";

export const SchoolEdit: FC<{}> = observer(({}) => {
    const {sSchool} = useStore();
    const {school} = sSchool;

    if (school == null) return null;

    return (
        <Card className="mt-lg">
            <Form>
                <Form.Item label="Logo">
                    <Input value={school.schoolName} onChange={school.set_schoolName}
                        placeholder="School name" required />
                </Form.Item>
                <Form.Item label="Logo">
                    <ImageInput image={school.logoUrl} imageFile={school.logoFile} onChange={school.set_logoFile} />
                </Form.Item>
                <Form.Item label="Icon">
                    <ImageInput image={school.iconUrl} imageFile={school.iconFile} onChange={school.set_iconFile} />
                </Form.Item>
                <Form.Item label="Current grading term">

                </Form.Item>
                <Form.Item>
                    <Button htmlType="submit" type="primary">Save</Button>
                </Form.Item>
            </Form>
        </Card>
    );
});
