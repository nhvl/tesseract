import React, { FC } from "react";
import { observer } from "mobx-react";

import { Typography, } from "antd";
const { Title } = Typography;

import { BasicLayout } from "../../layouts/BasicLayout";
import { SchoolList } from "./SchoolList";

export const SchoolListPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <div className="header">
            <Title level={3}>Schools</Title>
        </div>
        <SchoolList />
    </BasicLayout>);
});
