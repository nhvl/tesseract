import React, { FC, useMemo, useEffect, useCallback, FormEvent } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DefaultId } from "../../../models/types";

import { useStore } from "../../stores";

import { Typography, Form, Skeleton, Button, Checkbox, Card, Breadcrumb, } from "antd";
const { Title } = Typography;

import { BasicLayout } from "../../layouts/BasicLayout";
import { Input, TextArea } from "../../../components/inputs/antd/Input";
import { showError } from "../../../services/api/ShowError";

import { Link } from "../../../components/router/Links";
import { TextAreaProps } from 'antd/lib/input/TextArea';
import styles from "./EditRole.module.less";

const RoleNameMaxLength = 256;
const RoleDescriptionMaxLength = 2048;
const RoleDescriptionAutoSize: TextAreaProps["autosize"] = ({ minRows: 4, maxRows: 8 });

const EditRole: FC<{}> = observer(() => {
  const { t } = useTranslation();
  const store = useStore();
  const { sEditRole, routerStore } = store;

  const { roleId: sRoleId } = routerStore.routerState.params;

  const goBack = useCallback(() => {
    if (history.length > 1) history.back();
    else routerStore.goTo("roles");
  }, [routerStore]);

  useEffect(() => {
    const roleId = sRoleId == "new" ? DefaultId : Number(sRoleId);
    if (Number.isNaN(roleId)) {
      goBack();
      return;
    }
    sEditRole.init({ roleId }).then(err => {
      if (err == null) return;
      showError(err, t);
    });
  }, [sRoleId, goBack]);

  const handleSubmit = useCallback(async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const err = await sEditRole.save();
    if (err == null) {
      goBack();
    } else {
      showError(err, t, {tNameSpace:"app.settings.role.edit.error", tOptions:{name: sEditRole.item!.name}});
    }
  }, [sEditRole, t, goBack]);

  const { item } = sEditRole;
  if (item == null) return (<Skeleton />);



  return (<>
    <div className="breadcrumb">
        <Breadcrumb>
            <Breadcrumb.Item>
                <Link routeName="roles">{t("menu.district.roles")}</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
                {item.roleId < 1 ? t("app.settings.role.createRole") : t("app.settings.role.editRole")}
            </Breadcrumb.Item>
        </Breadcrumb>
    </div>
    <Card>
      <Form onSubmit={handleSubmit}>
        <Form.Item label={t("app.settings.role.edit.name")}>
          <Input value={item.name} onChange={item.set_name}
            maxLength={RoleNameMaxLength} />
        </Form.Item>

        <Form.Item label={t("app.settings.role.edit.description")}>
          <TextArea value={item.description} onChange={item.set_description}
            maxLength={RoleDescriptionMaxLength}
            autosize={RoleDescriptionAutoSize}
            className={styles.inputWidth}
          />
        </Form.Item>

        <Form.Item label={t("app.settings.role.edit.permissions")}>
          <RolePermissions value={item.permissions} onChange={item.set_permissions} />
        </Form.Item>

        <Form.Item>
          <Button onClick={goBack} className="mr-sm">{t("form.cancel")}</Button>
          <Button type="primary" htmlType="submit">{t("form.save")}</Button>
        </Form.Item>
      </Form>
    </Card>
  </>);
});

const RolePermissions: FC<{ value: string[], onChange: (_: string[]) => void }> = observer(({ value, onChange }) => {
  const { t } = useTranslation();
  const { sEditRole } = useStore();
  const { permissions } = sEditRole;
  const options = useMemo(() => permissions.map(p => ({label:t(`app.permission.${p}`, {defaultValue:p}), value:p})), [permissions]);

  return (
    <Checkbox.Group
      options={options}
      value={value}
      onChange={onChange}
      className={`${styles.permissionRole}`}
    />
  );
})

export const EditRolePage: FC<{}> = observer(() => {
  return (
    <BasicLayout>
      <EditRole />
    </BasicLayout>
  );
});
