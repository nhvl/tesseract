import React, { FC, useMemo, useEffect, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Card, Typography, Table, Divider, Popover, Popconfirm, Icon, Button, } from "antd";
const { Title } = Typography;

import { BasicLayout } from "../../layouts/BasicLayout";
import { showError } from "../../../services/api/ShowError";
import { Role } from "../../../models/Role";
import { TooltipPlacement } from "antd/lib/tooltip";
import { ColumnProps } from "antd/lib/table";
import { Link } from "../../../components/router/Links";

const RoleList: FC<{}> = observer(() => {
    const { t } = useTranslation();
    const store = useStore();
    const { sRoleList } = store;
    useEffect(() => {
        sRoleList.init().then(err => {
            if (err == null) return;
            showError(err, t);
        });
    }, []);

    const columns: ColumnProps<Role>[] = useMemo(() => ([
        {
            key: "name",
            title: t("app.settings.role.list.name"),
            dataIndex: "name"
        },
        {
            key: "description",
            title: t("app.settings.role.list.description"),
            render: (_, item) => (<div className="whitespace-pre-wrap">{item.description}</div>)
        },
        {
            key: "actions",
            title: t("app.settings.role.list.action"),
            render: (_, item) => (<RoleActionCell item={item} />)
        },
    ]), [t]);

    return (
        <Card>
            <Link routeName="editRole" params={{ roleId: "new" }}><Button type="primary" className="mb-sm">{t("app.settings.role.addNew")}</Button></Link>
            <div className="overflow-y-auto">
                <Table dataSource={sRoleList.items}
                    columns={columns}
                />
            </div>
        </Card>
    );
});

export const RolesPage: FC<{}> = observer(() => {
    const { t } = useTranslation();
    return (
        <BasicLayout>
            <div className="header">
                <Title level={3}>{t("app.district.roles")}</Title>
            </div>
            <RoleList />
        </BasicLayout>
    );
});

const RoleActionCell: FC<{ item: Role }> = observer(({ item }) => {
    const { t } = useTranslation();
    if (item.isSystemRole) return null;
    return (<>
        <Link routeName="editRole" params={item.params}>{t("app.settings.role.edit")}</Link>
        <Divider type="vertical" />
        <DeleteRole item={item} />
    </>)
});

const DeleteRole: FC<{ item: Role, placement?: TooltipPlacement }> = observer(({ item, placement = "top" }) => {
    const { t } = useTranslation();
    const {sRoleList} = useStore();
    const onRemove = useCallback(async () => {
        const err = await sRoleList.deleteRole(item.roleId);
        if (err) showError(err, t);
    }, [item.roleId]);

    return (
        <Popconfirm
            placement={placement}
            onConfirm={onRemove}
            title={t("app.settings.role.removeRoleConfirm")}
            okText={t("form.yes")}
            cancelText={t("form.no")}
            okType="danger"
        >
            <a>{t("app.settings.role.delete")}</a>
        </Popconfirm>
    );
});
