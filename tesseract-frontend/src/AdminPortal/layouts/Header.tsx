import React, { CSSProperties } from 'react';
import { observer } from 'mobx-react';

import { Layout } from 'antd';
const { Header } = Layout;

import { GlobalHeader } from '../components/GlobalHeader/GlobalHeader';

const style: CSSProperties = { padding:0 }

export const HeaderView = observer(() => {

    return (
        <Header style={style}>
            <GlobalHeader />
        </Header>
    );
});
