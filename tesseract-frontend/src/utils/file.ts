import { contentType2Extension } from "./getMediaInfo";

export function toBase64(img: File) {
    return new Promise<string>(resolve => {
        const reader = new FileReader();
        reader.addEventListener('load', () => resolve(reader.result as string));
        reader.readAsDataURL(img);
    })
}

export function blob2File(blob:Blob, fileName?:string) {
    const now = Date.now();
    if (!fileName) {
        const ext = contentType2Extension(blob.type);
        fileName = !ext ? `${now}` : `${now}.${ext}`;
    }
    return new File([blob], fileName, {lastModified:now, type:blob.type});
}

