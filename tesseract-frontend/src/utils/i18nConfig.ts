import moment from "moment";

export const momentLocaleMap = {
    'en-US': 'en',
    'es'   : 'es',
    'pt-BR': 'pt-br',
    'vi'   : 'vi',
};
export const languageLabels = {
    'en-US': 'English',
    'es'   : 'Español',
    'pt-BR': 'Português',
    'vi'   : 'Tiếng Việt',
};
export const languageIcons = {
    'zh-CN': `https://lipis.github.io/flag-icon-css/flags/4x3/cn.svg`, // '🇨🇳',
    'zh-TW': `https://lipis.github.io/flag-icon-css/flags/4x3/tw.svg`, // '🇭🇰',
    'en-US': `https://lipis.github.io/flag-icon-css/flags/4x3/us.svg`, // '🇺🇸',
    'en-GB': `https://lipis.github.io/flag-icon-css/flags/4x3/gb.svg`, // '🇬🇧',
    'es'   : `https://lipis.github.io/flag-icon-css/flags/4x3/es.svg`, // '🇪🇸',
    'pt'   : `https://lipis.github.io/flag-icon-css/flags/4x3/pt.svg`, // '🇧🇷',
    'pt-BR': `https://lipis.github.io/flag-icon-css/flags/4x3/br.svg`, // '🇧🇷',
    'vi'   : `https://lipis.github.io/flag-icon-css/flags/4x3/vn.svg`, // '🇻🇳',
};
moment.updateLocale('en', {
    relativeTime: {
        dd(number:number) {
            if (number < 7) return number + ' days'; // Moment uses "d" when it's just 1 day.

            const weeks = Math.round(number / 7);
            return weeks + ' ' + (weeks > 1 ? 'weeks' : 'week');
        }
    }
});
