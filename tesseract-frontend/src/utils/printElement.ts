export interface PrintOptions {
    /** Copy styles over into print window. default: true */
    copyStyles?: boolean;
    /** Callback function to trigger before print */
    onBeforePrint?: () => Promise<any>|undefined;
    /** Callback function to trigger after print */
    onAfterPrint?: () => void;
    /** Override default print window styling */
    pageStyle?: string;
    /** Optional class to pass to the print window body */
    bodyClass?: string;
    /** Optional - remove the iframe after printing. */
    removeAfterPrint?: boolean;
}

/* remove date/time from top */
import defaultPageStyle from "!!raw-loader!./default-print.css";

export async function printElement(contentNodes:HTMLElement, props:PrintOptions) {
    const {
        bodyClass = "",
        copyStyles = true,
        pageStyle,
        onAfterPrint,
        removeAfterPrint,
    } = props;

    if (contentNodes === undefined) {
        console.error("contentEl is null");
        return;
    }

    const printWindow = document.createElement("iframe");
    printWindow.style.position = "absolute";
    printWindow.style.top = "-1000px";
    printWindow.style.left = "-1000px";
    printWindow.id = "printWindow";
    const pFrameLoaded = whenFrameLoad(printWindow);

    {
        const f = document.getElementById("printWindow");
        if (f) document.body.removeChild(f);
    }
    document.body.appendChild(printWindow);

    await pFrameLoaded;

    const fDocument = printWindow.contentDocument || printWindow.contentWindow!.document;
    const srcCanvasEls = contentNodes.querySelectorAll("canvas");

    fDocument.open();
    fDocument.write(contentNodes.outerHTML);
    fDocument.close();

    addStyle(fDocument, pageStyle == null ? defaultPageStyle : pageStyle);

    if (bodyClass.length) {
        fDocument.body.classList.add(bodyClass);
    }

    const canvasEls = fDocument.querySelectorAll("canvas");
    for (let i = 0, canvasElsLen = canvasEls.length; i < canvasElsLen; ++i) {
        const node = canvasEls[i];
        node.getContext("2d")!.drawImage(srcCanvasEls[i], 0, 0);
    }

    if (copyStyles !== false) {
        copyStyleElements(fDocument);

        const ps = copyLinkElement(fDocument);
        if (ps.length > 0) await Promise.all(ps);
    }

    const {onBeforePrint} = props;
    if (onBeforePrint) await onBeforePrint();

    await sleep(500);

    printWindow.contentWindow!.focus();
    printWindow.contentWindow!.print();
    if (onAfterPrint) onAfterPrint();

    if (removeAfterPrint) {
        // The user may have removed the iframe in `onAfterPrint`
        const f = document.getElementById("printWindow");
        if (f) document.body.removeChild(f);
    }
}

function copyLinkElement(targetDocument:Document) {
    const elementLoadPs: Array<Promise<boolean>> = [];

    const linkElements = document.querySelectorAll<HTMLLinkElement>("link[rel='stylesheet']");
    const linkTotal = linkElements.length || 0;

    for (let i = 0, headElsLen = linkTotal; i < headElsLen; ++i) {
        const linkElement = linkElements[i];

        // Many browsers will do all sorts of weird things if they encounter an
        // empty `href` tag (which is invalid HTML). Some will attempt to load the
        // current page. Some will attempt to load the page"s parent directory.
        // These problems can cause `react-to-print` to stop  without any error
        // being thrown. To avoid such problems we simply do not attempt to load
        // these links.
        if (linkElement.hasAttribute("href") && !!linkElement.getAttribute("href")) {
            const newHeadEl = targetDocument.createElement(linkElement.tagName);

            // node.attributes has NamedNodeMap type that is not an Array and can be
            // iterated only via direct [i] access
            for (let j = 0, attrLen = linkElement.attributes.length; j < attrLen; ++j) {
                const attr = linkElement.attributes[j];
                newHeadEl.setAttribute(attr.nodeName, attr.nodeValue!);
            }

            elementLoadPs.push(whenElementLoad(newHeadEl));
            targetDocument.head.appendChild(newHeadEl);
        } else {
            console.warn(`"react-to-print" encountered a <link> tag with an empty "href" attribute. In addition to being invalid HTML, this can cause problems in many browsers, and so the <link> was not loaded. The <link> is:`, linkElement);
        }
    }

    return elementLoadPs;
}

function copyStyleElements(targetDocument:Document) {
    const styleElements = document.querySelectorAll("style");

    for (let i = 0, headElsLen = styleElements.length; i < headElsLen; ++i) {
        const styleElement = styleElements[i];
        const newHeadEl = targetDocument.createElement("STYLE") as HTMLStyleElement;
        const sheet = styleElement.sheet as CSSStyleSheet;

        if (sheet) {
            let styleCSS = "";
            // NOTE: for-of is not supported by IE
            for (let j = 0, cssLen = sheet.cssRules.length; j < cssLen; ++j) {
                if (typeof sheet.cssRules[j].cssText === "string") {
                    styleCSS += `${sheet.cssRules[j].cssText}\r\n`;
                }
            }
            newHeadEl.setAttribute("id", `react-to-print-${i}`);
            newHeadEl.appendChild(targetDocument.createTextNode(styleCSS));
            targetDocument.head.appendChild(newHeadEl);
        }
    }
}

function addStyle(document:Document, style:string) {
    const styleEl = document.createElement("style");
    styleEl.appendChild(document.createTextNode(style));
    document.head.appendChild(styleEl);
}

function whenElementLoad(element:HTMLElement) {
    let resolveFrameLoad: (value: boolean) => void;
    const pFrameLoaded = new Promise<boolean>(resolve => resolveFrameLoad = resolve);
    element.onload = function () {
        resolveFrameLoad(true);
    };
    element.onerror = function() {
        resolveFrameLoad(false)
    };
    return pFrameLoaded;
}

function whenFrameLoad(frame:HTMLIFrameElement) {
    let resolveFrameLoad: () => void;
    const pFrameLoaded = new Promise(resolve => resolveFrameLoad = resolve);
    frame.onload = function () {
        /* IE11 support */
        if (window.navigator && window.navigator.userAgent.indexOf("Trident/7.0") > -1) {
            this.onload = null;
        }
        resolveFrameLoad();
    };
    return pFrameLoaded;
}

function sleep(n:number) { return new Promise(resolve => setTimeout(resolve, n)) }
