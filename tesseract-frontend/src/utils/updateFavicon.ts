export function updateFavicon(logo:string){
    const link:HTMLLinkElement = document.querySelector("link[rel*='icon']") || document.createElement('link');
    if (logo.toLowerCase().endsWith(".ico")) {
        link.type = 'image/x-icon';
    } else if (logo.toLowerCase().endsWith(".png")) {
        link.type = 'image/png';
    }
    link.rel = 'shortcut icon';
    link.href = logo;
    document.getElementsByTagName('head')[0].appendChild(link);
}
