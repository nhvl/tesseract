//Implement darken and lighten css in javascript
//https://codepen.io/jreyesgs/pen/MyKoRj
function addLight(color: string, amount: number) {
    const c = Math.min(parseInt(color, 16) + amount, 255);
    return c.toString(16).padStart(2, "0");
}

function subtractLight(color: string, amount: number) {
    const c = Math.max(parseInt(color, 16) - amount, 0);
    return c.toString(16).padStart(2, "0");
}

export function lighten (color: string, amount: number) {
    color = (color.indexOf("#") >= 0) ? color.substring(1, color.length) : color;
    amount = Math.floor((255 * amount) / 100);
    return `#${addLight(color.substring(0, 2), amount)}${addLight(color.substring(2, 4), amount)}${addLight(color.substring(4, 6), amount)}`;
}

export function darken(color: string, amount: number) {
    color = (color.indexOf("#") >= 0) ? color.substring(1, color.length) : color;
    amount = Math.floor((255 * amount) / 100);
    return `#${subtractLight(color.substring(0, 2), amount)}${subtractLight(color.substring(2, 4), amount)}${subtractLight(color.substring(4, 6), amount)}`;
}
