export function shuffle<T>(xs:T[]): T[] {
    //👍
    return xs.map(x => ({r:Math.random(), x}))
             .sort((a,b) => a.r - b.r)
             .map(({x}) => x);
    //👎
    return [...xs].sort(_ => (Math.random() - .5));
}
