import { RouterState, JsRouterState } from "mobx-state-router";

export function extendsRouterState(baseState:RouterState, options:Partial<JsRouterState>) {
    return RouterState.create({
        ...baseState,
        ...options
    });
}
