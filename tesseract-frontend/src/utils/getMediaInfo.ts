
import { MediaTypeEnum } from "../models/MediaTypeEnum";

import { aFetch } from "../services/api/fetch";

const videoSiteRegex = [
    /youtube\.com/i,
    /youtu\.be/i,
    /vimeo\.com/i
];
const audioSiteRegex = [
    /soundcloud\.com/i,
];
const videoExts = [
    /\.mp4$/i,
    /\.mpeg$/i,
    /\.webm$/i,
    /\.ogv$/i,
];
const audioExts = [
    /\.mp3$/i,
    /\.wav$/i,
    /\.m4a$/i,
    /\.oga$/i,
];
const imageExts = [
    /\.jpg$/i,
    /\.jpeg$/i,
    /\.png$/i,
    /\.gif$/i,
    /\.svg$/i,
    /\.webp$/i,
    /\.tiff$/i,
    /\.bmp$/i,
];

export async function getMediaInfo(url:string): Promise<{mediaType:MediaTypeEnum, contentType:string}> {
    if (videoSiteRegex.some(regex => regex.test(url))) {
        return { mediaType:MediaTypeEnum.VideoLink, contentType:"" };
    }
    if (audioSiteRegex.some(regex => regex.test(url))) {
        return { mediaType:MediaTypeEnum.AudioLink, contentType:"" };
    }
    if (videoExts.some(regex => regex.test(url))) {
        return { mediaType:MediaTypeEnum.VideoFile, contentType:"" };
    }
    if (audioExts.some(regex => regex.test(url))) {
        return { mediaType:MediaTypeEnum.AudioFile, contentType:"" };
    }
    if (imageExts.some(regex => regex.test(url))) {
        return { mediaType:MediaTypeEnum.ImageFile, contentType:"" };
    }

    const mediaInfo = await tryGetMediaInfo(url);
    if (mediaInfo != null) {
        return ({ mediaType:mediaInfo.mediaType, contentType:"" });
    }

    return {mediaType:MediaTypeEnum.Unsupport, contentType:""}
}

export function contentType2MediaType(contentType:string):MediaTypeEnum {
    if (/image\/\w+/i.test(contentType)) {
        return MediaTypeEnum.ImageFile;
    }
    if (/audio\/\w+/i.test(contentType)) {
        return MediaTypeEnum.AudioFile;
    }
    if (/video\/\w+/i.test(contentType)) {
        return MediaTypeEnum.VideoFile;
    }
    return MediaTypeEnum.Unsupport;
}


export function contentType2Extension(contentType:string):string {
    return (
        (contentType.startsWith("video/webm") ? "webm" : (
        (contentType.startsWith("video/mp4" ) ? "mp4"  : (
        (contentType.startsWith("audio/webm") ? "webm" : (
        (contentType.startsWith("audio/wav" ) ? "wav" : (
        (contentType.startsWith("audio/mpeg") ? "mp3" : (
        (contentType.startsWith("image/jpeg") ? "jpg"  : (
        (contentType.startsWith("image/png" ) ? "png"  : (
        ""))))))))))))))
    );
}

async function tryGetMediaInfo(src:string) {
    const imageInfo = await tryGetImageInfo(src);
    if (imageInfo != null) return imageInfo;
    const videoInfo = await tryGetVideoInfo(src);
    if (videoInfo != null) return videoInfo;
    const [err, info] = await aFetch<{contentType?:string, link?:string, mediaType:MediaTypeEnum}>("GET", `/Image/GetMediaInfo`, {url: src});
    if (!err && info.mediaType != MediaTypeEnum.Unknown) return info;
    return undefined;
}

function tryGetVideoInfo(src:string) {
    return new Promise<{mediaType:MediaTypeEnum, width?:number, height?:number}>(resolve => {
        const video = document.createElement("video");
        video.onloadedmetadata = () => {
            resolve({
                mediaType: (video.videoTracks && video.videoTracks.length > 0) ? MediaTypeEnum.VideoFile : MediaTypeEnum.AudioFile,
                width: video.videoWidth,
                height: video.videoHeight,
            });
        };
        video.onerror = (err) => {
            console.warn(err);
            resolve(undefined)
        };
        video.src = src;
    });
}

function tryGetImageInfo(src:string) {
    return new Promise<{mediaType:MediaTypeEnum, width?:number, height?:number}>(resolve => {
        const img = new Image();
        img.onload = () => {
            resolve({
                mediaType: MediaTypeEnum.ImageFile,
                width: img.naturalWidth,
                height: img.naturalHeight,
            });
        };
        img.onerror = (err) => {
            console.warn(err);
            resolve(undefined)
        };
        img.src = src;
    });
}
