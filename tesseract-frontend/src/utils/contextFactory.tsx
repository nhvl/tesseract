import React, { FC, useState, createContext, useContext, ComponentType } from "react";
import { observer } from "mobx-react";

export function contextFactory<S>(useTheStore: () => S) {
    const storeContext = createContext<S>(undefined!);
    const Provider: FC<{}> = observer(({children}) => {
        const store = useTheStore();
        return (
            <storeContext.Provider value={store}>
                {children}
            </storeContext.Provider>
        )
    });
    function useStore() {
        return useContext(storeContext);
    }
    function withStore<P>(Comp:ComponentType<P>) {
        return observer<ComponentType<P>>((props) => (
            <Provider>
                <Comp {...props} />
            </Provider>
        ));
    }
    return ({ Provider, useStore, withStore });
}

function useStateFactory<S>(factory:() => S) {
    const [store] = useState<S>(factory);
    return store;
}



