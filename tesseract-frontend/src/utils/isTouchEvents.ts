// https://github.com/Modernizr/Modernizr/blob/master/feature-detects/touchevents.js

const prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
function mq(query:string) {
    return window.matchMedia(query).matches;
}

function touchevents() {
    // @ts-ignore
    if (('ontouchstart' in window) || window.TouchEvent || window.DocumentTouch && document instanceof DocumentTouch) {
      return true;
    }

    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://github.com/Modernizr/Modernizr/issues/1814
    const query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
}

export const isTouchEvents =
    !!('ontouchstart' in window || navigator.maxTouchPoints)
    // touchevents()
    ;
