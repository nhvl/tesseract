import {useState, useEffect, useRef, RefObject} from "react";

/*
function Usage() {
  const [hoverRef, isHovered] = useHover();

  return (
    <div ref={hoverRef}>{isHovered ? '😁' : '☹️'}</div>
  );
}
*/

export function useHover(): [RefObject<Node>, boolean] {
    const [value, setValue] = useState(false);

    const ref = useRef<Node>(null);

    const handleMouseOver = () => setValue(true);
    const handleMouseOut = () => setValue(false);

    useEffect(
      () => {
        const node = ref.current;
        if (node) {
          node.addEventListener('mouseover', handleMouseOver);
          node.addEventListener('mouseout', handleMouseOut);

          return () => {
            node.removeEventListener('mouseover', handleMouseOver);
            node.removeEventListener('mouseout', handleMouseOut);
          };
        }
        return;
      },
      [ref.current] // Recall only if ref changes
    );

    return [ref, value];
  }
