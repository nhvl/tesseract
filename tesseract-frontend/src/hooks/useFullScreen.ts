import {useState, useEffect} from "react";

export function useFullScreen() {
    const [isFullScreen, setState] = useState<boolean>(screen.width === window.innerWidth);
    useEffect(() => {
        window.addEventListener("resize", handler);

        function handler() {
            setState(screen.width === window.innerWidth)
        }
        return (() => {
            window.removeEventListener("resize", handler);
        });
    }, [setState]);
    return isFullScreen;
}
