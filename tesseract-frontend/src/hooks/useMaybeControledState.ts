import {useState, Dispatch, SetStateAction} from "react";

function Usage() {
    function AccordinItem({
        expanded,
        onExpandedChange = noop,
        initialExpanded = false
    }: {
        expanded        ?:boolean,
        onExpandedChange?:Dispatch<SetStateAction<boolean>>,
        initialExpanded ?:boolean,
    }) {
        const [expandedState, setExpanded] = useMaybeControledState(
            expanded != null,
            expanded!,
            onExpandedChange,
            initialExpanded
        );
    }

    function noop() {}
}

export function useMaybeControledState<S = any>(
    isControlled           : boolean,
    value                  : S,
    onChangeControlledValue: Dispatch<SetStateAction<S>>,
    initialValue           : (S | (() => S))
): [S, Dispatch<SetStateAction<S>>] {
    const [uncontrolledValue, setUncontrolledState] = useState(initialValue);
    return [
        isControlled ? value                   : uncontrolledValue,
        isControlled ? onChangeControlledValue : setUncontrolledState
    ];
}
