import {useEffect, RefObject} from "react";

// https://usehooks.com/useOnClickOutside/

export function useOnClickOutside(ref: RefObject<Element>, handler: (event: MouseEvent|TouchEvent) => any) {
  useEffect(() => {
    const listener = (event: MouseEvent|TouchEvent) => {
      // Do nothing if clicking ref's element or descendent elements
      if (!ref.current || ref.current.contains(event.target as Node)) {
        return;
      }

      handler(event);
    };

    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);

    return () => {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, []); // Empty array ensures that effect is only run on mount and unmount
}
