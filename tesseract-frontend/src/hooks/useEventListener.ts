// https://github.com/donavon/use-event-listener/

import { useRef, useEffect } from "react";

export function useEventListener<K extends keyof WindowEventMap>(eventName:K, handler: (ev: WindowEventMap[K]) => any, element:Window|HTMLElement = window) {
  const savedHandler = useRef<typeof handler>();

  useEffect(() => {
    savedHandler.current = handler;
  }, [handler]);

  useEffect(
    () => {
      const isSupported = element && element.addEventListener;
      if (!isSupported) return;

      const eventListener:typeof handler = (event) => savedHandler.current && savedHandler.current(event);
      element.addEventListener(eventName, eventListener);
      return () => {
        element.removeEventListener(eventName, eventListener);
      };
    },
    [eventName, element]
  );
}

