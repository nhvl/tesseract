import {useRef, useEffect} from "react";

// https://usehooks.com/useWhyDidYouUpdate/

export function useWhyDidYouUpdate<T = any>(name:string, props:T) {
  // Get a mutable ref object where we can store props ...
  // ... for comparison next time this hook runs.
  const previousProps = useRef<T>();

  useEffect(() => {
    const prevProps = previousProps.current;
    if (prevProps) {
      // Get all keys from previous and current props
      const allKeys = Object.keys({ ...prevProps, ...props });
      // Use this object to keep track of changed props
      const changesObj = {};
      // Iterate through keys
      allKeys.forEach(key => {
        // If previous is different from current
        if (prevProps[key] !== props[key]) {
          // Add to changesObj
          changesObj[key] = {
            from: prevProps[key],
            to: props[key]
          };
        }
      });

      // If changesObj not empty then output to console
      if (Object.keys(changesObj).length) {
        console.log('[why-did-you-update]', name, changesObj);
      }
    }

    // Finally update previousProps with current props for next hook call
    previousProps.current = props;
  }, Object.values(props)); // Re-run if props change
}
