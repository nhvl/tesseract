import {useState, useEffect, RefObject} from "react";

// https://www.hooks.guide/community/useDraggable

export function useDraggable(el: RefObject<HTMLElement>) {
  const [{ dx, dy }, setOffset] = useState({ dx: 0, dy: 0 });
  useEffect(() => {
    const handleMouseDown = ({pageX, pageY}: MouseEvent) => {
      const startX = pageX - dx;
      const startY = pageY - dy;
      const handleMouseMove = ({pageX, pageY}: MouseEvent) => {
        const newDx = pageX - startX;
        const newDy = pageY - startY;
        setOffset({ dx: newDx, dy: newDy });
      };
      document.addEventListener("mousemove", handleMouseMove);
      document.addEventListener("mouseup", () => document.removeEventListener("mousemove", handleMouseMove), { once: true });
    };
    el.current!.addEventListener("mousedown", handleMouseDown);
    return () => el.current!.removeEventListener("mousedown", handleMouseDown);
  }, [dx, dy]);

  useEffect(() => {
    el.current!.style.transform = `translate3d(${dx}px, ${dy}px, 0)`;
  }, [dx, dy]);
}
