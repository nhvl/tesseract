import {useState, useLayoutEffect, RefObject, useCallback} from "react";

// https://github.com/rehooks/component-size/blob/master/index.js

declare const ResizeObserver: any;

function getSize(el: HTMLElement|null) {
  if (!el) {
    return {
      width: 0,
      height: 0,
    };
  }

  return {
    width: el.offsetWidth,
    height: el.offsetHeight
  };
}

export function useComponentSize(ref: RefObject<HTMLElement>) {
  let [ComponentSize, setComponentSize] = useState(getSize(ref.current));

  const handleResize = useCallback(
    function handleResize() {
      if (ref && ref.current) {
        setComponentSize(getSize(ref.current))
      }
    },
    [ref]
  );

  useLayoutEffect(() => {
    if (!ref.current) return;

    handleResize();

    if (typeof ResizeObserver === 'function') {
      let resizeObserver = new ResizeObserver(handleResize);
      resizeObserver.observe(ref.current);

      return () => {
        resizeObserver.disconnect(ref.current)
        resizeObserver = null
      };
    } else {
      window.addEventListener('resize', handleResize);

      return () => window.removeEventListener('resize', handleResize);
    }
  }, [ref.current]);

  return ComponentSize;
}
