import {useState, useEffect} from "react";

function getSize() {
  return {
    innerHeight: window.innerHeight,
    innerWidth : window.innerWidth,
    outerHeight: window.outerHeight,
    outerWidth : window.outerWidth,
  };
}

export function useWindowSize() {
  const isClient = typeof window === 'object';

  const [windowSize, setWindowSize] = useState(getSize());

  useEffect(() => {
    if (!isClient) return;

    function handleResize() {
      setWindowSize(getSize());
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []); // Empty array ensures that effect is only run on mount and unmount

  return windowSize;
}
