import {useState, useEffect, useRef} from "react";

// https://github.com/bhaskarGyan/use-throttle/blob/master/src/index.js

export function useDebounce<T = any>(value: T, limit: number) {
    const [throttledValue, setThrottledValue] = useState(value);
    const lastRan = useRef(Date.now());

    useEffect(() => {
      const handler = setTimeout(function() {
        if (Date.now() - lastRan.current >= limit) {
          setThrottledValue(value);
          lastRan.current = Date.now();
        }
      }, limit - (Date.now() - lastRan.current));

      return () => clearTimeout(handler);
    }, [value, limit]);

    return throttledValue;
}
