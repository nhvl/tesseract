import { observable, computed } from "mobx";

import { aFetch } from "../../services/api/fetch";

import { DbIdentity } from "../../models/types";
import { Student } from "../../models/Student";

export class ParentStudent extends Student {
    @observable.shallow activities: { id: number; due: number; }[] = [];

    @computed get activityCount() { return this.activities.length; };
    @computed get nextDueAct() {
        return  this.activities[0];
    }

    constructor(data?: {}) {
        super(data);
        if (data != null){
            Object.assign(this, data);
        }

        if (this.activityCount != 0) {
            this.activities = this.activities.slice().sort((a, b) => a.due - b.due);
        }
    }

    static async fetchForParent(parentId: DbIdentity) {
        const [err, xs] = await aFetch<{}[]>("GET", `/parent/${parentId}/students`);
        return [err, (err ? [] : xs.map(x => new ParentStudent(x)))] as const;
    }

    static sorter = ({
        ...Student.sorter,
        nextDueAct: (a?: ParentStudent, b?: ParentStudent) => ((a && a.nextDueAct && a.nextDueAct.due || -1) - (b && b.nextDueAct && b.nextDueAct.due || -1)),
        activityCount: (a?: ParentStudent, b?: ParentStudent) => ((a.activityCount) - (a.activityCount)),
    });
}
