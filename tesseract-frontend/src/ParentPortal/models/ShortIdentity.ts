import { DbIdentity } from '../../models/types';
export interface ShortIdentity {
    id: DbIdentity;
    name: string;
}
