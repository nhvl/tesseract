import { DbIdentity } from '../../models/types';
import { ShortIdentity } from "./ShortIdentity";
export interface IStudentActivity {
    id         : DbIdentity;
    class      : ShortIdentity;
    term       : ShortIdentity;
    teachers   : ShortIdentity[];
    due        : number | undefined;
    description: string;
    title      : string;
    isOpen     : boolean;
    warning    : boolean;
}
