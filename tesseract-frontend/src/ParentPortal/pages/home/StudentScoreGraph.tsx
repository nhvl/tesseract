import React, { FC } from "react";
import { observer } from "mobx-react";

import { Card, Typography } from "antd";
const { Title } = Typography;

import { IFiveCScore } from "../../../models/SchoolReport";
import { FiveCChart } from "../../../components/FiveCChart";

import { useStore } from "../../stores";

export const StudentScoreGraph: FC<{score: IFiveCScore | undefined}> = observer(({ score }) => {
    const {scoreBadge} = useStore();

    return (
        score == undefined ? null : (
            <Card title="Student Achievements" className="mb-lg">
                <div className="header-sub text-center">
                    <Title level={4}>Student's 5C</Title>
                </div>
                <div className="chart-wrap">
                    <FiveCChart report={score} scoreBadge={scoreBadge} />
                </div>
            </Card>
        )
    )
});
