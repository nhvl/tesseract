import React, { FC, useMemo } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Student } from "../../../models/Student";

import { Table } from "antd";
import { ColumnProps } from "antd/lib/table";

import { Link } from "../../../components/router/Links";
import { StyledDateTime } from '../../../components/DateTime/DateTime';
import { ParentStudent } from "../../models/ParentStudent";

// <div>{t("app.parent.students.table.col.next-assignment" )}: {student.nextDueAct && format(student.nextDueAct.due, "L")}</div>
// <div>{t("app.parent.students.table.col.open-assignments")}: {student.activityCount}</div>

export const StudentListTable: FC<{dataSource:ParentStudent[]}> = observer(({dataSource}) => {
    const { t } = useTranslation();

    const renderTitle          = (_: any, item: ParentStudent) =>                           <Link routeName="studentDetail"  params={{studentId: String( item.studentId )                                        }}> {       item.fullName            } </Link>       ;
    const renderNextDue        = (_: any, item: ParentStudent) => item.nextDueAct != null ? <Link routeName="activityDetail" params={{studentId: String( item.studentId ), activityId: String(item.nextDueAct.id)}}> <StyledDateTime value={ item.nextDueAct.due}/> </Link>: null ;
    const renderOpenAssigments = (_: any, item: ParentStudent) =>                           <Link routeName="activities"     params={{studentId: String( item.studentId )                                        }}> {       item.activityCount       } </Link>       ;

    const columns = useMemo<ColumnProps<ParentStudent>[]>(() => [
        {title:t("app.parent.students.table.col.student"         ), key:"fullName"      , dataIndex:"fullName"      , render: renderTitle                    , sortDirections: ["descend", "ascend"], sorter: Student.sorter.fullName },
        {title:t("app.parent.students.table.col.id-number"       ), key:"studentNumber" , dataIndex:"studentNumber" , render:(_, item) => item.studentNumber , sortDirections: ["descend", "ascend"], sorter: Student.sorter.studentNumber },
        {title:t("app.parent.students.table.col.next-assignment" ), key:"nextDue"       , dataIndex:"nextDue"       , render: renderNextDue                  , sortDirections: ["descend", "ascend"], sorter: ParentStudent.sorter.nextDueAct },
        {title:t("app.parent.students.table.col.open-assignments"), key:"activityCount" , dataIndex:"activityCount" , render: renderOpenAssigments           , sortDirections: ["descend", "ascend"], sorter: ParentStudent.sorter.activityCount, },
    ], []);

    return (<Table columns={columns} dataSource={dataSource} pagination={false} rowKey={rowKey}/>);
});

function rowKey(record: ParentStudent) {return `${record.studentId}` }
