import React, { FC, useMemo, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DbIdentity } from "../../../models/types";

import { useStore } from "../../stores";

import { Table, Card, Button, Badge } from "antd";
import { ColumnProps } from "antd/lib/table";

import { BasicLayout } from "../../layouts/BasicLayout";

import { Link } from "../../../components/router/Links";
import { StyledDateTime } from "../../../components/DateTime/DateTime";
import {StudentBreadcrumb} from "../../components/StudentBreadcrumb";
import { StudentScoreGraph } from "./StudentScoreGraph";
import { Modal } from "../../../components/Modal/ModalAntd";
import { Activity } from "../../../models/Activity";
import { IStudentActivity } from "../../stores/StudentDetailStore";

export const StudentDetailPage: FC<{}> = observer(({ }) => {
    return (
        <BasicLayout>
            <StudentDetail />
        </BasicLayout>
    );
});

const StudentDetail: FC<{}> = observer(({}) => {
    const { mStudent, routerStore, sStudentDetail, currentUser } = useStore();

    const {studentId} = routerStore.routerState.params;
    const sId = !studentId ? NaN : Number(studentId);
    const student = mStudent.get(sId);

    useEffect(() => {
        if (currentUser == undefined || student == undefined) return;

        sStudentDetail.init(student.studentId, currentUser.parentId).then(err => {
            if (err) Modal.error({content: err.message});
        });
    }, [student, currentUser]);

    const nScore = useMemo(() => sStudentDetail.score5c == null ? 0 :
        Object.values(sStudentDetail.score5c).flatMap<number>(m => Object.values(m)).reduce((s, x) => s + x, 0),
        [sStudentDetail.score5c]);

    if (student == null) return (<>Student not found.</>);

    return (
        <>
            <StudentBreadcrumb studentName={student.fullName}/>
            {sStudentDetail.score5c != null && nScore > 0 && (<StudentScoreGraph score={sStudentDetail.score5c}/>)}
            <CompletedAssimentsSection activities={sStudentDetail.completedActivities} studentId={sStudentDetail.studentId} />
            <OpenAssignMentSection     activities={sStudentDetail.openActivities     } studentId={sStudentDetail.studentId} />
        </>
    );
});

const CompletedAssimentsSection: FC<{activities: IStudentActivity[], studentId: DbIdentity}> = observer(({ activities, studentId }) => {
    const { t } = useTranslation();

    return (
        <Card title={t("app.parent.completed-assigments")} className="mb-lg">
            <div className="mb-sm">
                <Button className="mr-sm">{t("app.parent.print")}</Button>
                <Button>{t("app.parent.download")}</Button>
            </div>
            <AssignmentTable activities={activities} studentId={studentId} completed />
        </Card>
    );
});

const OpenAssignMentSection: FC<{activities: IStudentActivity[], studentId: DbIdentity}> = observer(({ activities, studentId }) => {
    const { t } = useTranslation();

    return (
        <Card title={t('app.parent.open-assigments')}>
            <AssignmentTable activities={activities} studentId={studentId} />
        </Card>
    );
});

const AssignmentTable: FC<{
    activities: IStudentActivity[],
    studentId : DbIdentity,
    completed?:boolean,
}> = observer(({ activities, studentId, completed = false }) => {
    const { t } = useTranslation();

    const columns = useMemo<ColumnProps<IStudentActivity>[]>(() => [
        {
            title: t("app.parent.assigments.table.col.class"), key:"class",
            render: (_, item) => (<ClassLink studentId={studentId} item={item} />),
            sortDirections: ["descend", "ascend"],
            sorter: (a, b) => (a.class ? a.class.className : "").localeCompare(b.class ? b.class.className : ""),
        },
        {
            title: t("app.parent.assigments.table.col.teacher"), key:"teachers",
            render: (_, item) => item.faculties.map(tc => tc.fullName).join(", "),
            sortDirections: ["descend", "ascend"],
            sorter: (a, b) => (a.faculties.length < 1 ? "" : a.faculties[0].fullName).localeCompare(
                               b.faculties.length < 1 ? "" : b.faculties[0].fullName),
        },
        {
            title:t("app.parent.assigments.table.col.assignment"), key:"title",
            render: (_, item) => item.activity.title,
            sortDirections: ["descend", "ascend"],
            sorter: (a, b) => a.activity.title.localeCompare(b.activity.title),
            className: "whitespace-normal min-w",
        },
        !completed
        ? {
            title:t("app.parent.assigments.table.col.due"), key:"due",
            render: (_, item) => item.activity.dateDue == null ? t("app.students.detail.DateNA") : (<StyledDateTime value={item.activity.dateDue} />),
            sortDirections: ["descend", "ascend"],
            sorter: (a, b) => (a.activity.dateDue || -1) - (b.activity.dateDue || -1),
        }
        : {
            title:t("app.parent.assigments.table.col.year"), key:"term",
            render: (_, item) => item.gradingTerm ? item.gradingTerm.name : "",
            sortDirections: ["descend", "ascend"],
            sorter: (a, b) => (a.gradingTerm ? a.gradingTerm.name : "").localeCompare(b.gradingTerm ? b.gradingTerm.name : ""),
        },
        { title:"", key:"detail", render: (_: any, item: IStudentActivity) => {
            const isLate = item.activity.isGraded && item.activity.dateDue != null && item.activity.dateDue < Date.now() && (
                item.score == null || !item.score.isSubmitted);
            return (<>
                <Link routeName="activityDetail" params={{studentId: String(studentId), activityId: String(item.activity.activityId)}}>
                    {t("app.parent.assigments.table.cell.view-details")}
                </Link>
                {isLate && (<Badge><i className="fas fa-exclamation-circle text-danger ml-xs" /></Badge>)}
            </>)
        }, },
    ], [studentId, completed]);

    return (
        <div className="overflow-y-auto">
            <Table columns={columns}
                dataSource={activities}
                pagination={false}
                rowKey={rowKey}
                />
        </div>
    );
});

function rowKey(record: IStudentActivity) { return String(record.activity.activityId) }


const ClassLink:FC<{item:IStudentActivity, studentId:DbIdentity}> = observer(({studentId, item}) => {
    return (
        <Link routeName="studentClassDetail"
            params={{studentId:String(studentId), classId:String(item.activity.classId)}}
            >{item.class ? item.class.className : item.activity.classId}</Link>
    );
});
