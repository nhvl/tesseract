import React, { FC, useMemo, useEffect, ReactNode } from "react";
import { observer, useLocalStore } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Student } from "../../../models/Student";

import { useStore } from "../../stores";

import { Table, Card, Typography, Avatar } from "antd";
const {Title} = Typography;
import { ColumnProps } from "antd/lib/table";

import { Link } from "../../../components/router/Links";
import { useNumberTrans } from "../../../components/DateTime/Number";
import { BasicLayout } from "../../layouts/BasicLayout";
import { StudentScoreGraph } from "./StudentScoreGraph";

export const StudentDashboard: FC<{}> = observer(({ }) => (
    <BasicLayout>
        <StudentList/>
    </BasicLayout>)
);

const StudentList: FC<{}> = observer(({}) => {
    const { t } = useTranslation();
    const { tStudent, sStudentList } = useStore();

    useEffect(() => {
        sStudentList.init();
    }, []);

    return (<>
        <div className="header">
            <Title level={3}>{t("app.parent.my-student")}</Title>
        </div>
        {tStudent.map(student => (
            <StudentDetail key={student.studentId} student={student} />
        ))}
    </>);
});

const StudentDetail: FC<{student:Student}> = observer((props) => {
    const {student} = props;

    const { t } = useTranslation();
    const store = useStore();
    const { sStudentList, } = store;

    const score = sStudentList.student2Score.get(student.studentId);

    const sLocal = useLocalStore((props) => ({
        get classes() {
            return store.tClassStudent.filter(cs => cs.studentId == props.student.studentId).map<IStudentClassRecord>(cs => {
                const c = store.mClass.get(cs.classId);
                const fs = store.tClassFaculty.filter(cf => cf.classId == cs.classId)
                    .map(cf => store.mFaculty.get(cf.facultyId)!).filter(Boolean);

                return ({
                    className  : c ? c.className: String(cs.classId),
                    classParams: ({studentId: String(cs.studentId), classId: String(cs.classId)}),
                    grade      : cs.cumulativeGrade,
                    teacher    : fs.map(f => f.fullName).join(", "),
                });
            });
        }
    }), props);

    const nScore = useMemo(() => score == null ? 0 :
        Object.values(score).flatMap<number>(m => Object.values(m)).reduce((s, x) => s + x, 0),
        [score]);

    return (<Card className="mb-lg">
        <div className="flex mb-sm">
            <Avatar src={student.avatar} icon="user" size={64} className="mr-md" />
            <div className="flex flex-col justify-center">
                <div className="text-black">{student.fullName}</div>
                <div className="text-grey font-medium">{student.studentNumber}</div>
            </div>
        </div>

        {score != null && nScore > 0 && (<StudentScoreGraph score={score} />)}

        <StudentClassTable dataSource={sLocal.classes} />
    </Card>);
});

interface IStudentClassRecord {
    className  : string,
    classParams: any,
    teacher    : ReactNode,
    grade     ?: number,
}

const StudentClassTable: FC<{dataSource:IStudentClassRecord[]}> = observer(({dataSource}) => {
    const { t } = useTranslation();
    const {percent} = useNumberTrans();

    const classColumns = useMemo<ColumnProps<IStudentClassRecord>[]>(() => [
        {title:"Class", key:"className", render:(_, item) => (<Link routeName="studentClassDetail" params={item.classParams}>{item.className}</Link>)  },
        {title:"Teacher", key:"teacher", dataIndex:"teacher" },
        {title:"Current Grade", key:"grade", dataIndex:"grade", render:(_, item) => item.grade && percent(item.grade, 0) },
    ], []);


    return (<div className="overflow-y-auto">
        <Table dataSource={dataSource}
            columns={classColumns}
            pagination={false} />
    </div>);
});
