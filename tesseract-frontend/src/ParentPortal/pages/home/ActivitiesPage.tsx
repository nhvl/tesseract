import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { BasicLayout } from '../../layouts/BasicLayout';
import { InConstructionLabel } from '../../../components/Misc/InConstructionLabel';

export const ActivitiesPage: FC<{}> = observer(({ }) => (
    <BasicLayout>
        <InConstructionLabel title="ActivitiesPage is in consctruction"/>
    </BasicLayout>)
);


