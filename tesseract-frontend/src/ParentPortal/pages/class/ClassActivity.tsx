import React, { FC, useCallback, useState } from "react";
import { observer } from "mobx-react";

import { Activity } from "../../../models/Activity";

import { useStore } from "../../stores";

import { Icon } from "antd";

import styles from './ClassActivity.module.less';

export const ClassActivity: FC<{}> = observer(({}) => {
    const {sClassDetail} = useStore();

    return (<>{
        sClassDetail.rootActivities.map(a =>
            <ActivityTree key={a.activityId} item={a} level={0}/>
        )
    }</>);
});

const ActivityTree: FC<{item:Activity, level:number}> = observer(({item, level}) => {
    const {sClassDetail} = useStore();
    const xs = sClassDetail.activityId2Children.get(item.activityId);

    const [collapsed, setCollapsed] = useState(false);
    const toggleCollapsed = useCallback(() => { setCollapsed(collapsed => !collapsed) }, []);

    return (<>
            <div className={`${styles.node} p-2`} style={{marginLeft: `${level*2}em`}}>
                <div className={styles.nodeContent}>
                    {xs && (
                        <a onClick={toggleCollapsed}>{collapsed
                            ? (<span className={styles.rst__expandButton} />)
                            : (<span className={styles.rst__collapseButton} />)
                        }</a>
                    )}
                    {item.title}
                </div>
            </div>
            {xs && !collapsed && xs.map(x => (
                <ActivityTree key={x.activityId} item={x} level={level+1} />
            ))}
        </>
    );
});
