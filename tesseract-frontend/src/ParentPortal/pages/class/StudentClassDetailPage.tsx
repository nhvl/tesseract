import React, { FC, useEffect, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Card, Skeleton, Tabs, Typography, } from "antd";
const {Title} = Typography;

import { useNumberTrans } from "../../../components/DateTime/Number";
import { Link } from "../../../components/router/Links";
import { RouterView } from "../../../components/router/RouterView";
import { BasicLayout } from "../../layouts/BasicLayout";
import { StudentClassBreadcrumb } from "../../components/StudentClassBreadcrumb";

import {ClassActivity} from "./ClassActivity";
import {ClassAssessments, ClassAssignments} from "./ClassAssignments";

import {extendsRouterState} from "../../../utils/extendsRouterState";

export const StudentClassDetailPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <ClassDetail />
    </BasicLayout>);
});

const viewMap = ({
    classDetail     : (<ClassActivity />),
    classActivities : (<ClassActivity />),
    classAssignments: (<ClassAssignments />),
    classAssessments: (<ClassAssessments />),
});

export const ClassDetail: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const {percent} = useNumberTrans();

    const store = useStore();
    const {sClassDetail, routerStore} = store;
    const {routeName, params} = routerStore.routerState;

    const {classId, studentId} = params;
    const cId = !classId ? NaN : Number(classId);
    const sId = !studentId ? NaN : Number(studentId);
    useEffect(() => {
        if (Number.isNaN(cId) || cId < 1 ||
            Number.isNaN(sId) || sId < 1
        ) {
            routerStore.goTo("home"); return;
        }

        sClassDetail.init({classId:cId, studentId:sId});
    }, [cId, sId]);

    useEffect(() => {
        if (routeName == "studentClassDetail")
            store.replaceRouterState(extendsRouterState(routerStore.routerState, {routeName:"classActivities"}))
    }, [routeName])

    const changeTab = useCallback((activeKey: string) => {
        routerStore.goTo(activeKey, params)
    }, [params]);

    const {aClass, student, classStudent, faculties} = sClassDetail;
    if (!aClass) return (<Skeleton />);

    return (<>
        <StudentClassBreadcrumb student={student} aClass={aClass} />

        <div className="flex items-start justify-between">
            <div className="header">
                <Title level={3}>{aClass.className}</Title>
            </div>
            <div className="mb-xs">
                <div className="mb-xs">
                    <label className="text-grey font-medium mr-xs">{t('app.classes.header.classGrade')}:</label>
                    <span className="text-black heading-3">{(classStudent ? percent(classStudent.cumulativeGrade!) : t("app.students.detail.DateNA"))}</span>
                </div>
                <div className="mb-xs">
                    <label className="text-grey font-medium mr-xs">{t('app.classes.header.teacher')}:</label>
                    <span className="text-black">{faculties.map((f) => (
                        <span key={f.facultyId}>{f.lastName} {f.firstName}</span>)
                    )}</span>
                </div>
            </div>
        </div>
        <Card>
            <Tabs activeKey={routeName} onChange={changeTab}>
                <Tabs.TabPane key="classActivities"  tab={<Link routeName="classActivities"  params={params}>{t('app.classes.menu.activities')}</Link>} />
                <Tabs.TabPane key="classAssignments" tab={<Link routeName="classAssignments" params={params}>{t('app.classes.menu.assignments')}</Link>} />
                <Tabs.TabPane key="classAssessments" tab={<Link routeName="classAssessments" params={params}>{t('app.classes.menu.assessments')}</Link>} />
            </Tabs>
            <RouterView viewMap={viewMap} />
        </Card>
    </>);
});
