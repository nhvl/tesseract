import React, { FC, useMemo, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Activity } from "../../../models/Activity";
import { ActivityScore } from "../../../models/ActivityScore";

import { useStore } from "../../stores";

import { Table, } from "antd";
import { ColumnProps } from "antd/lib/table";

import { StyledDateTime } from "../../../components/DateTime";
import { LongScoreMedals } from "../../../components/ScoreMedals";

export const ClassAssessments: FC<{}> = observer(({}) => {
    const {sClassDetail} = useStore();
    const {t} = useTranslation();

    return (
        <ActivityTable items={sClassDetail.cAssessments} activityTitle={t("app.activities.assessment")} />
    );
});

export const ClassAssignments: FC<{}> = observer(({}) => {
    const {sClassDetail} = useStore();
    const {t} = useTranslation();

    return (
        <ActivityTable items={sClassDetail.cAssignments} activityTitle={t("app.activities.assignment")} />
    );
});

const ActivityTable: FC<{items:Activity[], activityTitle:string}> = observer(({items, activityTitle}) => {
    const {t} = useTranslation();
    const {sClassDetail} = useStore();
    const dateRender = useCallback((d:number) => (
        (d == null || d <= 0)
            ? t("app.students.detail.DateNA")
            : (<StyledDateTime value={d} />)
        ), [t]);

    const columns = useMemo<Array<ColumnProps<Activity>>>(() => [
        {
            dataIndex: 'title', key: 'title',
            title: activityTitle,
            className: "whitespace-normal min-w",
        },
        {
            key: 'dateDue',
            title: t("app.students.detail.activtyDueDate"),
            dataIndex: 'dateDue',
            render: dateRender,
            sorter: Activity.sorter.dateDue,
            sortDirections: ['descend', 'ascend']
        },
        {
            dataIndex: 'score',
            key: 'score',
            title: t("app.students.detail.score"),
            className: "whitespace-normal",
            render: (_, a) => (<GradeCell activity={a} score={sClassDetail.mActivityId2Score.get(a.activityId)} />),
            sorter: (a:Activity, b:Activity) => {
                const sa = sClassDetail.mActivityId2Score.get(a.activityId);
                const sb = sClassDetail.mActivityId2Score.get(b.activityId);
                return (sa ? (sa.score || -1) : -1) - (sb ? (sb.score || -1) : -1);
            },
            sortDirections: ['descend', 'ascend']
        },
    ], [t]);

    return (
        <div className="responsiveTable">
            <Table columns={columns}
                dataSource={items}
                pagination={false}
                rowKey={rowKey}
                />
        </div>
    );
});

const GradeCell: FC<{score?: ActivityScore, activity:Activity}> = observer(({score, activity}) => {
    const store = useStore();
    return (score != null && activity.isGraded) ? (
        <div>
            {score.score != null && (
                <span className="font-bold">{score.score}/{activity.maxScore}</span>
            )}
            <LongScoreMedals score={score}
                scoreBadge={store.scoreBadge} />
        </div>
    ) : null;
});

function rowKey(record:Activity) { return String(record.activityId) }


