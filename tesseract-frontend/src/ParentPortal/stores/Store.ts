import { observable, action, computed, reaction, runInAction } from "mobx";
import { uniqBy } from "lodash-es";

import { history } from "../../services/history";
import { routes, notFound } from "../routes";
import { RouterStore, HistoryAdapter } from "mobx-state-router";
import { aFetch } from "../../services/api/fetch";

import { IErrorData } from "../../services/api/AppError";
import { DbIdentity, DefaultId } from "../../models/types";
import { District } from "../../models/District";
import { ParentUser } from "../../models/User";
import { Faculty } from "../../models/Faculty";
import { Class, IClassFaculty } from "../../models/Class";
import { IClassStudent } from "../../models/ClassStudent";
import { ParentStudent } from "../models/ParentStudent";
import { ScoreBadge } from "../../models/ScoreBadge";

import { AuthorizedStore } from "../../stores/AuthorizedStore";
import { GTMStore } from "../../stores/GTMStore";

import { ActivityScoreStore } from "../../stores/ActivityScoreStore";
import { ActivityStore } from "../../stores/ActivityStore";
import { ClassStore } from "../../stores/ClassStore";
import { StudentStore } from "../../stores/StudentStore";
import { FacultyStore } from "../../stores/FacultyStore";

import { AccountSettingStore } from "../../components/Account/Settings/AccountSettingStore";
import { StudentDetailStore } from "./StudentDetailStore";
import { StudentListStore } from "./StudentListStore";
import { ClassDetailStore } from "./ClassDetailStore";
import { GeneralDto, parseGeneralViewModel } from "../../models/GeneralViewModel";

export class Store extends AuthorizedStore {
    @observable.ref currentDistrict?: District;

    @observable.shallow tStudent: ParentStudent[] = [];
    @computed get mStudent() { return observable.map(this.tStudent.map(c => [c.studentId, c])) }

    @observable.shallow tFaculty: Faculty[] = [];
    @computed get mFaculty() { return observable.map(this.tFaculty.map(f => [f.facultyId, f])) }
    @action storeFaculty(xs: Faculty[]) {
        this.tFaculty = uniqBy(xs.concat(this.tFaculty), f => f.facultyId);
    }

    @observable.shallow tClass: Class[] = [];
    @computed get mClass() { return observable.map(this.tClass.map(c => [c.classId, c])) }
    @action storeClass(xs: Class[]) {
        this.tClass = uniqBy(xs.concat(this.tClass), c => c.classId);
    }

    @observable.shallow tClassStudent: IClassStudent[] = [];
    @action storeClassStudent(xs: IClassStudent[]) {
        this.tClassStudent = uniqBy(xs.concat(this.tClassStudent), c => `${c.classId}/${c.studentId}`);
    }
    storeClassStudentOfStudent(studentId:DbIdentity, xs: IClassStudent[]) {
        this.storeClassStudent(
            xs.concat(this.tClassStudent.filter(cs => cs.studentId != studentId))
        );
    }

    @observable.shallow tClassFaculty: IClassFaculty[] = [];
    @action storeClassFaculty(xs: IClassFaculty[]) {
        this.tClassFaculty = uniqBy(xs.concat(this.tClassFaculty), c => `${c.classId}/${c.facultyId}`);
    }



    constructor() {
        super();

        this.routerStore = new RouterStore(this, routes, notFound);
        const historyAdapter = new HistoryAdapter(this.routerStore, history);
        historyAdapter.observeRouterStateChanges();

        this.sLeftNav.menuOpenKeys = ["home", "config"];
        this.sLeftNav.expandedMenuOpenKeys = ["home", "config"];

        reaction(() => this.routerStore.routerState, routerState => {
            switch (routerState.routeName) {
                case "studentDetail":
                case "activityDetail":
                case "activities":
                    this.sLeftNav.set_selectedKey("student" + routerState.params.studentId); break;
                default:this.sLeftNav.set_selectedKey(routerState.routeName);
            }
            this.sGTagManager.run();
        });
    }

    @observable districtId: DbIdentity = DefaultId;
    @observable.ref currentUser ?: ParentUser;
    @action async setToken(token:string) {
        const [err, data] = await super.setToken(token);
        if (!err) runInAction(() => {
            this.districtId = data.districtId;
            this.currentUser = new ParentUser({...data, token});
        });

        this.refresh();

        return [err, data] as const;
    }

    @action async refresh() {
        if (this.currentUser == null) { console.error("currentUser is null"); return; }
        this.refreshScoreBadge();
        this.sGTagManager.initWithDistrict(this.districtId);
        this.fetchStudent();
    }

    async fetchStudent(): Promise<[IErrorData<any> | undefined, ParentStudent[]]> {
        if (this.currentUser == null) throw new Error("currentUser is null");

        const [err, students] = await ParentStudent.fetchForParent(this.currentUser.parentId);
        if (!err) runInAction(() => {
            this.tStudent = students;
            this.sStudent.storeItems(students);
        });

        return [err, students];
    }



    sStudent       = new StudentStore(this);
    sFaculty       = new FacultyStore(this);
    sClass         = new ClassStore(this);
    sActivity      = new ActivityStore(this);
    sActivityScore = new ActivityScoreStore(this);


    async fetchActivityScoresOfStudentInClass({studentId, classId}:{studentId:DbIdentity, classId:DbIdentity}) {
        const {currentUser} = this;
        if (currentUser == null) throw new Error("currentUser is null");

        const [err, dto] = await aFetch<GeneralDto>("GET", `/parent/${currentUser.parentId}/student/${studentId}/class/${classId}/activity`);
        const vm = err ? undefined : parseGeneralViewModel(dto);
        if (vm != null) {
            this.sActivityScore.storeItems(vm.activitiesScores);
            this.sActivity.storeItems(vm.activities);
            this.sClass.storeItems(vm.classes);
        }
        return [err, vm!] as const;
    }
    async fetchFacultyOfClass({studentId, classId}:{studentId:DbIdentity, classId:DbIdentity}) {
        const {currentUser} = this;
        if (currentUser == null) throw new Error("currentUser is null");

        const [err, dto] = await aFetch<GeneralDto>("GET", `/parent/${currentUser.parentId}/student/${studentId}/class/${classId}/faculty`);
        const vm = err ? undefined : parseGeneralViewModel(dto);
        if (vm != null) {
            this.sFaculty.storeItems(vm.faculties);
        }
        return [err, vm!] as const;
    }



    sGTagManager    = new GTMStore(this);
    sAccountSetting = new AccountSettingStore(this);
    sStudentDetail  = new StudentDetailStore(this);
    sStudentList    = new StudentListStore(this);
    sClassDetail    = new ClassDetailStore(this);

    @observable scoreBadge: ScoreBadge = new ScoreBadge();
    @action set_ScoreBadge(scoreColor : ScoreBadge){
        this.scoreBadge = scoreColor;
    }
    @action async refreshScoreBadge(){
        if (this.currentUser == null) return;
        const [err,data] = await ScoreBadge.getDistrictScoreBadge(this.districtId);
        if (!err) this.set_ScoreBadge(data);
        return err;
    }
}
