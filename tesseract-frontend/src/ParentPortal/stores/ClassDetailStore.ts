import { observable, action, runInAction, computed } from "mobx";
import { map, groupBy } from "lodash-es";

import { DbIdentity, DefaultId } from "../../models/types";
import { Activity, ActivityType } from "../../models/Activity";

import { Store } from "./Store";
import { ActivityScore } from "../../models/ActivityScore";
import { IClassStudent } from "../../models/ClassStudent";
import { Faculty } from "../../models/Faculty";

export class ClassDetailStore {
    constructor(private store: Store) {
    }

    @observable studentId: DbIdentity = DefaultId;
    @observable classId: DbIdentity = DefaultId;

    @action async init({studentId, classId}:{studentId:DbIdentity, classId:DbIdentity}) {
        const {currentUser} = this.store;
        if (currentUser == null) return new Error("Unauthenthicate");

        this.activities = [];
        this.scores = [];
        this.classStudent = undefined;
        this.faculties = [];
        this.studentId = studentId;
        this.classId = classId;

        const pFaculty = this.store.fetchFacultyOfClass({classId, studentId});
        const [err, vm] = await this.store.fetchActivityScoresOfStudentInClass({studentId, classId});
        const [fErr, fVm] = await pFaculty;
        if (this.studentId != studentId || this.classId != classId) return;
        if (!err) runInAction(()=> {
            this.activities = vm.activities;
            this.scores = vm.activitiesScores;
            this.classStudent = vm.classStudents[0];
        });
        if (!fErr) runInAction(()=> {
            this.faculties = fVm.faculties;
        });
        return err;
    }



    @computed get aClass() { return this.store.sClass.mId2Item.get(this.classId) }
    @computed get student() { return this.store.sStudent.mId2Item.get(this.studentId) }

    @observable.shallow activities : Activity[] = [];
    @computed get cActivities() {
        return this.activities.filter(a => a.type == ActivityType.Activity || a.type == 0);
    }
    @computed get cAssignments() {
        return this.activities.filter(a => a.type == ActivityType.Assignment);
    }
    @computed get cAssessments() {
        return this.activities.filter(a => a.type == ActivityType.Assessment);
    }
    @computed get id2Activity() {
        return observable.map(this.cActivities.map(a => [a.activityId, a]));
    }
    @computed get rootActivities() {
        return this.cActivities.filter(a => a.parentActivityId == null).sort(Activity.sorter.sortIndex);
    }
    @computed get activityId2Children() {
        return observable.map(
            map(
                groupBy(this.cActivities.filter(a => a.parentActivityId != null), a => a.parentActivityId!),
                xs => [xs[0].parentActivityId!, xs.sort(Activity.sorter.sortIndex)]
            )
        );
    }

    @observable.shallow scores : ActivityScore[] = [];
    @computed get mActivityId2Score() { return observable.map(this.scores.map(s => [s.activityId, s])) }

    @observable.ref classStudent?: IClassStudent;

    @observable.shallow faculties:Faculty[] = [];



    @computed get getActivitiesOfStudentInClass() {
        const {sActivity, sActivityScore} = this.store;
        const {studentId, classId} = this;
        const studentActivityScores = sActivityScore.mStudentId2Items.get(studentId) || [];
        const studentActivitiesInClass = studentActivityScores.map(s => sActivity.mId2Item.get(s.activityId)!)
            .filter(Boolean).filter(a => a.classId == classId);
        const studentActivityScoresInClass = studentActivityScores.filter(s => studentActivitiesInClass.some(a => a.activityId == s.activityId));
        return {activities: studentActivitiesInClass, activityScores:studentActivityScoresInClass};
    }

}
