import { createContext, useContext, Context } from "react";
import { Store } from "./Store";

import { BaseStore } from "../../stores/BaseStore";
import {setBaseStoreContext} from "../../stores/useBaseStore";

export const store = new Store();

export const StoreContext = createContext(store)
setBaseStoreContext(StoreContext as any as Context<BaseStore>);

import {setAuthorizedStoreContext} from "../../stores/useAuthorizedStore";
setAuthorizedStoreContext(StoreContext as Context<any>);

export function useStore() {
    return useContext(StoreContext);
}

// @ts-ignore
window.store = store;
