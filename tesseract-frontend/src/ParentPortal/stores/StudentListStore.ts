import { observable, action, computed } from "mobx";

import { Store } from "./Store";

import { aFetch } from "../../services/api/fetch";
import { DbIdentity, DefaultId } from '../../models/types';
import { IFiveCScore } from "../../models/SchoolReport";
import { IStudentActivity } from "../models/IStudentActivity";
import { GeneralDto, parseGeneralViewModel } from "../../models/GeneralViewModel";

export class StudentListStore {
    constructor(private store: Store) {}

    student2Score = observable.map<DbIdentity, IFiveCScore>();
    student2Classes = observable.map<DbIdentity, IFiveCScore>();

    @action async init() {
        const {currentUser} = this.store;
        if (currentUser == null) return;

        const {parentId} = currentUser;

        const [err, xs] = await this.store.fetchStudent();
        if (err) return err;

        return (await Promise.all(xs.map(async s => {
            const pScore = StudentListStore.fetchStudent5cScores(parentId, s.studentId);
            const pClasses = StudentListStore.fetchStudentClasses(parentId, s.studentId);

            const [sErr, score] = await pScore;
            if (!sErr) this.student2Score.set(s.studentId, score);

            const [cErr, vm] = await pClasses;
            if (!cErr) {
                this.store.storeClass(vm.classes);
                this.store.storeClassStudentOfStudent(s.studentId, vm.classStudents);
                this.store.storeFaculty(vm.faculties);
                this.store.storeClassFaculty(vm.classFaculties);
            }

            return sErr || cErr
        }))).filter(Boolean)[0];
    }

    static async fetchStudentActivities(parentId:DbIdentity, studentId: DbIdentity) {
        const [err, xs] = await aFetch<{scores: IFiveCScore, activities: IStudentActivity[]}>("GET", `/parent/${parentId}/student/${studentId}`);

        return [err, err ? undefined : xs.scores, err ? undefined : xs.activities] as const;
    }

    static async fetchStudent5cScores(parentId:DbIdentity, studentId: DbIdentity) {
        const [err, scores] = await aFetch<IFiveCScore>("GET", `/parent/${parentId}/student/${studentId}/5c`);

        return [err, (err ? undefined : scores)!] as const;
    }

    static async fetchStudentClasses(parentId:DbIdentity, studentId: DbIdentity) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/parent/${parentId}/student/${studentId}/class`);

        return [err, (err ? undefined : parseGeneralViewModel(dto))!] as const;
    }
}
