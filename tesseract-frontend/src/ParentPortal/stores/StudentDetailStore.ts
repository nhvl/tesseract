import { observable, action, computed, runInAction } from "mobx";

import { aFetch } from "../../services/api/fetch";

import { DbIdentity, DefaultId } from "../../models/types";
import { IFiveCScore } from "../../models/SchoolReport";
import { parseGeneralViewModel, GeneralDto } from "../../models/GeneralViewModel";

import { Store } from "./Store";
import { ActivityScore, FiveCValues } from "../../models/ActivityScore";
import { groupBy, mapValues, map } from "lodash-es";
import { Activity } from "../../models/Activity";
import { Faculty } from "../../models/Faculty";
import { Class } from "../../models/Class";
import { GradingTerm } from "../../models/GradingTerm";

export interface IStudentActivity {
    activity    : Activity,
    score      ?: ActivityScore,
    class      ?: Class,
    faculties   : Faculty[],
    gradingTerm?: GradingTerm,
}

export class StudentDetailStore {
    constructor(private store: Store) {}

    @observable studentId : DbIdentity = DefaultId;
    @observable score5c   : IFiveCScore | undefined;
    @observable activities: IStudentActivity[] = [];

    @action async init(studentId: DbIdentity, parentId:DbIdentity) {
        this.studentId = studentId;
        this.activities = [];
        this.score5c = undefined;

        const [err, vm] = await StudentDetailStore.fetchActivityOfStudent(parentId, studentId);
        if (this.studentId != studentId) return;
        if (err) return err;
        runInAction(() => {
            this.store.sActivity.storeItems(vm.activities);
            this.store.sFaculty.storeItems(vm.faculties);
            this.store.sClass.storeItems(vm.classes);

            const score5c = to5CScore(vm.activitiesScores);
            this.score5c = score5c;

            const activityId2Score = new Map(vm.activitiesScores.map(s => [s.activityId, s]));
            const classId2FacultyIds = new Map(map(groupBy(vm.classFaculties, cf => cf.classId), fs => [fs[0].classId, fs.map(f => f.facultyId)]));
            const id2Term= new Map(vm.gradingTerms.map(x => [x.gradingTermId, x]));

            this.activities = vm.activities.map(a => {
                const c = this.store.sClass.mId2Item.get(a.classId);
                const fs = (classId2FacultyIds.get(a.classId) || []).map(facultyId => this.store.sFaculty.mId2Item.get(facultyId)!).filter(Boolean);
                const gradingTerm = c ? id2Term.get(c.gradingTerm) : undefined;

                return ({
                    activity   : a,
                    class      : c,
                    score      : activityId2Score.get(a.activityId),
                    faculties  : fs,
                    gradingTerm: gradingTerm,
                });
            });
        });

        return err;
    }

    @computed get openActivities() { return this.activities.filter(a => a.activity.dateDue != null && Date.now() < a.activity.dateDue) }
    @computed get completedActivities() { return this.activities.filter(a => a.activity.dateDue == null || a.activity.dateDue < Date.now()) }

    static async fetchActivityOfStudent(parentId:DbIdentity, studentId: DbIdentity) {
        const [err, dto] = await aFetch<GeneralDto>("GET", `/parent/${parentId}/student/${studentId}/activity`);
        const vm = (err ? undefined : parseGeneralViewModel(dto))!;
        return [err, vm] as const;
    }
}

function to5CScore(scores: ActivityScore[]): IFiveCScore {
    return ({
        communication   : to5CDict(scores.map(s => s.communication   )),
        collaboration   : to5CDict(scores.map(s => s.collaboration   )),
        character       : to5CDict(scores.map(s => s.character       )),
        creativity      : to5CDict(scores.map(s => s.creativity      )),
        criticalThinking: to5CDict(scores.map(s => s.criticalThinking)),
    })
}

function to5CDict(xs: FiveCValues[]) {
    return (mapValues(
        groupBy(xs.filter(s => s != null && FiveCValues.Unknown < s && s <= FiveCValues.High)),
        xs => xs.length,
    ));
}
