import React, { ReactNode } from "react";
import { RouterState, Route, StringMap, TransitionHook } from "mobx-state-router";

import {mapValues, toPairs} from "lodash-es";

import { Store } from "./stores/Store";

export const homeRoute = new RouterState("home");
export const notFound = new RouterState("notFound");

const checkForUserSignedIn: TransitionHook = async (fromState, toState, routerStore) => {
    const store:Store = routerStore.rootStore;
    const isLogin = await store.checkLogin();
    if (!isLogin) {
        location.reload();
    }
};

import { StudentDashboard    } from "./pages/home/StudentDashboard";
import { StudentDetailPage   } from "./pages/home/StudentDetailPage";
import { PowerSearchDetail   } from "./pages/home/PowerSearchDetail";
import { ActivitiesPage      } from "./pages/home/ActivitiesPage";
import { ActivityDetailPage  } from "./pages/home/ActivityDetailPage";
import { StudentClassDetailPage  } from "./pages/class/StudentClassDetailPage";
import { AccountSettingsPage } from "./pages/Account/Settings/AccountSettingsPage";
import { Exception403        } from "./pages/Exception/403";
import { Exception404        } from "./pages/Exception/404";
import { Exception500        } from "./pages/Exception/500";

export const routeConfig:{[key:string]: {pattern:string, comp:ReactNode, allowAnonymous?:boolean}} = {
    notFound          : ({pattern:"/404"                                            , comp: (<Exception404           />), allowAnonymous:true }),
    403               : ({pattern:"/403"                                            , comp: (<Exception403           />), allowAnonymous:true }),
    500               : ({pattern:"/500"                                            , comp: (<Exception500           />), allowAnonymous:true }),
    home              : ({pattern:"/"                                               , comp: (<StudentDashboard       />) }),
    studentDetail     : ({pattern:"/student/:studentId"                             , comp: (<StudentDetailPage      />) }),
    studentClassDetail: ({pattern:"/student/:studentId/class/:classId"              , comp: (<StudentClassDetailPage />) }),
    classActivities   : ({pattern:"/student/:studentId/classes/:classId/activities" , comp: (<StudentClassDetailPage />), }),
    classAssignments  : ({pattern:"/student/:studentId/classes/:classId/assignments", comp: (<StudentClassDetailPage />), }),
    classAssessments  : ({pattern:"/student/:studentId/classes/:classId/assessments", comp: (<StudentClassDetailPage />), }),
    activities        : ({pattern:"/student/:studentId/activities"                  , comp: (<ActivitiesPage         />) }),
    activityDetail    : ({pattern:"/student/:studentId/activities/:activityId"      , comp: (<ActivityDetailPage     />) }),
    accountSettings   : ({pattern:"/account-settings"                               , comp: (<AccountSettingsPage    />) }),
    powerSearchDetail : ({pattern:"/powerSearch"                                    , comp: (<PowerSearchDetail      />) }),
};

export const appViewMap = mapValues(routeConfig, c => c.comp);
export const routes = toPairs(routeConfig).map<Route>(([name, c]) => ({
    name, pattern:c.pattern,
    beforeEnter: !!c.allowAnonymous ? undefined :checkForUserSignedIn
}));

function safeFromState(state: RouterState) {
    return state.routeName === "__initial__" ? state : homeRoute
}

function redirect(routeName:string, params?: StringMap, queryParams?: Object) {
    return () => Promise.reject(new RouterState(routeName, params, queryParams))
}
