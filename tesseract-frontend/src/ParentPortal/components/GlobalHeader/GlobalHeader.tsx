import React from "react";
import { observer } from "mobx-react";

import { useStore } from "../../stores";

import { Icon } from "antd";

import { RightContent } from "../../../components/GlobalHeader/RightContent";
import { PowerSearch } from "../../../components/PowerSearch";

import styles from "./index.module.less";

export const GlobalHeader = observer(() => {
    const store = useStore();
    const { isMobile, sLeftNav, } = store;

    return (
        <div className={styles.header}>
            {isMobile && (
                (<span className={styles.trigger} onClick={sLeftNav.toggleLeftNavCollapsed}>
                    <Icon type={sLeftNav.collapsed ? "menu-unfold" : "menu-fold"} />
                </span>)
            )}
            <span className={styles.search}>
                <PowerSearch store={store}/>
            </span>
            <RightContent />
        </div>
    );
});




