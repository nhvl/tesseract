import React, { CSSProperties, useCallback } from 'react';
import { observer } from 'mobx-react';

import { Drawer } from 'antd';
import { SiderMenu } from './SiderMenu';

import { useStore } from '../../stores';

const drawerStyle: CSSProperties = {
    padding: 0,
    height: '100vh',
}

export const SiderMenuWrapper = observer(() => {
    const {isMobile, sLeftNav } = useStore();

    const onClose = useCallback(() => { sLeftNav.setLeftNavCollapsed(true); }, []);

    return (
        isMobile ? (
            <Drawer visible={!sLeftNav.collapsed} placement="left" onClose={onClose} style={drawerStyle}>
                <SiderMenu collapsed={isMobile ? false : sLeftNav.collapsed} />
            </Drawer>
        ) : (
            <SiderMenu collapsed={sLeftNav.collapsed} />
        )
    )
});

