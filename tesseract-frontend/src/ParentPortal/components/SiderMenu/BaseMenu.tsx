import React, { FC, CSSProperties, useMemo, useCallback, useEffect, useState } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Menu, Icon, message } from "antd";

import { Link } from "../../../components/router/Links";
import { subMenu } from "../../../components/inputs/antd/subMenu";

const style: CSSProperties = { padding: "0 0 16px 0", width: "100%" };

export const BaseMenu: FC<{}> = observer(() => {
    const [scrollMenuStyle, set_ScrollMenuStyle] = useState(undefined as CSSProperties|undefined);
    const store = useStore();
    const {sLeftNav} = store;
    const {t} = useTranslation();
    const selectedKeys = useMemo(() => [store.sLeftNav.selectedKey], [store.sLeftNav.selectedKey]);
    const openAeries = useCallback(() => message.info(t("menu.openaeries.open")), [t]);
    useEffect(() => {
        updateScrollMenu();
    }, []);
    const updateScrollMenu = useCallback(()=>{
        const item: CSSProperties = {height: "100%", maxHeight:(window.innerHeight - sLeftNav.nonMenuHeight), overflowY: "auto" }
        set_ScrollMenuStyle(item);
    },[window.innerHeight]);
    return (<div style={scrollMenuStyle}>
        <Menu theme="dark" mode="inline"
            selectedKeys={selectedKeys}
            openKeys = {store.sLeftNav.menuOpenKeys}
            onOpenChange = {store.sLeftNav.set_menuOpenKeys}
            style={style}
            inlineIndent={18}
            className="main-nav"
            >
            {...subMenu({
                collapsed:sLeftNav.collapsed,
                subMenuClassName: "level-1",
                key:"home",
                subMenuTitle:(
                    <Link routeName="home"><Icon type="home" /><span>{t("app.parent.menu.mystudent")}</span></Link>
                ),
                menuItems:store.tStudent.map(student => (
                    <Menu.Item key={"student" + student.studentId}>
                        <Link routeName="studentDetail" params={student.params}>
                            {student.fullName}
                        </Link>
                    </Menu.Item>
                )),
            })}

            <Menu.Item key="aeries">
                <a onClick={openAeries}>
                    <img className="anticon" src="https://ps.orangeusd.org/favicon.ico" alt={t("menu.openaeries")} />
                <span>{t("menu.openaeries")}</span>
                </a>
            </Menu.Item>
        </Menu>
    </div>)
})
