import React, { FC, useEffect, CSSProperties, useMemo, } from 'react';
import { observer } from 'mobx-react';
import { qs } from '../../../utils/url';

import { Layout, Icon, Spin, Avatar, Tooltip, Modal, Divider, } from 'antd';
const {Sider} = Layout;

import { SideFooter } from '../../../components/SideFooter';
import { BaseMenu } from './BaseMenu';
import { Link } from '../../../components/router/Links';

import styles from './index.module.less';

import { useStore } from '../../stores';
import { useTranslation } from 'react-i18next';

const spinStyle:CSSProperties = { marginLeft: 8, marginRight: 8 };

const defaultLogo =  "https://district1.tesseract.dory.vn/static/media/logo.f921bfde.png";

export const SiderMenu: FC<{collapsed:boolean}> = observer(({collapsed}) => {
    const {t} = useTranslation();
    const store = useStore();
    const {logout, sLeftNav, currentUser} = store;
    const helpUrl = useMemo(() => qs("mailto:support@denovu.com", {
        cc     : undefined,
        bcc    : undefined,
        subject: t("menu.account.help.subject"),
        body   : `\n\n${currentUser ? `--\n${currentUser.firstName} ${currentUser.lastName} | ${currentUser.userId}` : ""}`,
    }).toJSON(), []);

    return (
        <Sider trigger={null} collapsible collapsed={collapsed}
            breakpoint="lg"
            width={256}
            className={`${styles.sider} slider ${styles.fixSiderBar}`}>
            <div className="slider-footer">
                <div className="menu">
                    <div className={`${styles.logo} logo`}>
                        <span className={`${styles.trigger} trigger`} onClick={sLeftNav.toggleLeftNavCollapsed}>
                            <Icon type={sLeftNav.collapsed ? 'menu-unfold' : 'menu-fold'} />
                        </span>
                        <img src={defaultLogo} alt={t('menu.district.logo')} />
                    </div>
                    <div className={`${styles.infoAccount} infoAccount`}>
                        <Avatar size="large" className={styles.avatar} src={(currentUser && currentUser.avatar)} alt={t('menu.account.avatar')} icon="user" />

                        {!sLeftNav.collapsed && (<>
                        <div className="font-bold text-black">
                            {currentUser ? (
                                <span className={styles.name}>{currentUser.fullName}</span>
                            ) : (
                                <Spin size="small" style={spinStyle} />
                            )}
                        </div>
                        <ul className={styles.listIcons}>
                            <li>
                            <Tooltip title={t('menu.account.settings')}>
                                    <Link routeName="accountSettings"><Icon type="setting" className={styles.large} /></Link>
                                </Tooltip>
                            </li>
                            <li>
                                <Tooltip title={t('menu.account.notifications')}>
                                    <a><Icon type="bell" className={styles.large} /></a>
                                </Tooltip>
                            </li>
                            <li>
                                <Tooltip title={t('menu.account.help')}>
                                    <a href={helpUrl}
                                        target="_blank" rel="noopener noreferrer" className={styles.action}>
                                        <Icon type="question-circle" className={styles.large} />
                                    </a>
                                </Tooltip>
                            </li>
                            <li>
                                <Tooltip title={t('menu.account.logout')}>
                                    <a onClick={logout}>
                                        <Icon type="logout" className={styles.large} />
                                    </a>
                                </Tooltip>
                            </li>
                        </ul>
                        </>)}
                        <div className={styles.marginLeftRight}>
                            <Divider/>
                        </div>
                    </div>
                    <BaseMenu />
                </div>
                <SideFooter collapsed={sLeftNav.collapsed} />
            </div>
        </Sider>
    )
});

