import React, { FC } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Breadcrumb } from "antd";

import { Link } from "../../components/router/Links";

export const StudentBreadcrumb: FC<{studentName: string}> = observer(({ studentName }) => {
    const { t } = useTranslation();

    return(
        <div className="breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link routeName="home">{t("app.parent.my-student")}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{studentName}</Breadcrumb.Item>
            </Breadcrumb>
        </div>
    );
});
