import React, { FC } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Breadcrumb, Spin, } from "antd";

import { Link } from "../../components/router/Links";

import { Student } from "../../models/Student";
import { Class } from "../../models/Class";

export const StudentClassBreadcrumb:FC<{student?:Student, aClass?:Class}> = observer(({student, aClass, children}) => {
    const { t } = useTranslation();

    return (
        <div className="breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link routeName="home">{t("app.parent.my-student")}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    {student ? (<Link routeName="studentDetail" params={student.params}>{student.fullName}</Link>) : (<Spin/>)}
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    {t('menu.classes')}
                </Breadcrumb.Item>
                {aClass && (
                    <Breadcrumb.Item>{aClass.className}</Breadcrumb.Item>
                )}
                {children}
            </Breadcrumb>
        </div>
    );
});
