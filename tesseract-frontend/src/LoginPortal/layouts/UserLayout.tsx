import React, { FC, useEffect, useState } from 'react';
import { observer } from "mobx-react";

import { Icon, Card, Typography, } from 'antd';
const { Title } = Typography;
import { useTranslation } from 'react-i18next';

import { District } from '../../models/District';

import { useStore } from '../stores';

import { GlobalFooter } from '../../components/GlobalFooter';
import { SelectLang } from '../../components/SelectLang/index';

import styles from './UserLayout.module.less';

const links = [
  { key: 'help', children: "Help", href: '' },
  // { key: 'privacy', children: "Privacy", href: '' },
  // { key: 'terms', children: "Terms", href: '' },
].slice(1);

const copyright = (
  <>
    Copyright <Icon type="copyright" /> 2019 Tesseract
  </>
);


export const UserLayout: FC<{ className?: string }> = observer(({ children }) => {
  const {sDistrict} = useStore();
  const { t } = useTranslation();

  useEffect(() => {
    District.getDistrict().then(([error, disctrict]) => {
      if (error){
        throw(error.message);
      }

      sDistrict.set_district(disctrict);
    });
  }, []);
  return (
    <div className={styles.container}>
      <div className={styles.lang}>
        <SelectLang />
      </div>
      <div className={styles.containerLogin}>
        <div className={styles.h100}>
          <div className={styles.content}>
            <div className={styles.wrapLogin}>
              <Card className={styles.login}>
                {children}
              </Card>
              <div className={styles.branching}>
                <a className="no-underline">{sDistrict.district.logoUrl && (<img alt="logo" className={styles.logo} src={sDistrict.district.logoUrl} />)}</a>
                <span className={`${styles.title} block text-color-inverse`}>Tesseract</span>
                <span className={styles.desc}>{sDistrict.district.districtName}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <GlobalFooter links={links} copyright={copyright} />
    </div>
  );
})


