import { observable, action } from "mobx";

import { aFetch } from "../../services/api/fetch";
import {storeJwtToken} from "./loginUtil";

import { Store } from "./Store";

export class LoginStore {
    constructor(private store:Store) {
    }

    @observable email     : string  = "";//"faculty@tesseract.com";
    @observable password  : string  = "";//"Asd123!!";
    @observable rememberMe: boolean = true;

    @action set_email      = (v: string ) => { this.email      = v;this.store.set_temp_email(v); }
    @action set_password   = (v: string ) => { this.password   = v; }
    @action set_rememberMe = (v: boolean) => { this.rememberMe = v; }

    @action async doLogin() {
        const [err, data] = await aFetch<{token:string}>("POST", "/Account/Login", {
            email: this.email, password:this.password, rememberMe:this.rememberMe
        });

        if (!err) {
            storeJwtToken(data.token, this.rememberMe);
            location.href = this.signInRedirect;
        }

        return err;
    }

    async requestConfirmEmail () {
        const [err] = await aFetch<{token:string}>("POST", "/Account/RequestConfirmEmail", this.email);
        return err;
    }

    signInRedirect      : string = "/";
}
