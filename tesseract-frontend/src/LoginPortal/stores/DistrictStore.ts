import { observable, action } from "mobx";

import { District } from '../../models/District';

import { Store } from "./Store";

export class DistrictStore {
    constructor(private store:Store) {
    }

    @observable district: District = new District();
    set_district    = (v: District) => { this.district = v }
}
