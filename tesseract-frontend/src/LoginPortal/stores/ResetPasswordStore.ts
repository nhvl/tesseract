import { observable, action } from "mobx";
import { aFetch } from "../../services/api/fetch";

export class ResetPasswordStore {
    @observable password     : string  = "";
    @observable confirmPassword: string = "";
    @observable email:string = "";
    @action set_password      = (v: string ) => { this.password = v; }
    @action set_confirmPassword      = (v: string ) => { this.confirmPassword = v; }
    @action set_email = (v:string)=>{this.email = v;}



    @action async requestResetPassword(): Promise<[Error|null, any]> {
        var urlParams = new URLSearchParams(location.search);
        if(urlParams.has('code') == false){
            return[new Error("InvalidCode"), null];
        }
        var token= urlParams.get('code');
        const [err, data] = await aFetch<{token:string}>("POST", "/Account/ResetPassword", {
            password: this.password,
            email: this.email,
            code: token
        });
        if (err) {
            return [err, null as any];
        }

        return [null, data];
    }
}
