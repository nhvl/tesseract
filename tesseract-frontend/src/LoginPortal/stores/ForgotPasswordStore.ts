import { observable, action } from "mobx";
import { aFetch } from "../../services/api/fetch";
import { Store } from "./Store";

export class ForgotPasswordStore {
    @observable email     : string  = "";
    @action set_email      = (v: string ) => { this.email = v; }

    constructor(private store:Store) {
    }

    @action async requestForgotPassword(): Promise<[Error|null, any]> {
        const [err, data] = await aFetch<{token:string}>("POST", "/Account/ForgotPassword", {
            email: this.email,
            host: location.origin
        });
        if (err) {
            return [err, null as any];
        }

        return [null, data];
    }
}
