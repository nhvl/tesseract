import { JwtTokenKey } from "../../config";

export function isPersistent() {
    const token = sessionStorage.getItem(JwtTokenKey);
    return !token;
}

export function storeJwtToken(token:string, isPersistent:boolean|"auto") {
    if (isPersistent) {
        localStorage.setItem(JwtTokenKey, token);
    } else {
        sessionStorage.setItem(JwtTokenKey, token);
    }
}

export function getJwtToken() {
    return (
        localStorage.getItem(JwtTokenKey) ||
        sessionStorage.getItem(JwtTokenKey)
    );
}

export function clearJwtToken() {
    localStorage.removeItem(JwtTokenKey);
    sessionStorage.removeItem(JwtTokenKey);
}

