
import {history} from "../../services/history";
import { routes, notFound } from "../routes";
import { RouterStore, HistoryAdapter } from "mobx-state-router";
import { action } from "mobx";
import { LoginStore } from "./LoginStore";

import {BaseStore} from "../../stores/BaseStore";
import { ForgotPasswordStore } from "./ForgotPasswordStore";
import { DistrictStore } from "./DistrictStore";
import { RegisterStore } from "../pages/register/RegisterStore";

export class Store extends BaseStore {
    @action set_temp_email = (v: string ) => { this.sForgotPassword.email = this.setTempEmail(v); }
    constructor() {
        super();

        this.routerStore = new RouterStore(this, routes, notFound);
        const historyAdapter = new HistoryAdapter(this.routerStore, history);
        historyAdapter.observeRouterStateChanges();

        this.refreshLocales();
    }
    @action setTempEmail = (email:string) => {
        if(this.validateEmail(email)){
            return email;
        }else{
            return "";
        }
    }
    @action validateEmail=(email:string) =>{
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    sLogin          = new LoginStore(this);
    sRegister       = new RegisterStore(this)
    sForgotPassword = new ForgotPasswordStore(this);
    sDistrict       = new DistrictStore(this);
}
