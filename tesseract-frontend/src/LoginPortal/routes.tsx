import React, { ReactNode, FC, unstable_Profiler, useEffect } from 'react';
import { RouterState, Route, StringMap, TransitionHook } from 'mobx-state-router';
import { observer } from 'mobx-react';
import { mapValues, toPairs } from 'lodash-es';

export const homeRoute = new RouterState("home");
export const notFound = new RouterState('notFound');

import { useStore } from './stores';

import {LoginPage} from "./pages/login/LoginPage";
import {RegisterPage} from "./pages/register/RegisterPage";
import {ForgotPasswordPage} from "./pages/forgotPassword/ForgotPasswordPage";
import {ResetPasswordPage} from "./pages/resetPassword/ResetPasswordPage";
const NotFound: FC = observer(() => {
    const store = useStore();
    useEffect(() => {
        store.sLogin.signInRedirect = location.href;
        store.routerStore.goTo("login");
    }, []);
    return null;
});

export const routeConfig: {[key:string]: (Pick<Route, Exclude<keyof Route, "name">> & {comp:ReactNode})} = {
    notFound      : ({pattern:"/404"            , comp: (<NotFound />), beforeEnter:redirect("login"), onEnter:redirect("login"), }),
    home          : ({pattern:"/" , comp: (null), beforeEnter:redirect("login") }),
    login         : ({pattern:"/login"          , comp: (<LoginPage          />), }),
    register      : ({pattern:"/register"       , comp: (<RegisterPage       />), }),
    forgotPassword: ({pattern:"/forgot-password", comp: (<ForgotPasswordPage />), }),
    resetPassword : ({pattern:"/reset-password" , comp: (<ResetPasswordPage  />), }),
};

export const appViewMap = mapValues(routeConfig, c => c.comp);
export const routes = toPairs(routeConfig).map<Route>(([name, {comp, ...c}]) => ({
    name,
    ...c
}));


function safeFromState(state: RouterState) {
    return state.routeName === "__initial__" ? state : homeRoute
}

function redirect(routeName:string, params?: StringMap, queryParams?: Object): TransitionHook {
    return (fromState, toState, routerStore) => Promise.reject(new RouterState(routeName, params, queryParams));
}

