import React, { FC, useCallback, useState, FormEvent, useRef } from 'react';
import { observer } from "mobx-react";
import { useTranslation } from 'react-i18next';
import { UserLayout } from '../../layouts/UserLayout';
import { ForgotPassword} from './ForgotPassword';

import classNames from 'classnames';

export const ForgotPasswordPage: FC<{ className?: string }> = observer(({ className = "" }) => {
    return(
        <UserLayout>
            <ForgotPassword/>
        </UserLayout>
    );
})
