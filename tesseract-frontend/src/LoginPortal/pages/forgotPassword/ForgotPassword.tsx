import React, { FC, useCallback, useState, FormEvent, useRef } from 'react';
import { observer } from "mobx-react";
import { Icon, Button, Form, Typography, } from 'antd';
const { Title } = Typography;
import styles from '../login/Login.module.less';
import { Input, Checkbox } from '../../../components/inputs/antd/Input';
import { useTranslation } from 'react-i18next';
import { Link } from '../../../components/router/Links';
import { useStore } from '../../stores';
const FormItem = Form.Item;
const useNameIcon = (<Icon type="user" className={styles.prefixIcon} />);

import classNames from 'classnames';
import { initial } from 'lodash-es';
import { Modal } from '../../../components/Modal/ModalAntd';

export const ForgotPassword: FC<{ className?: string }> = observer(({ className = "" }) => {
    const { t } = useTranslation();
    const [submitting, setSubmitting] = useState(false);
    const {sForgotPassword} = useStore();

    const onSubmit = useCallback((event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (submitting) { debugger; return; }

        setSubmitting(true);

        var result = sForgotPassword.requestForgotPassword().then(([err,data]) => {
          setSubmitting(false);
          if (err) {
            Modal.error({ content: t("app.login.error.invalid") });
          }
          if(data.result == -1){
            Modal.error({ content: t("app.forgotpassword.error.accountnotexist") });
          }
          else if(data.result == 1){
            Modal.info({content:t("app.forgotpassword.emailsend")});
          }


        });
      }, []);

    return(
        <div className={styles.main}>
        <div className={classNames(className, styles.login)}>
          <div className="mb-lg capitalize">
            <Title level={3}>{t("app.login.forgot-pass")}</Title>
          </div>
          <Form onSubmit={onSubmit}>
            <fieldset disabled={submitting}>
              <FormItem>
                  <Input value={sForgotPassword.email} onChange={sForgotPassword.set_email}
                  type="email" required placeholder={t("form.email.placeholder")}
                  name="email" autoFocus autoComplete="email username"
                  size="large" prefix={useNameIcon}
                  />
              </FormItem>
              <FormItem>
                <Button size="large" type="primary"
                  className={classNames(styles.submit, className)}
                  htmlType="submit" loading={submitting}>{t('app.forgotpassword.resetpassword')}</Button>
              </FormItem>
              <div className="mb-lg">{t('app.forgotpassword.or')} <Link routeName="login">{t("app.login.log-in")}</Link></div>
            </fieldset>
          </Form>
        </div>
        </div>

    );
})
