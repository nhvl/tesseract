import React, { FC } from "react";
import {observer} from "mobx-react";

import { UserLayout } from "../../layouts/UserLayout";
import { Login } from "./Login";

export const LoginPage: FC<{className?:string}> = observer(() => {
  return (
    <UserLayout>
      <Login />
    </UserLayout>
  );
})


