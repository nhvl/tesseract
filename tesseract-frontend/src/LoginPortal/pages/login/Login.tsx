import React, { FC, useCallback, useState, FormEvent } from 'react';
import { observer } from "mobx-react";
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';

import { ErrorCode } from '../../../services/api/AppError';

import { useStore } from '../../stores';

import { Icon, Button, Form, message, Typography, } from 'antd';
const FormItem = Form.Item;
const { Title } = Typography;

import { Input, Checkbox } from '../../../components/inputs/antd/Input';
import { Link } from '../../../components/router/Links';

import styles from './Login.module.less';
import { showError } from '../../../services/api/ShowError';
import { Modal } from '../../../components/Modal/ModalAntd';

const useNameIcon = (<Icon type="user" className={styles.prefixIcon} />);
const passwordIcon = (<Icon type="lock" className={styles.prefixIcon} />);

export const Login: FC<{ className?: string }> = observer(({ className = "" }) => {
  const { t } = useTranslation();
  const {sLogin} = useStore();
  const [submitting, setSubmitting] = useState(false);

  const onSubmit = useCallback((event:FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (submitting) { debugger; return; }

    setSubmitting(true);

    sLogin.doLogin().then((err) => {
      setSubmitting(false);
      if (err) {
        if (!err.error) return Modal.error({ content: t("app.login.error.invalid") });

        switch (err.error.errorCode) {
          case ErrorCode.EmailNotConfirm:
            return Modal.error({
              title: t('app.login.error.emailNotConfirm'),
              content: t('app.login.error.emailNotConfirm.content'),
              okText: t('app.login.error.emailNotConfirm.okText'),
              onOk: () => {
                sLogin.requestConfirmEmail().then(err => {
                  if (err) message.error(t('app.login.error.emailNotConfirm.sendConfirmationEmailFail'))
                  else message.success(t('app.login.error.emailNotConfirm.emailSent'))
                });
              }
            });
          case ErrorCode.UserIsLockedOut:
            return Modal.error({
              title: t('app.login.error.userIsLockedOut'),
              content: t('app.login.error.userIsLockedOut.content'),
            });
          default:
            showError(err, t, {
              serializableCase: {
                password: (m:string) => m == t('app.login.password.required') ? t('app.login.password.invalid') : undefined,
              }
            });
        }
      }
    })
  }, []);

  return (
    <div className={styles.main}>
      <div className={classNames(className, styles.login)}>
        <div className="mb-lg capitalize">
          <Title level={3}>{t("app.login.log-in")}</Title>
        </div>
        <Form onSubmit={onSubmit}><fieldset disabled={submitting}>
          <FormItem>
            <Input value={sLogin.email} onChange={sLogin.set_email}
              type="email" required placeholder={t("form.email.placeholder")}
              name="email" autoFocus autoComplete="email username"
              size="large" prefix={useNameIcon}
            />
          </FormItem>
          <FormItem>
            <Input value={sLogin.password} onChange={sLogin.set_password}
              type="password" required placeholder={t("form.password.placeholder")}
              name="password" autoComplete="current-password"
              size="large" prefix={passwordIcon}
            />
          </FormItem>
          <div className="flex justify-between mb-lg">
            <Checkbox checked={sLogin.rememberMe} onChange={sLogin.set_rememberMe}>{t("app.login.remember-me")}</Checkbox>
            {/*<a style={{ float: 'right' }} onClick={goToForgotPassword}>{t("app.login.forgot-password")}</a>*/}
            <Link routeName="forgotPassword">{t("app.login.forgot-password")}</Link>
          </div>
          <FormItem>
            <Button size="large" type="primary"
              className={classNames(styles.submit, className)}
              htmlType="submit" loading={submitting}>{t('app.login.login')}</Button>
          </FormItem>
          <div className="mb-lg text-center">
            <label>{t('app.login.dontHaveAnAccount')}</label> <Link routeName="register" className="capitalize">{t("app.login.signup")}</Link>
          </div>
        </fieldset></Form>
      </div>
    </div>
  );
})


