import React, { FC } from 'react';
import { Button } from 'antd';

import {Result} from "../../../components/Result";
import { Link } from '../../../components/router/Links';

import styles from './RegisterResult.module.less';

const actions = (
  <div className={styles.actions}>
    <a href=""><Button size="large" type="primary">app.register-result.view-mailbox</Button></a>
    <Link routeName="home"><Button size="large">app.register-result.back-home</Button></Link>
  </div>
);

const RegisterResult: FC<any> = ({ location }) => (
  <Result
    className={styles.registerResult}
    type="success"
    title={(
      <div className={styles.title}>
        app.register-result.msg email=abc@example.com
      </div>
    )}
    description="app.register-result.activation-email"
    actions={actions}
    style={{ marginTop: 56 }}
  />
);

export default RegisterResult;
