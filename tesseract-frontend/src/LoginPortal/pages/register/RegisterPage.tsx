import React, { FC } from 'react';

import {Register} from "./Register";
import {UserLayout} from "../../../LoginPortal/layouts/UserLayout";

export const RegisterPage: FC<{}> = (({ }) => {
  return (
    <UserLayout>
        <Register />
    </UserLayout>
  );
});
