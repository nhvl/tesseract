import React, { useState, FC, FormEvent, useCallback, ReactElement, ReactNode } from 'react';
import { observer } from 'mobx-react';
import { useTranslation, Trans } from 'react-i18next';

import { Form, Button, Select, Row, Col, Popover, Progress, Input as AntdInput, Typography, Divider, Descriptions, } from 'antd';
const { Title } = Typography;
const InputGroup = AntdInput.Group;
const FormItem = Form.Item;
const { Option } = Select;
import { ProgressProps } from 'antd/lib/progress';
type ProgressStatuses = ProgressProps["status"];

import { useStore } from '../../stores';
import { RegisterError } from './RegisterStore';

import {Input} from "../../../components/inputs/antd/Input";
import { Link } from '../../../components/router/Links';

import styles from './Register.module.less';
import { IErrorData } from '../../../services/api/AppError';
import { showError } from '../../../services/api/ShowError';
import { Modal } from '../../../components/Modal/ModalAntd';

const passwordStatusMap = {
  ok  : (<div className={styles.success}><Trans>validation.password.strength.strong</Trans></div>),
  pass: (<div className={styles.warning}><Trans>validation.password.strength.medium</Trans></div>),
  poor: (<div className={styles.error  }><Trans>validation.password.strength.short</Trans></div>),
};

const passwordProgressMap: {[key:string]:ProgressStatuses} = {
  ok  : 'success',
  pass: 'normal',
  poor: 'exception',
};

function getPasswordStatus(password: string) {
  if (password && password.length > 9) return 'ok';
  if (password && password.length > 5) return 'pass';
  return 'poor';
}

export const Register: FC<{}> = observer(({ }) => {
  const {t} = useTranslation();
  const {sRegister, routerStore}= useStore();

  const [submitting, setSubmitting] = useState(false);

  const [confirmDirty, setConfirmDirty] = useState(false);
  const [visible, setVisible] = useState(false);
  const [help, setHelp] = useState("");



  const handleSubmit = useCallback(async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setSubmitting(true);
    const err = await sRegister.doRegister();
    setSubmitting(false);
    if (err) {
      showError(err, t, {
        tOptions: { email: sRegister.email },
        identityCase: {
          DuplicateUserName: (_, identiyErrors) => identiyErrors.some(({code}) => code == "DuplicateEmail") ? "" : undefined,
        }
      });
      return;
    }

    Modal.success({
      title: t('app.register-result.register-successful'),
      content: (<p>{t('app.register-result.emailHasSent')}<br/>{t('app.register-result.checkAndConfirmEmail')}</p>),
      okText: t('app.register-result.goToLogin'),
      onOk() { routerStore.goTo("login") },
      cancelButtonProps: { hidden: true },
    });
  }, [setSubmitting]);

  const handleConfirmBlur = useCallback((e: FormEvent<HTMLInputElement>) => {
    setConfirmDirty(confirmDirty || !!e.currentTarget.value);
  }, [setConfirmDirty]);

  const checkPassword = useCallback(() => {
    setHelp(!sRegister.password ? t("validation.password.required") : "")
    setVisible(!!sRegister.password);
  }, [sRegister.password, setHelp, setVisible]);

  const passwordStatus = getPasswordStatus(sRegister.password);

  return (
    <div className={styles.main}>
      <div className="mb-lg capitalize">
        <Title level={3}>{t("app.register.register")}</Title>
      </div>
      <Form onSubmit={handleSubmit}><fieldset disabled={submitting}>
        <div className="paddingBottom-lg">
          <p className="font-bold">{t("app.register.my-info")}</p>
          <Row gutter={16}>
              <Col xs={24} md={12} lg={12} xl={12}>
                <FormItem>
                  <Input value={sRegister.firstName} onChange={sRegister.set_firstName}
                    required autoFocus autoComplete="given-name"
                    size="large" placeholder={t("form.firstName.placeholder")} />
                </FormItem>
              </Col>
              <Col xs={24} md={12} lg={12} xl={12}>
                <FormItem>
                  <Input value={sRegister.lastName} onChange={sRegister.set_lastName}
                    required autoComplete="family-name"
                    size="large" placeholder={t("form.lastName.placeholder")} />
                </FormItem>
              </Col>
          </Row>
          <FormItem>
            <Input value={sRegister.phoneNumber} onChange={sRegister.set_phoneNumber}
              type="tel"
              autoComplete="tel"
              size="large" placeholder={t("form.phone-number.placeholder")} />
          </FormItem>
          <FormItem>
            <Input value={sRegister.email} onChange={sRegister.set_email}
              type="email" required
              autoComplete="email"
              size="large" placeholder={t("form.email.placeholder")} />
          </FormItem>
          <FormItem help={help}>
            <Popover
              getPopupContainer={node => node!.parentNode as HTMLElement}
              content={
                <div style={{ padding: '4px 0' }}>
                  {passwordStatusMap[passwordStatus]}
                  {!!sRegister.password ? (
                    <div className={styles[`progress-${passwordStatus}`]}>
                      <Progress
                        status={passwordProgressMap[passwordStatus]}
                        className={styles.progress}
                        strokeWidth={6}
                        percent={Math.min(sRegister.password.length * 10, 100)}
                        showInfo={false}
                      />
                    </div>
                  ) : null}
                  <div style={{ marginTop: 10 }}>{t("validation.password.strength.msg")}</div>
                </div>
              }
              overlayStyle={{ width: 240 }}
              placement="right"
              visible={visible}
            >
              <Input value={sRegister.password} onChange={sRegister.set_password}
                onBlur={checkPassword}
                type="password" required minLength={8}
                size="large" placeholder={t("form.password.placeholder")} />
            </Popover>
          </FormItem>
          <FormItem help={(!confirmDirty || sRegister.password == sRegister.confirmPassword) ? "" : t("validation.password.twice")}>
            <Input value={sRegister.confirmPassword} onChange={sRegister.set_confirmPassword}
              onBlur={handleConfirmBlur}
              size="large" type="password" required
              placeholder={t("form.confirm-password.placeholder")} />
          </FormItem>
        </div>

        <div>
          <p className="font-bold">{t("app.register.my-student")}</p>
          <Row gutter={16}>
              <Col xs={24} md={12} lg={12} xl={12}>
                <FormItem>
                  <Input value={sRegister.studentNumber} onChange={sRegister.set_studentNumber}
                    required
                    size="large" placeholder={t("app.register.studentNumber.placeholder")} />
                </FormItem>
              </Col>
              <Col xs={24} md={12} lg={12} xl={12}>
                <FormItem>
                  <Input value={sRegister.studentPhoneNumber} onChange={sRegister.set_studentPhoneNumber}
                    type="tel" required
                    size="large" placeholder={t("app.register.studentPhoneNumber.placeholder")} />
                </FormItem>
              </Col>
          </Row>
        </div>

        <FormItem>
          <Button size="large" loading={submitting}
            className={styles.submit} type="primary" htmlType="submit">
            {t("app.register.create-account")}
          </Button>
        </FormItem>
        <div className="mb-lg text-center">
          <label>{t("app.register.sign-in")} </label>
          <Link routeName="login" className="capitalize">{t("app.login.log-in")}</Link>
        </div>
      </fieldset></Form>
    </div>
  );

});
