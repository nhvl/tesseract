import { observable, action } from "mobx";

import { Store } from "../../stores/Store";

import { aFetch } from "../../../services/api/fetch";

export class RegisterStore {
    @observable email             : string = "";
    @observable phoneNumber       : string = "";
    @observable firstName         : string = "";
    @observable lastName          : string = "";
    @observable password          : string = "";
    @observable confirmPassword   : string = "";
    @observable studentNumber     : string = "";
    @observable studentPhoneNumber: string = "";

    constructor(private store: Store) {
    }

    @action set_email              = (v: string ) => { this.email              = v; }
    @action set_phoneNumber        = (v: string ) => { this.phoneNumber        = v; }
    @action set_firstName          = (v: string ) => { this.firstName          = v; }
    @action set_lastName           = (v: string ) => { this.lastName           = v; }
    @action set_password           = (v: string ) => { this.password           = v; }
    @action set_confirmPassword    = (v: string ) => { this.confirmPassword    = v; }
    @action set_studentNumber      = (v: string ) => { this.studentNumber      = v; }
    @action set_studentPhoneNumber = (v: string ) => { this.studentPhoneNumber = v; }

    @action async doRegister() {
        const [err,] = await aFetch<boolean, any>("POST", "/Account/RegisterParent", {
            firstName         : this.firstName,
            lastName          : this.lastName,
            email             : this.email,
            phoneNumber       : this.phoneNumber,
            password          : this.password,
            studentNumber     : this.studentNumber,
            studentPhoneNumber: this.studentPhoneNumber,
            language          : this.store.currentLocale,
        });

        return err;
    }
}
