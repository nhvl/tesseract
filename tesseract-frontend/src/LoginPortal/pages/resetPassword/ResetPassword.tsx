import React, { FC, useCallback, useState, FormEvent, useRef } from 'react';
import { observer } from "mobx-react";
import { Icon, Button, Form, Typography, } from 'antd';
const { Title } = Typography;
import { ResetPasswordStore} from '../../stores/ResetPasswordStore';
import styles from '../login/Login.module.less';
import { Input, Checkbox } from '../../../components/inputs/antd/Input';
import { useTranslation } from 'react-i18next';
import { Link } from '../../../components/router/Links';
import { RouterState } from "mobx-state-router";
const FormItem = Form.Item;
const useNameIcon = (<Icon type="user" className={styles.prefixIcon} />);
const passwordIcon = (<Icon type="lock" className={styles.prefixIcon} />);

import classNames from 'classnames';
import { SSL_OP_EPHEMERAL_RSA } from 'constants';
import { Modal } from '../../../components/Modal/ModalAntd';

export const ResetPassword: FC<{ className?: string }> = observer(({ className = "" }) => {
    const [sResetPassword] = useState(() => new ResetPasswordStore());
    const { t } = useTranslation();
    const [submitting, setSubmitting] = useState(false);

    const onSubmit = useCallback((event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (submitting) { debugger; return; }

        if(sResetPassword.password != sResetPassword.confirmPassword){
          Modal.error({ content: t("app.resetpassword.error.passwordNotMatch") });
          return;
        }

        setSubmitting(true);

        var result = sResetPassword.requestResetPassword().then(([err,data]) => {
          setSubmitting(false);
          if (err) {
            Modal.error({ content: t("app.resetpassword.error.invalid") });
          }
          else{
            if(data.isSuccess == true){
              Modal.success({content:t("app.resetpassword.success")});

              setTimeout(() => {
                location.href = "/login";
              }, 800);

            }
            else{
              if(data.result == "InvalidToken"){
                Modal.error({ content: t("app.resetpassword.error.invalidtoken") });
              }
              else if(data.result == "PasswordTooShort"){
                Modal.error({ content: t("app.resetpassword.error.PasswordTooShort") });
              }
              else if(data.result == "PasswordRequiresNonAlphanumeric"){
                Modal.error({ content: t("app.resetpassword.error.PasswordRequiresNonAlphanumeric") });
              }
              else if(data.result == "PasswordRequiresUpper"){
                Modal.error({ content: t("app.resetpassword.error.PasswordRequiresUpper") });
              }
              else{
                Modal.error({ content: data.result });
              }
            }

          }
        });
      }, []);

    return(
        <div className={styles.main}>
        <div className={classNames(className, styles.login)}>
          <div className="mb-lg paddingBottom-sm text-center">
            <Title level={3}>{t("app.login.reset")}</Title>
          </div>
            <Form onSubmit={onSubmit}>
              <fieldset disabled={submitting}>
                <FormItem>
                    <Input value={sResetPassword.email} onChange={sResetPassword.set_email}
                    type="email" required placeholder={t("form.email.placeholder")}
                    name="email" autoFocus autoComplete="email username"
                    size="large" prefix={useNameIcon}
                    />
                </FormItem>
                <FormItem>
                  <Input value={sResetPassword.password} onChange={sResetPassword.set_password}
                    type="password" required placeholder={t("form.password.placeholder")}
                    name="password" autoComplete="current-password"
                    size="large" prefix={passwordIcon}
                  />
                </FormItem>
                <FormItem>
                  <Input value={sResetPassword.confirmPassword} onChange={sResetPassword.set_confirmPassword}
                    type="password" required placeholder={t("form.confirm-password.placeholder")}
                    name="password" autoComplete="current-password"
                    size="large" prefix={passwordIcon}
                  />
                </FormItem>
                <FormItem>
                  <Button size="large" type="primary"
                    className={classNames(styles.submit, className)}
                    htmlType="submit" loading={submitting}>{t('app.forgotpassword.resetpassword')}</Button>
                </FormItem>
                <div className="mb-lg"> {t('app.forgotpassword.or')} <Link routeName="login">{t("app.login.log-in")}</Link></div>
              </fieldset>
            </Form>
        </div>
        </div>

    );
})
