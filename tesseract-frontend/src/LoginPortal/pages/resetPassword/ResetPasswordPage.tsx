import React, { FC, useCallback, useState, FormEvent, useRef } from 'react';
import { observer } from "mobx-react";
import { useTranslation } from 'react-i18next';
import { UserLayout } from '../../layouts/UserLayout';
import { ResetPassword} from './ResetPassword';

import classNames from 'classnames';

export const ResetPasswordPage: FC<{ className?: string }> = observer(({ className = "" }) => {
    return(
        <UserLayout>
            <ResetPassword/>
        </UserLayout>
    );
})
