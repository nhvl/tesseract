import { createContext, useContext } from "react";
import { observable, action, computed } from "mobx";
import { uniqBy } from "lodash-es";

import { Faculty } from "../models/Faculty";

import { BaseStore } from "./BaseStore";

export class FacultyStore {
    constructor(private store:BaseStore) {
    }

    @observable.shallow items:Faculty[] = [];
    @computed get mId2Item() { return observable.map(this.items.map(c => [c.facultyId, c])) }

    @action storeItems(xs:Faculty[]) {
        this.items = uniqBy(xs.concat(this.items), c => c.facultyId);
    }

    static context = createContext<FacultyStore>(undefined!);
    static useContext() {
        return useContext(this.context);
    }
}
