import { createContext, useContext } from "react";
import { observable, action, computed } from "mobx";
import { groupBy, map, uniqBy } from "lodash-es";

import { DbIdentity } from "../models/types";
import { Student } from "../models/Student";

import { BaseStore } from "./BaseStore";

export class StudentStore {
    constructor(private store:BaseStore) {
    }

    @observable.shallow items:Student[] = [];
    @computed get mId2Item() { return observable.map(this.items.map(c => [c.studentId, c])) }

    @action storeItems(xs:Student[]) {
        this.items = uniqBy(xs.concat(this.items), c => c.studentId);
    }


    static context = createContext<StudentStore>(undefined!);
    static useContext() {
        return useContext(this.context);
    }
}
