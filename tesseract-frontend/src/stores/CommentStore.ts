import { createContext, useContext } from "react";
import { observable, action, computed, runInAction } from "mobx";
import { groupBy, map, uniqBy, maxBy } from "lodash-es";

import { DbIdentity } from "../models/types";
import { Comment } from "../models/Comment";

import { BaseStore } from "./BaseStore";

export class CommentStore {
    constructor(private store: BaseStore) {
    }

    @observable.shallow items:Comment[] = [];
    @computed get mCommentId2Comment() { return observable.map(this.items.map(c => [c.commentId, c])) }
    @computed get mThreadId2Comments() {
        return observable.map(
            map(groupBy(this.items, c => c.threadId), xs => [xs[0].threadId, xs])
        );
    }
    @computed get mThreadId2TopComments() {
        return observable.map(
            map(groupBy(this.items.filter(c => c.replyTo == null), c => c.threadId), xs => [xs[0].threadId, xs])
        );
    }
    @computed get mParent2Comments() {
        return observable.map(
            map(groupBy(this.items.filter(c => c.replyTo != null && 0 < c.replyTo), c => c.replyTo), xs => [xs[0].replyTo, xs])
        );
    }
    @computed get mThreadId2LastComment() {
        return observable.map(
            map(groupBy(this.items, c => c.threadId), xs => [xs[0].threadId, maxBy(xs, x => x.dateCreated || 0)])
        );
    }
    @action storeItems(comments:Comment[]) {
        this.items = uniqBy(comments.concat(this.items), c => c.commentId).sort(Comment.sorter.dateCreated);
    }

    @action async fetchCommentsOfThread(threadId:string) {
        const [err, vm] = await Comment.getCommentsOfThread(threadId);
        if (!err) {
            this.storeItems(vm.comments);
        }
        return [err, vm] as const;
    }

    @action async submitComment(data:ISubmitData) {
        const comment = new Comment(data);
        const [err, x] = await comment.save();
        if (!err) runInAction(() => {
            this.items.push(x);
        });
        return [err, x] as const;
    }

    static context = createContext<CommentStore>(undefined!);
    static useContext() {
        return useContext(this.context);
    }
}

interface ISubmitData {
    threadId  : string,
    content   : string,
    createdBy : DbIdentity,
    replyTo  ?: DbIdentity,
}
