import { observable, action, runInAction, computed } from "mobx";
import { IErrorData } from "../services/api/AppError";
import { ClientInfo, GoogleDocItem, GoogleDrive, FilePermission } from "../models/GoogleDrive";
import { AuthorizedStore } from "./AuthorizedStore";

export class AuthenFailed{
    @observable error          ?: AuthenErrorCode;
    @observable details          : string = "";
}

export enum AuthenErrorCode{
    idpiframe_initialization_failed = "idpiframe_initialization_failed",
    popup_closed_by_user = "popup_closed_by_user",
    access_denied = "access_denied",
    immediate_failed = "immediate_failed"
}

export class GoogleDriveStore {
    @observable maxSizeBytes       :number = 25000000;
    @observable.ref clientInfo     ?: ClientInfo;
    @observable.ref accessToken    ?: string = "";
    @observable loading = false;
    @observable googlePickerScope   : string[] = ["https://www.googleapis.com/auth/drive"];
    @observable scope               : string = "profile email openid https://www.googleapis.com/auth/drive";
    @observable offlineCode         : string | undefined = undefined;
    @observable allowSharing        : boolean = false;
    @observable filePermission     ?: FilePermission;

    constructor(private store: AuthorizedStore) {
    }

    @action async init() {
        this.loading = true;
        if(!this.clientInfo){
            let err: IErrorData<{}>|undefined;
            let result: ClientInfo|undefined;

            [err,result] = await GoogleDrive.getClientInfo();
            if (err) return err;
            runInAction(() => {
                this.set_clientInfo(result);
            });
        }
        
        this.loading = false;
        
        await this.initAccessToken();
        return;
    }   

    @action async exchangeAuthorizationCode() {
        if(!this.offlineCode) return;
        this.loading = true;

        let err: IErrorData<{}>|undefined;
        let result: string|undefined;

        [err,result] = await GoogleDrive.exchangeAuthorizationCode(this.offlineCode);
        this.loading = false;
        if (err) return err;
        runInAction(() => {
            this.accessToken = result;
        });
        // await this.initAccessToken();
        return;
    }

    @action async revokeToken() {
        this.loading = true;

        let err: IErrorData<{}>|undefined;
        let result: string|undefined;

        [err,result] = await GoogleDrive.revokeToken();
        this.loading = false;
        if (err) return err;
        runInAction(() => {
            this.accessToken = undefined;
        });
        return;
    }

    @action async shareFile(fileId: string) {
        if(!fileId) return;
        this.loading = true;

        let err: IErrorData<{}>|undefined;
        let result: FilePermission|undefined;

        [err,result] = await GoogleDrive.shareFile(fileId);
        this.loading = false;
        if (err) return err;
        runInAction(() => {
            this.filePermission = result;
        });
        return;
    }

    @action async getFilePermissionInfo(fileId: string) {
        if(!fileId) return;
        if(!this.accessToken) return;
        this.loading = true;

        let err: IErrorData<{}>|undefined;
        let result: FilePermission|undefined;

        [err,result] = await GoogleDrive.getFilePermissionInfo(fileId);
        this.loading = false;
        if (err) return err;
        runInAction(() => {
                this.filePermission = result;
        });
        return;
    }

    @action async initAccessToken() {
        this.loading = true;

        let err: IErrorData<{}>|undefined;
        let result: string|undefined;
        if(!this.accessToken){
            [err,result] = await GoogleDrive.getAccessToken();
            this.loading = false;
            if (err) return err;
            runInAction(() => {
                this.set_accessToken(result);
            });
        }
        
        return;
    }

    @action async checkSupportedFile(v: GoogleDocItem) : Promise<boolean> {
        const supportedType = ["photo","document","audio","video","file"];
        if (supportedType.indexOf(v.type) > -1) {
            if(v.type.includes("file")) {
                if (v.mimeType.endsWith('pdf') 
                    || v.mimeType.endsWith('powerpoint')   
                    || v.mimeType.endsWith('presentation')  
                    || v.mimeType.endsWith('excel')   
                    || v.mimeType.endsWith('sheet')    
                    || v.mimeType.endsWith('msword')   
                    || v.mimeType.endsWith('document')  
                )
                    return true;
            }
            else return true;
        }
        return false;
    }

    @action async checkAllowedSizeBytes(v: GoogleDocItem) : Promise<boolean> {
            if(v.sizeBytes > this.maxSizeBytes)
                return false;
        return  true;
    }

    @computed get maxSizeMB() {
        return Number(this.maxSizeBytes)/1000000;
    }

    @computed get isAuthorized() {
        if(this.accessToken && this.accessToken.length > 0)
        {
            return true;
        }
        return false;;
    }

    @action set_accessToken     = (v: string | undefined)        => { this.accessToken   = v; };
    @action set_clientInfo      = (v: ClientInfo | undefined)    => { this.clientInfo    = v; };
    
}

