import { createContext, useContext } from "react";
import { observable, action, computed } from "mobx";
import { groupBy, map, uniqBy } from "lodash-es";

import { ActivityScore } from "../models/ActivityScore";

import { BaseStore } from "./BaseStore";

export class ActivityScoreStore {
    constructor(protected store:BaseStore) {
    }

    @observable.shallow items:ActivityScore[] = [];
    @computed get mStudentId2Items() { return observable.map(map(groupBy(this.items, c => c.studentId), xs => [xs[0].studentId, xs])); }
    @computed get mActivityId2Items() { return observable.map(map(groupBy(this.items, c => c.activityId), xs => [xs[0].activityId, xs])); }

    @action storeItems(xs:ActivityScore[]) {
        this.items = uniqBy(xs.concat(this.items), c => `${c.activityId} ${c.studentId}`);
    }

    static context = createContext<ActivityScoreStore>(undefined!);
    static useContext() {
        return useContext(this.context);
    }
}
