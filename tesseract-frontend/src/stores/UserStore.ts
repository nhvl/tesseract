import { createContext, useContext } from "react";
import { observable, action, computed } from "mobx";
import { uniqBy } from "lodash-es";

import { User } from "../models/User";

import { BaseStore } from "./BaseStore";

export class UserStore {
    constructor(private store: BaseStore) {
    }

    @observable.shallow items:User[] = [];
    @computed get mId2Item() { return observable.map(this.items.map(u => [u.userId, u])) }

    @action storeItems(xs: User[]) {
        this.items = uniqBy(xs.concat(this.items), u => u.userId);
    }

    static context = createContext<UserStore>(undefined!);
    static useContext() {
        return useContext(this.context);
    }
}
