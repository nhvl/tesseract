import { createContext, useContext } from "react";
import { observable, action, computed } from "mobx";
import { groupBy, map, uniqBy } from "lodash-es";

import { Class } from "../models/Class";

import { BaseStore } from "./BaseStore";

export class ClassStore {
    constructor(private store:BaseStore) {
    }

    @observable.shallow items:Class[] = [];

    @computed get mId2Item() { return observable.map(this.items.map(c => [c.classId, c])) }
    @computed get mSchoolId2Items() {
        return observable.map(
            map(groupBy(this.items, c => c.schoolId), xs => [xs[0].schoolId, xs])
        );
    }

    @action storeItems(xs:Class[]) {
        this.items = uniqBy(xs.concat(this.items), c => c.classId);
    }

    static context = createContext<ClassStore>(undefined!);
    static useContext() {
        return useContext(this.context);
    }
}
