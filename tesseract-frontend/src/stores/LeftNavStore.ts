import { observable, action } from "mobx";
import { BaseStore } from "./BaseStore";

export class LeftNavStore {
    @observable selectedKey = "home";
    @observable collapsed = false;
    @observable menuOpenKeys: string[] = ["home", "classes"];
    @observable expandedMenuOpenKeys: string[] = ["home", "classes"];
    @observable nonMenuHeight: number = 281;
    constructor(private store: BaseStore) {
    }

    @action set_selectedKey        = (v: string)           => {this.selectedKey = v;}
    @action toggleLeftNavCollapsed = ()                   => {
        this.collapsed = !this.collapsed;
        this.showMenuOpenKeys();
        setTimeout(()=>{
            window.dispatchEvent(new Event("resize"));
        });
    }
    @action setLeftNavCollapsed    = (v: boolean)         => {
        this.collapsed = v;
        this.showMenuOpenKeys();
    }
    @action set_menuOpenKeys       = (openKeys: string[]) => {
        if(!this.collapsed) {
            this.expandedMenuOpenKeys = openKeys.concat();
        }
        if (this.collapsed)
            this.menuOpenKeys = openKeys.concat();
    }
    showMenuOpenKeys() {
        if(!this.collapsed){
            this.menuOpenKeys = this.expandedMenuOpenKeys.concat();
        } else {
            this.menuOpenKeys = [];
        }
    }
}
