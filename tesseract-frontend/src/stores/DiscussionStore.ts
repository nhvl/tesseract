import { createContext, useContext } from "react";
import { observable, action, computed } from "mobx";
import { groupBy, map, uniqBy } from "lodash-es";

import { Discussion } from "../models/Discussion";
import { Activity } from "../models/Activity";

import { BaseStore } from "./BaseStore";

export class DiscussionStore {
    constructor(private store: BaseStore) {
    }

    @observable.shallow items:Discussion[] = [];
    @computed get mId2Item() { return observable.map(this.items.map(c => [c.discussionId, c])) }
    @computed get mClassId2Items() {
        return observable.map(
            map(groupBy(this.items, c => c.classId), xs => [xs[0].classId, xs])
        );
    }
    @computed get mActivityId2Items() {
        return observable.map(
            map(groupBy(this.items, c => c.activityId), xs => [xs[0].activityId, xs])
        );
    }

    @action storeItems(xs:Discussion[], discussionActivity?:Activity[]) {
        this.items = uniqBy(xs.concat(this.items), c => c.discussionId).sort(Discussion.sorter.dateCreated);

        if(discussionActivity) {
            for(var i = 0; i < this.items.length;i++){
                var discussionActivityId = this.items[i].discussionActivityId;
                if(discussionActivityId){
                    var disAct = discussionActivity.find(a=>a.activityId==discussionActivityId);
                    if(disAct){
                        this.items[i].set_color(disAct.color);
                        this.items[i].set_maxScore(disAct.maxScore);
                        this.items[i].set_weight(disAct.weight);
                    }
                }
            }
        }
    }

    static context = createContext<DiscussionStore>(undefined!);
    static useContext() {
        return useContext(this.context);
    }
}
