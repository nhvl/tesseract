import { createContext, useContext } from "react";
import { observable, action, computed } from "mobx";
import { groupBy, map, uniqBy } from "lodash-es";

import { Activity } from "../models/Activity";

import { BaseStore } from "./BaseStore";

export class ActivityStore {
    constructor(protected store:BaseStore) {
    }

    @observable.shallow items:Activity[] = [];
    @computed get mId2Item() { return observable.map(this.items.map(c => [c.activityId, c])) }
    @computed get mClassId2Items() {
        return observable.map(
            map(groupBy(this.items, c => c.classId), xs => [xs[0].classId, xs])
        );
    }

    @action storeItems(xs:Activity[]) {
        this.items = uniqBy(xs.concat(this.items), c => c.activityId);
    }

    static context = createContext<ActivityStore>(undefined!);
    static useContext() {
        return useContext(this.context);
    }
}
