import { observable, action, runInAction, computed } from "mobx";
import {history} from "../services/history";
import { observeMedia } from "../observers/observeMedia";
import { observeMediaBreakpoint } from "../observers/observeMediaBreakpoint";
import i18n from "../i18n";
import moment from "moment";
import { RouterStore, RouterState, routerStateToUrl, StringMap } from "mobx-state-router";

import { aFetch } from "../services/api/fetch";

import { momentLocaleMap } from "../utils/i18nConfig";

import { LeftNavStore } from "./LeftNavStore";
import { PowerSearchStore } from "./PowerSearchStore";

const notFound = new RouterState("notFound");

export class BaseStore {
    constructor() {
        this.routerStore = new RouterStore(this, [], notFound);
    }

    routerStore         : RouterStore;
    replaceRouterState(state:RouterState): void;
    replaceRouterState(routeName: string, params?: StringMap, queryParams?: {[key: string]: any}): void;
    replaceRouterState(routeName: string|RouterState, params?: StringMap, queryParams?: {[key: string]: any}) {
        const state = (typeof routeName == "string"
            ? new RouterState(routeName, params, queryParams)
            : routeName);
        history.replace(routerStateToUrl(this.routerStore, state));
    }


    @observable mediaBreakpoint:string = observeMediaBreakpoint(action((v:string) => this.mediaBreakpoint = v));
    @computed get isMobile() { return this.mediaBreakpoint == "xs" }


    @observable.shallow locales:string[] = ["en-US"];
    @observable currentLocale:string = this.locales[0];
    async setLocale(v:string) {
        if (!v) return null;

        if (this.currentLocale == v) return null;
        if (!this.locales.includes(v)) console.warn("setLocale", v);

        const pMoment = (async () => {
            const l = momentLocaleMap[v];
            if (!l) { console.warn("momentLocaleMap not found", v); return; }
            if (l != "en") await import(`moment/locale/${l}`);
            moment.locale(l);
        })();

        if (!i18n.hasResourceBundle(v, "translation")) {
            const [err, resource] = await aFetch("GET", `/Localization/${v}`);
            if (err) { return err; }
            i18n.addResourceBundle(v, "translation", resource);
        }
        i18n.changeLanguage(v);
        await pMoment;
        runInAction(() => {
            this.currentLocale = v;
        });
        return null;
    };

    sPowerSearch      = new PowerSearchStore(this);
    private pRefreshLocales: Promise<void>|null = null;
    async refreshLocales() {
        if (this.pRefreshLocales != null) return this.pRefreshLocales;

        this.pRefreshLocales = (async () => {
            const [err, locales] = await aFetch<string[]>("GET", "/Localization");
            this.pRefreshLocales = null;

            if (err) { console.error(err); return; }
            runInAction(() => {
                this.locales = locales;
                if (!locales.includes(this.currentLocale)) this.currentLocale = locales[0];
            });
        })();
    }

    sLeftNav = new LeftNavStore(this);



}
