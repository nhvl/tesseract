import { BaseStore } from "./BaseStore";
import { HubConnectionBuilder, HubConnection, HubConnectionState } from "@aspnet/signalr";

export class NotificationStore {
    constructor(private store: BaseStore) {
    }

    private connection!: HubConnection;

    init(token: string){
        this.connection = (
            new HubConnectionBuilder()
            .withUrl(`/notificationHub`, { accessTokenFactory: () => token })
            .build()
        );

        this.connection.start().then(() => console.log("ws connected"), console.error);
    }

    on(methodName:"ThreadChanged", newMethod:(threadId: string) => void): void;
    on(methodName:"ClassUpdated", newMethod:() => void): void;
    on(methodName:string, newMethod: (...args: any[]) => void) {
        const {connection} = this;
        connection.on(methodName, newMethod);
    }
    off(methodName: string, method?: (...args: any[]) => void) {
        if (method == null) this.connection.off(methodName)
        else this.connection.off(methodName, method);
    }
}

/*
1. connect
    token
2. subscribe
    event
        -> data

event
async stream
    x[]
        await

Protocol
    Polling
    WebSocket


*/
