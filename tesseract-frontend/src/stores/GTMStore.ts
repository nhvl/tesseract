import { observable, action } from "mobx";
import TagManager from "react-gtm-module";
import { aFetch } from "../services/api/fetch";

import { EUserRole, DbIdentity } from "../models/types";

import { AuthorizedStore } from "./AuthorizedStore";

enum E_GTMType {
    FacultyPortal = "gaFacultyPortalEvent",
    StudentPortal = "gaStudentPortalEvent",
    AdminPortal   = "gaAdminPortalEvent",
    ParentPortal  = "gaParentPortalEvent"
}

export class GTMStore {
    // Google Tag Manager handler
    @observable gtmId   : string = "";
    @observable gtmType?: E_GTMType;

    @action set_gtmId   = (v: string)    => { this.gtmId   = v; };
    @action set_gtmType = (v: EUserRole) => {
        switch(v){
            case EUserRole.Admin:
                this.gtmType = E_GTMType.AdminPortal;
                break;
            case EUserRole.Faculty:
                this.gtmType = E_GTMType.FacultyPortal;
                break;
            case EUserRole.Parent:
                this.gtmType = E_GTMType.ParentPortal;
                break;
            case EUserRole.Student:
                this.gtmType = E_GTMType.StudentPortal;
                break;
            default:
                this.gtmType = undefined;
        }
    };
    constructor(private store: AuthorizedStore) {
    }

    //Run when load page and schoolId is set
    @action async init() {
        const [err, gtmId] = await aFetch<string>("GET", `/config/GoogleTagManager`);
        if (!err) {
            this.set_gtmId(gtmId);
            this.run();
        }
    }
    @action async initWithDistrict(districtId:DbIdentity) {
        const [err, gtmId] = await aFetch<string>("GET", `/config/district/${districtId}/GoogleTagManager`);
        if (!err) {
            this.set_gtmId(gtmId);
            this.run();
        }
    }
    @action async initWithSchool(schoolId:DbIdentity) {
        const [err, gtmId] = await aFetch<string>("GET", `/config/school/${schoolId}/GoogleTagManager`);
        if (!err) {
            this.set_gtmId(gtmId);
            this.run();
        }
    }

    //Run when route change
    @action async run() {
        const { currentUser, routerStore } = this.store;
        if (currentUser == null || routerStore == null) return;
        if (!this.gtmId) return;
        this.set_gtmType(currentUser.role);
        if (this.gtmType == undefined || this.gtmId == "") return;

        TagManager.initialize({
            gtmId: this.gtmId,
            events: {
                event    : this.gtmType,
                pagePath : location.pathname,
                pageTitle: routerStore.routerState.routeName,
                userId   : currentUser.userId
            }
        });
    }
}
