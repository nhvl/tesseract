import { observable, action, runInAction } from "mobx";

import { aFetch, setAuthtoken } from "../services/api/fetch";

import { EUserRole } from "../models/types";
import { User } from "../models/User";

import {BaseStore} from "./BaseStore";

import { UserRoleKey } from "../config";
import { createError } from "../services/api/AppError";
import { getJwtToken, clearJwtToken, storeJwtToken, isPersistent } from "../LoginPortal/stores/loginUtil";

export class AuthorizedStore extends BaseStore {
    @observable.ref currentUser ?: User;

    async setLocale (v:string) {
        const err = await super.setLocale(v);
        if (!err && this.currentUser && this.currentUser.language != v) {
            this.currentUser.set_language(v);
            this.currentUser.updateProfile().then((err) => {
                if (err) console.error(err);
            })
        }
        return err;
    };

    async checkLogin() {
        if (this.currentUser != null) return true;

        const authToken = getJwtToken();
        if (!authToken) return false;
        const [err] = await this.setToken(authToken);
        return err == null;
    }

    async setToken(token:string) {
        setAuthtoken(token);
        const [err, data] = await aFetch("GET", "/Account/Me");
        if (err) {
            setAuthtoken("");
            clearJwtToken();
            return [err, data] as const;
        }
        const u = this.currentUser = new User({...data, token});
        // storeJwtToken(token, false);
        if (!u.role) return [
            createError(new Error("No Role is set. Please contact admin."), 403),
            data
        ] as const;
        localStorage.setItem(UserRoleKey, u.role);

        runInAction(() => {
            if (!!data.language && this.locales.includes(data.language)) {
                super.setLocale(data.language);
            } else {
                this.refreshLocales().then(() => {
                    if (!!data.language && this.locales.includes(data.language)) {
                        super.setLocale(data.language);
                    }
                });
            }
        });

        return [err, data] as const;
    }

    async changeRole(role:EUserRole) {
        const [err, token] = await aFetch<string>("POST", "/Account/ChangeRole", role);
        if (!err) {
            storeJwtToken(token, isPersistent());
        }
        return [err, token];
    }

    @action logout = () => {
        this.currentUser = undefined;
        setAuthtoken("");
        clearJwtToken();
        location.assign("/");
    }
}
