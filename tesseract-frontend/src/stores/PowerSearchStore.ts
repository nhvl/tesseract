import { observable, action, runInAction } from "mobx";

import { EUserRole } from "../models/types";
import { PowerSearch, SearchView } from "../models/PowerSearch";

import { BaseStore } from "./BaseStore";

export class PowerSearchStore {
    @observable students    :SearchView[]=[];
    @observable classes     :SearchView[]=[];
    @observable pages       :SearchView[]=[];
    @observable assignments :SearchView[]=[];
    @observable assessments :SearchView[]=[];
    @observable schools     :SearchView[]=[];
    @observable faculties   :SearchView[]=[];
    @observable parents     :SearchView[]=[];

    constructor(private store: BaseStore) {
    }

    async handleSearch(role:EUserRole, q: string) {
        q = q.trim();
        const [err, data] = await PowerSearch.search({role, q, schoolId:this.store.currentSchoolId});
        if (!err) runInAction(() => {
            this.students    = data.students;
            this.classes     = data.classes;
            this.pages       = data.pages;
            this.assessments = data.assessments;
            this.assignments = data.assignments;
            this.parents     = data.parents;
            this.faculties   = data.faculties;
            this.schools     = data.schools;
        })
        return [err, data] as const;
    }
    @action handleSelect(value: any, option:any){
        const { routerStore } = this.store;
        const optionKey:string = option.key;
        if(optionKey.startsWith("Class")) {
            let classPage = "classDetail";
            if(routerStore.routes.find(r => r.name == classPage) != null) {
                routerStore.goTo(classPage, {classId: String(value.replace("Class","")) });
            }
        }
        else if(optionKey.startsWith("Student")) {
            let studentPage = "studentDetail";
            if(routerStore.routes.find(r => r.name == studentPage) != null) {
                routerStore.goTo(studentPage, {studentId: String(value.replace("Student","")) });
            }
        }
        else if(optionKey.startsWith("Page")
                || optionKey.startsWith("Assessment")
                || optionKey.startsWith("Assignment")) {
            let activityPages = ["activitySettings", "activityDetail"];
            for(var i = 0; i< activityPages.length; i++) {
                if(routerStore.routes.find(r => r.name == activityPages[i]) != null) {
                    routerStore.goTo(activityPages[i], {
                        activityId : String(value
                            .replace("Page","")
                            .replace("Assessment","")
                            .replace("Assignment","")) });
                }
            }
        }
    }

}
