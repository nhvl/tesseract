import React, { FC } from "react";
import {render} from "react-dom";
import "./i18n";

import "./theme";

import { getJwtToken, clearJwtToken } from "./LoginPortal/stores/loginUtil";
import { aFetch, setAuthtoken } from "./services/api/fetch";

const rootEle = document.getElementById("root");

export async function goToLogin() {
    setAuthtoken("");
    clearJwtToken();
    const {App} = await import("./LoginPortal/App");
    render(<App />, rootEle);
}

export async function bootstrap() {
    const jwtToken = getJwtToken();

    if (location.pathname == "/login" || location.pathname == "/forgot-passowrd") {
        goToLogin();
        return;
    }
    if (!jwtToken) {
        goToLogin();
        return;
    }

    setAuthtoken(jwtToken);
    const [err, data] = await aFetch<{role:string}>("GET", `/Account/Me`);
    if (err) {
        goToLogin();
        return;
    }

    let mApp: Promise<{App:FC<{}>}>;
    switch(data.role) {
        case "Faculty"    : mApp = import("./FacultyPortal/App") as any; break;
        case "SchoolAdmin":
        case "Admin"      : mApp = import("./AdminPortal/App") as any; break;
        case "Student"    : mApp = import("./StudentPortal/App") as any; break;
        case "Parent"     : mApp = import("./ParentPortal/App") as any; break;
        default: {
            alert(`Role(${data.role}) not support. Please login another account.`);
            goToLogin();
            return;
        }
    }

    const {App} = await mApp;
    render(<App />, rootEle);
}

bootstrap();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
import * as serviceWorker from "./serviceWorker";
serviceWorker.unregister();
