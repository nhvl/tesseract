import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import en from "./locales/en-US";

const resources = {
  en: { translation: en },
  "en-US": { translation: en },
};

if (process.env.NODE_ENV == "development") {
  resources["emoji"] = ({translation: new Proxy({}, {
    get: (target, name) => name in target ? target[name]:
      Math.random().toString(36).substring(7)
      //"😂😭😍"
  })});
  window.i18n = i18n;
}

i18n
  .use(initReactI18next)
  .init({
    resources,
    lng: "en",
    fallbackLng: 'en',
    debug: true,
    keySeparator: false,
    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;
