import React, { ReactNode } from 'react';
import { RouterState, Route, StringMap, TransitionHook } from 'mobx-state-router';

import {mapValues, toPairs} from "lodash-es";

import { Store } from './stores/Store';

export const homeRoute = new RouterState("home");
export const notFound = new RouterState('notFound');

const checkForUserSignedIn: TransitionHook = async (fromState, toState, routerStore) => {
    const store:Store = routerStore.rootStore;
    const isLogin = await store.checkLogin();
    if (!isLogin) {
        location.reload();
        throw new RouterState("404");
    }
};

import { HomePage            } from "./pages/home/HomePage";
import { ClassList           } from "./pages/classes/ClassList";
import { ClassDetailPage     } from "./pages/classes/ClassDetailPage";
import { DiscussionPage      } from "./pages/discussion/DiscussionPage";
import { ActivityDiscussionListPage } from "./pages/discussion/ActivityDiscussionListPage";
import { AccountSettingsPage } from "./pages/Account/Settings/AccountSettingsPage";
import { ActivityDetailPage  } from "./pages/activity/ActivityDetail";
import { ViewActDocPage      } from "./pages/activity/ViewActDocPage";

import { Exception403        } from "./pages/Exception/403";
import { Exception404        } from "./pages/Exception/404";
import { Exception500        } from "./pages/Exception/500";
import { PowerSearchDetail   } from './pages/home/PowerSearchDetail';


export const routeConfig: {[key:string]: {pattern:string, comp:ReactNode, allowAnonymous?:boolean}} = {
    notFound            : ({pattern:"/404"                                      , comp: (<Exception404        />), allowAnonymous:true }),
    403                 : ({pattern:"/403"                                      , comp: (<Exception403        />), allowAnonymous:true }),
    500                 : ({pattern:"/500"                                      , comp: (<Exception500        />), allowAnonymous:true }),
    home                : ({pattern:"/"                                         , comp: (<HomePage            />), }),
    accountSettings     : ({pattern:"/account/settings"                         , comp: (<AccountSettingsPage />), }),
    classes             : ({pattern:"/classes"                                  , comp: (<ClassList           />), }),
    classDetail         : ({pattern:"/classes/:classId"                         , comp: (<ClassDetailPage     />), }),
    classActivities     : ({pattern:"/classes/:classId/activities"              , comp: (<ClassDetailPage     />), }),
    classAssignments    : ({pattern:"/classes/:classId/assignments"             , comp: (<ClassDetailPage     />), }),
    classAssessments    : ({pattern:"/classes/:classId/assessments"             , comp: (<ClassDetailPage     />), }),
    classGrades         : ({pattern:"/classes/:classId/grades"                  , comp: (<ClassDetailPage     />), }),
    discussionList      : ({pattern:"/classes/:classId/discussion"              , comp: (<ClassDetailPage     />), }),
    discussion          : ({pattern:"/classes/:classId/discussion/:discussionId", comp: (<DiscussionPage      />), }),
    activityDetail      : ({pattern:"/activities/:activityId"                   , comp: (<ActivityDetailPage  />), }),
    viewActDoc          : ({pattern:"/activities/:activityId/doc"               , comp: (<ViewActDocPage      />), }),
    activityDiscussion  : ({pattern:"/activities/:activityId/discussion"        , comp: (<ActivityDiscussionListPage/>), }),
    powerSearchDetail   : ({pattern:"/powerSearch"                              , comp: (<PowerSearchDetail   />), }),
};

export const appViewMap = mapValues(routeConfig, c => c.comp);
export const routes = toPairs(routeConfig).map<Route>(([name, c]) => ({
    name, pattern:c.pattern,
    beforeEnter: !!c.allowAnonymous ? undefined :checkForUserSignedIn
}));

function safeFromState(state: RouterState) {
    return state.routeName === "__initial__" ? state : homeRoute
}

function redirect(routeName:string, params?: StringMap, queryParams?: Object) {
    return () => Promise.reject(new RouterState(routeName, params, queryParams))
}
