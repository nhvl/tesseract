import { observable, action, computed, reaction, runInAction } from "mobx";
import { RouterStore, HistoryAdapter,  } from "mobx-state-router";
import { uniqBy } from "lodash-es";

import {history} from "../../services/history";
import { routes, notFound, homeRoute } from "../routes";

import { DbIdentity, DefaultId } from "../../models/types";
import { School } from "../../models/School";
import { StudentUser, IUser } from "../../models/User";
import { Student } from "../../models/Student";
import { Faculty } from "../../models/Faculty";
import { ScoreBadge } from "../../models/ScoreBadge";
import { Comment } from "../../models/Comment";
import { Discussion } from "../../models/Discussion";
import { Activity, ActivityType } from "../../models/Activity";
import { StudentClass, sortClass } from "../models/StudentClass";
import { StudentActivity } from "../models/StudentActivity";

import { GTMStore } from "../../stores/GTMStore";

import { UserStore } from "../../stores/UserStore";
import { DiscussionStore } from "../../stores/DiscussionStore";
import { CommentStore } from "../../stores/CommentStore";
import { AsFacultyClassStore } from "./AsFacultyClassStore";

import { NotificationStore } from "../../stores/NotificationStore";
import { ClassStore } from "../../stores/ClassStore";
import { ActivityStore } from "./ActivityStore";
import { ActivityScoreStore } from "./ActivityScoreStore";

import {AuthorizedStore} from "../../stores/AuthorizedStore";

import { AccountSettingStore } from "../../components/Account/Settings/AccountSettingStore";
import { ActivityDetailStore } from "./ActivityDetailStore";
import { ClassDetailStore } from "./ClassDetailStore";
import { ViewActDocStore } from "./ViewActDocStore";
import { ActivityScoreCommentStore } from "./ActivityScoreCommentStore";
import { ActivityScoreCommentDialogStore } from "./ActivityScoreCommentDialogStore";
import { EditStudentActDocStore } from "./EditStudentActDocStore";
import { ClassDiscussionListStore, ActivityDiscussionListStore } from "./DiscussionListStore";
import { DiscussionThreadStore } from "./DiscussionThreadStore";

const CurrentSchoolKey = "CurrentSchool";

import { updateFavicon } from "../../utils/updateFavicon";
import { GradeRange } from "../../models/GradeRange";
import { GoogleDriveStore } from "../../stores/GoogleDriveStore";


export class Store extends AuthorizedStore {
    @observable currentSchool: School | undefined;
    @action storeSchool(cs: School) { this.currentSchool = cs; }

    @observable.shallow tClass: StudentClass[] = [];
    @computed get mClass() { return observable.map(this.tClass.map<[DbIdentity, StudentClass]>(c => [c.classId, c])) }
    @action storeClasses(cs: StudentClass[]) {
        this.tClass = this.tClass.concat(cs.filter(c => {
            const o = this.mClass.get(c.classId);
            if (o != null) o.extends(c);
            return o == null
        }));
    }

    @observable.shallow tStudent: Student[] = [];
    @computed get mStudent() { return observable.map(this.tStudent.map<[DbIdentity, Student]>(c => [c.studentId, c])) }
    @action storeStudents(cs: Student[]) {
        this.tStudent = this.tStudent.concat(cs.filter(c => {
            const o = this.mStudent.get(c.studentId);
            if (o != null) o.extends(c);
            return o == null;
        }));
    }

    @observable.shallow tFaculty: Faculty[] = [];
    @computed get mFaculty() { return observable.map(this.tFaculty.map<[DbIdentity, Faculty]>(c => [c.facultyId, c])) }
    @action storeFacultys(cs: Faculty[]) {
        this.tFaculty = this.tFaculty.concat(cs.filter(c => {
            const o = this.mFaculty.get(c.facultyId);
            if (o != null) o.extends(c);
            return o == null;
        }));
    }

    @observable.shallow tUser: IUser[] = [];
    @computed get mUser() {
        return observable.map(
            this.tStudent.map<[DbIdentity, IUser]>(c => [c.userId, c]).concat(
            this.tFaculty.map<[DbIdentity, IUser]>(c => [c.userId, c]),
            (!this.currentUser ? [] : [[this.currentUser.userId, this.currentUser]]),
            this.tUser.map<[DbIdentity, IUser]>(c => [c.userId, c])
        ));
    }
    @action storeUser(cs: IUser[]) {
        this.tUser = uniqBy(cs.concat(this.tUser), u => u.userId);
    }

    constructor() {
        super();

        this.routerStore = new RouterStore(this, routes, notFound);
        const historyAdapter = new HistoryAdapter(this.routerStore, history);
        historyAdapter.observeRouterStateChanges();

        reaction(() => this.routerStore.routerState, routerState => {
            switch (routerState.routeName) {
                case "classDetail":
                case "classActivities":
                case "classAssignments":
                case "classAssessments":
                case "discussionList":
                    this.sLeftNav.selectedKey = "class" + routerState.params.classId; break;
                default:this.sLeftNav.selectedKey = routerState.routeName;
            }
            this.sGTagManager.run();
        });

        this.refreshLocales();
    }

    @observable.ref     currentUser ?: StudentUser;
    async setToken(token:string) {
        const [err, data] = await super.setToken(token);
        if (err) return [err, data] as const;
        const u = this.currentUser = new StudentUser({...data, token});
        if (u.role != "Student") {
            location.href = location.origin;
            return [err, data] as const;
        }
        this.sNotification.init(token);

        runInAction(() => {
            const sCurrentSchool = localStorage.getItem(CurrentSchoolKey);
            let currentSchool = !sCurrentSchool ? NaN : Number(sCurrentSchool);
            if (Number.isNaN(currentSchool) || !u.schools.includes(currentSchool)) currentSchool = u.schools[0];
            this.currentSchoolId = currentSchool;
            localStorage.setItem(CurrentSchoolKey, String(currentSchool));
        });

        this.refresh();

        return [err, data] as const;
    }

    @observable currentSchoolId: DbIdentity = -1;
    @action set_currentSchool = (v: DbIdentity) => {
        this.currentSchoolId = v;
        this.refreshSchool(v);
        this.routerStore.goTo(homeRoute);
    };

    @observable currentGradingTerm:DbIdentity = DefaultId;
    @computed get curentGradingTermName() {
        const term = this.currentSchool ?  this.currentSchool.tGradingTerm.find(gt => gt.gradingTermId == this.currentGradingTerm) : undefined;
        return term ? term.name : undefined;
    };
    @action set_curentGradingTerm = (v: DbIdentity) => { this.currentGradingTerm = v; };

    mSchoolGradeRange = observable.map<DbIdentity, GradeRange[]>();
    @computed get currentGradeRanges() {
        const {currentSchoolId} = this;
        if (!this.mSchoolGradeRange.has(currentSchoolId)) {
            GradeRange.getSchoolGradeRange(currentSchoolId).then(([err, rs]) => {
                if (err) { console.error(err); return; }
                this.mSchoolGradeRange.set(currentSchoolId, rs)
            });
        }
        return this.mSchoolGradeRange.get(currentSchoolId);
    }

    @action async refresh() {
        if (this.currentUser == null) { console.error("currentUser is null"); return; }

        if (this.currentSchoolId < 0) this.currentSchoolId = this.currentUser!.schools[0];
        School.getSchoolAsStudent(this.currentUser.studentId, this.currentSchoolId).then(([err, ss]) => {
            if (err) { console.error(err); return }
            this.storeSchool(ss);
            const school = this.currentSchool;
            if (school == null) return;

            if (!school.tGradingTerm.some(t => t.gradingTermId == this.currentGradingTerm)) {
                this.set_curentGradingTerm(school.currentGradingTerm);
            }
            this.refreshFavicon();
        });
        this.refreshClasses(this.currentSchoolId);
        this.sGTagManager.initWithSchool(this.currentSchoolId);
    }
    @computed get schoolLogo(){
        return this.currentSchool ? this.currentSchool.logoUrl : null;
    }
    refreshFavicon(){
        if(this.currentSchool) {
            const icon = this.currentSchool.iconUrl;
            if(!icon) return;
            updateFavicon(icon);
        }
    }
    @action async refreshSchool(schoolId:DbIdentity) {
        this.refreshClasses(schoolId);

        const school = this.currentSchool;
        if (school == null) return new Error("System error.");

        if (!school.tGradingTerm.some(t => t.gradingTermId == this.currentGradingTerm)) {
            this.set_curentGradingTerm(school.currentGradingTerm);
        }
        this.refreshFavicon();
        return null;
    }

    async refreshClasses(schoolId?:DbIdentity) {
        if (schoolId == null) schoolId = this.currentSchoolId;

        if (!this.currentUser || this.currentUser.studentId < 0) { return new Error("401"); }

        this.sAsFacultyClass.fetchItems();

        const [error, data] = await StudentClass.getClasses({studentId: this.currentUser.studentId, schoolId});
        if (error) return error;

        this.storeClasses(data);
        return;
    }

    sUser           = new UserStore          (this);
    sClass          = new ClassStore         (this);
    sDiscussion     = new DiscussionStore    (this);
    sActivity       = new ActivityStore      (this);
    sActivityScore  = new ActivityScoreStore (this);
    sComment        = new CommentStore       (this);
    sAsFacultyClass = new AsFacultyClassStore(this);

    sAccountSetting             = new AccountSettingStore            (this);
    sActivityDetail             = new ActivityDetailStore            (this);
    sClassDetail                = new ClassDetailStore               (this);
    sViewActDoc                 = new ViewActDocStore                (this);
    sEditStudentActDoc          = new EditStudentActDocStore         (this);
    sGoogleDriveStore           = new GoogleDriveStore               (this);
    sGTagManager                = new GTMStore                       (this);
    sActivityScoreComment       = new ActivityScoreCommentStore      (this);
    sActivityScoreCommentDialog = new ActivityScoreCommentDialogStore(this);
    sClassDiscussionList        = new ClassDiscussionListStore       (this);
    sActivityDiscussionList     = new ActivityDiscussionListStore    (this);
    sDiscussionThread           = new DiscussionThreadStore          (this);
    sNotification               = new NotificationStore              (this);

    @computed get leftNavClasses() {
        return (this.tClass
            .filter(c => (!this.currentGradingTerm || c.gradingTerm == this.currentGradingTerm) && (c.schoolId == this.currentSchoolId))
            .sort(sortClass)
        );
    }

    @computed get scoreBadge() {
        if (this._scoreBadge) return this._scoreBadge;
        this.refreshScoreBadge();
        return new ScoreBadge();
    }
    @observable.ref _scoreBadge?: ScoreBadge;
    @action async refreshScoreBadge() {
        if (this.currentUser == null) return;
        const [err, data] = await ScoreBadge.getSchoolScoreBadge(this.currentSchoolId);
        if (!err) runInAction(() => { this._scoreBadge = data });
        return err;
    }

    @action changeOrder = async (startIndex: number, endIndex: number) => {
        const xs = this.leftNavClasses.slice();
        const [x] = xs.splice(startIndex, 1);
        xs.splice(endIndex, 0, x);
        const changedXs = xs.map((x, i) => {
            const index = i + 1;
            if (x.sortIndex == index) return undefined!;
            x.set_sortIndex(index);
            return new StudentClass({ classId:x.classId, sortIndex:index })
        }).filter(Boolean);
        if (changedXs.length < 1) return null;
        const [err, cs] = await StudentClass.batchUpdate(
            {studentId: this.currentUser!.studentId, schoolId:this.currentSchoolId},
            changedXs);

        if(!err) this.storeClasses(cs);

        return err;
    }

    //#region Activity
    async fetchActivities({schoolId, classId, type}:{classId?:DbIdentity, schoolId?:DbIdentity, type?:ActivityType}) {
        if (schoolId == null) schoolId = this.currentSchoolId;
        const [err, vm] = await StudentActivity.fetchActivities({classId, schoolId, studentId:this.currentUser!.studentId, type});
        if (!err) {
            this.sActivity.storeItems(vm.activities);
            this.sActivityScore.storeItems(vm.activitiesScores);
        }
        return [err, vm.activities, vm.activitiesScores, vm] as const;
    }
    async fetchActivity(activityId: DbIdentity) {
        const [err, vm] = await StudentActivity.fetchActivity({schoolId: this.currentSchoolId, activityId, studentId:this.currentUser!.studentId});
        if (!err) {
            this.sActivity.storeItems(vm.activities);
            this.sActivityScore.storeItems(vm.activitiesScores);
            //TODO:
            vm.classStudents;
        }
        return [err, (err ? undefined : vm.activities[0])!, vm] as const;
    }
    //#endregion

    //#region Discussion
    async fetchClassDiscussions(query:{schoolId:DbIdentity, studentId:DbIdentity, classId:DbIdentity}) {
        const [err, vm] = await Discussion.getDiscussionsOfClassAsStudent(query);
        if (!err) {
            this.sDiscussion.storeItems(vm.discussions);
            this.sUser      .storeItems(vm.users);
            this.sComment   .storeItems(vm.comments);
        }
        return [err, vm] as const;
    }
    async fetchActivityDiscussions(query:{schoolId:DbIdentity, studentId:DbIdentity, activityId:DbIdentity}) {
        const [err, vm] = await Discussion.getDiscussionsOfActivityAsStudent(query);
        if (!err) {
            this.sDiscussion.storeItems(vm.discussions);
            this.sUser      .storeItems(vm.users);
            this.sComment   .storeItems(vm.comments);
        }
        return [err, vm] as const;
    }
    async fetchDiscussion(query:{classId:DbIdentity, discussionId:DbIdentity}) {
        const [err, vm] = await Discussion.getDiscussionAsStudent({schoolId:this.currentSchoolId, studentId:this.currentUser!.studentId, ...query});
        if (!err) {
            this.sDiscussion.storeItems(vm.discussions);
            this.sUser.storeItems(vm.users);
        }
        return [err, (err ? undefined : vm.discussions[0])!, vm] as const;
    }
    //#endregion

    //#region Comment
    async fetchCommentsOfThread(threadId:string) {
        const [err, vm] = await this.sComment.fetchCommentsOfThread(threadId);
        if (!err) {
            this.sUser.storeItems(vm.users);
        }
        return [err, vm] as const;
    }
    setSeen(threadId:string) {
        const ds = (this.sComment.mThreadId2Comments.get(threadId) || [])
                .map(c => c.dateCreated!).filter(d => d != null);
        if (ds.length < 1) return;

        const lastSeen = Math.max(...ds) + 1; // `+1` because C# DateTime precision .xxxyyyy round to JS .xxx0000
        Comment.setSeen({threadId:threadId, lastSeen})
    }
    //#endregion
}
