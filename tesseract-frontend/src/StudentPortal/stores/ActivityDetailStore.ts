import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../models/types";
import { StudentClass } from "../models/StudentClass";
import { ActivityScore } from "../../models/ActivityScore";
import { Activity } from "../../models/Activity";
import { Submission, SubmissionAttachment } from "../../models/Submission";

import { UploadFile } from "antd/lib/upload/interface";

import { Store } from "./Store";
import { uploadFile } from "../../services/api/fetch";
import { createError } from "../../services/api/AppError";

export class ActivityDetailStore {
    constructor(private store: Store) {
    }

    @observable         activityId   : DbIdentity = DefaultId;
    @observable.ref     activity    ?: Activity;
    @observable.ref     score       ?: ActivityScore;
    @observable.ref     submission  ?: Submission;
    @observable.shallow attachments  : UploadFile[] = [];
    @observable         isSubmitted   = false;

    @computed get aClass() { return (this.activity != null) ? this.store.mClass.get(this.activity.classId) : undefined }

    @action async init(activityId: DbIdentity) {
        this.exit();

        const {currentUser, currentSchoolId} = this.store; if (currentUser == null) return createError(new Error("Unauthenthicate"), 403);

        if (Number.isNaN(activityId) || activityId < 1) return createError(new Error("Invalid Activity Id."), 400);
        this.activityId = activityId;

        const pA = this.store.fetchActivity(activityId);
        const pS = Submission.getAsStudent({schoolId: currentSchoolId,activityId, studentId:currentUser.studentId});

        const [aErr, a] = await pA;
        const [sErr, submission] = await pS;
        if (this.activityId != activityId) return undefined;
        if (aErr) return aErr;
        if (sErr) return sErr;

        runInAction(() => {
            this.activity = a;
            this.score = this.store.sActivityScore.mActivityId2Item.get(activityId);
            this.isSubmitted = (this.score != null && this.score.isSubmitted);

            this.setSubmission(submission || new Submission({
                studentId: currentUser.studentId,
                activityId,
            }));

            this.store.sLeftNav.selectedKey = `class${this.activity.classId}`;
        });

        if (this.score != null && !!this.score.threadId) {
            const cErr = await this.store.sActivityScoreComment.load2(this.score.threadId);
            if (cErr) return cErr;
        }

        return undefined;
    }

    @action exit = () => {
        this.activityId  = DefaultId;
        this.activity    = undefined;
        this.submission  = undefined;
        this.score       = undefined;
        this.attachments = [];
        this.isSubmitted = false;
    }

    @computed get allowSubmission(){
        if (this.activity == null) return false;
        if (this.activity.dateCutoff == null) return true;
        return (Date.now() < this.activity.dateCutoff);
    }

    @action setSubmission(submission:Submission) {
        this.submission = submission;
        this.attachments = this.submission.attachments.map<UploadFile>(a => ({
            uid      : a.url,
            size     : 0,
            name     : a.fileName,
            url      : a.url,
            fileName : a.fileName,
            status   : "done",
            type     : a.contentType,
        }));
    }

    @action changeAttachments = (file:UploadFile) => {
        if (file.status == "removed") this.attachments = this.attachments.filter(a => a.uid != file.uid);
        else this.attachments = this.attachments.concat([file]);
    }

    @action removeAttachment = (file: UploadFile) => {
        this.attachments = this.attachments.filter(a => a.uid != file.uid);
    }

    @computed get isReSubmitted() {
        return this.isSubmitted
                && this.submission
                && this.submission.dateCreated < this.submission.dateUpdated
    }

    @action submit = async (isSubmitted:boolean) => {
        if (!this.submission) return;

        const errs = await this.saveAttachements(this.submission);
        if (errs.length > 0) return errs[0];

        const [err, s] = await this.submission.update(isSubmitted, this.store.currentSchoolId);

        if (!err) {
            runInAction(() => {
                this.setSubmission(s);
                if (isSubmitted && this.score) this.score.isSubmitted = isSubmitted;
            });
            if ((this.isSubmitted || this.isReSubmitted) && this.store.currentUser != null) {
                this.generateResubmitComment();
            }

            this.isSubmitted = isSubmitted;
        }
        return err;
    }

    private async saveAttachements(submission: Submission){
        const errs = (await Promise.all(this.attachments.map(async (f, i) => {
            const file = f instanceof File ? f : undefined;
            if (file == null) return;
            const [err, url] = await uploadFile("POST", "/Image/UploadFile", file);
            if (!err) this.attachments[i] = ({
                uid     : url,
                size    : f.size,
                name    : f.name,
                url     : url,
                thumbUrl: url,
                fileName: f.fileName,
                status  : "done",
                type    : f.type,
            });
            return err;
        }))).filter(Boolean);
        if (this.submission != submission) return [];

        if (errs.length < 1) runInAction(() => {
            submission.attachments = this.attachments.map(f => new SubmissionAttachment({
                contentType: f.type,
                url        : f.url,
                fileName   : f.name || f.fileName,
            }));
        });

        return errs;
    }

    private generateResubmitComment() {
        if (this.store.currentUser == null) return;
        if (this.score == null || !this.score.threadId) return;
        return this.store.sComment.submitComment({
            threadId : this.score.threadId,
            content  : `Re-submitted at ${new Date().toLocaleString()}. (Autogenerated message)`,
            createdBy: this.store.currentUser!.userId,
        });
    }
}
