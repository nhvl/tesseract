import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../models/types";
import { ActDoc, EActItemType, PollQuizItem } from "../../models/ActDoc";

import { Store } from "./Store";

import { createError } from "../../services/api/AppError";
import { StudentActivity } from "../models/StudentActivity";
import { flatPages, ActivityType } from "../../models/Activity";

export class ViewActDocStore {
    constructor(private store: Store) {
    }

    @observable activityId:DbIdentity = DefaultId;
    @computed get activity() { return this.store.sActivity.mId2Item.get(this.activityId) }
    @computed get aClass() { return this.activity ? this.store.mClass.get(this.activity.classId) : undefined }
    @observable.shallow pages: StudentActivity[] = [];
    @computed get nextPage():StudentActivity|undefined {
        const i = this.pages.findIndex(p => p.activityId == this.activityId);
        return this.pages[i+1]
    }
    @computed get prevPage():StudentActivity|undefined {
        const i = this.pages.findIndex(p => p.activityId == this.activityId);
        return this.pages[i-1]
    }

    @observable.ref doc?:ActDoc;

    @action async init(activityId:DbIdentity) {
        this.activityId = activityId;

        const {currentUser, currentSchoolId} = this.store;
        if (currentUser == null) return createError(new Error("401"), 401);

        const pA = this.store.fetchActivity(activityId);
        const pD = ActDoc.fetchAsStudent({schoolId: currentSchoolId, activityId, studentId:currentUser.studentId});

        const [aErr, a] = await pA;
        if (!aErr) this.fetchPages(a);

        const [err, doc] = await pD;

        if (this.activityId != activityId) return undefined;

        if (!err) {
            (doc.items.filter(item => item.type == EActItemType.PollQuiz)
                .map(async item => {
                    const [pollErr, vote] = await PollQuizItem.fetchAsStudent(currentSchoolId, activityId, currentUser.studentId, item.id)
                    if (pollErr != null) item.set_pollValue(vote);
                }));
        }

        if (!aErr && !err) {
            doc.items.map(i => ((i.type == EActItemType.Discussion && i.discussionId != null && 0 < i.discussionId && i.discussionId) || undefined)!).filter(Boolean)
                .forEach(discussionId => this.store.fetchDiscussion({classId:a.classId, discussionId}));
        }

        return runInAction(() => {
            if (!aErr) {
                this.store.sLeftNav.selectedKey = `class${a.classId}`;
                if (this.aClass) {
                    this.store.set_curentGradingTerm(this.aClass.gradingTerm);
                }
            }

            if (!err) {
                this.doc = doc;
            } else if (err.status == 404) {
                this.doc = new ActDoc({
                    activityId,
                    title  : a.title,
                    summary: a.description,
                });
            } else return err;

            return aErr;
        });
    }

    async fetchPages(a:StudentActivity) {
        if (a.type != ActivityType.Activity && a.type != 0) return;

        const xs = this.store.sActivity.mClassId2Items.get(a.classId);
        if (xs != null) this.pages = flatPages(xs);

        const [err, ps] = await this.store.fetchActivities({classId:a.classId});
        if (!err) runInAction(() => {
            this.pages = flatPages(ps);
        });
    }

    @computed get isPollingTime() {
        if (this.activity == null) return false;
        if (this.activity.dateCutoff != null) return Date.now() <= this.activity.dateCutoff;
        if (this.activity.dateDue != null) return Date.now() <= this.activity.dateDue;
        return true;
    }
}
