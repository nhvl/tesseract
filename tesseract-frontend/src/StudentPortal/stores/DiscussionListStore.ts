import { observable, action, computed, runInAction } from "mobx";

import {DefaultId, DbIdentity} from "../../models/types";

import { Class } from "../../models/Class";
import { Discussion } from "../../models/Discussion";

import { DiscussionStore } from "../../stores/DiscussionStore";
import { ActivityStore } from "./ActivityStore";
import { Store } from "./Store";

export class DiscussionListStore {
    constructor(protected store:Store) {

    }

    @computed get displayItems():Discussion[] {
        return []
    }

    @observable.ref threadUnread = observable.map<string, number>();

}

export class ClassDiscussionListStore extends DiscussionListStore {
    @observable         classId  : DbIdentity = DefaultId;

    @computed get displayItems() {
        return (this.classId < 0 ? [] :
            this.store.sDiscussion.mClassId2Items.get(this.classId)) || []
    }

    @action async init({classId}: {classId:DbIdentity}) {
        this.classId = classId;
        const [err, vm] = await this.store.fetchClassDiscussions({schoolId: this.store.currentSchoolId, studentId: this.store.currentUser!.studentId, classId:classId});
        if (!err) runInAction(() => {
            this.threadUnread = observable.map(vm.threadUnreads.map(x => [x.threadId, x.unread]));
        });
        return err;
    }
}

export class ActivityDiscussionListStore extends DiscussionListStore {
    @observable         activityId: DbIdentity = DefaultId;

    @computed get activity() {
        return this.activityId < 1 ? undefined : this.store.sActivity.mId2Item.get(this.activityId)
    }
    @observable.ref aClass?:Class;

    @computed get displayItems() {
        return (this.activityId < 0 ? [] :
            this.store.sDiscussion.mActivityId2Items.get(this.activityId)) || []
    }

    @action async init({activityId}: {activityId:DbIdentity}) {
        this.activityId = activityId;

        const pA = this.store.fetchActivity(activityId);
        const pD = this.store.fetchActivityDiscussions({schoolId: this.store.currentSchoolId, studentId: this.store.currentUser!.studentId, activityId});

        const [aErr, a, aVm] = await pA;
        const [dErr, vm] = await pD;

        if (!aErr) runInAction(() => {
            this.aClass = aVm.classes[0];
        });

        if (!dErr) runInAction(() => {
            this.threadUnread = observable.map(vm.threadUnreads.map(x => [x.threadId, x.unread]));
        });

        return aErr || dErr;
    }
}
