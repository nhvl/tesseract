import { createContext, useContext } from "react";
import { observable, action, computed } from "mobx";
import { uniqBy } from "lodash-es";

import { ActivityScore } from "../../models/ActivityScore";

import { Store } from "./Store";

export class ActivityScoreStore {
    constructor(protected store:Store) {
    }

    @observable.shallow items:ActivityScore[] = [];
    @computed get mActivityId2Item() { return observable.map(this.items.map(score => [score.activityId, score])); }

    @action storeItems(xs:ActivityScore[]) {
        this.items = uniqBy(xs.concat(this.items), c => c.activityId);
    }

    static context = createContext<ActivityScoreStore>(undefined!);
    static useContext() {
        return useContext(this.context);
    }
}
