import { action, observable, computed } from "mobx";

import { AsFacultyClass, sortClass } from "../models/AsFacultyClass";

import { Store } from "./Store";
import { map, groupBy, uniqBy } from "lodash-es";

export class AsFacultyClassStore {
    constructor(protected store:Store) {
    }

    @observable.shallow items:AsFacultyClass[] = [];
    @computed get mId2Item() { return observable.map(this.items.map(c => [c.classId, c])) }

    @computed get currentGradingTermClasses(){
        return (this.items
            .filter(c =>
                (!this.store.currentGradingTerm || c.gradingTerm == this.store.currentGradingTerm) &&
                (c.schoolId == this.store.currentSchoolId))
            .sort(sortClass)
        );
    }

    @action storeItems(xs:AsFacultyClass[]) {
        this.items = uniqBy(xs.concat(this.items), c => c.classId);
    }


    async fetchItems() {
        const [err, cs] = await AsFacultyClass.fetchItems({
            studentId     : this.store.currentUser!.studentId,
            schoolId      : this.store.currentSchoolId,
            gradingTermId : this.store.currentGradingTerm,
        });
        if (!err) { this.storeItems(cs) }
        return [err, cs] as const;
    }
}
