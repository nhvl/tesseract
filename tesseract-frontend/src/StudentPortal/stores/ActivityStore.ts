import { createContext, useContext } from "react";
import { action, observable, computed } from "mobx";

import { StudentActivity } from "../models/StudentActivity";

import { Store } from "./Store";
import { map, groupBy, uniqBy } from "lodash-es";

export class ActivityStore {
    constructor(protected store:Store) {
    }

    @observable.shallow items:StudentActivity[] = [];
    @computed get mId2Item() { return observable.map(this.items.map(c => [c.activityId, c])) }
    @computed get mClassId2Items() {
        return observable.map(
            map(groupBy(this.items, c => c.classId), xs => [xs[0].classId, xs])
        );
    }

    @action storeItems(xs:StudentActivity[]) {
        this.items = uniqBy(xs.concat(this.items), c => c.activityId);
    }

    static context = createContext<ActivityStore>(undefined!);
    static useContext() {
        return useContext(this.context);
    }
}
