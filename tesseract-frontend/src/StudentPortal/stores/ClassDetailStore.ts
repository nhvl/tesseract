import { observable, action, runInAction, computed } from "mobx";
import { map, groupBy } from "lodash-es";

import { DbIdentity, DefaultId } from "../../models/types";
import { Activity, ActivityType } from "../../models/Activity";

import { Store } from "./Store";
import { StudentActivity } from "../models/StudentActivity";

export class ClassDetailStore {
    constructor(private store: Store) {
    }

    @observable classId: DbIdentity = DefaultId;
    @computed get aClass() { return this.classId < 1 ? undefined : this.store.mClass.get(this.classId) }

    @action async init(classId:DbIdentity) {
        this.classId = classId;
        this.activities = [];
        this.activityCountDiscussion = {};

        const pA = this.store.fetchActivities({classId});

        const [err, activities, _, vm] = await pA;
        if (this.classId != classId) return;
        if (!err) runInAction(() => {
            this.activities = activities.sort(Activity.sorter.dateDue);
            this.activityCountDiscussion = vm.activityCountDiscussion;
        });
        return err;
    }

    @observable.ref activityCountDiscussion: {[activityId:number]:number} = {};

    @observable.shallow activities : StudentActivity[] = [];
    @computed get cActivities() {
        return this.activities.filter(a => a.type == ActivityType.Activity || a.type == 0);
    }
    @computed get cScoreActivities() {
        return this.activities.filter(a => a.type == ActivityType.Assignment || a.type == ActivityType.Assessment || a.isGraded);
    }
    @computed get cAssignments() {
        return this.activities.filter(a => a.type == ActivityType.Assignment);
    }
    @computed get cAssessments() {
        return this.activities.filter(a => a.type == ActivityType.Assessment);
    }
    @computed get id2Activity() {
        return observable.map(this.cActivities.map(a => [a.activityId, a]));
    }
    @computed get rootActivities() {
        return this.cActivities.filter(a => a.parentActivityId == null).sort(Activity.sorter.sortIndex);
    }
    @computed get activityId2Children() {
        return observable.map(
            map(
                groupBy(this.cActivities.filter(a => a.parentActivityId != null), a => a.parentActivityId!),
                xs => [xs[0].parentActivityId!, xs.sort(Activity.sorter.sortIndex)]
            )
        );
    }
}
