import { observable, action } from "mobx";

import { StudentActivity } from "../models/StudentActivity";

import { Store } from "./Store";

export class ActivityScoreCommentDialogStore {
    constructor(private store:Store) {
    }

    @observable.ref activity?: StudentActivity;

    private resolve?: ((v:boolean) => void);

    @action open = (activity:StudentActivity) => {
        this.close(false);

        this.activity   = activity;

        if (this.store.currentUser != null)
            this.store.sActivityScoreComment.load2(activity.score!.threadId);

        return new Promise<boolean>(resolve => this.resolve = resolve);
    }
    @action close = (v:boolean) => {
        this.activity = undefined;

        if (this.resolve) {
            this.resolve(v);
            this.resolve = undefined;
        }
    }
}
