import { observable, runInAction, action } from "mobx";

import { Store } from "./Store";

import { DbIdentity } from "../../models/types";
import { SetSeenDuration } from "../../config";
import { CommentSeen } from "../../models/CommentSeen";

export class ActivityScoreCommentStore {
    constructor(private store:Store) {
    }

    @observable.ref activityId?: DbIdentity;
    @observable.ref studentId?: DbIdentity;
    @observable.ref commentSeen?: CommentSeen;
    @observable threadId: string = "";

    @action load = (activityId:DbIdentity, studentId: DbIdentity) => {
        this.reset();

        this.activityId   = activityId;
        this.studentId   = studentId;

        return this.fetchComments();
    }
    @action load2 = (threadId:string) => {
        this.reset();
        this.threadId = threadId;
        return this.fetchComments();
    }
    @action reset = () => {
        this.activityId = undefined;
        this.studentId = undefined;
        this.threadId = "";
    }

    async fetchComments() {
        const {activityId, studentId, threadId} = this;
        // if (activityId == null || studentId == null) return undefined;
        if (!threadId) return;

        const [err, vm] = await this.store.fetchCommentsOfThread(threadId);
        // if (activityId != this.activityId || studentId != this.studentId) return undefined;
        if (threadId != this.threadId) return;

        if (err) return err;
        else runInAction(() => {
            this.commentSeen = vm.commentSeens[0];
            setTimeout(() => {
                // if (activityId != this.activityId || studentId != this.studentId) return;
                if (threadId != this.threadId) return;
                this.store.setSeen(threadId);
            }, SetSeenDuration);
        });

        return undefined;
    }
}
