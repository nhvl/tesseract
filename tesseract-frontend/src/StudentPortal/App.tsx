import React, { Component } from 'react';

import { CommentStore } from '../stores/CommentStore';
import { UserStore } from '../stores/UserStore';
import { StoreContext, store } from './stores';

import { RouterView } from '../components/router/RouterView';

import 'react-quill/dist/quill.snow.css';

import { appViewMap } from "./routes";

import { ActivityScoreCommentDialog } from './pages/ActivityScoreCommentDialog/ActivityScoreCommentDialog';

export class App extends Component {
  render() {
    return (
      <StoreContext.Provider value={store}>
        <CommentStore.context.Provider value={store.sComment}>
          <UserStore.context.Provider value={store.sUser}>
            <>
              <RouterView viewMap={appViewMap} />

              <ActivityScoreCommentDialog />
            </>
          </UserStore.context.Provider>
        </CommentStore.context.Provider>
      </StoreContext.Provider>
    );
  }
}
