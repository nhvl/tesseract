import React, { FC, useMemo } from 'react';
import { observer } from 'mobx-react';

import { Breadcrumb } from 'antd';

import { Link } from '../../components/router/Links';

import { Class } from '../../models/Class';
import { useTranslation } from 'react-i18next';

export const ClassBreadcrumb:FC<{aClass?:Class}> = observer(({aClass, children}) => {
    const {t} = useTranslation();
    return (
        <div className="breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link routeName="classes">{t('app.classes.menu.classes')}</Link>
                </Breadcrumb.Item>
                {aClass && (
                    <Breadcrumb.Item>
                        <Link routeName="classDetail" params={aClass.params}>{aClass.className}</Link>
                    </Breadcrumb.Item>
                )}
                {children}
            </Breadcrumb>
        </div>
    );
});
