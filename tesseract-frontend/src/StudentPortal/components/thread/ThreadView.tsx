import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";

import { CommentSeen } from "../../../models/CommentSeen";

import { useStore } from "../../stores";

import {ThreadView as GeneralThreadView} from "../../../components/thread/ThreadView";

export const ThreadView: FC<{threadId:string, readOnly?:boolean, commentSeen?:CommentSeen}> = observer(({threadId, readOnly, commentSeen}) => {
    const store = useStore();
    const {sNotification, currentUser} = store;

    useEffect(() => {
        if (!threadId) return;
        sNotification.on("ThreadChanged", handler);
        function handler() {
            store.fetchCommentsOfThread(threadId);
        }
        return () => sNotification.off("ThreadChanged", handler);
    }, [threadId]);

    return (
        <GeneralThreadView threadId={threadId} currentUser={currentUser} readOnly={readOnly} commentSeen={commentSeen} />
    );
});
