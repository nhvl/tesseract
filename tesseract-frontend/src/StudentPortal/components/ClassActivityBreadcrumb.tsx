import React, { FC } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Class } from "../../models/Class";
import { ActivityType, Activity } from "../../models/Activity";

import { Breadcrumb, Spin } from "antd";

import { Link } from "../../components/router/Links";
import { ClassActivityLink } from "../../components/ClassActivityLink";
import { ActivityTypeDesc } from "../../components/ActivityTypeDesc";

export const ClassActivityBreadcrumb: FC<{
    activity?:Activity,
    aClass?:Class,
    activityRouteName:string,
}> = observer(({activity, aClass, activityRouteName, children}) => {
    const {t} = useTranslation();
    const classParams = (
        activity ? activity.params : (
        aClass ? aClass.params: (
        undefined
        )));

    return (
        <div className="breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link routeName="classes">{t('app.classes.menu.classes')}</Link>
                </Breadcrumb.Item>
                {classParams && (<Breadcrumb.Item>
                    <Link routeName="classDetail" params={classParams}>
                        {aClass ? aClass.className : <Spin />}
                    </Link>
                </Breadcrumb.Item>)}
                {activity && (
                    <Breadcrumb.Item>
                        <ClassActivityLink classId={activity.classId} type={activity.type}>
                            <ActivityTypeDesc value={activity.type}/>
                        </ClassActivityLink>
                    </Breadcrumb.Item>
                )}
                {activity && (
                    <Breadcrumb.Item>
                        {activity.type != ActivityType.Activity ?
                            (<Link routeName={activityRouteName} params={activity.params}>{activity.title}</Link>)
                            : (activity.title)
                        }
                    </Breadcrumb.Item>
                )}
                {children}
            </Breadcrumb>
        </div>
    );
});
