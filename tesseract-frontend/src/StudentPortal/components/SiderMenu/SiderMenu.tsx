import React, { FC, CSSProperties } from 'react';
import { observer } from 'mobx-react';

import { Layout, Spin, Icon, Tooltip, Avatar, Divider } from 'antd';
const {Sider} = Layout;

import { BaseMenu } from './BaseMenu';
import { Link } from '../../../components/router/Links';

import styles from './index.module.less';

import { useStore } from '../../stores';

import { useTranslation } from 'react-i18next';

import {SideFooter} from "../../../components/SideFooter";

const spinStyle:CSSProperties = { marginLeft: 8, marginRight: 8 };

export const SiderMenu: FC<{collapsed:boolean}> = observer(({collapsed}) => {
    const {t} = useTranslation();
    const store = useStore();
    const {
        sLeftNav,
        currentUser, logout,
        currentSchool,
    } = store;
    const schoolLogo = currentSchool ? currentSchool.logoUrl : "";

    return (
        <Sider trigger={null} collapsible collapsed={collapsed}
            breakpoint="lg"
            width={256}
            className={`${styles.sider} slider ${styles.fixSiderBar}`}>
            <div className="slider-footer">
                <div className="menu">
                    <div className={`${styles.logo} logo`}>
                        <span className={`${styles.trigger} trigger`} onClick={sLeftNav.toggleLeftNavCollapsed}>
                            <Icon type={sLeftNav.collapsed ? 'menu-unfold' : 'menu-fold'} />
                        </span>
                        <Link routeName="home">
                            {schoolLogo
                                ? (<img src={schoolLogo} alt={t('menu.schools.logo')} />)
                                : (<Spin size="large" />)
                            }
                        </Link>
                    </div>
                    <div className={`${styles.infoAccount} infoAccount`}>
                        <Avatar size="large" className={styles.avatar} src={(currentUser && currentUser.avatar)} icon="user" alt={t('menu.account.avatar')} />

                        {!sLeftNav.collapsed && (<>
                            <div className="paddingBottom-xs font-bold">
                                {currentUser ? (
                                    <span>{currentUser.firstName} {currentUser.lastName}</span>
                                ) : (
                                    <Spin size="small" style={spinStyle} />
                                )}
                            </div>
                            <div>{currentSchool != null ? currentSchool.schoolName : null}</div>
                            <ul className={styles.listIcons}>
                                <li>
                                    <Tooltip title={t('menu.account.settings')}>
                                        <Link routeName="accountSettings"><Icon type="setting" className={styles.large} /></Link>
                                    </Tooltip>
                                </li>
                                <li>
                                    <Tooltip title={t('menu.account.notifications')}>
                                        <a><Icon type="bell" className={styles.large} /></a>
                                    </Tooltip>
                                </li>
                                <li>
                                    <Tooltip title={t('menu.account.help')}>
                                        <a href={`${process.env.REACT_APP_API}/swagger/`}
                                            target="_blank" rel="noopener noreferrer" className={styles.action}>
                                            <Icon type="question-circle" className={styles.large} />
                                        </a>
                                    </Tooltip>
                                </li>
                                <li>
                                    <Tooltip title={t('menu.account.logout')}>
                                        <a onClick={logout}>
                                            <Icon type="logout" className={styles.large} />
                                        </a>
                                    </Tooltip>
                                </li>
                            </ul>
                        </>)}
                        <div className={styles.marginLeftRight}>
                            <Divider/>
                        </div>
                    </div>
                    <BaseMenu />
                </div>
                <SideFooter collapsed={sLeftNav.collapsed} />
            </div>
        </Sider>
    )
});

