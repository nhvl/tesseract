import React, { FC, useEffect, CSSProperties, useMemo, useCallback, MouseEvent, useState } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Menu, Icon, message, Spin, Divider } from "antd";

import { useStore } from "../../stores";

import { Link } from "../../../components/router/Links";
import { subMenu } from "../../../components/inputs/antd/subMenu";
import { EUserRole } from "../../../models/types";
import { AsFacultyClass } from "../../models/AsFacultyClass";

const style: CSSProperties = { padding: 0, width: "100%" };

export const BaseMenu: FC<{}> = observer(() => {
    const [scrollMenuStyle, set_ScrollMenuStyle] = useState(undefined as CSSProperties|undefined);
    const {t} = useTranslation();
    const store = useStore();
    const {sLeftNav} = store;
    useEffect(() => {
        if (store.leftNavClasses == null) store.refreshClasses(store.currentSchoolId);
        updateScrollMenu();
    }, []);
    const updateScrollMenu = useCallback(()=>{
        const item: CSSProperties = {height: "100%", maxHeight:(window.innerHeight - sLeftNav.nonMenuHeight), overflowY: "auto" }
        set_ScrollMenuStyle(item);
    },[window.innerHeight]);
    const selectedKeys = useMemo(() => [sLeftNav.selectedKey, `gradingTerm_${store.currentGradingTerm}`], [sLeftNav.selectedKey, store.currentGradingTerm]);
    const openAeries = useCallback(() => message.info(t("menu.openaeries.open")), [t]);

    return (<div style={scrollMenuStyle}>
        <Menu theme="dark" mode="inline" selectedKeys={selectedKeys}
            openKeys={sLeftNav.menuOpenKeys}
            onOpenChange={sLeftNav.set_menuOpenKeys}
            style={style}
            inlineIndent={18}
            className="main-nav"
            >

            <Menu.Item key="gradingTerm">
                <span>
                    {store.sLeftNav.collapsed && (<Icon type="ellipsis" />)}
                    <span>{store.curentGradingTermName}</span>
                </span>
            </Menu.Item>

            <Menu.Item key="home">
                <Link routeName="home"><Icon type="home" /><span>{t("menu.home")}</span></Link>
            </Menu.Item>

            {...subMenu({
                collapsed:sLeftNav.collapsed,
                subMenuClassName: "level-1",
                key:"classes",
                subMenuTitle:(
                    <Link routeName="classes"><Icon type="read" /><span>{t("menu.classes")}</span></Link>
                ),
                menuItems:(
                    store.leftNavClasses.map(c => (
                        <Menu.Item key={"class" + c.classId} title={`${c.period} ${c.className}`}>
                            <Link routeName="classDetail" params={{classId:String(c.classId)}}>
                                {c.period}
                                <Divider type="vertical"/>
                                {c.className}
                            </Link>
                        </Menu.Item>
                    )).concat([(
                        <Menu.Divider/>
                    )]).concat(store.sAsFacultyClass.currentGradingTermClasses.map(c => (
                        <Menu.Item key={"class" + c.classId} title={`${c.period} ${c.className}`}>
                            <AsFacultyClassLink item={c}>
                                {c.period}
                                <Divider type="vertical"/>
                                {c.className}
                            </AsFacultyClassLink>
                        </Menu.Item>
                    )))
                ),
            })}

            <Menu.Item key="aeries">
                <a onClick={openAeries}>
                    <img className="anticon" src="https://ps.orangeusd.org/favicon.ico" alt={t("menu.openaeries")} />
                    <span>{t("menu.openaeries")}</span>
                </a>
            </Menu.Item>
        </Menu>
    </div>)
});

const AsFacultyClassLink:FC<{item:AsFacultyClass}> = observer(({item, children}) => {
    const store = useStore();
    const classLink = `/classes/${item.classId}`;
    const goToClass = useCallback(async (event: MouseEvent<HTMLAnchorElement>) => {
        event.preventDefault();
        const [err] = await store.changeRole(EUserRole.Faculty);
        if (err) console.error(err);
        else location.assign(classLink);
    }, [item]);

    return (
        <a href={classLink} onClick={goToClass}>
            {children}
        </a>
    )
});
