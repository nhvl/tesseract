import React, { FC, useMemo } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { Class } from '../../models/Class';
import { Activity, ActivityType } from '../../models/Activity';

import { Breadcrumb } from 'antd';

import { Link } from '../../components/router/Links';
import { ClassActivityLink } from "../../components/ClassActivityLink";
import { ActivityTypeDesc } from '../../components/ActivityTypeDesc';

export const ActivityBreadcrumb:FC<{aClass?:Class, activity?:Activity}> = observer(({aClass, activity, children}) => {
    const {t} = useTranslation();
    return (
        <div className="breadcrumb">
            <Breadcrumb>
                {aClass && (
                    <Breadcrumb.Item>
                        <Link routeName="classes">{t('app.classes.menu.classes')}</Link>
                    </Breadcrumb.Item>
                )}
                {aClass && (
                    <Breadcrumb.Item>
                        <Link routeName="classDetail" params={aClass.params}>{aClass.className}</Link>
                    </Breadcrumb.Item>
                )}
                {activity && (
                    <Breadcrumb.Item>
                        <ClassActivityLink classId={activity.classId} type={activity.type}>
                            <ActivityTypeDesc value={activity.type}/>
                        </ClassActivityLink>
                    </Breadcrumb.Item>
                )}
                {activity && (<Breadcrumb.Item>
                    {children != null ? (
                        <Link routeName="activityDetail" params={activity.params}>{activity.title}</Link>
                    ) : (
                        activity.title
                    )}
                </Breadcrumb.Item>)}
                {children}
            </Breadcrumb>
        </div>
    );
});
