import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { DbIdentity } from '../../models/types';

import { useStore } from '../stores';

import { Avatar } from 'antd';
import { AvatarProps } from 'antd/lib/avatar';
import { IUser } from '../../models/User';

export interface IUserAvatar extends AvatarProps {
    userId ?:DbIdentity,
    user   ?:IUser,
}

export const UserAvatar: FC<IUserAvatar> = observer(({user, userId, ...props}) => {
    const store = useStore();

    const u = user ? user : store.mUser.get(userId!);

    return (
        <Avatar src={u ? u.avatar : undefined}
            alt={u ? `${u.firstName} ${u.lastName} avatar` : undefined}
            icon="user" {...props} />
    );
});

