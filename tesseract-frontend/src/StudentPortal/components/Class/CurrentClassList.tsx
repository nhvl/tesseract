import React, { FC, useMemo } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { StudentClass } from "../../models/StudentClass";

import { useStore } from "../../stores";

import { Card } from "antd";
import { ColumnProps } from "antd/lib/table";

import { Link } from "../../../components/router/Links";
import {useNumberTrans} from "../../../components/DateTime/Number";
import {DndTable} from "../../../components/DndTable";
import { GradeRange } from "../../../models/GradeRange";

import styles from './CurrentClassList.module.less';

export const CurrentClassList: FC<{}> = observer(({}) => {
    const store = useStore();

    const {t} = useTranslation();

    const columns = useMemo<Array<ColumnProps<StudentClass>>>(() => [
        { key:"period"     , title: t("app.classes.list.period"), dataIndex: "period", },
        { key:"className"  , title: t("app.classes.list.className"), dataIndex: "className", },
        { key:"faculties"  , title: t("app.classes.list.teacher"), render: (_, c) => c.faculties.map(f => f.fullName).join(",")},
        { key:"grade"      , title: t("app.classes.list.grade"), render:(_, c) => (<GradeCell item={c} />) },
        { key:"actions"    , render: (_, c) => (<Link routeName="classDetail" params={{classId:String(c.classId)}}>{t("app.classes.list.detail")}</Link>)},
    ], [t]);

    return (
        <Card title={t("app.classes.list.allCurrentClasses")}>
            <div className="responsiveTable">
                <DndTable dataSource={store.leftNavClasses} columns={columns} rowKey={rowKey}
                    onDragEnd={store.changeOrder}
                    pagination={false} />
            </div>
        </Card>
    );
});

function rowKey(record:StudentClass) { return String(record.classId) }

const GradeCell: FC<{item:StudentClass}> = observer(({item}) => {
    const store = useStore();
    const {t} = useTranslation();
    const {percent} = useNumberTrans();
    const gradeRanges = useMemo(() =>
        (item.gradeRanges == null || item.gradeRanges.length < 1)
            ? store.currentGradeRanges : item.gradeRanges,
        [item.gradeRanges, store.currentGradeRanges]);

    return (item.grade == null ? t("app.students.detail.GradeNA") : (<div className="flex">
        <div className={`${styles.currentGrade} font-medium mr-lg`}>{percent(item.grade)}</div>
        {gradeRanges && (<div> ({GradeRange.retrieveLetterGrade(gradeRanges, item.grade)})</div>)}
    </div>));
})
