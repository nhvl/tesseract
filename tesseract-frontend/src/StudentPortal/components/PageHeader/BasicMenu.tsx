import React, { ReactNode, FC } from 'react';
import { observer } from 'mobx-react';

import { useStore } from '../../stores';

import { Tabs } from 'antd';
const { TabPane } = Tabs;
import { TabsProps } from 'antd/lib/tabs';

import { Link } from '../router/Links';

import styles from './index.module.less';

export const BasicMenu: FC<{tabs:Array<{key:string, tab?:ReactNode}>}> = observer(({tabs}) => {
    const { routerStore } = useStore();
    const {name} = routerStore.getCurrentRoute();
    return (
        <div className={styles.pageHeader}>
            <div className={true ? styles.wide : ''}>
                <Tabs className={styles.tabs} activeKey={name}>
                    {tabs.map(({tab, key}) => (
                        <TabPane tab={tab == null ? (<Link routeName={key}>{key}</Link>) : tab} key={key} />
                    ))}
                </Tabs>
            </div>
        </div>
    )
});


export const BasicMenuControlled: FC<{tabs:Array<{key:string, tab?:ReactNode}>}&TabsProps> = observer(({tabs, ...props}) => {
    return (
        <div className={styles.pageHeader}>
            <div className={true ? styles.wide : ''}>
                <Tabs className={styles.tabs} {...props}>
                    {tabs.map(({tab, key}) => (
                        <TabPane tab={tab == null ? (<Link routeName={key}>{key}</Link>) : tab} key={key} />
                    ))}
                </Tabs>
            </div>
        </div>
    )
});
