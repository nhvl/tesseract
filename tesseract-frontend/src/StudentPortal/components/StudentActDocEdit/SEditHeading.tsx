import React, { FC } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { StudentActItem } from "../../../models/StudentActDoc";

import { Typography } from "antd";
const { Title } = Typography;

import {Input} from "../../../components/inputs/antd/Input";

import { RemoveItem } from "./RemoveItem";

import viewStyles from "../../../components/StudentDocSubmission/StudentDocSubmission.module.less";
import styles from "./StudentActDocEdit.module.less";

export const SEditHeading: FC<{item: StudentActItem}> = observer(({item}) => {
    const { t } = useTranslation();

    return (
        <div className={`${styles.addMaterial} ${styles.studentEditHeading} is-dragging-border mb-lg`}>
            <div className={`${viewStyles.subTitle} ${styles.studentEditHeadingSubTitle}`}>
                <div className={`${styles.studenEditHeader}`}>
                    <div className="flex-1">
                        <Title level={4}><Input value={item.title} onChange={item.set_title} placeholder={t('form.activity.heading.placeholder')}/></Title>
                    </div>
                    <div className="ml-sm">
                        <RemoveItem item={item} placement={'left'}/>
                    </div>
                </div>
            </div>
        </div>
    );
});
