import React, { FC, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { StudentActItem } from "../../../models/StudentActDoc";

import { useStore } from "../../stores";

import { Icon, Popconfirm } from "antd";
import { TooltipPlacement } from "antd/lib/tooltip";

export const RemoveItem: FC<{item: StudentActItem, placement?: TooltipPlacement}> = observer(({item, placement = "top"}) => {
    const { t } = useTranslation();
    const {sEditStudentActDoc} = useStore();
    const onRemove = useCallback(() => {
        sEditStudentActDoc.removeItem(item);
    }, [item]);

    return (
        <Popconfirm
            placement={placement}
            onConfirm={onRemove}
            title={t("app.actDoc.editor.editor.removeConfirm")}
            okText={t("form.yes")}
            cancelText={t("form.no")}
            okType="danger"
        >
            <a title={"Remove section"}><Icon type="delete" className="icon-sm" /></a>
        </Popconfirm>
    );
});

