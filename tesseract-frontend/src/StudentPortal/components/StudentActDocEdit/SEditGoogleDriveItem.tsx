import React, { FC, useState, useCallback, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { StudentActItem } from "../../../models/StudentActDoc";

import { useStore } from "../../stores";

import {  Icon,Typography, message } from "antd";
const { Title } = Typography;

import { Input } from "../../../components/inputs/antd/Input";
import { HtmlInput } from "../../../components/inputs/HtmlInput";

import { RemoveItem } from "./RemoveItem";

import viewStyles from "../../../components/StudentDocSubmission/StudentDocSubmission.module.less";
import styles from "./StudentActDocEdit.module.less";
import { Modal } from "../../../components/Modal/ModalAntd";
import { GoogleDriveInputWrap } from "../../../components/GoogleApis/GoogleDriveInputWrap";

export const SEditGoogleDriveItem: FC<{item: StudentActItem}> = observer(({item}) => {
    const { t } = useTranslation();
    const { sGoogleDriveStore } = useStore();

    // useEffect(() => {
    //     sGoogleDriveStore.init().then(err => {
    //         if(err) Modal.error({content: err.message})
    //     });
    // }, []);
    
    return (
        <div className={`${styles.addMaterial} is-dragging-border mb-lg`}>
            <div className={viewStyles.subTitle}>
                <div className={`${styles.studenEditHeader}`}>
                    <div className="flex-1">
                        <Title level={4}>
                            <Input value={item.title} onChange={item.set_title}
                                placeholder={t("form.activity.subtitle.placeholder")}/>
                        </Title>
                    </div>
                    <div className="ml-sm">
                        <RemoveItem item={item} placement={'left'}/>
                    </div>
                </div>
            </div>
            <GoogleDriveInputWrap gStore={sGoogleDriveStore} item={item}/>
        </div>
    );
});

