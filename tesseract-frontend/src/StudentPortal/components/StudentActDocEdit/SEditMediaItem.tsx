import React, { FC, useState, useCallback, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { MediaTypeEnum } from "../../../models/MediaTypeEnum";

import { StudentActItem } from "../../../models/StudentActDoc";

import { useStore } from "../../stores";

import { Button, Icon, Divider, Typography, Modal, Tooltip } from "antd";
const { Title } = Typography;

import ReactPlayer from "react-player";

import { Input } from "../../../components/inputs/antd/Input";
import { HtmlInput } from "../../../components/inputs/HtmlInput";
import { DumbUpload } from "../../../components/inputs/antd/Upload";
import { ImageInput } from "../../../components/ImageInput";

import { RemoveItem } from "./RemoveItem";

import viewStyles from "../../../components/StudentDocSubmission/StudentDocSubmission.module.less";
import styles from "./StudentActDocEdit.module.less";
import { VideoRecordButton } from "../../../components/inputs/VideoRecordButton";
import { AudioRecordButton } from "../../../components/inputs/AudioRecordButton";

export const SEditMediaItem: FC<{item: StudentActItem}> = observer(({item}) => {
    const { t } = useTranslation();
    const { sEditStudentActDoc} = useStore();
    const [, setEditting] = useState(false);

    const endEdit = useCallback(() => setEditting(false), [setEditting]);
    const onClearMedia = useCallback(() => {
        item.set_link("");
        item.set_mediaFile(undefined);
        item.clearMediaType();
    }, [item]);
    
    const onChangeLink = useCallback((v:string) => {
        item.set_link(v);
        if (!v) {
            if (!item.mediaFile) item.clearMediaType();
        } else {
            item.set_mediaFile(undefined);
            setTimeout(() => {
                if (v == item.link) {
                    sEditStudentActDoc.onBlurMediaLink(item);
                }
            }, 1000)
        }
    }, [item]);

    const onRecorded = useCallback((file:File) => {
        item.set_mediaFile(file);
        // onUploadPoster(thumbnailBlob);
    }, [item]);

    return (
        <div className={`${styles.addMaterial} is-dragging-border mb-lg`}>
            <div className={viewStyles.subTitle}>
                <div className={`${styles.studenEditHeader}`}>
                    <div className="flex-1">
                        <Title level={4}>
                            <Input value={item.title} onChange={item.set_title}
                                placeholder={t("form.activity.subtitle.placeholder")}/>
                        </Title>
                    </div>
                    <div className="ml-sm">
                        <RemoveItem item={item} placement={'left'}/>
                    </div>
                </div>
            </div>
            {(item.mediaType == MediaTypeEnum.VideoFile || item.mediaType == MediaTypeEnum.VideoLink || item.mediaType == MediaTypeEnum.AudioLink) ? (
                <div className="video">
                    <ReactPlayer url={(item.mediaFile ? item.mediaFile.url : item.link)} controls />
                </div>
            ) : ((item.mediaType == MediaTypeEnum.AudioFile) ? (
                <figure>
                    <video controls className="imgFile"
                        src={(item.mediaFile ? item.mediaFile.url : item.link)}
                        poster={item.imageFile ? item.imageFile.url : item.image}
                        />
                </figure>
            ) : ((item.mediaType == MediaTypeEnum.ImageFile) ? (
                ((item.link || item.mediaFile) && (<div className="paddingBottom-md">
                    <ImageInput image={item.link} imageFile={item.mediaFile!}
                        onChange={item.set_mediaFile}
                        accept="image/*, video/*, audio/*"
                        createThumb={false} />
                </div>))
            ) : ((item.mediaType == MediaTypeEnum.Unsupport) ? (
                !!item.link && (<div className="paddingBottom-md">
                    <label className="font-bold">{t('app.presentation.previewMedia.unsupportMedia')}:</label>{" "}
                    {item.link}
                </div>)
            ) : ((item.mediaType == MediaTypeEnum.Unknown) ? (
                null
            ) : `${t('app.presentation.previewMedia.todo')} ${item.mediaType}`))))}

            <div className="paddingBottom-lg">
                <div className="paddingBottom-sm">
                    {(item.link || item.mediaFile) && (<a onClick={onClearMedia} title="Clear media"><Icon type="delete" className="icon-sm" /></a>)}
                    <span className="paddingLeft-xs">{item.mediaFile && item.mediaFile.name}</span>
                </div>
                <div className="paddingBottom-sm">
                    <DumbUpload onChange={item.set_mediaFile} accept="image/*, video/*, audio/*">
                        <Button><Icon type="upload" />{t('app.presentation.mediaInput.upload')}</Button>
                    </DumbUpload>
                    <Divider type="vertical" />
                    <VideoRecordButton onChange={onRecorded}><Icon type="video-camera" />{t('app.presentation.mediaInput.record')}</VideoRecordButton>
                    <Divider type="vertical" />
                    <AudioRecordButton onChange={onRecorded}><Icon type="audio" />{t('app.presentation.mediaInput.record')}</AudioRecordButton>
                    {item.mediaType == MediaTypeEnum.AudioFile && (<>
                        <Divider type="vertical"/>
                        <DumbUpload onChange={item.set_imageFile} createThumb>
                            <Button><Icon type="picture" />{t('app.presentation.mediaInput.addPoster')}</Button>
                        </DumbUpload>
                    </>)}
                    
                </div>
                <Input value={item.link} onChange={onChangeLink}
                    type="url"
                    placeholder={t("form.activity.url.placeholder")} />
            </div>

            <HtmlInput value={item.content} onChange={item.set_content}
                onBlur={endEdit}
                placeholder={t("form.activity.description.placeholder")}
                />
        </div>
    );
});

