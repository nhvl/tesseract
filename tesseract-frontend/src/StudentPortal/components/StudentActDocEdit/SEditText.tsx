import React, { FC, useCallback, useState } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { StudentActItem } from "../../../models/StudentActDoc";

import { Typography, Panel } from "antd";
const { Title } = Typography;

import {Input} from "../../../components/inputs/antd/Input";
import { HtmlInput } from "../../../components/inputs/HtmlInput";

import { RemoveItem } from "./RemoveItem";

import viewStyles from "../../../components/StudentDocSubmission/StudentDocSubmission.module.less";
import styles from "./StudentActDocEdit.module.less";

export const SEditText: FC<{item: StudentActItem}> = observer(({item}) => {
    const { t } = useTranslation();
    const [, setEditting] = useState(false);
    const endEdit = useCallback(() => setEditting(false), [setEditting]);

    return (
        <div className={`${styles.addMaterial} is-dragging-border mb-lg`}>
            <div className={viewStyles.subTitle}>
                <div className={`${styles.studenEditHeader}`}>
                    <div className="flex-1">
                        <Title level={4}><Input value={item.title} onChange={item.set_title} placeholder={t('form.activity.subtitle.placeholder')}/></Title>
                    </div>
                    <div className="ml-sm">
                        <RemoveItem item={item} placement={'left'}/>
                    </div>
                </div>
            </div>
            <HtmlInput value={item.content} onChange={item.set_content}
                onBlur={endEdit}
                placeholder={t('form.activity.description.placeholder')} />
        </div>
    );
});
