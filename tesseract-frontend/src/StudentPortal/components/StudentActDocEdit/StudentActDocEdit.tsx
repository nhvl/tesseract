import React, { FC, useCallback } from "react";
import { observer } from "mobx-react";
import classNames from "classnames";

import { StudentActDoc, StudentActItem } from "../../../models/StudentActDoc";

import { useStore } from "../../stores";

import { Card, Typography, Icon, } from "antd";
const { Title } = Typography;

import { DragDropContextProps, DragDropContext, Droppable, DroppableProvided, DroppableStateSnapshot, Draggable } from "react-beautiful-dnd";

import { StudentActItemEdit } from "./StudentActItemEdit";
import { StudentAddBtn } from "./StudentAddBtn";

import viewStyles from "../../../components/StudentDocSubmission/StudentDocSubmission.module.less";

export const StudentActDocEdit: FC<{ doc: StudentActDoc }> = observer(({ doc }) => {
    const {sEditStudentActDoc} = useStore();

    const dragEndHandler:DragDropContextProps["onDragEnd"] = useCallback((result) => {
        if (!result.destination) return;
        sEditStudentActDoc.onDrag(result.source.index, result.destination.index);
    }, []);

    return (<>
        <Card bordered={false} className="none-shadow paddingBottom-lg"
            cover={!!doc.banner && (<div className={viewStyles.banner}><img src={doc.banner} /></div>)}>
            <Title level={1}>
                <div dangerouslySetInnerHTML={{ __html: doc.title }} />
            </Title>
            {doc.summary && (
                <div className="font-size-lg text-grey description"
                    dangerouslySetInnerHTML={{ __html: doc.summary }} />
            )}
        </Card>


        <div className="editor-drag-and-drop student">
            <DragDropContext onDragEnd={dragEndHandler}>
                <Droppable droppableId="droppable">{(provided, snapshot) => (
                    <DroppableInner
                        provided={provided}
                        snapshot={snapshot}
                        doc={doc}
                        />
                )}</Droppable>
            </DragDropContext>
        </div>
        <StudentAddBtn />
    </>);
});

const DroppableInner:FC<{
    doc:StudentActDoc,
    provided:DroppableProvided, snapshot:DroppableStateSnapshot,
}> = observer(({provided, snapshot, doc}) => {
    return (
        <div ref={provided.innerRef}
            {...provided.droppableProps}
            className={classNames({isDraggingOver:snapshot.isDraggingOver})}
            >
            {doc.items.map((item, index) => (
                <DragableEditItem key={item.id} item={item} index={index} />
            ))}
            {provided.placeholder}
        </div>
    );
});

const DragableEditItem: FC<{ item: StudentActItem, index: number }> = observer(({item, index}) => {
    const disableDrag = item.fromTeacher;
    return (
        <Draggable draggableId={item.id} index={index}
            isDragDisabled={disableDrag}
            >{(provided, snapshot) => (<>
            <div ref={provided.innerRef}
                {...provided.draggableProps}
                className={classNames("wrapper mb-lg", {'is-dragging':snapshot.isDragging})}
                style={provided.draggableProps.style}
                >
                <div {...provided.dragHandleProps} className="is-dragging" hidden={disableDrag}>
                    <Icon type="more" className="icon-sm" />
                </div>
                <StudentActItemEdit key={item.id} item={item} />
                <StudentAddBtn item={item} hidden />
            </div>
            </>
        )}</Draggable>
    );
});
