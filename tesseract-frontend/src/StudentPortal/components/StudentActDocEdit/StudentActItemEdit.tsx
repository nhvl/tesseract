import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";

import { StudentActItem } from "../../../models/StudentActDoc";
import { EActItemType } from "../../../models/ActDoc";

import { StudentActItemView } from "../../../components/StudentDocSubmission/StudentActItemView";

import { SEditHeading } from "./SEditHeading";
import { SEditText } from "./SEditText";
import { SEditMediaItem } from "./SEditMediaItem";
import { SEditGoogleDriveItem } from "./SEditGoogleDriveItem";
import { useStore } from "../../stores";
import { Modal } from "../../../components/Modal/ModalAntd";

export const StudentActItemEdit: FC<{ item: StudentActItem }> = observer((props) => {
    const {item} = props;
    const {sGoogleDriveStore} = useStore();
    
    useEffect(() => {
        sGoogleDriveStore.init().then(err => {
            if(err) Modal.error({content: err.message});
        });
    }, [item]);
    if (item.fromTeacher) return (<StudentActItemView  {...props} noAnswer />);

    switch (props.item.type) {
        case EActItemType.Heading       : return (<SEditHeading         {...props} />);
        case EActItemType.Text          : return (<SEditText            {...props} />);
        case EActItemType.Media         : return (<SEditMediaItem       {...props} />);
        case EActItemType.Embed         : return (<SEditGoogleDriveItem {...props} />);
        default                         : return (<StudentActItemView   {...props} noAnswer />);
    }
});
