import React, { FC } from "react";
import { observer } from "mobx-react";

import { StudentActItem } from "../../../models/StudentActDoc";

import { useStore } from "../../stores";

import { Popover, Divider, Row, Col, Button, } from "antd";

import styles from "./StudentActDocEdit.module.less";
import { useTranslation } from "react-i18next";

export const StudentAddBtn: FC<{item?:StudentActItem, hidden?:boolean}> = observer(({item, hidden}) => {
    const {t} = useTranslation();
    const {sEditStudentActDoc} = useStore();

    if (hidden) return null;

    return (
        <Popover
            content={(
                <Row gutter={24}>
                    <Col xs={24} sm={12} className={styles.column}>
                        <ul className="list-reset">
                            <li className="font-size-lg none">Content</li>
                            <Divider />
                            <li><a onClick={() => sEditStudentActDoc.addHeading(item)}><i className="fas fa-heading"    /><span className="paddingLeft-sm">{t('app.presentation.addBtn.heading')}</span></a></li>
                            <li><a onClick={() => sEditStudentActDoc.addText   (item)}><i className="fas fa-text-width" /><span className="paddingLeft-sm">{t('app.presentation.addBtn.text')}</span></a></li>
                            <li><a onClick={() => sEditStudentActDoc.addMedia  (item)}><i className="fas fa-image"      /><span className="paddingLeft-sm">{t('app.presentation.addBtn.multimedia')}</span></a></li>
                            <li><a onClick={() => sEditStudentActDoc.addGoogleDrive  (item)}><i className="fab fa-google-drive"      /><span className="paddingLeft-sm">{t('app.presentation.addBtn.googleDrive')}</span></a></li>
                        </ul>
                    </Col>
                </Row>
            )}>
            <Button type="primary" icon="plus" className="add-btn mb-lg">{t('app.presentation.addBtn.insertContent')}</Button>
        </Popover>
    );
});
