import React, { FC, CSSProperties } from 'react';
import { observer } from 'mobx-react';

import { Layout } from 'antd';
const { Content } = Layout;

import { SiderMenuWrapper } from '../components/SiderMenu';
import { HeaderView } from './Header';

import styles from './BasicLayout.module.less';

import { useStore } from '../stores';
import classNames from 'classnames';

const contentStyle:{[key:string]: CSSProperties} = {
  fixedHeader: { paddingTop:0 },
  default: {},
}

const layoutStyle: CSSProperties = { minHeight:"100vh" }

export const BasicLayout: FC = observer(({ children }) => {
  const store = useStore();
  const {sLeftNav, isMobile} = store;

  return (
    <Layout style={layoutStyle}>
      <SiderMenuWrapper />
      <Layout className={classNames(!isMobile && (sLeftNav.collapsed 
                                                      ? styles.collapsedLayout 
                                                      : styles.expandedLayout)
                                    , "min-h-screen")}>
        <HeaderView />
        <Content className={styles.content} style={contentStyle.fixedHeader}>
          {children}
        </Content>
      </Layout>
    </Layout>
  );
});
