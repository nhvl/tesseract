import {observable, computed, action} from "mobx";

import {DbIdentity } from "../../models/types"
import { Class } from "../../models/Class";
import { Faculty } from "../../models/Faculty";

import { aFetch } from "../../services/api/fetch";
import { parseGeneralViewModel, IGeneralViewModel, GeneralDto } from "../../models/GeneralViewModel";

export class StudentClass extends Class {
    @observable grade    : number|undefined  = undefined;
    @observable faculties: Faculty[]         = [];

    @observable sortIndex:number = 10000;
    @action set_sortIndex = (v:number) => { this.sortIndex = v }

    constructor(data?:any) {
        super(data);
        if (data) {
            const {grade, sortIndex, faculties} = data;
            Object.assign(this, {grade, sortIndex});
            if (Array.isArray(faculties)) this.faculties = faculties.map(x => new Faculty(x));
        }
    }

    toJS() {
        const data = super.toJS();
        return ({...data, sortIndex: this.sortIndex});
    }

    @computed get facultyNames(): string[]{
        return this.faculties.map(x => x.fullName);
    }

    static async getClasses({studentId, schoolId}:{studentId: DbIdentity, schoolId:DbIdentity}) {
        const [err, xs] = await aFetch<{}[]>("GET", `/student/${studentId}/school/${schoolId}/class`);
        return [err, err ? [] : xs.map((d) => new StudentClass(d))] as const;
    }

    static async batchUpdate({studentId, schoolId}:{studentId: DbIdentity, schoolId:DbIdentity}, xs: StudentClass[]) {
        const [err, dto] = await aFetch<GeneralDto>("PUT", `/student/${studentId}/school/${schoolId}/class`, xs.map(x => x.toJS()));
        return [err, err ? [] : fromGeneralViewModel(parseGeneralViewModel(dto))] as const;
    }
}

export function fromGeneralViewModel(vm: IGeneralViewModel) {
    const cs = vm.classes.map(c => new StudentClass(c));
    vm.classStudents.forEach(cf => {
        const c = cs.find(c => c.classId == cf.classId);
        if (c == null) return;
        c.sortIndex = cf.sortIndex;
        c.grade = cf.cumulativeGrade;
    });
    return cs;
}

export function sortClass(a:StudentClass, b:StudentClass) {
    const d = (a.sortIndex || Number.MAX_SAFE_INTEGER) - (b.sortIndex || Number.MAX_SAFE_INTEGER);
    if (d != 0) return d;
    return a.period.localeCompare(b.period);
}
