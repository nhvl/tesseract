import {observable, action} from "mobx";

import {DbIdentity } from "../../models/types"
import { Class } from "../../models/Class";

import { aFetch } from "../../services/api/fetch";
import { IGeneralViewModel, parseGeneralViewModel, GeneralDto } from "../../models/GeneralViewModel";

export class AsFacultyClass extends Class {
    @observable sortIndex:number = 10000;
    @action set_sortIndex = (v:number) => { this.sortIndex = v }

    constructor(data?:any) {
        super(data);
        if (data) {
            const {sortIndex} = data;
            Object.assign(this, {sortIndex});
        }
    }

    toJS() {
        const data = super.toJS();
        return ({...data, sortIndex: this.sortIndex});
    }

    static async fetchItems({studentId, schoolId, gradingTermId}:{studentId: DbIdentity, schoolId:DbIdentity, gradingTermId?:DbIdentity}) {
        const [err, data] = await aFetch<GeneralDto>("GET",
            gradingTermId == null || gradingTermId < 1
            ? `/student/${studentId}/school/${schoolId}/asFacultyClass`
            : `/student/${studentId}/school/${schoolId}/gradingTerm/${gradingTermId}/asFacultyClass`);
        const vm = err ? undefined : parseGeneralViewModel(data);
        const cs:AsFacultyClass[] = (vm == null ? [] : fromGeneralViewModel(vm));
        return [err, err ? [] : cs.map((d) => new AsFacultyClass(d))] as const;
    }
}

export function fromGeneralViewModel(vm: IGeneralViewModel) {
    const cs = vm.classes.map(c => new AsFacultyClass(c));
    vm.classFaculties.forEach(cf => {
        const c = cs.find(c => c.classId == cf.classId);
        if (c == null) return;
        c.sortIndex = cf.sortIndex;
    });
    return cs;
}

export function sortClass(a:AsFacultyClass, b:AsFacultyClass) {
    const d = (a.sortIndex || Number.MAX_SAFE_INTEGER) - (b.sortIndex || Number.MAX_SAFE_INTEGER);
    if (d != 0) return d;
    return a.period.localeCompare(b.period);
}
