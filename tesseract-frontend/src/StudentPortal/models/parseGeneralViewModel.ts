import { GeneralDto, parseGeneralViewModel as parseGvm, IGeneralViewModel } from "../../models/GeneralViewModel";
import { StudentActivity } from "./StudentActivity";
import { DbIdentity } from "../../models/types";
import { IActivityScore } from "../../models/ActivityScore";

export interface ISpGeneralVm extends IGeneralViewModel {
    activities: StudentActivity[];
}

export function parseGeneralViewModel(data:GeneralDto): ISpGeneralVm {
    const a2ac = new Map(Array.isArray(data.activityScores) ? data.activityScores.map<[DbIdentity, IActivityScore]>(ac => [ac.activityId, ac]) : []);
    const vm = parseGvm(data);
    const activities = !Array.isArray(data.activities) ? [] : data.activities.map(a => new StudentActivity({...a, score: a2ac.get(a.activityId)}));
    return ({
        ...vm,
        activities,
    });
}
