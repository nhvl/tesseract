import {observable, computed} from "mobx";

import { DbIdentity } from "../../models/types";
import { Activity, ActivityType } from "../../models/Activity";
import { ActivityScore } from "../../models/ActivityScore";
import { GeneralDto } from "../../models/GeneralViewModel";

import { aFetch } from "../../services/api/fetch";
import { parseGeneralViewModel } from "./parseGeneralViewModel";

export class StudentActivity extends Activity {
    @observable.ref score?: ActivityScore;

    constructor(data?:{}) {
        super(data);
        this.score = undefined;

        if (data != null) Object.assign(this, data);
    }

    @computed get scoreNumber(): number{
        return this.score && this.score.score ? this.score.score: -1 ;
    }

    @computed get isOverdate() {
        return this.dateDue != null && Date.now() > this.dateDue;
    }

    @computed get isOverdue() {
        return this.isOverdate && (this.score == null || this.score.savedDate == null);
    }

    static async fetchActivity({schoolId, activityId, studentId}:{schoolId:DbIdentity, activityId:DbIdentity, studentId:DbIdentity}) {
        const [err, dto] = await aFetch<GeneralDto>("GET" , `/student/${studentId}/school/${schoolId}/activity/${activityId}`);
        const vm = (err ? undefined : parseGeneralViewModel(dto))!;
        return [err, vm] as const;
    }

    static async fetchActivities({studentId, schoolId, classId, type}:{studentId: DbIdentity, schoolId:DbIdentity, classId?:DbIdentity, type?:ActivityType}) {
        const [err, dto] = await aFetch<GeneralDto>("GET" , `/student/${studentId}/school/${schoolId}${!classId ? "" : `/class/${classId}`}/activity`, {type});
        const vm = (err ? undefined : parseGeneralViewModel(dto))!;
        return [err, vm] as const;
    }
}
