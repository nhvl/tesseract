import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { BasicLayout } from '../../layouts/BasicLayout';
import { CurrentClassList } from '../../components/Class/CurrentClassList';

export const ClassList: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <CurrentClassList />
    </BasicLayout>);
});
