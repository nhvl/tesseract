import React, { FC, useCallback, useState } from "react";
import { observer } from "mobx-react";

import { StudentActivity } from "../../models/StudentActivity";

import { useStore } from "../../stores";

import { Divider, Icon, } from "antd";

import { Link } from "../../../components/router/Links";
import { CommentLink } from "./CommentLink";
import styles from "./ClassActivity.module.less";
import { useTranslation } from "react-i18next";

export const ClassActivity: FC<{}> = observer(({}) => {
    const {sClassDetail} = useStore();

    return (<>{
        sClassDetail.rootActivities.map(a =>
            <ActivityTree key={a.activityId} item={a} level={0} />
        )
    }</>);
});

const ActivityTree: FC<{item:StudentActivity, level:number}> = observer(({item, level}) => {
    const {t} = useTranslation();
    const {sClassDetail} = useStore();
    const xs = sClassDetail.activityId2Children.get(item.activityId);

    const [collapsed, setCollapsed] = useState(false);
    const toggleCollapsed = useCallback(() => { setCollapsed(collapsed => !collapsed) }, []);

    const nDiscussion = sClassDetail.activityCountDiscussion[item.activityId] || 0;

    return (<>
            <div className={`${styles.node} p-2`} style={{marginLeft: `${level*2}em`}}>
                <div className={styles.nodeContent}>
                    {xs && (
                        <a onClick={toggleCollapsed}>{collapsed
                            ? (<span className={styles.rst__expandButton} />)
                            : (<span className={styles.rst__collapseButton} />)
                        }</a>
                    )}
                    <div>
                        <span className="font-medium font-size-md">{item.title}</span>
                    </div>
                    <Link routeName="viewActDoc" params={item.params}>{t("app.classes.activity.tree.viewDetails")}</Link>
                    <Divider type="vertical" />
                    <CommentLink item={item} />
                    {0 < nDiscussion && (<>
                        <Divider type="vertical" />
                        <Link routeName="activityDiscussion" params={item.params}>{t("app.classes.activity.tree.discussions")}</Link>
                    </>)}
                </div>
            </div>
            {xs && !collapsed && xs.map(x => (
                <ActivityTree key={x.activityId} item={x} level={level+1} />
            ))}
        </>
    );
});
