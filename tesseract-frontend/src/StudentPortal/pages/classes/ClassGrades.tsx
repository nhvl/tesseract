import React, { FC, useMemo, useCallback, useRef } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Activity, ActivityType } from "../../../models/Activity";
import { ActivityScore } from "../../../models/ActivityScore";
import { StudentActivity } from "../../models/StudentActivity";

import { useStore } from "../../stores";

import { Table, Button, Typography, Divider } from "antd";
const {Title} = Typography;
import { ColumnProps } from "antd/lib/table";

import { StyledDateTime } from "../../../components/DateTime";
import { Link } from "../../../components/router/Links";
import { LongScoreMedals } from "../../../components/ScoreMedals";
import { CommentLink } from "./CommentLink";
import { ResponsiveActions } from "../../../components/ResponsiveTable/ResponsiveActions";

import { printElement } from "../../../utils/printElement";
import { useNumberTrans } from "../../../components/DateTime/Number";

export const ClassGrades: FC<{}> = observer(({}) => {
    const store = useStore();
    const {sClassDetail, currentSchool} = store;
    const {t} = useTranslation();
    const {percent} = useNumberTrans();

    const rTable = useRef<HTMLDivElement>(undefined!);

    const onPrint = useCallback(() => {
        printElement(rTable.current, {
            copyStyles      : true,
            removeAfterPrint:  true,
        });
    }, []);

    const {aClass, } = sClassDetail;

    return (<>
        <Button className="mb-sm" onClick={onPrint} type="primary">Print my Grades</Button>
        <div ref={rTable}>
            <div className="hidden print:show">
                <div className="flex justify-between mb-lg text-black">
                    <div>
                        <div className="flex items-center mb-lg">
                            <div className="heading-3 mr-lg">
                                {store.currentUser!.firstName} {store.currentUser!.lastName}
                            </div>
                            <div>{store.currentUser!.studentNumber}</div>
                        </div>

                        {aClass && (<div className="flex items-center mb-xs">
                            <div className="mr-lg"><span>{t("app.classes.list.className")}: </span><span className="font-bold">{aClass.className}</span></div>
                            <div><span>{t("app.classes.gradingTerm")}: </span><span className="font-bold">{store.curentGradingTermName}</span></div>
                        </div>)}

                        {aClass && (<div className="flex">
                            <div className="mr-lg">
                                <span>{t("app.classes.header.Grade")}: </span>
                                <span className="font-bold">{(aClass.grade ? (
                                    <>{percent(aClass.grade)}</>
                                ) : t("app.students.detail.DateNA"))}</span>
                            </div>
                            <div>
                                <span>{t("app.classes.header.teacher")}: </span>
                                <span className="font-bold">{aClass.faculties.map((f, index) => <span key={index}>{f.fullName}</span>)}</span>
                            </div>
                        </div>)}
                    </div>
                    {currentSchool && (<div>
                        <img src={currentSchool.logoUrl || currentSchool.iconUrl} />
                        <div className="font-bold font-size-lg">{currentSchool.schoolName}</div>
                    </div>)}
                </div>
            </div>
            <ActivityTable items={sClassDetail.cScoreActivities} activityTitle="" />
        </div>
    </>);
});

const ActivityTable: FC<{items:StudentActivity[], activityTitle:string}> = observer(({items, activityTitle}) => {
    const {t} = useTranslation();

    const dateRender = useCallback((d:number) => (
        (d == null || d <= 0)
            ? t("app.students.detail.DateNA")
            : (<StyledDateTime value={d} />)
        ), [t]);

    const columns = useMemo<Array<ColumnProps<StudentActivity>>>(() => [
        {
            key: "title",
            title: t("app.students.detail.name"),
            dataIndex: "title",
            className: "whitespace-normal min-w",
        },
        {
            key: "dateDue",
            title: t("app.students.detail.activtyDueDate"),
            dataIndex: "dateDue",
            render: dateRender,
            sorter: Activity.sorter.dateDue,
            sortDirections: ["descend", "ascend"]
        },
        {
            key: "weight",
            title: t("app.students.detail.weight"),
            render: (_, a) => (a.weight),
            sorter: Activity.sorter.weight,
            sortDirections: ["descend", "ascend"]
        },
        {
            key: "score",
            title: t("app.students.detail.score"),
            className: "whitespace-normal",
            render: (_, a) => (<SkillScoreCell activity={a} score={a.score} />),
            sorter: scoreSorter,
            sortDirections: ["descend", "ascend"]
        },
        {
            key: "actions",
            className: "print:hidden",
            title: t("app.students.detail.action"),
            render: (_, a) => (<ActionCell item={a} />)
        },
    ], [t]);

    return (
        <div className="responsiveTable">
            <Table columns={columns}
                dataSource={items}
                pagination={false}
                rowKey={rowKey}
                rowClassName={rowClassName}/>
        </div>
    );
});

const SkillScoreCell: FC<{score?: ActivityScore, activity: Activity}> = observer(({score, activity}) => {
    return ((score != null && activity.isGraded) ? (<>
        <ScoreCell score={score} activity={activity} />
        <SkillCell score={score} activity={activity} />
    </>) : null);
});

const ScoreCell: FC<{score?: ActivityScore, activity: Activity}> = observer(({score, activity}) => {
    return ((score != null && activity.isGraded && score.score != null) ? (
        <div className="text-black font-medium"><span>{score.score}</span><span>/{activity.maxScore}</span></div>
    ) : null);
});

const SkillCell: FC<{score?: ActivityScore, activity: Activity}> = observer(({score, activity}) => {
    const store = useStore();
    return ((score != null && activity.isGraded) ? (
        <LongScoreMedals score={score} scoreBadge={store.scoreBadge} />
    ) : null);
});

const ActionCell: FC<{item: StudentActivity}> = observer(({item}) => {
    const {t} = useTranslation();
    const {mediaBreakpoint, sClassDiscussionList} = useStore();
    const discussion = sClassDiscussionList.displayItems.find(x => x.discussionActivityId==item.activityId);

    return (<ResponsiveActions breakpoint={mediaBreakpoint} xs={0}>
        {discussion && <Link hidden={item.type != ActivityType.Discussion} routeName="discussion" params={discussion.params}>{t('app.classes.activity.tree.viewDetails')}</Link>}
        {item.type != ActivityType.Discussion && <Link routeName="viewActDoc" params={item.params}>{t('app.classes.activity.tree.viewDetails')}</Link>}
        {item.type != ActivityType.Discussion && <Link routeName="activityDetail" params={item.params}>{t('app.classes.activity.tree.submissions')}</Link>}
        <CommentLink item={item} />
        {item.type != ActivityType.Discussion && <Link routeName="activityDiscussion" params={item.params}>{t('app.classes.activity.tree.discussions')}</Link>}
    </ResponsiveActions>);
});

function rowKey(record:StudentActivity) { return String(record.activityId) }
function rowClassName(record:StudentActivity) { return (record.isOverdue ? "warning-row": "") }

function scoreSorter(a:StudentActivity, b:StudentActivity) { return (a.scoreNumber) - (b.scoreNumber) }
