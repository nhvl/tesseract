import React, { FC, useEffect, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Card, Skeleton, Breadcrumb, Tabs, Typography, } from "antd";
const {Title} = Typography;

import { useNumberTrans } from "../../../components/DateTime/Number";
import { Link } from "../../../components/router/Links";
import { RouterView } from "../../../components/router/RouterView";
import { BasicLayout } from "../../layouts/BasicLayout";
import { ClassBreadcrumb } from "../../components/ClassBreadcrumb";

import {ClassActivity} from "./ClassActivity";
import {ClassAssessments, ClassAssignments} from "./ClassAssignments";
import {ClassGrades} from "./ClassGrades";
import {ClassDiscussionList} from "../discussion/ClassDiscussionList";

import {extendsRouterState} from "../../../utils/extendsRouterState";
import { GradeRange } from "../../../models/GradeRange";

export const ClassDetailPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <ClassDetail />
    </BasicLayout>);
});

const viewMap = ({
    classDetail     : (<ClassActivity       />),
    classActivities : (<ClassActivity       />),
    classAssignments: (<ClassAssignments    />),
    classAssessments: (<ClassAssessments    />),
    discussionList  : (<ClassDiscussionList />),
    classGrades     : (<ClassGrades         />),
});

export const ClassDetail: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const {percent} = useNumberTrans();

    const store = useStore();
    const {sClassDetail, routerStore, sClassDiscussionList} = store;
    const {routeName, params} = routerStore.routerState;

    const sClassId = params.classId;
    useEffect(() => {
        const classId = !sClassId ? NaN : Number(sClassId);
        if (Number.isNaN(classId) || classId < 1) {
            routerStore.goTo("classes");
            return;
        }

        sClassDetail.init(classId);
        sClassDiscussionList.init({classId});
    }, [sClassId, sClassDiscussionList]);

    useEffect(() => {
        if (routeName == "classDetail")
            store.replaceRouterState(extendsRouterState(routerStore.routerState, {routeName:"classActivities"}))
    }, [routeName]);

    const changeTab = useCallback((activeKey: string) => {
        routerStore.goTo(activeKey, params)
    }, [params]);

    const {aClass} = sClassDetail;
    if (!aClass) return (<Skeleton />);

    return (<>
        <ClassBreadcrumb>
            <Breadcrumb.Item>{aClass.className}</Breadcrumb.Item>
        </ClassBreadcrumb>
        <div className="flex items-start justify-between">
            <div className="header">
                <Title level={3}>{aClass.className}</Title>
            </div>
            <div className="text-black font-size-md flex">
                <div className="mr-lg">
                    <span>{t('app.classes.header.classGrade')}: </span>
                    <span className="heading-3">{(aClass.grade ? (
                        <>{percent(aClass.grade)}</>
                    ) : t("app.students.detail.DateNA"))}</span>
                </div>
                <div>
                    <span>{t('app.classes.header.teacher')}: </span>
                    <span className="heading-3">{aClass.faculties.map((f, index) => <span key={index}>{f.fullName}</span>)}</span>
                </div>
            </div>
        </div>
        <Card>
            <Tabs activeKey={routeName} onChange={changeTab}>
                <Tabs.TabPane key="classActivities"  tab={<Link routeName="classActivities"  params={params}>{t('app.classes.menu.activities')}</Link>} />
                <Tabs.TabPane key="classAssignments" tab={<Link routeName="classAssignments" params={params}>{t('app.classes.menu.assignments')}</Link>} />
                <Tabs.TabPane key="classAssessments" tab={<Link routeName="classAssessments" params={params}>{t('app.classes.menu.assessments')}</Link>} />
                <Tabs.TabPane key="discussionList"   tab={<Link routeName="discussionList"   params={params}>{t('app.classes.menu.discussions')}</Link>} />
                <Tabs.TabPane key="classGrades"      tab={<Link routeName="classGrades"      params={params}>{t('app.classes.menu.classGrades')}</Link>} />
            </Tabs>
            <RouterView viewMap={viewMap} />
        </Card>
    </>);
});
