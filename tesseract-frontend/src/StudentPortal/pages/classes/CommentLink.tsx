import React, { FC, useCallback } from "react";
import { observer } from "mobx-react";

import { StudentActivity } from "../../models/StudentActivity";

import { useStore } from "../../stores";
import { useTranslation } from "react-i18next";

export const CommentLink: FC<{item: StudentActivity}> = observer(({item}) => {
    const {t} = useTranslation();
    const {sActivityScoreCommentDialog} = useStore();

    const showComment = useCallback(() => {
        sActivityScoreCommentDialog.open(item);
    }, [sActivityScoreCommentDialog]);

    return (<span>
        <a onClick={showComment}>{t('app.classes.comments')}</a>
    </span>);
});
