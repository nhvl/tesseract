import React, { FC, useMemo, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Activity } from "../../../models/Activity";
import { ActivityScore } from "../../../models/ActivityScore";
import { StudentActivity } from "../../models/StudentActivity";

import { useStore } from "../../stores";

import { Table, Tag, } from "antd";
import { ColumnProps } from "antd/lib/table";

import { StyledDateTime, useDateTrans } from "../../../components/DateTime";
import { Link } from "../../../components/router/Links";
import { LongScoreMedals } from "../../../components/ScoreMedals";
import { CommentLink } from "./CommentLink";
import { ResponsiveActions } from "../../../components/ResponsiveTable/ResponsiveActions";

export const ClassAssessments: FC<{}> = observer(({}) => {
    const {sClassDetail} = useStore();
    const {t} = useTranslation();

    return (
        <ActivityTable items={sClassDetail.cAssessments} activityTitle={t("app.activities.assessment")} />
    );
});

export const ClassAssignments: FC<{}> = observer(({}) => {
    const {sClassDetail} = useStore();
    const {t} = useTranslation();

    return (
        <ActivityTable items={sClassDetail.cAssignments} activityTitle={t("app.activities.assignment")} />
    );
});


const ActivityTable: FC<{items:StudentActivity[], activityTitle:string}> = observer(({items, activityTitle}) => {
    const {t} = useTranslation();
    const dateRender = useCallback((d:number) => (
        (d == null || d <= 0)
            ? t("app.students.detail.DateNA")
            : (<StyledDateTime value={d} />)
        ), [t]);

    const columns = useMemo<Array<ColumnProps<StudentActivity>>>(() => [
        {
            key: "title",
            title: activityTitle,
            dataIndex: "title",
            className: "whitespace-normal min-w",
        },
        {
            key: "dateDue",
            title: t("app.students.detail.activtyDueDate"),
            dataIndex: "dateDue",
            render: dateRender,
            sorter: Activity.sorter.dateDue,
            sortDirections: ["descend", "ascend"],
        },
        {
            key: "score",
            title: t("app.students.detail.score"),
            render: (_, a) => (<GradeCell max={a.maxScore} score={a.score} isGraded={a.isGraded} activityId={a.activityId} />),
            className: "whitespace-normal",
            sorter: scoreSorter,
            sortDirections: ["descend", "ascend"]
        },
        {
            key: "status",
            title: t("app.students.activityTable.status.label"),
            render: (_, a) => (<StatusCell activity={a} score={a.score} />),
            className: "whitespace-normal",
        },
        {
            key: "actions",
            title: t("app.students.detail.action"),
            render: (_, a) => (<ActionCell item={a} />)
        },
    ], [t]);

    return (
        <div className="responsiveTable">
            <Table columns={columns}
                dataSource={items}
                pagination={false}
                rowKey={rowKey}
                rowClassName={rowClassName}/>
        </div>
    );
});

const GradeCell: FC<{score?: ActivityScore, max: number, isGraded: boolean, activityId: number}> = observer(({score, max, isGraded}) => {
    const store = useStore();
    return (score != null && isGraded) ? (
        <div>
            {score.score != null && (
                <div className="text-black font-medium"><span>{score.score}</span><span>/{max}</span></div>
            )}
            <LongScoreMedals score={score} scoreBadge={store.scoreBadge} />
        </div>
    ) : null;
});

const StatusCell: FC<{activity: StudentActivity, score?:ActivityScore}> = observer(({activity, score}) => {
    const {t} = useTranslation();
    const { fromNow } = useDateTrans();

    const dateDue = activity.dateDue || activity.dateCutoff;
    const savedDate = score ? score.savedDate || 0 : Infinity;
    const isSubmitted = score && score.isSubmitted;

    return (isSubmitted ? (
        (dateDue == null || savedDate < dateDue)
            ? <Tag className="success">{t("app.students.activityTable.status.submitted")}</Tag>
            : <Tag className="success">{t("app.students.activityTable.status.lateSubmitted")}</Tag>
    ) : (dateDue == null ? t("app.students.activityTable.status.dueNA") :
        (Date.now() < dateDue)
            ? <Tag className="info">{t("app.students.activityTable.status.due", {x: fromNow(dateDue)})}</Tag>
            : <Tag className="error">{t("app.students.activityTable.status.missing")}</Tag>
    ));
});

const ActionCell: FC<{item: StudentActivity}> = observer(({item}) => {
    const {t} = useTranslation();
    const {mediaBreakpoint, sClassDetail} = useStore();
    const nDiscussion = sClassDetail.activityCountDiscussion[item.activityId] || 0;
    return (<ResponsiveActions breakpoint={mediaBreakpoint} md={Infinity}>
        <Link routeName="viewActDoc" params={item.params}>{t("app.classes.activity.tree.viewDetails")}</Link>
        <Link routeName="activityDetail" params={item.params}>{t("app.classes.activity.tree.submissions")}</Link>
        <CommentLink item={item} />
        {0 < nDiscussion && (
            <Link routeName="activityDiscussion" params={item.params}>{t("app.classes.activity.tree.discussions")}</Link>
        )}
    </ResponsiveActions>);
});

function rowKey(record:StudentActivity) { return String(record.activityId) }
function rowClassName(record:StudentActivity) { return (record.isOverdue ? "warning-row": "") }

function scoreSorter(a:StudentActivity, b:StudentActivity) { return (a.scoreNumber) - (b.scoreNumber) }
