import React, { FC, useEffect, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Card, Skeleton, Button, Typography, Divider, Icon, Alert, } from "antd";
const {Title} = Typography;

import { BasicLayout } from "../../layouts/BasicLayout";

import { ActivityBreadcrumb } from "../../components/ActivityBreadcrumb";

import { ViewActDoc } from "../../../components/ViewActDoc/ViewActDoc";

import { PollQuizItem } from "../../../models/ActDoc";

import { Link } from "../../../components/router/Links";
import { ActivityType } from "../../../models/Activity";
import { Modal } from "../../../components/Modal/ModalAntd";

export const ViewActDocPage: FC<{}> = observer(() => {
    const { t } = useTranslation();
    const { sViewActDoc, routerStore, currentUser, currentSchoolId } = useStore();

    const sActivityId = routerStore.routerState.params.activityId;
    const activityId = !sActivityId ? NaN : Number(sActivityId);
    useEffect(() => {
        if (Number.isNaN(activityId)) { routerStore.goTo("home"); return; }
        sViewActDoc.init(activityId).then((err) => {
            if (!err) return;
            Modal.error({ content: err.message, onOk() { routerStore.goTo("home") } });
        });
        window.scrollTo(0, 0);
    }, [routerStore.routerState.params.activityId]);

    const goBack = useCallback(() => {
        if (history.length > 1) history.back();
        else routerStore.goTo("editActDoc", routerStore.routerState.params)
    }, [routerStore]);

    const savePoll = useCallback((pollItemId:string, votes:number) => {
        return PollQuizItem.savePolling(currentSchoolId, sViewActDoc.activityId, currentUser!.studentId, pollItemId, votes);
    },[sViewActDoc, currentUser]);

    const { doc, activity } = sViewActDoc;
    if (doc == null) return (<Skeleton />);

    return (<BasicLayout>
        <ActivityBreadcrumb aClass={sViewActDoc.aClass} activity={sViewActDoc.activity} />
        <div className="flex items-start justify-between">
            <div className="header">

                <Title level={3}>{t("app.activities.actDoc.view")}</Title>
            </div>
            <div hidden={!activity || activity.type == ActivityType.Activity}>
                <Link routeName="activityDetail" params={routerStore.routerState.params}>
                    <Button className="mr-sm mb-sm" type="primary">{t("app.activities.actDoc.submit")}</Button>
                </Link>
            </div>
        </div>
        <div className="mb-4" hidden={!activity || activity.weight > 0}>
            <Alert message={t("app.activities.actDoc.isExclude")} type="info" />
        </div>
        <div className="mb-4" hidden={!activity || !activity.isCredit}>
            <Alert message={t("app.activities.actDoc.isCredit")} type="info" />
        </div>
        <Card>
            <ViewActDoc doc={doc} savePoll={sViewActDoc.isPollingTime ? savePoll : undefined}
                noAnswer={!activity || activity.type != ActivityType.Activity}/>
            <Divider />
            <div className="overflow-hidden mt-lg mb-lg">
                {/* <Button className="mr-sm" onClick={goBack}>{t("app.activities.actDoc.back")}</Button> */}
                {sViewActDoc.prevPage && (
                    <Link routeName="viewActDoc" params={sViewActDoc.prevPage.params} className="text-prev-next float-left">
                        <Icon type="left" />
                        <span className="ml-sm">{sViewActDoc.prevPage.title}</span>
                    </Link>
                )}
                {sViewActDoc.nextPage && (
                    <Link routeName="viewActDoc" params={sViewActDoc.nextPage.params} className="text-prev-next float-right text-right">
                        <span className="mr-sm">{sViewActDoc.nextPage.title}</span>
                        <Icon type="right" />
                    </Link>
                )}
            </div>
        </Card>
    </BasicLayout>);
});
