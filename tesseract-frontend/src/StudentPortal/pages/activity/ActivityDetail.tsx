import React, { FC, useEffect, useCallback } from "react";
import { observer } from "mobx-react";
import { RouterState } from "mobx-state-router";
import { useTranslation } from "react-i18next";

import { Submission } from "../../../models/Submission";

import { useStore } from "../../stores";

import { Button, Card, Divider, Skeleton, Breadcrumb, Icon, Alert, message, Typography, } from "antd";
const {Title} = Typography;

import { Upload3 } from "../../../components/inputs/antd/Upload";
import { LongDateLabel } from "../../../components/DateTime";
import { HtmlInput } from "../../../components/inputs/HtmlInput";
import { StudentActDocEdit } from "../../components/StudentActDocEdit/StudentActDocEdit";
import { StudentActDocView } from "../../../components/StudentDocSubmission/StudentActDocView";
import { ThreadView } from "../../components/thread/ThreadView";
import { BasicLayout } from "../../layouts/BasicLayout";
import { ActivityBreadcrumb } from "../../components/ActivityBreadcrumb";
import { ActivityType } from "../../../models/Activity";
import { Modal } from "../../../components/Modal/ModalAntd";

export const ActivityDetailPage: FC<{}> = observer(({ }) => {
  return (<BasicLayout>
    <ActivityDetail />
  </BasicLayout>)
});

export const ActivityDetail: FC<{}> = observer(({ }) => {
  const { t } = useTranslation();
  const { sActivityDetail, sActivityScoreComment, routerStore, sEditStudentActDoc } = useStore();

  const sActivityId = routerStore.routerState.params.activityId;
  const activityId = !sActivityId ? NaN : Number(sActivityId);
  useEffect(() => {
    if (Number.isNaN(activityId) || activityId < 1) {
      if (history.length > 1) history.back(); else routerStore.goTo("home");
      return;
    }
    ((async () => {
      const err = await sActivityDetail.init(activityId);
      if (err) return err;
      const err2 = await sEditStudentActDoc.init(activityId);
      if (err2) return err2;
      return;
    })()).then(err => {
      if(sActivityDetail.activity != null && sActivityDetail.activity.type == ActivityType.Activity){
        routerStore.goToNotFound();
      }
      else{
        if (!err) return;
        Modal.error({
          content: err.message,
          onOk() {
            if (history.length > 1) history.back(); else routerStore.goTo("home");
          }
        })
      }
    });

    window.scrollTo(0, 0);
    return sActivityDetail.exit;
  }, [activityId]);

  const isNoAnswer = useCallback(() => {
    return !(!sActivityDetail.allowSubmission && (sActivityDetail.score && sActivityDetail.score.score != null));
  }, [sActivityDetail]);

  if (sActivityDetail.activity == null) return (<Skeleton />);

  return (<>
    <ActivityBreadcrumb aClass={sActivityDetail.aClass} activity={sActivityDetail.activity}>
    </ActivityBreadcrumb>
    <div className="flex items-start justify-between">
      <div className="header">
          <Title level={3}>{t('app.activities.actDoc.submissions')}</Title>
      </div>
      {(sActivityDetail.activity && sActivityDetail.activity.isGraded && sActivityDetail.score && sActivityDetail.score.score != null) && (
          <div>
            <label className="text-grey font-medium mr-xs">{t("app.students.detail.grade")}: </label>
            <span className="text-black heading-3">{sActivityDetail.score.score}</span>
          </div>
      )}
    </div>
    <Card>
      <div className="mb-lg font-size-md">
        <label className="text-black mr-xs">{t('app.activities.due')}</label>
        {sActivityDetail.activity.dateDue
          ? (<LongDateLabel value={sActivityDetail.activity.dateDue!} />)
          : (<span className="text-grey font-medium">{t('app.activities.noDueDate')}</span>)}
      </div>

      {sEditStudentActDoc.tDoc && (
        <Alert
          message={<>
              {t('app.activities.student.submit.warning')}
              {(sEditStudentActDoc.activity == null || (Date.now() < (sEditStudentActDoc.activity.dateCutoff || Infinity))) && (
                <Button onClick={sEditStudentActDoc.mergeNewActdoc} type="link">{t('app.activities.student.update')}</Button>
              )}
            </>}
          type="warning"
          showIcon
          closable
          onClose={sEditStudentActDoc.ignoreNewActDoc}
          className="mb-lg"
        />
      )}

      {isNoAnswer()
        ? sEditStudentActDoc.doc && (<StudentActDocEdit doc={sEditStudentActDoc.doc}/>)
        : sEditStudentActDoc.doc && (<StudentActDocView doc={sEditStudentActDoc.doc}/>)
      }

      <SubmissionButtons />

      <Divider />

      {sActivityDetail.score && (
        <ThreadView threadId={sActivityDetail.score.threadId}
          commentSeen={sActivityScoreComment.commentSeen}
          />
      )}
    </Card>
  </>)
});

const SubmissionButtons: FC<{}> = observer(({ }) => {
  const { t } = useTranslation();
  const { sActivityDetail, routerStore, sEditStudentActDoc } = useStore();

  const onCancel = useCallback(() => {
    if (history.length < 2) {
      const classId = sActivityDetail.activity ? sActivityDetail.activity.classId : undefined;
      routerStore.goTo(classId ? new RouterState("classDetail", { classId: String(classId) }) : new RouterState("classes"));
    } else history.back()
  }, []);

  const onSave = useCallback(async () => {
    const submitErr = await sActivityDetail.submit(false);
    if (submitErr) {
      Modal.error({ title: t("app.activities.actDoc.save.failed"), content: t("app.activities.student.save.failed") });
      return;
    }
    const saveErr = await sEditStudentActDoc.save();
    if (saveErr) {
      Modal.error({ title: t("app.activities.actDoc.save.failed"), content: (<>
        <div>{saveErr.message}</div>
        {t("app.activities.student.save.failed")}
      </>) });
      return;
    }

    message.success(t("app.activities.student.save.success"));
  }, []
  );
  const onSubmit = useCallback(async () => {
    const submitErr = await sActivityDetail.submit(true);
    if (submitErr) {
      Modal.error({ title: "submit.failed", content: t("app.activities.student.submit.failed") });
      return;
    }
    const saveErr = await sEditStudentActDoc.save();
    if (saveErr) {
      Modal.error({ title: "submit.failed", content: t("app.activities.student.submit.failed") });
      return;
    }

    message.success(t("app.activities.student.submit.success"));
  }, []);

  if (sActivityDetail.activity == null) return (<Skeleton />);

  return (
    <div>
      <div>
        <Divider />
        <Button className="mr-sm" onClick={onCancel}>{t('app.activities.student.cancel')}</Button>
        {!sActivityDetail.isSubmitted && (
          <Button className="mr-sm" onClick={onSave} type="primary" disabled={!sActivityDetail.allowSubmission}>{t('app.activities.student.save')}</Button>
        )}
        <Button className="mr-sm" onClick={onSubmit} type="primary" disabled={!sActivityDetail.allowSubmission}>
          {(sActivityDetail.isSubmitted) ? t('app.activities.student.re-submit') : t('app.activities.student.submit')}
        </Button>
      </div>
      {!sActivityDetail.allowSubmission && (
        <div className="mt-sm">
          <span>{t('app.activities.student.thisAssignmentIsBlocked')}</span><Divider type="vertical" />
          <label className="text-black mr-xs">{t('app.activities.student.submissionCutOffTime')}</label>
          <LongDateLabel value={sActivityDetail.activity.dateCutoff!} />
        </div>
      )}
      {sActivityDetail.submission != null && sActivityDetail.submission.dateUpdated != 0 && (
        <div className="mt-lg mb-lg">
          <span className="font-medium text-black mb-xs">{(sActivityDetail.isReSubmitted) ? t('app.activities.student.re-submit') : t('app.activities.student.submit')} <LongDateLabel value={sActivityDetail.submission.dateUpdated} /></span>
          <time dateTime={new Date(sActivityDetail.submission.dateUpdated).toJSON()} hidden={sActivityDetail.activity.dateDue == null}
            title={new Date(sActivityDetail.submission.dateUpdated).toLocaleString()}>
            {
              (sActivityDetail.activity.dateDue == null || sActivityDetail.submission.dateUpdated <= sActivityDetail.activity.dateDue)
              ? (<span> - <span className="font-medium text-success">{t('app.activities.score.onTime')}</span></span>)
              : (<span> - <span className="font-medium text-danger">{t('app.activities.score.Late')}</span></span>)
            }
          </time>
        </div>
      )}
    </div>
  );
});

export const SubmissionDetail: FC<{ submission?: Submission }> = observer(({ submission }) => {
  const {t} = useTranslation();
  const { sActivityDetail } = useStore();

  if (!submission) return null;

  return (<>
    <HtmlInput value={submission.content} onChange={submission.set_content} />
    <div className="paddingTop-sm paddingBottom-lg">
      <Upload3 fileList={sActivityDetail.attachments}
        onChange={sActivityDetail.changeAttachments}
        onRemove={sActivityDetail.removeAttachment}
        multiple>
        <div className="flex">
          <div>
            <Icon type="upload" className="icon-lg paddingRight-xs" />
            <span>{t('app.activities.student.uploadAttachment')}</span>
          </div>
          <span className="text-grey paddingLeft-lg">({t('app.activities.student.filesAllowed')} jpg, png, doc, docx, xls, xlsx, mov, avi, mp4)</span>
        </div>
      </Upload3>
    </div>

  </>)
});
