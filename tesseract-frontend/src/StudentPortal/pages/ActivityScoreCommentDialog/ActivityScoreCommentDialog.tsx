import React, { FC, useCallback } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { useStore } from '../../stores';

import { Modal } from 'antd';

import { ThreadView } from '../../components/thread/ThreadView';

export const ActivityScoreCommentDialog: FC<{}> = observer(() => {
    const { t } = useTranslation();
    const store = useStore();
    const { sActivityScoreComment, sActivityScoreCommentDialog } = store;

    const onCancel = useCallback(() => {
        sActivityScoreCommentDialog.close(false);
    }, [])

    if (sActivityScoreCommentDialog.activity == null) return null;

    return (
        <Modal visible={!!sActivityScoreCommentDialog.activity}
            title={sActivityScoreCommentDialog.activity.title}
            footer={null}
            onCancel={onCancel}
        >
            <ThreadView threadId={sActivityScoreComment.threadId}
                commentSeen={sActivityScoreComment.commentSeen}
                />
        </Modal>
    );
});
