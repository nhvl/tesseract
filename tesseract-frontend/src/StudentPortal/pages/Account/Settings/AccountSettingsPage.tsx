import React, { FC } from 'react';
import { observer } from "mobx-react";

import { useStore } from '../../../stores';

import { BasicLayout } from '../../../layouts/BasicLayout';
import { Info } from '../../../../components/Account/Settings/Info';

export const AccountSettingsPage: FC = observer(() => {
  const {sAccountSetting} = useStore();

  return (
    <BasicLayout>
      <Info store={sAccountSetting} />
    </BasicLayout>
  );
});
