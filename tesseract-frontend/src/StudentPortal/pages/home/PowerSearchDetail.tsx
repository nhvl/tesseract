import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { BasicLayout } from '../../layouts/BasicLayout';
import { InConstructionLabel } from '../../../components/Misc/InConstructionLabel';
import { useTranslation } from 'react-i18next';

export const PowerSearchDetail: FC<{}> = observer(({ }) => {
    const {t} = useTranslation();
    
    return (<BasicLayout>
            <InConstructionLabel title={t('app.home.powerSearchDetailIsInConstruction')}/>
        </BasicLayout>);
});



