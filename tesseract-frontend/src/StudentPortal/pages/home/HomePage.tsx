import React, { FC, useEffect, useCallback, useMemo } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DbIdentity      } from "../../../models/types";
import { Activity, ActivityType        } from "../../../models/Activity";
import { StudentClass    } from "../../models/StudentClass";
import { StudentActivity } from "../../models/StudentActivity";

import { useStore } from "../../stores";

import { Card, Table, Typography, Badge, Icon } from "antd";
import { ColumnProps } from "antd/lib/table";
const { Title } = Typography;

import {DateFromNowToolTip} from "../../../components/DateTime";
import { Link } from "../../../components/router/Links";
import { BasicLayout } from "../../layouts/BasicLayout";
import { CurrentClassList } from "../../components/Class/CurrentClassList";
import { showError } from "../../../services/api/ShowError";

export const HomePage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <HomeBase/>
    </BasicLayout>);
});

const HomeBase: FC<{}> = observer(({}) => {
    const store = useStore();
    const {t} = useTranslation();

    useEffect( () => {
        if (!store.currentUser) return;
        store.fetchActivities({}).then(([err]) => {
            if (err) showError(err, t);
        });
    }, []);

    const overdueActs = useMemo(() => store.sActivity.items.filter(x => x.isOverdue && x.type!=ActivityType.Discussion), [store.sActivity.items]);
    const upcomingActs = useMemo(() => store.sActivity.items.filter(x => !x.isOverdate && x.type!=ActivityType.Discussion).sort(Activity.sorter.dateDue), [store.sActivity.items]);

    return (
        <div>
            <div><Notifications count={overdueActs.length}/></div>
            <div><ActivitiesCard title={t("app.students.header.overdateActivities")} acts={overdueActs} classes={store.tClass}/></div>
            <div><ActivitiesCard title={t("app.students.header.upcomingActivities")} acts={upcomingActs} classes={store.tClass}/></div>
            <div><CurrentClassList /></div>
        </div>
    );
});

const ActivitiesCard: FC<{title: string, acts: StudentActivity[], classes: StudentClass[]}> = observer(({title, acts, classes}) => {
    const {t} = useTranslation();
    const store = useStore();
    const dateRender = useCallback((d:number) => ((d == null || d <= 0) ? t("app.students.detail.DateNA") : (<DateFromNowToolTip value={d} />)), [t]);
    const classRender = useCallback((classId:DbIdentity) => {const cls = store.mClass.get(classId); return cls ? cls.className : t("app.students.detail.DateNA");}, [store.tClass]);
    const teacherRender = useCallback((classId:DbIdentity) => {const cls = store.mClass.get(classId); return cls ? cls.facultyNames : t("app.students.detail.DateNA");}, [store.tClass]);

    const columns = useMemo<Array<ColumnProps<StudentActivity>>>(() => [
        { dataIndex: 'classId', key: 'class', title: t("app.classes.list.className"), render: classRender },
        { dataIndex: 'classId', key: 'faculty', title: t("app.classes.list.teacher"), render: teacherRender },
        { dataIndex: 'title', key: 'title', title: t("app.students.detail.activity"), className: "whitespace-normal min-w",
            render: (title:string, c:StudentActivity) => (<Link routeName={ c.type == ActivityType.Activity ? "viewActDoc" : "activityDetail"} params={c.params}>{c.title}</Link>)},
        { dataIndex: 'dateDue', key: 'dateDue', title: t("app.students.detail.activtyDue"), render: dateRender},
    ], [t]);

    return (
        <>
            <div className="paddingBottom-lg">
                <Card title={title}>
                    <div className="responsiveTable">
                        <Table columns={columns} dataSource={acts} pagination={false} rowKey={rowKey} />
                    </div>
                </Card>
            </div>
        </>
    );
})

const Notifications: FC<{count: number}> = observer(({count}) => {
    const {t} = useTranslation();
    const {currentUser} = useStore();
    return (
        <>
            <div className="header">
                <Title level={3}>{t('app.students.welcome', { fullName: currentUser.fullName })}</Title>
            </div>
            <div className="paddingBottom-lg">
                <Card title={t("app.students.header.notifications")}>
                    <div className="flex justify-between items-center flex-wrap font-medium">
                        <div className="paddingBottom-lg paddingRight-sm text-danger">
                            <span className="paddingRight-xs">{t('app.students.notifications.missingAssignment')}</span>
                            <Badge count={count}><Icon type="warning" className="icon-lg" /></Badge>
                        </div>
                        <div className="paddingBottom-lg paddingRight-sm">
                            <span className="paddingRight-xs">{t('app.students.notifications.newAssignments')}</span>
                            <Badge count={2}><Icon type="inbox" className="icon-lg" /></Badge>
                        </div>
                        <div className="paddingBottom-lg paddingRight-sm">
                            <span className="paddingRight-xs">{t('app.students.notifications.message')}</span>
                            <Badge count={8}><Icon type="message" className="icon-lg" /></Badge>
                        </div>
                    </div>
                </Card>
            </div>
        </>
    );
});

function rowKey(record:StudentActivity) { return String(record.activityId) }

