import React, { FC, useEffect, useCallback } from 'react';
import { observer } from 'mobx-react';

import { ActivityDetail } from '../../../models/Activity';

import { useStore } from '../../stores';

import { Card, Row, Col, Divider, Typography, Select, Icon } from 'antd';

import { Link } from '../../../components/router/Links';

import { DateFromNowToolTip, useDateTrans, DateFromNowResponsive } from "../../../components/DateTime";

import { useTranslation } from 'react-i18next';

import styles from './ActivityToGrade.module.less';
import { Modal } from '../../../components/Modal/ModalAntd';

const { Title } = Typography;
const Option = Select.Option;

export const ActivityToGrade: FC<{}> = observer(({}) => {
    const store = useStore();
    const {sActivityToGrade} = useStore();
    const {t} = useTranslation();
    const facultyId = store.currentUser!.facultyId;
    useEffect(() => {
        sActivityToGrade.doFetch(facultyId).then(([err, data]) => {
            if (err) {
                Modal.error({ title: "getActivityToGrade.failed", content: err.message });
                return;
            }
            sActivityToGrade.set_activityDetail(data);
        });
    },[facultyId]);
    const handleOrderChange = useCallback(async (v: boolean) => {
        sActivityToGrade.set_order(v);
    }, [sActivityToGrade]);
    return (
        <div>
            <div className={styles.header}>
                <Title level={3}>{t("dashboard.haveActivities")}</Title>
            </div>
            <div className={styles.orderBy}>
                <strong>Order by: </strong>
                <Select value={sActivityToGrade.isASCOrder} onChange={handleOrderChange} size="large">
                    <Option value={true}>Ascending</Option>
                    <Option value={false}>Descending</Option>
                </Select>
            </div>
            <Card>
                {sActivityToGrade.activityDetail.map((data:ActivityDetail, index:number) => {

                    return (data.submisstionCount!=0 && data.unGradeCount!=0) ? (
                    <div>
                        <Row gutter={16} key={index}>
                            <Col xs={24} sm={12} className={styles.textBold}><Link routeName="activityDetail" params={data.params}>{data.title}</Link></Col>
                            <Col xs={24} sm={12}><Link routeName="submission" params={data.params}>Submissions({data.unGradeCount}/{data.submisstionCount})</Link></Col>
                        </Row>
                        <div className="m-4"></div>
                        <Row>
                            <Col xs={24} sm={24}>
                                <div className={styles.paddingBottom}><span className={styles.labelW}>{data.class.period}</span> {data.class.className}</div>
                                <div className={`${styles.paddingBottom} ${styles.dueDate}`}>
                                    <label className={styles.labelW}>Due </label>
                                    {data.dateDue != null && (
                                        <span><DateFromNowResponsive value={data.dateDue} breakAt="xl" /></span>
                                    )}
                                </div>
                                <Divider />
                            </Col>
                        </Row>
                    </div>
                    ) : ("")
                })}
            </Card>
        </div>
    );
})
