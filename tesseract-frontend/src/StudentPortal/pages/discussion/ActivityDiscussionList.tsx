import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import {showError} from "../../../services/api/ShowError";

import { Card, Breadcrumb, Typography, } from "antd";
const {Title} = Typography;

import { ClassActivityBreadcrumb } from "../../components/ClassActivityBreadcrumb";

import { DiscussionList } from "./DiscussionList";

export const ActivityDiscussionList: FC<{}> = observer(({}) => {
    const {sActivityDiscussionList, routerStore} = useStore();
    const {t} = useTranslation();

    const {activityId} = routerStore.routerState.params;
    const aId = !activityId ? NaN : Number(activityId);
    useEffect(() => {
        if (Number.isNaN(aId)) return;
        sActivityDiscussionList.init({activityId: aId}).then(err => {
            if (err) {
                showError(err, t);
                return;
            }
        });
    }, [aId]);

    const {activity, aClass} = sActivityDiscussionList;

    return (<>
        <ClassActivityBreadcrumb aClass={aClass} activity={activity} activityRouteName="activityDetail">
        </ClassActivityBreadcrumb>
        <div className="header">
            <Title level={3}>{t('app.classes.discussions.discussions')}</Title>
        </div>

        <Card>
            <DiscussionList items={sActivityDiscussionList.displayItems}
                sDiscussionList={sActivityDiscussionList} />
        </Card>
    </>);
});
