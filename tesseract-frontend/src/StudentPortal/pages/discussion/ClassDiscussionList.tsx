import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import {showError} from "../../../services/api/ShowError";

import {DiscussionList} from "./DiscussionList";

export const ClassDiscussionList: FC<{}> = observer(({}) => {
    const {sClassDiscussionList, routerStore} = useStore();
    const {t} = useTranslation();

    const {classId} = routerStore.routerState.params;
    const cId = !classId ? NaN : Number(classId);
    useEffect(() => {
        if (Number.isNaN(cId)) return;
        sClassDiscussionList.init({classId: cId}).then(err => {
            if (err) {
                showError(err, t);
                return;
            }
        });
    }, [cId]);

    return (<>
        <DiscussionList items={sClassDiscussionList.displayItems}
            sDiscussionList={sClassDiscussionList} />
    </>);
});
