import React, { FC } from "react";
import { observer } from "mobx-react";

import { BasicLayout } from "../../layouts/BasicLayout";
import {ActivityDiscussionList} from "./ActivityDiscussionList";

export const ActivityDiscussionListPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <ActivityDiscussionList />
    </BasicLayout>);
});
