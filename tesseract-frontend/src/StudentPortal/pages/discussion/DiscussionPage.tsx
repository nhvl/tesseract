import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Card, Spin, Breadcrumb, Typography, } from "antd";
const {Title} = Typography;

import {showError} from "../../../services/api/ShowError";

import { Link } from "../../../components/router/Links";
import { ClassActivityBreadcrumb } from "../../components/ClassActivityBreadcrumb";
import { DiscussionDetail } from "../../../components/Discussion/DiscussionDetail";
import { BasicLayout } from "../../layouts/BasicLayout";
import {ThreadView} from "../../components/thread/ThreadView";

export const DiscussionPage: FC<{}> = observer(({}) => {
    const {sDiscussionThread, routerStore} = useStore();
    const {t} = useTranslation();

    const {classId, discussionId} = routerStore.routerState.params;
    const cId = !classId ? NaN : Number(classId);
    const dId = !discussionId ? NaN : Number(discussionId);
    useEffect(() => {
        if (Number.isNaN(cId)) return;
        sDiscussionThread.init({classId: cId, discussionId:dId}).then(err => {
            if (err) {
                showError(err, t);
                return;
            }
        });
        return () => sDiscussionThread.reset({classId: cId, discussionId:dId});
    }, [cId, dId]);

    return (<BasicLayout>
        <DiscussionThread />
    </BasicLayout>);
});

const DiscussionThread: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const {sDiscussionThread} = useStore();
    const {discussion, aClass, activity, commentSeen} = sDiscussionThread;

    if (discussion == null) return (<Spin />);

    const readOnly = discussion.endTime != null && (discussion.endTime < Date.now());

    return (<>
        <ClassActivityBreadcrumb aClass={aClass} activity={activity} activityRouteName="activityDetail">
            <Breadcrumb.Item>
                {activity ? (
                    <Link routeName="activityDiscussion" params={activity.params}>{t('app.classes.discussions.discussions')}</Link>
                ) : (aClass ? (
                    <Link routeName="discussionList" params={aClass.params}>{t('app.classes.discussions.discussions')}</Link>
                ) : (
                    t('app.classes.discussions.discussions')
                ))}
            </Breadcrumb.Item>
            <Breadcrumb.Item>{discussion.title}</Breadcrumb.Item>
        </ClassActivityBreadcrumb>

        <Card>
            <div className="border-bottom mb-lg">
                <DiscussionDetail discussion={discussion} />
            </div>

            <ThreadView threadId={discussion.threadId}
                commentSeen={commentSeen}
                readOnly={readOnly}
                />
        </Card>
    </>);
});
