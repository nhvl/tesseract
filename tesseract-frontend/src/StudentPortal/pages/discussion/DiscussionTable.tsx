import React, { FC, useEffect, useMemo } from "react";
import { observer } from "mobx-react";

import { Discussion } from "../../../models/Discussion";

import { Table } from "antd";
import { ColumnProps } from "antd/lib/table";

import { Link } from "../../../components/router/Links";
import { useTranslation } from "react-i18next";


export const DiscussionTable: FC<{items:Discussion[]}> = observer(({items}) => {
    const {t} = useTranslation();
    const columns = useMemo<ColumnProps<Discussion>[]>(() => [
        { title: t('app.classes.discussions.title'), key:"title", dataIndex:"title", sorter:Discussion.sorter.title,
            render:(_, item) => (<Link routeName="discussion" params={item.params}>{item.title}</Link>) },
    ], []);

    return (
        <div className="responsiveTable">
            <Table columns={columns} dataSource={items} pagination={false} rowKey={rowKey} />
        </div>
    );
});

function rowKey(record:Discussion) { return String(record.discussionId) }
