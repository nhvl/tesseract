import React, { FC } from "react";
import { observer } from "mobx-react";

import { Discussion } from "../../../models/Discussion";

import { Empty, } from "antd";

import { DiscussionListStore } from "../../stores/DiscussionListStore";

import { DiscussionItem } from "../../../components/Discussion/DiscussionItem";

export const DiscussionList: FC<{items:Discussion[], sDiscussionList: DiscussionListStore}> = observer(({items, sDiscussionList}) => {
    return (
        <>{ items.length > 0 ?
            items.map(item => (
            <DiscussionItem item={item} key={item.discussionId}
                unread={sDiscussionList.threadUnread.get(item.threadId) || 0}
            />
            ))
            : <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}/>
    }</>
    );
});
