import React, { FC, useMemo } from 'react';
import { observer } from 'mobx-react';

import { Breadcrumb } from 'antd';

import { Link } from '../../components/router/Links';

import { Activity } from '../../models/Activity';
import { Class } from '../../models/Class';
import { ClassLink } from '../pages/classes/ClassLink';

export const ActivityBreadcrumb:FC<{
    aClass  ?: Class,
    activity?: Activity,

}> = observer(({aClass, activity, children}) => {
    return (
        <div className="breadcrumb">
            <Breadcrumb>
                {aClass && (<Breadcrumb.Item>
                    <ClassLink routeName="classDetail" classId={aClass.classId}>{aClass.className}</ClassLink>
                </Breadcrumb.Item>)}
                {activity && (<Breadcrumb.Item>
                    <Link routeName="activitySettings" params={activity.params}>{activity.title}</Link>
                </Breadcrumb.Item>)}
                {children}
            </Breadcrumb>
        </div>
    );
});
