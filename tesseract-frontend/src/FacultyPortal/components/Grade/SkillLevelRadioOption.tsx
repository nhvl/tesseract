import React, { FC, useEffect, useCallback, useMemo, useState } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';
import { ActivityScoreInfo, ActivityScore, FiveC, FiveCValues } from "../../../models/ActivityScore";

import { ScoreBadge } from "../../../models/ScoreBadge";

import { Button, Card, Radio} from 'antd';

import { FiveCValueDesc } from '../../../components/FiveCTitle';

export const SkillLevelRadioOption:FC<{
    record:ActivityScore,
    cType:FiveC,
    onSave: () => void,
    onCancel: ()=> void,
}>  = observer(({record, cType, onSave, onCancel}) => {
    const {t} = useTranslation();
    const [radioValue, setRadioValue] = useState(FiveCValues.Unknown);
    useEffect(()=>{
        setRadioValue(record.scoreBadge(cType));
    },[record, cType]);


    return (<Card title={t("app.activities.score.setLevel", {skill: (cType == null) ? "" : cType})}>
            <Radio.Group value={radioValue} onChange={(e) => {setRadioValue(e.target.value);}}>
                <Radio value={FiveCValues.Low   }><FiveCValueDesc value={FiveCValues.Low   } /></Radio>
                <Radio value={FiveCValues.Medium}><FiveCValueDesc value={FiveCValues.Medium} /></Radio>
                <Radio value={FiveCValues.High  }><FiveCValueDesc value={FiveCValues.High  } /></Radio>
            </Radio.Group>
            <div className="mt-lg">
                <Button onClick={onCancel} className="mr-sm">{t("form.cancel")}</Button>
                <Button type="primary" onClick={()=>{record.set_scoreBadge(FiveCValues.Unknown, cType); onSave();}}
                    disabled={record.scoreBadge(cType) == FiveCValues.Unknown}
                    className="mr-sm">{t("app.activities.score.skill.remove")}</Button>
                <Button type="primary" onClick={()=>{record.set_scoreBadge(radioValue, cType); onSave()}}
                    disabled={record.scoreBadge(cType) == radioValue}>{t("form.save")}</Button>
            </div>
        </Card>);
});
