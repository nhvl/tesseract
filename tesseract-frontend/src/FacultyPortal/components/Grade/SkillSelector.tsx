import React, { FC, useEffect, useCallback, useMemo, useState } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';
import { ActivityScoreInfo, ActivityScore, FiveC, FiveCValues } from "../../../models/ActivityScore";
import {Select} from 'antd';
import styles from "./SkillSelector.module.less";
export const SkillSelector: FC<{options:FiveC[], selected:FiveC|undefined, onSelect:(cType:FiveC) => void}> = observer(({options, selected, onSelect}) => {
    const {t} = useTranslation();
    return (<Select className={styles.select5C} onChange={onSelect} placeholder={t("app.activities.score.skill.add")} value={selected}>
                {options.length > 0 && options.map((item) => (
                    <Select.Option key={`${item}`} value={item}>
                        {ActivityScore.getFiveCLocale(item, t)}
                    </Select.Option>))}
            </Select>);
});
