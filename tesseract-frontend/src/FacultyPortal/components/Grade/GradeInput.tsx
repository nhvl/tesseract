import React, { FC, KeyboardEvent } from "react";
import { observer } from 'mobx-react';

import {IInputNumberProps, InputNumber} from "../../../components/inputs/antd/InputNumber"

const ignoreKeys = ["-", "+", "e"];

export const GradeKeyDownRestriction = (event:KeyboardEvent<HTMLInputElement>) => {
    if (ignoreKeys.includes(event.key)) event.preventDefault();
};

export const GradeInput: FC<IInputNumberProps> = observer((props) => (
    <InputNumber
        min={0}
        step={0.01}
        onKeyDown={GradeKeyDownRestriction}
        {...props}/>
));
