import React from "react";
import { observer } from "mobx-react";

import { useStore } from "../../stores";

import { Icon, Spin } from "antd";

import { RightContent } from "../../../components/GlobalHeader/RightContent";
import { PowerSearch } from "../../../components/PowerSearch";

import styles from "./index.module.less";

export const GlobalHeader = observer(() => {
    const store = useStore();
    const { isMobile, sLeftNav, schoolLogo } = store;

    return (
        <div className={styles.header}>
            {isMobile && (schoolLogo ?
                (<span className={styles.trigger} onClick={sLeftNav.toggleLeftNavCollapsed}>
                    <Icon type={sLeftNav.collapsed ? "menu-unfold" : "menu-fold"} />
                    <span className={styles.logo}><img src={schoolLogo} alt="logo" width="32" /></span>
                </span>)
                :
                (<Spin size="large" />)
            )}
            <span className={`${styles.search} search`}>
                <PowerSearch store={store} />
            </span>
            <RightContent />
        </div>
    );
});




