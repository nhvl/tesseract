import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";
import { useStore } from "../../stores";
import {showError} from "../../../services/api/ShowError";
import { DefaultId, DbIdentity } from "../../../models/types";
import {ThreadView} from "../../components/thread/ThreadView";
import { Spin } from "antd";
import {DiscussionDetail} from "../../../components/Discussion/DiscussionDetail";

export const DiscussionThread: FC<{classId: DbIdentity, discussionId: DbIdentity, readonly?:boolean}> = observer(({classId, discussionId, readonly}) => {
    const {t} = useTranslation();
    const {sDiscussionThread} = useStore();
    const {discussion} = sDiscussionThread;

    useEffect(() => {
        if (Number.isNaN(classId)) return;
        sDiscussionThread.init({classId: classId, discussionId:discussionId}).then(err => {
            if (err) {
                showError(err, t);
                return;
            }
        });
        return () => sDiscussionThread.reset({classId: classId, discussionId:discussionId});
    }, [classId, discussionId]);

    if (discussion == null) return (<Spin />);

    const readOnly = (discussion.endTime != null && discussion.endTime < Date.now()) || readonly;

    return (<>
            <div className="border-bottom mb-lg">
                <DiscussionDetail discussion={discussion} />
            </div>

            <ThreadView threadId={discussion.threadId}
                readOnly={readOnly}
                commentSeen={sDiscussionThread.commentSeen} />
        </>);
});
