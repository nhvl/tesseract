import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";

import { useStore } from "../../stores";

import {ThreadView as GeneralThreadView} from "../../../components/thread/ThreadView";
import { CommentSeen } from "../../../models/CommentSeen";

export const ThreadView: FC<{
    threadId     : string,
    readOnly    ?: boolean,
    commentSeen ?: CommentSeen,
}> = observer(({threadId, readOnly, commentSeen}) => {
    const store = useStore();
    const {sNotification, currentUser} = store;

    useEffect(() => {
        if (!threadId) return;
        sNotification.on("ThreadChanged", handler);
        function handler() {
            store.fetchCommentsOfThread(threadId);
        }
        return () => sNotification.off("ThreadChanged", handler);
    }, [threadId]);

    return (
        <GeneralThreadView
            threadId={threadId}
            readOnly={readOnly}
            currentUser={currentUser}
            commentSeen={commentSeen}
            />
    );
});
