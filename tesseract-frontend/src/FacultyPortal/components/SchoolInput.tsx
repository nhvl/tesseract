import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { DbIdentity } from "../../models/types";

import { useStore } from "../stores";

import { Select, Dropdown, Menu, Icon } from 'antd';
import { SelectProps } from 'antd/lib/select';

export const SchoolName: FC<{id: DbIdentity}> = observer(({id}) => {
    const store = useStore();
    const s = store.mSchool.get(id);
    if (s == null) return null;

    return (
        <>{s.schoolName}</>
    );
});

export const SchoolField: FC<{id: DbIdentity, field:string}> = observer(({id, field}) => {
    const store = useStore();
    const s = store.mSchool.get(id);
    if (s == null) return null;

    return (
        <>{s[field]}</>
    );
});

export const SchoolSelect: FC<SelectProps<DbIdentity>> = observer((props) => {
    const store = useStore();

    return (
        <Select {...props}>{store.tSchool.map(s => (
            <Select.Option key={s.schoolId} value={s.schoolId}>{s.schoolName}</Select.Option>
        ))}</Select>
    );
});


export const SchoolSelectOrName: FC<SelectProps<DbIdentity>> = observer((props) => {
    const store = useStore();
    const {currentUser} = store;
    if (currentUser == null) return null;

    if (currentUser.schools.length < 1) return null;
    if (currentUser.schools.length < 2) return (<SchoolName id={props.value!} />);
    return (
        <Dropdown overlay={(
            <Menu>
                {currentUser.schoolList.map(s => (
                    <Menu.Item key={s.schoolId}
                        onClick={() => store.set_currentSchool(s.schoolId)}
                        >{s.schoolName}</Menu.Item>
                ))}
            </Menu>
        )}>
            <a className="ant-dropdown-link">{store.currentSchool && store.currentSchool.schoolName} <Icon type="down" /></a>
        </Dropdown>
    );
});
