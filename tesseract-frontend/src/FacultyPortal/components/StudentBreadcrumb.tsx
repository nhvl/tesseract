import React, { FC, useMemo, ReactNode } from 'react';
import { observer } from 'mobx-react';

import { DbIdentity } from '../../models/types';

import { Breadcrumb } from 'antd';

import { Link } from '../../components/router/Links';

export const StudentBreadcrumb:FC<{classId?:DbIdentity, studentId:DbIdentity, studentName:ReactNode}> = observer(({classId, studentId, studentName, children}) => {
    const params = useMemo(() => ({ classId:String(classId), studentId:String(studentId) }), [classId, studentId]);

    return (
        <div className="breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link routeName="students">Students</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link routeName={classId == null ? "studentDetail" : "studentDetailInClass"}
                        params={params}>{studentName}</Link>
                </Breadcrumb.Item>
                {children}
            </Breadcrumb>
        </div>
    );
});
