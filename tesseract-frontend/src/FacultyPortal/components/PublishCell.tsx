import React, { FC } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { PublishStatusEnum } from "../../models/Activity";

import { Button, Icon, Tooltip, Typography } from "antd";
const { Text } = Typography;

import { LongDateLabel, format } from "../../components/DateTime/DateTime";

interface IPublishCellProps {
    startDate: number | undefined,
    status: PublishStatusEnum,
    onClick: () => void
}

export const PublishCell: FC<IPublishCellProps> = observer(({startDate, status, onClick}) => {
    const { t } = useTranslation();
    const tooltipTitle = ((stat: PublishStatusEnum) => {
        switch(stat){
            case PublishStatusEnum.Unpublished:
                return null;
            case PublishStatusEnum.Pending:
                return t("app.activities.publish.tooltip.AutoSet");
            default:
                return startDate == null ? "N/A" : t("app.activities.publish.tooltip.Publishedon") + format(startDate, "L");
        }
    })(status);

    return  (<>
                <PublishStatusIcon status={status}/>
                <Tooltip title={tooltipTitle}>
                    <Button type="link" onClick={onClick}>
                        <Text>
                            { status == PublishStatusEnum.Unpublished
                                ? <span className="text-grey font-medium underline">{t("app.activities.publish.publishAction")}</span>
                                : (<LongDateLabel value={startDate}/>)
                            }
                        </Text>
                    </Button>
                </Tooltip>
            </>);
});


const PublishStatusIcon: FC<{status: PublishStatusEnum}> = observer(({status}) => {
    switch(status){
        case PublishStatusEnum.Unpublished:
            return (<Icon type="check-circle" theme="filled" className="icon-sm text-grey" />);
        case PublishStatusEnum.Pending:
            return (<Icon type="check-circle" theme="filled" className="icon-sm text-link" />);
        default:
            return (<Icon type="check-circle" theme="filled" className="icon-sm text-success" />);
    }
});
