import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { useStore } from "../stores";
import { Select } from 'antd';
import { SelectProps } from 'antd/lib/select';
import { DbIdentity } from '../../models/types';

export const GradeTermSelect: FC<SelectProps<DbIdentity>> = observer((props) => {
    const {currentSchool} = useStore();
    return (
        <Select {...props} loading={currentSchool == null}>{currentSchool != null && currentSchool.tGradingTerm.map(s => (
            <Select.Option key={s.gradingTermId} value={s.gradingTermId}>{s.name}</Select.Option>
        ))}</Select>
    );
});
