import React, { FC, CSSProperties, useMemo } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';
import { qs } from '../../../utils/url';

import { useStore } from '../../stores';

import { Layout, Spin, Icon, Tooltip, Avatar, Divider } from 'antd';
const {Sider} = Layout;

import { SideFooter } from '../../../components/SideFooter';

import { Link } from '../../../components/router/Links';
import { BaseMenu } from './BaseMenu';
import { SchoolSelectOrName } from '../SchoolInput';

import styles from './index.module.less';

const spinStyle:CSSProperties = { marginLeft: 8, marginRight: 8 };

export const SiderMenu: FC<{collapsed:boolean}> = observer(({collapsed}) => {
    const store = useStore();
    const {
        sLeftNav,
        currentUser, logout,
        currentSchoolId, set_currentSchool,
        currentSchool,
    } = store;
    const {t} = useTranslation();
    const schoolLogo = store.currentSchool ? store.currentSchool.logoUrl : "";

    const helpUrl = useMemo(() => qs("mailto:support@denovu.com", {
        cc     : undefined,
        bcc    : undefined,
        subject: t("menu.account.help.subject"),
        body   : `\n\n${currentUser ? `--\n${currentUser.firstName} ${currentUser.lastName} | ${currentUser.userId}` : ""}`,
    }).toJSON(), []);

    return (
        <Sider trigger={null} collapsible collapsed={collapsed}
            breakpoint="lg"
            width={256}
            className={`${styles.sider} slider ${styles.fixSiderBar}`} >
            <div className="slider-footer">
                <div className="menu">
                    <div className={`${styles.logo} logo`}>
                        <span className={`${styles.trigger} trigger`} onClick={sLeftNav.toggleLeftNavCollapsed}>
                            <Icon type={sLeftNav.collapsed ? 'menu-unfold' : 'menu-fold'} />
                        </span>
                        <Link routeName="home">
                            {schoolLogo
                                ? (<img src={schoolLogo} alt={t('menu.schools.logo')} />)
                                : (<Spin size="large" />)
                            }
                        </Link>
                    </div>
                    <div className={`${styles.infoAccount} infoAccount`}>
                        <Avatar size="large" className={styles.avatar} src={(currentUser ? currentUser.avatar : undefined)} alt={t('menu.account.avatar')} icon="user" />

                        {!sLeftNav.collapsed && (<>
                        <div className="paddingBottom-xs font-bold">
                            {currentUser ? (
                                <span>{currentUser.fullName}</span>
                            ) : (
                                <Spin size="small" style={spinStyle} />
                            )}
                        </div>
                        <div>
                            <SchoolSelectOrName value={currentSchoolId} onChange={set_currentSchool} />
                        </div>
                        <ul className={styles.listIcons}>
                            <li>
                                <Tooltip title={t('menu.account.settings')}>
                                    <Link routeName="accountSettings"><Icon type="setting" className={styles.large} /></Link>
                                </Tooltip>
                            </li>
                            <li>
                                <Tooltip title={t('menu.account.notifications')}>
                                    <a><Icon type="bell" className={styles.large} /></a>
                                </Tooltip>
                            </li>
                            <li>
                                <Tooltip title={t('menu.account.help')}>
                                    <a href={helpUrl}
                                        target="_blank" rel="noopener noreferrer" className={styles.action}>
                                        <Icon type="question-circle" className={styles.large} />
                                    </a>
                                </Tooltip>
                            </li>
                            <li>
                                <Tooltip title={t('menu.account.logout')}>
                                    <a onClick={logout}>
                                        <Icon type="logout" className={styles.large} />
                                    </a>
                                </Tooltip>
                            </li>
                        </ul>
                        </>)}
                        <div className={styles.marginLeftRight}>
                            <Divider/>
                        </div>
                    </div>
                    <BaseMenu />
                </div>
                <SideFooter collapsed={collapsed} />
            </div>
        </Sider>
    )
});

