import React, { FC, useEffect, CSSProperties, useCallback, useMemo, useState, MouseEvent } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import {DefaultId, DbIdentity, EUserRole} from "../../../models/types";

import { useStore } from "../../stores";

import { Menu, Icon, message, Spin, Divider } from "antd";
const { SubMenu } = Menu;

import { Link } from "../../../components/router/Links";
import { subMenu } from "../../../components/inputs/antd/subMenu";
import {GradeTermSelect} from "../GradeTermSelect"
import { ClassLink } from "../../pages/classes/ClassLink";
import { AsStudentClass } from "../../models/AsStudentClass";

const GradingTermSelector: FC<{}> = observer(() => {
    const store = useStore();
    const { sLeftNav } = useStore();

    const onChange = useCallback((v:DbIdentity) => {
        store.set_currentGradingTerm(v);
    }, [store]);

    return (sLeftNav.collapsed ? null : (
        <GradeTermSelect value={store.currentGradingTerm} onChange={onChange} className="w-full" />
    ));
});

const style: CSSProperties = { padding: 0, width: "100%" };

export const BaseMenu: FC<{}> = observer(() => {
    const [scrollMenuStyle, set_ScrollMenuStyle] = useState(undefined as CSSProperties|undefined);
    const {t} = useTranslation();
    const store = useStore();
    const { sNotification, sLeftNav } = useStore();
    const openAeries = useCallback(() => message.info(t("menu.openaeries.open")), [t]);

    useEffect(() => {
        sNotification.on("ClassUpdated", handler);
        function handler() {
            store.refresh();
        }
        return () => sNotification.off("ClassUpdated", handler);
    }, [])

    useEffect(() => {
        if (store.currentGradingTermClasses == null) store.refreshClasses(store.currentSchoolId);
        updateScrollMenu();
        window.addEventListener("resize", updateScrollMenu);
    }, []);

    const updateScrollMenu = useCallback(()=>{
        const item: CSSProperties = {height: "100%", maxHeight:(window.innerHeight - sLeftNav.nonMenuHeight), overflowY: "auto" }
        set_ScrollMenuStyle(item);
    },[window.innerHeight]);

    const selectedKeys = useMemo(() => [sLeftNav.selectedKey, `gradingTerm_${store.currentGradingTerm}`], [sLeftNav.selectedKey, store.currentGradingTerm]);
    const selectGradeTerm = (id:DbIdentity) => () => { store.set_currentGradingTerm(id) };

    return (<div style={scrollMenuStyle}>
        <Menu theme="dark" mode="inline" selectedKeys={selectedKeys}
            openKeys={sLeftNav.menuOpenKeys}
            onOpenChange={sLeftNav.set_menuOpenKeys}
            style={style}
            inlineIndent={18}
            className="main-nav"
            >
            {sLeftNav.collapsed ? (
                <SubMenu key="gradingTerm" title={<Icon type="ellipsis" />}>{
                    !store.currentSchool ? null : store.currentSchool.tGradingTerm.map(s => (
                        <Menu.Item key={`gradingTerm_${s.gradingTermId}`} onClick={selectGradeTerm(s.gradingTermId)}>{s.name}</Menu.Item>
                ))}</SubMenu>
            ) : (
                <Menu.Item key="gradingTerm" className="grading-term">
                    <GradingTermSelector />
                </Menu.Item>
            )}

            {...subMenu({
                collapsed       : sLeftNav.collapsed,
                subMenuClassName: "level-1",
                key             : "home",
                subMenuTitle    : (<Link routeName = "home"><Icon type = "home" /><span>{t("menu.home")}</span></Link>),
                menuItems       : [
                                    <Menu.Item key="openAssignments"><Link routeName="openAssignments"><span>{t("menu.openAssignments")}</span></Link></Menu.Item>,
                                    <Menu.Item key="pastAssignments"><Link routeName="pastAssignments"><span>{t("menu.pastAssignments")}</span></Link></Menu.Item>,
                                ],
            })}

            {...subMenu({
                collapsed       : sLeftNav.collapsed,
                subMenuClassName: "level-1",
                key             : "classes",
                subMenuTitle    : (<Link routeName="classes"><Icon type="read" /><span>{t("menu.classes")}</span></Link>),
                menuItems       : store.currentGradingTermClasses.map(c => (
                    <Menu.Item key={"class" + c.classId} title={`${c.period} ${c.className}`}>
                        <ClassLink routeName="classDetail" classId={c.classId}>
                            {c.period}
                            <Divider type="vertical"/>
                            {c.className}
                        </ClassLink>
                    </Menu.Item>
                )).concat([(
                    <Menu.Item key={"class" + DefaultId}>
                        <ClassLink routeName="classEditor" classId={DefaultId}><Icon type="plus" /><span>{t("app.classes.menu.addClass")}</span></ClassLink>
                    </Menu.Item>
                )]).concat(store.sAsStudentClass.currentGradingTermClasses.map(c => (
                    <Menu.Item key={"class" + c.classId} title={`${c.period} ${c.className}`}>
                        <AsStudentClassLink item={c}>
                            {c.period}
                            <Divider type="vertical"/>
                            {c.className}
                        </AsStudentClassLink>
                    </Menu.Item>
                ))),
            })}

            <Menu.Item key="students" className="level-1">
                <Link routeName="students"><Icon type="team" /><span>{t("menu.students")}</span></Link>
            </Menu.Item>

            <Menu.Item key="aeries" className="level-1">
                <a onClick={openAeries}>
                    <img className="anticon" src="https://ps.orangeusd.org/favicon.ico" alt={t("menu.openaeries")} />
                    <span>{t("menu.openaeries")}</span>
                </a>
            </Menu.Item>
        </Menu>
    </div>)
});

const AsStudentClassLink:FC<{item:AsStudentClass}> = observer(({item, children}) => {
    const store = useStore();
    const classLink = `/classes/${item.classId}`;
    const goToClass = useCallback(async (event: MouseEvent<HTMLAnchorElement>) => {
        event.preventDefault();
        const [err] = await store.changeRole(EUserRole.Student);
        if (err) console.error(err);
        else location.assign(classLink);
    }, [item]);

    return (
        <a href={classLink} onClick={goToClass}>
            {children}
        </a>)
})
