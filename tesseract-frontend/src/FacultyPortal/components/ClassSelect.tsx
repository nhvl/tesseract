import React, { ReactNode, FC, ReactElement, useMemo } from 'react';
import { observer } from 'mobx-react';

import { DbIdentity, DefaultId } from '../../models/types';
import { Class } from '../../models/Class';

import { useStore } from "../stores";

import { Select } from 'antd';
import { SelectProps } from 'antd/lib/select';

interface ClassSelectProps extends SelectProps<DbIdentity> {
    emptyLabel ?: string;
    gradingTerm?:DbIdentity;
}

export const ClassSelect: FC<ClassSelectProps> = observer(({emptyLabel, gradingTerm, ...props}) => {
    const store = useStore();
    const cs = useMemo(() => (gradingTerm == null || gradingTerm < 1) ? store.tClass
        : store.tClass.filter(c => c.gradingTerm == gradingTerm), [store.tClass, gradingTerm]);
    return (
        <Select {...props}>
            {!!emptyLabel && (<Select.Option key={DefaultId} value={DefaultId}>{emptyLabel}</Select.Option>)}
            {cs.map(c => (
                <Select.Option key={c.classId} value={c.classId}>{c.period}. {c.className}</Select.Option>
            ))}
        </Select>
    );
});

interface IClassFieldProps {
    id:DbIdentity;
    notFound?:ReactNode;
    field?:(((c:Class) => ReactNode)|string);
}
export const ClassField: FC<{id:DbIdentity, notFound?:ReactNode, field:(((c:Class) => ReactNode)|string)}> =
observer(({id, notFound = "", field}) => {
    const {mClass} = useStore();
    const c = mClass.get(id);
    return ((!c) ? notFound : (typeof field === "string" ? c[field] : field(c))) as ReactElement;
});

function classPeriod(c:Class) { return c.period }
export const ClassPeriod: FC<IClassFieldProps> =
observer((props) => (<ClassField {...props} field={classPeriod} />));

function className(c:Class) { return c.className }
export const ClassName: FC<IClassFieldProps> =
observer((props) => (<ClassField {...props} field={className} />));
