import React, { FC, useCallback } from "react";
import { observer } from "mobx-react";

import { Activity } from "../../../models/Activity";

import { useStore } from "../../stores";

import { Link } from "../../../components/router/Links";
import { ResponsiveActions } from "../../../components/ResponsiveTable/ResponsiveActions";
import { useTranslation } from "react-i18next";

export const ActivityActions: FC<{item:Activity}> = observer(({item}) => {
    const {t} = useTranslation();
    const {sActivityDup,  mediaBreakpoint} = useStore();
    const onDuplicate = useCallback(() => {
        sActivityDup.showModal(item.activityId)
    }, [item.activityId]);

    return (<ResponsiveActions breakpoint={mediaBreakpoint} md={5}>
        <Link routeName="editActDoc" params={item.params}>{t('app.classes.activity.tree.editContent')}</Link>
        <Link routeName="viewActDoc" params={item.params}>{t('app.classes.activity.tree.preview')}</Link>
        {item.isGraded && (
            <Link routeName="gradeDetail" params={item.params}>{t('app.classes.activity.tree.submissions')}</Link>
        )}
        <Link routeName="activityDiscussion" params={item.params}>{t('app.classes.activity.tree.discussions')}</Link>
        <a onClick={onDuplicate}>{t('app.classes.activity.tree.duplicate')}</a>
    </ResponsiveActions>);
});
