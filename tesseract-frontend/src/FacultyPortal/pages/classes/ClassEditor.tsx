import React, { FC, FormEvent, useCallback, useEffect, useState } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DefaultId } from "../../../models/types";
import {GradeRange} from "../../../models/GradeRange";

import { useStore } from "../../stores";

import { Form, Button, Card, Row, Modal as AntdModal, Col, message, Spin, Divider, Breadcrumb, Icon, Checkbox } from 'antd';
import { TextAreaProps } from 'antd/lib/input/TextArea';
const FormItem = Form.Item;

import { Input, TextArea } from "../../../components/inputs/antd/Input";
import { InputNumber } from "../../../components/inputs/antd/InputNumber";
import { FormSelect } from "../../../components/inputs/FormSelect";
import { Link } from "../../../components/router/Links";

import { GradeTermSelect } from "../../components/GradeTermSelect";
import { GradeRangeModal } from "../../../components/GradeRangeModal";

import styles from "./ClassEditor.module.less";
import { Modal } from "../../../components/Modal/ModalAntd";

const MaxLengthPeriod = 10;
const MaxLengthClassName = 50;
const MaxLengthClassDescription = 500;
const ClassDescriptionAutoSize: TextAreaProps["autosize"] = ({ minRows: 4, maxRows: 8 });

export const ClassEditor: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const store = useStore();
    const {routerStore, sClassEditor} = store;

    const sId = routerStore.routerState.params.classId;
    useEffect(() => {
        let classId = !sId ? DefaultId : Number(sId);
        if (Number.isNaN(classId)) { classId = DefaultId }

        store.sClassEditor.init(classId).then(err => {
            if (!err) return;
            Modal.error({content: err.message, onOk() {
                if (history.length > 1) history.back();
            }});
        });
    }, [sId]);

    const handleSubmit = useCallback(async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (sClassEditor.aClass == null) return;
        let errorMessage = Object.values(sClassEditor.validateStatuses).filter(Boolean).join("\n");
        if (!errorMessage && sClassEditor.aClass.gradingTerm < 1) {
            errorMessage = t('app.classes.gradingTermIsRequired');
        }
        if (!!errorMessage) {
            alert(errorMessage);
            return;
        }

        sClassEditor.refineGradeRanges();
        for(var i = 1; i < sClassEditor.aClass.gradeRanges.length;i++){
            if(sClassEditor.aClass.gradeRanges[i].percentGrade >= sClassEditor.aClass.gradeRanges[i-1].percentGrade){
                message.error(t("app.classes.inOrderPercent"));
                return;
            }
            if(sClassEditor.aClass.gradeRanges[i].percentGrade <= 0){
                message.error(t("app.classes.invalidPercent"));
                return;
            }
        }


        const [err, data] = await sClassEditor.doSave();
        if (err) {
            Modal.error({ title: t("app.activities.actDoc.save.failed"), content: err.message });
        } else {
            setTimeout(message.success(sClassEditor.isCreateNew
                ? t("app.classes.createSuccess", {className: data.className})
                : t("app.classes.updateSuccess", {className: data.className}), 0), 2000);
            store.refreshClasses();
            routerStore.goTo("classDetail", {classId : String(data.classId)});
        }
    }, []);

    const handleClose = useCallback(async () => {
        if (history.length > 1) {
            history.back();
        } else {
            routerStore.goTo("classes");
        }
    }, []);

    const onCancelGradeRangeModal = useCallback(() => { sClassEditor.set_isOpenGradeRangeModal(false); }, [sClassEditor]);
    const onSubmitGradeRangeModal = useCallback(() => {
        if(!sClassEditor.aClass!.useDefaultGradesRange){
            sClassEditor.aClass!.gradeRanges = sClassEditor.modalGradeRange.slice();
        }
        sClassEditor.set_isOpenGradeRangeModal(false);
    },[sClassEditor]);

    if (sClassEditor.aClass == null) return (<Spin />);

    return (
        <div className={styles.main}>
            <div className="breadcrumb">
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <Link routeName="classes">{t("app.classes.allClass")}</Link>
                    </Breadcrumb.Item>
                    { (sClassEditor.aClass.isTemplate) ?
                        <Breadcrumb.Item>
                            <Link routeName="classTemplates">{t("app.classes.templates")}</Link>
                        </Breadcrumb.Item>
                        : null
                    }
                    <Breadcrumb.Item>{sClassEditor.isCreateNew ? t("app.classes.createClass") : sClassEditor.origClassName}</Breadcrumb.Item>
                </Breadcrumb>
            </div>
            <Card>
                <Form onSubmit={handleSubmit}>
                    <Row gutter={16}>
                        <Col xs={24} md={12} lg={12} xl={12} xxl={8}>
                            <FormItem label={t("app.classes.period")}>
                                <Input value={sClassEditor.aClass.period} onChange={sClassEditor.aClass.set_period}
                                    type="text" required pattern=".*\S+.*" maxLength={MaxLengthPeriod}
                                    size="large" className={styles.inputWidth} />
                            </FormItem>
                        </Col>
                        { (!sClassEditor.aClass.isTemplate) ?
                            <Col xs={24} md={12} lg={12} xl={12} xxl={8}>
                                <FormSelect label={t("app.classes.gradingTerm")}
                                    value={sClassEditor.aClass.gradingTerm}
                                    onChange={sClassEditor.aClass.set_gradingTerm}
                                    onValid={sClassEditor.setValidateStatus}
                                    name="gradingTerm"
                                    required
                                    SelectComponent={GradeTermSelect}
                                    className={styles.inputWidth}
                                    size="large" />
                            </Col>
                            : null
                        }

                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} md={24} lg={24} xl={24} xxl={16}>
                            <FormItem label={t("app.classes.className")}>
                                <Input value={sClassEditor.aClass.className} onChange={sClassEditor.aClass.set_className}
                                    type="text" maxLength={MaxLengthClassName} required pattern=".*\S+.*"
                                    size="large" className={styles.inputWidth} />
                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} md={24} lg={24} xl={24} xxl={16}>
                            <FormItem label={t("app.classes.classDesc")}>
                                <TextArea value={sClassEditor.aClass.description} onChange={sClassEditor.aClass.set_description}
                                    maxLength={MaxLengthClassDescription}
                                    autosize={ClassDescriptionAutoSize}
                                    className={styles.inputWidth} />
                            </FormItem>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} md={24} lg={24} xl={24} xxl={16}>
                            <FormItem label={t("app.classes.gradingRanges")}>
                                <Checkbox checked={sClassEditor.aClass.useDefaultGradesRange}
                                    onChange={(e)=>sClassEditor.set_isDefaultGradeRange(e.target.checked)}>Use default grading range</Checkbox>
                                <div className={styles.gradingRange}>
                                    <a onClick={()=>sClassEditor.set_isOpenGradeRangeModal(true)} hidden={!sClassEditor.aClass.useDefaultGradesRange}>{t("app.classes.gradeRange.linkDefault")}</a>
                                    <a onClick={()=>sClassEditor.set_isOpenGradeRangeModal(true)} hidden={sClassEditor.aClass.useDefaultGradesRange}>{t("app.classes.gradeRange.linkCustom")}</a>
                                </div>
                            </FormItem>
                        </Col>
                    </Row>
                    <Divider />
                    <FormItem>
                        <span className="paddingRight-sm">
                            <Button size="large" onClick={handleClose}>
                                {t("form.cancel")}
                            </Button>
                        </span>
                        <span className="paddingRight-sm">
                            <Button htmlType="submit"
                                type="primary" size="large" className={styles.submit}
                                >{t("form.save")}</Button>
                        </span>
                    </FormItem>
                </Form>
            </Card>
            <GradeRangeModal 
                useDefaultGradesRange={sClassEditor.aClass.useDefaultGradesRange}
                onCancel = {onCancelGradeRangeModal}
                onSubmit = {onSubmitGradeRangeModal}
                modalGradeRange = {sClassEditor.modalGradeRange}
                visible = {sClassEditor.isOpenGradeRangeModal}
                />
        </div>
    );
})




