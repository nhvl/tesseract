import React, { FC, useMemo } from 'react';
import { observer } from 'mobx-react';

import { DbIdentity } from '../../../models/types';

import { Link } from '../../../components/router/Links';

export const ClassLink: FC<{routeName:"classDetail"|"classEditor", classId:DbIdentity}> = observer(({classId, children, ...props}) => {
    const params = useMemo(() => ({classId:String(classId)}), [classId]);
    return (
        <Link params={params} {...props}>{children}</Link>
    );
});
