import React, { FC } from 'react';
import {observer} from "mobx-react";

import { ClassEditor } from './ClassEditor';
import { BasicLayout } from '../../layouts/BasicLayout';

export const ClassEditorPage: FC<{}> = observer(({}) => {
  return (
    <BasicLayout>
        <ClassEditor/>
    </BasicLayout>
  );
})

