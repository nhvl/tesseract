import React, { FC, useMemo, useState, useCallback, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Student } from "../../../models/Student";
import { GradeRange } from "../../../models/GradeRange";

import { DefaultId, DbIdentity } from "../../../models/types";
import { FiveC } from "../../../models/ActivityScore";
import { ActivityType } from "../../../models/Activity";

import { useStore } from "../../stores";

import { Table, message, Popover, Divider, Select, Radio, Icon, Empty, Tooltip, Button, } from "antd";
import { ColumnProps, SorterResult } from "antd/lib/table";

import { Link } from "../../../components/router/Links";
import { useNumberTrans } from "../../../components/DateTime/Number";
import { ShortScoreMedals } from "../../../components/ScoreMedals";
import { ScoreBadge } from "../../../models/ScoreBadge";

import { GradeInput } from "../../components/Grade/GradeInput";
import { SkillLevelRadioOption } from "../../components/Grade/SkillLevelRadioOption";

import styles from "./GradeBookCard.module.less";
import { Modal } from "../../../components/Modal/ModalAntd";
import classNames from "classnames";

export const GradeBookTable: FC<{}> = observer(() => {
    const { t } = useTranslation();
    const [scrollX, set_ScrollX] = useState<string | undefined>(undefined);
    const [scrollY, set_ScrollY] = useState<string | undefined>(undefined);
    const { sClassDetail: sClass } = useStore();
    const { percent } = useNumberTrans();

    const columns = useMemo<ColumnProps<Student>[]>(() => [
        {
            title: t("app.activities.score.Name"),
            key: "Name",
            className: `${styles.colName}`,
            fixed: "left",
            defaultSortOrder: "ascend",
            sorter: Student.sorter.fullName,
            sortDirections: ["descend", "ascend"],
            render: (_, record) => (<ActionCell record={record} />),
        },
        {
            title: t("app.activities.score.Grade"),
            key: "Grade",
            fixed: "left",
            className: `${styles.colGrade}`,
            defaultSortOrder: "ascend",
            sorter: Student.sorter.cumulativeGrade,
            sortDirections: ["descend", "ascend"],
            render: (_, record) => <div className="flex justify-between">
                <div className="font-medium">{percent(record.cumulativeGrade, 2)}</div>
                <div className={styles.letterGrade}>{sClass.aClass && GradeRange.retrieveLetterGrade(sClass.aClass.gradeRanges, record.cumulativeGrade)}</div>
            </div>,
        },
        ...sClass.sortedActivities.map((a, index) => ({
            title: (sClass.students.length > 0 ? (<>
                    <Tooltip placement="top" title={`[${t('app.classes.gradebook.quickGrader')}] ${a.title}`}>
                        <Link routeName="quickGrader"
                            params={({
                                studentId: String(sClass.students[0].studentId),
                                activityId: String(a.activityId)
                            })}
                        >{a.title}</Link>
                    </Tooltip>
                    <div className="text-grey">{t('app.activities.score.outOfNumber', { number: a.maxScore })}</div>
                </>) : (<>
                    <div>{a.title}</div>
                    <div className="text-grey">{t('app.activities.score.outOfNumber', { number: a.maxScore })}</div>
                </>)
            ),
            key: `${index}${a.title}`,
            className: `act_${a.activityId}`,
            render: (_: any, record: Student) => (<ScoreCell activityId={a.activityId} studentId={record.studentId} maxScore={a.maxScore} />),
        })),
    ], [sClass.sortBy, sClass.isDescending]);

    const renderHorizontalScroll = useCallback(() => {
        const length = sClass.sortedActivities.length;
        const width = length * 135 + 150 + 90 + 256 + 24 * 2 + 17 * 2;
        //console.log({table:width, window: window.innerWidth});
        set_ScrollX(width >= window.innerWidth ? '100%' : undefined);
    }, [sClass.sortedActivities.length, window.innerWidth]);

    const renderVerticalScroll = useCallback(() => {
        const actualHeight: number = sClass.students.length * 51 + 500;
        //console.log("A "+actualHeight);
        //console.log(window.innerHeight);
        if (actualHeight > window.innerHeight) {
            const tableMaxHeight: number = window.innerHeight / 2;
            set_ScrollY(tableMaxHeight.toString()+"px");
        }
        else {
            set_ScrollY(undefined);
        }
        console.log(scrollY);
    }, [sClass.students.length, window.innerHeight]);

    const onChangeTableData = useCallback((extra) => {
        sClass.students = extra.currentDataSource;
    },[]);

    const applyHeaderColor = useCallback(() => {
        var items = document.querySelectorAll<HTMLElement>(`th[class^="act_"]`);
        for (var i = 0; i < items.length; i++) {
            //fill color for table header
            const actId = items[i].className.replace("act_", "");
            const act = sClass.activities.find(x => x.activityId == Number(actId));
            if (act) {
                Object.assign(items[i].style, {
                    background: act.color,
                    color: ScoreBadge.getTextColorFromBackground(act.color),
                });
            }
        }
    },[]);

    useEffect(() => {
        renderVerticalScroll();
        renderHorizontalScroll();
        setTimeout(()=>{
            applyHeaderColor();
        });

        window.addEventListener("resize", handleResize);

        function handleResize() {
            renderVerticalScroll();
            renderHorizontalScroll();
        }
        return () => { window.removeEventListener("resize", handleResize) };
    }, [window.innerHeight, window.innerWidth, sClass.sortedActivities]);

    //Re-render data in table
    for (var i = 0; i < sClass.students.length; i++) {
        sClass.students[i].cumulativeGrade;
    }

    return (columns.length == 2 ? (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
    ) : (<>
        <div className="flex items-center mb-lg">
            <div className="mr-lg">
                <label className="text-black font-medium mr-xs">{t("app.activities.score.sortBy")}:</label>
                <Select value={sClass.sortBy} onChange={sClass.set_sortBy} className={styles.sortDue}>
                    {sClass.sortItems.map(opt =>
                        (<Select.Option key={opt.id} value={opt.id}>{t(opt.value)}</Select.Option>))}
                </Select>
            </div>
            <div>
                <label className="text-black font-medium mr-xs">{t("app.activities.score.sortDirection")}:</label>
                <Radio.Group value={sClass.isDescending} onChange={(e) => sClass.set_isDescending(e.target.value)}>
                    <Radio value={false}>{t("app.activities.score.sortDirection.asc")}</Radio>
                    <Radio value={true}>{t("app.activities.score.sortDirection.desc")}</Radio>
                </Radio.Group>
            </div>
            <div><Button onClick={sClass.export2Csv}>{t('app.activities.exportToCsv')}</Button></div>
        </div>
        <Table columns={columns}
            dataSource={sClass.students}
            pagination={false}
            rowKey={rowKey}
            onChange={(pagination, filters, sorter, extra) => onChangeTableData(extra)}
            bordered
            className={classNames(styles.gradeBook
                                ,!scrollX && scrollY && styles.noHorizontalScroll
                                , scrollY && styles.noVerticalScroll)}
            scroll={{ x: scrollX, y: scrollY}}
        />
    </>));
});

const ScoreCell: FC<{ activityId: DbIdentity, studentId: DbIdentity, maxScore: number }> = observer(({ activityId, studentId, maxScore }) => {
    const { t } = useTranslation();
    const [editScore, setEditScore] = useState<number|undefined>(DefaultId as number | undefined);
    const [editBadge, setEditBadge] = useState<FiveC|undefined>(undefined);
    const { sClassDetail, scoreBadge, currentSchoolId, currentFacultyId } = useStore();

    const actScore = sClassDetail.actScores.find(x => x.activityId == activityId && x.studentId == studentId);

    const save = useCallback(async () => {
        window.dispatchEvent(new Event("resize"));
        if (editScore == actScore!.score) {
            setEditScore(DefaultId as number);
            setEditBadge(undefined);
            return;
        }

        setEditScore(DefaultId as number);
        setEditBadge(undefined);
        if (actScore) actScore.update(currentSchoolId, currentFacultyId).then(([err, data]) => {
            if (err) {
                Modal.error({
                    title: t("app.activities.actDoc.save.failed"),
                    content: err.message,
                });
                return;
            }

            const student = sClassDetail.students.find(x => x.studentId == studentId);
            if (student) {
                student.set_cumulativeGrade(data);
                setTimeout(message.success(t("app.activities.score.saveSuccess", { student: student.fullName }), 0), 1000);
            }

        });
    }, [actScore, editScore, editBadge]);

    const onClickScore = useCallback(() => {
        if (editBadge != undefined) return;
        setEditScore(actScore!.score);

        var el: HTMLInputElement | null = document.getElementById(studentId + "-" + activityId) as HTMLInputElement;
        setTimeout(() => {
            if (el) {
                el.select();
                window.dispatchEvent(new Event("resize"));
            }
        });
    }, [actScore, studentId, activityId, editScore, editBadge]);

    const onKeydown = useCallback((event) => {
        if (event.keyCode == 13) {
            event.target.blur();
        }
    }, [actScore, studentId, activityId]);

    const handleMedalsClick = useCallback(async (cType: FiveC) => {
        setEditBadge(cType);
        window.dispatchEvent(new Event("resize"));
    }, [actScore, editBadge]);

    const handleCancel = useCallback(async () => {
        setEditBadge(undefined);
        window.dispatchEvent(new Event("resize"));
    }, [actScore, editBadge]);

    const toggleIsExclude = useCallback(async () => {
        if (!actScore) return;
        actScore.set_isExclude(!actScore.isExclude);
        save();
    }, [actScore, save]);

    return (!actScore ? null : (<Popover trigger="click" visible={editBadge != undefined}
            content={editBadge && (
                <div className={styles.display5C}>
                    <SkillLevelRadioOption record={actScore} cType={editBadge} onSave={save} onCancel={handleCancel} />
                </div>
                )}>
        <div className="text-center relative">
            <span hidden={editScore == DefaultId}>
                <GradeInput id={studentId + "-" + activityId} value={actScore.score} onChange={actScore.set_score} onKeyUp={onKeydown} onBlur={save} max={maxScore * 2} />
            </span>
            <span hidden={editScore != DefaultId} onClick={onClickScore} className={styles.editGrade}>
                {actScore.score != undefined
                    ? (<span className={styles.scoreStudent}>{actScore.score}</span>)
                    : (<span className={styles.scoreStudent}>{t("app.activities.score.NoGrade")}</span>)
                }
                <i className={`${styles.iconEdit} fas fa-pen`}></i>
            </span>
            <Tooltip title={t("app.activities.score.isExclude")}>
                <Icon type={actScore.isExclude ? "eye-invisible" : "eye"} onClick={toggleIsExclude}  />
            </Tooltip>
            <span>
                <Popover content={(
                    <ul className="list-reset">
                        <li className="font-size-lg none">{t("app.activities.score.skill.add")}</li>
                        <Divider />
                        {actScore.emptyBadges.length > 0 && actScore.emptyBadges.map((item) => (
                            <li key={item}><a onClick={() => handleMedalsClick(item)}><i className="fas fa-award" /><span className="paddingLeft-sm">{item}</span></a></li>
                        ))}
                    </ul>
                )} trigger="hover">
                    {actScore.emptyBadges.length > 0 &&
                        <a className={styles.add5C}><Icon type="plus" />5C</a>
                    }
                </Popover>
            </span>
        </div>
        <div className={`${styles.icon5C} flex justify-center`}>
            <ShortScoreMedals score={actScore} scoreBadge={scoreBadge} scoreChangeHandler={handleMedalsClick} className={styles.icons} />
        </div>
    </Popover>));
});

const ActionCell: FC<{ record: Student }> = observer(({ record }) => {
    const { sClassDetail: sClass } = useStore();
    const params = useMemo(() => ({
        classId: String(sClass.classId),
        studentId: String(record.studentId),
    }), [sClass.classId, record.studentId])

    return (
        <Link routeName="studentDetailInClass" params={params}>
            {record.fullName}
        </Link>
    );
})

function rowKey(record: Student) { return String(record.studentId) }
