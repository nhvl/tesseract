import React, { FC } from "react";
import { observer } from "mobx-react";

import { Activity } from "../../../models/Activity";
import { Category } from "../../../models/Category";

import { useStore } from "../../stores";

import { useTranslation } from "react-i18next";
import { LongDateLabel } from "../../../components/DateTime/DateTime";

import { Collapse, Typography, Divider, } from "antd";
const { Title } = Typography;
const { Panel } = Collapse;

import { Link } from "../../../components/router/Links";
import styles from "./ActivityTable.module.less";
import { ActivityActions } from "./ActivityActions";

export const ActivityCategoriesView: FC<{ activities: Activity[], title: string }> = observer(({ activities, title }) => {
    const { t } = useTranslation();
    const { sEditActivity } = useStore();
    return (<>
        {sEditActivity.categories.filter(c => activities.filter(a => a.categoryId == c.activityCategoryId).length > 0)
            .map(category => (<div className="mb-lg" key={category.activityCategoryId}>
                <Collapse defaultActiveKey={[String(category.activityCategoryId)]}>
                    <Panel key={category.activityCategoryId}
                        header={(
                            <Title level={4}>
                                {category.name}
                            </Title>
                        )}
                        >
                        {activities.filter(a => a.categoryId == category.activityCategoryId).map(activity => (
                            <ActivityItem key={activity.activityId} item={activity} title={title} />
                        ))}
                    </Panel>
                </Collapse>
            </div>))}
        {activities.filter(a => !Category.isValidCategory(a.categoryId, sEditActivity.categories)).length > 0 && (
        <Collapse defaultActiveKey={["Uncategorized"]}>
            <Panel key="Uncategorized"
                header={(
                    <Title level={4}>
                        {t("app.activities.category.uncategorized")}
                    </Title>
                )}
                >
                {activities.filter(a => !Category.isValidCategory(a.categoryId, sEditActivity.categories)).map(activity => (
                    <ActivityItem key={activity.activityId} item={activity} title={title} />
                ))}
            </Panel>
        </Collapse>)}
    </>);
});

const ActivityItem: FC<{ item: Activity, title: string }> = observer(({ item, title }) => {
    const { t } = useTranslation();
    return (<div className={`${styles.categoryGroup} border-bottom paddingBottom-sm mb-md`}>
        <div className="mb-xs">
            <Link routeName="activitySettings" params={item.params}>
                <span className="font-medium font-size-md text-link-grey">{item.title}</span>
            </Link>
        </div>
        <div>
            <span>
                <label className="text-black mr-xs">{t("app.activities.assign")}</label>
                <span>{item.dateAssigned != null && (<LongDateLabel value={item.dateAssigned} />)}</span>
            </span><Divider type="vertical" />
            <span>
                <label className="text-black mr-xs">{t("app.activities.due")}</label>
                <span>{item.dateDue != null && (<LongDateLabel value={item.dateDue} />)}</span>
            </span><Divider type="vertical" />
            <span>
                <ActivityActions item={item} />
            </span>
        </div>
    </div>)
});
