import React, { FC } from "react";
import { observer } from "mobx-react";

import { ActivityType } from "../../../models/Activity";

import { useStore } from "../../stores";

import { useTranslation } from "react-i18next";

import { Button } from "antd";

import { Link } from "../../../components/router/Links";

import { ActivityTable } from "./ActivityTable";
import { ActivityCategoriesView } from "./ActivityCategoriesView";
import { DuplicateActivityModal } from "./DuplicateActivityModal";

export const AssignmentCard: FC<{}> = observer(() => {
    const { sClassDetail: sClass } = useStore();

    const { t } = useTranslation();

    return (
        <>
            <Link routeName="newActivitySettings"
                params={({classId: String(sClass.classId)})}
                queryParams={({type: ActivityType.Assignment})}>
                <Button type="primary" icon="plus" className="add-btn mb-sm">{t('app.activities.assignments')}</Button>
            </Link>
            <ActivityTable
                dataSource={sClass.cAssignments}
                titleHeader={t("app.activities.assignment")}
            />
            <DuplicateActivityModal />
        </>
    );
});
