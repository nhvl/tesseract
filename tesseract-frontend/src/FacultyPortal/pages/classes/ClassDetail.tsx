import React, { FC, useCallback, useEffect } from "react";
import { observer } from "mobx-react";

import { useStore } from "../../stores";

import { Skeleton, Typography, Tabs, Card, Breadcrumb, } from "antd";
const {Title} = Typography;

import { Link } from "../../../components/router/Links";
import { RouterView } from "../../../components/router/RouterView";

import { ActivityCard } from "./ActivityCard";
import { AssignmentCard } from "./AssignmentCard";
import { AssessmentCard } from "./AssessmentCard";
import { StudentCard } from "./StudentCard";
import { ClassroomTools } from "./ClassroomTools";
import { ClassDiscussionList } from "../discussion/ClassDiscussionList";

import { extendsRouterState } from "../../../utils/extendsRouterState";
import { useTranslation } from "react-i18next";
import { Modal } from "../../../components/Modal/ModalAntd";
import { GradeBookTable } from "./GradeBookCard";

const viewMap = ({
    classActivities : (<ActivityCard   />),
    classAssignments: (<AssignmentCard />),
    classAssessments: (<AssessmentCard />),
    classGradebook  : (<GradeBookTable    />),
    classStudents   : (<StudentCard    />),
    classTools      : (<ClassroomTools />),
    discussionList  : (<ClassDiscussionList />),
});

export const ClassDetail: FC<{}> = observer(({}) => {
    const store = useStore();
    const {routerStore, sClassDetail, sEditActivity, currentUser} = store;
    const {t} = useTranslation();
    const {routeName, params} = routerStore.routerState;
    const sId = params.classId;
    useEffect(() => {
        const classId = !sId ? NaN : Number(sId);
        if (Number.isNaN(classId) || classId < 1) {
            routerStore.goTo("classes");
            return;
        }
        if(currentUser) {
            sEditActivity.loadCategories(currentUser.facultyId);
        }
        sClassDetail.initClass(store.currentSchoolId, currentUser!.facultyId, classId).then((err) => {
            if (err) {
                Modal.error({
                    title: err.message,
                    onOk(){ routerStore.goTo("classes"); },
                });
                return;
            }
            if (sClassDetail.aClass != null && sClassDetail.aClass.gradingTerm != store.currentGradingTerm) {
                store.set_currentGradingTerm(sClassDetail.aClass.gradingTerm, true);
            }
        });
    }, [sId]);

    useEffect(() => {
        if (routeName == "classDetail") store.replaceRouterState(extendsRouterState(routerStore.routerState, {routeName:"classActivities"}));
    }, [routeName]);

    const changeTab = useCallback((activeKey: string) => {
        routerStore.goTo(activeKey, params)
    }, [params]);


    if (sClassDetail.aClass == null) return (<Skeleton />);

    return (<>
        {(sClassDetail.aClass.isTemplate)
            ? <div className="breadcrumb">
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            <Link routeName="classes">{t("app.classes.allClass")}</Link>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                            <Link routeName="classTemplates">{t("app.classes.templates")}</Link>
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                            {sClassDetail.aClass.className}
                        </Breadcrumb.Item>
                    </Breadcrumb>
              </div>
            : null
        }
        <div className="header">
            <Title level={3}>{sClassDetail.aClass.className}</Title>
        </div>
        <Card>
            <Tabs activeKey={routeName} onChange={changeTab}>
                <Tabs.TabPane key="classActivities"  tab={<Link routeName="classActivities"  params={params}>{t('app.classes.menu.activities')}</Link>} />
                <Tabs.TabPane key="classAssignments" tab={<Link routeName="classAssignments" params={params}>{t('app.classes.menu.assignments')}</Link>} />
                <Tabs.TabPane key="classAssessments" tab={<Link routeName="classAssessments" params={params}>{t('app.classes.menu.assessments')}</Link>} />
                <Tabs.TabPane key="classGradebook"   tab={<Link routeName="classGradebook"   params={params}>{t('app.classes.menu.gradebook')}</Link>} />
                <Tabs.TabPane key="classStudents"    tab={<Link routeName="classStudents"    params={params}>{t('app.classes.menu.students')}</Link>} />
                <Tabs.TabPane key="discussionList"   tab={<Link routeName="discussionList"   params={params}>{t('app.classes.menu.discussions')}</Link>} />
                <Tabs.TabPane key="classTools"       tab={<Link routeName="classTools"       params={params}>{t('app.classes.menu.classTools')}</Link>} />
            </Tabs>
            <RouterView viewMap={viewMap} />
        </Card>
    </>);
});





