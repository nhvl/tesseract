import React, { FC, useCallback, useEffect, useMemo, useState } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DbIdentity, DefaultId } from "../../../models/types";
import { Class } from "../../../models/Class";

import { useStore } from "../../stores";

import { showError } from "../../../services/api/ShowError";

import { Button, Modal as AntdModal, Card, Icon, Tooltip, Typography, message, Breadcrumb, Form } from "antd";
const { Title } = Typography;
import Table, { ColumnProps } from "antd/lib/table";

import { Link } from "../../../components/router/Links";
import { Input } from "../../../components/inputs/antd/Input";
import { Modal } from "../../../components/Modal/ModalAntd";

import { GradeTermSelect } from "../../components/GradeTermSelect";
import { BasicLayout } from "../../layouts/BasicLayout";

import { ClassLink } from "./ClassLink";

export const ClassTemplatePage: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    return (<BasicLayout>
        <div className="breadcrumb">
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link routeName="classes">{t("app.classes.menu.classes")}</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{t("app.classes.templates")}</Breadcrumb.Item>
            </Breadcrumb>
        </div>
        <div className="header">
            <Title level={3}>{t("app.classes.allTemplates")}</Title>
        </div>
        <Card className="responsiveTable">
            <TemplateTable/>
        </Card>
    </BasicLayout>);
});

const TemplateTable: FC<{}> = observer(({}) => {
    const store = useStore();
    const {t} = useTranslation();
    const [applyTemplateId, setApplyTemplateId] = useState(DefaultId);
    const [gradingTermId, setGradingTermId] = useState(DefaultId);
    const [classTitle, setClassTitle] = useState("");

    const onApply = useCallback(async (templateId: DbIdentity, className: string, gradingTermId: DbIdentity) => {
        const err = await Class.applyTemplate({schoolId:store.currentSchoolId, facultyId:store.currentFacultyId}, {templateId, gradingTermId, className});
        if(err) showError(err, t); else message.info(t("app.classes.list.applyingTemplateMessage"));;
    }, [store.currentFacultyId, store.currentGradingTerm]);

    const onDelete =  useCallback((templateId: DbIdentity) => {
        console.log(templateId);
        Modal.deleteConfirm({
            content: t("app.template.modal.removeConfirm"),
            okText: t("form.yes"),
            okType: "danger",
            cancelText: t("form.no"),
            onOk() {
                store.sClassTemplates.templates = store.sClassTemplates.templates.filter(t => t.classId != templateId);
                Class.deleteTemplate({schoolId: store.currentSchoolId, facultId:store.currentFacultyId, templateId}).then(([err]) => {
                    if (err) showError(err, t);
                    else message.info(t("app.template.message.deleteDone"));

                    store.sClassTemplates.fetchTemplates(store.currentFacultyId).then(err => {
                        if (err) showError(err, t)
                    });
                });
            }
        });
    }, [store.currentFacultyId]);

    useEffect(() => {
        store.sClassTemplates.init(store.currentFacultyId).then(err => { if(err) showError(err, t) });
    }, [store.currentFacultyId]);

    const columns = useMemo<Array<ColumnProps<Class>>>(() => [
        { dataIndex: "period", key: "period", title: t("app.classes.list.period"),
            // defaultSortOrder:"ascend", sorter: (a, b) => a.period.localeCompare(b.period),
        },
        { dataIndex: "className", key: "className", title: t("app.classes.menu.classTemplates"),
            render: (_, c) => (<ClassLink routeName="classDetail" classId={c.classId}>{c.className}</ClassLink>)
        },
        { dataIndex: "classId", key: "actions", render:(_, c) => (
            <ClassLink routeName="classEditor" classId={c.classId}>
                <Tooltip title="Edit">
                    <Icon type="edit" className="icon-sm" />
                </Tooltip>
            </ClassLink>
        )},
        { dataIndex: "classId", key: "delete", render:(_, c) => (
            <Tooltip title={t("app.classes.tooltip.delete")}>
                <a onClick={() => onDelete(c.classId)}><Icon type="delete" className="icon-sm" /></a>
            </Tooltip>
        )},
        { dataIndex: "classId", key: "apply", render: (_, c) => (
            <Button onClick={() => {
                setApplyTemplateId(c.classId);
                setClassTitle(c.className);
                setGradingTermId(store.currentGradingTerm);
            }}>{t("app.classes.template.apply")}</Button>
        )},
    ], [t]);

    return store.sClassTemplates.loading ? null : (<>
        <Table rowKey={rowKey}
            dataSource={store.sClassTemplates.templates}
            columns={columns}
            pagination={false}
        />
        <AntdModal
            title={t("app.classes.template.modal.title")}
            onCancel={() => setApplyTemplateId(DefaultId)}
            onOk={() => {
                onApply(applyTemplateId, classTitle, gradingTermId);
                setApplyTemplateId(DefaultId);
            }}
            okText={t("app.classes.template.modal.ok")}
            cancelText={t("app.classes.template.modal.cancel")}
            okButtonProps={{ disabled: classTitle.length < 1 }}
            visible={applyTemplateId != DefaultId}
            >
            <Form layout="vertical">
                <Form.Item label={t("app.classes.template.modal.classNameLabel")}>
                    <Input onChange={setClassTitle} value={classTitle}/>
                </Form.Item>
                <Form.Item label={t("app.classes.template.modal.gradingTermLabel")}>
                    <GradeTermSelect value={gradingTermId} onChange={setGradingTermId}/>
                </Form.Item>
            </Form>
        </AntdModal>
    </>);
});

const rowKey = (c:Class) => String(c.classId);
