import React, { FC } from "react";
import { observer } from "mobx-react";

import { useStore } from "../../stores";

import { useTranslation } from "react-i18next";

import { Button, } from "antd";

import { ActivityTree } from "./ActivityTree";
import { DuplicateActivityModal } from "./DuplicateActivityModal";
import { PublishActivityModal } from "./PublishActivityModal";
import { Link } from "../../../components/router/Links";
import { ActivityType } from "../../../models/Activity";

export const ActivityCard: FC<{}> = observer(() => {
    const { sClassDetail } = useStore();

    const { t } = useTranslation();

    return (
        <>
            <Link routeName="newActivitySettings"
                params={({classId: String(sClassDetail.classId)})}
                queryParams={({type: ActivityType.Activity})}>
                <Button type="primary" icon="plus" className="add-btn mb-sm">{t('app.classes.activity.activity')}</Button>
            </Link>
            <ActivityTree />

            <DuplicateActivityModal />
            <PublishActivityModal />
        </>
    );
});
