import React, { FC, useEffect, useMemo } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { DbIdentity, DefaultId } from '../../../models/types';
import { FacultyClass } from '../../models/FacultyClass';

import { useStore } from '../../stores';

import { Card, Icon, Typography, Tooltip, Button, message, } from 'antd';
import { ColumnProps } from 'antd/lib/table';
const { Title } = Typography;

import { BasicLayout } from '../../layouts/BasicLayout';
import { ClassLink } from "./ClassLink";
import { DndTable } from '../../../components/DndTable';
import { Class } from '../../../models/Class';
import { showError } from '../../../services/api/ShowError';
import { Link } from '../../../components/router/Links';
import { Modal } from '../../../components/Modal/ModalAntd';

const rowKey = (c:FacultyClass) => String(c.classId);

export const ClassList: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const store = useStore();
    useEffect(() => {
        store.sClassList.init().then(err => {
            if (!err) return;
            Modal.error({content: err.message});
        });
    }, []);

    return (<BasicLayout>
        <div className="header">
            <Title level={3}>{t("app.classes.allClass")}</Title>
        </div>
        <Card className="mb-lg">
            <Button type="primary" className="mb-sm"><Link routeName="classTemplates"><span>{t("app.classes.menu.templateButtonTitle")}</span></Link></Button>
            <ClassTable />
        </Card>
    </BasicLayout>);
});

const ClassTable: FC<{}> = observer(({}) => {
    const {sClassList, currentUser, currentSchoolId} = useStore();
    const {t} = useTranslation();
    const createTempalate = async (classId: DbIdentity) => {
        const err = await Class.createTempate({schoolId: currentSchoolId, facultyId: currentUser == null ? DefaultId : currentUser.facultyId, classId});
        if (err) showError(err, t);
        else message.info(t('app.classes.list.creatingTemplateMessage'));
    }
    const columns = useMemo<Array<ColumnProps<FacultyClass>>>(() => [
        { dataIndex: 'period', key: 'period', title: t("app.classes.list.period"),
            // defaultSortOrder:"ascend", sorter: (a, b) => a.period.localeCompare(b.period),
        },
        { dataIndex: 'className', key: 'className', title: t("app.classes.list.className"),
            // sorter: (a, b) => a.className.localeCompare(b.className),
            render: (name:string, c:FacultyClass) => (<ClassLink routeName="classDetail" classId={c.classId}>{c.className}</ClassLink>)},
        { dataIndex: 'classId', key: 'actions', title: '',
            render:(id:DbIdentity, c:FacultyClass) => (
                <ClassLink routeName="classEditor" classId={c.classId}>
                  <Tooltip title={t('app.classes.list.edit')}>
                    <Icon type="edit" className="icon-sm" />
                  </Tooltip>
                </ClassLink>
            )
        },
        { key: 'template',
            render: (_:string, c:FacultyClass) => (<Button onClick={() => createTempalate(c.classId)}>{t("app.classes.list.createTemplate")}</Button>   )
        },
    ], [t]);

    return (
        <DndTable rowKey={rowKey}
            dataSource={sClassList.items}
            columns={columns}
            pagination={false}
            onDragEnd={sClassList.changeOrder}
            />
    );
});
