import React, { FC, useCallback, useState } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Modal, List, } from "antd";

import { DiceRoller } from "../tools/DiceRoller";
import { GroupGenerator } from "../tools/GroupGenerator";

export const ClassroomTools: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const store = useStore();
    const {sClassDetail} = store;

    const [isShowDiceRoller, setShowDiceRoller] = useState(false);
    const openDiceRoller = useCallback(() => setShowDiceRoller(true), []);
    const closeDiceRoller = useCallback(() => setShowDiceRoller(false), []);

    const [isShowGroupGenerator, setShowGroupGenerator] = useState(false);
    const openGroupGenerator = useCallback(() => setShowGroupGenerator(true), []);
    const closeGroupGenerator = useCallback(() => setShowGroupGenerator(false), []);

    return (<>
        <List>
            <List.Item>
                <a className="flex" onClick={openDiceRoller}>
                    <i className="fas fa-dice icon-lg text-center w-8"/>
                    <span className="paddingLeft-xs">Dice Roller</span>
                </a>
            </List.Item>
            <List.Item>
                <a onClick={openGroupGenerator} className="flex">
                    <i className="fas fa-random icon-lg text-center w-8"/>
                    <span className="paddingLeft-xs">{t("app.classes.tools.groupGenerator")}</span>
                </a>
            </List.Item>
        </List>

        <Modal title={t("app.classes.tools.diceRoller")} visible={isShowDiceRoller} onCancel={closeDiceRoller} footer={false}>
            <DiceRoller />
        </Modal>

        <Modal title={t("app.classes.tools.groupGenerator")} visible={isShowGroupGenerator} onCancel={closeGroupGenerator} footer={false}>
            <GroupGenerator students={sClassDetail.students} />
        </Modal>
    </>);
});
