import React, { FC, useMemo, useCallback, useEffect } from "react";
import { observer } from "mobx-react";

import { Activity } from "../../../models/Activity";

import { useStore } from "../../stores";

import { useTranslation } from "react-i18next";
import { LongDateLabel } from "../../../components/DateTime/DateTime";

import { ColumnProps, SortOrder } from "antd/lib/table";

import { Link } from "../../../components/router/Links";
import { ResponsiveTable } from "../../../components/ResponsiveTable/ResponsiveTable";
import { ActivityActions } from "./ActivityActions";
import styles from "./ActivityTable.module.less";

const sortDirections:SortOrder[] = ["descend", "ascend"];

export const ActivityTable: FC<{
    dataSource  :Activity[],
    titleHeader?:string,
}> = observer(({dataSource, titleHeader}) => {
    const { isMobile } = useStore();

    const { t } = useTranslation();

    const columns = useMemo<Array<ColumnProps<Activity>>>(() => [
        {
            title: titleHeader || t("app.activities.activity"),
            key: "title",
            render: (_, item) => {
                return {
                    props:{
                        //style:{ background: item.color }
                    },
                    children:(<div className="flex items-center">
                        <span className={styles.color} style={{ background: item.color }}></span>
                        <Link routeName="activitySettings" params={item.params}><span className="text-link-grey font-medium">{item.title}</span></Link>
                    </div>)
                }},
            sorter: Activity.sorter.title,
            sortDirections,
            className: "whitespace-normal min-w"
        },
        {
            title: t("app.activities.createdDate"),
            key: "dateCreated",
            render: (_, item) => (<LongDateLabel value={item.dateCreated} />),
            sorter: Activity.sorter.dateCreated,
            sortDirections,
        },
        {
            title: t("app.activities.dueDate"),
            key: "dateDue",
            defaultSortOrder: "ascend",
            render: (_, item) => (item.dateDue != null && (<LongDateLabel value={item.dateDue} />)),
            sorter: Activity.sorter.dateDue,
            sortDirections,
        },
        {
            title: t("app.activities.assignDate"),
            key: "dateAssigned",
            render: (_, item) => (item.dateAssigned != null && (<LongDateLabel value={item.dateAssigned} />)),
            sorter: Activity.sorter.dateAssigned,
            sortDirections,
        },
        {
            key: "actions",
            render: (_, item) => (<ActivityActions item={item} />),
        }
    ], []);

    return (
        <ResponsiveTable
            dataSource={dataSource}
            columns={columns}
            pagination={false}
            rowKey={rowKey}
            isMobile={isMobile}
        />
    );
});

function rowKey(record: Activity) { return String(record.activityId) }
