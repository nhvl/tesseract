import React, { FC, useEffect, useCallback } from "react";
import { observer } from "mobx-react";

import { Activity, PublishStatusEnum } from "../../../models/Activity";

import { useStore } from "../../stores";
import { ITreeNode, ClassDetailStore } from "../../stores/ClassDetailStore";

import { DndProvider  } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import TouchBackend from 'react-dnd-touch-backend';
import {isTouchEvents} from "../../../utils/isTouchEvents";

import {SortableTreeWithoutDndContext} from 'react-sortable-tree';

import { Link } from "../../../components/router/Links";
import { ResponsiveActions } from "../../../components/ResponsiveTable/ResponsiveActions";

import { PublishCell } from "../../components/PublishCell";
import { useTranslation } from "react-i18next";

const Backend = isTouchEvents ? TouchBackend : HTML5Backend;

export const ActivityTree: FC<{}> = observer(() => {
    const { sClassDetail } = useStore();

    useEffect(() => {
        sClassDetail.set_treeData(sClassDetail.rootActivities.map(a => toTreeData(a, sClassDetail)))
    }, [sClassDetail.rootActivities])

    return (<div className="sortable-wrapper">
        <DndProvider backend={Backend}>
            <SortableTreeWithoutDndContext
                treeData={sClassDetail.treeData}
                onChange={sClassDetail.treeChange}
                getNodeKey={getNodeKey}
                isVirtualized={false}
                rowHeight={62 + 12}
            />
        </DndProvider>
    </div>);
});

function getNodeKey({node}:{node:ITreeNode} ) { return node.activityId }

function toTreeData(a:Activity, sClass: ClassDetailStore): ITreeNode {
    const xs = sClass.activityId2Children.get(a.activityId);
    return ({
        activityId: a.activityId,
        title     : (<Link routeName="activitySettings" params={a.params}><span className="text-link-grey">{a.title}</span></Link>),
        subtitle  : (<ActivityAction item={ a} />),
        children  : xs ? xs.map(x => toTreeData(x, sClass)): [],
        expanded  : true,
    });
}

const ActivityAction: FC<{item:Activity}> = observer(({item}) => {
    const {t} = useTranslation();
    const {sActivityDup, sActivityPublish, mediaBreakpoint} = useStore();
    const onDuplicate = useCallback(() => { sActivityDup.showModal(item.activityId) }, [item.activityId]);
    const onPublish = useCallback(() => { sActivityPublish.showModal(item.activityId, item.publishStatus != PublishStatusEnum.Unpublished) }, [item.activityId, item.publishStatus]);

    return (<ResponsiveActions breakpoint={mediaBreakpoint} xs={Infinity}>
        <PublishCell startDate={item.publishStartDate} status={item.publishStatus} onClick={onPublish}/>
        <Link routeName="editActDoc" params={item.params}>{t('app.classes.activity.tree.editContent')}</Link>
        <Link routeName="viewActDoc" params={item.params}>{t('app.classes.activity.tree.preview')}</Link>
        <Link routeName="activityDiscussion" params={item.params}>{t('app.classes.activity.tree.discussions')}</Link>
        <a onClick={onDuplicate}>{t('app.classes.activity.tree.duplicate')}</a>
    </ResponsiveActions>);
});
