import React, { FC } from "react";
import { observer } from "mobx-react";

import { ActivityType } from "../../../models/Activity";

import { useStore } from "../../stores";

import { useTranslation } from "react-i18next";

import { Button } from "antd";

import { Link } from "../../../components/router/Links";
import { ActivityTable } from "./ActivityTable";
import { DuplicateActivityModal } from "./DuplicateActivityModal";


export const AssessmentCard: FC<{}> = observer(() => {
    const { sClassDetail } = useStore();

    const { t } = useTranslation();

    return (
        <>
            <Link routeName="newActivitySettings"
                params={({classId: String(sClassDetail.classId)})}
                queryParams={({type: ActivityType.Assessment})}>
                <Button type="primary" icon="plus" className="add-btn mb-sm">{t('app.activities.assessments')}</Button>
            </Link>
            <ActivityTable
                dataSource={sClassDetail.cAssessments}
                titleHeader={t("app.activities.assessment")}
            />
            <DuplicateActivityModal />
        </>
    );
});
