import React, { FC, useMemo, useCallback, useState } from "react";
import { observer } from "mobx-react";

import { Activity } from "../../../models/Activity";
import { FacultyClass } from '../../models/FacultyClass';

import { useStore } from "../../stores";

import { useTranslation } from "react-i18next";

import { Modal, Divider, message, Pagination } from "antd";

import { Checkbox } from "../../../components/inputs/antd/Input";

export const DuplicateActivityModal: FC<{}> = observer(() => {
    const { tClass, sActivityDup, sClassDetail, currentFacultyId, currentSchoolId } = useStore();
    const { t } = useTranslation();
    const [okLoading, setOkLoading] = useState(false);
    const [pageId, setPageId] = useState(1);
    const rowCount = 10;

    const onCancel = useCallback(() => {
        return sActivityDup.closeModal();
    }, []);

    const onOk = useCallback(async () => {
        if (sActivityDup.chosenClasses.length == 0) return sActivityDup.closeModal();
        setOkLoading(true);
        const [err] = await Activity.duplicate(currentSchoolId, currentFacultyId, sActivityDup.activityId, sActivityDup.chosenClasses);
        err ? message.error(err.message) : message.success(t("form.activity.duplicate.message.success"));
        setOkLoading(false);
        sActivityDup.closeModal();
    }, []);

    const classes = useMemo(() =>
        tClass.filter(cls => cls.classId != sClassDetail.classId)
        .sort((cl1, cl2) => cl1.gradingTerm - cl2.gradingTerm),
        [tClass, sClassDetail.classId]);

    const paginationClasses = useMemo(() => classes.slice((pageId - 1)* rowCount, pageId * rowCount), [classes, pageId]);

    return (
        <Modal
            title={t("form.activity.duplicate.title")}
            visible={sActivityDup.ActivityDuplicateVisible}
            onOk={onOk}
            onCancel={onCancel}
            confirmLoading={okLoading}
        >
            <div className="max-h-screen overflow-y-auto">
                {paginationClasses.map(cls => (
                    <div key={cls.classId} className="m-b-checkbox">
                        <DuplicateClassCheckbox cls={cls} />
                    </div>
                ))}
            </div>
            <Pagination defaultCurrent={1} onChange={(page: number) => setPageId(page)} total={classes.length} pageSize={rowCount}/>
        </Modal>
    );
});

const DuplicateClassCheckbox: FC<{ cls: FacultyClass }> = observer(({ cls }) => {
    const { sActivityDup, currentSchool } = useStore();

    const gradingTerm = currentSchool ? currentSchool.tGradingTerm.find(x => x.gradingTermId === cls.gradingTerm) : null;
    const isChosen = sActivityDup.chosenClasses.includes(cls.classId);

    const onCheckboxChange = useCallback((v: boolean) => {
        v ? sActivityDup.addClass(cls.classId) : sActivityDup.removeClass(cls.classId);
    }, [cls]);

    return (<>
        <Checkbox checked={isChosen} onChange={onCheckboxChange} />
        <span className="-mr-2 text-black">{cls.className}</span>
        <Divider type="vertical" />
        {gradingTerm && (<span>{gradingTerm.name}</span>)}
    </>);
});


