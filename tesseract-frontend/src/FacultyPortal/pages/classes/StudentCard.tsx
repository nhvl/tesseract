import React, { FC, useEffect, useCallback, useState, useMemo } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DefaultId, EPermission } from "../../../models/types";

import { StudentItemResult } from "../editStudent/EditStudentStore";
import { StudentItem, pageSize } from "../../stores/ImportExistingStudentsStore";

import { useStore } from "../../stores";

import { Divider, Button, Modal as AntdModal, Select, message, Tag, Pagination } from "antd";
import Table, { ColumnProps } from "antd/lib/table";
import { UploadFile } from "antd/lib/upload/interface";

import { CSVLink } from "react-csv";

import { Link } from "../../../components/router/Links";
import { Input } from "../../../components/inputs/antd/Input";

import { Modal } from "../../../components/Modal/ModalAntd";
import { DumbUpload } from "../../../components/inputs/antd/Upload";
import { PermissionGuard } from "../../../components/PermissionGuard";
import { StudentTable } from "../students/StudentTable";

import { showError } from "../../../services/api/ShowError";

import styles from './StudentCard.module.less';

export const StudentCard: FC<{}> = observer(({}) => {
    const store = useStore();
    const { sImportExistingStudent : sExistingStudent, sBatchImportStudent, sStudentList, sClassDetail, routerStore } = store;

    const {classId} = routerStore.routerState.params;
    const {t} = useTranslation();

    let cId = !classId ? DefaultId : Number(classId);
    if (Number.isNaN(cId)) cId = DefaultId;

    const openBatchImportFile = useCallback(() => {
        sBatchImportStudent.init({classId: cId});
    },[cId]);

    useEffect(() => {
        sStudentList.classId = sClassDetail.classId;
        sStudentList.fetchData().then(err => {
            if (err) {
                Modal.error({content: err.message});
                return;
            }
        });
    }, []);

    const loadAssignExistingStudent =  useCallback(() => {
        sExistingStudent.init({ classId: cId }).then(err => {
            if (err) {
                Modal.error({content: err.message});
                return;
            }
        });
        sExistingStudent.set_isOpenStudentListModal(true);
    }, [cId]);

    return (<>
        <PermissionGuard permissions={[EPermission.CanAddStudent]}>
            <div className="mb-sm">
                <Button icon="import" onClick={loadAssignExistingStudent} className="add-btn mr-sm">{t("app.students.list.importFromExisting")}</Button>
                <Button icon="import" onClick={openBatchImportFile} className="add-btn mr-sm">{t("app.students.list.importFromFile")}</Button>
                <Link routeName="createStudent" params={({classId: String(sClassDetail.classId)})}>
                    <Button type="primary" icon="plus" className="add-btn mr-sm">{t("app.students.list.add")}</Button>
                </Link>
            </div>
        </PermissionGuard>

        <StudentTable students={sStudentList.displayStudents} defaultSort="lastName"/>

        <ImportExsitingStudent />
        <BatchUpdateStudent />
    </>);
});

//#region Import from Another Class
export const ImportExsitingStudent: FC<{}>
        = observer(({}) => {
    const store = useStore()
    const { sImportExistingStudent: sExistingStudent, sStudentList } = store;
    const {t} = useTranslation();

    const onCancel = useCallback(() => {
        sExistingStudent.set_isOpenStudentListModal(false);
    },[sExistingStudent.isOpenStudentListModal]);

    const importExsitingStudent = useCallback(async () => {
        const err = await sExistingStudent.save();
        if (err) showError(err, t);
        else
        {
            message.success(t('form.importSuccess'));
            sStudentList.classId = sExistingStudent.classId;
            sStudentList.fetchData().then(err => {
                if (err) {
                    Modal.error({content: err.message});
                    return;
                }
            });
        }
    },[]);
    const onSearchChange = useCallback((v:string)=>{
        sExistingStudent.set_searchText(v);
        sExistingStudent.set_pageIndex(0);
        sExistingStudent.fetchStudent();
    },[sExistingStudent.searchText]);
    const onFilterChange = useCallback((v: number)=>{
        sExistingStudent.set_selectedClassId(v);
        sExistingStudent.set_pageIndex(0);
        sExistingStudent.fetchStudent();
    },[]);

    return (
        <AntdModal
            confirmLoading={sExistingStudent.loading}
            title={t("app.students.list.importFromExisting")}
            onCancel={onCancel}
            onOk={importExsitingStudent}
            visible={sExistingStudent.isOpenStudentListModal}>
            <div className={styles.result}>
                <div className={styles.filter}>
                    <label className="text-black font-medium mr-xs">{t("app.students.list.selectLabel")}:</label>
                    <div className={styles.searchText}>
                        <Select value={sExistingStudent.selectedClassId} onChange={onFilterChange}
                            size="large" className={styles.classFilter}>
                            <Select.Option key={DefaultId} value={DefaultId}>{t("app.students.list.selectClass")}</Select.Option>
                            {store.tClass.filter(c => c.classId != sExistingStudent.classId).map(c => (
                                <Select.Option key={c.classId} value={c.classId}>{c.period}. {c.className}</Select.Option>
                            ))}
                        </Select>
                        <Input className={"mt-sm"} value={sExistingStudent.searchText} onChange={onSearchChange} placeholder={t('app.students.list.searchText')}/>
                    </div>
                </div>
            </div>
            <ExistingStudentTable students={sExistingStudent.filterStudents} />
        </AntdModal>);
});


export const ExistingStudentTable: FC<{students:StudentItem[]}> = observer(({students}) => {
    const {t} = useTranslation();
    const {sImportExistingStudent: sExistingStudent} = useStore();
    const {selectedRowKeys} = sExistingStudent;

    const onPageChange = useCallback((pageValue:number)=>{
        sExistingStudent.set_pageIndex(pageValue - 1);
        sExistingStudent.fetchStudent();
    },[]);
    const onTableChange = useCallback((pagination, filters, sorter)=>{
        if(sExistingStudent.sortOrder!=sorter["order"]
            || sExistingStudent.sortColumnKey!=sorter["columnKey"]){
            sExistingStudent.set_sortColumnKey(sorter["columnKey"]);
            sExistingStudent.set_sortOrder(sorter["order"]);
            sExistingStudent.set_pageIndex(0);
            sExistingStudent.fetchStudent();
        }
    },[])

    const columns = useMemo<ColumnProps<StudentItem>[]>(() => [
        {title: t("app.students.list.name")          , key: "fullName"      , sorter:fullNameSorter      , dataIndex:"fullName"      , render: (_, item: StudentItem) => (item.student.fullName)      , sortOrder: sExistingStudent.sortColumnKey==="fullName" && sExistingStudent.sortOrder},
        {title: t("app.students.list.studentNumber") , key: "studentNumber" , sorter:studentNumberSorter , dataIndex:"studentNumber" , render: (_, item: StudentItem) => (item.student.studentNumber) , sortOrder: sExistingStudent.sortColumnKey==="studentNumber" && sExistingStudent.sortOrder},
        // {title: t("app.students.list.class")            , key:"className", dataIndex:"classes_", sorter:classNameSorter, render: (classes_:Class[]) => (!classes_ ? "" : classes_.map(c => (<span>{c.className}</span>)))}
    ], [sExistingStudent.sortColumnKey, sExistingStudent.sortOrder]);

    const onSelectChange = ((selectedRowKeys: string[], selectedRows: StudentItem[]) => {
        sExistingStudent.set_isSelectAll(false);
        sExistingStudent.set_selectedRowKeys(selectedRowKeys);
    });

    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
        hideDefaultSelections: true,
        selections:[{
                key: 'all-data',
                text: 'Select All Pages',
                onSelect: (props:any) => {
                    sExistingStudent.set_isSelectAll(true);
                }
            },
            {
                key: 'unselect-all-data',
                text: 'Unselect All Pages',
                onSelect: (props:any) => {
                    sExistingStudent.set_isSelectAll(false);
                    sExistingStudent.set_selectedRowKeys([]);
                }
            }]
        // getCheckboxProps: record => ({
        //   disabled: record.name === 'Disabled User', // Column configuration not to be checked
        //   name: record.name,
        // }),
    };

    return (<>
            <div className={"mb-sm"}>
                <label>{t("app.students.list.selectedLabel", { nStudent: sExistingStudent.nSelectedStudents})}</label>
            </div>
            <Table rowSelection={rowSelection}
                columns={columns}
                dataSource={students}
                pagination={false}
                rowKey={rowKey}
                onChange={onTableChange}
            />
                <Pagination className={"mt-sm"} current={sExistingStudent.pageIndex+1}
                            total={sExistingStudent.totalStudents} pageSize={pageSize}
                            onChange={onPageChange} hideOnSinglePage={true}/>
    </>);
});

function stringSorter(selector:(s:StudentItem) => string) { return (a:StudentItem, b:StudentItem) => selector(a).localeCompare(selector(b)) }
function numberSorter(selector:(s:StudentItem) => number) { return (a:StudentItem, b:StudentItem) => (selector(a) - selector(b)) }
const fullNameSorter = stringSorter(s => s.student.fullName);
const studentNumberSorter = stringSorter(s => s.student.studentNumber);
// const classNameSorter = stringSorter(s => s.classes_ ? s.classes_[0].className : "");

function rowKey(record:StudentItem) { return `${record.key}` }
//#endregion

//#region BatchUpdate Student
export const BatchUpdateStudent: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const { sBatchImportStudent, sStudentList } = useStore();

    const onChangeImportFile = useCallback((file: UploadFile) => {
        sBatchImportStudent.importFile = file;
        // console.log(sBatchImportStudent.importFile.name);
    },[]);

    const submitImportFile = useCallback(() =>{
        if(!sBatchImportStudent.importFile)
            Modal.error({content: t('app.students.list.importFromFile.FileNotFound')});
        sBatchImportStudent.save().then(err => {
            if (err) {
                if(err.error){
                    Modal.error({content: err.error.message});
                    // console.log(typeof(err.error.errorObject));
                }
                else{
                    Modal.error({content: err.message});
                }
            }
            else{
                sStudentList.classId = sBatchImportStudent.classId;
                sStudentList.fetchData().then(err => {
                    if (err) {
                        Modal.error({content: err.message});
                        return;
                    }
                });
            }
        });
    },[]);

    const onCancelBatchUpdate = useCallback(() =>{
        sBatchImportStudent.batchResults = undefined;
        sBatchImportStudent.importFile = undefined;
        sBatchImportStudent.set_openUploadFileModal(false);
    },[]);

    return (<>
       <AntdModal
            title={t("app.students.list.importFromFile")}
            onCancel={onCancelBatchUpdate}
            cancelText={t('form.close')}
            onOk={submitImportFile}
            okText={t('form.submit')}
            okButtonProps={sBatchImportStudent.batchResults ? ({ disabled: true }) : ({})}
            visible={sBatchImportStudent.openUploadFileModal}
            width={sBatchImportStudent.batchResults ? 920 : 500}
            confirmLoading={sBatchImportStudent.loading}
            >
            <div className="mb-lg">
                <p>{t('app.students.list.importFromFile.csvTemplateDetail')}</p>
                <a href="/documents/batch-import-students-template.csv" title={t('app.students.list.importFromFile.csvTemplate')}>
                    {t('app.students.list.importFromFile.csvTemplate')}
                </a>
                <div className="mt-md">
                    <DumbUpload onChange={onChangeImportFile} accept=".csv">
                        <Button icon="upload" className="add-btn mb-sm">{t('app.students.list.uploadFileCSV')}</Button>
                    </DumbUpload>
                    {sBatchImportStudent.importFile && (<div className="mb-sm">{sBatchImportStudent.importFile.name}</div>)}
                    <div>
                        <span className="mr-xs">{t('app.students.list.uploadFileCSVFields')}</span>
                        <span className="text-black font-medium mr-xs">{t('app.students.list.uploadFileCSVFieldsRequired')}</span>
                        <span className="mr-xs">{t('app.students.list.uploadFileCSVRequired')}</span>
                    </div>

                </div>
            </div>
            {sBatchImportStudent.batchResults && (<>
                <Divider />
                <BatchUpdateErrorShow result={sBatchImportStudent.batchResults} />
            </>)}
        </AntdModal>
    </>);
});

export const BatchUpdateErrorShow: FC<{result :StudentItemResult[]}> = observer(({result}) => {
    const {t} = useTranslation();
    const [resultMessage, setResultMessage] = useState(undefined);

    const columns = useMemo<ColumnProps<StudentItemResult>[]>(() => [
        {title: t("app.students.list.firstName")        , key:"firstName"       , render:({firstName}:StudentItemResult) => firstName},
        {title: t("app.students.list.lastName")         , key:"lastName"        , render:({lastName}:StudentItemResult) => lastName },
        {title: t("app.students.list.studentNumber")    , key:"studentNumber"   , render:({studentNumber}:StudentItemResult) => studentNumber },
        {title: t("app.students.list.grade")            , key:"grade"           , render:({grade}:StudentItemResult) => grade },
        {title: t("app.students.list.email")            , key:"email"           , render:({email}:StudentItemResult) => email },
        {title: t("app.students.list.phoneNumber")      , key:"phoneNumber"     , render:({phoneNumber}:StudentItemResult) => phoneNumber },
        {title: t("app.students.list.externalId")       , key:"externalId"      , render:({externalId}:StudentItemResult) => externalId},
        {title: t("app.students.list.status")           , key:"status"          ,
            render:({errors,status}:StudentItemResult) => (errors && errors.length > 0 ?
            (<Tag className="error">{t(`app.students.list.importFromFile.resultMessage.error`)}</Tag>):
                (status && status.toLowerCase() == "updated" ?
                    (<Tag className="info">{t(`app.students.list.importFromFile.resultMessage.${status.toLowerCase()}`)}</Tag>)
                    : (<Tag className="success">{t(`app.students.list.importFromFile.resultMessage.${status && status.toLowerCase()}`)}</Tag>)
                )
            )
        },
        {title: t("app.students.list.message")           , key:"result"          ,
            render:({errors,results}:StudentItemResult) =>
                (errors && errors.length > 0 ? (<ul className={`${styles.messageList} list-disc`}>
                            {errors.map((r, index) => (
                                                    <li key={index}>{r}</li>
                                                )
                                            )}
                        </ul>)

                    : (<ul className={`${styles.messageList} list-disc`}>
                        {results && results.map((r, index) => (
                                <li key={index}>{r}</li>
                                ))}
                    </ul>)
                )
        },
    ], []);

    const headers = [
        { label: "ExternalId", key: "externalId" },
        { label: "StudentNumber", key: "studentNumber" },
        { label: "Grade", key: "grade" },
        { label: "Email", key: "email" },

        { label: "PhoneNumber", key: "phoneNumber" }
      ];

    useEffect(() => {
        let total = result.length;
        let errorList = result.filter(r => r.errors && r.errors.length > 0);
        let error = errorList.length;
        let updated = result.filter(r => !errorList.includes(r)).filter(r => r.status && r.status.toLowerCase().includes("update")).length;
        let imported = result.filter(r => !errorList.includes(r)).filter(r => r.status && r.status.toLowerCase().includes("imported")).length;
        setResultMessage({ total: total, updated: updated, imported: imported, error: error });
    },[resultMessage]);

    const data = result.filter(r => r.errors && r.errors.length > 0).map(r => ({externalId: r.externalId,
        studentNumber: r.studentNumber,
        grade: r.grade,
        email: r.email,
        firstName: r.firstName,
        lastName: r.lastName,
        phoneNumber: r.phoneNumber }));

    return (<>
        <div className="flex items-center justify-between mb-sm">
            {resultMessage && (<div className="font-size-md text-black">
                <span className="mr-lg">
                    <span>{t('app.students.list.importFromFile.resultMessage.total')}: </span>
                    <span className="heading-3">{resultMessage.total}</span>
                </span>
                <span className="mr-lg">
                    <span>{t('app.students.list.importFromFile.resultMessage.updated')}: </span>
                    <span className="heading-3">{resultMessage.updated}</span>
                </span>
                <span className="mr-lg">
                    <span>{t('app.students.list.importFromFile.resultMessage.imported')}: </span>
                    <span className="heading-3">{resultMessage.imported}</span>
                </span>
                <span>
                    <span>{t('app.students.list.importFromFile.resultMessage.error')}: </span>
                    <span className="heading-3">{resultMessage.error}</span>
                </span>
            </div>)}
            {data.length > 0 && (<CSVLink
                headers={headers}
                data={data}
                filename={'import-student-error-file.csv'}
            >
                <Button type="link" icon="download">{t('app.students.list.importFromFile.downloadErrorRecords')}</Button>
            </CSVLink>)}
        </div>
        <div className="responsiveTable">
            <Table columns={columns}
            dataSource={result}
            pagination={false}
            rowKey={(record: StudentItemResult,index: number) => `${index}.${record.externalId}.${record.studentNumber}`}
            />
        </div>
    </>);
});

//#endregion
