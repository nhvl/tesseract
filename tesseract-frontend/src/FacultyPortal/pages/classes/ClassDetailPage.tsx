import React, { FC } from 'react';
import {observer} from "mobx-react";

import { ClassDetail } from './ClassDetail';
import { BasicLayout } from '../../layouts/BasicLayout';

export const ClassDetailPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <ClassDetail />
    </BasicLayout>);
})
