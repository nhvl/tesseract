import React, { FC, useCallback, useState } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";
import moment from "moment";

import { DefaultId } from "../../../models/types";

import { useStore } from "../../stores";

import { Button, message, Modal } from "antd";

import { DatePicker } from "../../../components/inputs/antd/DatePicker";

export const PublishActivityModal: FC<{}> = observer(() => {
    const { sActivityPublish, sActivity, currentFacultyId, currentSchoolId } = useStore();
    const { t } = useTranslation();

    const onCancel = useCallback(() => {
        return sActivityPublish.closeModal();
    }, []);

    const onSubmit = useCallback(async (start: number | undefined, end: number | undefined ) => {
        const [err] = await sActivity.setPublish(currentSchoolId, currentFacultyId, sActivityPublish.activityId, start, end);
        err ? message.error(err.message) : message.success(t("app.activities.publish.message.success"));

        sActivityPublish.closeModal();
    }, []);

    return (
        <Modal
            title={t("app.activities.publish.modal.title")}
            visible={sActivityPublish.activityId != DefaultId}
            onCancel={onCancel}
            footer={null}
        >
            <div className="mb-lg">
                <label className="mr-xs">
                    {t("app.activities.publish.modal.publishChoosing")}
                </label>
                {
                    sActivityPublish.isSet
                        ?   ( <Button type="primary" onClick={() => onSubmit(undefined, undefined)}>
                                {t("app.activities.publish.modal.unpublish")}
                            </Button>)
                        :   (<Button type="primary" onClick={() => onSubmit (Date.now(), undefined)}>
                                {t("app.activities.publish.modal.publishNow")}
                            </Button>)
                }
            </div>
            <div className="mb-lg">
                <span> {t("app.activities.publish.modal.orText")} </span>
                <span> {t("app.activities.publish.modal.setAutoText")} </span>
            </div>
            <div className="flex items-end">
                <div className="mr-md">
                    <label className="mb-xs text-black block font-medium"> {t("app.activities.publish.modal.startText")} </label>
                    <DatePicker
                        value={sActivityPublish.startTime}
                        onChange={sActivityPublish.setStartTime}
                        disabledDate={(current: moment.Moment | undefined) => current == undefined || current < moment(new Date())}
                        showToday={false}
                    />
                </div>
                <div className="mr-md">
                    <div className="mb-xs">
                        <label className=" text-black font-medium"> {t("app.activities.publish.modal.endText")} </label>
                        <span className="text-grey font-size-sm">({t("app.activities.publish.modal.Optional")})</span>
                    </div>
                    <DatePicker
                        value={sActivityPublish.endTime}
                        onChange={sActivityPublish.setEndTime}
                        disabled={sActivityPublish.startTime == undefined}
                        disabledDate={(current: moment.Moment | undefined) => current == undefined || sActivityPublish.startTime == undefined || current.startOf('day') <= moment(sActivityPublish.startTime).startOf('day')}
                        showToday={false}
                    />
                </div>
                <Button type="primary" disabled={sActivityPublish.startTime == undefined} onClick={() => onSubmit(sActivityPublish.startTime, sActivityPublish.endTime)}>{t("app.activities.publish.modal.setAutoPublish")}</Button>
            </div>
        </Modal>
    );
});
