import React, { FC, useCallback, useMemo, useState, useEffect } from 'react';
import { observer } from 'mobx-react';

import { useStore } from '../../stores';

import { Typography, Icon, Button, Card, Skeleton, } from 'antd';
const { Title } = Typography;

import { BasicLayout } from '../../layouts/BasicLayout';
import { ActivityToGrade } from "./ActivityToGrade";
import { useTranslation, Trans } from 'react-i18next';
import { ClassLink } from '../classes/ClassLink';
import { DefaultId } from '../../../models/types';

export const HomePage: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const {currentUser, currentSchool, currentGradingTermClasses, finishRefreshClass} = useStore();

    // useEffect(() => {
    //     if(currentSchool && currentSchool.tGradingTerm.length > 0)
    //         store.set_finishRefreshClass(true);
    // },[currentGradingTermClasses]);

    return (<BasicLayout>
        { currentSchool &&
            <div>
            {finishRefreshClass && (currentGradingTermClasses.length > 0 ?
                    <div className="header">
                        <Title level={3}>{`${t("dashboard.welcome")}, ${currentUser && currentUser.fullName}`}</Title>
                    </div>
                    :<>
                        <div className="header">
                            <Title level={3} className="mr-lg">
                                <Trans i18nKey="dashboard.welcomeAndCreateClass">
                                    <span>{currentSchool.currentGradingTermObject && currentSchool.currentGradingTermObject.name}</span>
                                </Trans>
                            </Title>
                        </div>
                        <Card className="border-left-color">
                            <div className="flex items-center">
                                <div>
                                    <span className="action-create-class">
                                        <Icon type="read" />
                                    </span>
                                </div>
                                <div className="action-button">
                                    <div className="text text-black">{t('dashboard.getActionModify')}</div>
                                    <ClassLink routeName="classEditor" classId={DefaultId}>
                                        <Button type="primary">
                                            <span>{t('dashboard.getStartedNow')}</span>
                                        </Button>
                                    </ClassLink>
                                </div>
                            </div>
                        </Card>
                    </>)
            }
            </div>
        }
        <ActivityToGrade />
    </BasicLayout>);
});




