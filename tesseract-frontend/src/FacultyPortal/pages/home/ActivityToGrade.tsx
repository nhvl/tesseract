import React, { FC, useEffect, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { ActivityDetail } from "../../../models/Activity";

import { useStore } from "../../stores";

import { Card, Divider, Select, Alert, } from "antd";
const Option = Select.Option;

import { Link } from "../../../components/router/Links";
import { LongDateLabel } from "../../../components/DateTime";
import { Modal } from "../../../components/Modal/ModalAntd";

export const ActivityToGrade: FC<{}> = observer(({}) => {
    const store = useStore();
    const {sActivityToGrade, currentGradingTerm, currentFacultyId, currentGradingTermClasses } = store;
    const {t} = useTranslation();

    useEffect(() => {
        if (currentFacultyId < 1 || currentGradingTerm < 1) return;
        sActivityToGrade.doFetch(currentFacultyId, currentGradingTerm).then(([err, data]) => {
            if (err) {
                Modal.error({ title: "getActivityToGrade.failed", content: err.message });
                return;
            }
            sActivityToGrade.set_activityDetail(data);
        });
    },[currentFacultyId, currentGradingTerm]);

    const handleOrderChange = useCallback(async (v: number) => {
        sActivityToGrade.set_order(v);
    }, [sActivityToGrade]);

    return (currentGradingTermClasses.length < 1 ? "":
        <>
            <Alert
                message={(sActivityToGrade.displayActivities.length < 1)
                    ? t("dashboard.noActivities")
                    : t("dashboard.haveActivities")
                }
                type="info"
                showIcon
                closable
                className="mb-lg"
            />
            <div className="paddingBottom-sm" hidden={sActivityToGrade.displayActivities.length < 1}>
                <label className="text-black font-medium mr-xs">{ t("dashboard.orderby") }:</label>
                <Select value={sActivityToGrade.isASCOrder} onChange={handleOrderChange} size="large">
                    <Option value={0}>{ t("dashboard.orderby.ascending") }</Option>
                    <Option value={1}>{ t("dashboard.orderby.descending") }</Option>
                </Select>
            </div>
            <Card hidden={sActivityToGrade.displayActivities.length < 1}>
                {sActivityToGrade.displayActivities.map((data:ActivityDetail, index:number) => (
                    <div key={index}>
                        <div className="border-bottom mb-md paddingBottom-sm">
                            <div className="mb-xs">
                                <Link routeName="activitySettings" params={data.params}><span className="text-link-grey font-size-md font-medium">{data.title}</span></Link>
                            </div>
                            <div>
                                <span className="text-black">
                                    <label className="paddingRight-xs">{data.class.period}</label>
                                    <span>{data.class.className}</span>
                                </span><Divider type="vertical" />
                                <span>
                                    <label className="text-black paddingRight-xs">Due</label>
                                    <span className="text-grey font-medium"><LongDateLabel value={data.dateDue} /></span>
                                </span>
                                <Divider type="vertical" />
                                <Link routeName="gradeDetail" params={data.params}>Submissions({data.unGradeCount}/{data.submisstionCount})</Link>
                            </div>
                        </div>
                    </div>
                ))}
            </Card>
        </>
    );
})
