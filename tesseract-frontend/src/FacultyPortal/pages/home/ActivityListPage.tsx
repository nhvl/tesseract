import React, { FC, useEffect, useCallback, useMemo } from 'react';
import { observer } from 'mobx-react';
import { useStore } from '../../stores';
import { BasicLayout } from '../../layouts/BasicLayout';
import { useTranslation } from 'react-i18next';
import { Typography, Table, Card } from 'antd';
const { Title } = Typography;
import { Activity } from "../../../models/Activity";
import { ColumnProps } from 'antd/lib/table';
import { Link } from "../../../components/router/Links";
import { DateFromNowToolTip } from "../../../components/DateTime";
import { GradeTermSelect } from "../../components/GradeTermSelect";
import styles from './ActivityListPage.module.less';
import { Modal } from '../../../components/Modal/ModalAntd';

export const ActivityListPage: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const store = useStore();
    const {sActivityList, routerStore } = store;
    const facultyId = store.currentUser!.facultyId;
    useEffect(() => {
        sActivityList.set_openActivity(routerStore.routerState.routeName == "openAssignments");
        handleGradingTermChange(store.currentGradingTerm);
    }, [routerStore.routerState.routeName, store.currentGradingTerm]);

    const handleGradingTermChange = useCallback(async (v:number) => {
        sActivityList.set_gradingTerm(v);
        sActivityList.initActivities(facultyId).then((err) => {
                if (!err) return;
                Modal.error({
                    title: "getActivites.failed",
                    content: err.message,
                    onOk(){ routerStore.goTo("home") },
                });
            });
    },[sActivityList]);

    return (<BasicLayout>
        <div className="header">
            <Title level={3}>{ sActivityList.isOpenActivity ? t("menu.openAssignments"): t("menu.pastAssignments")}</Title>
        </div>
        {!sActivityList.isOpenActivity && (<div className="paddingBottom-sm">
            <label className="text-black font-medium mr-xs">{t('app.activities.listpage.filterby')}:</label>
            <GradeTermSelect size="large" className={styles.gradingTermFilter} value={sActivityList.selectedGradingTerm} onChange={handleGradingTermChange} />
        </div>)}
        <Card title={t('app.activities.listpage.title')}>
            <ActivityTable isLoading={sActivityList.isLoading} activities={sActivityList.displayActivities}/>
        </Card>
    </BasicLayout>);
});

const ActivityTable: FC<{activities:Activity[], isLoading:boolean}> = observer(({activities, isLoading}) => {
    const {t} = useTranslation();

    const columns = useMemo<Array<ColumnProps<Activity>>>(() => [
        {
            title: t('app.activities.listpage.table.period'),
            key: 'Period',
            render: (_, record: Activity) => record.class.period
        },
        {
            title: t('app.activities.listpage.table.class'),
            key: 'classId',
            render: (_, record: Activity) => record.class.className
        },
        {
            title: t('app.activities.listpage.table.assignment'), className: "whitespace-normal min-w",
            key: 'Assignment',
            render: (_, record: Activity) => (
                <Link routeName="activitySettings" params={record.params}>{record.title}</Link>
            )
        },
        {
            title: t('app.activities.listpage.table.duedate'),
            dataIndex: 'dateDue',
            key: 'dateDue',
            render: (_, record: Activity) => record.dateDue && (<DateFromNowToolTip value={record.dateDue} />)
        },
        {
            key: 'Grade',
            render: (_, record: Activity) => (record.isGraded && (
                <Link routeName="gradeDetail" params={record.params}>{t('app.activities.listpage.table.grades')}</Link>
            ))
        }
    ], [activities]);

    return (
        <div className="responsiveTable">
            <Table dataSource={activities} columns={columns} pagination={false} rowKey={rowKey} loading={isLoading} />
        </div>
    );
});

function rowKey(record:Activity) { return String(record.activityId) }

