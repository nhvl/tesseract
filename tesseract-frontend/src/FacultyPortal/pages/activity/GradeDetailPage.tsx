import React, { FC, useEffect, useCallback, useMemo, useState } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { DbIdentity } from '../../../models/types';
import { ActivityScoreInfo, ActivityScore, FiveC, FiveCValues } from "../../../models/ActivityScore";

import { useStore } from '../../stores';

import { Breadcrumb, Table, Card, Row, Col, message, Spin, Radio, Checkbox, Typography, Select, Button, Icon, Divider, } from 'antd';
const { Title } = Typography;
import { ColumnProps } from 'antd/lib/table';

import { BasicLayout } from '../../layouts/BasicLayout';
import { GradeInput } from "../../components/Grade/GradeInput";
import { SkillLevelRadioOption } from "../../components/Grade/SkillLevelRadioOption";
import { SkillSelector } from "../../components/Grade/SkillSelector";
import { ActivityBreadcrumb } from "../../components/ActivityBreadcrumb";

import { LongScoreMedals } from "../../../components/ScoreMedals";
import { ScoreBadge } from "../../../models/ScoreBadge";
import { Link } from '../../../components/router/Links';

import styles from "./GradeDetailPage.module.less";
import { Modal } from '../../../components/Modal/ModalAntd';

const RadioGroup = Radio.Group;

export const GradeDetailPage: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const {sGradeDetail, sActivityModal, routerStore} = useStore();
    const refresh = useCallback(() => {
        const sId = routerStore.routerState.params.activityId;
        if (!sId) {}
        const activityId: DbIdentity = Number(sId);
        if (Number.isNaN(activityId)) {}
        sGradeDetail.init(activityId).then(err => {
            if (!err) return;
            Modal.error({ title: "⚠", content: err.message, onOk(){
                routerStore.goTo("home");
            }});
        });
    }, []);
    useEffect(refresh, [routerStore.routerState.params.activityId]);

    const openCommentDialog = useCallback((record:ActivityScoreInfo) => {
        sActivityModal.open(sGradeDetail.activity, record).then((shouldRefresh) => {
            if (!shouldRefresh) return;
            setTimeout(message.success(t("app.activities.score.saveSuccess", {student: record.fullName}), 0), 1000);
            refresh();
        });
    }, [sGradeDetail, refresh]);

    const columns = useMemo<Array<ColumnProps<ActivityScoreInfo>>>(() => [{
        title: t("app.activities.score.Name"),
        dataIndex: 'fullName',
        key: 'fullName',
        sorter: ActivityScoreInfo.sorter.fullName,
        sortDirections: ['descend', 'ascend'],
        defaultSortOrder: 'ascend',
    }, {
        title: t("app.activities.score.ID"),
        dataIndex: 'externalId',
        key: 'externalId',
        sorter: ActivityScoreInfo.sorter.externalId,
        sortDirections: ['descend', 'ascend']
    }, {
        title: t("app.activities.score.Submission"),
        render: (_, record) => {
            if(record.isSubmitted){
                const params = ({
                    studentId: String(record.studentId),
                    activityId: String(record.activityId),
                });
                return (<Link routeName="studentSubmission" params={params}>{t("app.activities.score.Submissions.view")}</Link>);
            }
            return (<>
                <label>{t("app.activities.score.noSubmission")}</label> <Divider type="vertical" />
                <a onClick={() => openCommentDialog(record)}><Icon type="message" className="icon-sm" /></a>
            </>);
        }
    }, {
        title: t("app.activities.score.Late"),
        dataIndex: 'isLate',
        key: 'isLate',
        render: (_, record) => (<Checkbox disabled checked={record.isLate}></Checkbox>),
    }, {
        title: t("app.activities.score.Grade"),
        render: (_, record, index) => (<GradesCell record={record} index={index} />),
    }],[sGradeDetail.activityScores]);

    const rowClassName = useCallback((record: ActivityScoreInfo)=>{
        return record.score!=undefined ? styles.isGraded : "";
    },[sGradeDetail.activityScores]);

    return (<BasicLayout>
        <ActivityBreadcrumb aClass={sGradeDetail.aClass} activity={sGradeDetail.activity}>
            <Breadcrumb.Item>{t("app.activities.score.Grade")}</Breadcrumb.Item>
        </ActivityBreadcrumb>
        <div className="header">
            <Title level={3}>{t('app.activities.score.Submissions')}</Title>
        </div>
        <Card>
            <div className="flex justify-between mb-sm" hidden={sGradeDetail.activityScores.length==0}>
                <Button type="primary" onClick={()=>routerStore.goTo("quickGrader", {
                    studentId:String(sGradeDetail.activityScores.sort(ActivityScoreInfo.sorter.fullName)[0].studentId),
                    activityId: String(sGradeDetail.activity.activityId)})}
                >{t('app.activities.score.QuickGrade')}</Button>
                <div class="font-size-md text-black">
                    <label>{t("app.activities.score.AssignmentNum")} </label>
                    <span className="heading-3">{sGradeDetail.activity.activityId}</span>
                </div>
            </div>
            <div className="responsiveTable">
                <Table rowClassName={rowClassName} dataSource={sGradeDetail.activityScores} columns={columns} rowKey={record => record.studentId.toString()} pagination={false} className={styles.grade} />
            </div>
        </Card>
    </BasicLayout>);
});

const GradesCell:FC<{record:ActivityScoreInfo, index: number}> = observer(({record, index}) => {
    const {t} = useTranslation();
    const {sGradeDetail, scoreBadge, currentSchoolId, currentFacultyId} = useStore();
    const handleSave = useCallback(async () => {
            sGradeDetail.cachedActivityScores.forEach((cached:ActivityScoreInfo) => {
            if(cached.studentId == record.studentId
                && JSON.stringify(cached) != JSON.stringify(record)) {
                    sGradeDetail.isSaving = true;
                    record.update(currentSchoolId, currentFacultyId).then(([error, data]) => {
                        sGradeDetail.isSaving = false;
                        if (error) {
                            Modal.error({ title: "saveGrade.failed", content: error.message });
                            return;
                        }
                        sGradeDetail.reset_cache();
                        setTimeout(message.success(t("app.activities.score.saveSuccess", {student: record.fullName}), 0), 1000);
                    });
            }
        });
    },[record]);
    const handleCancel = useCallback(async ()=>{
        sGradeDetail.clear_selectedBadges();
    },[record])
    const handleInputChange = useCallback(async(v:number)=>{
        sGradeDetail.isSaving = false;
        record.set_score(v);
    },[record]);
    const handleMedalsClick = useCallback(async(cType:FiveC) => {
        sGradeDetail.set_selectedBadges(index, cType);
    },[record]);
    return (<Spin spinning={sGradeDetail.isShowSpinning(record)} tip={t("app.activities.score.Save")}>
        <div className={`${styles.colGrades}`}>
            <div className="flex items-center mt-xs mb-xs">
                <div className="mr-md">
                    <label className="font-medium">{t("app.activities.score.ActualGrade")}: </label>
                    <GradeInput value={record.score || undefined} onChange={handleInputChange} onBlur={()=>{if(!sGradeDetail.isUpdatingBadges) handleSave();} } max={sGradeDetail.activity.maxScore*2}/>
                    <span className="ml-xs mr-xs">/</span>{sGradeDetail.activity.maxScore}
                </div>
                {!sGradeDetail.isSaving && record.emptyBadges.length != 0 &&
                    (<SkillSelector options={record.emptyBadges} selected={sGradeDetail.selectedBadges[index] as any} onSelect={ handleMedalsClick } />)}
            </div>
            {sGradeDetail.activity.isGraded && (
                <div className={`${styles.fiveC}`}>
                    <LongScoreMedals score={record} scoreBadge={scoreBadge} scoreChangeHandler={handleMedalsClick}/>
                    <div hidden={sGradeDetail.selectedBadges[index] == undefined}>
                        <SkillLevelRadioOption record={record} cType={sGradeDetail.selectedBadges[index] as any} onSave={handleSave} onCancel={handleCancel} />
                    </div>
                </div>
            )}
        </div>
    </Spin>)
});
