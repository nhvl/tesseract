import React, { useState, FC, FormEvent, useCallback, useEffect, useMemo } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DefaultId } from "../../../models/types";
import { ActivityType } from "../../../models/Activity";
import { Category  } from "../../../models/Category";

import { useStore } from "../../stores";

import { Breadcrumb, Form, Button, Modal as AntdModal, Card, Row, Col, message, Divider, Select, Icon, Tooltip, Table } from "antd";
import { ColumnProps } from "antd/lib/table";
import { DatePickerProps } from "antd/lib/date-picker/interface";

import { ColorPicker  } from "../../../components/ColorPicker";
import { Input, Checkbox, TextArea } from "../../../components/inputs/antd/Input";
import { InputNumber } from "../../../components/inputs/antd/InputNumber";
import { DatePicker } from "../../../components/inputs/antd/DatePicker";
import { Link } from "../../../components/router/Links";
import { Modal } from "../../../components/Modal/ModalAntd";

import { ActivityBreadcrumb } from "../../components/ActivityBreadcrumb";
import { BasicLayout } from "../../layouts/BasicLayout";

const limitMaxScore: number = 10000;
const limitWeight: number = 100;

export const ActivitySettings: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const store = useStore();
    const {sEditActivity, routerStore, currentUser} = store;
    const [submitting, setSubmitting] = useState(false);

    const {activity} = sEditActivity;

    const {activityId, classId} = routerStore.routerState.params;
    const {type} = routerStore.routerState.queryParams;
    const aId = !activityId ? NaN : Number(activityId);
    const cId = !classId    ? NaN : Number(classId);

    const handleClose = useCallback(async () => {
        if (history.length > 1) history.back();
        else routerStore.goTo("home");
    },[]);

    useEffect(() => {
        if(currentUser){
            sEditActivity.loadCategories(currentUser.facultyId);
        }

        if (!sEditActivity.isCreateNew(aId)) {
            sEditActivity.startEditActivity(aId).then((err) => {
                if (err) {
                    Modal.error({
                        title: "getActivity.failed",
                        content: err.message,
                        onOk: handleClose,
                    });
                    return;
                }
            });
        } else {
            if (Number.isNaN(cId)) {
                Modal.error({ title: t("app.activities.error.invalidClassId", {classId : classId}), onOk: handleClose });
                return;
            }
            sEditActivity.startCreateActivity(cId, Number(type));
        }
    }, [aId]);

    const handleSubmit = useCallback(async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if(sEditActivity.isUseCategory && activity.categoryId==undefined){
            message.error(t("app.activities.category.required"));
            return;
        }
        setSubmitting(true);
        const [err, data] = await sEditActivity.doSave();
        setSubmitting(false);
        if (err) {
            Modal.error({ title: t("app.activities.actDoc.save.failed"), content: err.message });
        } else {
            message.success(
                sEditActivity.isCreateNew(aId)
                ? t("app.activities.createSuccess", {activity:activity.title})
                : t("app.activities.editSuccess",   {activity:activity.title})
            , 2);
            routerStore.goTo("editActDoc", data.params);
        }
    }, [setSubmitting, sEditActivity.isUseCategory, activity.categoryId, activity.type]);

    const disabledStartDate = useCallback((startValue)=>{
        const endValue = activity.dateDue;
        if (!startValue || !endValue) return false;
        return startValue.valueOf() > endValue.valueOf();
    }, [activity]);

    const disabledEndDate = useCallback<NonNullable<DatePickerProps["disabledDate"]>>((endValue) => {
        const startValue = activity.dateAssigned;
        if (!endValue || !startValue) return false;
        return endValue.valueOf() <= startValue.valueOf();
    }, [activity]);

    const disabledCutoffDate = useCallback((endValue) => {
        const startValue = activity.dateDue;
        if (!endValue || !startValue) return false;
        return startValue.valueOf() > endValue.valueOf();
    }, [activity]);

    const handleDueDateChange = useCallback((dueDate: number|undefined) => {
        activity.set_dateDue(dueDate);
        //TES-93
        //When first create activity
        //Cutoff Time should default with due date when due date is selected
        if (sEditActivity.isCreateNew(aId) && dueDate != null && activity.isGraded) {
            activity.set_dateCutoff(dueDate);
        }
    }, [activity]);

    return (<BasicLayout>
        <div>
            <ActivityBreadcrumb aClass={sEditActivity.aClass}>
                {sEditActivity.aClass && (<Breadcrumb.Item>{(
                    activity.type == ActivityType.Assessment ? (<Link routeName="classAssessments" params={({classId:String(sEditActivity.aClass.classId)})}>{t("app.activities.assessments")}</Link>) : (
                    activity.type == ActivityType.Assignment ? (<Link routeName="classAssignments" params={({classId:String(sEditActivity.aClass.classId)})}>{t("app.activities.assignments")}</Link>) : (
                    <Link routeName="classActivities" params={({classId:String(sEditActivity.aClass.classId)})}>{t("app.activities.activities")}</Link>
                )))}</Breadcrumb.Item>)}
                <Breadcrumb.Item>{
                    sEditActivity.isCreateNew(aId)
                    ? t(`app.activities.createActivity.${activity.type}`)
                    : activity.title
                }</Breadcrumb.Item>
            </ActivityBreadcrumb>

            <Card>
                <Form onSubmit={handleSubmit}>
                    <Row gutter={16}>
                        <Col xs={24} md={24} lg={24} xl={20} xxl={16}>
                            <Form.Item label={t("form.title.label")} colon>
                                <Input size="large" value={activity.title} onChange={activity.set_title} required maxLength={200}/>
                            </Form.Item>
                        </Col>

                        <Col span={24} />

                        <Col xs={24} md={24} lg={24} xl={20} xxl={16}>
                            <Form.Item label={t("form.title.description")} colon>
                                <TextArea maxLength={500}
                                    value={activity.description} onChange={activity.set_description}
                                    autosize={{ minRows: 4, maxRows: 8 }}
                                    />
                            </Form.Item>
                        </Col>
                    </Row>

                    <Row gutter={16} hidden={activity.type == ActivityType.Activity}>
                        <Col xs={24} md={24} lg={24} xl={20} xxl={16}>
                            <Form.Item>
                                <Checkbox checked={sEditActivity.isUseCategory} onChange={sEditActivity.set_isUseCategory}>{t("app.activities.category.use")}</Checkbox>
                                <a hidden={!sEditActivity.isUseCategory} onClick={() => sEditActivity.set_isOpenCategoryModal(true)}>{t("app.activities.category.link")}</a>
                                <Select value={activity.categoryId} onChange={sEditActivity.categoryChange} disabled={!sEditActivity.isUseCategory}>
                                    {sEditActivity.categories.map(category => (
                                        <Select.Option key={category.activityCategoryId} value={category.activityCategoryId}>
                                            {category.name}
                                        </Select.Option>
                                        ))}
                                </Select>
                            </Form.Item>
                        </Col>

                        <Col span={24} />

                        <Col xs={8} md={6} xl={5} xxl={4}>
                            <Form.Item label={t("app.activities.color")} colon>
                                <ColorPicker color={activity.color} set_color={activity.set_color}
                                    isDisabled={!!sEditActivity.isUseCategory} colors={sEditActivity.categoryColorRange}/>
                            </Form.Item>
                        </Col>
                        <Col xs={8} md={6} xl={5} xxl={4}>
                            <Form.Item label={t("app.activities.isGraded")} colon>
                                <Checkbox checked={activity.isGraded} onChange={activity.set_isGraded} disabled={sEditActivity.isUseCategory} />
                            </Form.Item>
                        </Col>
                        <Col xs={8} md={6} xl={5} xxl={4} hidden={!activity.isGraded}>
                            <Form.Item label={t("app.activities.maxScore")} colon>
                                <InputNumber max={limitMaxScore}
                                    value={activity.maxScore} onChange={activity.set_maxScore}
                                    required={activity.isGraded} min={1}
                                    size="large" disabled={sEditActivity.isUseCategory}
                                    />
                            </Form.Item>
                        </Col>
                        <Col xs={8} md={6} xl={5} xxl={4} hidden={!activity.isGraded || activity.weight <= 0}>
                            <Form.Item label={t("app.activities.weight")} colon>
                                <InputNumber max={limitWeight}
                                    value={activity.weight} onChange={activity.set_weight}
                                    required={activity.isGraded} min={1}
                                    size="large" disabled={sEditActivity.isUseCategory} step={0.1}
                                    />
                            </Form.Item>
                        </Col>

                        <Col span={24} hidden={!activity.isGraded || sEditActivity.isUseCategory}>
                            <Form.Item>
                                <Checkbox checked={activity.weight <= 0}
                                    onChange={activity.set_isExclude}
                                    >{t("app.activity.edit.isExclude.desc")}</Checkbox>
                            </Form.Item>
                        </Col>

                        <Col span={24} hidden={!activity.isGraded || activity.weight <= 0}>
                            <Form.Item>
                                <Checkbox checked={activity.isCredit}
                                    onChange={activity.set_isCredit}
                                    >{t("app.activity.edit.isCredit.desc")}</Checkbox>
                            </Form.Item>
                        </Col>

                        <Col span={24} />

                        <Col xs={24} md={12} lg={12} xl={10} xxl={8}>
                            <Form.Item label={t("app.activities.assignDate")} colon>
                                <DatePicker
                                    value={activity.dateAssigned} onChange={activity.set_dateAssigned}
                                    disabledDate={disabledStartDate}
                                    format="YYYY-MM-DD HH:mm" showTime={{format: "HH:mm"}}
                                    size="large" className="w-full"
                                    />
                            </Form.Item>
                        </Col>
                        <Col xs={24} md={12} lg={12} xl={10} xxl={8}>
                            <Form.Item label={t("app.activities.dueDate")} colon>
                                <DatePicker
                                    value={activity.dateDue} onChange={handleDueDateChange}
                                    disabledDate={disabledEndDate}
                                    format="YYYY-MM-DD HH:mm" showTime={{format: "HH:mm"}}
                                    size="large" className="w-full"
                                    />
                            </Form.Item>
                        </Col>

                        <Col span={24} />

                        <Col xs={24} md={24} lg={24} xl={20} xxl={16}>
                            <Form.Item label={t("app.activities.cutoffTime")} colon>
                                <DatePicker
                                    value={activity.dateCutoff} onChange={activity.set_dateCutoff}
                                    disabledDate={disabledCutoffDate}
                                    size="large" className="w-full"
                                    format="YYYY-MM-DD HH:mm" showTime={{format: "HH:mm"}}
                                    />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Divider />
                    <Form.Item>
                        <span className="paddingRight-sm">
                            <Button size="large" onClick={handleClose}>{t("form.cancel")}</Button>
                        </span>
                        <span className="paddingRight-sm">
                            <Button htmlType="submit" loading={submitting}
                                size="large" type="primary"
                                >{t("form.save")}</Button>
                        </span>
                        {!sEditActivity.isCreateNew(aId) && (
                            <span className="paddingRight-sm">
                                <Button size="large" type="primary">
                                    <Link routeName="editActDoc" params={({activityId : String(aId)})}>
                                        {t("app.activities.Editor")}
                                    </Link>
                                </Button>
                            </span>
                        )}
                    </Form.Item>
                </Form>
                <CategoryModal/>
            </Card>
        </div>
    </BasicLayout>);
});

const CategoryModal: FC<{}> = observer(() => {
    const {t} = useTranslation();
    const store = useStore();
    const {sEditActivity, currentUser} = store;

    const onClose = useCallback(() => sEditActivity.set_isOpenCategoryModal(false), [sEditActivity]);
    const onCancel = useCallback(() => { sEditActivity.edittingCategory = undefined}, [sEditActivity]);

    const onSubmit = useCallback((event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const facultyId = currentUser!.facultyId;
        if(!facultyId || !sEditActivity.edittingCategory) return;
        Category.update({schoolId: store.currentSchoolId, facultyId}, sEditActivity.edittingCategory)
            .then(([err,data]) => {
                if(err){
                    Modal.error({content: err.message});
                    return;
                }
                setTimeout(message.success(t("app.activities.category.save", {name: sEditActivity.edittingCategory!.name}), 0), 1000);
                if(currentUser){
                    sEditActivity.loadCategories(currentUser.facultyId, true);
                }
                sEditActivity.edittingCategory = undefined;
            });
    }, [sEditActivity.edittingCategory]);

    const onRemove = useCallback((item:Category)=>{
        Modal.deleteConfirm({
            content: t("app.activities.category.removeConfirm", {name: item.name}),
            okText: t("app.activities.category.removeConfirm.yes"),
            okType: "danger",
            cancelText: t("app.activities.category.removeConfirm.no"),
            onOk() {
                Category.delete({schoolId: store.currentSchoolId, facultyId: currentUser!.facultyId}, item).then(([err,data]) => {
                    if(err){
                        Modal.error({content: err.message});
                        return;
                    }
                    setTimeout(message.success(t("app.activities.category.removed", {name: item.name}), 0), 1000);
                    if(currentUser){
                        sEditActivity.loadCategories(currentUser.facultyId, true);
                    }
                    sEditActivity.edittingCategory = undefined;
                })
            }
        })
    },[sEditActivity]);

    const columns = useMemo<Array<ColumnProps<Category>>>(() => [
            { title: t("app.activities.category.name"), key: "Name", render:(_, item, index) => (
                sEditActivity.isEdittingCategory(index)
                ? <Input size="large" value={sEditActivity.edittingCategory!.name} onChange={sEditActivity.edittingCategory!.set_name} required maxLength={200}/>
                : item.name), className: "whitespace-normal" },
            { title: t("app.activities.color"), key: "Color", render:(_, item, index) =>
                sEditActivity.isEdittingCategory(index)
                ? <ColorPicker color={sEditActivity.edittingCategory!.color} set_color={sEditActivity.edittingCategory!.set_color} isDisabled={false} colors={sEditActivity.categoryColorRange}/>
                : <ColorPicker color={item.color} set_color={item.set_color} isDisabled={true} colors={sEditActivity.categoryColorRange}/>
            },
            { title: t("app.activities.isGraded"), key: "IsGraded", render:(_, item, index) => (
                sEditActivity.isEdittingCategory(index)
                ? <Checkbox onChange={sEditActivity.edittingCategory!.set_isGraded} disabled={false} checked={sEditActivity.edittingCategory!.isGraded}></Checkbox>
                : <Checkbox onChange={item.set_isGraded} disabled={true} checked={item.isGraded}></Checkbox>), className:"text-center" },
            { title: t("app.activities.maxScore"), key: "MaxScore", render:(_, item, index) =>
                sEditActivity.isEdittingCategory(index)
                ? sEditActivity.edittingCategory!.isGraded && <InputNumber max={limitMaxScore} required={sEditActivity.edittingCategory!.isGraded} min={1}
                                    size="large" value={sEditActivity.edittingCategory!.maxScore} onChange={sEditActivity.edittingCategory!.set_maxScore}/>
                : item.isGraded && item.maxScore, className:"text-center" },
            { title: t("app.activities.weight"), key: "Weight", render:(_, item, index) =>
                sEditActivity.isEdittingCategory(index)
                ? sEditActivity.edittingCategory!.isGraded && <InputNumber max={limitWeight} required={sEditActivity.edittingCategory!.isGraded} min={1} size="large" value={sEditActivity.edittingCategory!.weight} onChange={sEditActivity.edittingCategory!.set_weight} step={0.1}/>
                : item.isGraded && item.weight, className:"text-center" },
            { title: "", key: "", render:(_, item) => (<div hidden={sEditActivity.edittingCategory != undefined}>
                    <a onClick={()=>{sEditActivity.edittingCategory=new Category(item)}} className="mr-lg">
                        <Tooltip title={t("app.activities.edit")}><Icon type="edit" className="icon-sm" /></Tooltip>
                    </a>
                    <a onClick={()=> onRemove(item)}>
                        <Tooltip title={t("app.activities.remove")}><Icon type="delete" className="icon-sm" /></Tooltip>
                    </a>
                </div>)}
        ],[sEditActivity.edittingCategory, sEditActivity.categories, sEditActivity]);

    //Re-render data in table
    for(var i = 0;i < sEditActivity.categories.length; i++) {
        sEditActivity.categories[i].color;
        sEditActivity.categories[i].name;
        sEditActivity.categories[i].isGraded;
        sEditActivity.categories[i].maxScore;
        sEditActivity.categories[i].weight;
    }
    if(sEditActivity.edittingCategory){
        sEditActivity.edittingCategory.color;
        sEditActivity.edittingCategory.name;
        sEditActivity.edittingCategory.isGraded;
        sEditActivity.edittingCategory.maxScore;
        sEditActivity.edittingCategory.weight;
    }
    return (<AntdModal
        title={t("app.activities.category.editor")}
        visible={sEditActivity.isOpenCategoryModal}
        onCancel={onClose}
        footer={sEditActivity.edittingCategory
            ? [
                <Button key="back" onClick={onCancel}>{t("form.cancel")}</Button>,
                <Button key="submit" type="primary" htmlType="submit" form="CategoryEditor">{t("form.save")}</Button>
            ] : [
                <Button key="close" onClick={onClose}>{t("form.close")}</Button>
            ]}>
            <Form onSubmit={onSubmit} id="CategoryEditor">
                {(sEditActivity.edittingCategory && sEditActivity.edittingCategory.activityCategoryId == DefaultId)
                    ? (<>
                        <Row gutter={16}>
                            <Col md={24}>
                                <Form.Item label={t("app.activities.category.name")} colon>
                                    <Input value={sEditActivity.edittingCategory.name} onChange={sEditActivity.edittingCategory.set_name} required maxLength={200}/>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col xs={6}>
                                <Form.Item label={t("app.activities.color")} colon>
                                    <ColorPicker color={sEditActivity.edittingCategory.color} set_color={sEditActivity.edittingCategory.set_color} isDisabled={false} colors={sEditActivity.categoryColorRange}/>
                                </Form.Item>
                            </Col>
                            <Col xs={6}>
                                <Form.Item label={t("app.activities.isGraded")} colon>
                                    <Checkbox checked={sEditActivity.edittingCategory.isGraded} onChange={sEditActivity.edittingCategory.set_isGraded} />
                                </Form.Item>
                            </Col>
                            <Col xs={6} hidden={!sEditActivity.edittingCategory.isGraded}>
                                <Form.Item label={t("app.activities.maxScore")} colon>
                                    <InputNumber max={limitMaxScore}
                                        value={sEditActivity.edittingCategory.maxScore} onChange={sEditActivity.edittingCategory.set_maxScore}
                                        required={sEditActivity.edittingCategory.isGraded} min={1}
                                        size="large" />
                                </Form.Item>
                            </Col>
                            <Col xs={6} hidden={!sEditActivity.edittingCategory.isGraded}>
                                <Form.Item label={t("app.activities.weight")} colon>
                                    <InputNumber max={limitWeight}
                                        value={sEditActivity.edittingCategory.weight} onChange={sEditActivity.edittingCategory.set_weight}
                                        required={sEditActivity.edittingCategory.isGraded} min={1}
                                        size="large" step={0.1} />
                                </Form.Item>
                            </Col>
                        </Row>
                    </>)
                    : !sEditActivity.edittingCategory && (
                    <Button onClick={()=>{sEditActivity.edittingCategory = new Category()}} type="primary" icon="plus" className="add-btn mb-sm">
                        {t("app.activities.category")}
                    </Button>)
                }
                <div>
                    <Table
                        dataSource={sEditActivity.categories}
                        columns={columns}
                        pagination={false}
                        rowKey={rowKey}
                    />
                </div>
            </Form>
    </AntdModal>);
});

function rowKey(record: Category) { return String(record.activityCategoryId) }
