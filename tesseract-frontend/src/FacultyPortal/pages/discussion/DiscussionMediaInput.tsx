import React, { FC, useCallback } from "react";
import { runInAction } from "mobx";
import { observer } from "mobx-react";

import { Discussion } from "../../../models/Discussion";

import { MediaInput } from "../presentation/MediaInput";
import { getMediaInfo } from "../../../utils/getMediaInfo";

export const DiscussionMediaInput: FC<{ item: Discussion }> = observer(({ item }) => {
    const onClearMedia = useCallback(() => {
        item.set_link("");
        item.set_mediaFile(undefined);
        item.clearMediaType();
    }, [item]);

    const onChangeLink = useCallback(async (v: string) => {
        item.set_link(v);
        if (!v) {
            if (!item.mediaFile) item.clearMediaType();
        } else {
            item.set_mediaFile(undefined);
            await sleep(1000); // wait till user stop input
            if (v != item.link) return; // user update link, the v is outdated
            if (v == item.origLink) return;
            const {mediaType, contentType} = await getMediaInfo(v);
            if (v != item.link) return; // user update link, the v is outdated
            runInAction(() => {
                item.mediaType   = mediaType;
                item.contentType = contentType;
                item.origLink    = v;
            });
        }
    }, [item]);

    return (
        <MediaInput
            link={item.link}
            mediaFile={item.mediaFile}
            mediaType={item.mediaType}
            onChangeLink={onChangeLink}
            onUploadFile={item.set_mediaFile}
            onClearMedia={onClearMedia}
            />
    );
});

function sleep(n:number) { return new Promise(r => setTimeout(r, n)) }
