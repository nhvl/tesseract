import React, { FC, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Discussion } from "../../../models/Discussion";

import { DiscussionListStore } from "../../stores/DiscussionListStore";

import { Button, Divider, Icon, } from "antd";

import { EditDiscussionModal } from "./EditDiscussionModal";

import {DiscussionItem} from "../../../components/Discussion/DiscussionItem";

export const DiscussionList: FC<{sDiscussionList: DiscussionListStore}> = observer(({sDiscussionList}) => {
    const {t} = useTranslation();

    return (<>
        <Button onClick={sDiscussionList.beginCreate}
            type="primary" icon="plus"
            className="add-btn mb-sm">{t("app.classes.discussions.addDiscussion")} </Button>
        <div>
            {sDiscussionList.displayItems.map(item => (
                <FacultyDiscussionItem
                    key       = {item.discussionId}
                    item      = {item}
                    unread    = {sDiscussionList.threadUnread.get(item.threadId) || 0}
                    onEdit    = {sDiscussionList.beginEdit}
                    onPublish = {sDiscussionList.publish}
                    onHide    = {sDiscussionList.hide}
                    onReopen  = {sDiscussionList.reopen}
                    onClose   = {sDiscussionList.close}
                    />
            ))}
        </div>

        <EditDiscussionModal item={sDiscussionList.item}
            onSubmit={sDiscussionList.create}
            onCancel={sDiscussionList.cancelCreate}
            colors={sDiscussionList.categoryColorRange}
            />
    </>);
});

const FacultyDiscussionItem: FC<{
    item     : Discussion,
    unread   : number,
    onEdit   : (_:Discussion) => void,
    onPublish: (_:Discussion) => void,
    onHide   : (_:Discussion) => void,
    onReopen : (_:Discussion) => void,
    onClose  : (_:Discussion) => void,
}> = observer((props) => {
    const {t} = useTranslation();

    const {item, onEdit} = props;

    const edit = useCallback(() => { onEdit(item) }, [item, onEdit]);

    return (
        <DiscussionItem {...props}>
            <Divider type="vertical" />
            <a onClick={edit}>{t("app.classes.discussions.editDiscussion")}</a>

            <Divider type="vertical" />
            <DicussionActions {...props} />
        </DiscussionItem>
    );
});

export const DicussionActions: FC<{
    item     : Discussion,
    onPublish: (_:Discussion) => void,
    onHide   : (_:Discussion) => void,
    onReopen : (_:Discussion) => void,
    onClose  : (_:Discussion) => void,
}> = observer(({item, onPublish, onHide, onReopen, onClose}) => {
    const {t} = useTranslation();
    const publish = useCallback(() => { onPublish(item) }, [item, onPublish]);
    const hide    = useCallback(() => { onHide   (item) }, [item, onHide   ]);
    const reopen  = useCallback(() => { onReopen (item) }, [item, onReopen ]);
    const close   = useCallback(() => { onClose  (item) }, [item, onClose  ]);

    return (
        !item.isPublish ? (<>
            <Icon type="check-circle" theme="filled" className="icon-sm text-grey mr-xs" />
            <a onClick={publish}><span className="text-link-grey underline">{t("app.classes.discussions.publishNow")}</span></a>
        </>) : (<>
            <Icon type="check-circle" theme="filled" className="icon-sm text-success mr-xs" />
            <a onClick={hide}><span className="text-link-grey underline">{t("app.classes.discussions.hide")}</span></a>
            <Divider type="vertical" />
            {item.isClosed ? (
                <a onClick={reopen}><span className="text-link-grey underline">{t("app.classes.discussions.reOpen")}</span></a>
            ) : (
                <a onClick={close}><span className="text-link-grey underline">{t("app.classes.discussions.close")}</span></a>
            )}
        </>)
    );
});
