import React, { FC, useMemo } from "react";
import { observer } from "mobx-react";

import { Discussion } from "../../../models/Discussion";

import { Table, Tooltip, Icon, } from "antd";
import { ColumnProps } from "antd/lib/table";

import { Link } from "../../../components/router/Links";
import { useTranslation } from "react-i18next";

export const DiscussionTable: FC<{items:Discussion[], onEdit:(_:Discussion) => void}> = observer(({items, onEdit}) => {
    const {t} = useTranslation()
    const columns = useMemo<ColumnProps<Discussion>[]>(() => [
        { title: t('app.classes.discussions.title'), key:"title", sorter:Discussion.sorter.title,
            render:(_, item) => (<Link routeName="discussion" params={item.params}>{item.title}</Link>) },
        { key:"actions", render:(_, item) => (<a onClick={() => onEdit(item)}><Tooltip title="Edit"><Icon type="edit" className="icon-sm" /></Tooltip></a>) },
    ], []);

    return (
        <div className="responsiveTable">
            <Table columns={columns} dataSource={items} pagination={false} rowKey={rowKey} />
        </div>
    );
});

function rowKey(record:Discussion) { return String(record.discussionId) }
