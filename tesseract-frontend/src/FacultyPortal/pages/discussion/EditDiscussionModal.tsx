import React, { FC, useCallback, FormEvent } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Discussion } from "../../../models/Discussion";

import { Form, Modal, Button } from "antd";
import { DefaultId } from "../../../models/types";
import { Input, Checkbox } from "../../../components/inputs/antd/Input";
import { HtmlInput } from "../../../components/inputs/HtmlInput";
import { InputNumber } from "../../../components/inputs/antd/InputNumber";
import { DatePicker } from "../../../components/inputs/antd/DatePicker";

import { PreviewMedia } from "../presentation/PreviewMedia";
import { DiscussionMediaInput } from "./DiscussionMediaInput";

import { ColorPicker  } from "../../../components/ColorPicker";

import styles from "./DiscussionList.module.less";

const limitMaxScore: number = 10000;
const limitWeight: number = 100;

export const EditDiscussionModal: FC<{item?:Discussion, onSubmit:() => void, onCancel:() => void, colors:string[]}> = observer(({item, onSubmit, onCancel, colors}) => {
    const {t} = useTranslation();

    const submitHandler = useCallback((event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        onSubmit();
    }, [onSubmit]);

    if (item == null) return null;

    return (
        <Modal visible={item != null}
            onCancel={onCancel}
            title={item.discussionId != DefaultId ? t('app.classes.discussions.editDiscussion') : t('app.classes.discussions.createDiscussion')}
            footer={[<Button key="back" onClick={onCancel}>{t("form.cancel")}</Button>,
            <Button key="submit" type="primary" htmlType="submit" form="DiscussionEditor">{t("form.save")}</Button>]}>
            <Form onSubmit={submitHandler} id="DiscussionEditor">
                <Form.Item label={t('app.classes.discussions.topicTitle')}>
                    <Input value={item.title} onChange={item.set_title}
                        required
                        />
                </Form.Item>
                <Form.Item label={t('app.classes.discussions.addMedia')} className={`${styles.addMedia}`}>
                    <PreviewMedia
                        mediaType={item.mediaType}
                        link     ={item.link     }
                        mediaFile={item.mediaFile}
                        onUpload ={item.set_mediaFile}
                    />
                    <DiscussionMediaInput item={item} />
                </Form.Item>
                <Form.Item label={t('app.classes.discussions.content')}>
                    <HtmlInput value={item.content} onChange={item.set_content}
                        />
                </Form.Item>
                <div className="flex">
                    <Form.Item label={t('app.classes.discussions.start')} className="mr-md">
                        <DatePicker value={item.startTime} onChange={item.set_startTime}
                            showTime />
                    </Form.Item>
                    <Form.Item label={t('app.classes.discussions.end')}>
                        <DatePicker value={item.endTime} onChange={item.set_endTime}
                        showTime />
                    </Form.Item>
                </div>
                <div className="flex">
                    <Form.Item label={t("app.activities.isGraded")} colon className="mr-md">
                        <Checkbox checked={item.isGraded} onChange={item.set_isGraded} />
                    </Form.Item>
                    <div className="flex" hidden={!item.isGraded}>
                        <Form.Item label={t("app.activities.color")} colon className="mr-md">
                            <ColorPicker color={item.color} set_color={item.set_color} colors={colors}/>
                        </Form.Item>
                        <Form.Item label={t("app.activities.maxScore")} colon className="mr-md">
                            <InputNumber max={limitMaxScore}
                                value={item.maxScore} onChange={item.set_maxScore}
                                required={item.isGraded} min={1}
                                size="large" />
                        </Form.Item>
                        <Form.Item label={t("app.activities.weight")} colon>
                            <InputNumber max={limitWeight}
                                value={item.weight} onChange={item.set_weight}
                                required={item.isGraded} min={1}
                                size="large" step={0.1} />
                        </Form.Item>
                    </div>
                </div>
            </Form>
        </Modal>
    );
});
