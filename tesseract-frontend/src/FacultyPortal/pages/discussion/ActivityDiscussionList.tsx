import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Card, Breadcrumb } from "antd";

import { useStore } from "../../stores";

import {showError} from "../../../services/api/ShowError";

import { BasicLayout } from "../../layouts/BasicLayout";

import {ClassActivityBreadcrumb} from "./../../components/ClassActivityBreadcrumb";

import { DiscussionList } from "./DiscussionList";

export const ActivityDiscussionPage: FC<{}> = observer(({}) => {
    const {sActivityDiscussionList, routerStore} = useStore();
    const {t} = useTranslation();

    const {activityId} = routerStore.routerState.params;
    const aId = !activityId ? NaN : Number(activityId);
    useEffect(() => {
        if (Number.isNaN(aId)) return;
        sActivityDiscussionList.init({activityId: aId}).then(err => {
            if (err) {
                showError(err, t);
                return;
            }
        });
    }, [aId]);

    const {activity, aClass} = sActivityDiscussionList;

    return (<BasicLayout>
        <ClassActivityBreadcrumb aClass={aClass} activity={activity} activityRouteName="activitySettings">
            <Breadcrumb.Item>{t('app.classes.menu.discussions')}</Breadcrumb.Item>
        </ClassActivityBreadcrumb>
        <Card>
            <DiscussionList sDiscussionList={sActivityDiscussionList} />
        </Card>
    </BasicLayout>);
});
