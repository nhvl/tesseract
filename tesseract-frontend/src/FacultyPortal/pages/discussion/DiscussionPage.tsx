import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Card, Spin, Breadcrumb, } from "antd";

import {showError} from "../../../services/api/ShowError";

import { BasicLayout } from "../../layouts/BasicLayout";
import { ClassActivityBreadcrumb } from "../../components/ClassActivityBreadcrumb";
import {ThreadView} from "../../components/thread/ThreadView";
import {DiscussionThread} from "../../components/DiscussionThread";
import { Link } from "../../../components/router/Links";
import { DefaultId, DbIdentity } from "../../../models/types";

import {DiscussionDetail} from "../../../components/Discussion/DiscussionDetail";

export const DiscussionPage: FC<{}> = observer(({}) => {
    const {sDiscussionThread, routerStore} = useStore();
    const {aClass, activity} = sDiscussionThread;
    const {t} = useTranslation();

    const {classId, discussionId} = routerStore.routerState.params;
    const cId = !classId ? NaN : Number(classId);
    const dId = !discussionId ? NaN : Number(discussionId);

    return (<BasicLayout>
        <ClassActivityBreadcrumb aClass={aClass} activity={activity} activityRouteName="activitySettings">
            <Breadcrumb.Item>
                {activity ? (<Link routeName="activityDiscussion" params={activity.params}>{t('app.classes.discussions.discussions')}</Link>)
                          : t('app.classes.discussions.discussions') }
            </Breadcrumb.Item>
        </ClassActivityBreadcrumb>
        <Card>
            <DiscussionThread classId={cId} discussionId={dId} />    
        </Card>
    </BasicLayout>);
});
