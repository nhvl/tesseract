import React from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { Button } from 'antd';
import { Link } from '../../../components/router/Links';
import {Exception} from '../../../components/Exception';

export const Exception500 = observer(() => {
  const [t] = useTranslation()
  return (<Exception
    type="500"
    desc={t('app.exception.description.500')}
    actions={<Link routeName="home"><Button type="primary">{t("app.exception.back")}</Button></Link>}
  />);
});
