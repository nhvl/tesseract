import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";

import { ActItem } from "../../../models/ActDoc";


import { EditGeneralItem } from "./EditGeneralItem";
import { GoogleDriveInputWrap } from "../../../components/GoogleApis/GoogleDriveInputWrap";
import { useStore } from "../../stores";
import { Modal } from "../../../components/Modal/ModalAntd";


export const EditGoogleDriveItem: FC<{ item: ActItem, dragHandleProps: any }> = observer(({ item, dragHandleProps }) => {
    const { sGoogleDriveStore } = useStore();
    // useEffect(() => {
    //     sGoogleDriveStore.init().then(err => {
    //         if(err) Modal.error({content: err.message});
    //     });
    // }, [item]);

    return (<EditGeneralItem item={item} dragHandleProps={dragHandleProps}>

        <GoogleDriveInputWrap gStore={sGoogleDriveStore} item={item}/>

    </EditGeneralItem>
    );
});