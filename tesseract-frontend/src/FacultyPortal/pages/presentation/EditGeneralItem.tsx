import React, { FC, useCallback, SyntheticEvent, MouseEvent } from "react";
import { observer } from "mobx-react";

import { ActItem } from "../../../models/ActDoc";

import { useStore } from "../../stores";

import { Icon, Typography, Collapse } from "antd";
const { Panel } = Collapse;
const { Title } = Typography;

import { Input } from "../../../components/inputs/antd/Input";

import { AddBtn } from "./AddBtn";
import { useTranslation } from "react-i18next";

import styles from "./EditActDoc.module.less";
import { SectionTimeRange } from "./SectionTimeRange";
import { Modal } from "../../../components/Modal/ModalAntd";

export const EditGeneralItem: FC<{ item: ActItem, dragHandleProps: any }> = observer(({ item, dragHandleProps, children }) => {
    const {t} = useTranslation();
    const { sEditActDoc } = useStore();
    const onRemove = useCallback((event:MouseEvent) => {
        event.stopPropagation();
        Modal.deleteConfirm({content: t("app.actDoc.editor.editor.removeConfirm"),
            okText: t('form.yes'),
            okType: 'danger',
            cancelText: t('form.no'),
            onOk() {
                sEditActDoc.removeItem(item);
            }
        }, t);
    }, [item]);
    const openSettingDialog = useCallback((event:MouseEvent) => {
        event.stopPropagation();
        sEditActDoc.beginEditItemSettings(item);
    }, [item]);

    return (<>
        <div className={`${styles.addMaterial} is-dragging-border`}>
            <Collapse defaultActiveKey={[item.id]}>
                <Panel
                    header={(<div className="flex-1">
                        <div className={styles.subTitle}>
                            <Title level={4}>
                                <Input
                                    onClick={stopPropagation}
                                    value={item.title} onChange={item.set_title}
                                    placeholder={item.typeString} />
                            </Title>
                        </div>
                        <SectionTimeRange item={item} />
                    </div>)}
                    extra={<>
                        <a className="ml-sm" onClick={onRemove}><Icon type="delete" className="icon-md" /></a>
                        <a className="ml-sm" onClick={openSettingDialog}><Icon type="setting" className="icon-md" /></a>
                    </>}
                    key={item.id}
                    >
                    {children}
                </Panel>
            </Collapse>
        </div>
    </>);
});

function stopPropagation(event:SyntheticEvent) {
    event.stopPropagation();
    // event.currentTarget.focus();
}
