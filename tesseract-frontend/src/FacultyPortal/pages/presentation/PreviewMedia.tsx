import React, { FC } from "react";
import { observer } from "mobx-react";

import { MediaTypeEnum } from "../../../models/MediaTypeEnum";

import ReactPlayer from "react-player";

import { ImageInput } from "../../../components/ImageInput";

import styles from "./EditActDoc.module.less";
import { UploadFile } from "antd/lib/upload/interface";
import { useTranslation } from "react-i18next";

export const PreviewMedia: FC<{
    mediaType : MediaTypeEnum,
    link      : string,
    mediaFile?: UploadFile,
    image    ?: string,
    imageFile?: UploadFile,
    onUpload ?: (_: UploadFile) => void,
}> = observer(({ mediaType, link, mediaFile, image, imageFile, onUpload }) => {
    const {t} = useTranslation();
    switch (mediaType) {
        case MediaTypeEnum.VideoFile:
        case MediaTypeEnum.VideoLink:
        case MediaTypeEnum.AudioLink: return (
            <div className="video">
                <ReactPlayer url={(mediaFile ? mediaFile.url : link)} controls />
            </div>
        );
        case MediaTypeEnum.AudioFile: return (
            <figure>
                <video controls className="imgFile"
                    src={(mediaFile ? mediaFile.url : link)}
                    poster={imageFile ? imageFile.url : image}
                />
            </figure>
        );
        case MediaTypeEnum.ImageLink:
        case MediaTypeEnum.ImageFile: return (
            ((link || mediaFile != null) ? (
                <div className="paddingBottom-md">
                    {onUpload ? (
                        <ImageInput className="imgFile-sm" image={link} imageFile={mediaFile!}
                            onChange={onUpload}
                            accept="image/*, video/*, audio/*"
                            createThumb={false} />
                    ) : (
                        <img src={mediaFile ? mediaFile.url : link} className="imgFile" />
                    )}
                </div>
            ) : null)
        );
        case MediaTypeEnum.Unsupport: return (
            !link ? null : (
                <div className="paddingBottom-md">
                    <label className="font-bold">{t('app.presentation.previewMedia.unsupportMedia')}:</label> {link}
                </div>
            )
        );
        case MediaTypeEnum.Unknown: return null;
        default: return (<>{t('app.presentation.previewMedia.todo')}: {mediaType}</>);
    }
});

