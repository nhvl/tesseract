import React, { FC, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { ActItem, PollQuizItem } from "../../../models/ActDoc";

import { Icon, Row, Col, } from "antd";
import { TextArea } from "../../../components/inputs/antd/Input";
import { TextAreaProps } from "antd/lib/input/TextArea";

import { EditGeneralContentItem } from "./EditGeneralContentItem";
import { ImageInput } from "../../../components/ImageInput";
import { DumbUpload } from "../../../components/inputs/antd/Upload";

const maxLength = 500;
const autoSize: TextAreaProps["autosize"] = ({ minRows: 1, maxRows: 5 });

export const EditPollQuiz: FC<{ item: ActItem, dragHandleProps:any }> = observer(({ item, dragHandleProps }) => {
    const { t } = useTranslation();
    const isAllOptionsEmpty = item.pollItems != null && item.pollItems.filter(x => !x.isRequired).length == 0;
    return (<EditGeneralContentItem item={item} dragHandleProps={dragHandleProps}>
        <div className="mt-md">
            <Row gutter={16} className="mb-sm flex items-center">
                <Col xs={16} md={20} lg={21} xl={22}>
                    <label className="font-bold">{t("form.activity.question.options")}</label>
                </Col>
            </Row>
            {item.pollItems && item.pollItems.map((pollItem: PollQuizItem, index: number) => (
                <PollRow item={item} index={index} key={index} isAllOptionsEmpty={isAllOptionsEmpty} />
            ))}
        </div>
    </EditGeneralContentItem>);
});

const PollRow: FC<{
    item: ActItem,
    index:number,
    isAllOptionsEmpty:boolean
}> = observer(({ item, index, isAllOptionsEmpty }) => {
    const { t } = useTranslation();
    const pollItems = item.pollItems![index];
    const removePollItem = useCallback(() => {
        if (item.pollItems) {
            item.pollItems.splice(index, 1);
        }
    }, [item, index]);
    const onFocusItem = useCallback(() => {
        if (item.pollItems && index >= (item.pollItems.length - 1)) {
            item.pollItems.push(new PollQuizItem());
        }
    }, [item]);
    return (
        <Row gutter={16} className={"paddingTop-sm paddingBottom-sm flex items-center border-bottom"}>
            <Col xs={20} md={22} xl={23}>
                {(pollItems.image || pollItems.imageFile) && (<div className="mb-sm">
                    <ImageInput image={pollItems.image} imageFile={pollItems.imageFile!} onChange={pollItems.set_imageFile} className="imgFile-sm" />
                    <a onClick={pollItems.clear_image}><Icon type="delete" className="icon-sm" /></a>
                </div>)}
                <div className="flex items-center">
                    {(!pollItems.image && !pollItems.imageFile) &&
                    (<DumbUpload onChange={pollItems.set_imageFile} createThumb>
                        <a><Icon type="picture" className="icon-sm mr-xs" /></a>
                    </DumbUpload>)}
                    <TextArea required={pollItems.isRequired && isAllOptionsEmpty}
                    value={pollItems.label} onChange={pollItems.set_label}
                    onFocus={onFocusItem}
                    maxLength={maxLength}
                    autosize={autoSize}
                    placeholder={t("form.activity.question.option.placeholder")} />
                </div>
            </Col>
            <Col xs={4} md={2} xl={1}>
                <a onClick={removePollItem} hidden={item.pollItems && item.pollItems.length <= 2}>
                    <Icon type="minus-square" className="icon-sm" />
                </a>
            </Col>
        </Row>
    );
});
