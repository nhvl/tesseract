import React, { FC, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { ActItem, ActOption } from "../../../models/ActDoc";

import { Icon, Row, Col, } from "antd";
import { TextArea, Checkbox } from "../../../components/inputs/antd/Input";
import { TextAreaProps } from "antd/lib/input/TextArea";

import styles from "./EditActDoc.module.less";
import { EditGeneralContentItem } from "./EditGeneralContentItem";
import { ImageInput } from "../../../components/ImageInput";
import { DumbUpload } from "../../../components/inputs/antd/Upload";
const maxLength = 500;
const autoSize: TextAreaProps["autosize"] = ({ minRows: 1, maxRows: 5 });

export const EditChoiceQuiz: FC<{ item: ActItem, dragHandleProps:any }> = observer(({ item, dragHandleProps }) => {
    const { t } = useTranslation();

    return (<EditGeneralContentItem item={item} dragHandleProps={dragHandleProps}>
        <div className="mt-md">
            <Checkbox checked={item.isMultipleAnswer} onChange={item.set_isMultipleAnswer}>{t('form.activity.question.option.allowMultiple')}</Checkbox>
        </div>
        <div className="mt-md">
            <Row gutter={16} className="mb-sm flex items-center">
                <Col xs={8} md={4} lg={3} xl={2} className="text-center">
                    <label className="font-bold">{t("form.activity.question.option.correctAnswer")}</label>
                </Col>
                <Col xs={16} md={20} lg={21} xl={22}>
                    <label className="font-bold">{t("form.activity.question.options")}</label>
                </Col>
            </Row>
            {item.options && item.options.map((option: ActOption, index: number) => (
                <OptionRow item={item} index={index} key={index} />
            ))}
        </div>
    </EditGeneralContentItem>);
});

const OptionRow: FC<{
    item: ActItem,
    index:number
}> = observer(({ item, index }) => {
    const option = item.options![index];

    const removeOption = useCallback(() => {
        if (item.options) {
            item.options.splice(index, 1);
        }
    }, [item, index]);
    const onFocusOption = useCallback((index: number) => {
        if (item.options && index >= (item.options.length - 1)) {
            item.options.push(new ActOption());
        }
    }, [item]);
    return (
        <Row gutter={16} className={option.isCorrect ? "paddingTop-sm paddingBottom-sm flex items-center border-bottom selected" : "paddingTop-sm paddingBottom-sm flex items-center border-bottom"}>
            <Col xs={8} md={4} lg={3} xl={2} className="text-center">
                <Checkbox checked={option.isCorrect} className={styles.checkboxMultiple} onChange={(v:boolean)=>item.set_multipleChoice_correctAnswers(v, index)} />
            </Col>
            <Col xs={12} md={18} lg={19} xl={21}>
                <InputMChoice option={option} index={index} onFocusOption={onFocusOption} />
            </Col>
            <Col xs={4} md={2} lg={2} xl={1}>
                <a onClick={removeOption} hidden={!item.is_multipleChoice_deleteAble(index)}>
                    <Icon type="minus-square" className="icon-sm" />
                </a>
            </Col>
        </Row>
    );
});

const InputMChoice: FC<{
    option: ActOption, index: number,
    onFocusOption:(index: number) => void,
}> = observer(({ option, index, onFocusOption }) => {
    const { t } = useTranslation();
    const onFocus = useCallback(() => { onFocusOption(index); }, [onFocusOption, index]);

    return (<>
        {(option.image || option.imageFile) && (<div className="mb-sm">
            <ImageInput image={option.image} imageFile={option.imageFile!} onChange={option.set_imageFile} className="imgFile-sm" />
            <a onClick={option.clear_image}><Icon type="delete" className="icon-sm" /></a>
        </div>)}
        <div className="flex items-center">
            {(!option.image && !option.imageFile) &&
            (<DumbUpload onChange={option.set_imageFile} createThumb>
                <a><Icon type="picture" className="icon-sm mr-xs" /></a>
            </DumbUpload>)}
            <TextArea required={option.isRequired}
            value={option.label} onChange={option.set_label}
            onFocus={onFocus}
            maxLength={maxLength}
            autosize={autoSize}
            placeholder={t("form.activity.question.option.placeholder")} />
        </div>
    </>);
});

