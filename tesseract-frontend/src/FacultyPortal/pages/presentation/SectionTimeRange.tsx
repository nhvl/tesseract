import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { ActItem } from '../../../models/ActDoc';

import { DateTime } from "../../../components/DateTime/DateTime";

import styles from "./EditActDoc.module.less";
import { useTranslation } from 'react-i18next';

export const SectionTimeRange: FC<{item:ActItem}> = observer(({item}) => {
    const {t} = useTranslation();

    return (
        <div className={`${styles.timeRange} flex font-size-sm`}>
            {item.startDate && <div className="flex mr-sm">
                <label className="mr-xs">{t('app.presentation.sectionTimeRange.start')}</label><DateTime value={item.startDate}/>
            </div>}
            {item.endDate && <div className="flex mr-sm">
                <label className="mr-xs">{t('app.presentation.sectionTimeRange.end')}</label><DateTime value={item.endDate}/>
            </div>}
        </div>
    );
});
