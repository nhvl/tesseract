import React, { FC, useEffect, useCallback } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { useStore } from '../../stores';

import { Card, Skeleton, Breadcrumb, Button, Divider, } from 'antd';

import { BasicLayout } from '../../layouts/BasicLayout';

import { Link } from '../../../components/router/Links';
import { ActivityBreadcrumb } from '../../components/ActivityBreadcrumb';

import { ViewActDoc } from '../../../components/ViewActDoc/ViewActDoc';
import { Modal } from '../../../components/Modal/ModalAntd';

export const ViewActDocPage: FC<{}> = observer(() => {
    const { t } = useTranslation();
    const { sEditActDoc, routerStore } = useStore();
    const sActivityId = routerStore.routerState.params.activityId;
    useEffect(() => {
        if (!sActivityId) { routerStore.goTo("home"); return; }
        const activityId = Number(sActivityId);
        if (Number.isNaN(activityId)) { routerStore.goTo("home"); return; }
        sEditActDoc.init(activityId).then((err) => {
            if (!err) return;
            Modal.error({ content: err.message, onOk() { routerStore.goTo("home") } });
        });
        window.scrollTo(0, 0);
    }, [sActivityId]);

    const goBack = useCallback(() => {
        if (history.length > 1) history.back();
        else routerStore.goTo("editActDoc", routerStore.routerState.params)
    }, [routerStore]);

    const { doc } = sEditActDoc;
    if (doc == null) return (<Skeleton />);

    return (<BasicLayout>
        <ActivityBreadcrumb aClass={sEditActDoc.aClass} activity={sEditActDoc.activity}>
            <Breadcrumb.Item><Link routeName="editActDoc" params={routerStore.routerState.params}>
                {t("app.actDoc.editor.editor")}
            </Link></Breadcrumb.Item>
            <Breadcrumb.Item>{t("app.actDoc.preview.preview")}</Breadcrumb.Item>
        </ActivityBreadcrumb>

        <Card>
            <ViewActDoc doc={doc} />
            <Divider />
            <Button size="large" className="paddingRight-sm" onClick={goBack}>{t("app.actDoc.preview.back")}</Button>
        </Card>
    </BasicLayout>);
});
