import React, { FC } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { ActItem } from "../../../models/ActDoc";

import { useStore } from "../../stores";

import { InputNumber } from "../../../components/inputs/antd/InputNumber";

import { EditGeneralContentItem } from "./EditGeneralContentItem";
import styles from "./EditActDoc.module.less";

export const EditNumericQuestion: FC<{ item: ActItem, dragHandleProps:any }> = observer(({ item, dragHandleProps }) => {
    const { t } = useTranslation();
    const { sEditActDoc } = useStore();

    return (<EditGeneralContentItem item={item} dragHandleProps={dragHandleProps}>
        <div className="flex items-center paddingTop-md paddingBottom-lg">
            <label className="text-black font-medium mr-xs">{t("form.activity.question.numeric.correctResponse")}</label>
            <div className={styles.response}>
                <InputNumber step={sEditActDoc.numericStep} size="large" value={item.numericMin} required={item.isEmpty(item.numericMin) && item.isEmpty(item.numericMax)} onChange={item.set_numericMin} onBlur={item.populateMaxValue} placeholder={t("form.activity.question.numeric.minValue")} />
            </div>
            <div className={styles.response}>
                <InputNumber step={sEditActDoc.numericStep} size="large" value={item.numericMax} required={item.isEmpty(item.numericMin) && item.isEmpty(item.numericMax)} onChange={item.set_numericMax} onBlur={item.populateMinValue} placeholder={t("form.activity.question.numeric.maxValue")} />
            </div>
        </div>
        <p className="font-medium">{t("form.activity.question.numeric.note")}</p>
        <ul>
            <li>{t("form.activity.question.numeric.note1")}</li>
            <li>{t("form.activity.question.numeric.note2")}</li>
            <li>{t("form.activity.question.numeric.note3")}</li>
        </ul>
    </EditGeneralContentItem>);
});
