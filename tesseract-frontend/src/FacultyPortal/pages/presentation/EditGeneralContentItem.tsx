import React, { FC, useState, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { ActItem } from "../../../models/ActDoc";

import { Icon } from "antd";

import { DumbUpload } from "../../../components/inputs/antd/Upload";
import { ImageInput } from "../../../components/ImageInput";
import { HtmlInput } from "../../../components/inputs/HtmlInput";

import { EditGeneralItem } from "./EditGeneralItem";

export const EditGeneralContentItem: FC<{ item: ActItem, dragHandleProps: any }> = observer(({ item, dragHandleProps, children }) => {
    const { t } = useTranslation();
    const [, setEditting] = useState(false);
    const endEdit = useCallback(() => setEditting(false), [setEditting]);
    const onRemoveImg = useCallback(() => { item.set_image(""); item.set_imageFile(undefined) }, [item]);
    return (
        <EditGeneralItem item={item} dragHandleProps={dragHandleProps}>
            {(item.image || item.imageFile) && (
                <div className="paddingBottom-lg">
                    <ImageInput image={item.image} imageFile={item.imageFile!} onChange={item.set_imageFile} />
                    <div className="paddingTop-sm">
                        <a onClick={onRemoveImg}><Icon type="delete" className="icon-sm" /></a>
                    </div>
                </div>
            )}

            <HtmlInput value={item.content} onChange={item.set_content}
                onBlur={endEdit}
                placeholder={t("form.activity.description.placeholder")}
            />

            {children}

            {(!item.image && !item.imageFile) && (
                <div className="paddingTop-sm">
                    <DumbUpload onChange={item.set_imageFile} createThumb>
                        <a><Icon type="picture" className="icon-sm" /></a>
                    </DumbUpload>
                </div>
            )}
        </EditGeneralItem>
    );
});


