import React, { FC, useCallback } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { ActItem, MatchQuizzItem } from "../../../models/ActDoc";

import { Icon } from "antd";

import { TextArea } from "../../../components/inputs/antd/Input";


import { EditGeneralContentItem } from "./EditGeneralContentItem";

import { DumbUpload } from "../../../components/inputs/antd/Upload";
import { ImageInput } from "../../../components/ImageInput";

export const EditMatchingQuiz: FC<{ item: ActItem, dragHandleProps: any }> = observer(({ item, dragHandleProps }) => {
    const { t } = useTranslation();
    const onFocus = useCallback((index:number) => {
        if (item.matchQuizzItems && index >= (item.matchQuizzItems.length - 1)) {
            item.matchQuizzItems.push(new MatchQuizzItem());
        }
    }, [item]);
    const isAllOptionsEmpty = item.matchQuizzItems.filter(x => (x.hasLeftValue && x.hasRightValue)).length == 0;

    return (
        <EditGeneralContentItem item={item} dragHandleProps={dragHandleProps}>
            <div className="flex mt-sm mb-sm">
                <div className="flex-1 mr-sm">
                    <label className="font-bold">{t('app.presentation.editMatchingQuiz.question')}</label>
                </div>
                <div className="flex-1">
                    <label className="font-bold">{t('app.presentation.editMatchingQuiz.answer')}</label>
                </div>
            </div>
            {item.matchQuizzItems.map((result, index) => (
                <MatchingQuizRow key={index} item={result} onFocus={() => onFocus(index)}
                    onRemove={item.removeMathQuizzItem} isAllOptionsEmpty={isAllOptionsEmpty}
                    isDeleteAble={item.matchQuizzItems.length > 1}  />
            ))}
        </EditGeneralContentItem>
    );
});

const MatchingQuizRow: FC<{
    item: MatchQuizzItem,
    onRemove?: (item: MatchQuizzItem) => void,
    onFocus ?:() => void,
    isAllOptionsEmpty:boolean,
    isDeleteAble : boolean
}> = observer(({ item, onRemove, onFocus, isAllOptionsEmpty, isDeleteAble }) => {
    const { t } = useTranslation();
    const handleRemove = useCallback(() => { if (onRemove) onRemove(item); }, [item, onRemove]);
    const handleFocus = useCallback(() => { if (onFocus) onFocus() }, [onFocus]);
    return (
        <div className="flex items-center border-bottom mb-sm paddingBottom-sm">
            <div className="flex-1">
                {(item.leftImage || item.leftImageFile) && (<div className="mb-sm">
                    <ImageInput image={item.leftImage} imageFile={item.leftImageFile!} onChange={item.set_leftImageFile}  className="imgFile-sm"/>
                    <a onClick={item.clear_leftImage}><Icon type="delete" className="icon-sm" /></a>
                </div>)}
                <div className="flex items-center">
                    {(!item.leftImage && !item.leftImageFile) &&
                    (<DumbUpload onChange={item.set_leftImageFile} createThumb>
                        <a className="leading-none"><Icon type="picture" className="icon-sm mr-xs" /></a>
                    </DumbUpload>)}
                    <TextArea value={item.left} onChange={item.set_left}
                    required={ item.isLeftRequired || (!item.hasLeftValue && isAllOptionsEmpty) }
                    placeholder={t('form.MathQuizz.leftValue.placeholder')}
                    autosize={{ minRows: 1, maxRows: 8 } } />
                </div>
            </div>
            <div className="ml-sm mr-sm">
                <Icon type="swap" />
            </div>
            <div className="flex-1">
                {(item.rightImage || item.rightImageFile) && (<div className="mb-sm">
                    <ImageInput image={item.rightImage} imageFile={item.rightImageFile!} onChange={item.set_rightImageFile} className="imgFile-sm" />
                    <a onClick={item.clear_rightImage}><Icon type="delete" className="icon-sm" /></a>
                </div>)}
                <div className="flex items-center">
                    {(!item.rightImage && !item.rightImageFile) &&
                    (<DumbUpload onChange={item.set_rightImageFile} createThumb>
                        <a className="leading-none"><Icon type="picture" className="icon-sm mr-xs" /></a>
                    </DumbUpload>)}
                    <TextArea value={item.right} onChange={item.set_right}
                    required={ item.isRightRequired || (!item.hasRightValue && isAllOptionsEmpty) }
                    onFocus={handleFocus}
                    placeholder={t('form.MathQuizz.rightValue.placeholder')}
                    autosize={{ minRows: 1, maxRows: 8 }} />
                </div>
            </div>
            <div className="ml-sm"><a onClick={handleRemove} hidden={!isDeleteAble}><Icon type="minus-square" className="icon-sm" /></a></div>
        </div>
    );
});
