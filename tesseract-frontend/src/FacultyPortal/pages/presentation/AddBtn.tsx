import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { ActItem } from '../../../models/ActDoc';

import { useStore } from '../../stores';

import { Popover, Divider, Row, Col, Button, } from 'antd';

import styles from "./EditActDoc.module.less";
import { useTranslation } from 'react-i18next';

export const AddBtn: FC<{item?:ActItem}> = observer(({item}) => {
    const {t} = useTranslation();
    const {sEditActDoc: sEditPresentation} = useStore();

    return (<>
        <Popover content={(<Row gutter={24}>
            <Col xs={24} sm={12} className={styles.column}>
                <ul className="list-reset">
                    <li className="font-size-lg none">{t('app.presentation.addBtn.content')}</li>
                    <Divider />
                    <li><a onClick={() => sEditPresentation.addHeading   (item)}><i className="fas fa-heading"    /><span className="paddingLeft-sm">{t('app.presentation.addBtn.heading')}</span></a></li>
                    <li><a onClick={() => sEditPresentation.addText      (item)}><i className="fas fa-text-width" /><span className="paddingLeft-sm">{t('app.presentation.addBtn.text')}</span></a></li>
                    <li><a onClick={() => sEditPresentation.addMedia     (item)}><i className="fas fa-image"      /><span className="paddingLeft-sm">{t('app.presentation.addBtn.multimedia')}</span></a></li>
                    <li><a onClick={() => sEditPresentation.addDiscussion(item)}><i className="fas fa-comments"   /><span className="paddingLeft-sm">{t('app.presentation.addBtn.discussion')}</span></a></li>
                    <li><a onClick={() => sEditPresentation.addEmbed(item)}><i className="fab fa-google-drive"          /><span className="paddingLeft-sm">{t('app.presentation.addBtn.googleDrive')}</span></a></li>
                </ul>
            </Col>
            <Col xs={24} sm={12} className={styles.column}>
                <ul className="list-reset">
                    <li className="font-size-lg none">{t('app.presentation.addBtn.questions')}</li>
                    <Divider />
                    <li><a onClick={() => sEditPresentation.addCQuiz     (item)}><i className="fas fa-tasks"           /><span className="paddingLeft-sm">{t('app.presentation.addBtn.multipleChoice')}</span></a></li>
                    <li><a onClick={() => sEditPresentation.addTQuiz     (item)}><i className="fas fa-align-left"      /><span className="paddingLeft-sm">{t('app.presentation.addBtn.questionText')}</span></a></li>
                    <li><a onClick={() => sEditPresentation.addNumQuiz   (item)}><i className="fas fa-sort-numeric-up" /><span className="paddingLeft-sm">{t('app.presentation.addBtn.numeric')}</span></a></li>
                    <li><a onClick={() => sEditPresentation.addMQuiz     (item)}><i className="fas fa-list-ol"         /><span className="paddingLeft-sm">{t('app.presentation.addBtn.matchingOptions')}</span></a></li>
                    <li><a onClick={() => sEditPresentation.addPollQuiz  (item)}><i className="fas fa-vote-yea"        /><span className="paddingLeft-sm">{t('app.presentation.addBtn.polling')}</span></a></li>
                </ul>
            </Col>
        </Row>)}>
            <Button type="primary" icon="plus" className="add-btn mt-lg mb-lg">{t('app.presentation.addBtn.insertContent')}</Button>
        </Popover>
    </>);
});
