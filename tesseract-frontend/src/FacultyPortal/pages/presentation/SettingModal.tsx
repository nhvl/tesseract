import React, { FC, useCallback, useState, useEffect } from "react";
import { observer } from "mobx-react";
import moment from "moment";

import { ActItem } from "../../../models/ActDoc";

import { useStore } from "../../stores";

import { Modal, Form } from "antd";
import { DatePicker } from "../../../components/inputs/antd/DatePicker";

import styles from "./EditActDoc.module.less";

export const SettingModal: FC<{ item?: ActItem }> = observer(({ item }) => {
    const { sEditActDoc } = useStore();
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();

    useEffect(() => {
        if (item == null) return;
        setStartDate(item.startDate);
        setEndDate(item.endDate);
    }, [sEditActDoc, item]);

    const onCancel = useCallback(() => {
        sEditActDoc.beginEditItemSettings(undefined);
    }, [sEditActDoc]);

    const onOk = useCallback(async () => {
        if (item == null) return;
        item.set_startDate(startDate);
        item.set_endDate(endDate);
        sEditActDoc.beginEditItemSettings(undefined);
    }, [sEditActDoc, item, startDate, endDate]);

    const disabledStartDate = useCallback((startValue) => {
        const endValue = endDate;
        if (!startValue || !endValue) return false;
        return startValue.valueOf() > endValue.valueOf();
    }, [item, endDate]);

    const disabledEndDate = useCallback((endValue) => {
        const startValue = startDate;
        if (!endValue || !startValue) return false;
        return endValue.valueOf() <= startValue.valueOf();
    }, [item, startDate]);


    const disabledStartTime = useCallback((selectedDate) => {
        const hour = moment(endDate).hour();
        const minute = moment(endDate).minute();
        return {
            disabledHours: () => range(hour + 1, 24),
            disabledMinutes: () => {
                const selectedHour = moment(selectedDate).hour();
                if (selectedHour == hour) return range(minute - 1, 60);
                if (selectedHour > hour) return range(0, 60);
                return [];
            },
            disabledSeconds: () => range(0, 60),
        }
    }, [item, endDate]);

    const disabledEndTime = useCallback((selectedDate) => {
        const hour = moment(startDate).hour();
        const minute = moment(startDate).minute();
        return {
            disabledHours: () => range(0, hour),
            disabledMinutes: () => {
                const selectedHour = moment(selectedDate).hour();
                if (selectedHour == hour) return range(0, minute + 1);
                if (selectedHour < hour) return range(0, 60);
                return [];
            },
            disabledSeconds: () => range(0, 60),
        }
    }, [item, startDate]);

    if (item == null) return null;

    return (
        <Modal title="Time Range"
            onOk={onOk}
            onCancel={onCancel}
            visible>
            <div className={`${styles.timeRange} flex`}>
                <Form.Item label={"Start Date"} className="mr-md">
                    <DatePicker
                        value={startDate} onChange={setStartDate}
                        showTime
                        disabledDate={disabledStartDate}
                        disabledTime={disabledStartTime}
                    />
                </Form.Item>
                <Form.Item label={"End Date"}>
                    <DatePicker
                        value={endDate} onChange={setEndDate}
                        showTime
                        disabledDate={disabledEndDate}
                        disabledTime={disabledEndTime}
                    />
                </Form.Item>
            </div>
        </Modal>)
});

function range(start: number, end: number) {
    const result = [];
    for (let i = start; i < end; i++) {
        result.push(i);
    }
    return result;
}
