import React, { FC, useCallback, useEffect, FormEvent } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";
import classNames from "classnames";

import { AutosaveDuration } from "../../../config";

import { ActItem, EActItemType, ActDoc } from "../../../models/ActDoc";

import { useStore } from "../../stores";

import { Card, Button, Icon, Divider, Skeleton, Form, message } from "antd";
import { DragDropContext, Droppable, Draggable, DragDropContextProps, DroppableProvided, DroppableStateSnapshot } from "react-beautiful-dnd";

import { HtmlInput } from "../../../components/inputs/HtmlInput";
import { ImageInput } from "../../../components/ImageInput";

import { EditGeneralContentItem } from "./EditGeneralContentItem";
import { EditHeading } from "./EditHeading";
import { EditMediaItem } from "./EditMediaItem";
import { EditDiscussionItem } from "./EditDiscussionItem";
import { EditMatchingQuiz } from "./EditMatchingQuiz";
import { EditChoiceQuiz } from "./EditChoiceQuiz";
import { EditNumericQuestion } from "./EditNumericQuestion";
import { EditPollQuiz } from "./EditPollQuiz";

import { AddBtn } from "./AddBtn";
import { SettingModal } from "./SettingModal";

import styles from "./EditActDoc.module.less";
import "./qlSummary.less";
import { Modal } from "../../../components/Modal/ModalAntd";
import { EditGoogleDriveItem } from "./EditGoogleDriveItem";

export const EditActDoc: FC<{}> = observer(({ }) => {
    const { t } = useTranslation();
    const { sEditActDoc, routerStore, sGoogleDriveStore } = useStore();
    const sActivityId = routerStore.routerState.params.activityId;
    useEffect(() => {
        const activityId = !sActivityId ? NaN : Number(sActivityId);
        if (Number.isNaN(activityId)) { routerStore.goTo("home"); return; }
        sEditActDoc.init(activityId).then((err) => {
            if (!err) return;
            Modal.error({ content: err.message, onOk() { routerStore.goTo("home") } });
        });
        sGoogleDriveStore.init().then(err => {
            if(err) Modal.error({content: err.message});
        });
    }, [sActivityId]);

    const handleClose = useCallback(() => {
        if (history.length > 1) history.back();
        else routerStore.goTo("home");
    }, []);

    const autoSave = useCallback(async () => {
        if (sEditActDoc.loading) return;
        if (!sEditActDoc.isChanged) return;
        sEditActDoc.enable_numericStep(false);
        const formItem = (document.getElementsByName("EditActDocForm")[0] as HTMLFormElement);
        const isValid = formItem ? formItem.checkValidity() : true;
        sEditActDoc.enable_numericStep(true);
        if (!isValid) return;
        const hideSaving = message.info(t("app.actDoc.editor.editor.saveAuto"), 0);
        doSave().then(err => {
            hideSaving();
            if (err) {
                Modal.error({ title: t("app.activities.actDoc.save.failed"), content: err.message });
            } else {
                message.success(t("app.actDoc.editor.editor.saveSuccess"), 2);
            }
        });
    }, [sEditActDoc]);

    useEffect(() => {
        let h = setTimeout(async function f() {
            await autoSave();
            h = setTimeout(f, AutosaveDuration);
        }, AutosaveDuration);
        return (() => clearInterval(h));
    }, [autoSave])

    const doSave = useCallback(async () => {
        const err = await sEditActDoc.save();
        sEditActDoc.enable_numericStep(true);
        return err;
    }, [sEditActDoc]);

    const handleSubmit = useCallback(async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (sEditActDoc.loading) return;
        const isChanged = sEditActDoc.isChanged;
        let err: Error|undefined = undefined;
        if(isChanged) err = await doSave();
        if (err) {
            Modal.error({ title: t("app.activities.actDoc.save.failed"), content: err.message });
        } else {
            if (isChanged)
                message.success(t("app.actDoc.editor.editor.saveSuccess"), 2);

            if (sEditActDoc.isSavePreview) {
                routerStore.goTo("viewActDoc", routerStore.routerState.params);
            }
        }
    }, []);

    const onSave = useCallback(() => {
        //set up variables before submitting.
        sEditActDoc.enable_numericStep(false);
        sEditActDoc.set_isSavePreview(false);
    }, [sEditActDoc]);
    const onSavePreview = useCallback(() => {
        //set up variables before submitting.
        sEditActDoc.enable_numericStep(false);
        sEditActDoc.set_isSavePreview(true);
    }, [sEditActDoc]);

    const dragEndHandler:DragDropContextProps["onDragEnd"] = useCallback((result) => {
        if (!result.destination) return;
        sEditActDoc.onDrag(result.source.index, result.destination.index);
    }, []);

    const { doc } = sEditActDoc;
    if (doc == null) return (<Skeleton />);

    return (<Card>
        <Form onSubmit={handleSubmit} name="EditActDocForm">
            <Card className="none-shadow paddingBottom-lg"
                cover={(
                    <div className={styles.banner}>
                        <ImageInput placeholder={t("app.actDoc.editor.editor.clickToAddBannerImage")} image={doc.banner} imageFile={sEditActDoc.bannerFile!} onChange={sEditActDoc.set_bannerFile} />
                        {(!!doc.banner || sEditActDoc.bannerFile != null) && (
                            <div className="paddingTop-sm"><a onClick={sEditActDoc.removeBanner}><Icon type="delete" className="icon-sm" /></a></div>
                        )}
                    </div>
                )}>
                <div className="editable">
                    <HtmlInput theme="bubble"
                        value={doc.title} onChange={doc.set_title}
                        placeholder={t("form.activity.title.placeholder")}
                        className="ql-title paddingRight-xs"
                    />
                    <div className="iconEdit"><i className="fas fa-pen"></i></div>
                </div>
                <div className="editable summary">
                    <HtmlInput theme="bubble"
                        value={doc.summary} onChange={doc.set_summary}
                        placeholder={t("form.activity.description.placeholder")}
                        className="ql-summary paddingRight-xs"
                    />
                    <div className="iconEdit"><i className="fas fa-pen"></i></div>
                </div>
            </Card>

            <div className="editor-drag-and-drop">
                <DragDropContext onDragEnd={dragEndHandler}>
                    <Droppable droppableId="droppable">{(provided, snapshot) => (
                        <DroppableInner
                            provided={provided}
                            snapshot={snapshot}
                            doc={doc}
                            />
                    )}</Droppable>
                </DragDropContext>
            </div>
            <AddBtn />
            <Divider />
            <span className="paddingRight-sm"><Button size="large" onClick={handleClose}>{t("form.cancel")}</Button></span>
            <span className="paddingRight-sm"><Button htmlType="submit" type="primary" size="large" onClick={onSave} disabled={!sEditActDoc.isChanged}>{t("form.save")}</Button></span>
            <span className="paddingRight-sm"><Button htmlType="submit" type="primary" size="large" onClick={onSavePreview}>{sEditActDoc.isChanged ? t("app.actDoc.editor.editor.SavePreview") : t("app.actDoc.preview.preview")}</Button></span>
        </Form>

        <SettingModal item={sEditActDoc.edittingSettingItem} />
    </Card>);
});

const DroppableInner:FC<{provided:DroppableProvided, snapshot:DroppableStateSnapshot, doc:ActDoc}> = observer(({provided, snapshot, doc}) => {
    return (
        <div ref={provided.innerRef}
            {...provided.droppableProps}
            className={classNames({isDraggingOver:snapshot.isDraggingOver})}
            >
            {doc.items.map((item, index) => (
                <DragableEditItem key={item.id} item={item} index={index} />
            ))}
            {provided.placeholder}
        </div>
    );
});

const DragableEditItem: FC<{ item: ActItem, index: number }> = observer(({item, index}) => {
    return (
        <Draggable draggableId={item.id} index={index}>{(provided, snapshot) => (
            <div ref={provided.innerRef}
                {...provided.draggableProps}
                className={classNames("wrapper mb-lg", {'is-dragging':snapshot.isDragging})}
                style={provided.draggableProps.style}
                >
                <div {...provided.dragHandleProps} className="is-dragging">
                    <Icon type="more" className="icon-sm" />
                </div>
                <EditItem item={item} dragHandleProps={provided.dragHandleProps} />
            </div>
        )}</Draggable>
    );
});

const EditItem: FC<{ item: ActItem, dragHandleProps: any }> = observer((props) => {
    switch (props.item.type) {
        case EActItemType.Heading        : return (<EditHeading            {...props} />);
        case EActItemType.Media          : return (<EditMediaItem          {...props} />);
        case EActItemType.Embed          : return (<EditGoogleDriveItem    {...props} />);
        case EActItemType.MatchQuiz      : return (<EditMatchingQuiz       {...props} />);
        case EActItemType.NumericQuestion: return (<EditNumericQuestion    {...props} />);
        case EActItemType.ChoiceQuiz     : return (<EditChoiceQuiz         {...props} />);
        case EActItemType.PollQuiz       : return (<EditPollQuiz           {...props} />);
        case EActItemType.Discussion     : return (<EditDiscussionItem     {...props} />);
        case EActItemType.TextQuiz       :
        default                          : return (<EditGeneralContentItem {...props} />);
    }
});
