import React, { FC, useCallback, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { MediaTypeEnum } from "../../../models/MediaTypeEnum";

import { Button, Icon, Divider, Modal, Tooltip } from "antd";
import { UploadFile } from "antd/lib/upload/interface";

import { Input } from "../../../components/inputs/antd/Input";
import { DumbUpload } from "../../../components/inputs/antd/Upload";
import { VideoRecordButton } from "../../../components/inputs/VideoRecordButton";
import { AudioRecordButton } from "../../../components/inputs/AudioRecordButton";

export const MediaInput: FC<{
    link           : string,
    mediaFile     ?: UploadFile,
    mediaType      : MediaTypeEnum,
    onClearMedia   : () => void,
    onChangeLink   : (_:string)     => void,
    onUploadFile   : (_:UploadFile) => void,
    onUploadPoster?: (_:UploadFile) => void,
}> = observer(({
    link, mediaFile, mediaType,
    onClearMedia,
    onChangeLink,
    onUploadFile,
    onUploadPoster,
}) => {
    const { t } = useTranslation();

    const onRecorded = useCallback((file:File) => {
        onUploadFile(file);
        // onUploadPoster(thumbnailBlob);
    }, [onUploadFile]);

    return (
        <div className="paddingBottom-lg">
            {(link || mediaFile) && (
                <div className="paddingBottom-sm">
                    <a onClick={onClearMedia} title="Clear media"><Icon type="delete" className="icon-sm" /></a>
                    <span className="paddingLeft-xs">{mediaFile ? mediaFile.name : "" }</span>
                </div>
            )}
            <div className="paddingBottom-sm">
                <DumbUpload onChange={onUploadFile} accept="image/*, video/*, audio/*">
                    <Button><Icon type="upload" />{t('app.presentation.mediaInput.upload')}</Button>
                </DumbUpload>
                <Divider type="vertical" />
                <VideoRecordButton onChange={onRecorded}><Icon type="video-camera" />{t('app.presentation.mediaInput.record')}</VideoRecordButton>
                <Divider type="vertical" />
                <AudioRecordButton onChange={onRecorded}><Icon type="audio" />{t('app.presentation.mediaInput.record')}</AudioRecordButton>
                {mediaType == MediaTypeEnum.AudioFile && onUploadPoster && (<>
                    <Divider type="vertical" />
                    <DumbUpload onChange={onUploadPoster} createThumb>
                        <Button><Icon type="picture" />{t('app.presentation.mediaInput.addPoster')}</Button>
                    </DumbUpload>
                </>)}
            </div>

            <Input value={link} onChange={onChangeLink}
                type="url"
                placeholder={t('form.activity.url.placeholder')} />
        </div>
    );
});
