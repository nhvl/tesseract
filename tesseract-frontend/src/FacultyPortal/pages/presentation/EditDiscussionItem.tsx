import React, { FC, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { DefaultId } from "../../../models/types";
import { ActItem } from "../../../models/ActDoc";
import { Discussion } from "../../../models/Discussion";

import { useStore } from "../../stores";

import { Select, Alert, } from "antd";

import { HtmlInput } from "../../../components/inputs/HtmlInput";

import { EditGeneralItem } from "./EditGeneralItem";
import { Input } from "../../../components/inputs/antd/Input";
import { PreviewMedia } from "./PreviewMedia";
import { DiscussionMediaInput } from "../discussion/DiscussionMediaInput";
import { DicussionActions } from "../discussion/DiscussionList";

export const EditDiscussionItem: FC<{ item: ActItem, dragHandleProps: any }> = observer(({ item, dragHandleProps }) => {
    const { t } = useTranslation();

    const {sEditActDoc, sDiscussion, sActivityDiscussionList} = useStore();
    const {availableDiscussions} = sEditActDoc;

    useEffect(() => {
        if (item.discussion == null || item.discussion.discussionId != item.discussionId) {
            const d = (item.discussionId ? sDiscussion.mId2Item.get(item.discussionId) : undefined);
            const d2 = (d != null ? d :
                new Discussion({
                    classId: sEditActDoc.activity ? sEditActDoc.activity.classId : undefined,
                    activityId: sEditActDoc.activityId,
                    startTime: Date.now(),
                }));
            item.set_discussion(d2);
            if (item.discussionId == null) item.set_discussionId(d2.discussionId);
        }
        // after item was changed, must be update originContentJS
        sEditActDoc.set_originContentJS();
    }, [item.discussionId, item.discussion]);

    return (
        <EditGeneralItem item={item} dragHandleProps={dragHandleProps}>
            {item.discussion && (
                <Alert
                    message={(
                        <DicussionActions item={item.discussion}
                            onPublish={sActivityDiscussionList.publish}
                            onHide   ={sActivityDiscussionList.hide}
                            onReopen ={sActivityDiscussionList.reopen}
                            onClose  ={sActivityDiscussionList.close}
                        />
                    )}
                    type="info" />
            )}
            {availableDiscussions.length > 0 && (
                <div className="mt-md mb-md">
                    <label className="text-black font-medium">{t('app.presentation.editdiscussionItem.existingTopic')}</label>
                    <div className="mt-xs">
                        <Select value={item.discussionId} onChange={item.set_discussionId}>
                            <Select.Option value={DefaultId}>New discussion</Select.Option>
                            {availableDiscussions.map(d => (
                                <Select.Option key={d.discussionId} value={d.discussionId}>{d.title}</Select.Option>
                            ))}
                        </Select>
                    </div>
                </div>
            )}
            {item.discussion && (<>
                <div className="mb-md">
                    <label className="text-black font-medium">{t('app.presentation.editdiscussionItem.newTopic')}</label>
                    <div className="mt-xs">
                        <Input value={item.discussion.title} onChange={item.discussion.set_title}
                            placeholder="Discussion topic" />
                    </div>
                </div>
                <div>
                    <label className="text-black font-medium">{t('app.presentation.editdiscussionItem.addMedia')}</label>
                    <div className="mt-xs">
                        <PreviewMedia
                            mediaType={item.discussion.mediaType}
                            link     ={item.discussion.link     }
                            mediaFile={item.discussion.mediaFile}
                            onUpload ={item.discussion.set_mediaFile}
                        />
                        <DiscussionMediaInput item={item.discussion} />
                    </div>
                </div>
                <HtmlInput value={item.discussion.content} onChange={item.discussion.set_content}
                    placeholder={t("form.activity.description.placeholder")}
                />
            </>)}
        </EditGeneralItem>
    );
});





