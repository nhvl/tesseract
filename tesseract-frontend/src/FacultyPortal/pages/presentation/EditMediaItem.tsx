import React, { FC, useState, useCallback } from "react";
import { runInAction } from "mobx";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { ActItem } from "../../../models/ActDoc";


import { HtmlInput } from "../../../components/inputs/HtmlInput";

import { EditGeneralItem } from "./EditGeneralItem";
import {PreviewMedia} from "./PreviewMedia";
import {MediaInput} from "./MediaInput";
import { getMediaInfo } from "../../../utils/getMediaInfo";

export const EditMediaItem: FC<{ item: ActItem, dragHandleProps: any }> = observer(({ item, dragHandleProps }) => {
    const { t } = useTranslation();
    const [, setEditting] = useState(false);
    const endEdit = useCallback(() => setEditting(false), [setEditting]);

    return (<EditGeneralItem item={item} dragHandleProps={dragHandleProps}>
        <PreviewMedia
            mediaType={item.mediaType}
            link     ={item.link     }
            mediaFile={item.mediaFile}
            image    ={item.image    }
            imageFile={item.imageFile}
            onUpload ={item.set_mediaFile}
        />

        <MediaInputWrap item={item} />

        <HtmlInput value={item.content} onChange={item.set_content}
            onBlur={endEdit} placeholder={t('form.activity.description.placeholder')} />
    </EditGeneralItem>
    );
});

const MediaInputWrap: FC<{ item: ActItem }> = observer(({ item }) => {
    const onClearMedia = useCallback(() => {
        item.set_link("");
        item.set_mediaFile(undefined);
        item.clearMediaType();
    }, [item]);

    const onChangeLink = useCallback(async (v: string) => {
        item.set_link(v);
        if (!v) {
            if (!item.mediaFile) item.clearMediaType();
        } else {
            item.set_mediaFile(undefined);
            await sleep(1000); // wait till user stop input
            if (v != item.link) return; // user update link, the v is outdated
            if (v == item.origLink) return;
            const {mediaType, contentType} = await getMediaInfo(v);
            if (v != item.link) return; // user update link, the v is outdated
            runInAction(() => {
                item.mediaType   = mediaType;
                item.contentType = contentType;
                item.origLink    = v;
            });
        }
    }, [item]);

    return (
        <MediaInput
            link={item.link}
            mediaFile={item.mediaFile}
            mediaType={item.mediaType}
            onChangeLink={onChangeLink}
            onUploadFile={item.set_mediaFile}
            onClearMedia={onClearMedia}
            onUploadPoster={item.set_imageFile}
            />
    );
});

function sleep(n:number) { return new Promise(r => setTimeout(r, n)) }
