import React, { FC } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Breadcrumb } from "antd";

import { ActivityBreadcrumb } from "../../components/ActivityBreadcrumb";
import { BasicLayout } from "../../layouts/BasicLayout";

import { EditActDoc } from "./EditActDoc";

export const EditActDocPage: FC = observer(() => {
    const { t } = useTranslation();
    const { sEditActDoc } = useStore();
    return (<BasicLayout>
        <ActivityBreadcrumb aClass={sEditActDoc.aClass} activity={sEditActDoc.activity}>
            <Breadcrumb.Item>{t("app.actDoc.editor.editor")}</Breadcrumb.Item>
        </ActivityBreadcrumb>

        <EditActDoc />
    </BasicLayout>);
});
