import React, { FC, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import Table, { ColumnProps } from 'antd/lib/table';

import { observer } from 'mobx-react';

import { StudentItem } from '../../stores/StudentListStore';
import { DateFromNowToolTip } from '../../../components/DateTime';
import { Student } from '../../../models/Student';
import { ClassLink } from '../classes/ClassLink';
import { Class } from '../../../models/Class';

import styles from './StudentListPage.module.less';
import { Icon, Tooltip } from 'antd';
import { Link } from '../../../components/router/Links';
import { useStore } from '../../stores';
import { DbIdentity } from '../../../models/types';

export const StudentTable: FC<{students:StudentItem[], defaultSort:String}> = observer(({students, defaultSort}) => {
    const {t} = useTranslation();
    const {routerStore} = useStore();
    const {classId} = routerStore.routerState.params;
    const columns = useMemo<ColumnProps<StudentItem>[]>(() => [
        // {title:"Profile", key:"avatar", dataIndex:"student", render: (student:Student) => (<StudentAvatar student={student} />)},
        {key:"actions", dataIndex:"student", render: ({studentId}:Student, si:StudentItem) => (<ActionCell studentId={studentId} classId={si.classId} />)},
        {title: t("app.students.list.firstName")        , key:"firstName"   , sorter:firstNameSorter   , dataIndex:"student", render:({firstName}:Student) => firstName},
        {title: t("app.students.list.lastName")         , key:"lastName"    , sorter:lastNameSorter    , dataIndex:"student", defaultSortOrder:defaultSort=="lastName" ? 'ascend' : undefined, render:({lastName}:Student) => lastName },
        {title: t("app.students.list.studentNumber")    , key:"studentNumber", sorter:studentNumberSorter    , dataIndex:"student", render:({studentNumber}:Student) => studentNumber },
        {title: t("app.students.list.class")            , key:"className", dataIndex:"class_", sorter:classNameSorter, defaultSortOrder: defaultSort=="className" ? 'ascend' : undefined, render: (class_:Class) => (
            !class_ ? "" : (<>
                <span className={styles.paddingRight}>{class_.className}</span>
                <ClassLink routeName="classDetail" classId={class_.classId}><Icon type="link" /></ClassLink>
            </>)) },
        {title: t("app.students.list.period")           , key:"period", dataIndex:"class_", sorter:periodSorter, render: (class_:Class) => (!class_ ? "" : class_.period) },
        {title: t("app.students.list.lastAccessedCourse"), key:"lastAccessed", sorter:lastAccessedSorter, dataIndex:"student", render:(student:Student, {classId}:StudentItem) => {
            const cs = student.mClassStudent.get(classId);
            return cs ? (<DateFromNowToolTip value={cs.lastAccessed!} />) : ""
        } },
        {key: "edit"  , render:(_, item) => (item.student ? (<Link routeName="editStudent" params={({classId : String(item.classId), studentId: String(item.student.studentId)})}><Tooltip title={t('app.students.list.edit')}><Icon type="edit" className="icon-sm" /></Tooltip></Link>) : null) }
    ], []);

    return (
        <div className="responsiveTable">
            <Table columns={columns} dataSource={students} pagination={false} rowKey={rowKey} />
        </div>
    );
});

function stringSorter(selector:(s:StudentItem) => string) { return (a:StudentItem, b:StudentItem) => selector(a).localeCompare(selector(b)) }
function numberSorter(selector:(s:StudentItem) => number) { return (a:StudentItem, b:StudentItem) => (selector(a) - selector(b)) }
const firstNameSorter = stringSorter(s => s.student.firstName.trim());
const lastNameSorter = stringSorter(s => s.student.lastName.trim());
const studentNumberSorter = stringSorter(s => s.student.studentNumber);
const lastAccessedSorter = numberSorter(s => {
    const cs = s.class_ ? s.student.mClassStudent.get(s.class_.classId) : undefined;
    return cs ? (cs.lastAccessed || 0) : 0
});
const periodSorter = stringSorter(s => s.class_ ? s.class_.period : "");
const classNameSorter = stringSorter(s => s.class_ ? s.class_.className : "");

function rowKey(record:StudentItem) { return `${record.classId}.${record.student.studentId}` }


export const ActionCell: FC<{studentId:DbIdentity, classId:DbIdentity}> = observer(({classId, studentId}) => {
    const {t} = useTranslation();

    return (classId < 0
        ? (<Link routeName="studentDetail" params={{studentId:String(studentId)}}>{t("app.students.list.detail")}</Link>)
        : (<Link routeName="studentDetailInClass" params={{studentId:String(studentId), classId:String(classId)}}>{t("app.students.list.detail")}</Link>)
    );
});
