import React, { FC, useCallback, useMemo } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Modal, Form, Skeleton, Divider, Empty } from "antd";

import { FileList } from "../../../components/inputs/antd/Upload";
import { GradeInput } from "../../components/Grade/GradeInput";
import { Link } from "../../../components/router/Links";

import styles from "./ActivityScoreDialog.module.less";
import { ThreadView } from "../../components/thread/ThreadView";

const commentSkeleton = (<Skeleton avatar active paragraph={{ rows: 3 }} />);

export const ActivityScoreDialog: FC<{}> = observer(() => {
    const { t } = useTranslation();
    const store = useStore();
    const { sActivityModal, routerStore } = store;

    const onCancel = useCallback(() => {
        sActivityModal.close(false);
    }, []);

    const contentHtml = useMemo(() => ({ __html: !sActivityModal.submission ? "" : sActivityModal.submission.content }), [sActivityModal.submission]);
    const studentId = sActivityModal.actScore ? sActivityModal.actScore.studentId : undefined;
    const activityId = sActivityModal.actScore ? sActivityModal.actScore.activityId : undefined;
    const params = useMemo(() => ({
        studentId: studentId ? String(studentId) : "",
        activityId: activityId ? String(activityId) : "",
    }), [studentId, activityId]);
    const goToStudentSubmission = useCallback(() => {
        sActivityModal.close(false);
        // routerStore.goTo("studentSubmission", params);
    }, [routerStore]);

    if (!sActivityModal.actScore || !sActivityModal.activity) return null;

    return (
        <Modal visible={!!sActivityModal.actScore}
            title={sActivityModal.activity.title}
            footer={sActivityModal.activity.isGraded ? undefined : null}
            okText={t("form.save")}
            cancelText={t("form.cancel")}
            onOk={sActivityModal.save}
            onCancel={onCancel}
            confirmLoading={sActivityModal.isSaving}
        >
            <div className={styles.studentView}>
                <div className={styles.more}>
                    {sActivityModal.submission === undefined ? (<Skeleton />) : (
                    sActivityModal.submission === null ? (<Empty description={t("app.students.detail.noSubmission")}/>) : (<>
                        <div dangerouslySetInnerHTML={contentHtml}></div>
                        <FileList fileList={sActivityModal.attachments} />
                        <Link routeName="studentSubmission" params={params}
                            onClick={goToStudentSubmission}
                            className={styles.info}
                            ><span>Read the full Submission</span></Link>
                    </>))}
                </div>
                {sActivityModal.activity.isGraded && (
                    <div>
                        <div className={styles.gradeInput}>
                            <Form.Item label={"Grade"}>
                                <GradeInput value={sActivityModal.score} onChange={sActivityModal.set_score} />
                            </Form.Item>
                        </div>
                    </div>
                )}
            </div>
            <Divider />

            <ThreadView threadId={sActivityModal.actScore.threadId}
                commentSeen={sActivityModal.commentSeen} />
        </Modal>
    );
});
