import React, { FC, useEffect, useCallback, FocusEvent } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { BasicLayout } from "../../layouts/BasicLayout";
import { Student } from "../../../models/Student";
import { ActivityScore, ActivityScoreInfo, FiveC } from "../../../models/ActivityScore";

import { Modal, Divider, Skeleton, Card, Breadcrumb, Button, message, Typography, Row, Col, Pagination, Select, Icon, Input, Alert, Tooltip, } from "antd";
const {Title} = Typography;

import { GradeInput } from "../../components/Grade/GradeInput";
import { SkillLevelRadioOption } from "../../components/Grade/SkillLevelRadioOption";
import { SkillSelector } from "../../components/Grade/SkillSelector";

import { StudentActDocView } from "../../../components/StudentDocSubmission/StudentActDocView";
import { ThreadView } from "../../components/thread/ThreadView";

import styles from "./QuickGrader.module.less";
import { useStore } from '../../stores';
import { LongDateLabel } from "../../../components/DateTime";
import { useNumberTrans } from "../../../components/DateTime/Number";
import { LongScoreMedals } from "../../../components/ScoreMedals";
import { ClassLink } from '../classes/ClassLink';
import { Link } from "../../../components/router/Links";
import { ActivityType } from '../../../models/Activity';
import {DiscussionThread} from "../../components/DiscussionThread";

export const QuickGraderPage: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const { routerStore, sClassDetail, currentSchoolId, currentFacultyId, sStudentSubmission, sViewStudentActDoc } = useStore();
    const {studentId, activityId} = routerStore.routerState.params;
    const sId = !studentId ? NaN : Number(studentId);
    const aId = !activityId ? NaN : Number(activityId);
    useEffect(() => {
        if (Number.isNaN(sId) || sId < 1) Modal.error({title: t('app.students.submissionDetails.invalidUrl'), content: t('app.students.submissionDetails.invalidstudentID'), onOk() { routerStore.goTo("home") }})
        else if (Number.isNaN(aId) || aId < 1) Modal.error({title: t('app.students.submissionDetails.invalidUrl'), content: t('app.students.submissionDetails.invalidActivityID'), onOk() { routerStore.goTo("home") }})
        else {
            sStudentSubmission.init({studentId:sId, activityId:aId});
            sViewStudentActDoc.init(aId, sId).then((err) => {
                if (!err) {
                    if(sViewStudentActDoc.activity){
                        sClassDetail.initClass(currentSchoolId, currentFacultyId, sViewStudentActDoc.activity.classId);
                    }
                    return;
                }
                Modal.error({ content: err.message, onOk() { routerStore.goTo("home") } });
            });
        }
    }, [sId, aId]);

    return (
        <BasicLayout>
            <div className="breadcrumb">
            <Breadcrumb>
                {sClassDetail.aClass && (<Breadcrumb.Item>
                    <ClassLink routeName="classDetail" classId={sClassDetail.classId}>{sClassDetail.aClass.className}</ClassLink>
                </Breadcrumb.Item>
                )}
                {sClassDetail.aClass && (<Breadcrumb.Item>
                    <Link routeName="classGradebook" params={({classId : String(sClassDetail.classId)})}>{t('app.classes.menu.gradebook')}</Link>
                </Breadcrumb.Item>)}
                {sStudentSubmission.activity && (<Breadcrumb.Item>
                    {(sStudentSubmission.activity.type == ActivityType.Assessment
                        || sStudentSubmission.activity.type == ActivityType.Assignment)
                    && (<Link routeName="activitySettings" params={sStudentSubmission.activity.params}>{sStudentSubmission.activity.title}</Link>)
                    }
                    {(sStudentSubmission.activity.type == ActivityType.Discussion && sStudentSubmission.discussion) &&
                        (<Link routeName="discussion" params={({classId : String(sClassDetail.classId), discussionId:String(sStudentSubmission.discussion.discussionId)})}>{sStudentSubmission.activity.title}</Link>)
                    }
                </Breadcrumb.Item>)}
                <Breadcrumb.Item>{t('app.classes.gradebook.quickGrader')}</Breadcrumb.Item>
            </Breadcrumb>
            </div>
            <SubmissionInfo studentId={sId} activityId={aId} />
            <QuickGrader studentId={sId} activityId={aId} />
        </BasicLayout>
    );
});

const SubmissionInfo: FC<{studentId: number, activityId: number}> = observer(({studentId, activityId}) => {
    const {t} = useTranslation();
    const {percent} = useNumberTrans();
    const { routerStore, sClassDetail: sClass, sStudentSubmission } = useStore();
    const onChange = useCallback((value:number)=>{
        routerStore.goTo("quickGrader", {
                    studentId:String(value),
                    activityId: String(activityId)});
    },[studentId, activityId]);
    const isDisableBack = useCallback(()=>{
        return sClass.students.findIndex(s=>s.studentId==studentId) == 0;
    },[studentId, activityId]);
    const isDisableNext = useCallback(()=>{
        return sClass.students.findIndex(s=>s.studentId==studentId) == sClass.students.length - 1;
    },[studentId, activityId]);
    const onPrev = useCallback(()=>{
        const sIndex = sClass.students.findIndex(s=>s.studentId==studentId) - 1;
        if(sIndex < 0) return;
        routerStore.goTo("quickGrader", {
                    studentId:String(sClass.students[sIndex].studentId),
                    activityId: String(activityId)});
    },[studentId, activityId]);
    const onNext = useCallback(()=>{
        const sIndex = sClass.students.findIndex(s=>s.studentId==studentId) + 1;
        if(sIndex >= sClass.students.length) return;
        routerStore.goTo("quickGrader", {
                    studentId:String(sClass.students[sIndex].studentId),
                    activityId: String(activityId)});
    },[studentId, activityId]);
    const onLast = useCallback(()=>{
        routerStore.goTo("quickGrader", {
                    studentId:String(sClass.students[sClass.students.length - 1].studentId),
                    activityId: String(activityId)});
    },[studentId, activityId]);
    const onFirst = useCallback(()=>{
        routerStore.goTo("quickGrader", {
                    studentId:String(sClass.students[0].studentId),
                    activityId: String(activityId)});
    },[studentId, activityId]);
    const averageScore = useCallback(() => {
        const scoreList = sClass.actScores.filter(s => s.activityId == activityId && s.score);
        let sum = scoreList.reduce(function(prev, cur) { return prev + (Math.min(cur.score || 0, sStudentSubmission.activity!.maxScore))}, 0);
        return sum / scoreList.length;
    },[activityId]);
    return (<>
        <div className="flex items-start justify-between">
            <div className="header">
                <Title level={3}>{sStudentSubmission.activity && sStudentSubmission.activity.title}</Title>
            </div>
            <div>
                <div className="flex items-center">
                    <a onClick={onFirst} className={isDisableBack() ? "isNextPrev isDisabled" : "isNextPrev"}><Icon type="double-left" /><span className="ml-xs">First</span></a>
                    <a onClick={onPrev} className={isDisableBack() ? "isNextPrev isDisabled" : "isNextPrev"}><Icon type="left" /></a>
                    <div className={styles.studentName}>
                        <Select showSearch
                            optionFilterProp="children"
                            value = {studentId}
                            onChange={onChange}
                            >
                            {sClass.students.map(s=>(
                                    <Select.Option key={s.studentId} value={s.studentId}>{s.fullName}</Select.Option>
                                ))}
                        </Select>
                    </div>
                    <a onClick={onNext} className={isDisableNext() ? "isNextPrev isDisabled" : "isNextPrev"}><Icon type="right" /></a>
                    <a onClick={onLast} className={isDisableNext() ? "isNextPrev isDisabled" : "isNextPrev"}><span className="mr-xs">Last</span><Icon type="double-right" /></a>
                </div>
                <div className="text-center text-black font-size-md">
                    {`${sClass.students.findIndex(s=>s.studentId==studentId)+1}/${sClass.students.length}`}
                </div>
            </div>
        </div>
        <div className="font-size-md mb-sm">
            <label className="text-black mr-xs">{t('app.activities.due')}</label>
            {(sStudentSubmission.activity && sStudentSubmission.activity.dateDue)
            ? (<LongDateLabel value={sStudentSubmission.activity.dateDue!} />)
            : (<span className="text-grey font-medium">{t('app.activities.noDueDate')}</span>)}
            <Divider type="vertical" />
            <label className="text-black mr-xs">{t('app.activities.score.Graded')}</label>
            <span className="text-grey font-medium">{`${sClass.actScores.filter(s => s.activityId == activityId && s.score).length}/${sClass.students.length}`}</span>
            <Divider type="vertical" />
            <label className="text-black mr-xs">{t('app.activities.score.Average')}</label>
            <span className="text-grey font-medium">
                {sStudentSubmission.activity && sStudentSubmission.activity.maxScore != 0 && (<>
                    {`${Math.round(averageScore())}/${sStudentSubmission.activity.maxScore} (${percent(averageScore()/sStudentSubmission.activity.maxScore, 2)})`}
                </>)}
            </span>
        </div>
    </>);
});

const QuickGrader: FC<{studentId: number, activityId: number}> = observer(({studentId, activityId}) => {
    const {t} = useTranslation();
    const { routerStore, sStudentSubmission, sViewStudentActDoc, sClassDetail: sClass, currentFacultyId, currentSchoolId, scoreBadge } = useStore();
    useEffect(() => {
        var el: HTMLInputElement|null = document.getElementById(studentId + "-" + activityId) as HTMLInputElement;
        setTimeout(() => {
            if(el){
                el.select();
            }
        });
    }, [studentId, activityId]);

    const onSave = useCallback(async () => {
        if (sStudentSubmission.actScore && sStudentSubmission.isChangeScore){
            const err = await sStudentSubmission.save();
            if (err) {
                Modal.error({ title: t("app.activities.actDoc.save.failed"), content: err.message });
            } else {
                message.success(t("app.actDoc.editor.editor.saveSuccess"));
                sStudentSubmission.currentScore = sStudentSubmission.actScore.clone();
                sStudentSubmission.set_selectedSkill(undefined);
                const [sErr, s] = await ActivityScore.fetchActivityScoresOfClassAsFaculty(currentSchoolId, currentFacultyId, sClass.classId);
                if(!sErr){
                    sClass.actScores = s;
                }
            }
        }
    }, [studentId, activityId]);
    const onKeydown = useCallback((event) => {
        if(event.keyCode == 13){
            onSave().then(()=>{
                let sIndex = sClass.students.findIndex(s=>s.studentId==studentId) + 1;
                if(sIndex >= sClass.students.length) sIndex = 0;
                routerStore.goTo("quickGrader", {
                    studentId:String(sClass.students[sIndex].studentId),
                    activityId: String(activityId)});
            });
        }
    },[studentId, activityId]);

    const handleCancel = useCallback(async ()=>{
        sStudentSubmission.set_selectedSkill(undefined);
    },[studentId, activityId]);

    const toggleIsExclude = useCallback(async () => {
        if (sStudentSubmission.actScore == null) return;
        sStudentSubmission.actScore.set_isExclude(!sStudentSubmission.actScore.isExclude);
        onSave();
    }, [sStudentSubmission, onSave]);

    if (!sStudentSubmission.actScore || !sStudentSubmission.activity) return (<Skeleton />);
    return (<Card>
        <div className={`${styles.openGrader} flex`}>
            <div className={styles.submitLeft}>
                {(sStudentSubmission.activity
                && sStudentSubmission.submission
                && sStudentSubmission.actScore
                && sStudentSubmission.actScore.isSubmitted) && (<div>
                    <Alert
                        message={(<>
                            <span className="font-medium text-black mb-xs">{(sStudentSubmission.isReSubmitted) ? t('app.activities.student.re-submit') : t('app.activities.student.submit')} <LongDateLabel value={sStudentSubmission.submission.dateUpdated} /></span>
                            <time dateTime={new Date(sStudentSubmission.submission.dateUpdated).toJSON()} hidden={sStudentSubmission.activity.dateDue == null}
                                title={new Date(sStudentSubmission.submission.dateUpdated).toLocaleString()}>
                                {
                                    (sStudentSubmission.activity.dateDue == null || sStudentSubmission.submission.dateUpdated <= sStudentSubmission.activity.dateDue)
                                    ? (<span> - <span className="font-medium text-success">{t('app.activities.score.onTime')}</span></span>)
                                    : (<span> - <span className="font-medium text-danger">{t('app.activities.score.Late')}</span></span>)
                                }
                            </time>
                        </>)}
                        type="info"
                        showIcon
                        closable
                        className="mb-lg"
                    />
                </div>)}
                {sViewStudentActDoc.doc && sStudentSubmission.actScore.isSubmitted
                    ? (<StudentActDocView doc={sViewStudentActDoc.doc} />)
                    : (sStudentSubmission.activity.type == ActivityType.Discussion && sStudentSubmission.discussion)
                    ? (<DiscussionThread classId={sStudentSubmission.activity.classId} discussionId={sStudentSubmission.discussion.discussionId} readonly={true} />)
                    :<Alert
                        message={t('app.students.submissionDetail.noWorkSubmission')}
                        type="warning"
                        showIcon
                        closable
                        className="mb-lg"
                    />
                }
            </div>
            <div className={styles.gradeRight}>
                {sStudentSubmission.activity.isGraded && (<>
                    <label className="block text-black font-medium mb-xs">{t("app.students.detail.grade")}:</label>
                    <div className="flex items-center mb-sm">
                        <div className="mr-md">
                            <GradeInput autoFocus onFocus={whenFocusSelectText}
                                id={studentId + "-" + activityId}
                                value={sStudentSubmission.actScore.score}
                                onChange={sStudentSubmission.actScore.set_score}
                                onKeyUp={onKeydown} onBlur={onSave}
                                max={sStudentSubmission.activity.maxScore*2}
                                />
                            <span className="ml-xs mr-xs">/</span>{sStudentSubmission.activity.maxScore}
                        </div>
                        <div className={styles.skillAssessment}>
                            { !sStudentSubmission.isSaving && sStudentSubmission.actScore.emptyBadges.length != 0 &&
                                (<SkillSelector options={sStudentSubmission.actScore.emptyBadges} selected={sStudentSubmission.selectedSkill} onSelect={ sStudentSubmission.set_selectedSkill }/>)}
                        </div>
                    </div>
                    <Tooltip title={t("app.activities.score.isExclude")}>
                        <Icon type={sStudentSubmission.actScore.isExclude ? "eye-invisible" : "eye"} onClick={toggleIsExclude}  />
                    </Tooltip>
                    <div>
                        <LongScoreMedals score={sStudentSubmission.actScore} scoreBadge={scoreBadge} scoreChangeHandler={sStudentSubmission.set_selectedSkill}/>

                        {sStudentSubmission.selectedSkill && (<div>
                            <SkillLevelRadioOption record={sStudentSubmission.actScore} cType={sStudentSubmission.selectedSkill} onSave={onSave} onCancel={handleCancel} />
                        </div>)}
                    </div>
                </>)}
                <Divider />
                <ThreadView threadId={sStudentSubmission.actScore.threadId}
                    commentSeen={sStudentSubmission.commentSeen}
                    />
            </div>
        </div>
    </Card>);
});

function whenFocusSelectText(event:FocusEvent<HTMLInputElement>) {
    event.target.select()
}
