import React, { FC, useEffect, useMemo } from 'react';
import { observer } from 'mobx-react';
import { useTranslation, Trans } from 'react-i18next';

import { DbIdentity, DefaultId } from '../../../models/types';
import { Student } from '../../../models/Student';
import { Class } from '../../../models/Class';

import { StudentItem } from '../../stores/StudentListStore';
import { useStore } from '../../stores';

import { Card, Table, Avatar, Icon, Typography, Select, Button, Divider, } from 'antd';
import { ColumnProps } from 'antd/lib/table';
const { Title } = Typography;

import { BasicLayout } from '../../layouts/BasicLayout';
import { DateFromNowToolTip } from '../../../components/DateTime';
import { Link } from '../../../components/router/Links';
import { ClassLink } from '../classes/ClassLink';

import styles from './StudentListPage.module.less';
import { Modal } from '../../../components/Modal/ModalAntd';
import { StudentTable } from './StudentTable';

export const StudentListPage: FC<{}> = observer(({}) => {
    const store = useStore();
    const {sStudentList} = store;
    const {t} = useTranslation();
    useEffect(() => {
        sStudentList.fetchData().then(err => {
            if (err) {
                Modal.error({content: err.message});
                return;
            }
        });
    }, []);
    return (<BasicLayout>
        <div className="header">
            <Title level={3}>{t("app.students.list.header")}</Title>
        </div>
        <div className={styles.result}>
            <div className={styles.filter}>
                <label className="text-black font-medium mr-xs">{t("app.students.list.selectLabel")}:</label>
                <Select value={sStudentList.classId} onChange={sStudentList.set_classId}
                    size="large" className={styles.classFilter}>
                    <Select.Option key={DefaultId} value={DefaultId}>{t("app.students.list.selectClass")}</Select.Option>
                    {store.currentGradingTermClasses.map(c => (
                        <Select.Option key={c.classId} value={c.classId}>{c.period}. {c.className}</Select.Option>
                    ))}
                </Select>
            </div>
            <div className="font-size-md text-black">
                <Trans i18nKey="app.students.list.countStudent" count={sStudentList.nStudent}>
                    <span className="heading-2">{({nStudent:sStudentList.nStudent})}</span>
                </Trans>
            </div>
        </div>
        <Card>
            <StudentTable students={sStudentList.displayStudents} defaultSort="className" />
        </Card>
    </BasicLayout>);
});

export const StudentAvatar: FC<{student:Student}> = observer(({student}) => {
    const {t} = useTranslation();
    // const store = useStore();
    // const n = useComputed(() => student.calculateMissingAssigment(store.tActivity).length, []);
    // return (
    //     <Badge count={n}>
    //         <Avatar src={student.avatar} alt={`Avatar of ${student.firstName} ${student.lastName}`} shape="square" />
    //     </Badge>
    // );
    return (student.avatar
        ? (<Avatar src={student.avatar} alt={t('app.students.list.avatar', { firstName: student.firstName, lastName:student.lastName })} />)
        : (<Avatar icon="user" />)
    );
});

