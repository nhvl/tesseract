import React, { FC, useEffect, useMemo, ReactNode, ReactElement, useCallback } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { DbIdentity } from '../../../models/types';
import { Class } from '../../../models/Class';
import { Activity } from '../../../models/Activity';
import { Student } from '../../../models/Student';

import { useStore } from '../../stores';

import { Card, Table, Icon, Divider, Badge, Typography, Skeleton } from 'antd';
import { ColumnProps } from 'antd/lib/table';
const { Title } = Typography;

import { BasicLayout } from '../../layouts/BasicLayout';
import { DateLabel, DateFromNowToolTip, } from '../../../components/DateTime';
import { ClassLink } from "../classes/ClassLink";
import {UserAvatar} from "../../components/UserAvatar";

import {useNumberTrans} from "../../../components/DateTime/Number";
import styles from './StudentDetailPage.module.less';
import { Link } from '../../../components/router/Links';
import { Modal } from '../../../components/Modal/ModalAntd';

export const StudentDetailPage: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const store = useStore();
    const {sStudentDetail, routerStore} = store;
    const {studentId, classId} = routerStore.routerState.params;

    useEffect(() => {
        const sId: DbIdentity = !studentId ? NaN : Number(studentId);
        const cId: DbIdentity = !classId   ? NaN : Number(classId);
        if (Number.isNaN(sId) || sId < 1) Modal.error({content: t('app.students.detail.invalidStudentId'), onOk() { routerStore.goTo("students"); }});
        else sStudentDetail.init(sId, cId).then(err => {
            if (!err) return;
            Modal.error({content: err.message, onOk() { routerStore.goTo("students"); }});
        });
    }, [studentId, classId]);

    const {student, missedActivities} = sStudentDetail;
    const studentClasses = student == null ? [] : student.classes;

    const classes = useMemo(() => (sStudentDetail.classId < 1 ? studentClasses : [sStudentDetail.classId])
        .map(classId => store.mClass.get(classId)!).filter(Boolean), [sStudentDetail.classId, studentClasses]);

    if (student == null) return (<Skeleton />);

    return (<BasicLayout>
        <div className="header">
            <Title level={3}>{student.fullName}</Title>
        </div>
        <div className="paddingBottom-lg">
            <Card title={"Profile"}>
                <div className={styles.profile}>
                    <div className="flex mr-lg">
                        <UserAvatar user={student} className={styles.avatar} size={96} />
                        <div className="flex flex-col justify-center">
                            <div className="text-grey font-medium mb-xs">{student.fullName}</div>
                            <div className="font-bold text-danger">
                                <span className={styles.labelError}>{t("app.students.detail.missingAssignment", {count:missedActivities})}</span>
                                <Badge count={missedActivities}><Icon type="warning" className="icon-md" /></Badge>
                            </div>
                        </div>
                    </div>
                    <div className={styles.info}>
                        <div className="text-grey font-medium mb-xs">{t("app.students.detail.studentNumber")}</div>
                        <div className="font-bold">{student.studentNumber}</div>
                    </div>
                    {student.dob != null && (<div className={styles.info}>
                        <div className="text-grey font-medium mb-xs">{t("app.students.detail.dob")}</div>
                        <div className="font-bold"><DateLabel value={student.dob}/></div>
                    </div>)}
                    {student.lastActive != null && (<div className={styles.info}>
                        <div className="text-grey font-medium mb-xs">{t("app.students.detail.lastLoggedIn")}</div>
                        <div className="font-bold">{<DateFromNowToolTip value={student.lastActive} />}</div>
                    </div>)}
                </div>
            </Card>
        </div>
        <Card title={t("app.students.detail.activities")}>
            {classes.map(c => (
                <ClassDetail key={c.classId} student={student} aClass={c} />
            ))}
        </Card>
    </BasicLayout>);
});

const ClassDetail: FC<{student:Student, aClass: Class}> = observer(({student, aClass}) => {
    const {t} = useTranslation();
    const {percent} = useNumberTrans();
    const store = useStore();

    const dateRender = useCallback((d:number) => ((d == null || d <= 0) ? t("app.students.detail.DateNA") : (<DateLabel value={d} />)), [t]);

    const columns = useMemo<Array<ColumnProps<Activity>>>(() => [
        { dataIndex: 'title', key: 'title', className: "whitespace-normal min-w", },
        { dataIndex: 'dateAssigned', key: 'dateAssigned', title: t("app.students.detail.activityAssigned"), render: dateRender},
        { dataIndex: 'dateDue', key: 'dateDue', title: t("app.students.detail.activtyDue"), render: dateRender},
        { dataIndex: 'activityId', key: 'submission', title: t("app.students.detail.submission"), render: (activityId:DbIdentity) => (<SubmissionCell activityId={activityId} />) },
        { dataIndex: 'activityId', key: 'grade', title: t("app.students.detail.grade"), render: (activityId:DbIdentity, activity:Activity) => (<GradeCell activity={activity} />) },
    ], [t]);

    const activities = store.sActivity.mClassId2Items.get(aClass.classId);
    const cs = student.mClassStudent.get(aClass.classId);

    return (
        <div className={styles.classActivity}>
            <div className="paddingBottom-xs">
                <span className={`${styles.widthLabel} text-grey font-medium`}>{t("app.students.detail.class")}</span>
                <span className={styles.name}>{aClass.className}</span>
                <ClassLink routeName="classDetail" classId={aClass.classId}><Icon type="link" /></ClassLink>
            </div>

            <div className="paddingBottom-sm">
                <span className={`${styles.widthLabel} text-grey font-medium`}>{t("app.students.detail.classGrade")}</span>
                <span className={styles.name}>{cs && cs.cumulativeGrade != null && percent(cs.cumulativeGrade, 2)}</span>
            </div>

            <div className="responsiveTable">
                <Table columns={columns} dataSource={activities} pagination={false} rowKey={rowKey} />
            </div>

            <div className="m-8" />
        </div>
    );
});

const SubmissionCell: FC<{activityId: DbIdentity}> = observer(({activityId}) => {
    const {t} = useTranslation();
    const store = useStore();

    const {sStudentDetail} = store;
    const {student} = sStudentDetail;
    const s = student ? student.mActivitiScore.get(activityId) : undefined;
    const activity = store.sActivity.mId2Item.get(activityId);

    if (s == null || s.savedDate == null || !s.isSubmitted)
        return t("app.students.detail.submissionNA") as ReactNode as ReactElement;

    return (<div>
        <span><DateLabel value={s.savedDate} /> </span>
        {(s.savedDate == null || activity == null || activity.dateDue == null) ? null : (
            s.savedDate < activity.dateDue
            ? (<span className="text-success paddingLeft-xs">{t('app.students.detail.onTime')}</span>)
            : (<span className="text-danger paddingLeft-xs">{t('app.students.detail.late')}</span>)
        )}
    </div>);
});

const GradeCell: FC<{activity:Activity}> = observer(({activity}) => {
    const {t} = useTranslation();
    const {sStudentDetail} = useStore();
    const {student} = sStudentDetail;
    const openCommentDialog= useCallback(() => {
        sStudentDetail.openCommentDialog(activity)
    }, [sStudentDetail, activity]);
    const s = student ? student.mActivitiScore.get(activity.activityId) : undefined;

    const params = useMemo(() => ({
        studentId: s == null ? "" : String(s.studentId),
        activityId: String(activity.activityId),
    }), [s, activity]);

    const isGrade = activity.isGraded ? 1 : 0;
    const submitted = (s != null && s.savedDate != null && s.isSubmitted) ? 1 : 0;
    const scored = (s != null && s.score != null && 0 < s.score) ? 1 : 0;
    const spec = specs.find(([sIsGrade, sSubmitted, sScored]) => isGrade == sIsGrade && submitted == sSubmitted && scored == sScored);
    const renderCase = spec == null ? null : spec[3];
    const c = (
        renderCase == "Submitted" ? (<Link routeName="studentSubmission" params={params}>{t('app.students.detail.submitted')}</Link>) : (
        renderCase == "Score" ? (
            <Link routeName="studentSubmission" params={params} className={styles.grade}>
                <span className="text-black font-medium mr-xs">{s && s.score}/{activity.maxScore}</span>
                <i className="fas fa-pen"></i>
            </Link>
        ) : (
        null)));

    return (
        <>
            <a onClick={openCommentDialog}><Icon type="message" className="icon-sm" /></a>
            {c != null && (<>
                <Divider type="vertical" />
                {c}
            </>)}
        </>
    );
});

const specs:[number, number, number, string][] = [
  // isGrade  submitted scored    renderCase
    [0,       0,        0,        ""         ],
    [0,       0,        1,        ""         ],
    [0,       1,        0,        "Submitted"],
    [0,       1,        1,        "Submitted"],
    [1,       0,        0,        ""         ],
    [1,       0,        1,        "Score"    ],
    [1,       1,        0,        "Submitted"],
    [1,       1,        1,        "Score"    ],
];

function rowKey(record:Activity) { return String(record.activityId) }
