import React, { FC, useCallback, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { useStore } from "../../stores";

import { Divider, Skeleton, Card, Breadcrumb, Button, message, Typography, } from "antd";
const {Title} = Typography;

import { BasicLayout } from "../../layouts/BasicLayout";

import { GradeInput } from "../../components/Grade/GradeInput";
import {StudentBreadcrumb} from "../../components/StudentBreadcrumb";

import { StudentActDocView } from "../../../components/StudentDocSubmission/StudentActDocView";
import { ThreadView } from "../../components/thread/ThreadView";
import { Modal } from "../../../components/Modal/ModalAntd";


export const StudentSubmissionPage: FC<{}> = observer(({}) => {
    return (
        <BasicLayout>
            <StudentSubmission />
        </BasicLayout>
    );
});

const StudentSubmission: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const { routerStore, sStudentSubmission, sViewStudentActDoc } = useStore();

    const {studentId, activityId} = routerStore.routerState.params;
    const sId = !studentId ? NaN : Number(studentId);
    const aId = !activityId ? NaN : Number(activityId);
    useEffect(() => {
        if (Number.isNaN(sId) || sId < 1) Modal.error({title: t('app.students.submissionDetails.invalidUrl'), content: t('app.students.submissionDetails.invalidstudentID'), onOk() { routerStore.goTo("home") }})
        else if (Number.isNaN(aId) || aId < 1) Modal.error({title: t('app.students.submissionDetails.invalidUrl'), content: t('app.students.submissionDetails.invalidActivityID'), onOk() { routerStore.goTo("home") }})
        else {
            sStudentSubmission.init({studentId:sId, activityId:aId});
            sViewStudentActDoc.init(aId, sId).then((err) => {
                if (!err) return;
                Modal.error({ content: err.message, onOk() { routerStore.goTo("home") } });
            });
            window.scrollTo(0, 0);
        }
    }, [sId, aId]);

    const goBack = useCallback(() => {
        if (history.length > 1) history.back();
        else
            if (sStudentSubmission.activity != null)
                routerStore.goTo("studentDetailInClass", {classId:String(sStudentSubmission.activity.classId), studentId:String(sStudentSubmission.studentId)})
            else
                routerStore.goTo("studentDetail", {studentId:String(sStudentSubmission.studentId)})
    }, []);

    const onSave = useCallback(async () => {
        const err = await sStudentSubmission.save();
        if (err) {
            Modal.error({ title: t("app.activities.actDoc.save.failed"), content: err.message });
        } else {
            message.success(t("app.actDoc.editor.editor.saveSuccess"));
        }
    }, []);

    if (!sStudentSubmission.actScore || !sStudentSubmission.activity) return (<Skeleton />);

    return (<>
        <StudentBreadcrumb classId={sStudentSubmission.activity.classId} studentId={sStudentSubmission.studentId}
            studentName={sStudentSubmission.student ? `${sStudentSubmission.student.fullName}` : ""}>
            <Breadcrumb.Item>
                {sStudentSubmission.activity.title}
            </Breadcrumb.Item>
        </StudentBreadcrumb>
        <div className="flex items-start justify-between">
            <div className="header">
                <Title level={3}>{t('app.students.detail.submissionDetails')}</Title>
            </div>
            {sStudentSubmission.activity.isGraded && (<div className="flex items-center">
                <label className="font-size-md font-medium mr-xs">{t("app.students.detail.grade")}: </label>
                <div className="flex items-end">
                    <GradeInput value={sStudentSubmission.actScore.score} onChange={sStudentSubmission.actScore.set_score} />
                    <Button onClick={onSave} loading={sStudentSubmission.isSaving} type="primary" className="ml-md">{t("form.save")}</Button>
                </div>
            </div>)}
        </div>
        <Card>
            <div>
                {sViewStudentActDoc.doc
                    ? (<StudentActDocView doc={sViewStudentActDoc.doc} />)
                    : t("app.students.submissionDetail.noSubmission")}
            </div>
            <div className="mt-lg mb-lg">
                <Divider />
                <Button onClick={goBack} loading={sStudentSubmission.isSaving} className="mr-sm">{t("form.back")}</Button>
            </div>
            <Divider />

            <ThreadView threadId={sStudentSubmission.actScore.threadId}
                commentSeen={sStudentSubmission.commentSeen}
                />
        </Card>
    </>);
});
