import React, { FC,  useEffect, useCallback, FormEvent } from "react";
import { observer } from "mobx-react";

import { DefaultId } from "../../../models/types";

import { useStore } from "../../stores";

import { Card, Skeleton, Button, Form, Select, Breadcrumb, Row, Col, Divider, } from "antd";

import { Input } from "../../../components/inputs/antd/Input";

import { Link } from "../../../components/router/Links";
import { InputNumber } from "../../../components/inputs/antd/InputNumber";
import { useTranslation } from "react-i18next";
import { Modal } from "../../../components/Modal/ModalAntd";
import { ClassLink } from "../classes/ClassLink";
import { showError } from "../../../services/api/ShowError";

export const EditStudent: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const {sEditStudent, routerStore} = useStore();
    const {studentId , classId} = routerStore.routerState.params;

    let sId = !studentId ? DefaultId : Number(studentId);
    if (Number.isNaN(sId)) sId = DefaultId;

    let cId = !classId ? DefaultId : Number(classId);
    if (Number.isNaN(cId)) cId = DefaultId;
    
    useEffect(() => {
        sEditStudent.init({studentId: sId, classId: cId}).then(err => {
            if (err) showError(err, t);
        })
    }, [sId, cId]);
    
    const {student, loading, aClass} = sEditStudent;

    const onSubmit = useCallback(async (event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (sEditStudent.student == null) return;
        const err = await sEditStudent.save();
        if (err) showError(err, t);
        else 
        {
            if (cId > 0)
                routerStore.goTo("classStudents", {classId: String(cId)});
            else
                routerStore.goTo("students");
        }
    }, []);

    if (student == null) return (<Card><Skeleton /></Card>);

    return (<>
        <div className="breadcrumb">
            <Breadcrumb>
                {aClass && (<Breadcrumb.Item>
                    <ClassLink routeName="classDetail" classId={aClass.classId}>{aClass.className}</ClassLink>
                </Breadcrumb.Item>)}
                <Breadcrumb.Item>
                    { aClass ?
                        <Link routeName="classStudents" params={({classId: String(aClass.classId)})}>{t('menu.students')}</Link>
                        : <Link routeName="students">{t('menu.students')}</Link>
                    }
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    {sEditStudent.isCreateNew ? t('app.students.edit.createTitle') : t('app.students.edit.editTitle')}
                </Breadcrumb.Item>
            </Breadcrumb>
        </div>
        <Card>
            <Form onSubmit={onSubmit}>
                <fieldset disabled={loading}>
                    <Row gutter={16}>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.studentNumber')}>
                                <Input value={student.studentNumber} onChange={student.set_studentNumber}
                                    required
                                    placeholder={t('app.students.edit.studentNumber')} />
                            </Form.Item>
                        </Col>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.grade')}>
                                <InputNumber value={student.grade} onChange={student.set_grade}
                                    placeholder={t('app.students.edit.grade')}
                                    min={0} max={12} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.firstName')}>
                                <Input value={student.firstName} onChange={student.set_firstName}
                                    required
                                    placeholder={t('app.students.edit.firstName')} />
                            </Form.Item>
                        </Col>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.lastName')}>
                                <Input value={student.lastName} onChange={student.set_lastName}
                                    required
                                    placeholder={t('app.students.edit.lastName')} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} xxl={16}>
                            <Form.Item label={t('app.students.edit.email')}>
                                <Input value={student.email} onChange={student.set_email}
                                    type="email" required
                                    placeholder={t('app.students.edit.email')} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.phoneNumber')}>
                                <Input value={student.phoneNumber} onChange={student.set_phoneNumber}
                                    type="tel"
                                    placeholder={t('app.students.edit.phoneNumber')}/>
                            </Form.Item>
                        </Col>
                        <Col xs={24} md={12} xxl={8}>
                            <Form.Item label={t('app.students.edit.externalID')}>
                                <Input value={student.externalId} onChange={student.set_externalId}
                                    placeholder={t('app.students.edit.externalID')} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Divider />
                    <Button className="mr-sm">
                        {aClass ?
                            <Link routeName="classStudents" params={{classId: String(aClass.classId)}}>{t('form.cancel')}</Link>
                            : <Link routeName="students">{t('form.cancel')}</Link>
                        }
                    </Button>
                    <Button type="primary" htmlType="submit" loading={loading}>{t('form.save')}</Button>
                </fieldset>
            </Form>
        </Card>
    </>);
});
