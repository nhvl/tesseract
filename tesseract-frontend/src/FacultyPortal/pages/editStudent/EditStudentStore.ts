import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../../models/types";
import { Student } from "../../../models/Student";
import { Class } from "../../../models/Class";

import { Store } from "../../stores/Store";
import { IErrorData } from "../../../services/api/AppError";

import { UploadFile } from "antd/lib/upload/interface";
import { uploadFile } from "../../../services/api/fetch";

export class EditStudentStore {
    constructor(private store:Store) {
    }

    @observable     studentId : DbIdentity = DefaultId;
    @observable     facultyId : DbIdentity = DefaultId;
    @observable.ref student  ?: Student;
    @observable     schoolId  : DbIdentity = DefaultId;
    @observable     classId   : DbIdentity = DefaultId;
    @observable.ref aClass   ?: Class = undefined;

    @action async init({studentId, classId}:{studentId: DbIdentity, classId: DbIdentity}) {
        this.studentId = studentId;
        this.classId = classId;
        this.loading = false;

        if (this.schoolId < 1) {
            this.schoolId = this.store.currentSchoolId;
        }

        if(this.facultyId < 1){
            this.facultyId = this.store.currentFacultyId;
        }

        const [cErr, _class] = await Class.fetchClassAsFaculty({schoolId:this.store.currentSchoolId, facultyId:this.store.currentFacultyId, classId});
        if (cErr) return cErr;
        runInAction(() => {
            this.aClass = _class;
            this.store.set_currentGradingTerm(this.aClass.gradingTerm, true);
        });

        if (this.studentId < 1) {
            this.student = new Student();
        } else {
            const [fErr, _student] = await Student.fetchStudentAsFaculty(this.schoolId, this.facultyId, this.studentId);
            if (fErr) return fErr;
            runInAction(() => {
                this.student = _student;
            });
        }
        this.loading = false;
        return;
    }

    @observable loading = false;
    @action async save() {
        if (this.student == null) return;
        this.loading = true;

        let err: IErrorData<{}>|undefined;
        if (this.student.studentId < 1) {
            [err, ] = await this.student.createStudentsAsFaculty(this.schoolId, this.facultyId ,this.classId);
        } else {
            [err, ] = await this.student.updateStudentsAsFaculty(this.schoolId, this.facultyId ,this.classId);
        }
        runInAction(() => {
            this.loading = false;
        });
        return err;
    }

    @action set_schoolId = (v:DbIdentity) => { this.schoolId = v }

    @computed get isCreateNew() { return this.studentId == null || this.studentId < 1 }
}


export class StudentItemResult extends Student {
    @observable errors          ?: string[] | undefined = [];
    @observable results          ?: string[] | undefined = [];
    @observable status          ?: string | undefined;
}
export class BatchImportStudentsStore {
    constructor(private store:Store) {
    }

    @observable studentId: DbIdentity = DefaultId;
    @observable facultyId: DbIdentity = DefaultId;
    @observable.ref importFile   : UploadFile|undefined = undefined;
    @observable schoolId : DbIdentity = DefaultId;
    @observable classId : DbIdentity = DefaultId;
    @observable.ref aClass ?: Class = undefined;
    @observable.ref batchResults : StudentItemResult[]|undefined = undefined;

    @observable openUploadFileModal: boolean = false;

    @action async init({classId}:{classId: DbIdentity}) {
        this.classId = classId;
        this.loading = false;
        this.set_openUploadFileModal(true);

        if(this.schoolId < 1){
            this.schoolId = this.store.currentSchoolId;
        }

        if(this.facultyId < 1){
            this.facultyId = this.store.currentFacultyId;
        }

        const [cErr, _class] = await Class.fetchClassAsFaculty({schoolId: this.store.currentSchoolId, facultyId:this.store.currentFacultyId, classId});
        if (cErr) return cErr;
        runInAction(() => {
            this.aClass = _class;
            this.store.set_currentGradingTerm(this.aClass.gradingTerm, true);
        });

        return;
    }

    @observable loading = false;
    @action async save() {
        if (this.importFile == undefined) return;
        this.loading = true;

        let err: IErrorData<{}>|undefined;
        let results: StudentItemResult[]|undefined;
        if (!!this.importFile) {
            [err, results] = await uploadFile("POST", `/faculty/${this.facultyId}/school/${this.schoolId}/class/${this.classId}/student/batch`, this.importFile as any as File);
            this.loading = false;
            if (err) return err;
            runInAction(() => {
                this.batchResults = results;
                this.importFile = undefined;
            });
        }
        return;
    }

    @action set_schoolId = (v:DbIdentity) => { this.schoolId = v }

    @action set_openUploadFileModal = (v:boolean) => { this.openUploadFileModal = v }
}
