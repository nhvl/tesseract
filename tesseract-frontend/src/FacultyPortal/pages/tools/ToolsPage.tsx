import React, { FC, useCallback, useEffect, useState, useRef } from 'react';
import { observer } from 'mobx-react';

import { BasicLayout } from '../../layouts/BasicLayout';
import { Card, Button, Divider, List, Row, Col } from 'antd';

import {DiceRoller} from "./DiceRoller";

export const ToolsPage: FC<{}> = observer(({}) => {
    return (<BasicLayout>
        <CurrentTime />
        <Divider />
        <DiceRoller />
        <Divider />
        <StopWatch />
    </BasicLayout>);
});

export const CurrentTime: FC<{}> = observer(({}) => {
    const [, setState] = useState(0);
    useEffect(() => {
        let h: ReturnType<typeof setTimeout>;
        tick();
        function tick() {
            setState((c) => c+1);
            h = setTimeout(tick, 1000);
        }
        return () => clearTimeout(h)
    }, [])

    return (<Card>
        <h1>{new Date().toLocaleTimeString()}</h1>
    </Card>);
});

export const StopWatch: FC<{}> = observer(({}) => {
    const [startTime, setStartTime] = useState(Date.now());
    const [prevTime, setPrevTime] = useState(0);
    const [currentTime, setCurrentTime] = useState(Date.now());
    const [lapses, setLapses] = useState<number[]>([]);
    const handlerRef = useRef<ReturnType<any>>();
    const [mode, setMode] = useState<0|1|2>(0);

    const start = useCallback(() => {
        setStartTime(Date.now() - prevTime);
        setCurrentTime(Date.now());
        setMode(1);
        tick();
        function tick() {
            setCurrentTime(Date.now());
            handlerRef.current = setTimeout(tick, 100);
        }
    }, [prevTime, setStartTime, setCurrentTime]);

    const pause = useCallback(() => {
        clearTimeout(handlerRef.current);
        setPrevTime(Date.now() - startTime);
        setMode(2);
    }, [startTime, setPrevTime]);

    const lapse = useCallback(() => {
        setLapses(lapse => lapse.concat([Date.now() - startTime]))
    }, []);

    const stop = useCallback(() => {
        setStartTime(Date.now());
        setCurrentTime(Date.now());
        setPrevTime(0);
        setMode(0);
        clearTimeout(handlerRef.current);
    }, [setStartTime, setCurrentTime, setPrevTime]);

    return (<Card>
        <h1><Time value={Date.now() - startTime}/></h1>
        {mode == 0 ? (
            <Button onClick={start} icon="play-circle" />
        ) : (mode == 1 ? (<>
            <Button onClick={lapse} icon="plus-circle" />
            <Button onClick={pause} icon="pause-circle" />
        </>) : (<>
            <Button onClick={stop} icon="reload" />
            <Button onClick={start} icon="play-circle" />
        </>))}
        <div className="m-4" />
        {lapses.length > 0 && (
            <List
                dataSource={lapses}
                renderItem={(lapse:number, i:number) => (<List.Item>
                    <Row className="w-full">
                        <Col span={4}>{i+1}.</Col>
                        <Col span={10}>{i > 0 && (<h4>+<Time value={lapse - lapses[i-1]}/></h4>)}</Col>
                        <Col span={10}><h3><Time value={lapse}/></h3></Col>
                    </Row>
                </List.Item>)}
                />
        )}
    </Card>);
});

const Time: FC<{value:number}> = ({value}) => {
    let s = new Date(value).toJSON().slice(11, 23);
    if (s.startsWith("00:00")) s = s.slice(3);
    return (<>{s}</>);
}


