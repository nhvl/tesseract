import React, { FC, useCallback, useState, useRef, useEffect } from 'react';
import { observer } from 'mobx-react';
import { useStore } from '../../stores';
import { Row, Col, Button, Modal, message, Card, Form } from 'antd';
import {InputNumber} from "../../../components/inputs/antd/InputNumber";
import { useTranslation } from 'react-i18next';
import { Student } from "../../../models/Student";
import styles from "./GroupGenerator.module.less";
export const GroupGenerator: FC<{students:Student[]}> = observer(({students}) => {
    const {t} = useTranslation();
    const [nStudent, set_nStudent] = useState(null as number|null);
    const [nGroup, set_nGroup] = useState(null as number|null);
    const [groups, set_Groups] = useState([] as Student[][]);
    const handleStudentChange = useCallback((v:number) => {
        set_nStudent(v);
        set_nGroup(v ? Math.ceil(students.length/v): null);

    },[nStudent]);
    const handleGroupChange = useCallback((v:number) => {
        set_nGroup(v);
        set_nStudent(v ? Math.floor(students.length/v): null);
    },[nGroup]);

    const generateGroup = useCallback(() => {
        let divideStudents = students.concat();
        let groupItems = [];
        if(!nGroup){
            setTimeout(message.error(t("app.classes.tools.groupGenerator.validValues")),1000);
            return;
        }
        for(var i = 0; i < nGroup; i++){
            groupItems.push([] as Student[]);
        }
        while(divideStudents.length > 0){
            const divideStudents_Length = divideStudents.length;
            for(var i = 0; i < Math.min(nGroup, divideStudents_Length); i++) {
                groupItems[i].push(divideStudents.splice(Math.floor(Math.random() * divideStudents.length), 1)[0]);
            }
        }
        set_Groups(groupItems);
    },[nStudent, nGroup]);
    return(<div>
        <Row>
            <Col xs={24}>
                <label className={styles.grey}>{t("app.classes.tools.groupGenerator.classSize")}:</label>
                <span className="font-bold">{students.length}</span>
            </Col>
        </Row>
        <div className="m-4"></div>
        <Row gutter={16} className={`${styles.flex} ${styles.generate}`}>
            <Col xs={8}>
                <Form.Item label={t("app.classes.tools.groupGenerator.studentGroup")}>
                    <InputNumber value={nStudent!} onChange={handleStudentChange} max={students.length} min={1}/>
                </Form.Item>
            </Col>
            <Col xs={8}>
                <Form.Item label={t("app.classes.tools.groupGenerator.numberGroup")}>
                    <InputNumber value={nGroup!} onChange={handleGroupChange} max={students.length} min={1}/>
                </Form.Item>
            </Col>
            <Col xs={8} className="text-right">
                <Form.Item>
                    <Button htmlType="button" type="primary" onClick={generateGroup}>{t("app.classes.tools.groupGenerator.generate")}</Button>
                </Form.Item>
            </Col>
        </Row>
        <Row gutter={16} className={`${styles.flex} ${styles.group}`}>
            {groups.map((group, index) => (<Col xs={24} lg={12}>
                    <Card title = {t('app.classes.tools.groupGenerator.groupName', {name: index + 1})}>
                        {group.map(student => (<p>
                            {student.fullName}
                        </p>))}
                    </Card>
                    <div className="m-4"></div>
                </Col>))}
        </Row>
    </div>);
});
