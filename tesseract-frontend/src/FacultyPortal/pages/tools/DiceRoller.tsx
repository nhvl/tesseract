import React, { FC, useCallback, useState, useRef, useEffect, ReactNode } from 'react';
import { observer } from 'mobx-react';

import { Card, Button, Divider, InputNumber, Table, Form, Row, Col } from 'antd';
import { ColumnProps } from 'antd/lib/table';

import styles from "./DiceRoller.module.less";
import { useTranslation } from 'react-i18next';

const MAX_HISTORY = 10;
const MAX_DICES = 100;
const LsKvDiceRollHistory = "DiceRollHistory";

const drColumns: Array<ColumnProps<number[]>> = [
    { key:"", title:"Past Rolls", render: (_:any, xs:number[]) =>
        `${xs.reduce((s, x) => s + x, 0)} (${xs.map(x => x).join(", ")})`
    }
];

const diceFaces: ReadonlyArray<string> = "? ⚀ ⚁ ⚂ ⚃ ⚄ ⚅".split(" ");
const iDiceFaces:{[key:number]: ReactNode} = {
    [0]: "?",
    [1]: (<i className="fas fa-dice-one" />),
    [2]: (<i className="fas fa-dice-two" />),
    [3]: (<i className="fas fa-dice-three" />),
    [4]: (<i className="fas fa-dice-four" />),
    [5]: (<i className="fas fa-dice-five" />),
    [6]: (<i className="fas fa-dice-six" />),
};

export const DiceRoller: FC<{}> = observer(({}) => {
    const {t} = useTranslation();
    const [dices, setDices] = useState<ReadonlyArray<number>>([1]);
    const [rolling, setRolling] = useState(false);
    const [history, setHistory] = useState<Array<Array<number>>>([]);
    const handlerRef = useRef<ReturnType<any>>();
    const stopRolling = useCallback((_dices:ReadonlyArray<number>) => {
        clearTimeout(handlerRef.current);
        setRolling(false);
        const h = [_dices.slice()].concat(history).slice(0, MAX_HISTORY);
        setHistory(h);
        localStorage.setItem(LsKvDiceRollHistory, JSON.stringify(h));
    }, [dices, history]);
    const changeNoDice = useCallback((v:number|undefined) => {
        if (v == null || v < 1) return;
        v = Math.min(v, MAX_DICES);
        const ds = v < dices.length ? dices.slice(0, v) : (
                      v > dices.length ? dices.concat(Array.apply(null, Array(v - dices.length)).map(randomDice)) : (
                      dices));
        setDices(ds);
        if (rolling) stopRolling(ds);
    }, [dices, rolling, stopRolling]);
    const removeDice = useCallback(() => {
        if (dices.length > 1) changeNoDice(dices.length - 1);
    }, [dices, changeNoDice]);
    const addDice = useCallback(() => {
        changeNoDice(dices.length + 1);
    }, [dices, changeNoDice]);
    useEffect(() => {
        const h = localStorage.getItem(LsKvDiceRollHistory);
        if (!h) return;
        try {
            const hs = JSON.parse(h);
            if (!Array.isArray(hs) || hs.length < 1) return;
            setHistory(hs);
            setDices(hs[0])
        } catch(e) {
            console.warn(e)
        }
    }, []);

    const toogleRoll = useCallback(() => {
        if (rolling) {
            stopRolling(dices);
            return;
        }

        setRolling(true);
        tick(20);
        function tick(n:number) {
            const ds = dices.map(randomDice);
            setDices(ds);
            if (n > 0) handlerRef.current = setTimeout(tick, 100, n-1);
            else stopRolling(ds);
        }
    }, [dices, rolling, stopRolling]);

    return (<>
        <div className="flex justify-between items-end">
            <div className="flex items-end">
                <div><Button onClick={removeDice} icon="minus" className="mr-4" /></div>
                <div>
                    <Form.Item label={t('app.classes.tools.diceRoller.noOfDice')} className={styles.roll}>
                        <InputNumber value={dices.length} onChange={changeNoDice} max={MAX_DICES} />
                    </Form.Item>
                </div>
                <div>
                    <Button onClick={addDice} icon="plus" className="ml-4" />
                </div>
            </div>
            <div><Button type="primary" onClick={toogleRoll}>{rolling ? t("app.classes.tools.diceRoller.rolling") : t("app.classes.tools.diceRoller.throw")}</Button></div>
        </div>
        <div className={styles.dice}>{dices.map((dice, i) => (
            <span key={i}>{iDiceFaces[dice]}</span>
        ))}</div>
        <div className="responsiveTable">
            <Table dataSource={history} columns={drColumns} pagination={false} className={styles.pastRoll} rowKey={rowKey} />
        </div>
    </>);
});

function randomDice() {
    return Math.floor(Math.random() * 6) + 1
}

function rowKey(record:number[], index:number) { return String(index) }

