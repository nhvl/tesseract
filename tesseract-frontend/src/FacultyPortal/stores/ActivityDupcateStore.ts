import { observable, action, computed } from "mobx";
import { DefaultId, DbIdentity } from "../../models/types";
import { Store } from "./Store";

export class ActivityDuplicateStore {
  @observable activityId: DbIdentity = DefaultId;
  @observable chosenClasses: DbIdentity[] = [];

  @computed get ActivityDuplicateVisible(): boolean {
    return this.activityId != DefaultId;
  }

  constructor(private _: Store) {}

  @action showModal = (v: DbIdentity) => this.activityId = v;

  @action addClass = (v: DbIdentity) => this.chosenClasses = this.chosenClasses.concat([v]);
  @action removeClass = (v: DbIdentity) => this.chosenClasses = this.chosenClasses.filter(clsId => clsId != v);

  @action init = () => {
    this.activityId = DefaultId;
    this.chosenClasses=[];
  }

  @action closeModal =  this.init;
}
