import { observable, action, runInAction, computed } from "mobx";

import {DefaultId, DbIdentity} from "../../models/types";
import { GradeRange } from "../../models/GradeRange";
import { Class } from "../../models/Class";

import { Store } from "./Store";

export class ClassEditorStore {
    constructor(private store: Store) {
    }

    @action async init(classId:DbIdentity): Promise<Error|undefined> {
        this.aClass = undefined;
        this.classId = classId;
        this.loading = false;
        this.origClassName = "";

        const [gErr, _gradeRange] = await GradeRange.getSchoolGradeRange(this.store.currentSchoolId);
        if(!gErr) {
            this.defaultGradeRange = _gradeRange;
        }

        if (classId < 1) {
            this.aClass = new Class({
                gradingTerm: this.store.currentGradingTerm,
                schoolId   : this.store.currentSchoolId,
            });
            this.set_isDefaultGradeRange(true);
            return undefined;
        } else {
            this.loading = true;
            const [cErr, _class] = await Class.fetchClassAsFaculty({schoolId:this.store.currentSchoolId, facultyId:this.store.currentUser!.facultyId, classId});
            runInAction(() => {
                if (this.classId != classId) return;
                this.loading = false;
                if (!cErr) {
                    this.aClass = _class;
                    this.origClassName = _class.className;
                    this.store.set_currentGradingTerm(_class.gradingTerm, false);
                    if(this.aClass.useDefaultGradesRange){
                        this.set_isDefaultGradeRange(true);
                    }
                }
            });
            return cErr;
        }
    }

    @observable origClassName:string = "";

    @observable     classId     : DbIdentity = DefaultId;
    @observable     loading     : boolean = false;
    @observable.ref aClass     ?: Class;


    @observable     defaultGradeRange: GradeRange[] = [];
    @observable     isOpenGradeRangeModal: boolean = false;
    @observable     modalGradeRange: GradeRange[] = [];

    @action set_isDefaultGradeRange = (v: boolean) => {
        this.aClass!.set_useDefaultGradesRange(v);
        if(!v && this.aClass!.gradeRanges.length == 0){
            this.aClass!.gradeRanges = this.defaultGradeRange.slice();
        }
    }
    @action set_isOpenGradeRangeModal = (v: boolean) => {
        this.isOpenGradeRangeModal = v;
        if(v) {
            this.modalGradeRange = (this.aClass!.useDefaultGradesRange
            ? this.defaultGradeRange.slice()
            : this.aClass!.gradeRanges.slice());
        }
    }

    @observable.ref validateStatuses = {};
    @action setValidateStatus = (v:{}) => { this.validateStatuses = v }

    @computed get isCreateNew() { return this.classId < 1 }

    @action refineGradeRanges() {
        if (this.aClass == null) return;
        this.aClass.gradeRanges = this.aClass.gradeRanges
        .filter((option:GradeRange, index:number, array: GradeRange[]) => {
            return option.letterGrade.trim() != "" && option.percentGrade != null;
        });
    }
    @action async doSave(): Promise<[Error|undefined, Class]> {
        if (this.aClass == null) return [new Error("Class is null"), undefined!];

        this.loading = true;
        if(this.aClass.useDefaultGradesRange) {
            this.aClass.gradeRanges = [];
        }
        const [err, data] = await this.aClass.save(this.store.currentUser!.facultyId);
        runInAction(() => {
            this.loading = false;
            this.origClassName = data.className;
            this.aClass = data;
        });

        return [err, data];
    }
}
