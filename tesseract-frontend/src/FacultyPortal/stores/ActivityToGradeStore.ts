import { observable, action, computed } from "mobx";
import { ActivityDetail, Activity } from '../../models/Activity';
import { Store } from "./Store";

export class ActivityToGradeStore {
    constructor(private store: Store) {
    }

    @observable activityDetail : ActivityDetail[] = [];
    @observable isASCOrder:number = 0;
    @action set_order(v: number){
        this.isASCOrder = v;
        this.activityDetail = this.sortData(this.activityDetail);
    }
    @action set_activityDetail(v: ActivityDetail[]){
        this.activityDetail = this.sortData(v);
    }

    @action sortData(v: ActivityDetail[]){
        return v.slice().sort((a, b)=> ((this.isASCOrder == 0) ? 1 : -1) * Activity.sorter.dateDue(a, b));
    }

    @computed get displayActivities() {
        return this.activityDetail.filter(data => (data.submisstionCount!=0 && data.unGradeCount!=0));
    }

    @action async doFetch(facultyId:number, gradingTerm:number) {
        return ActivityDetail.getActivitiesToGrade(this.store.currentSchoolId, facultyId, gradingTerm);
    }
}
