import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../models/types";
import { Activity } from "../../models/Activity";
import { FacultyClass } from "../models/FacultyClass";
import { MediaTypeEnum } from "../../models/MediaTypeEnum";
import { ActItem, ActOption, EActItemType, IActItem, PollQuizItem } from "../../models/ActDoc";
import { StudentActDoc, StudentActItem } from "../../models/StudentActDoc";

import { Store } from "./Store";
import { ActivityStore } from "../../stores/ActivityStore";

import { UploadFile } from "antd/lib/upload/interface";

import { uploadFile, aFetch } from "../../services/api/fetch";
import { createError } from "../../services/api/AppError";

export class ViewStudentActDocStore {
    constructor(private store:Store) {
        this.sActivity = store.sActivity
    }

    private sActivity: ActivityStore;

    @observable activityId : DbIdentity = DefaultId;
    @observable studentId  : DbIdentity = DefaultId;
    @observable facultyId  : DbIdentity = DefaultId;
    @observable activity  ?: Activity   = undefined;
    @computed get aClass(): FacultyClass|undefined { return this.activity ? this.store.mClass.get(this.activity.classId) : undefined }

    @action async init(activityId:DbIdentity, studentId:DbIdentity):Promise<Error|undefined> {
        const {currentUser} = this.store;
        if (currentUser == null) return createError(new Error("401"), 401);

        this.activityId = activityId;
        this.facultyId  = currentUser.facultyId;
        this.studentId  = studentId;

        this.loading = true;
        const pA = this.store.fetchActivity(activityId);
        const pDoc = this.store.fetchStudentActDoc({activityId, studentId});
        const [err, doc] = await pDoc;
        const [aErr, a] = await pA;
        if (this.activityId != activityId) return;
        return runInAction(() => {
            this.loading = false;
            if (!err && !aErr) {
                this.activity = new Activity(a);
                this.doc = doc;
                this.set_originContentJS();
            }
            return err || aErr;
        });
    }

    @observable.ref doc?:StudentActDoc;

    //Store the origin content of doc to detect changes.
    private originContentJS:string = "";

    @observable isSavePreview    : boolean = false;

    @observable.ref bannerFile?: UploadFile;

    //Step = any will bypass validation for decimal values.
    //Step = 1 will allow increase and decrease number in InputNumber
    @observable numericStep : string = "any";

    @computed get isChanged() {
        return (
            JSON.stringify(this.doc) + JSON.stringify(this.bannerFile) !== this.originContentJS
        );
    }
    @action set_originContentJS = () => { this.originContentJS = JSON.stringify(this.doc) + JSON.stringify(this.bannerFile)}
    @action enable_numericStep = (v:boolean) => { this.numericStep = v ? "1" : "any"; }
    @action set_isSavePreview = (v: boolean) => { this.isSavePreview = v }

    @action addHeading = (item?: StudentActItem) => { this.addItem(new StudentActItem({type: EActItemType.Heading , fromTeacher: false }), item) }
    @action addText    = (item?: StudentActItem) => { this.addItem(new StudentActItem({type: EActItemType.Text    , fromTeacher: false }), item) }
    @action addMedia   = (item?: StudentActItem) => { this.addItem(new StudentActItem({type: EActItemType.Media   , fromTeacher: false }), item) }

    @action addItem(item: StudentActItem, afterItem: StudentActItem|undefined) {
        if (this.doc == null) return;
        if (afterItem == null) {
            this.doc.items.splice(0, 0, item);
        } else {
            const i = this.doc.items.findIndex(i => i == afterItem);
            this.doc.items.splice(i+1, 0, item);
        }
    }
    @action removeItem(item: ActItem): any {
        if (this.doc == null) return;
        this.doc.items = this.doc.items.filter(i => i != item);
    }

    @action onBlurMediaLink = (item: ActItem) => {
        if (item.type != EActItemType.Media) return;
        const {link} = item;
        if (!link) return;
        item.set_mediaFile(undefined);
        if (item.link == item.origLink) return;
        getMediaInfo(link).then(({mediaType, contentType}) => {
            if (link != item.link) return;
            item.mediaType   = mediaType;
            item.contentType = contentType;
            item.origLink    = item.link;
        })
    }

    //Remove and fix ActDoc data before submitting to server
    @action refine(): Error|undefined {
        if (this.doc == null) return;
        for(let item of this.doc.items){
            switch (item.type) {
                case EActItemType.ChoiceQuiz:
                    if(item.options){
                        item.options = item.options.filter((option:ActOption)=>{
                            return option.label.trim() != "" || option.imageFile != undefined || option.image != "";
                        });
                    }

                    if(!item.options || item.options.length == 0) {
                        return new Error("Multiple Choice Options are required.");
                    }

                    break;
                case EActItemType.MatchQuiz:
                    item.matchQuizzItems = item.matchQuizzItems.filter(i => i.hasLeftValue && i.hasRightValue);
                    break;
                case EActItemType.PollQuiz:
                    if(item.pollItems){
                        item.pollItems = item.pollItems.filter((pollItem:PollQuizItem)=>{
                            return pollItem.label.trim() != "" || pollItem.imageFile != undefined || pollItem.image != "";
                        });
                    }

                    if(!item.pollItems || item.pollItems.length == 0) {
                        return new Error("Polling Options are required.");
                    }
                    break;
                default:
                    break;
            }
        }
        return;
    }

    @observable loading = false; @action set_loading = (v:boolean) => { this.loading = v }
    save = async (): Promise<Error|undefined> => {
        const {doc} = this;
        if (doc == null) return undefined;

        const refined = this.refine();
        if (refined) return refined;

        this.loading = true;
        const errs = (await Promise.all(
            doc.items.map(async item => {
                if (item.imageFile == null) return null;
                const f = item.imageFile.originFileObj || (item.imageFile as any as File);
                const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                if (!err) {
                    item.set_imageFile(undefined);
                    item.set_image(url);
                }
                return err;
            }).concat(doc.items.map(async item => {
                if (item.mediaFile == null) return null;
                const f = item.mediaFile.originFileObj || (item.mediaFile as any as File);
                const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                if (!err) {
                    item.set_mediaFile(undefined);
                    item.set_link(url);
                }
                return err;
            })).concat(doc.items.map(async item => {
                //Upload images in Multiple Choice questions
                if (item.options == null) return null;
                const optionErrs = ([] as any).concat(await Promise.all(item.options.map(async option =>{
                    if(option.imageFile==null) return null;
                    const f = option.imageFile.originFileObj || (option.imageFile as any as File);
                    const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                    if (!err) {
                        option.set_imageFile(undefined);
                        option.set_image(url);
                    }
                    return err;
                }))).filter(Boolean);
                if(optionErrs.length > 0) return optionErrs;
                return null;
            })).concat(doc.items.map(async item => {
                //Upload images in Polling questions
                if (item.pollItems == null) return null;
                const pollErrs = ([] as any).concat(await Promise.all(item.pollItems.map(async poll =>{
                    if(poll.imageFile==null) return null;
                    const f = poll.imageFile.originFileObj || (poll.imageFile as any as File);
                    const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                    if (!err) {
                        poll.set_imageFile(undefined);
                        poll.set_image(url);
                    }
                    return err;
                }))).filter(Boolean);
                if(pollErrs.length > 0) return pollErrs;
                return null;
            })).concat(doc.items.map(async item => {
                //Upload images in Matching questions
                if (item.matchQuizzItems == null || item.matchQuizzItems.length == 0) return null;
                const matchQuizzErrs = ([] as any).concat(await Promise.all(item.matchQuizzItems.map(async matchQuizz => {
                    if(matchQuizz.leftImageFile != null) {
                        const f = matchQuizz.leftImageFile.originFileObj || (matchQuizz.leftImageFile as any as File);
                        const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                        if (!err) {
                            matchQuizz.set_leftImageFile(undefined);
                            matchQuizz.set_leftImage(url);
                        }
                        else {
                            return err;
                        }
                    }
                    if(matchQuizz.rightImageFile != null) {
                        const f = matchQuizz.rightImageFile.originFileObj || (matchQuizz.rightImageFile as any as File);
                        const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                        if (!err) {
                            matchQuizz.set_rightImageFile(undefined);
                            matchQuizz.set_rightImage(url);
                        }
                        else {
                            return err;
                        }
                    }

                    return null;
                }))).filter(Boolean);
                if(matchQuizzErrs.length > 0) return matchQuizzErrs;
                return null;
            }))
        )).filter(Boolean);

        if (errs.length > 0) { this.set_loading(false); return (errs[0]!); }
        const [err, newDoc] = await doc.save();
        runInAction(() => {
            this.set_loading(false);
            this.doc = newDoc;
            this.set_originContentJS();
        });
        return err;
    }

    onDrag(startIndex:number, endIndex:number) {
        if (!this.doc) return;
        const xs = this.doc.items;
        const [x] = this.doc.items.splice(startIndex, 1);
        xs.splice(endIndex, 0, x);
    }
}

const videoSiteRegex = [
    /youtube\.com/i,
    /youtu\.be/i,
    /vimeo\.com/i
];
const audioSiteRegex = [
    /soundcloud\.com/i,
];
const videoExts = [
    /\.mp4$/i,
    /\.mpeg$/i,
    /\.webm$/i,
    /\.ogv$/i,
];
const audioExts = [
    /\.mp3$/i,
    /\.wav$/i,
    /\.m4a$/i,
    /\.oga$/i,
];
const imageExts = [
    /\.jpg$/i,
    /\.jpeg$/i,
    /\.png$/i,
    /\.gif$/i,
    /\.svg$/i,
    /\.webp$/i,
    /\.tiff$/i,
    /\.bmp$/i,
];
async function getMediaInfo(url:string): Promise<{mediaType:IActItem["mediaType"], contentType:IActItem["contentType"]}> {
    if (videoSiteRegex.some(regex => regex.test(url))) {
        return { mediaType:MediaTypeEnum.VideoLink, contentType:"" };
    }
    if (audioSiteRegex.some(regex => regex.test(url))) {
        return { mediaType:MediaTypeEnum.AudioLink, contentType:"" };
    }
    if (videoExts.some(regex => regex.test(url))) {
        return { mediaType:MediaTypeEnum.VideoFile, contentType:"" };
    }
    if (audioExts.some(regex => regex.test(url))) {
        return { mediaType:MediaTypeEnum.AudioFile, contentType:"" };
    }
    if (imageExts.some(regex => regex.test(url))) {
        return { mediaType:MediaTypeEnum.ImageFile, contentType:"" };
    }

    const mediaInfo = await tryGetMediaInfo(url);
    if (mediaInfo != null) {
        return ({ mediaType:mediaInfo.mediaType, contentType:"" });
    }

    return {mediaType:MediaTypeEnum.Unsupport, contentType:""}
}


async function tryGetMediaInfo(src:string) {
    const imageInfo = await tryGetImageInfo(src);
    if (imageInfo != null) return imageInfo;
    const videoInfo = await tryGetVideoInfo(src);
    if (videoInfo != null) return videoInfo;
    const [err, info] = await aFetch<{contentType?:string, link?:string, mediaType:IActItem["mediaType"]}>("GET", `/Image/GetMediaInfo`, {url: src});
    if (!err && info.mediaType != MediaTypeEnum.Unknown) return info;
    return undefined;
}

function tryGetVideoInfo(src:string) {
    return new Promise<{mediaType:IActItem["mediaType"], width?:number, height?:number}>(resolve => {
        const video = document.createElement("video");
        video.onloadedmetadata = () => {
            resolve({
                mediaType: (video.videoTracks && video.videoTracks.length > 0) ? MediaTypeEnum.VideoFile : MediaTypeEnum.AudioFile,
                width: video.videoWidth,
                height: video.videoHeight,
            });
        };
        video.onerror = (err) => {
            console.warn(err);
            resolve(undefined)
        };
        video.src = src;
    });
}

function tryGetImageInfo(src:string) {
    return new Promise<{mediaType:IActItem["mediaType"], width?:number, height?:number}>(resolve => {
        const img = new Image();
        img.onload = () => {
            resolve({
                mediaType: MediaTypeEnum.ImageFile,
                width: img.naturalWidth,
                height: img.naturalHeight,
            });
        };
        img.onerror = (err) => {
            console.warn(err);
            resolve(undefined)
        };
        img.src = src;
    });
}
