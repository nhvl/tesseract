import { observable, action, runInAction } from "mobx";

import { DefaultId, DbIdentity } from "../../models/types";
import { Class } from "../../models/Class";
import { Category  } from "../../models/Category";
import { Activity, ActivityType } from "../../models/Activity";
import { ActDoc } from "../../models/ActDoc";
import { ActivityStore } from "./ActivityStore";

import { Store } from "./Store";

export class EditActivityStore {
    constructor(private store: Store) {
        this.sActivity = store.sActivity;
    }

    @observable         activityId         : DbIdentity = DefaultId  ;
    @observable         classId            : DbIdentity = DefaultId  ;
    @observable.ref     aClass            ?: Class      = undefined;

    @observable.ref     activity           : Activity = new Activity();

    @observable         isOpenCategoryModal: boolean = false;
    @observable         isUseCategory      : boolean = false;

    @observable.ref     edittingCategory  ?: Category;

    @observable.shallow categories         : Category[] = [];

    @observable.shallow categoryColorRange : string[] = [];

    isEdittingCategory(index: DbIdentity){
        return this.edittingCategory && this.edittingCategory.activityCategoryId == this.categories[index].activityCategoryId;
    }

    @action set_isUseCategory = (v: boolean) => {
        this.isUseCategory = v;
        if (v) {
            this.edittingCategory = undefined;
            if (this.activity.categoryId) {
                this.populateCategoryValues(this.activity.categoryId);
            } else {
                if (this.activity.weight <= 0) this.activity.weight = 1.;
            }
        }
    }

    @action set_isOpenCategoryModal = (v: boolean) => {
        this.isOpenCategoryModal = v;
    }

    @action categoryChange = (v: DbIdentity) => {
        this.activity.set_categoryId(v);
        this.populateCategoryValues(v);
    }

    isCreateNew(activityId: DbIdentity){
        return (activityId == DefaultId) || Number.isNaN(activityId);
    }

    isEmptyOrSpaces = (str:String)=>{
        return str === null || str.match(/^ *$/) !== null;
    }

    @action populateCategoryValues(categoryId: DbIdentity) {
        var currentCategory = this.categories.find(x=>x.activityCategoryId==categoryId);
        if(currentCategory){
            this.activity.set_color(currentCategory.color);
            this.activity.set_isGraded(currentCategory.isGraded);
            this.activity.set_maxScore(currentCategory.maxScore);
            this.activity.set_weight(currentCategory.weight);
        }
    }

    @action async loadCategories(facultyId: DbIdentity, isForce:boolean = false){
        if(isForce) this.categories = [];
        if(this.categories.length != 0) return;
        const [cErr, _categories] = await Category.fetch({schoolId:this.store.currentSchoolId, facultyId});
        if (cErr) return cErr;
        if (Array.isArray(_categories)) this.categories = _categories.map(opt => new Category(opt));

        const [aErr, _colors] = await Category.getSchoolDefaultColors(this.store.currentSchoolId);
        if (aErr) return aErr;
        if (Array.isArray(_colors)) this.categoryColorRange = _colors.map(opt => opt);

        if(this.activity.categoryId) {
            if(!this.isUseCategory) this.set_isUseCategory(true);
        }
        else{
            if(this.isUseCategory) this.set_isUseCategory(false);
        }
        return cErr || aErr;
    }

    @action async startCreateActivity(classId: DbIdentity, type: ActivityType) {
        if (classId < 1) return;

        this.set_isUseCategory(false);
        this.set_isOpenCategoryModal(false);

        this.classId = classId;
        this.activityId = DefaultId;
        this.aClass = undefined;
        this.activity = new Activity({
            classId,
            type,
            isGraded: type != ActivityType.Activity,
            maxScore: 10,
            weight: 1,
        });

        const pClass = await Class.fetchClassAsFaculty({schoolId:this.store.currentSchoolId, facultyId:this.store.currentFacultyId, classId});
        const [cErr, _class] = await pClass;
        if (this.classId != classId) return;
        if (cErr) return cErr;
        runInAction(() => {
            this.aClass = _class;
            this.store.set_currentGradingTerm(this.aClass.gradingTerm, true);
        });

        return cErr;
    }

    @action async startEditActivity(activityId: DbIdentity) {
        this.activityId = activityId;
        const [aErr, activity] = await this.store.fetchActivity(activityId);
        if (aErr || !activity) return aErr;
        if(activity.type == ActivityType.Discussion) return new Error("Cannot edit discussion activity.");
        this.activity = activity;
        runInAction(() => {
            this.set_isUseCategory(this.activity.categoryId!=undefined);
            const _class = this.aClass = activity.class;
            this.store.set_currentGradingTerm(_class.gradingTerm, true);
            this.store.sLeftNav.set_selectedKey(`class${_class.classId}`); // TODO: seperate sLeftNav logic
        });
        return aErr;
    }

    @action async doSave() {
        const isNew = this.isCreateNew(this.activityId);
        if(!this.isUseCategory){
            this.activity.set_categoryId(undefined);
        }
        const [aErr, activity] = await this.store.sActivity.save(this.activity, this.store.currentSchoolId, this.store.currentFacultyId);

        if (!aErr && isNew) {
            const actDoc = new ActDoc({
                activityId: activity.activityId,
                title     : activity.title,
                summary   : activity.description,
            });
            const [err,] = await actDoc.save(this.store.currentSchoolId, this.store.currentFacultyId);
            if (err) return [err, activity] as const;
        }

        return [aErr, activity] as const;
    }
}
