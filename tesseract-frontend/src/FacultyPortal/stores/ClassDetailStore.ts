import { ReactNode } from "react";
import { observable, action, runInAction, computed } from "mobx";
import { groupBy, map } from "lodash-es";

import { DefaultId, DbIdentity } from "../../models/types";
import { Class } from "../../models/Class";
import { Student } from "../../models/Student";
import { Activity, ActivityType } from "../../models/Activity";
import {ActivityScore} from "../../models/ActivityScore";

import { Store } from "./Store";
import { IErrorData } from "../../services/api/AppError";
import { message } from "antd";
import { GradeRange } from "../../models/GradeRange";

export class ClassDetailStore {
    constructor(private store: Store) {
        this.sActivity = store.sActivity;
    }

    private sActivity: Store["sActivity"];

    @observable         classId     : DbIdentity = DefaultId;
    @observable.shallow students    : Student[] = [];
    @observable         actScores   : ActivityScore[] = [];
    @observable         loading     : boolean = false;
    @observable.ref     aClass     ?: Class;

    @action set_classId     = (v: DbIdentity ) => { this.classId = Number.isNaN(v) ? DefaultId: v; }
    @action async initClass(schoolId:DbIdentity, facultyId:DbIdentity, classId:DbIdentity) {
        this.aClass = undefined;
        this.treeData = [];
        this.savingChangeResolve = undefined;
        this.savingChange = false;
        this.set_classId(classId);

        this.loading = true;

        const pClass = Class.fetchClassAsFaculty({schoolId, facultyId, classId});
        const pS = Student.fetchStudentsOfClassAsFaculty(schoolId, facultyId, classId);
        const pA = this.fetchActivities(classId);
        const pActScore = ActivityScore.fetchActivityScoresOfClassAsFaculty(schoolId, facultyId, this.classId);

        const [sErr, xs] = await pS;
        const [aErr] = await pA;
        const [cErr, c] = await pClass;
        const [scoresErr, scores] = await pActScore;

        return runInAction(() => {
            if (this.classId != classId) return;
            this.loading = false;
            if (!sErr) this.students = xs.sort(Student.sorter.fullName);
            if (!cErr) this.aClass = c;
            if (!scoresErr && Array.isArray(scores)) {
                const students = this.students;
                const activities = this.activities.filter(a => a.type > ActivityType.Activity && a.isGraded);

                this.actScores = students.flatMap(student =>
                    activities.flatMap(activity =>
                        scores.find(x => x.activityId == activity.activityId && x.studentId == student.studentId) ||
                        new ActivityScore({
                            activityId: activity.activityId,
                            studentId: student.studentId
                        })
                    ));
            }

            return (cErr || sErr || aErr || scoresErr);
        });
    }

    //Student Card
    @observable sortBy : number = 0;
    @computed get sortItems():{id:number, value:string}[] {
        return [
            {id: 0, value: "app.activities.dueDate"},
            {id: 1, value: "app.activities.assignDate"}
        ];
    }
    @observable isDescending: boolean = true;

    @action set_sortBy = (v: number)=>{ this.sortBy = v; }
    @action set_isDescending = (v: boolean)=>{ this.isDescending = v; }

    @computed get sortedActivities(){
        const gradedActities = this.activities.filter(x => x.isGraded && x.type > ActivityType.Activity && x.dateAssigned != null && x.dateAssigned < Date.now());
        switch (this.sortBy) {
            case 0:
                return gradedActities.sort((a, b) => ((a.dateDue || 0) - (b.dateDue || 0)) * (this.isDescending ? -1 : 1));
            default:
                return gradedActities.sort((a, b) => ((a.dateAssigned || 0) - (b.dateAssigned || 0)) * (this.isDescending ? -1 : 1));
        }
    }

    @computed get activities() { return this.sActivity.mClassId2Items.get(this.classId) || [] }
    @computed get cActivities() {
        return this.activities.filter(a => a.type == ActivityType.Activity || a.type == 0);
    }
    @computed get cAssignments() {
        return this.activities.filter(a => a.type == ActivityType.Assignment);
    }
    @computed get cAssessments() {
        return this.activities.filter(a => a.type == ActivityType.Assessment);
    }
    @computed get id2Activity() {
        return observable.map(this.cActivities.map(a => [a.activityId, a]));
    }
    @computed get rootActivities() {
        return this.cActivities.filter(a => a.parentActivityId == null).sort(Activity.sorter.sortIndex);
    }
    @computed get activityId2Children() {
        return observable.map(
            map(
                groupBy(this.cActivities.filter(a => a.parentActivityId != null), a => a.parentActivityId!),
                xs => [xs[0].parentActivityId!, xs.sort(Activity.sorter.sortIndex)]
            )
        );
    }

    @observable.ref treeData: ITreeNode[] = [];
    @action set_treeData = (treeData:any[]) => { this.treeData = treeData }

    @action async fetchActivities(classId:DbIdentity) {
        const [err, xs] = await this.sActivity.fetchActivitiesOfClass(this.store.currentSchoolId, this.store.currentFacultyId,classId);
        return [err, xs] as const;
    }

    private savingChangeResolve?: ((v?:IErrorData<any>) => void);
    private savingChange = false;
    @action treeChange = async (treeData:any[]) => {
        this.set_treeData(treeData);

        if (this.savingChangeResolve) {
            this.savingChangeResolve(undefined);
            this.savingChangeResolve = undefined;
        }
        if (this.savingChange) {
            return new Promise<IErrorData<any>>(resolve => this.savingChangeResolve = resolve);
        }
        const [err, isSaved] = await this.saveActivities();
        if (this.savingChangeResolve) {
            this.saveActivities().then(this.savingChangeResolve);
            this.savingChangeResolve = undefined;
        } else if (isSaved) message.success("Saved.");
        return err;
    }
    @action saveActivities = async () => {
        const xs = this.treeData.flatMap((node, i) => this.treeNode2Activities(node, i, undefined));
        if (xs.length < 1) return [undefined, false];
        this.savingChange = true;
        const [err] = await this.store.saveActivities(xs);
        runInAction(() => {
            this.savingChange = false;
        });
        return [err, true];
    }

    private treeNode2Activities(node:ITreeNode, i:number, parentActivityId?:DbIdentity): Activity[] {
        const {activityId, children} = node;

        const activity = this.id2Activity.get(activityId);
        if (activity == null) return [];

        const hasChanged = (activity.sortIndex != i || activity.parentActivityId != parentActivityId);
        if (hasChanged) {
            activity.sortIndex = i;
            activity.parentActivityId = parentActivityId;
        }

        const cs = children.flatMap((c, i) => this.treeNode2Activities(c, i, activityId));
        return (hasChanged ? ([activity].concat(cs)) : cs);
    }


    export2Csv = async () => {
        const {aClass} = this;
        if (aClass == null) return;

        const data = this.students.map(student => Object.assign(
            {
                "First Name"    : student.firstName,
                "Last Name"     : student.lastName,
                "Student Number": student.studentNumber,
                "Class Grade %" : student.cumulativeGrade,
                "Class Grade"   : GradeRange.retrieveLetterGrade(aClass!.gradeRanges, student.cumulativeGrade),
            }, ...this.sortedActivities.map(activity => {
                const score = this.actScores.find(score => score.activityId == activity.activityId && score.studentId == student.studentId);
                return ({[`${activity.title} (out of ${activity.maxScore})`]: score == null ? null : score.score })
            })));

        const mFileSaver = import("file-saver");
        const { csvFormat } = await import("d3-dsv");
        const csv = csvFormat(data);
        const {saveAs} = await mFileSaver;
        saveAs(new Blob([csv], {type: "text/csv;charset=utf-8"}), "grade.csv");
    }
}

export interface ITreeNode {
    activityId : DbIdentity,
    title      : ReactNode,
    subtitle  ?: ReactNode,
    expanded  ?: boolean,
    children   : ITreeNode[],
}
