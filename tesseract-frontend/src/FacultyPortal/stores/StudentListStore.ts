import { observable, action, runInAction, computed, reaction } from "mobx";

import { Student } from "../../models/Student";
import { Store } from "./Store";


import {DefaultId, DbIdentity} from "../../models/types";
import { Class } from "../../models/Class";
import { fromGeneralViewModel } from "../models/FacultyClass";

export interface StudentItem {
    student:Student,
    class_?:Class,
    classId:DbIdentity,
}

export class StudentListStore {
    constructor(private store:Store) {
        reaction(() => this.store.currentGradingTerm, (t) => {
            if (this.store.currentGradingTermClasses.some(c => c.classId == this.classId)) return;
            this.set_classId(DefaultId);
        })
    }

    @observable         classId  : DbIdentity = DefaultId;
    @observable.shallow students : Student[] = [];

    @computed get displayStudents(): StudentItem[] {
        return this.classId < 0
            ? this.students.flatMap(s =>
                s.classes.filter(classId => this.store.currentGradingTermClasses.some(c => c.classId == classId))
                    .map(classId => ({student:s, classId, class_:this.store.mClass.get(classId)})))
            : this.students.filter(s => s.classes.includes(this.classId))
                .map(s => ({student:s, classId:this.classId, class_:this.store.mClass.get(this.classId)}))
    }
    @computed get nStudent(): number {
        return (this.classId < 0
            ? this.students.reduce((count, s) => count + (s.classes.some(classId => this.store.currentGradingTermClasses.some(c => c.classId == classId)) ? 1 : 0), 0)
            : this.students.reduce((count, s) => count + (s.classes.includes(this.classId) ? 1 : 0), 0)
        );
    }

    async fetchData() {
        const [err, students] = await this.store.fetchStudents();
        if (!err) runInAction(() => {
            this.students = students;
        });
        return err;
    }

    @action set_classId = (v: DbIdentity) => { this.classId = v; }
}
