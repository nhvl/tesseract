import { observable, runInAction, action, computed } from "mobx";

import { Activity } from "../../models/Activity";
import { ActivityScore } from "../../models/ActivityScore";
import { Submission } from "../../models/Submission";

import { Store } from "./Store";

import { UploadFile } from "antd/lib/upload/interface";
import { SetSeenDuration } from "../../config";
import { CommentSeen } from "../../models/CommentSeen";
import { Modal } from "../../components/Modal/ModalAntd";
import i18n from "../../i18n";

export class ActivityModalStore {
    constructor(private store:Store) {
    }

    @observable score: number|undefined = undefined;
    @action set_score = (v:number) => { this.score = v }

    @observable.ref activity   ?: Activity;
    @observable.ref actScore   ?: ActivityScore;
    @observable.ref submission ?: Submission|null; // undefined: Not load | null: no submission
    @observable.ref commentSeen?: CommentSeen;
    @computed get attachments() {
        if (!this.submission) return [];
        return this.submission.attachments.map<UploadFile>(a => ({
            uid      : a.url,
            size     : 0,
            name     : a.fileName,
            url      : a.url,
            fileName : a.fileName,
            status   : "done",
            type     : a.contentType,
        }));
    }

    private resolve?: ((v:boolean) => void);

    @action open = (activity:Activity, actScore: ActivityScore) => {
        this.close(false);

        this.activity = activity;
        this.actScore = actScore;
        this.score    = actScore.score;

        this.fetchComments();
        this.fetchSubmission();
        return new Promise<boolean>(resolve => this.resolve = resolve);
    }
    @action close = (v:boolean) => {
        this.activity    = undefined;
        this.actScore    = undefined;
        this.score       = undefined;
        this.submission  = undefined;
        this.commentSeen = undefined;

        if (this.resolve) {
            this.resolve(v);
            this.resolve = undefined;
        }
    }

    async fetchComments() {
        const {actScore} = this; if (actScore == null || !actScore.threadId) return;
        const [err, vm] = await this.store.fetchCommentsOfThread(actScore.threadId);
        if (this.actScore != actScore) return;

        if (err) Modal.error({title: i18n.t('app.thread.comment.getCommentsFail'), content:err.message});
        else runInAction(() => {
            this.commentSeen = vm.commentSeens[0];
            setTimeout(() => {
                if (this.actScore != actScore) return;
                this.store.setSeen(actScore.threadId);
            }, SetSeenDuration);
        });
    }
    async fetchSubmission() {
        const {actScore} = this; if (actScore == null) return;
        const [err, submission] = await Submission.getAsFaculty({schoolId:this.store.currentSchoolId, facultyId:this.store.currentFacultyId, activityId:actScore.activityId, studentId:actScore.studentId});
        if (actScore != this.actScore) return;

        if (err) Modal.error({title: i18n.t('app.thread.comment.getCommentsFail'), content:err.message});
        else runInAction(() => {
            this.submission = submission;
        });
    }

    @observable isSaving:boolean = false;
    @action set_isSaving = (v:boolean) => { this.isSaving = v; }
    save = async () => {
        if (this.actScore != null &&
            this.score != null && this.actScore.score != this.score
        ) {
            this.set_isSaving(true);
            const actScore = this.actScore.clone();
            actScore.set_score(this.score);
            const err = await actScore.update(this.store.currentSchoolId, this.store.currentFacultyId);
            this.set_isSaving(false);
            if (err) return err;
        }

        this.close(true);
        return null;
    }
}
