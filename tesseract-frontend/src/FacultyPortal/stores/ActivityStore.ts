import { action } from "mobx";

import { DbIdentity } from "../../models/types";
import { Activity } from "../../models/Activity";

import {ActivityStore as BaseActivityStore} from "../../stores/ActivityStore"

export class ActivityStore extends BaseActivityStore {
    @action async fetchActivitiesOfClass(schoolId: DbIdentity, facultyId: DbIdentity, classId:DbIdentity) {
        const [err, xs] = await Activity.fetchActivitiesOfClassAsFaculty(schoolId, facultyId, classId);
        if (!err) this.storeItems(xs);
        return [err, xs] as const;
    }

    async save(item:Activity, schoolId: DbIdentity, facultyId: DbIdentity, ) {
        const [err, x] = await item.save(schoolId, facultyId);
        if (!err) this.storeItems([x]);
        return [err, x] as const;
    }

    async setPublish(schoolId: DbIdentity, facultyId: DbIdentity, activityId:DbIdentity, start: number | undefined, end: number | undefined) {
        const [err, x] = await Activity.setPublish(schoolId, facultyId, activityId, start, end);
        if (!err) this.storeItems([x]);
        return [err, x] as const;
    }
}
