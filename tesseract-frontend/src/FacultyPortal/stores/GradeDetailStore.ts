import { observable, action, runInAction, computed } from "mobx";
import { createError } from "../../services/api/AppError";

import {DbIdentity, DefaultId} from "../../models/types";
import { Activity } from "../../models/Activity";
import { Class } from "../../models/Class";
import { ActivityScoreInfo, ActivityScore } from "../../models/ActivityScore";

import { Store } from "./Store";

export class GradeDetailStore {
    constructor(private store: Store) {
    }

    @observable activityId           : DbIdentity = DefaultId;
    @observable activity             : Activity = new Activity();
    @observable activityScores       : ActivityScoreInfo[] = [];
    @observable cachedActivityScores : ActivityScoreInfo[] = [];
    @observable aClass               : Class = new Class();

    @computed get isOpenActivity() {
        return this.activity.dateDue == null || this.activity.dateDue <= Date.now();
    }

    async init(activityId: DbIdentity) {
        this.activityId = activityId;
        const [sErr, vm] = await ActivityScore.fetchActivityScoresOfActivityAsFaculty(this.store.currentSchoolId, this.store.currentFacultyId, activityId);
        if (this.activityId != activityId) return;
        if (sErr) return sErr;
        return runInAction(() => {
            const activity = this.activity = vm.activities[0];
            if(!activity.isGraded) {
                return createError(new Error(`You can"t set grade for activity ${this.activity.title}`), 400);
            }

            {
                this.aClass = new Class(vm.classes[0]);
                if (this.aClass != null) {
                    this.store.set_currentGradingTerm(this.aClass.gradingTerm, true);
                    this.store.sLeftNav.set_selectedKey(`class${this.aClass.classId}`);
                }
            }

            const studentId2ActivityScore = new Map(vm.activitiesScores.map(s => [s.studentId, s]));

            this.set_activityScore(
                vm.students.map(s => {
                    const activityScore = studentId2ActivityScore.get(s.studentId);
                    return new ActivityScoreInfo({
                        ...s.toJS(), fullName:`${s.fullName}`,
                        activityId,
                        isLate: activity.dateDue! < Date.now() && (activityScore == null || activity.dateDue! < activityScore.savedDate!),
                        isSubmitted: activityScore != null && activityScore.isSubmitted,
                        ...activityScore,
                    });
                }).sort(ActivityScore.sorter.score)
            );

            return;
        });
    }

    @observable isSaving: boolean = false;
    isShowSpinning(activityScore:ActivityScoreInfo) {
        if(!this.isSaving) return false;
        return this.cachedActivityScores.some(c => c.studentId == activityScore.studentId && JSON.stringify(c) != JSON.stringify(activityScore));
    }

    @action reset_cache = () => {
        this.cachedActivityScores = this.activityScores.map(s => s.clone());
        this.selectedBadges = this.activityScores.map(_ => undefined);
    }

    @observable.shallow selectedBadges: (string|undefined)[] = [];
    @action set_selectedBadges = (index: number, value: string) => {
        this.selectedBadges = this.selectedBadges.map((_, i) => i == index ? value : undefined);
    }
    @action clear_selectedBadges = () => {
        this.selectedBadges = this.selectedBadges.map(b => undefined);
    }
    @computed get isUpdatingBadges(){
        return this.selectedBadges.some(b => b != undefined);
    }

    @action save_score = (activityScore:ActivityScoreInfo)=>{
        return activityScore.update(this.store.currentSchoolId, this.store.currentFacultyId);
    }
    @action set_activityScore  = (v: ActivityScoreInfo[])=>{
        this.activityScores = v;
        this.reset_cache();
    }
}
