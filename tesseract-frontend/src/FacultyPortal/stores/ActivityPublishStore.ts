import { observable, action, computed } from "mobx";
import { Store } from "./Store";
import { DbIdentity, DefaultId } from '../../models/types';
import moment from "moment";

export class ActivityPublishStore {
  constructor(private store: Store) { }

  @observable activityId: DbIdentity = DefaultId;
  @observable isSet: boolean = false;
  @observable startTime: number | undefined;
  @observable endTime: number | undefined;

  @computed get ActivityPublishStore(): boolean {
    return this.activityId != undefined;
  }

  @action setStartTime = (v: number | undefined) => {
    if (v == undefined) {
      this.startTime = undefined;
      return;
    }

    this.startTime = moment(v).startOf('day').valueOf();
    
    if (this.endTime == undefined) return;
    if (this.startTime >= this.endTime)  this.endTime = undefined;
  }
  
  @action setEndTime = (v: number | undefined) => this.endTime =  v == undefined ? v : moment(v).startOf('day').valueOf();

  @action showModal = (activityId: DbIdentity, hasPublishDate: boolean) => {
    this.activityId = activityId;
    this.isSet = hasPublishDate;
  }


  @action init = () => {
    this.activityId = DefaultId;
    this.isSet = false;
    this.startTime = undefined;
    this.endTime = undefined;
  }
  
  @action closeModal =  this.init;
}
