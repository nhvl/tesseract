import { observable, action, computed, reaction, runInAction } from "mobx";
import { uniqBy } from "lodash-es";
import { RouterStore, HistoryAdapter } from "mobx-state-router";
import { history } from "../../services/history";
import { updateFavicon } from "../../utils/updateFavicon";

import i18n from "../../i18n";

import { routes, notFound, homeRoute } from "../routes";

import { DbIdentity, DefaultId } from "../../models/types";
import { FacultyUser, IUser } from "../../models/User";
import { School } from "../../models/School";
import { Activity } from "../../models/Activity";
import { Student } from "../../models/Student";
import { Faculty } from "../../models/Faculty";
import { Comment } from "../../models/Comment";
import { FacultyClass, sortClass, fromGeneralViewModel } from "../models/FacultyClass";

import {AuthorizedStore} from "../../stores/AuthorizedStore";
import {GTMStore} from "../../stores/GTMStore";

import { UserStore } from "../../stores/UserStore";
import { DiscussionStore } from "../../stores/DiscussionStore";
import { CommentStore } from "../../stores/CommentStore";
import { ActivityStore } from "./ActivityStore";
import { AsStudentClassStore } from "./AsStudentClassStore";

import { ClassDetailStore } from "./ClassDetailStore";
import { ClassEditorStore } from "./ClassEditorStore";
import { ClassListStore } from "./ClassListStore";
import { EditActivityStore } from "./EditActivityStore";
import { StudentListStore } from "./StudentListStore";
import { StudentDetailStore } from "./StudentDetailStore";
import { EditActDocStore } from "./EditActDocStore";
import { AccountSettingStore } from "../../components/Account/Settings/AccountSettingStore";
import { ActivityToGradeStore } from "./ActivityToGradeStore";
import { ActivityListStore } from "./ActivityListStore";
import { ActivityModalStore } from "./ActivityModalStore";
import { StudentSubmissionStore } from "./StudentSubmissionStore";
import { GradeDetailStore } from "./GradeDetailStore";
import { ActivityDuplicateStore } from "./ActivityDupcateStore";
import { ViewStudentActDocStore } from "./ViewStudentActDocStore";
import { ClassDiscussionListStore, ActivityDiscussionListStore } from "./DiscussionListStore";
import { DiscussionThreadStore } from "./DiscussionThreadStore";

const CurrentSchoolKey = "CurrentSchool";

import { createError } from "../../services/api/AppError";
import { ActivityPublishStore } from "./ActivityPublishStore";
import { NotificationStore } from "../../stores/NotificationStore";
import { Discussion } from "../../models/Discussion";
import { ClassTemplateStore } from "./ClassTemplateStore";
import { StudentActDoc } from "../../models/StudentActDoc";

import { ScoreBadge } from "../../models/ScoreBadge";
import { ActivityScore } from "../../models/ActivityScore";
import { Modal } from "../../components/Modal/ModalAntd";
import { EditStudentStore, BatchImportStudentsStore } from "../pages/editStudent/EditStudentStore";
import { ImportExistingStudentsStore } from "./ImportExistingStudentsStore";
import { GoogleDriveStore } from "../../stores/GoogleDriveStore";

export class Store extends AuthorizedStore {
    constructor() {
        super();
        this.routerStore = new RouterStore(this, routes, notFound);
        const historyAdapter = new HistoryAdapter(this.routerStore, history);
        historyAdapter.observeRouterStateChanges();

        reaction(() => this.routerStore.routerState, routerState => {
            const {classId} = routerState.params;
            if (!!classId) {
                this.sLeftNav.set_selectedKey("class" + routerState.params.classId);
            } else {
                switch (routerState.routeName) {
                    case "activitySettings":
                    case "editActDoc":
                    case "editStudent": this.sLeftNav.set_selectedKey("students"); break;
                    case "viewActDoc":
                    case "gradeDetail": break;
                    default:this.sLeftNav.set_selectedKey(routerState.routeName);
                }
            }
            this.sGTagManager.run();
        });

        this.refreshLocales();
    }

    @observable.shallow tSchool: School[] = [];
    @computed get mSchool() { return observable.map(this.tSchool.map(c => [c.schoolId, c])) }
    @action storeSchools(cs: School[]) { this.tSchool = uniqBy(cs.concat(this.tSchool), c => c.schoolId) }

    @observable.shallow tClass: FacultyClass[] = [];
    @computed get mClass() { return observable.map(this.tClass.map(c => [c.classId, c])) }
    @action storeClasses(cs: FacultyClass[]) {
        this.tClass = this.tClass.concat(cs.filter(c => {
            const o = this.mClass.get(c.classId);
            if (o != null) o.extends(c);
            return o == null
        }));
    }

    @observable.shallow tStudent: Student[] = [];
    @computed get mStudent() { return observable.map(this.tStudent.map(c => [c.studentId, c])) }
    @action storeStudents(cs: Student[]) {
        this.tStudent = this.tStudent.concat(cs.filter(c => {
            const o = this.mStudent.get(c.studentId);
            if (o != null) o.extends(c);
            return o == null;
        }));
    }

    @observable.shallow tFaculty: Faculty[] = [];
    @computed get mFaculty() { return observable.map(this.tFaculty.map(c => [c.facultyId, c])) }
    @action storeFacultys(cs: Faculty[]) {
        this.tFaculty = this.tFaculty.concat(cs.filter(c => {
            const o = this.mFaculty.get(c.facultyId);
            if (o != null) o.extends(c);
            return o == null;
        }));
    }

    sUser = new UserStore(this);
    @computed get mUser() {
        return observable.map(
            this.tStudent.map<[DbIdentity, IUser]>(c => [c.userId, c]).concat(
            this.tFaculty.map<[DbIdentity, IUser]>(c => [c.userId, c]),
            (!this.currentUser ? [] : [[this.currentUser.userId, this.currentUser]]),
            this.sUser.items.map<[DbIdentity, IUser]>(c => [c.userId, c])
        ));
    }

    sActivity       = new ActivityStore(this);
    sDiscussion     = new DiscussionStore(this);
    sComment        = new CommentStore(this);
    sAsStudentClass = new AsStudentClassStore(this);

    @observable.ref currentUser ?: FacultyUser;
    @computed get currentFacultyId() { return this.currentUser ? this.currentUser.facultyId : DefaultId; }

    async setToken(token:string) {
        const [err, data] = await super.setToken(token);
        if (err) return [err, data] as const;
        const u = this.currentUser = new FacultyUser({...data, token});
        if (u == null) return [createError(new Error("currentUser is null"), 403), data] as const;
        if (u.role != "Faculty") {
            location.href = location.origin;
            return [err, data] as const;
        }

        this.sNotification.init(token);

        runInAction(() => {
            let currentSchool = Number(data.activeSchoolId);
            if (Number.isNaN(currentSchool) || !u.schools.includes(currentSchool)) currentSchool = u.schools[0];
            this.currentSchoolId = currentSchool;
            localStorage.setItem(CurrentSchoolKey, String(currentSchool));
        });

        this.refresh();

        return [err, data] as const;
    }

    @observable currentSchoolId: DbIdentity = -1;
    @action set_currentSchool = (v: DbIdentity) => {
        this.UpdateActiveSchool(v).then(()=>{
            this.currentSchoolId = v;
            this.refreshSchool(v);
            this.routerStore.goTo(homeRoute);
        });
    };
    @computed get currentSchool() { return this.mSchool.get(this.currentSchoolId) }
    @computed get currentSchoolGradingTerm() { return this.currentSchool == null ? "" : this.currentSchool.currentGradingTerm }

    @observable currentGradingTerm:DbIdentity = DefaultId;
    @action set_currentGradingTerm = (v: DbIdentity, noRedirect = false) => {
        const gt = this.currentGradingTerm;
        if (gt === v) return;
        this.currentGradingTerm = v;
        if ((!noRedirect) && (gt > 0) &&
            (!"home openAssignments pastAssignments classes students accountSettings powerSearch"
            .includes(this.routerStore.routerState.routeName))
        ) {
            this.routerStore.goTo(homeRoute);
        }
    };

    @action async refresh() {
        if (this.currentUser == null) { console.error("currentUser is null"); return; }

        if (this.currentSchoolId < 0) this.currentSchoolId = this.currentUser!.schools[0];
        School.getSchoolsAsFaculty(this.currentFacultyId).then(([err, ss]) => {
            if (err) { console.error(err); return; }
            runInAction(() => {
                this.tSchool = uniqBy(ss.concat(this.tSchool), s => s.schoolId);
            });
            const school = this.mSchool.get(this.currentSchoolId);
            if (school != null && !school.tGradingTerm.some(t => t.gradingTermId == this.currentGradingTerm)) {
                this.set_currentGradingTerm(school.currentGradingTerm);
            }
            this.refreshFavicon();
        });
        this.refreshClasses(this.currentSchoolId);
        this.refreshScoreBadge();
        this.sGTagManager.initWithSchool(this.currentSchoolId);
    }
    @computed get schoolLogo(){
        return this.currentSchool ? this.currentSchool.logoUrl : null;
    }
    refreshFavicon(){
        if (this.currentSchool == null) return;
        const icon = this.currentSchool.iconUrl;
        if(!icon) return;
        updateFavicon(icon);
    }

    @action async refreshSchool(schoolId:DbIdentity) {
        const school = this.mSchool.get(schoolId);
        if (school == null) return createError(new Error(i18n.t("app.school.schoolNotFound")), 404);

        this.refreshClasses(schoolId);
        if (!school.tGradingTerm.some(t => t.gradingTermId == this.currentGradingTerm)) {
            this.set_currentGradingTerm(school.currentGradingTerm);
        }
        this.refreshFavicon();
        return;
    }
    @action async UpdateActiveSchool(schoolId:DbIdentity){
        const [err, data] = await FacultyUser.updateActiveSchool(schoolId);
        if (err) {
            Modal.error({content: i18n.t('component.updateActiveSchool.Error') });
        } else {
            this.setToken(data);
        }
    }

    @observable.ref finishRefreshClass = false;
    @action set_finishRefreshClass = (v: boolean) => { this.finishRefreshClass = v}; 
    async refreshClasses(schoolId?:DbIdentity) {
        if (schoolId == null) schoolId = this.currentSchoolId;

        if (!this.currentUser || this.currentUser.facultyId < 0) { debugger; return new Error("401"); }

        this.sAsStudentClass.fetchItems();

        const [error, cs] = await FacultyClass.fetchItems({
            schoolId,
            facultyId: this.currentUser.facultyId,
            gradingTermId: this.currentGradingTerm,
        });
        if (error) return error;
        this.storeClasses(cs);
        this.set_finishRefreshClass(true);
        return null;
    }

    sGTagManager            = new GTMStore(this);

    sAccountSetting         = new AccountSettingStore(this);
    sClassDetail            = new ClassDetailStore(this);
    sClassEditor            = new ClassEditorStore(this);
    sClassList              = new ClassListStore(this);
    sClassTemplates         = new ClassTemplateStore(this);
    sEditActivity           = new EditActivityStore(this);
    sEditActDoc             = new EditActDocStore(this);
    sStudentList            = new StudentListStore(this);
    sEditStudent            = new EditStudentStore(this);
    sBatchImportStudent     = new BatchImportStudentsStore(this);
    sImportExistingStudent  = new ImportExistingStudentsStore(this);
    sStudentDetail          = new StudentDetailStore(this);
    sActivityToGrade        = new ActivityToGradeStore(this);
    sActivityList           = new ActivityListStore(this);
    sActivityDup            = new ActivityDuplicateStore(this);
    sActivityPublish        = new ActivityPublishStore(this);
    sActivityModal          = new ActivityModalStore(this);
    sStudentSubmission      = new StudentSubmissionStore(this);
    sGradeDetail            = new GradeDetailStore(this);
    sViewStudentActDoc      = new ViewStudentActDocStore(this);
    sClassDiscussionList    = new ClassDiscussionListStore(this);
    sActivityDiscussionList = new ActivityDiscussionListStore(this);
    sDiscussionThread       = new DiscussionThreadStore(this);
    sGoogleDriveStore       = new GoogleDriveStore(this);
    sNotification           = new NotificationStore(this);

    @computed get currentGradingTermClasses(): FacultyClass[] {
        return (this.tClass
            .filter(c => (!this.currentGradingTerm || c.gradingTerm == this.currentGradingTerm) && (c.schoolId == this.currentSchoolId))
            .sort(sortClass)
        );
    }

    @observable scoreBadge: ScoreBadge = new ScoreBadge();
    @action set_ScoreBadge(scoreColor : ScoreBadge) { this.scoreBadge = scoreColor; }
    async refreshScoreBadge(){
        if (this.currentUser == null) return;
        const [err,data] = await ScoreBadge.getSchoolScoreBadge(this.currentSchoolId);
        if (err) throw err;
        this.set_ScoreBadge(data);
    }

    //#region Student
    async fetchStudentOfSchool() {
        const [err, vm] = await Student.fetchStudentsInSchoolAsFaculty(this.currentSchoolId, this.currentFacultyId);
        if (!err) runInAction(() => {
            this.storeStudents(vm.students);
            this.storeClasses(fromGeneralViewModel(vm));
            this.sActivity.storeItems(vm.activities);
        });
        return [err, vm.students, vm] as const;
    }
    async fetchStudentOfSchoolPagination(pageIndex:number
        , pageSize:number
        , excludedClassId: DbIdentity
        , filteredClassId: DbIdentity
        , searchText:string
        , sortColumnKey: string|undefined
        , sortOrder: boolean|"ascend"|"descend"|undefined){
        const [err, vm] = await Student.fetchStudentsInSchoolPaginationAsFaculty(this.currentSchoolId
            , this.currentFacultyId
            , pageIndex
            , pageSize
            , excludedClassId
            , filteredClassId
            , searchText
            , sortColumnKey
            , sortOrder
            );
        if (!err) runInAction(() => {
            this.storeStudents(vm.students);
            this.storeClasses(fromGeneralViewModel(vm));
            this.sActivity.storeItems(vm.activities);
        });
        return [err, vm.students, vm] as const;
    }
     async fetchStudentOfSchoolTotalPagination(excludedClassId: DbIdentity
         , filteredClassId: DbIdentity
         , searchText:string){
        const [err, vm] = await Student.fetchStudentsInSchoolTotalPaginationAsFaculty(this.currentSchoolId
            , this.currentFacultyId
            , excludedClassId
            , filteredClassId
            , searchText);
        return [err, vm] as const;
    }
    async fetchStudents() {
        const [err, vm] = await Student.fetchStudentsAsFaculty(this.currentSchoolId, this.currentFacultyId);
        if (!err) runInAction(() => {
            this.storeStudents(vm.students);
            this.storeClasses(fromGeneralViewModel(vm));
            this.sActivity.storeItems(vm.activities);
        });
        return [err, vm.students, vm] as const;
    }
    async fetchStudentDetail(q:{studentId:DbIdentity, classId?:DbIdentity}){
        const [err, vm] = await Student.fetchTeacherView({...q, facultyId: this.currentFacultyId, schoolId: this.currentSchoolId});
        if (!err) {
            this.storeStudents(vm.students);
            const cs = fromGeneralViewModel(vm);
            this.storeClasses(cs);
            this.sActivity.storeItems(vm.activities);
        };
        return [err, vm.students[0], vm] as const;
    }
    //#endregion

    //#region Activity
    async fetchActivity(activityId:DbIdentity){
        if(activityId == DefaultId || activityId == 0) return [undefined, undefined];
        const [err, x] = await Activity.fetchActivityAsFaculty({
            schoolId:this.currentSchoolId,
            facultyId: this.currentFacultyId,
            activityId,
        });
        if (!err) this.sActivity.storeItems([x]);
        return [err, x] as const;
    }
    async saveActivities(xs:Activity[]) {
        const [err, ys] = await Activity.updateAsFaculty(xs, {schoolId:this.currentSchoolId, facultyId:this.currentFacultyId});
        if (!err) this.sActivity.storeItems(ys);
        return [err, ys] as const;
    }
    //#endregion

    //#region ActivityScore
    async fetchActivityScoresOfActivity(activityId:DbIdentity){
        const [err, vm] = await ActivityScore.fetchActivityScoresOfActivityAsFaculty(this.currentSchoolId, this.currentFacultyId, activityId);
        if (!err) {
            this.sActivity.storeItems(vm.activities);
            vm.classes;
            vm.students;
            vm.activitiesScores;
            vm.classStudents;
        }
        return [err, vm] as const;
    }
    //#endregion


    //#region ActDoc
    fetchStudentActDoc(q:{activityId:DbIdentity, studentId:DbIdentity}) {
        return StudentActDoc.fetchForFaculty({
            schoolId: this.currentSchoolId,
            facultyId:this.currentFacultyId,
            ...q,
        });
    }
    //#endregion

    //#region Discussion
    async fetchClassDiscussions(classId:DbIdentity) {
        const [err, vm] = await Discussion.getDiscussionsOfClassAsFaculty({schoolId: this.currentSchoolId, facultyId: this.currentFacultyId, classId});
        if (!err) {
            this.sDiscussion.storeItems(vm.discussions, vm.activities);
            this.sActivity  .storeItems(vm.activities);
            this.sUser      .storeItems(vm.users);
            this.sComment   .storeItems(vm.comments);
        }
        return [err, vm] as const;
    }

    async fetchActivityDiscussions(activityId:DbIdentity) {
        const [err, vm] = await Discussion.getDiscussionsOfActivityAsFaculty({schoolId: this.currentSchoolId, facultyId: this.currentFacultyId, activityId});
        if (!err) {
            this.sDiscussion.storeItems(vm.discussions, vm.activities);
            this.sActivity  .storeItems(vm.activities);
            this.sUser      .storeItems(vm.users);
            this.sComment   .storeItems(vm.comments);
        }
        return [err, vm] as const;
    }

    async fetchDiscussion(q:{discussionId:DbIdentity, classId:DbIdentity}) {
        const [err, vm] = await Discussion.getDiscussionAsFaculty({schoolId: this.currentSchoolId, facultyId:this.currentFacultyId, ...q});
        if (!err) {
            this.sActivity.storeItems(vm.activities);
            this.sDiscussion.storeItems(vm.discussions, vm.activities);
            this.sUser.storeItems(vm.users);
        }
        return [err, vm.discussions[0], vm] as const;
    }

    @action async saveDiscussion(item:Discussion) {
        const [err, vm] = await item.save(this.currentSchoolId, this.currentUser!.facultyId);
        if (!err) {
            this.sActivity.storeItems(vm.activities);
            this.sDiscussion.storeItems(vm.discussions, vm.activities);
            this.sClassDetail.initClass(this.currentSchoolId, this.currentFacultyId, this.sClassDetail.classId);
        }
        return [err, vm] as const;
    }
    //#endregion

    //#region Comment
    async fetchCommentsOfThread(threadId:string) {
        const [err, vm] = await this.sComment.fetchCommentsOfThread(threadId);
        if (!err) {
            this.sUser.storeItems(vm.users);
        }
        return [err, vm] as const;
    }
    setSeen(threadId:string) {
        const ds = (this.sComment.mThreadId2Comments.get(threadId) || [])
                .map(c => c.dateCreated!).filter(d => d != null);
        if (ds.length < 1) return;

        const lastSeen = Math.max(...ds) + 1; // `+1` because C# DateTime precision .xxxyyyy round to JS .xxx0000
        Comment.setSeen({threadId:threadId, lastSeen})
    }
    //#endregion
}
