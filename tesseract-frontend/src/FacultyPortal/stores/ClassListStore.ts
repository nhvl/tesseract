import { observable, action, runInAction, computed } from "mobx";

import { Store } from "./Store";
import { FacultyClass, sortClass } from "../models/FacultyClass";


export class ClassListStore {
    constructor(private store: Store) {
    }

    @action async init() {
        this.loading = true;
        const err = await this.store.refreshClasses();
        runInAction(() => {
            this.loading = false;
            if (!err) {

            }
        })
        return err;
    }

    @computed get items(): FacultyClass[] {
        return this.store.tClass
            .filter(c => c.gradingTerm == this.store.currentGradingTerm)
            .sort(sortClass);
    }
    @observable         loading : boolean = false;

    @action changeOrder = async (startIndex:number, endIndex:number) => {
        const xs = this.items.slice();
        const [x] = xs.splice(startIndex, 1);
        xs.splice(endIndex, 0, x);
        const changedXs = xs.map((x, i) => {
            const index = i + 1;
            if (x.sortIndex == index) return undefined!;
            x.set_sortIndex(index);
            return new FacultyClass({ classId:x.classId, sortIndex:index })
        }).filter(Boolean);
        if (changedXs.length < 1) return null;
        const [err, cs] = await FacultyClass.batchUpdate(
            this.store.currentSchoolId,
            this.store.currentUser!.facultyId,
            changedXs);

        if(!err) this.store.storeClasses(cs);

        return err;
    }
}

