import { observable, action, computed } from "mobx";
import { Activity } from '../../models/Activity';
import {DefaultId, DbIdentity} from "../../models/types";
import { Store } from "./Store";

export class ActivityListStore {
    @observable activities: Activity[] = [];
    @observable isOpenActivity: boolean = true;

    @observable isLoading: boolean = false;

    @observable selectedGradingTerm: DbIdentity = DefaultId;

    @computed get displayActivities(): Activity[] {
        return (this.selectedGradingTerm < 0 || this.isOpenActivity)
            ? this.activities
            : this.activities.filter(s => s.class.gradingTerm==this.selectedGradingTerm)
    }

    constructor(private store: Store) {
    }
    @action set_gradingTerm  = (v: DbIdentity ) => { this.selectedGradingTerm = v; }
    @action set_activities   = (v: Activity[])  => { this.activities = v;}
    @action set_openActivity = (v: boolean)     => { this.isOpenActivity = v;}
    @action set_loading      = (v:boolean)      => { this.isLoading = v;}

    @action initActivities = async (facultyId: number) => {
        this.set_loading(true);
        if(this.isOpenActivity) {
            const [err, data] = await Activity.getOpenActivities(this.store.currentSchoolId, facultyId, this.selectedGradingTerm);
            this.set_loading(false);
            if(err) return err;
            this.set_activities(data);
        }
        else {
            const [err, data] = await Activity.getClosedActivities(this.store.currentSchoolId, facultyId, this.selectedGradingTerm);
            this.set_loading(false);
            if(err) return err;
            this.set_activities(data);
        }
        return null;
    }
}
