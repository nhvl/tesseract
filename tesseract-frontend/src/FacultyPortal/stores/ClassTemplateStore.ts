import { observable, action, runInAction } from "mobx";

import { Store } from "./Store";
import { Class } from "../../models/Class";
import { DbIdentity } from "../../models/types";


export class ClassTemplateStore {
    constructor(private store: Store) {}
    @observable loading : boolean = false;
    @observable templates: Class[] = [];

    @action async init(facultyId: DbIdentity) {
        this.loading = true;
        this.templates = [];
        var error = await this.fetchTemplates(facultyId);
        this.loading = false;
        return error;
    }

    @action async fetchTemplates(facultyId: DbIdentity) {
        const [err, templates] = await Class.getTemplates({schoolId: this.store.currentSchoolId, facultyId});
        if (!err) runInAction(() => {
            this.templates = templates;
        });
        return err;
    }
}

