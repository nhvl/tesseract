import { observable, action, runInAction, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../models/types";
import { Student } from "../../models/Student";
import { ActDoc, ActItem, EActItemType, MatchQuizzItem } from "../../models/ActDoc";

import { Store } from "./Store";

import { UploadFile } from "antd/lib/upload/interface";
import { uploadFile } from "../../services/api/fetch";
import { GoogleDrive } from "../../models/GoogleDrive";

export class EditActDocStore {
    constructor(private store: Store) {}

    @observable activityId:DbIdentity = DefaultId;
    @computed get activity() { return this.store.sActivity.mId2Item.get(this.activityId) }
    @computed get aClass() { return this.activity ? this.store.mClass.get(this.activity.classId) : undefined }

    @computed get availableDiscussions() {
        const classId = this.activity ? this.activity.classId : undefined;
        return (classId
            ? (this.store.sDiscussion.mClassId2Items.get(classId) || [])
            : (this.store.sDiscussion.mActivityId2Items.get(this.activityId) || []))
            .filter(d => d.activityId == this.activityId || d.activityId == null)
    }

    @action async init(activityId:DbIdentity):Promise<Error|undefined> {
        this.activityId = activityId;
        this.set_isSavePreview(false);
        this.enable_numericStep(true);

        this.loading = true;
        const pA = this.store.fetchActivity(activityId);
        const [dErr, doc] = await ActDoc.fetchAsFaculty(this.store.currentSchoolId, this.store.currentFacultyId, activityId);
        const [aErr, a] = await pA;
        if (this.activityId != activityId || !a) return undefined;

        if (!aErr && !dErr) {
            const [sErr, ss] = await Student.fetchStudentsOfClassAsFaculty(this.store.currentSchoolId, this.store.currentFacultyId, a.classId);
            if (sErr) console.error(sErr)
                else doc.set_totalStudent(ss.length);
            this.store.fetchClassDiscussions(a.classId);
        }

        return runInAction(() => {
            this.loading = false;
            if (!aErr) {
                if (this.aClass) {
                    this.store.set_currentGradingTerm(this.aClass.gradingTerm, true);
                    this.store.sLeftNav.set_selectedKey(`class${a.classId}`);
                }
            }
            if (!dErr) {
                this.doc = doc;
                this.set_originContentJS();
            } else if (dErr.status == 404) {
                this.doc = new ActDoc({
                    activityId, title:a.title, summary:a.description,
                });
                this.set_originContentJS();
            } else return dErr;
            return aErr;
        });
    }

    @observable.ref doc?:ActDoc;

    //Store the origin content of doc to detect changes.
    @observable.ref originContentJS:string = "";

    @observable isSavePreview    : boolean = false;

    @observable.ref bannerFile?: UploadFile;
    @action set_bannerFile = async (v: UploadFile|undefined) => { this.bannerFile = v }
    @action removeBanner = () => {
        this.set_bannerFile(undefined);
        if (this.doc) this.doc.set_banner("");
    }

    //Step = any will bypass validation for decimal values.
    //Step = 1 will allow increase and decrease number in InputNumber
    @observable numericStep : string = "any";

    @computed get isChanged() {
        return (
            JSON.stringify(this.doc) + JSON.stringify(this.bannerFile) !== this.originContentJS
        );
    }
    @action set_originContentJS = () => { this.originContentJS = JSON.stringify(this.doc) + JSON.stringify(this.bannerFile)}
    @action enable_numericStep = (v:boolean) => { this.numericStep = v ? "1" : "any"; }
    @action set_isSavePreview = (v: boolean) => { this.isSavePreview = v }

    @action addHeading    = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.Heading    , }), item) }
    @action addImage      = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.Heading    , }), item) }
    @action addText       = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.Text       , }), item) }
    @action addMedia      = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.Media      , }), item) }
    @action addEmbed      = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.Embed      , }), item) }
    @action addNumQuiz    = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.NumericQuestion, }), item) }
    @action addCQuiz      = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.ChoiceQuiz, }), item) }
    @action addMQuiz      = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.MatchQuiz, matchQuizzItems: Array.apply(null, Array(4)).map(_ => new MatchQuizzItem) }), item) }
    @action addTQuiz      = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.TextQuiz   , }), item) }
    @action addPollQuiz   = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.PollQuiz   , }), item) }
    @action addDiscussion = (item?: ActItem) => { this.addItem(new ActItem({type: EActItemType.Discussion, }), item) }

    @action addItem(item:ActItem, afterItem:ActItem|undefined) {
        if (this.doc == null) return;
        if (afterItem == null) {
            const i = this.doc.items.length;
            this.doc.items.splice(i, 0, item);
        } else {
            const i = this.doc.items.findIndex(i => i == afterItem);
            this.doc.items.splice(i+1, 0, item);
        }
    }
    @action removeItem(item: ActItem) {
        if (this.doc == null) return;
        this.doc.items = this.doc.items.filter(i => i != item);
    }

    @observable.ref edittingSettingItem?: ActItem;
    @action beginEditItemSettings(item?: ActItem){
        this.edittingSettingItem = item;
    }

    //Remove and fix ActDoc data before submitting to server
    @action refine(): Error|undefined {
        if (this.doc == null) return;
        for (let item of this.doc.items) {
            switch (item.type) {
                case EActItemType.ChoiceQuiz:
                    if (item.options) item.options = item.options.filter((option) => option.label.trim() != "" || option.imageFile != undefined || option.image != "");
                    if (!item.options || item.options.length == 0) return new Error("Multiple Choice Options are required.");
                    break;
                case EActItemType.MatchQuiz:
                    item.matchQuizzItems = item.matchQuizzItems.filter(i => i.hasLeftValue && i.hasRightValue);
                    break;
                case EActItemType.PollQuiz:
                    if (item.pollItems) item.pollItems = item.pollItems.filter((pollItem) => pollItem.label.trim() != "" || pollItem.imageFile != undefined || pollItem.image != "");
                    if (!item.pollItems || item.pollItems.length == 0) return new Error("Polling Options are required.");
                    break;
                default:
                    break;
            }
        }
        return;
    }

    @observable loading = false; @action set_loading = (v:boolean) => { this.loading = v }
    save = async (): Promise<Error|undefined> => {
        const {doc} = this;
        if (doc == null) return undefined;

        const refined = this.refine();
        if (refined) return refined;

        this.loading = true;
        if (this.bannerFile != null) {
            const f = this.bannerFile.originFileObj || (this.bannerFile as any as File);
            const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
            if (err) { this.set_loading(false); return err }
            this.set_bannerFile(undefined);
            doc.set_banner(url);
        };

        const pImages = Promise.all(
            doc.items.map(async item => {
                if (item.imageFile == null) return;
                const f = item.imageFile.originFileObj || (item.imageFile as any as File);
                const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                if (!err) {
                    item.set_imageFile(undefined);
                    item.set_image(url);
                }
                return err;
            }).concat(doc.items.map(async item => {
                if (item.mediaFile == null) return;
                const f = item.mediaFile.originFileObj || (item.mediaFile as any as File);
                const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                if (!err) {
                    item.set_mediaFile(undefined);
                    item.set_link(url);
                }
                return err;
            }))
            .concat(doc.items.map(async item => {
                if (item.googleFile == null) return;
                const f = item.googleFile;
                const [err, url] = await GoogleDrive.downloadGoogleFile(f);
                if (!err) {
                    item.set_googleFile(undefined);
                    item.set_embedLink(url);
                }
                return err;
            }))
            .concat(doc.items.flatMap(item => {
                //Upload images in Multiple Choice questions
                if (item.options == null) return [];
                return (item.options.map(async option => {
                    if (option.imageFile == null) return;
                    const f = option.imageFile.originFileObj || (option.imageFile as any as File);
                    const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                    if (!err) {
                        option.set_imageFile(undefined);
                        option.set_image(url);
                    }
                    return err;
                }));
            })).concat(doc.items.flatMap(item => {
                //Upload images in Polling questions
                if (item.pollItems == null) return [];
                return (item.pollItems.map(async poll => {
                    if (poll.imageFile == null) return;
                    const f = poll.imageFile.originFileObj || (poll.imageFile as any as File);
                    const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                    if (!err) {
                        poll.set_imageFile(undefined);
                        poll.set_image(url);
                    }
                    return err;
                }));
            })).concat(doc.items.flatMap(item => {
                //Upload images in Matching questions
                if (item.matchQuizzItems == null || item.matchQuizzItems.length == 0) return [];
                return (item.matchQuizzItems.map(async matchQuizz => {
                    if (matchQuizz.leftImageFile != null) {
                        const f = matchQuizz.leftImageFile.originFileObj || (matchQuizz.leftImageFile as any as File);
                        const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                        if (err) return err;
                        matchQuizz.set_leftImageFile(undefined);
                        matchQuizz.set_leftImage(url);
                    }
                    if (matchQuizz.rightImageFile != null) {
                        const f = matchQuizz.rightImageFile.originFileObj || (matchQuizz.rightImageFile as any as File);
                        const [err, url] = await uploadFile("POST", "/Image/UploadMedia", f);
                        if (err) return err;
                        matchQuizz.set_rightImageFile(undefined);
                        matchQuizz.set_rightImage(url);
                    }
                    return;
                }));
            }))
        );

        const pDiscussions = Promise.all(doc.items
            .filter(item => item.type == EActItemType.Discussion &&
                            item.discussion != null &&
                            !!item.discussion.title.trim())
            .map(async item => {
                const [err, vm] = await this.store.saveDiscussion(item.discussion!);
                if (!err) {
                    const discussion = vm.discussions[0];
                    item.set_discussionId(discussion.discussionId);
                    item.set_discussion(discussion.clone());
                }
                return err!;
            }));

        const iErrs = (await pImages).filter(Boolean);
        if (iErrs.length > 0) { this.set_loading(false); return (iErrs[0]!); }

        const pErrs = (await pDiscussions).filter(Boolean);
        if (pErrs.length > 0) { this.set_loading(false); return (pErrs[0]!); }

        const [err, newDoc] = await doc.save(this.store.currentSchoolId, this.store.currentFacultyId);
        runInAction(() => {
            this.set_loading(false);
            this.doc = newDoc;
            this.set_originContentJS();
        });
        return err;
    }

    onDrag(startIndex:number, endIndex:number) {
        if (!this.doc) return;
        const xs = this.doc.items;
        const [x] = this.doc.items.splice(startIndex, 1);
        xs.splice(endIndex, 0, x);
    }
}
