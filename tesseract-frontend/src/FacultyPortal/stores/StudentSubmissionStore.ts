import { observable, runInAction, action, computed } from "mobx";

import { DbIdentity, DefaultId } from "../../models/types";
import { Student } from "../../models/Student";
import { Activity, ActivityType } from "../../models/Activity";
import { Discussion } from "../../models/Discussion";
import { ActivityScore, FiveC } from "../../models/ActivityScore";
import { Submission } from "../../models/Submission";
import { CommentSeen } from "../../models/CommentSeen";

import { ActivityStore } from "./ActivityStore";
import { Store } from "./Store";

import { UploadFile } from "antd/lib/upload/interface";
import { SetSeenDuration } from "../../config";

import i18n from "../../i18n";
import { Modal } from "../../components/Modal/ModalAntd";


export class StudentSubmissionStore {
    constructor(private store:Store) {
        this.sActivity = store.sActivity
    }

    private sActivity: ActivityStore;

    @observable studentId: DbIdentity = DefaultId;
    @observable activityId: DbIdentity = DefaultId;

    @observable currentScore?: ActivityScore;

    @observable.ref activity    ?: Activity;
    @observable.ref actScore    ?: ActivityScore;
    @observable.ref submission  ?: Submission;
    @observable.ref discussion  ?: Discussion;
    @observable.ref student     ?: Student;
    @observable.ref commentSeen ?: CommentSeen;
    @computed get attachments() {
        if (!this.submission) return [];
        return this.submission.attachments.map<UploadFile>(a => ({
            uid      : a.url,
            size     : 0,
            name     : a.fileName,
            url      : a.url,
            fileName : a.fileName,
            status   : "done",
            type     : a.contentType,
        }));
    }

    @computed get isReSubmitted() { 
        return this.actScore 
                && this.actScore.isSubmitted 
                && this.submission 
                && this.submission.dateCreated < this.submission.dateUpdated 
    }

    @action private reset() {
        this.studentId = DefaultId;
        this.activityId = DefaultId;
        this.activity = undefined;
        this.actScore = undefined;
        this.submission = undefined;
        this.discussion = undefined;
        this.commentSeen = undefined;
        this.selectedSkill = undefined;
    }

    @action init = async (q:{studentId:DbIdentity, activityId:DbIdentity}) => {
        this.reset();
        const {studentId, activityId} = q;
        this.studentId = studentId;
        this.activityId = activityId;

        this.fetchActivity(activityId);
        this.fetchStudent(studentId);
        this.fetchSubmission(q);
        const err = await this.fetchActivityScore(q);
        if (err) return err;
        this.fetchComments(q);
        return;
    }

    private async fetchActivity(activityId:DbIdentity) {
        const [err, activity] = await this.store.fetchActivity(activityId);
        if (this.activityId != activityId) return;
        if (err) Modal.error({title: i18n.t('app.students.errror.getActivityFail'), content: err.message})
        else runInAction(() => {
            this.activity = activity;
            if(this.activity && this.activity.type == ActivityType.Discussion){
                this.fetchDiscussion(activityId);
            }
        });
    }

    private async fetchActivityScore({activityId, studentId}:{activityId:DbIdentity, studentId:DbIdentity}) {
        const [err, score] = await ActivityScore.fetchAsFaculty(this.store.currentSchoolId, this.store.currentFacultyId, activityId, studentId);
        if (this.studentId != studentId || this.activityId != activityId) return;
        if (err) Modal.error({title: i18n.t('app.students.errror.getStudentFail'), content: err.message})
        else runInAction(() => {
            this.actScore = score.clone();
            this.currentScore = score.clone();
        });
        return err;
    }

    private async fetchStudent(studentId:DbIdentity) {
        const [err, student] = await Student.fetchStudentAsFaculty(this.store.currentSchoolId, this.store.currentUser!.facultyId, studentId);
        if (this.studentId != studentId) return;
        if (err) Modal.error({title: i18n.t('app.students.errror.getStudentFail'), content: err.message})
        else runInAction(() => {
            this.student = student;
        });
    }

    private async fetchComments({studentId, activityId}:{studentId:DbIdentity, activityId:DbIdentity}) {
        if (this.actScore == null || !this.actScore.threadId) return;
        const [err, vm] = await this.store.fetchCommentsOfThread(this.actScore.threadId);
        if (this.studentId != studentId || this.activityId != activityId) return;

        if (err) Modal.error({title: i18n.t('app.thread.comment.getCommentsFail'), content:err.message});
        else runInAction(() => {
            this.commentSeen = vm.commentSeens[0];
            setTimeout(() => {
                if (this.studentId != studentId || this.activityId != activityId) return;
                if (this.actScore == null || !this.actScore.threadId) return;
                this.store.setSeen(this.actScore.threadId);
            }, SetSeenDuration);
        });
    }

    private async fetchSubmission({studentId, activityId}:{studentId:DbIdentity, activityId:DbIdentity}) {
        const [err, submission] = await Submission.getAsFaculty({schoolId: this.store.currentSchoolId, facultyId: this.store.currentFacultyId, studentId, activityId});
        if (this.studentId != studentId || this.activityId != activityId) return;

        if (err) Modal.error({title: i18n.t('app.activities.actDoc.getSubmissionFail'), content:err.message});
        else runInAction(() => {
            this.submission = submission;
        });
    }

    private async fetchDiscussion(activityId:DbIdentity){
        const [err, discussion] = await Discussion.getDiscussionFromDiscussionActivityAsFaculty({schoolId: this.store.currentSchoolId, facultyId: this.store.currentFacultyId, discussionActivityId: activityId});
        if (this.activityId != activityId) return;

        if (err) Modal.error({title: i18n.t('app.activities.actDoc.getDiscussionFail'), content:err.message});
        else runInAction(() => {
            this.discussion = discussion;
        });
    }

    @computed get isChangeScore(){
        return this.actScore != null &&
            this.currentScore != null && JSON.stringify(this.actScore)!=JSON.stringify(this.currentScore);
    }

    @observable isSaving:boolean = false;
    @action set_isSaving = (v:boolean) => { this.isSaving = v; }
    save = async () => {
        if (this.actScore != null && this.isChangeScore) {
            this.set_isSaving(true);
            const actScore = this.actScore.clone();
            const [err, data] = await actScore.update(this.store.currentSchoolId, this.store.currentFacultyId);
            this.set_isSaving(false);
            return err;
        }

        return null;
    }

    @observable selectedSkill:FiveC|undefined = undefined;
    @action set_selectedSkill = (v: FiveC|undefined) => {this.selectedSkill = v;}
}
