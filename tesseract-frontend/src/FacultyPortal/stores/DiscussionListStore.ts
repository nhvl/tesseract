import { observable, action, computed, runInAction } from "mobx";

import {DefaultId, DbIdentity} from "../../models/types";
import { Discussion } from "../../models/Discussion";
import { Category } from "../../models/Category";
import { Activity } from "../../models/Activity";

import { Store } from "./Store";
import { DiscussionStore } from "../../stores/DiscussionStore";
import { ActivityStore } from "../../stores/ActivityStore";

export class EditDiscussionStore {
    constructor(protected store:Store) {
        this.sDiscussion = store.sDiscussion;
        this.sActivity = store.sActivity;
    }
    protected sDiscussion: DiscussionStore;
    protected sActivity : ActivityStore;

    @observable.ref item?: Discussion;
    @action beginEdit = (item:Discussion) => {
        this.item = item.clone();
    }
    @action beginCreate = () => {
        this.item = new Discussion();
    }
    @action cancelCreate = () => {
        this.item = undefined;
    }
    @action create = async () => {
        const {item} = this;
        if (item == null) return;
        const [err, x] = await this.store.saveDiscussion(item);
        if (item !== this.item) return;
        if (err == null) runInAction(() => {
            this.item = undefined;
        });
        return err;
    }
    @action publish = async (item:Discussion) => {
        item.startTime = Date.now();
        const [err, x] = await this.store.saveDiscussion(item);
        return err;
    }
    @action hide = async (item:Discussion) => {
        item.startTime = undefined;
        const [err, x] = await this.store.saveDiscussion(item);
        return err;
    }
    @action reopen = async (item:Discussion) => {
        item.endTime = undefined;
        const [err, x] = await this.store.saveDiscussion(item);
        return err;
    }
    @action close = async (item:Discussion) => {
        item.endTime = Date.now();
        const [err, x] = await this.store.saveDiscussion(item);
        return err;
    }
}

export class DiscussionListStore extends EditDiscussionStore {
    @observable.ref threadUnread = observable.map<string, number>();
    @observable      categoryColorRange : string[] = [];

    @computed get displayItems():Discussion[] {
        return [];
    }

    @action reset() {
        this.item = undefined;
    }
}

export class ClassDiscussionListStore extends DiscussionListStore {
    @observable         classId   : DbIdentity = DefaultId;

    @computed get displayItems() {
        return (this.classId < 0 ? [] :
            this.sDiscussion.mClassId2Items.get(this.classId)) || []
    }

    @action reset() {
        super.reset();
        this.classId = DefaultId;
    }

    @action async init({classId}: {classId:DbIdentity}) {
        this.reset();
        this.classId = classId;
        const [err, vm] = await this.store.fetchClassDiscussions(classId);

        const [cErr, _colors] = await Category.getSchoolDefaultColors(this.store.currentSchoolId);
        if (cErr) return cErr;
        if (Array.isArray(_colors)) this.categoryColorRange = _colors.map(opt => opt);

        if (!err) runInAction(() => {
            this.threadUnread = observable.map(vm.threadUnreads.map(x => [x.threadId, x.unread]));
        });
        return err;
    }

    @action beginCreate = () => {
        this.item = new Discussion({
            classId: this.classId,
        });
    }
}

export class ActivityDiscussionListStore extends DiscussionListStore {
    @observable         activityId: DbIdentity = DefaultId;

    @computed get displayItems() {
        return (this.activityId < 1 ? [] :
            this.sDiscussion.mActivityId2Items.get(this.activityId)) || []
    }

    @computed get activity() {
        return this.activityId < 1 ? undefined : this.store.sActivity.mId2Item.get(this.activityId)
    }
    @computed get aClass() {
        return this.activity ? this.activity.class : undefined;
    }

    @action reset() {
        this.activityId = DefaultId;
        this.item = undefined;
    }

    @action async init({activityId}: {activityId:DbIdentity}) {
        this.reset();
        this.activityId = activityId;

        const pA = this.store.fetchActivity(activityId);
        const pD = this.store.fetchActivityDiscussions(activityId);

        const [aErr] = await pA;
        const [dErr, vm] = await pD;

        const [cErr, _colors] = await Category.getSchoolDefaultColors(this.store.currentSchoolId);
        if (cErr) return cErr;
        if (Array.isArray(_colors)) this.categoryColorRange = _colors.map(opt => opt);

        if (!dErr) runInAction(() => {
            this.threadUnread = observable.map(vm.threadUnreads.map(x => [x.threadId, x.unread]));
        });
        return aErr || dErr;
    }

    @action beginCreate = () => {
        this.item = new Discussion({
            activityId: this.activityId,
            classId: this.activity ? this.activity.classId : undefined,
        });
    }
}
