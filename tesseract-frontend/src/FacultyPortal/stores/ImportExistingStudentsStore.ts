import { observable, action, runInAction, computed, reaction } from "mobx";

import { DefaultId, DbIdentity } from "../../models/types";
import { Student } from "../../models/Student";
import { Class } from "../../models/Class";

import { Store } from "./Store";

import { IErrorData } from "../../services/api/AppError";
import { aFetch } from "../../services/api/fetch";

export const pageSize = 10;

export interface StudentItem {
    key: string;
    student:Student,
    classes_?:Class[],
    classId:DbIdentity,
}

export class ImportExistingStudentsStore {
    constructor(private store:Store) {
        reaction(() => this.store.currentGradingTerm, (t) => {
            if (this.store.currentGradingTermClasses.some(c => c.classId == this.selectedClassId)) return;
            this.set_classId(DefaultId);
        })
    }

    @observable         schoolId              : DbIdentity                           = DefaultId;
    @observable         facultyId             : DbIdentity                           = DefaultId;
    @observable         classId               : DbIdentity                           = DefaultId;
    @observable.ref     selectedClassId       : DbIdentity                           = DefaultId;
    @observable.ref     aClass             ?  : Class                                = undefined;
    @observable.shallow students              : Student[]                            = [];
    @observable         totalStudents         : number                               = 0;
    @observable         selectedRowKeys       : string[]                             = [];
    @observable         isOpenStudentListModal: boolean                              = false;

    @observable         searchText            : string                               = "";
    @observable         pageIndex             : number                               = 0;
    @observable         sortColumnKey         : string|undefined                     = undefined;
    @observable         sortOrder             : boolean|"ascend"|"descend"|undefined = undefined;
    @observable         isSelectAll           : boolean                              = false;

    @action async init({classId}:{classId: DbIdentity}) {
        this.set_selectedClassId(DefaultId);
        this.set_selectedRowKeys([]);
        this.classId = classId;
        this.loading = false;

        this.set_isSelectAll(false);
        this.set_sortColumnKey(undefined);
        this.set_sortOrder(undefined);
        this.set_pageIndex(0);
        this.set_searchText("");

        if(this.schoolId < 1){
            this.schoolId = this.store.currentSchoolId;
        }

        if(this.facultyId < 1){
            this.facultyId = this.store.currentFacultyId;
        }

        const [cErr, _class] = await Class.fetchClassAsFaculty({schoolId:this.store.currentSchoolId, facultyId:this.store.currentFacultyId, classId});
        if (cErr) return cErr;
        runInAction(() => {
            this.aClass = _class;
            this.store.set_currentGradingTerm(this.aClass.gradingTerm, true);
        });

        return await this.fetchStudent();
    }

    @action async fetchStudent(){
        const [err, students] = await this.store.fetchStudentOfSchoolPagination(this.pageIndex
                                        , pageSize
                                        , this.classId
                                        , this.selectedClassId
                                        , this.searchText
                                        , this.sortColumnKey
                                        , this.sortOrder
                                        );
        const [errTotal, total] = await this.store.fetchStudentOfSchoolTotalPagination(this.classId
                                        , this.selectedClassId
                                        , this.searchText);
        if (!err && !errTotal) runInAction(() => {
            this.students = students;
            this.set_totalStudent(total);
            if(this.isSelectAll) {
                this.set_isSelectAll(true);
            }
        });
        else return err;

        return;
    }

    @computed get filterStudents(): StudentItem[] {
        const filter = this.selectedClassId < 0
            ? this.students.filter(s => !s.classes.includes(this.classId))
                .map(s => ({key: `${s.classes.join(".")}.${s.studentId}`, student:s, classId: DefaultId, classes_: this.store.tClass.filter(c => s.classes.includes(c.classId))}))
            : this.students.filter(s => !s.classes.includes(this.classId))
                .filter(s => s.classes.includes(this.selectedClassId))
                .map(s => ({key: `${s.classes.join(".")}.${s.studentId}`,student:s, classId:this.selectedClassId, classes_:this.store.tClass.filter(c => s.classes.includes(c.classId))}))
        // console.log(this.store.tClass);
        return filter;
    }
    @computed get nSelectedStudents(){
        return this.isSelectAll ? this.totalStudents : this.selectedRowKeys.length;
    }

    @observable loading = false;
    @action async save() {
        if(this.isSelectAll){
            const [err] = await aFetch<{}>("POST", `/faculty/${this.facultyId}/school/${this.schoolId}/class/${this.classId}/student/importAllToClass`, {
                filteredClassId: this.selectedClassId,
                searchText: this.searchText
            });
            if (err) return err;
        }
        if (this.selectedRowKeys.length < 1) return;
        this.loading = true;

        let err: IErrorData<{}>|undefined;
        if (this.selectedRowKeys.length!=0) {
            const selectStudentIds = this.selectedRowKeys.map(s => s.split(/[. ]+/).pop());
            const [err] = await aFetch<{}>("POST", `/faculty/${this.facultyId}/school/${this.schoolId}/class/${this.classId}/student/importToClass`,selectStudentIds);
            if (err) return err;
            runInAction(() => {
                this.set_selectedRowKeys([]);
            });
        }
        runInAction(() => {
            this.loading = false;
        });

        this.set_isOpenStudentListModal(false);
        return err;
    }
    @action set_isSelectAll = (v: boolean) => {
        this.isSelectAll = v;
        if(v) {
            this.set_selectedRowKeys(this.filterStudents.map(s=>s.key));
        }
    }
    @action set_sortColumnKey   = (v: string|undefined)                     => { this.sortColumnKey   = v; }
    @action set_sortOrder       = (v: boolean|"ascend"|"descend"|undefined) => { this.sortOrder       = v; }
    @action set_searchText      = (v: string)                               => { this.searchText      = v; }
    @action set_pageIndex       = (v: number)                               => { this.pageIndex       = v; }
    @action set_totalStudent    = (v: number)                               => { this.totalStudents   = v; }
    @action set_classId         = (v: DbIdentity)                           => { this.classId         = v; }
    @action set_selectedClassId = (v: DbIdentity)                           => { this.selectedClassId = v; }

    @action set_isOpenStudentListModal = (v: boolean) => {
        this.isOpenStudentListModal = v;
    }

    @action set_selectedRowKeys = (v: string[]) => {
        this.selectedRowKeys = v;
    }
}
