import { observable, runInAction, computed } from "mobx";

import {DefaultId, DbIdentity} from "../../models/types";
import { Student } from "../../models/Student";
import { Activity } from "../../models/Activity";
import { ActivityScore } from "../../models/ActivityScore";
import { fromGeneralViewModel } from "../models/FacultyClass";

import { Store } from "./Store";

export class StudentDetailStore {
    constructor(private store:Store) {
    }

    @observable.ref student ?:Student;
    @observable classId:DbIdentity = DefaultId;
    @observable studentId:DbIdentity = DefaultId;

    async init(studentId:DbIdentity, classId:DbIdentity) {
        this.classId = classId = !Number.isNaN(classId) ? classId : DefaultId;
        this.studentId = studentId;

        const [err, student] = await this.store.fetchStudentDetail({studentId, classId: (classId < 1 ? undefined : classId)});
        if (this.studentId != studentId || this.classId != classId) return;

        if (!err) runInAction(() => {
            this.student = student;
            if (classId > 0) {
                const c = this.store.mClass.get(this.classId);
                if (c != null) this.store.set_currentGradingTerm(c.gradingTerm, true);
            }
        });

        return err;
    }

    openCommentDialog = (activity:Activity) => {
        if (this.student == null) return;
        const {student} = this;
        const score = student.mActivitiScore.get(activity.activityId) || new ActivityScore({
            studentId : student.studentId,
            activityId: activity.activityId,
        });
        this.store.sActivityModal.open(activity, score).then((shouldRefresh) => {
            if (!shouldRefresh) return;
            this.init(student.studentId, activity.classId);
        });
    }

    @computed get missedActivities() {
        return this.student ? this.student.calculateMissingAssigment(this.store.sActivity.items).length : 0
    }
}
