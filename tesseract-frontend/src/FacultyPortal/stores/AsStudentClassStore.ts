import { observable, action, computed, runInAction } from "mobx";
import { groupBy, map, uniqBy } from "lodash-es";

import { AsStudentClass, sortClass } from "../models/AsStudentClass";

import { Store } from "./Store";

export class AsStudentClassStore {
    constructor(private store: Store) {
        console.assert(store != null);
    }

    @observable.shallow items:AsStudentClass[] = [];
    @computed get mId2Item() { return observable.map(this.items.map(c => [c.classId, c])) }
    @computed get mSchoolId2Items() {
        return observable.map(
            map(groupBy(this.items, c => c.schoolId), xs => [xs[0].schoolId, xs])
        );
    }

    @action storeItems(xs:AsStudentClass[]) {
        this.items = uniqBy(xs.concat(this.items), c => c.classId);
    }

    @computed get currentGradingTermClasses(): AsStudentClass[] {
        return (this.items
            .filter(c =>
                (!this.store.currentGradingTerm || c.gradingTerm == this.store.currentGradingTerm) &&
                (c.schoolId == this.store.currentSchoolId))
            .sort(sortClass)
        );
    }

    async fetchItems() {
        const [err, cs] = await AsStudentClass.fetchItems({ facultyId: this.store.currentFacultyId, schoolId:this.store.currentSchoolId, gradingTermId:this.store.currentGradingTerm });
        if (!err) runInAction(() => {
            this.storeItems(cs);
        });
        return [err, cs] as const;
    }
}
