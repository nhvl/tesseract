import { observable, action, runInAction, computed } from "mobx";

import {DefaultId, DbIdentity} from "../../models/types";
import { Discussion } from "../../models/Discussion";
import { Comment } from "../../models/Comment";
import { CommentSeen } from "../../models/CommentSeen";

import { Store } from "./Store";
import { SetSeenDuration } from "../../config";

export class DiscussionThreadStore {
    constructor(private store:Store) {

    }

    @observable classId          : DbIdentity = DefaultId;
    @observable discussionId     : DbIdentity = DefaultId;
    @observable.ref discussion  ?: Discussion;
    @observable.ref commentSeen ?: CommentSeen;

    @computed get aClass(){ return this.store.mClass.get(this.classId) }
    @computed get activity(){ return (this.discussion ? this.store.sActivity.mId2Item.get(this.discussion.activityId) : undefined) }

    @action reset(o?:{classId:DbIdentity, discussionId:DbIdentity}) {
        if (o != null) {
            const {classId, discussionId} = o;
            if (this.classId != classId || this.discussionId != discussionId) return;
        }
        this.classId = DefaultId;
        this.discussionId = DefaultId;
        this.discussion = undefined;
        this.commentSeen = undefined;
    }

    @action async init({classId, discussionId}: {classId:DbIdentity, discussionId:DbIdentity}) {
        this.classId = classId;
        this.discussionId = discussionId;
        this.discussion = undefined;

        const err = await this.fetchDiscussion();
        if (!err) setTimeout(() => {
            if (this.discussionId != discussionId) return;
            const {discussion} = this; if (discussion == null) return;
            this.store.setSeen(discussion.threadId);
        }, SetSeenDuration);

        return err;
    }

    @action async fetchDiscussion() {
        const {classId, discussionId} = this;
        if (classId < 1 || discussionId < 1) return;
        const [err, x] = await this.store.fetchDiscussion({classId, discussionId});
        if (this.discussionId != discussionId) return;
        if (err) return err;
        if (x.discussionId != this.discussionId) return;
        runInAction(() => {
            this.discussion = x;
        });

        this.store.fetchActivity(x.activityId);

        return await this.fetchComments();
    }

    async fetchComments() {
        const {discussion} = this; if (discussion == null) return;

        const [err, vm] = await this.store.fetchCommentsOfThread(discussion.threadId);
        if (err) return err;
        this.commentSeen = vm.commentSeens[0];
        return;
    }
}
