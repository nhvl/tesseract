import React, { ReactNode } from 'react';
import { RouterState, Route, StringMap, TransitionHook } from 'mobx-state-router';

import {mapValues, toPairs} from "lodash-es";

import { Store } from './stores/Store';

export const homeRoute = new RouterState("home");
export const notFound = new RouterState('notFound');

const checkForUserSignedIn: TransitionHook = async (fromState, toState, routerStore) => {
    const store:Store = routerStore.rootStore;
    const isLogin = await store.checkLogin();
    if (!isLogin) {
        location.reload();
    }
};

import { HomePage              } from "./pages/home/HomePage";
import { ClassList             } from "./pages/classes/ClassList";
import { ClassEditorPage       } from "./pages/classes/ClassEditorPage";
import { ClassDetailPage       } from "./pages/classes/ClassDetailPage";
import { StudentListPage       } from "./pages/students/StudentListPage";
import { EditStudentPage       } from "./pages/editStudent/EditStudentPage";
import { StudentDetailPage     } from "./pages/students/StudentDetailPage";
import { StudentSubmissionPage } from "./pages/students/StudentSubmission";
import { QuickGraderPage       } from "./pages/students/QuickGrader";
import { GradeDetailPage       } from "./pages/activity/GradeDetailPage";
import { EditActDocPage        } from "./pages/presentation/EditActDocPage";
import { ViewActDocPage        } from "./pages/presentation/ViewActDocPage";
import { DiscussionPage        } from "./pages/discussion/DiscussionPage";
import { ActivityDiscussionPage } from './pages/discussion/ActivityDiscussionList';
import { AccountSettingsPage   } from "./pages/Account/Settings/AccountSettingsPage";
import { ActivityListPage      } from "./pages/home/ActivityListPage";
import { ActivitySettings      } from "./pages/activity/ActivitySettings";
import { PowerSearchDetail     } from "./pages/home/PowerSearchDetail";
import { ToolsPage             } from "./pages/tools/ToolsPage";
import { Exception403          } from "./pages/Exception/403";
import { Exception404          } from "./pages/Exception/404";
import { Exception500          } from "./pages/Exception/500";
import { ClassTemplatePage } from './pages/classes/ClassTemplatePage';

export const routeConfig: {[key:string]: {pattern:string, comp:ReactNode, allowAnonymous?:boolean}} = {
    notFound            : ({pattern:"/404"                                       , comp: (<Exception404          />), allowAnonymous:true }),
    403                 : ({pattern:"/403"                                       , comp: (<Exception403          />), allowAnonymous:true }),
    500                 : ({pattern:"/500"                                       , comp: (<Exception500          />), allowAnonymous:true }),
    home                : ({pattern:"/"                                          , comp: (<HomePage              />), }),
    openAssignments     : ({pattern:"/open-assignments"                          , comp: (<ActivityListPage      />), }),
    pastAssignments     : ({pattern:"/past-assignments"                          , comp: (<ActivityListPage      />), }),
    classes             : ({pattern:"/classes"                                   , comp: (<ClassList             />), }),
    classTemplates      : ({pattern:"/class-templates"                           , comp: (<ClassTemplatePage     />), }),
    classEditor         : ({pattern:"/classes/:classId/edit"                     , comp: (<ClassEditorPage       />), }),
    classDetail         : ({pattern:"/classes/:classId"                          , comp: (<ClassDetailPage       />), }),
    classActivities     : ({pattern:"/classes/:classId/activities"               , comp: (<ClassDetailPage       />), }),
    classAssignments    : ({pattern:"/classes/:classId/assignments"              , comp: (<ClassDetailPage       />), }),
    classAssessments    : ({pattern:"/classes/:classId/assessments"              , comp: (<ClassDetailPage       />), }),
    classGradebook      : ({pattern:"/classes/:classId/gradebook"                , comp: (<ClassDetailPage       />), }),
    classStudents       : ({pattern:"/classes/:classId/students"                 , comp: (<ClassDetailPage       />), }),
    createStudent       : ({pattern:"/classes/:classId/students/new"             , comp: (<EditStudentPage       />), }),
    editStudent         : ({pattern:"/classes/:classId/students/:studentId/edit" , comp: (<EditStudentPage       />), }),
    classTools          : ({pattern:"/classes/:classId/tools"                    , comp: (<ClassDetailPage       />), }),
    students            : ({pattern:"/students"                                  , comp: (<StudentListPage       />), }),
    studentDetail       : ({pattern:"/students/:studentId"                       , comp: (<StudentDetailPage     />), }),
    studentDetailInClass: ({pattern:"/classes/:classId/students/:studentId"      , comp: (<StudentDetailPage     />), }),
    studentSubmission   : ({pattern:"/students/:studentId/activities/:activityId", comp: (<StudentSubmissionPage />), }),
    quickGrader         : ({pattern:"/quickGrader/students/:studentId/activities/:activityId", comp: (<QuickGraderPage />), }),
    discussionList      : ({pattern:"/classes/:classId/discussion"               , comp: (<ClassDetailPage       />), }),
    activityDiscussion  : ({pattern:"/activities/:activityId/discussion"         , comp: (<ActivityDiscussionPage/>), }),
    discussion          : ({pattern:"/classes/:classId/discussion/:discussionId" , comp: (<DiscussionPage        />), }),
    accountSettings     : ({pattern:"/account/settings"                          , comp: (<AccountSettingsPage   />), }),
    newActivitySettings : ({pattern:"/classes/:classId/new-activity"             , comp: (<ActivitySettings      />), }),
    activitySettings    : ({pattern:"/activities/:activityId"                    , comp: (<ActivitySettings      />), }),
    editActDoc          : ({pattern:"/activities/:activityId/editor"             , comp: (<EditActDocPage        />), }),
    viewActDoc          : ({pattern:"/activities/:activityId/present"            , comp: (<ViewActDocPage        />), }),
    gradeDetail         : ({pattern:"/activities/:activityId/grade"              , comp: (<GradeDetailPage       />), }),
    powerSearchDetail   : ({pattern:"/powerSearch"                               , comp: (<PowerSearchDetail     />), }),
    tools               : ({pattern:"/tools"                                     , comp: (<ToolsPage             />), }),
};

export const appViewMap = mapValues(routeConfig, c => c.comp);
export const routes = toPairs(routeConfig).map<Route>(([name, c]) => ({
    name, pattern:c.pattern,
    beforeEnter: !!c.allowAnonymous ? undefined :checkForUserSignedIn
}));

function safeFromState(state: RouterState) {
    return state.routeName === "__initial__" ? state : homeRoute
}

function redirect(routeName:string, params?: StringMap, queryParams?: Object) {
    return () => Promise.reject(new RouterState(routeName, params, queryParams))
}

