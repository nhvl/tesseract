import {observable, action} from "mobx";

import {DbIdentity} from "../../models/types";
import { Class } from "../../models/Class";
import { aFetch } from "../../services/api/fetch";
import { GeneralDto, IGeneralViewModel, parseGeneralViewModel } from "../../models/GeneralViewModel";

export class FacultyClass extends Class {
    @observable sortIndex?: number;
    @action set_sortIndex = (v: number) => { this.sortIndex = v }

    constructor(data?:{}) {
        super(data);
    }

    toJS() {
        const data = super.toJS();
        return ({...data, sortIndex: this.sortIndex});
    }

    static async fetchItems({schoolId, facultyId, gradingTermId}:{schoolId:DbIdentity, facultyId:DbIdentity, gradingTermId?:DbIdentity}) {
        const [err, data] = await aFetch<GeneralDto>("GET",
            (gradingTermId == null || gradingTermId < 1)
            ? `/faculty/${facultyId}/school/${schoolId}/class`
            : `/faculty/${facultyId}/school/${schoolId}/gradingTerm/${gradingTermId}/class`
        );
        const vm = err ? undefined : parseGeneralViewModel(data);
        const cs:FacultyClass[] = (vm == null ? [] : fromGeneralViewModel(vm));
        return [err, cs] as [typeof err, typeof cs];
    }

    static async batchUpdate(schoolId:DbIdentity, facultyId:DbIdentity, xs: FacultyClass[]) {
        const [err, data] = await aFetch<GeneralDto>("PUT", `/faculty/${facultyId}/school/${schoolId}/class`, xs.map(x => x.toJS()));
        const cs = (err ? undefined : fromGeneralViewModel(parseGeneralViewModel(data)))!;
        return [err, cs] as [typeof err, typeof cs];
    }
}

export function fromGeneralViewModel(vm: IGeneralViewModel) {
    const cs = vm.classes.map(c => new FacultyClass(c));
    vm.classFaculties.forEach(cf => {
        const c = cs.find(c => c.classId == cf.classId);
        if (c == null) return;
        c.sortIndex = cf.sortIndex;
    });
    return cs;
}

export function sortClass(a:FacultyClass, b:FacultyClass) {
    const d = (a.sortIndex || Number.MAX_SAFE_INTEGER) - (b.sortIndex || Number.MAX_SAFE_INTEGER);
    if (d != 0) return d;
    return Class.sorter.period(a, b);
}
