import {observable, action} from "mobx";

import {DbIdentity} from "../../models/types";
import { Class } from "../../models/Class";

import { aFetch } from "../../services/api/fetch";
import { GeneralDto, IGeneralViewModel, parseGeneralViewModel } from "../../models/GeneralViewModel";

export class AsStudentClass extends Class {
    cumulativeGrade ?:number = undefined;

    @observable sortIndex?: number;
    @action set_sortIndex = (v: number) => { this.sortIndex = v }

    constructor(data?:{}) {
        super(data);
    }

    toJS() {
        const data = super.toJS();
        return ({...data, sortIndex: this.sortIndex, cumulativeGrade:this.cumulativeGrade});
    }

    static async fetchItems({schoolId, facultyId, gradingTermId}:{schoolId:DbIdentity, facultyId:DbIdentity, gradingTermId?:DbIdentity}) {
        const [err, data] = await aFetch<GeneralDto>("GET",
            (gradingTermId == null || gradingTermId < 1)
            ? `/faculty/${facultyId}/school/${schoolId}/asStudentClass`
            : `/faculty/${facultyId}/school/${schoolId}/gradingTerm/${gradingTermId}/asStudentClass`
        );
        const vm = err ? undefined : parseGeneralViewModel(data);
        const cs:AsStudentClass[] = (vm == null ? [] : fromGeneralViewModel(vm));
        return [err, cs] as [typeof err, typeof cs];
    }
}

export function fromGeneralViewModel(vm: IGeneralViewModel) {
    const classes = vm.classes.map(c => new AsStudentClass(c));
    vm.classStudents.forEach(cs => {
        const c = classes.find(c => c.classId == cs.classId);
        if (c == null) return;
        c.sortIndex = cs.sortIndex;
        c.cumulativeGrade = cs.cumulativeGrade;
    });
    return classes;
}

export function sortClass(a:AsStudentClass, b:AsStudentClass) {
    const d = (a.sortIndex || Number.MAX_SAFE_INTEGER) - (b.sortIndex || Number.MAX_SAFE_INTEGER);
    if (d != 0) return d;
    return Class.sorter.period(a, b);
}
