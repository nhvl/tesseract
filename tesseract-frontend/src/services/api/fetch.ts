import {qs} from "../../utils/url";
import {IErrorData, createError} from "./AppError";

export const apiHost = new URL(process.env.REACT_APP_API!, location.href ).toJSON();
const versionPrefix = "Tesseract/v1/";
const headers: {[key:string]:string} = {
    'Content-Type': 'application/json',
    "Accept"      : "application/json",
}
export function setAuthtoken(token:string){
    if (!token) delete headers.Authorization;
    else headers.Authorization = `Bearer ${token}`;
}

export async function aFetch<T, TError = any>(
    method:"GET"|"POST"|"PUT"|"DELETE",
    input: string,
    body ?: {},
    init?: RequestInit,
) : Promise<[(IErrorData<TError>)|undefined, T]> {
    try {
        const apiService = (`${versionPrefix}${input}`).replace(/([^:]\/)\/+/g, "$1");
        const url = (method != "GET" || !body) ? new URL(apiService, apiHost) : qs(new URL(apiService, apiHost), body);
        const response = await fetch(url.toJSON(), {
            method,
            headers: headers,
            mode:"cors",
            ...((method != "GET" && body) ? {body: JSON.stringify(body)} : {}),
            ...init
        });
        if (response.ok) {
            try {
                const {data} = await response.json();
                return [undefined, data as T];
            } catch(e) {
                console.warn(e);
                return [undefined, undefined! as T]
            }
        }

        const err = createError(new Error(response.statusText || String(response.status)), response.status);
        try {
            const e = await response.json();
            return [Object.assign(err, e), undefined! as any];
        } catch {
            return [err, undefined! as any];
        }
    } catch (err) {
        return [err, undefined! as any];
    }
}

export async function uploadFile<T, TError = any>(
    method:"POST"|"PUT"|"DELETE",
    input: string,
    file: Blob,
    init?: RequestInit,
) : Promise<[(IErrorData<TError>)|undefined, T]> {
    if (file == null) return [createError(new Error("File is null"), 400), undefined!];

    const data = new FormData();
    data.append("files", file);
    const { "Content-Type":_, ...hs }  = headers;

    try {
        const apiService = (`${versionPrefix}${input}`).replace(/([^:]\/)\/+/g, "$1");
        const url = new URL(apiService, apiHost);
        const response = await fetch(url.toJSON(), {
            method,
            headers: hs,
            mode:"cors",
            body:data,
            ...init
        });
        if (response.ok) {
            try {
                const {data} = await response.json();
                return [undefined, data as T];
            } catch(e) {
                console.warn(e);
                return [undefined, undefined! as T]
            }
        }

        const err = createError(new Error(response.statusText || String(response.status)), response.status);

        try {
            const e = await response.json();
            return [Object.assign(err, e), undefined! as any];
        } catch {
            return [err, undefined! as any];
        }
    } catch (err) {
        return [err, undefined! as any];
    }
}
