export interface IErrorData<T> extends Error {
    status:number
    error?:{
        statusCode  : number;
        message     : string;
        errorCode   : string;
        errorObject?: T;
    }
}

export function createError<T = any>(error:Error, status:number): IErrorData<T> {
    return Object.assign(error, {status});
}

export enum ErrorCode {
    EmailNotConfirm            = "EmailNotConfirm",
    UserIsLockedOut            = "UserIsLockedOut",
    UserIsNotAllowed           = "UserIsNotAllowed",
    LoginRequiresTwoFactor     = "LoginRequiresTwoFactor",
    LoginInvalid               = "LoginInvalid",

    StudentNotFound            = "StudentNotFound",
    StudentNumberNotFound      = "StudentNumberNotFound",
    StudentPhoneNumberNotFound = "StudentPhoneNumberNotFound",

    SchoolIsRequired           = "SchoolIsRequired",
    DuplicateUserName          = "DuplicateUserName",
    DuplicateEmail             = "DuplicateEmail",
}
