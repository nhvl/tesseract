import { IErrorData } from "./AppError";
import React, { ReactNode } from "react";
import i18next from "i18next";
import { Modal } from "../../components/Modal/ModalAntd";

interface ISerializableError {
  [fieldName: string]: string
}
interface IIdentiyError {
  code: string,
  description: string
}

interface IGoogleApiError {
  reason: string,
  message: string
}


interface ISerializableErrHandler {
  (message: string, error: ISerializableError): ReactNode | undefined;
}

interface IIdentityResultHandler {
  (description: string, identityErrors: IIdentiyError[]): ReactNode | undefined;
}

interface IShowErrorOptions {
  serializableCase?: {
    [field: string]: ReactNode | ISerializableErrHandler
  },
  identityCase?: {
    [code: string]: ReactNode | IIdentityResultHandler
  },
  tOptions?: i18next.TOptions,
  tNameSpace?: string,
}

export function showError(err: IErrorData<unknown>, t: i18next.TFunction, options?: IShowErrorOptions) {
  const tNameSpace = (options && options.tNameSpace) || "api.error";
  const tOptions = (options && options.tOptions) || undefined;

  let content: ReactNode = null;
  if (!err.error) {
    content = err.message;
  } else {
    switch (err.error.errorCode) {
      case "StudentNumberNotFound":
        content = t("api.student.error.StudentNumberNotFound");
        break;
      case "StudentNotFound":
        content = t('api.student.error.StudentNotFound');
        break;
      case "StudentPhoneNumberNotFound":
        content = t('api.student.error.StudentPhoneNumberNotFound');
        break;
      case "LoginInvalid":
        content = t("api.error.LoginInvalid", tOptions);
        break;
      case "Google.GoogleApiException":
        const googleApiErrors = err.error.errorObject as IGoogleApiError[];
        content = (<div>{
          googleApiErrors.map(({ reason, message }, i) => {
            if (options && options.identityCase) {
              const h = options.identityCase[reason];
              if (typeof h === "function") {
                const v = h(message, identityErrors);
                if (v != null) return v;
              } else if (h != null) return h;
            }
            return (<div key={i} title={reason}>{message}</div>);
          })
        }</div>)
        break;
      case "Microsoft.AspNetCore.Mvc.SerializableError":
        const errorObject = err.error.errorObject as ISerializableError;
        content = Object.keys(errorObject).map(key => {
          const message = errorObject[key];
          const handler = (options && options.serializableCase && options.serializableCase[key]) || undefined;
          if (handler != null) {
            if (typeof handler == "function") {
              const x = handler(message, errorObject);
              if (x != null) return x;
            } else if (typeof handler != undefined) {
              return handler;
            }
          }
          return message;
        }).filter(Boolean).map((v, i) => (<div key={i}>{v}</div>));
        break;
      case "Microsoft.AspNetCore.Identity.IdentityResult":
        const identityErrors = err.error.errorObject as IIdentiyError[];
        content = (<div>{
          identityErrors.map(({ code, description }, i) => {
            if (options && options.identityCase) {
              const h = options.identityCase[code];
              if (typeof h === "function") {
                const v = h(description, identityErrors);
                if (v != null) return v;
              } else if (h != null) return h;
            }
            return (<div key={i} title={code}>{
              t(`${tNameSpace}.${code}`, {
                ...(tOptions),
                defaultValue: description || t("api.error.unspecific"),
              })
            }</div>);
          })
        }</div>)
        break;
      default:
        content = t(err.error.message || err.message);
    }
  }

  Modal.error({ content: content });
}



