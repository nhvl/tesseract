const themekey = "TessereactTheme";

export const theme = localStorage.getItem(themekey) || "light";
loadTheme(theme);

async function loadTheme(theme:string) {
    console.log("loadTheme", theme);
    switch (theme) {
        case "dark":
            import("./css/index-dark.less");
            return;
        case "light": break;
        default: console.warn("unsupport theme", theme);
    }
    import("./css/index-light.less");
}

export async function switchTheme(theme:string) {
    localStorage.setItem(themekey, theme);
    location.reload();
}
