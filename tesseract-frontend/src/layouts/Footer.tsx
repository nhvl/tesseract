import React from 'react';
import { observer } from 'mobx-react';

import { Layout, Icon } from 'antd';
import { GlobalFooter } from '../components/GlobalFooter';
const { Footer } = Layout;

export const FooterView = observer(() => {
    return (
        <Footer style={{ padding: 0 }}>
            <GlobalFooter
                copyright={(<>
                    Copyright <Icon type="copyright" /> 2019 Tesseract
                </>)}
            />
        </Footer>
    );
});
