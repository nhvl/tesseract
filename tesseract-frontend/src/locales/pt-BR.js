import analysis from './pt-BR/analysis';
import activities from './pt-BR/activities';
import exception from './pt-BR/exception';
import form from './pt-BR/form';
import globalHeader from './pt-BR/globalHeader';
import login from './pt-BR/login';
import classes from './pt-BR/classes';
import menu from './pt-BR/menu';
import monitor from './pt-BR/monitor';
import result from './pt-BR/result';
import settingDrawer from './pt-BR/settingDrawer';
import settings from './pt-BR/settings';
import pwa from './pt-BR/pwa';
import component from './pt-BR/component';
import dashboard from './pt-BR/dashboard';
import api from './en-US/api';

export default {
  'navBar.lang': 'Idiomas',
  'layout.user.link.help': 'ajuda',
  'layout.user.link.privacy': 'política de privacidade',
  'layout.user.link.terms': 'termos de serviços',
  'app.home.welcome': 'bem vinda',
  'app.home.introduce': 'introduzir',
  'app.forms.basic.title': 'Basic form',
  'app.forms.basic.description':
    'Páginas de formulário são usadas para coletar e verificar as informações dos usuários e formulários básicos são comuns nos cenários onde existem alguns formatos de informações.',
  ...analysis,
  ...activities,
  ...exception,
  ...form,
  ...globalHeader,
  ...login,
  ...classes,
  ...menu,
  ...monitor,
  ...result,
  ...settingDrawer,
  ...settings,
  ...pwa,
  ...component,
  ...dashboard,
  ...api,
};
