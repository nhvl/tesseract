export default {
  'api.error.LoginInvalid': 'Usuario o contraseña invalido. Por favor, compruebe su nombre de usuario y contraseña y vuelva a intentarlo.',

  'api.login.error.EmailNotConfirm': 'El correo electrónico no está confirmado. Por favor revise el correo electrónico para confirmación e intente iniciar sesión nuevamente.',
  'api.login.error.UserIsLockedOut': 'Su cuenta está bloqueada. Póngase en contacto con su administrador.',
  'api.login.error.UserIsNotAllowed': 'Este usuario ahora tiene permitido iniciar sesión.',
  'api.login.error.LoginRequiresTwoFactor': 'Se requiere autenticación de dos factores.',
  'api.login.error.LoginInvalid': 'Usuario o contraseña invalido. Por favor, compruebe su nombre de usuario y contraseña y vuelva a intentarlo.', 
  'api.student.error.StudentNotFound': 'El estudiante no fue encontrado.',
  'api.student.error.StudentNumberNotFound': 'El número de estudiante no fue encontrado.',
  'api.student.error.StudentPhoneNumberNotFound': 'El número de teléfono del estudiante no fue encontrado.',
};







