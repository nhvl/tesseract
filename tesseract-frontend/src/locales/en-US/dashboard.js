export default {
  'dashboard.welcome': 'Welcome',
  'dashboard.welcomeAndCreateClass': 'Welcome to <0><0></0></0> ',
  'dashboard.getAction': 'Let\'s get started by creating a class! ',
  'dashboard.getActionModify': 'Start creating a class! ',
  'dashboard.getStartedNow': 'Get Started Now ',
  'dashboard.clickHereToAddClass': 'Add a Class',
  'dashboard.haveActivities': 'You have new assignments to grade.',
  'dashboard.noActivities': 'You have no new assignments to grade right now.',
  'dashboard.orderby': 'Order by',
  'dashboard.orderby.ascending': 'Ascending',
  'dashboard.orderby.descending': 'Descending'
}
