export default {
    'app.faculty'                               : 'Faculty',

    'app.faculty.edit.addFaculty'               : 'Add Faculty',
    'app.faculty.edit.createTitle'              : 'Create Faculty',
    'app.faculty.edit.editTitle'                : 'Edit Faculty',
    'app.faculty.edit.schoolName'               : 'School Name',
    'app.faculty.edit.schoolName.error.selectAtLeast1'  : 'Please select at least 1 school',
    'app.faculty.edit.firstName'                : 'First Name',
    'app.faculty.edit.lastName'                 : 'Last Name',
    'app.faculty.edit.email'                    : 'Email',
    'app.faculty.edit.phoneNumber'              : 'Phone Number',
    'app.faculty.edit.externalID'               : 'External ID',
    'app.faculty.edit.cancel'                   : 'Cancel',
    'app.faculty.edit.save'                     : 'Save',
    'app.faculty.edit.roles'                    : 'Additional Roles'    
}