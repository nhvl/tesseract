export default {
    'app.parent.menu.mystudent': 'My Students',
    'app.parent.students.table.col.student' : 'Student',
    'app.parent.students.table.col.id-number' : 'Id Number',
    'app.parent.students.table.col.next-assignment' : 'Next Assignment Due',
    'app.parent.students.table.col.open-assignments' : 'Open Assignments ',
    'app.parent.my-student': 'My Students',
    'app.parent.assigments.table.col.class': 'Class',
    'app.parent.assigments.table.col.teacher': 'Teacher',
    'app.parent.assigments.table.col.assignment': 'Assignment',
    'app.parent.assigments.table.col.year': 'Year',
    'app.parent.completed-assigments': 'Completed Assignments',
    'app.parent.assigments.table.cell.view-details': 'View Details',
    'app.parent.assigments.table.col.due': 'Due Date',
    'app.parent.open-assigments': 'Open Assignments',
    'app.parent.print': 'Print',
    'app.parent.download': 'DownLoad',
};

