export default{
    'app.thread.comment.getCommentsFail': 'Get comments FAIL',
    'app.thread.comment.replyto': 'Reply to',
    'app.thread.comment.cancel': 'Cancel',
    'app.thread.comment.add': 'Add Comment',
}