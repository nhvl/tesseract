export default {
  'component.tagSelect.expand': 'Expand',
  'component.tagSelect.collapse': 'Collapse',
  'component.tagSelect.all': 'All',
  'component.updateActiveSchool.Error':'Error when update active school',

  'component.fiveCChart.Collaboration' : 'Collaboration',
  'component.fiveCChart.Communication' : 'Communication',
  'component.fiveCChart.Creativity' : 'Creativity',
  'component.fiveCChart.Character' : 'Character',
  'component.fiveCChart.CriticalThinking' : 'Critical Thinking',

  'component.modal.info': 'Info',
  'component.modal.error': 'Error',
  'component.modal.error.systemError': 'System Error',
  'component.modal.success': 'Success',
  'component.modal.warning': 'Warning',
  'component.modal.confirm': 'Confirmation',
  'component.modal.confirm.delete': 'Delete Confirmation',
};
