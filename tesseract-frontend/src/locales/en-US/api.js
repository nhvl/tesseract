export default {
  "api.error.LoginInvalid": 'Invalid username or password. Please check your login and password and try again.',
  'app.error.DuplicateUserName': "User name {{email}} is already taken.",
  'app.error.DuplicateEmail': "Email {{email}} is already taken.",
  'app.error.PasswordRequiresNonAlphanumeric': 'Password Requires Non Alphanumeric',
  'app.error.PasswordRequiresLower': 'Password Requires Lower',
  'app.error.PasswordRequiresUpper': 'Password Requires Upper',
  'app.error.unspecific': 'Something went wrong.',

  'api.login.error.EmailNotConfirm': 'Email is not confirm. Please check email for confirmation and try to login again.',
  'api.login.error.UserIsLockedOut': 'Your account is locked out. Please contact to your Administrator.',
  'api.login.error.UserIsNotAllowed': 'This user is now allowed to log in.',
  'api.login.error.LoginRequiresTwoFactor': 'Two factor authentication is required.',
  'api.login.error.LoginInvalid': 'Invalid username or password. Please check your login and password and try again. ', 

  'api.student.error.StudentNotFound': 'The student was not found.',
  'api.student.error.StudentNumberNotFound': 'The student number was not found.',
  'api.student.error.StudentPhoneNumberNotFound': 'The student phone number was not found.',

  'api.school.error.SchoolIsRequired':'School is required',
  'api.user.error.DuplicateUserName':'UserName is duplicated',
  'api.user.error.DuplicateEmail':'User email: {{email}} is already existed.',

  'api.common.error.default': 'There is an error when handle this request. Please contact the admin and try again later.',
};
