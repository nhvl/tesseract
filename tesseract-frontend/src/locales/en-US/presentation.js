export default{
    'app.presentation.addBtn.content': 'Content',
    'app.presentation.addBtn.questions': 'Questions',
    'app.presentation.addBtn.heading': 'Heading',
    'app.presentation.addBtn.text': 'Text',
    'app.presentation.addBtn.multimedia': 'Multimedia',
    'app.presentation.addBtn.googleDrive': 'Google Drive',
    'app.presentation.addBtn.googleAccount': 'Google Account',
    'app.presentation.addBtn.discussion': 'Discussion',
    'app.presentation.addBtn.multipleChoice': 'Multiple Choice',
    'app.presentation.addBtn.questionText': 'Text',
    'app.presentation.addBtn.numeric': 'Numeric',
    'app.presentation.addBtn.matchingOptions': 'Matching Options',
    'app.presentation.addBtn.polling': 'Polling',
    'app.presentation.addBtn.insertContent': 'Insert Content',

    'app.presentation.mediaInput.upload': 'Upload',
    'app.presentation.mediaInput.record': 'Record',
    'app.presentation.mediaInput.addPoster': 'Add poster',
    'app.presentation.mediaInput.pickGoogleFile': 'Open Google Drive',
    'app.presentation.mediaInput.pleaseConnect': 'Go to Account Settings to connect Google Account',

    'app.presentation.embedInput.permissionConfirmation': 'Permission Confirmation',
    'app.presentation.previewEmbed.fileIsNotShared': 'File is not shared',
    'app.presentation.previewEmbed.clickToShare': 'Click to share',
    'app.presentation.embedInput.theFileHasBeenSharedWithAnyone': 'The file has been shared to anyone.',
    'app.presentation.embedInput.theFileWasExists': 'This file was exists.',
    'app.presentation.embedInput.confirmAllowSharingContent': 'This permission allows anyone to read \'{{fileName}}\' file.',

    'app.presentation.previewMedia.unsupportMedia': 'Unsupport media',
    'app.presentation.previewMedia.todo': 'TODO',
    
    'app.presentation.previewEmbed.openInGoogleDriveEditor': 'Open in Google Drive Editor',

    'app.presentation.editMatchingQuiz.question': 'Question',
    'app.presentation.editMatchingQuiz.answer': 'Answer',

    'app.presentation.editdiscussionItem.existingTopic': 'Existing Topic',
    'app.presentation.editdiscussionItem.newTopic': 'New Topic',
    'app.presentation.editdiscussionItem.addMedia': 'Add Media',

    'app.presentation.sectionTimeRange.start': 'Start',
    'app.presentation.sectionTimeRange.end': 'End',
}
