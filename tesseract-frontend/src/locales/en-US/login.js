export default {
  'app.login.userName': 'userName',
  'app.login.password': 'password',
  'app.login.password.required': 'The Password field is required.',
  'app.login.password.invalid': 'The password is invalid.',
  'app.login.message-invalid-credentials': 'Invalid username or password',
  'app.login.message-invalid-verification-code': 'Invalid verification code',
  'app.login.tab-login-credentials': 'Credentials',
  'app.login.tab-login-mobile': 'Mobile number',
  'app.login.remember-me': 'Remember me',
  'app.login.forgot-password': 'Forgot your password?',
  'app.login.sign-in-with': 'Or sign in with',
  'app.login.log-in-with': 'Or log in with',
  'app.login.signup': 'Sign up',
  'app.login.log-in': 'log in',
  'app.login.login': 'Login',
  'app.login.welcome': 'Welcome,',
  'app.login.dontHaveAnAccount': "Parents - Don't have an account yet?",
  'app.login.forgot-pass': 'Forgot Password',
  'app.login.reset': 'Reset Password',
  'app.login.error.invalid': 'Invalid email/password.',
  'app.login.error.emailNotConfirm': 'Email is not confirm.',
  'app.login.error.emailNotConfirm.content': 'Please check email for confirmation and try to login again.',
  'app.login.error.emailNotConfirm.okText': 'Send confirm email',
  'app.login.error.emailNotConfirm.sendConfirmationEmailFail': 'Send confirmation mail FAIL.',
  'app.login.error.emailNotConfirm.emailSent': 'Email sent.',
  'app.login.error.userIsLockedOut': 'Your account is locked out.',
  'app.login.error.userIsLockedOut.content': 'Please contact to your Administrator.',
  'app.login.error.unspecific': 'Something went wrong.',

  'app.register.register': 'Parent Portal Sign up',
  'app.register.my-info': 'My Information',
  'app.register.my-student': 'My Student',
  'app.register.create-account': 'Create Account',
  'app.register.studentNumber.placeholder': 'Student Number',
  'app.register.studentPhoneNumber.placeholder': 'Student Phone Number',
  'app.register.get-verification-code': 'Get code',
  'app.register.sign-in': 'Or already have an account?',
  'app.register-result.msg': 'Account：registered at {{email}}',
  'app.register-result.activation-email': 'The activation email has been sent to your email address and is valid for 24 hours. Please log in to the email in time and click on the link in the email to activate the account.',
  'app.register-result.back-home': 'Back to home',
  'app.register-result.view-mailbox': 'View mailbox',
  'app.register-result.goToLogin': 'Go to Login.',
  'app.register-result.emailHasSent': 'An email has sent to your mailbox for confirmation.',
  'app.register-result.checkAndConfirmEmail':'Please check and confirm email before login.',
  'app.register-result.register-successful': 'Register successfully',

  'validation.email.required': 'Please enter your email!',
  'validation.email.wrong-format': 'The email address is in the wrong format!',
  'validation.userName.required': 'Please enter your userName!',
  'validation.password.required': 'Please enter your password!',
  'validation.password.twice': 'The passwords entered twice do not match!',
  'validation.password.strength.msg':
    "Please enter at least 6 characters and don't use passwords that are easy to guess.",
  'validation.password.strength.strong': 'Strength: strong',
  'validation.password.strength.medium': 'Strength: medium',
  'validation.password.strength.short': 'Strength: too short',
  'validation.confirm-password.required': 'Please confirm your password!',
  'validation.phone-number.required': 'Please enter your phone number!',
  'validation.phone-number.wrong-format': 'Malformed phone number!',
  'validation.verification-code.required': 'Please enter the verification code!',
  'validation.title.required': 'Please enter a title',
  'validation.date.required': 'Please select the start and end date',
  'validation.goal.required': 'Please enter a description of the goal',
  'validation.standard.required': 'Please enter a metric',
};
