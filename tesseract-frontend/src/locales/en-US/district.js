export default {
    'app.district.settings': 'District Settings',
    'app.district.roles': 'Roles',

    'app.district.faculty.table.col.firstName': 'First Name',
    'app.district.faculty.table.col.lastName': 'Last Name',
    'app.district.faculty.table.col.school': 'School',
    'app.district.faculty.table.col.open': 'Open Activities',
    'app.district.faculty.table.col.completed': 'Completed Activities',
    'app.district.faculty.table.impersonate': 'Impersonate',
    'app.district.faculty.filter': 'Filter by:',
    'app.district.faculty.filter.selectSchool': 'Filter by School',
    'app.district.faculty.includeInactive': 'Include Inactive',
};

