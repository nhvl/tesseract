export default{
    'app.forgotpassword.error.accountnotexist': 'This email is not exist',
    'app.forgotpassword.emailsend':'An email has send to you, please check.',
    'app.forgotpassword.resetpassword':'Reset Password',
    'app.forgotpassword.or': 'Or',
    'app.resetpassword.error.invalid':'Can not reset your password. Please contact admin.',
    'app.resetpassword.success':'Your password has been successfully reset. Please login again.',
    'app.resetpassword.error.invalidtoken':'Invalid Token',
    'app.resetpassword.error.PasswordTooShort':'Pasword too short',
    'app.resetpassword.error.PasswordRequiresNonAlphanumeric':'Password requires non alphanumeric',
    'app.resetpassword.error.PasswordRequiresUpper':'Password requires upper',
    'app.resetpassword.error.passwordNotMatch':'The passwords do not match',
}