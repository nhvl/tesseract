export default {
    'admin.gradingterm.list.GradingTerm': 'Grading Term',
    'admin.gradingterm.list.Name': 'Name',
    'admin.gradingterm.list.AddGradingTerm': 'Add Grading Term',
    'admin.gradingterm.list.Period': 'Period',
    'admin.gradingterm.list.StartDate': 'Start Date',
    'admin.gradingterm.list.EndDate': 'End Date',
    'admin.gradingterm.form.TermNameRequired': 'Grading Name is required',
    'admin.gradingterm.form.StartDateRequired': 'Start Date is required',
    'admin.gradingterm.form.EndDateRequired': 'End Date is required',
    'admin.gradingterm.form.InvalidDateRange': 'End Date should be greater than Start Date',

    'app.admin.dashboard' : 'Dashboard',
    'app.admin.dashboard.adoptionAndParticipation' : 'Adoption and Participation',
    'app.admin.dashboard.adoptionAndParticipation.openAssignments' : 'Open Assignments',
    'app.admin.dashboard.adoptionAndParticipation.teachersParticipating' : 'Teachers Participating',
    'app.admin.dashboard.adoptionAndParticipation.studentsParticipating' : 'Students Participating',
    'app.admin.dashboard.adoptionAndParticipation.activeSchools' : 'Active Schools',
    'app.admin.dashboard.adoptionAndParticipation.pie.inActive' : 'Inactive',
    'app.admin.dashboard.adoptionAndParticipation.pie.active' : 'Active',
    
    'app.admin.dashboard.studentAchievements' : 'Student Achievements',
    'app.admin.dashboard.studentAchievements.students5C' : 'Student\'s 5C',
  };
