import analysis from './en-US/analysis';
import activities from './en-US/activities';
import exception from './en-US/exception';
import form from './en-US/form';
import globalHeader from './en-US/globalHeader';
import login from './en-US/login';
import classes from './en-US/classes';
import students from './en-US/students';
import menu from './en-US/menu';
import monitor from './en-US/monitor';
import result from './en-US/result';
import settingDrawer from './en-US/settingDrawer';
import settings from './en-US/settings';
import pwa from './en-US/pwa';
import component from './en-US/component';
import dashboard from './en-US/dashboard';
import forgotPassword from './en-US/forgotPassword';
import district from './en-US/district';
import parent from './en-US/parent';
import api from './en-US/api';
import admin from './en-US/admin';
import thead from './en-US/thread';
import presentation from './en-US/presentation';
import home from './en-US/home';
import faculty from './en-US/faculty';
import school from './en-US/school';

export default {
  'navBar.lang': 'Languages',
  'layout.user.link.help': 'Help',
  'layout.user.link.privacy': 'Privacy',
  'layout.user.link.terms': 'Terms',
  'app.home.welcome': 'Welcome',
  'app.home.introduce': 'introduce',
  'app.forms.basic.title': 'Basic form',
  'app.forms.basic.description':
    'Form pages are used to collect or verify information to users, and basic forms are common in scenarios where there are fewer data items.',
  ...analysis,
  ...activities,
  ...exception,
  ...form,
  ...globalHeader,
  ...login,
  ...classes,
  ...students,
  ...menu,
  ...monitor,
  ...result,
  ...settingDrawer,
  ...settings,
  ...pwa,
  ...component,
  ...dashboard,
  ...forgotPassword,
  ...district,
  ...parent,
  ...api,
  ...admin,
  ...thead,
  ...presentation,
  ...home,
  ...faculty,
  ...school
};
