/// <reference types="react-scripts" />

declare module "*.module.less" {
    interface Styles { [key:string]: string }
    const styles: Styles;
    export default styles;
}

declare module "!!raw-loader!*" {
    const text: string;
    export default text;
}

declare module "react-gtm-module" {
    export function initialize(_:{
        gtmId            : string,
        events          ?: {
            event       ?: any,
            pagePath    ?: string,
            pageTitle   ?: string,
            [key:string] : any,
        },
        dataLayer       ?: {
            [key:string] : string,
        },
        dataLayerName   ?: string,
        auth            ?: string,
        preview         ?: string,
    }): void;
}
