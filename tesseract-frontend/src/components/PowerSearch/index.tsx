import React, { FC, useCallback, useState } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { SearchView } from "../../models/PowerSearch";
import { EUserRole } from "../../models/types";

import {AuthorizedStore} from "../../stores/AuthorizedStore";

import { AutoComplete, Tooltip, Icon, Input, Spin } from "antd";
import { DataSourceItemType } from "antd/lib/auto-complete";
const Option = AutoComplete.Option;
const OptGroup = AutoComplete.OptGroup;

import { Link } from "../../components/router/Links";

import styles from "./index.module.less";
import { Modal } from "../Modal/ModalAntd";

export const PowerSearch: FC<{store:AuthorizedStore}> = observer(({store}) => {
    const {sPowerSearch, currentUser} = store;
    const {t} = useTranslation();
    const [options, setOptions] = useState([] as any);
    const [isSelectingKey, set_isSelectingKey] = useState(false);
    const handleSelect = useCallback((value: any, option:any) => {
        set_isSelectingKey(true);
        sPowerSearch.handleSelect(value, option);
    },[isSelectingKey]);

    const handleSearch = useCallback((value:any) => {
        if(isSelectingKey || !currentUser) return;
        value = value.trim();
        if(value.length > 1) {
            //show loading
            setOptions([
                <Option disabled key="all" className={styles.showAll}>
                    <Spin />
              </Option>
            ]);
            sPowerSearch.handleSearch(currentUser.role, value).then(([err, data]) => {
                if (err) {
                    Modal.error({ title: "search.failed", content: err.message });
                    return;
                }
                setOptions(renderOptions());
            });
        }
        else {
            setOptions([]);
        }
    },[isSelectingKey]);

    const renderItems = useCallback((items:SearchView[], key:string):DataSourceItemType=>{
        let options:DataSourceItemType
            = (items.length > 0 ? (<OptGroup key={key} label={key}>
                {items.map((opt: SearchView) => (
                    <Option key={key + String(opt.id)} value={key + String(opt.id)}>
                        {opt.name}
                    </Option>))}
                </OptGroup>) : "");
        return options;
    },[]);

    const renderOptions = useCallback(():DataSourceItemType[]=>{
        let studentOptions   : DataSourceItemType = renderItems(sPowerSearch.students, "Student");
        let classOptions     : DataSourceItemType = renderItems(sPowerSearch.classes, "Class");
        let pageOptions      : DataSourceItemType = renderItems(sPowerSearch.pages, "Page");
        let assignmentOptions: DataSourceItemType = renderItems(sPowerSearch.assignments, "Assignment");
        let assessmentOptions: DataSourceItemType = renderItems(sPowerSearch.assessments, "Assessment");
        let schoolsOptions   : DataSourceItemType = renderItems(sPowerSearch.schools, "School");
        let facultiesOptions : DataSourceItemType = renderItems(sPowerSearch.faculties, "Faculty");
        let parentsOptions   : DataSourceItemType = renderItems(sPowerSearch.parents, "Parent");
        let options = [];
        if(studentOptions    != "") options.push(studentOptions);
        if(classOptions      != "") options.push(classOptions);
        if(pageOptions       != "") options.push(pageOptions);
        if(assignmentOptions != "") options.push(assignmentOptions);
        if(assessmentOptions != "") options.push(assessmentOptions);
        if(schoolsOptions    != "") options.push(schoolsOptions);
        if(facultiesOptions  != "") options.push(facultiesOptions);
        if(parentsOptions    != "") options.push(parentsOptions);
        const maxItems = 5;
        if(sPowerSearch.students.length >= maxItems
            || sPowerSearch.classes.length >= maxItems
            || sPowerSearch.pages.length >= maxItems
            || sPowerSearch.assignments.length >= maxItems
            || sPowerSearch.assessments.length >= maxItems
            || sPowerSearch.schools.length >= maxItems
            || sPowerSearch.faculties.length >= maxItems
            || sPowerSearch.parents.length >= maxItems
            ) {
            options = options.concat([
                <Option disabled key="all" className={styles.showAll}>
                {/*TODO: show more search result in another page.*/}
                <Link routeName="powerSearchDetail"><span>{t("component.globalHeader.powerSearch.showMore")}</span></Link>
              </Option>,
            ]);
        }

        if(options.length == 0) {
            //can"t find any
            options = options.concat([
                <Option disabled key="all" className={styles.showAll}>
                    {t("component.globalHeader.powerSearch.notFound")}
              </Option>,
            ])
        }

        return options;
    },[]);

    const tooltipSearch = useCallback(()=>{
        if(currentUser) {
            switch (currentUser.role) {
                case EUserRole.Admin:
                case EUserRole.SchoolAdmin:
                    return t("component.globalHeader.powerSearch.guide.admin");
                case EUserRole.Faculty:
                    return t("component.globalHeader.powerSearch.guide.faculty");
                case EUserRole.Student:
                    return t("component.globalHeader.powerSearch.guide.student");
                case EUserRole.Parent:
                    return t("component.globalHeader.powerSearch.guide.parent");
            }
        }
        return "";
    },[currentUser]);

    return (<Tooltip placement="bottomLeft" title={options.length != 0 ? "" : tooltipSearch()}>

                <AutoComplete
                    className = "power-search"
                    dataSource = {options}
                    onSelect = {handleSelect}
                    onChange = {handleSearch}
                    placeholder={t("component.globalHeader.powerSearch")}
                    size="large"
                >
                    <Input suffix={(
                        <Icon type="search" className="icon-lg"/>
                    )} className="search-input" />
                </AutoComplete>

            </Tooltip>);
})

