import React, { FC, CSSProperties } from 'react';
import { observer } from 'mobx-react';

import { UploadFile } from 'antd/lib/upload/interface';
import { DumbDragger, IDumbDraggerProps } from "./inputs/antd/Upload";

const style:CSSProperties = { height:100 };

interface IImageInputProps extends IDumbDraggerProps {
    image:string,
    imageFile:UploadFile|undefined,
    className?:string|undefined,
    onChange:(file:UploadFile) => void,
    placeholder?:string|undefined
}

export const ImageInput: FC<IImageInputProps> = observer(({onChange, image, imageFile, className, placeholder, ...props}) => {
    const src = (imageFile != null
        ? (imageFile.url || imageFile.thumbUrl)
        : "") || image;

    return (<DumbDragger onChange={onChange} createThumb {...props}>{
        !src
            ? (<div style={style}>{placeholder ? placeholder : "" }</div>)
            : (<img key={src} src={src} className={className ? className : "imgFile"} /> )
    }</DumbDragger>);
})
