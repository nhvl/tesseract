import React, { FC, useCallback, CSSProperties, ReactNode } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";
import classNames from "classnames";

import { ScoreBadge } from "../../models/ScoreBadge";
import { ActivityScore, FiveC, FiveCValues } from "../../models/ActivityScore";

import { Tooltip } from 'antd';

import {FiveCTitle, FiveCValueDesc} from "../FiveCTitle";

import styles from "./index.module.less";

export const LongScoreMedals: FC<{scoreBadge: ScoreBadge, score?: ActivityScore, scoreChangeHandler?: ((cType: FiveC) => void)}> = observer(({scoreBadge, score, scoreChangeHandler}) => {
    if (score == null) return null;
    const {communication, collaboration, character, creativity, criticalThinking} = score;

    return (<>
        <MedalIcon score={communication   } fiveCType={FiveC.Communication   } onClick={scoreChangeHandler} className={styles.scoreMinWidth} scoreBadge={scoreBadge} mode="long" />
        <MedalIcon score={collaboration   } fiveCType={FiveC.Collaboration   } onClick={scoreChangeHandler} className={styles.scoreMinWidth} scoreBadge={scoreBadge} mode="long" />
        <MedalIcon score={character       } fiveCType={FiveC.Character       } onClick={scoreChangeHandler} className={styles.scoreMinWidth} scoreBadge={scoreBadge} mode="long" />
        <MedalIcon score={creativity      } fiveCType={FiveC.Creativity      } onClick={scoreChangeHandler} className={styles.scoreMinWidth} scoreBadge={scoreBadge} mode="long" />
        <MedalIcon score={criticalThinking} fiveCType={FiveC.CriticalThinking} onClick={scoreChangeHandler} className={styles.scoreMinWidth} scoreBadge={scoreBadge} mode="long" />
    </>);
});

export const ShortScoreMedals: FC<{scoreBadge: ScoreBadge, score?: ActivityScore, scoreChangeHandler?: ((cType: FiveC) => void), className?: string}> = observer(({scoreBadge, score, scoreChangeHandler, className}) => {
    if (score == null) return null;
    const {communication, collaboration, character, creativity, criticalThinking} = score;

    return (<>
        <MedalIcon score={communication    } fiveCType={FiveC.Communication   } onClick={scoreChangeHandler} scoreBadge={scoreBadge} className={className} />
        <MedalIcon score={collaboration    } fiveCType={FiveC.Collaboration   } onClick={scoreChangeHandler} scoreBadge={scoreBadge} className={className} />
        <MedalIcon score={character        } fiveCType={FiveC.Character       } onClick={scoreChangeHandler} scoreBadge={scoreBadge} className={className} />
        <MedalIcon score={creativity       } fiveCType={FiveC.Creativity      } onClick={scoreChangeHandler} scoreBadge={scoreBadge} className={className} />
        <MedalIcon score={criticalThinking } fiveCType={FiveC.CriticalThinking} onClick={scoreChangeHandler} scoreBadge={scoreBadge} className={className} />
    </>);
});

export const MedalIcon: FC<{
    mode       ?: "long" | "short",
    score      ?: FiveCValues,
    fiveCType   : FiveC,
    onClick    ?: ((type:FiveC) => void),
    scoreBadge  : ScoreBadge,
    className  ?: string,
}> = observer(({score, fiveCType, scoreBadge, onClick, ...props}) => {
    const handleClick = useCallback(() => {
        if (onClick != null) onClick(fiveCType);
    }, [onClick, fiveCType]);

    const style: CSSProperties = ({
        color: score == null ? undefined : scoreBadge.getBadgeColor(fiveCType, score),
    });

    return (score == null || score == FiveCValues.Unknown) ? null : (
        <MedalIconInner
            {...props}
            score={score}
            title={<><FiveCTitle value={fiveCType} />: <FiveCValueDesc value={score} /></>}
            onClick={handleClick}
            style={style}
            />
    );
});

const MedalIconInner: FC<{
    mode      ?: "long" | "short",
    score     ?: FiveCValues,
    title      : ReactNode,
    style      : CSSProperties,
    onClick   ?: (() => void),
    className ?: string,
}> = observer(({mode = "short", score, title, style, onClick, className}) => {
    return (score == null || score == FiveCValues.Unknown) ? null : (
        <div className={classNames(styles.score, !!onClick && styles.clickableScore, className)}
            onClick={onClick}>
            {mode == "short" ? (
                <Tooltip title={title} placement="bottom">
                    <span className="mr-sm">
                        <i className="fas fa-award icon-md" style={style}></i>
                    </span>
                </Tooltip>
            ) : (<>
                <span className="mr-sm">
                    <i className="fas fa-award icon-md" style={style}></i>
                </span>
                <span>{title}</span>
            </>)}
        </div>
    );
});


