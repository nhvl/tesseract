import React, { FC } from "react";
import { observer } from "mobx-react";

import { MediaTypeEnum } from "../../models/MediaTypeEnum";
import { Discussion } from "../../models/Discussion";

import { UserStore } from "../../stores/UserStore";

import { Typography, } from "antd";
const { Title } = Typography;

import ReactPlayer from "react-player";

import { DateFromNowToolTip, } from "../../components/DateTime/DateTime";
import { Trans } from "react-i18next";

export const DiscussionDetail: FC<{discussion:Discussion}> = observer(({discussion}) => {
    return (<>
        <div className="mb-lg">
            <div className="mb-xs">
                <Title level={1}>{discussion.title}</Title>
            </div>
            <div><DiscussionMeta discussion={discussion} /></div>
        </div>
        <div className="video">
            {(  discussion.mediaType == MediaTypeEnum.VideoFile ||
                discussion.mediaType == MediaTypeEnum.VideoLink ||
                discussion.mediaType == MediaTypeEnum.AudioLink   ) ? (
                <ReactPlayer url={discussion.link} controls pip />
            ) : ((discussion.mediaType == MediaTypeEnum.AudioFile) ? (
                <video controls className="imgFile" src={discussion.link} />
            ) : null)}

            {(discussion && discussion.mediaType == MediaTypeEnum.ImageFile && (
                <img src={discussion.link} className="imgFile w-auto" />
            ))}
        </div>
        <div dangerouslySetInnerHTML={{__html:discussion.content}} />
    </>);
});

const DiscussionMeta: FC<{discussion:Discussion}> = observer(({discussion}) => {
    const sUser = UserStore.useContext();
    const user = discussion.createdBy == null || sUser == null ? undefined : sUser.mId2Item.get(discussion.createdBy);
    return (<>
        {user && (<Trans i18nKey="app.classes.discussions.postedBy">
                        <span className="text-grey font-medium"><DateFromNowToolTip value={discussion.dateCreated!} /></span>
                        <span className="text-grey font-medium">{user.fullName}</span>
                    </Trans>)}
    </>);
});

