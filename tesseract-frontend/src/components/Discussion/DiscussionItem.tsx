import React, { FC } from "react";
import { observer } from "mobx-react";
import { Trans } from "react-i18next";

import { Discussion } from "../../models/Discussion";

import { UserStore } from "../../stores/UserStore";
import { CommentStore } from "../../stores/CommentStore";

import { Badge, } from "antd";

import { Link } from "../router/Links";
import { DateFromNowToolTip } from "../DateTime/DateTime";

import styles from "./DiscussionItem.module.less";

export const DiscussionItem: FC<{
    item     : Discussion,
    unread   : number,
}> = observer((props) => {
    const {item, unread, children } = props;

    const sUser = UserStore.useContext();
    const user = item.createdBy ? sUser.mId2Item.get(item.createdBy) : undefined;

    const sComment = CommentStore.useContext();
    const lastComment = sComment.mThreadId2LastComment.get(item.threadId);
    const lastCommentUser = (lastComment == null || lastComment.createdBy == null) ? null
        : sUser.mId2Item.get(lastComment.createdBy);

    return (
        <div className={`${styles.listDiscussion} border mb-md`}>
            <div className="mb-xs flex items-center justify-between">
                <Link routeName="discussion" params={item.params}>
                    <span className="text-link-grey font-medium font-size-md">{item.title}</span>
                </Link>
                {unread > 0 && (<div className={`${styles.unreadComment} flex items-center`}>
                    <i className="fas fa-comments"></i>
                    <Badge count={unread} />
                </div>)}
            </div>

            {lastComment ? (
                lastCommentUser == null ? (
                    <Trans i18nKey="app.classes.discussions.lastComment">
                        <span className="text-grey font-medium"><DateFromNowToolTip value={lastComment.dateCreated!} /></span>
                    </Trans>
                ) : (
                    <Trans i18nKey="app.classes.discussions.lastCommentBy">
                        <span className="text-grey font-medium"><DateFromNowToolTip value={lastComment.dateCreated!} /></span>
                        <span className="text-grey font-medium">{lastCommentUser.fullName}</span>
                    </Trans>
                )
            ) : (
                user == null ? (
                    <Trans i18nKey="app.classes.discussions.posted">
                        <span className="text-grey font-medium"><DateFromNowToolTip value={item.dateCreated!} /></span>
                    </Trans>
                ) : (
                    <Trans i18nKey="app.classes.discussions.postedBy">
                        <span className="text-grey font-medium"><DateFromNowToolTip value={item.dateCreated!} /></span>
                        <span className="text-grey font-medium">{user.fullName}</span>
                    </Trans>
                )
            )}

            {children}
        </div>
    );
});
