import React, { FC, Children, Fragment,  } from 'react';
import { observer } from 'mobx-react';

import { Divider, Dropdown, Menu, Icon } from 'antd';

interface IResponsiveActionsProps  {
    breakpoint ?:string;
    sm         ?:number,
    xs         ?:number,
    md         ?:number,
    lg         ?:number,
    xl         ?:number,
}

export const ResponsiveActions: FC<IResponsiveActionsProps> = observer(({
    breakpoint,
    xs, sm, md, lg, xl,
    children
}) => {
    const n = getValueByBreakpoint({breakpoint:breakpoint||"", xs, sm, md, lg, xl, defaultValue:0});
    const ys = Children.toArray(children).filter(Boolean);
    const fs = ys.slice(0, n);
    const bs = ys.slice(n);



    return (<>
        {fs.map((child, index) => (
            <Fragment key={index}>
                {index > 0 && (<Divider type="vertical"/>)}
                {child}
            </Fragment>
        ))}
        {bs.length > 0 && (
            <Dropdown overlay={(
                <Menu>
                    {Children.map(bs, (child) => (<Menu.Item>{child}</Menu.Item>))}
                </Menu>
            )}>
                <a className="ant-dropdown-link"><Icon type="ellipsis" /></a>
            </Dropdown>
        )}
    </>)
});

function getValueByBreakpoint<T>({breakpoint, xl, lg, md, sm, xs, defaultValue}:{breakpoint:string, xl?:T, lg?:T, md?:T, sm?:T, xs?:T, defaultValue:T}) {
    let ys = [xl, lg, md, sm, xs];
    switch(breakpoint) {
        case "xl": ys = ys.slice(-5); break;
        case "lg": ys = ys.slice(-4); break;
        case "md": ys = ys.slice(-3); break;
        case "sm": ys = ys.slice(-2); break;
        case "xs":
        default  : ys = ys.slice(-1); break;
    }
    return ys.filter(x => x != null)[0] || defaultValue;
}
