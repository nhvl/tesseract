import React, { FC, useCallback, useMemo } from 'react';
import { observer } from 'mobx-react';

import { Table } from 'antd';
import { TableProps } from 'antd/lib/table';

interface ResponsiveTableProps<T> extends TableProps<T> {
    isMobile ?:boolean;
}

export const ResponsiveTable: FC<ResponsiveTableProps<any>> = observer(({isMobile, ...props}) => {
    return (<div className="overflow-y-auto">{
        (isMobile && props.columns && props.columns.length > 2) ? (
            <CollapseTable {...props} />
        ) : (
            <Table {...props} />
        )
    }</div>);
});

const CollapseTable: FC<ResponsiveTableProps<any>> = observer(({isMobile, ...props}) => {
    const expandedRowRender = useCallback((record:any, index:number) => (props.columns && (
        <table><tbody>{props.columns.slice(1, -1).map(column => (<tr key={column.key}>
            <th>{column.title}</th>
            <td>{column.render ? column.render(record[column.dataIndex!], record, index) : record[column.dataIndex!]}</td>
        </tr>))}</tbody></table>
    )), []);

    const columns = useMemo(() =>
        (props.columns && props.columns.length > 2)
            ? [props.columns[0], props.columns[props.columns.length - 1]]
            : props.columns,
        [props.columns]);

    return (
        <Table {...props}
            columns={columns}
            expandedRowRender={expandedRowRender}
            />
    );
});
