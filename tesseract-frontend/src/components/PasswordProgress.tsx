import React, { FC, useMemo } from 'react';
import { observer } from "mobx-react";

import { Progress } from 'antd';

import { scorePassword } from '../utils/password';

export const PasswordProgress: FC<{value:string}> = observer(({value}) => {
  const passScore = useMemo(() => scorePassword(value), [value])
  const progressStatus = passScore <= 30 ? "exception" : (
                         passScore <  60 ? "normal" : (
                         passScore <  80 ? "success" : (
                         undefined)));

  return (
    !!value ? (
      <Progress status={progressStatus}
        strokeWidth={6}
        percent={Math.min(passScore, 100)}
        showInfo={false} />
    ) : null
  );
});

