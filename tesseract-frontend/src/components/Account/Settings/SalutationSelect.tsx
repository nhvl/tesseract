import React, { FC } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { Select, } from "antd";
import { SelectProps } from "antd/lib/select";

export const SalutationSelect: FC<SelectProps<string>> = observer((props) => {
  const {t} = useTranslation();
  return (
    <Select {...props}>
      <Select.Option value="">&nbsp;</Select.Option>
      <Select.Option value="Mr." >{t("app.user.salutation.option.Mr" )}</Select.Option>
      <Select.Option value="Mrs.">{t("app.user.salutation.option.Mrs")}</Select.Option>
      <Select.Option value="Ms." >{t("app.user.salutation.option.Ms" )}</Select.Option>
      <Select.Option value="Dr." >{t("app.user.salutation.option.Dr" )}</Select.Option>
    </Select>
  );
})
