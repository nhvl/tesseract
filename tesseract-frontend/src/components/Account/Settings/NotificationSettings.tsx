import React, { FC, useCallback, useMemo, useEffect, ReactNode } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { ENotificationSendType, UserNotificationSetting } from "../../../models/UserNotificationSetting";

import { AccountSettingStore } from "./AccountSettingStore";

import { Select, Checkbox, Form, Row, Col, } from "antd";
import { SelectProps } from "antd/lib/select";

import {TimePicker} from "../../inputs/antd/TimePicker";

import styles from "./NotificationSettings.module.less";

export const NotificationSettings: FC<{store:AccountSettingStore}> = observer(({store:sAccountSetting}) => {
  const {t} = useTranslation();
  useEffect(() => {
    sAccountSetting.initNotificationSettings();
  }, []);

  return (
    <Form>
      <Row gutter={16}>
        <Col xs={24} md={8} lg={8} xl={8} xxl={8}>
          <NotificationSettingSection store={sAccountSetting} setting={sAccountSetting.emailNotificationSetting}
            label={t("app.settings.notification.email.title")} />
        </Col>
        <Col xs={24} md={8} lg={8} xl={8} xxl={8}>
          <NotificationSettingSection store={sAccountSetting} setting={sAccountSetting.smsNotificationSetting}
            label={t("app.settings.notification.sms.title")} />
        </Col>
        <Col xs={24} md={8} lg={8} xl={8} xxl={8}>
          <NotificationSettingSection store={sAccountSetting} setting={sAccountSetting.pwaPushNotificationSetting}
            label={t("app.settings.notification.pwaPush.title")} />
        </Col>
      </Row>
    </Form>
  );
});

const NotificationSettingSection: FC<{store:AccountSettingStore, setting:UserNotificationSetting, label:ReactNode}> = observer(({store, setting, label}) => {
  const onFormChange = useCallback(() => {
    store.onNotificationChange(setting);
  }, [store, setting]);

  const change_sendType = useCallback((v:ENotificationSendType) => {
    setting.set_sendType(v);
    store.onNotificationChange(setting);
  }, [store, setting]);

  const change_sendTime = useCallback((v:number) => {
    setting.set_sendTime(v);
    store.onNotificationChange(setting);
  }, [store, setting]);

  const change_weeklyDay = useCallback((v:number) => {
    setting.set_weeklyDay(v);
    store.onNotificationChange(setting);
  }, [store, setting]);

  return (
    <Form onChange={onFormChange}>
        <Form.Item label={label}>
          <NotificationTypeSelect value={setting.sendType} onChange={change_sendType} />
        </Form.Item>
      {setting.sendType == ENotificationSendType.Daily && (
        <Form.Item>
          <TimePicker value={setting.sendTime} onChange={change_sendTime} />
        </Form.Item>
      )}
      {setting.sendType == ENotificationSendType.Weekly && (
        <Form.Item>
          <WeekDayCheckbox value={setting.weeklyDay} onChange={change_weeklyDay} />
        </Form.Item>
      )}
    </Form>
  );
});

const NotificationTypeSelect: FC<SelectProps<ENotificationSendType>> = observer((props) => {
  const {t} = useTranslation();
  return (
    <Select {...props}>
      <Select.Option value={ENotificationSendType.DontSend  }>{t("app.settings.notification.sendType.DontSend"  )}</Select.Option>
      <Select.Option value={ENotificationSendType.Imediately}>{t("app.settings.notification.sendType.Imediately")}</Select.Option>
      <Select.Option value={ENotificationSendType.Daily     }>{t("app.settings.notification.sendType.Daily"     )}</Select.Option>
      <Select.Option value={ENotificationSendType.Weekly    }>{t("app.settings.notification.sendType.Weekly"    )}</Select.Option>
    </Select>
  );
});

const SundayMask    = 0b1000000;
const MondayMask    = 0b0100000;
const TuesdayMask   = 0b0010000;
const WednesdayMask = 0b0001000;
const ThursdayMask  = 0b0000100;
const FridayMask    = 0b0000010;
const SaturdayMask  = 0b0000001;

const WeekDayCheckbox: FC<{value:number, onChange?:(v:number) => void}> = observer(({value, onChange}) => {
  const {t} = useTranslation();
  const options = useMemo(() => [
    { label: t("app.weekday.Sunday"   ), value:SundayMask    },
    { label: t("app.weekday.Monday"   ), value:MondayMask    },
    { label: t("app.weekday.Tuesday"  ), value:TuesdayMask   },
    { label: t("app.weekday.Wednesday"), value:WednesdayMask },
    { label: t("app.weekday.Thursday" ), value:ThursdayMask  },
    { label: t("app.weekday.Friday"   ), value:FridayMask    },
    { label: t("app.weekday.Saturday" ), value:SaturdayMask  },
  ], [t]);
  const values = [
    SundayMask,
    MondayMask,
    TuesdayMask,
    WednesdayMask,
    ThursdayMask,
    FridayMask,
    SaturdayMask,
  ].map(mask => value & mask);
  const handleChange = useCallback((values:number[]) => {
    if (onChange) {
      onChange(values.reduce((s, v) => s + v, 0))
    }
  }, [onChange]);

  return (
    <Checkbox.Group options={options} value={values} onChange={handleChange} className={`${styles.weeklyCheckbox}`} />
  );
});
