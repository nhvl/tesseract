import React, { FC, useCallback, FormEvent, useState } from 'react';
import { observer } from "mobx-react";
import { useTranslation } from 'react-i18next';

import { AccountSettingStore } from './AccountSettingStore';

import { Form, Button, Divider, Row, Col, } from 'antd';
const FormItem = Form.Item;

import { Input } from "../../inputs/antd/Input";
import { PasswordProgress } from "../../PasswordProgress";
import { Modal } from '../../Modal/ModalAntd';

export const SecurityView: FC<{store:AccountSettingStore}> = observer(({store:sAccountSetting}) => {
  const {t} = useTranslation();
  const [loading, setLoading] = useState(false);
  const [confirmDirty, setDirty] = useState(false);
  const onBlurConfirm = useCallback(() => { setDirty(true); }, [setDirty]);

  const onSubmit = useCallback(async (event:FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (sAccountSetting.newPassword != sAccountSetting.confirmPassword) return;
    setLoading(true);
    const err = await sAccountSetting.changePassword();
    setLoading(false);
    if (err) {
      if (err.data == null) {
        Modal.error({content:t('app.login.error.unspecific')});
        return;
      }
      try {
        Modal.error({content:Object.values(err.data!).map((x:any) => (<div>{t(`form.error.${x.code}`, {count:8, defaultValue:x.description})}</div>))})
      } catch(e) {
        console.error(e);
        Modal.error({content:t('app.login.error.unspecific')});
      }
    }
  }, []);

  return (
    <Form onSubmit={onSubmit} hideRequiredMark><fieldset disabled={loading}>
      <Row gutter={16}>
        <Col xs={24} md={24} lg={8} xl={8} xxl={8}>
          <FormItem label={t('app.settings.form.oldPassword')}>
            <Input value={sAccountSetting.oldPassword} onChange={sAccountSetting.set_oldPassword}
              type="password" autoComplete="current-password" />
          </FormItem>
        </Col>
        <Col xs={24} md={24} lg={8} xl={8} xxl={8}>
          <FormItem label={t('app.settings.form.newPassword')}>
            <Input value={sAccountSetting.newPassword} onChange={sAccountSetting.set_newPassword}
              type="password" autoComplete="new-password" />
            <PasswordProgress value={sAccountSetting.newPassword} />
          </FormItem>
        </Col>
        <Col xs={24} md={24} lg={8} xl={8} xxl={8}>
          <FormItem label={t('app.settings.form.confirmPassword')}
            help={confirmDirty && sAccountSetting.newPassword != sAccountSetting.confirmPassword ? t("validation.password.twice") : ""}
            validateStatus={!confirmDirty ? "" : (sAccountSetting.newPassword != sAccountSetting.confirmPassword ? "error" : "success")}
            >
            <Input value={sAccountSetting.confirmPassword} onChange={sAccountSetting.set_confirmPassword}
              onBlur={onBlurConfirm}
              type="password" autoComplete="new-password" />
          </FormItem>
        </Col>
      </Row>
      <Divider />
      <Button type="primary" htmlType="submit">{t("app.settings.form.updatePassword")}</Button>
    </fieldset></Form>
  );
});

