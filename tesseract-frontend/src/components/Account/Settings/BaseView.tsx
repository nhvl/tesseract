import React, { FC, useCallback, FormEvent, useState, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";

import { User } from "../../../models/User";

import { AccountSettingStore } from "./AccountSettingStore";

import { Form, Button, message, Avatar, Icon, Row, Col, Divider } from "antd";
import { UploadFile } from "antd/lib/upload/interface";

import { Input } from "../../inputs/antd/Input";
import { DumbUpload } from "../../inputs/antd/Upload";
import { LanguageSelect } from "../../inputs/LanguageSelect";
import { PhoneInput } from "../../inputs/PhoneInput";

import styles from "./BaseView.module.less";
import { Modal } from "../../Modal/ModalAntd";
import { SalutationSelect } from "./SalutationSelect";
import { EUserRole } from "../../../models/types";

const AvatarView:FC<{
  user      : User,
  avatarFile: UploadFile|null,
  onChange  : (file:UploadFile) => void,
}> = observer(({user, avatarFile, onChange}) => {
  const {t} = useTranslation();
  const src = avatarFile == null ? (user.avatar) : avatarFile.thumbUrl;

  return (<div className="flex paddingBottom-lg">
    <div><Avatar className={styles.avatar} src={src} alt="avatar" icon="user" /></div>
    <div className="flex flex-col justify-center">
      <DumbUpload onChange={onChange} accept="image/x-png,image/jpeg" createThumb>
        <div>
          <span className="paddingRight-xs">{t("app.settings.basic.upload-new-avatar")}</span>
          <Icon type="upload" className="icon-lg" />
        </div>
      </DumbUpload>
    </div>
  </div>);
});

export const BaseView: FC<{store:AccountSettingStore}> = observer(({store:sAccountSetting}) => {
  const {user} = sAccountSetting;
  const {t} = useTranslation();
  const [loading, setLoading] = useState(false);

  const onSubmit = useCallback(async (event:FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setLoading(true);
    const err = await sAccountSetting.save();
    setLoading(false);
    if (err) {
      if (err.data != null) try {
        Modal.error({content:Object.values<string[]>(err.data!).map((x) => (<div>{x.map(y => (<div>{y}</div>))}</div>))})
        return;
      } catch(e) {
        console.error(e);
      } else if (err.message != null){
        Modal.error({content:err.message});
        return;
      }
      Modal.error({content:t("form.error.unspecific")});
    }
    message.success(t("app.settings.basic.updateSuccess"));
  }, [])

  return (<>
    <AvatarView user={user} avatarFile={sAccountSetting.avatarFile} onChange={sAccountSetting.setAvatarFile} />
    <Form onSubmit={onSubmit} hideRequiredMark><fieldset disabled={loading}>
      <Row gutter={16}>
        <Col xs={24} md={24} lg={24} xl={24} xxl={16}>
          <Form.Item label={t("app.settings.basic.email")}>
            <Input value={user.email} type="email" autoComplete="email" disabled />
          </Form.Item>
        </Col>
        {user.role == EUserRole.Faculty && (
        <Col xs={24} md={24} lg={24} xl={24} xxl={16}>
          <Form.Item label={t("app.settings.basic.salutation")}>
            <SalutationSelect value={user.salutation} onChange={user.set_salutation} />
          </Form.Item>
        </Col>
        )}
      </Row>
      <Row gutter={16}>
        <Col xs={24} md={24} lg={12} xl={12} xxl={8}>
          <Form.Item label={t("app.settings.basic.fname")}>
            <Input value={user.firstName} onChange={user.set_firstName}
              autoFocus required autoComplete="given-name" maxLength={70} />
          </Form.Item>
        </Col>
        <Col xs={24} md={24} lg={12} xl={12} xxl={8}>
          <Form.Item label={t("app.settings.basic.lname")}>
            <Input value={user.lastName} onChange={user.set_lastName}
              required autoComplete="family-name" maxLength={70} />
          </Form.Item>
        </Col>
        {user.role == EUserRole.Faculty && (
          <Col xs={24} md={24} lg={24} xl={24} xxl={16}>
            <Form.Item label={t("app.settings.basic.nickname")}>
              <Input value={user.nickname} onChange={user.set_nickname}
                placeholder={user.fullName}
                autoFocus autoComplete="honorific-prefix" maxLength={70} />
            </Form.Item>
          </Col>
        )}
      </Row>
      <Row gutter={16}>
        <Col xs={24} md={24} lg={12} xl={12} xxl={8}>
          <Form.Item label={t("app.settings.basic.phone")}>
            <PhoneInput value={user.phoneNumber} onChange={user.set_phoneNumber}
              country="US" />
          </Form.Item>
        </Col>
        <Col xs={24} md={24} lg={12} xl={12} xxl={8}>
          <Form.Item label={t("app.settings.basic.language")}>
            <LanguageSelect value={user.language} onChange={user.set_language} />
          </Form.Item>
        </Col>
      </Row>
      <Divider />
      <Button type="primary" htmlType="submit" loading={loading}>{t("app.settings.basic.update")}</Button>
    </fieldset></Form>
  </>);
});
