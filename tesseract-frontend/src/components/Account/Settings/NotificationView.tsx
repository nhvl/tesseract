import React, { FC, ReactNode } from 'react';
import { observer } from "mobx-react";
import { useTranslation } from 'react-i18next';

import { Switch, List } from 'antd';

interface IItem {
  title: ReactNode;
  description: ReactNode;
  actions: ReactNode[];
}

export const NotificationView: FC = observer(() => {
  const { t } = useTranslation();
  const Action = (
    <Switch
      checkedChildren={t('app.settings.open')}
      unCheckedChildren={t('app.settings.close')}
      defaultChecked
    />
  );
  const data: IItem[] = [
    {
      title: t('app.settings.notification.password'),
      description: t('app.settings.notification.password-description'),
      actions: [Action],
    },
    {
      title: t('app.settings.notification.messages'),
      description: t('app.settings.notification.messages-description'),
      actions: [Action],
    },
    {
      title: t('app.settings.notification.todo'),
      description: t('app.settings.notification.todo-description'),
      actions: [Action],
    },
  ];

  return (
    <List
      itemLayout="horizontal"
      dataSource={data}
      renderItem={(item: IItem) => (
        <List.Item actions={item.actions}>
          <List.Item.Meta title={item.title} description={item.description} />
        </List.Item>
      )}
    />
  );
});
