import React, { FC,  } from 'react';
import { observer } from 'mobx-react';
import { useTranslation, Trans } from 'react-i18next';

import { List } from 'antd';

const passwordStrength = {
  strong: (<span className="strong"><Trans i18nKey="app.settings.security.strong" /></span>),
  medium: (<span className="medium"><Trans i18nKey="app.settings.security.medium" /></span>),
  weak: (<span className="weak"><Trans i18nKey="app.settings.security.weak" /></span>),}
;

export const SecurityView:FC = observer(() => {
  const {t} = useTranslation();

  const data = [
    {
      title: t('app.settings.security.password'),
      description: (
        <>
          {t('app.settings.security.password-description')}：
          {passwordStrength.strong}
        </>
      ),
      actions: [
        <a>{t("app.settings.security.modify")}</a>,
      ],
    },
    {
      title: t('app.settings.security.phone'),
      description: `${t('app.settings.security.phone-description')}： 138****8293`,
      actions: [
        <a>{t("app.settings.security.modify")}</a>,
      ],
    },
    {
      title: t('app.settings.security.question'),
      description: t('app.settings.security.question-description'),
      actions: [
        <a>{t("app.settings.security.set")}</a>,
      ],
    },
    {
      title: t('app.settings.security.email'),
      description: `${t('app.settings.security.email-description')}： ant***sign.com`,
      actions: [
        <a>{t("app.settings.security.modify")}</a>,
      ],
    },
    {
      title: t('app.settings.security.mfa'),
      description: t('app.settings.security.mfa-description'),
      actions: [
        <a>{t("app.settings.security.bind")}</a>,
      ],
    },
  ];

  return (
    <List
      itemLayout="horizontal"
      dataSource={data}
      renderItem={(item: any) => (
        <List.Item actions={item.actions}>
          <List.Item.Meta title={item.title} description={item.description} />
        </List.Item>
      )}
    />
  );
});
