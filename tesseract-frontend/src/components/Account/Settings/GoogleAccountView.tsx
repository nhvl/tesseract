import React, { FC, useEffect } from 'react';
import { observer } from "mobx-react";

import { AccountSettingStore } from './AccountSettingStore';

import { GoogleAuthentication } from '../../GoogleApis/GoogleAuthentication';
import { Modal } from '../../Modal/ModalAntd';


export const GoogleAccountView: FC<{store:AccountSettingStore}> = observer(({store:sAccountSetting}) => {
  const {sGoogleDriveStore} = sAccountSetting;
  useEffect(() => {
    sGoogleDriveStore.init().then(err => {
          if (err) Modal.error({content: err.message});
      });
  }, []);
  return (<GoogleAuthentication store={sGoogleDriveStore}/>);
});

