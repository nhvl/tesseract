import { observable, action, runInAction } from "mobx";

import { User } from "../../../models/User";
import { AuthorizedStore } from "../../../stores/AuthorizedStore";

import { uploadFile } from "../../../services/api/fetch";
import { UploadFile } from "antd/lib/upload/interface";
import { UserNotificationSetting, ENotificationType, ENotificationSendType } from "../../../models/UserNotificationSetting";
import { GoogleDriveStore } from "../../../stores/GoogleDriveStore";

export class AccountSettingStore {
    @observable user: User = new User();

    constructor(private store: AuthorizedStore) {
    }

    @observable.ref avatarFile: UploadFile|null = null;
    @action setAvatarFile = (file: UploadFile) => { this.avatarFile = file; }

    @action async init() {
        const {currentUser} = this.store;
        if (currentUser == null) { debugger; return; }
        this.user = currentUser.clone();
    }


    async save() {
        if (!!this.avatarFile) {
            const [err, url] = await uploadFile("POST", "/Image/Upload", this.avatarFile as any as File);
            if (err) return err;
            runInAction(() => {
                this.user.set_avatar(url);
                this.avatarFile = null;
            });
        }

        const err = await this.user.updateProfile();
        if (!err) {
            const store = this.store;
            // @ts-ignore
            store.currentUser = this.user;
            store.setLocale(this.user.language);
        }
        return err;
    }

    @observable oldPassword    : string = "";
    @observable newPassword    : string = "";
    @observable confirmPassword: string =  "";

    @action set_oldPassword     = (v:string) => { this.oldPassword     = v }
    @action set_newPassword     = (v:string) => { this.newPassword     = v }
    @action set_confirmPassword = (v:string) => { this.confirmPassword = v }
    changePassword() {
        return User.changePassword(this.oldPassword, this.newPassword);
    }


    @observable.ref emailNotificationSetting: UserNotificationSetting = new UserNotificationSetting({ type:ENotificationType.Email });
    @observable.ref smsNotificationSetting: UserNotificationSetting = new UserNotificationSetting({ type:ENotificationType.SMS });
    @observable.ref pwaPushNotificationSetting: UserNotificationSetting = new UserNotificationSetting({ type:ENotificationType.PwaPush });
    @action async initNotificationSettings() {
        const {currentUser} = this.store;
        if (currentUser == null) { debugger; return; }
        this.emailNotificationSetting   = new UserNotificationSetting({ userId: currentUser.userId, channelId:ENotificationType.Email  , sendType:ENotificationSendType.DontSend });
        this.smsNotificationSetting     = new UserNotificationSetting({ userId: currentUser.userId, channelId:ENotificationType.SMS    , sendType:ENotificationSendType.DontSend });
        this.pwaPushNotificationSetting = new UserNotificationSetting({ userId: currentUser.userId, channelId:ENotificationType.PwaPush, sendType:ENotificationSendType.DontSend });
        const [err, xs] = await UserNotificationSetting.fetch(currentUser.userId);
        if (!err) runInAction(() => {
            this.emailNotificationSetting   = xs.find(s => s.channelId == ENotificationType.Email  ) || this.emailNotificationSetting  ;
            this.smsNotificationSetting     = xs.find(s => s.channelId == ENotificationType.SMS    ) || this.smsNotificationSetting    ;
            this.pwaPushNotificationSetting = xs.find(s => s.channelId == ENotificationType.PwaPush) || this.pwaPushNotificationSetting;
        });
        return err;
    }
    async saveNotificationSettings() {
        const [err, xs] = await UserNotificationSetting.save([
            this.emailNotificationSetting,
            this.smsNotificationSetting,
            this.pwaPushNotificationSetting,
        ]);
        return err;
    }
    async onNotificationChange(setting: UserNotificationSetting) {
        setting.save();
    }

    @observable.ref sGoogleDriveStore: GoogleDriveStore = new GoogleDriveStore(this.store);
}
