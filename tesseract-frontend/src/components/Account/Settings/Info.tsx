import React, { useRef, FC, useState, useCallback, useEffect } from "react";
import { observer } from "mobx-react";
import { Trans } from "react-i18next";

import { AccountSettingStore } from "./AccountSettingStore";

import { Menu } from "antd";
import { ClickParam } from "antd/lib/menu";
const { Item } = Menu;

import { useComponentSize } from "../../../hooks/useComponentSize";
import { GridContent } from "../../PageHeaderWrapper/GridContent";
import { BaseView } from "./BaseView";
import { SecurityView } from "./SecurityView";
import { NotificationSettings } from "./NotificationSettings";

import styles from "./Info.module.less";
import { GoogleAccountView } from "./GoogleAccountView";

const menuMap = {
  base        : (<Trans i18nKey="app.settings.menuMap.basic" />),
  security    : (<Trans i18nKey="app.settings.menuMap.security" />),
  googleAccount : (<Trans i18nKey="app.settings.menuMap.googleAccount" />),
  // binding     : (<Trans i18nKey="app.settings.menuMap.binding" />),
  notification: (<Trans i18nKey="app.settings.menuMap.notification" />),
};

const childMap = {
  base        : BaseView,
  security    : SecurityView,
  googleAccount : GoogleAccountView,
  // binding     : (<Trans i18nKey="app.settings.menuMap.binding" />),
  notification: NotificationSettings,
};

export const Info: FC<{store:AccountSettingStore}> = observer(({store:sAccountSetting}) => {
  useEffect(() => {
    sAccountSetting.init();
  }, []);

  const [selectKey, set_selectKey] = useState("base");
  const onSelect = useCallback((param: ClickParam) => {
    set_selectKey(param.key);
  }, [set_selectKey]);

  const ref = useRef<HTMLDivElement>(null);
  const { width } = useComponentSize(ref);
  const mode =
    (400 < width && width < 641) ? "horizontal" : (
    (400 < width && window.innerWidth < 768) ? "horizontal" :
    "inline");
  const title = menuMap[selectKey];
  const C = childMap[selectKey];

  return (
    <GridContent>
      <div
        className={styles.main}
        ref={ref}
      >
        <div className={styles.leftmenu}>
          <Menu mode={mode} selectedKeys={[selectKey]} onClick={onSelect}>{
            Object.keys(menuMap).map(key => (
              <Item key={key}>{menuMap[key]}</Item>
            ))
          }</Menu>
        </div>
        <div className={styles.right}>
          <div className={styles.title}>{title}</div>
          <C store={sAccountSetting} />
        </div>
      </div>
    </GridContent>
  );
});
