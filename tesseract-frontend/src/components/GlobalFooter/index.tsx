import React, { ReactNode, CSSProperties, FunctionComponent } from 'react';
import { observer } from 'mobx-react';

import classNames from 'classnames';

import styles from './index.module.less';

export interface IFooterLink {
    key     ?: string;
    children : ReactNode;
    href     : string;
    target  ?: "_blank"|"_self";
}

interface IGlobalFooterProps {
    links       ?: Array<IFooterLink>;
    copyright?: ReactNode;

    className?: string;
    style?: CSSProperties;
}

export const GlobalFooter: FunctionComponent<IGlobalFooterProps> = observer(({ className, links, copyright }) => {
    return (
        <footer className={classNames(styles.globalFooter, className)}>
            {copyright && <div className={styles.copyright}>{copyright}</div>}
        </footer>
    );
});
