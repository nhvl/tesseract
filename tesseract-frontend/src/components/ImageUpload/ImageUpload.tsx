import React, { useCallback, useState, FC, useMemo } from 'react';
import { observer } from 'mobx-react';

import { Upload, Icon, Modal , } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';

interface IProps {
    imgUrl  : string,
    file    : UploadFile|null,
    onChange: (file: UploadFile|null) => void
}

export const ImageUpload: FC<IProps> = observer(({imgUrl, file, onChange}) => {
    const fileList = useMemo<UploadFile[]>(() => (
        file != null ? [file] : (
        !!imgUrl ? [({ uid: '-1', name: 'xxx.png', status: 'done', url: imgUrl }) as any] : [])
    ), [imgUrl, file]);
    const handleChange = useCallback(({ fileList }) => onChange(fileList[0]), [onChange]);
    const onRemove = useCallback((file: UploadFile) => {
        if (file === file) onChange(null);
    }, [onChange]);
    const beforeUpload = useCallback((file: UploadFile) => false, []);

    const [previewVisible, setPreviewVisible] = useState(false);
    const [previewImage, setpreviewImage] = useState("");
    const handleCancel = useCallback(() => setPreviewVisible(false), [setPreviewVisible]);
    const handlePreview = useCallback((file: UploadFile) => {
        setPreviewVisible(true);
        setpreviewImage(file.url || file.thumbUrl || "");
    }, [setPreviewVisible]);

    return (
        <div className="clearfix">
            <Upload
                listType="picture-card"
                fileList={fileList}
                onPreview={handlePreview}
                onRemove={onRemove}
                onChange={handleChange}
                beforeUpload={beforeUpload}
            >
                {fileList.length > 0 ? null : (
                    <div>
                        <Icon type="plus" />
                        <div className="ant-upload-text">Upload</div>
                    </div>
                )}
            </Upload>
            <Modal visible={previewVisible} footer={null} onCancel={handleCancel}>
                <img alt="example" style={{ width: '100%' }} src={previewImage} />
            </Modal>
        </div>
    );
});
