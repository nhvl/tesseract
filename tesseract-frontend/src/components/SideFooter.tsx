import React, { FC, CSSProperties } from 'react';
import { observer } from 'mobx-react';

import { Icon } from 'antd';

export const SideFooter: FC<{collapsed:boolean}> = observer(({collapsed}) => {
    return (<footer className="footer">
        {!collapsed && <span className="paddingRight-xs">Copyright</span>}
        <Icon type="copyright" />
        {!collapsed && <span className="paddingLeft-xs">2019 Tesseract</span>}
    </footer>);
})
