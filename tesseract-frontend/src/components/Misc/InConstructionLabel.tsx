import React, { FC } from "react";
import { observer } from "mobx-react";

export const InConstructionLabel: FC<{title: string}> = observer(({title}) => {
    return <div>{`🚧👷 ${title}`}</div>;
});
