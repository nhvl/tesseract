import React, { useCallback, FC, ChangeEvent } from "react";

import {Input as AntdInput, Checkbox as AntdCheckbox} from "antd"
import { InputProps, SearchProps, TextAreaProps } from "antd/lib/input";
import { CheckboxChangeEvent, CheckboxProps } from "antd/lib/checkbox";
const {Search: AntdSearch} = AntdInput;
const {TextArea: AntdTextArea} = AntdInput;

interface IInputProps extends Omit<InputProps, "onChange"> {
    onChange?: (v:string) => void
}

export const Input:FC<IInputProps> = (({onChange, ...props}) => {
    const handleChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if (onChange) onChange(event.currentTarget.value);
    }, [onChange]);

    return (<AntdInput onChange={handleChange} {...props} />);
});

interface ISearchProps extends Omit<SearchProps, "onChange"> {
    onChange?: (v:string) => void
}
export const Search:FC<ISearchProps> = (({onChange, ...props}) => {
    const handleChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        if (onChange) onChange(event.currentTarget.value);
    }, [onChange]);

    return (<AntdSearch onChange={handleChange} {...props} />);
});

export interface ITextAreaProps extends Omit<TextAreaProps, "onChange"> {
    onChange?: (v:string) => void
}
export const TextArea:FC<ITextAreaProps> = (({onChange, ...props}) => {
    const handleChange = useCallback((event: ChangeEvent<HTMLTextAreaElement>) => {
        if (onChange) onChange(event.currentTarget.value);
    }, [onChange]);

    return (<AntdTextArea onChange={handleChange} {...props} />);
});

interface ICheckboxProps extends Omit<CheckboxProps, "onChange"> {
    onChange?: (v:boolean) => void
}
export const Checkbox:FC<ICheckboxProps> = (({onChange, ...props}) => {
    const handleChange = useCallback((event: CheckboxChangeEvent) => {
        if (onChange) onChange(event.target.checked);
    }, [onChange]);

    return (<AntdCheckbox onChange={handleChange} {...props} />);
});
