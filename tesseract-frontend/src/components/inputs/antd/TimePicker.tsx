import React, { FC, useCallback, useMemo } from "react";
import { observer } from "mobx-react";
import moment, {Moment} from "moment";

import { TimePicker as AntdTimePicker } from "antd";
import { TimePickerProps } from "antd/lib/time-picker";

interface ITimePickerProps extends Omit<TimePickerProps, "value"|"onChange"> {
    value   : number|undefined,
    onChange: (v: number|undefined) => void;
}

export const TimePicker: FC<ITimePickerProps> = observer(({value, onChange, ...props}) => {
    const v = useMemo(() => value ? moment(value) : undefined, [value]);
    const changeHandler = useCallback((date: Moment) => onChange(date ? date.valueOf() : undefined), [onChange]);

    return (
        <AntdTimePicker
            {...props}
            value={v}
            onChange={changeHandler}
            />
    );
})
