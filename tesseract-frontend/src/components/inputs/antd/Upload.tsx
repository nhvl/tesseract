import React, { FC, useCallback } from 'react';
import { observer } from 'mobx-react';

import { Upload, Icon } from 'antd';
import { UploadChangeParam, DraggerProps } from 'antd/lib/upload';
import { UploadFile, UploadProps } from 'antd/lib/upload/interface';

import { toBase64 } from '../../../utils/file';

import styles from "./Upload.module.less";

const emptyFileList: UploadFile[] = [];
const beforeUpload = () => false;

export interface DumpUploadProps extends Omit<UploadProps, "onChange"> {
    onChange:(file:UploadFile) => void,
    accept? : string,
    createThumb?:boolean,
}

export const DumbUpload: FC<DumpUploadProps> = observer(({onChange, accept, createThumb, ...props}) => {
    const changeHandler = useCallback(async ({ file }:UploadChangeParam) => {
        if (!file.url && !file.thumbUrl && createThumb) {
            file.url = file.thumbUrl =
                //await toBase64(file.originFileObj || file as any);
                URL.createObjectURL(file);
        }
        onChange(file);
    }, [createThumb, onChange]);

    return (<Upload fileList={emptyFileList} showUploadList={false} accept={accept}
        onChange={changeHandler} beforeUpload={beforeUpload}
        {...props} />
    );
});

export const Upload2: FC<DumpUploadProps> = observer(({onChange, createThumb, ...props}) => {
    const changeHandler = useCallback(async (info:UploadChangeParam) => {
        const { file } = info;
        if (!file.url && !file.thumbUrl && createThumb) {
            file.url = file.thumbUrl =
                //await toBase64(file.originFileObj || file as any);
                URL.createObjectURL(file);
        }
        onChange(file);
    }, [createThumb, onChange]);

    return (
        <Upload onChange={changeHandler} beforeUpload={beforeUpload} {...props} />
    );
});

export interface Upload3Props extends Omit<UploadProps, "onChange"> {
    onChange:(file:UploadFile) => void,
    onRemove:(file:UploadFile) => void,
    createThumb?:boolean,
}

export const Upload3: FC<Upload3Props> = observer(({onChange, onRemove, createThumb, fileList, ...props}) => {
    const changeHandler = useCallback(async (info:UploadChangeParam) => {
        const { file } = info;
        if (!file.url && !file.thumbUrl && createThumb) {
            file.url = file.thumbUrl =
                //await toBase64(file.originFileObj || file as any);
                URL.createObjectURL(file);
        }
        onChange(file);
    }, [createThumb, onChange]);

    return (<>
        <Upload onChange={changeHandler} beforeUpload={beforeUpload} {...props} showUploadList={false} />
        <FileList fileList={fileList} onRemove={onRemove} />
    </>);
});

export const FileList: FC<{fileList?:UploadFile[], onRemove?:(file:UploadFile) => void}> = observer(({fileList, onRemove}) => {
    if (fileList == null || fileList.length < 1) return null;
    return (
        <div className="ant-upload-list ant-upload-list-text paddingTop-sm paddingBottom-sm">
            {fileList.map(attachment => (<FileItem key={attachment.uid} file={attachment} onRemove={onRemove} />))}
        </div>
    );
});

const FileItem: FC<{file:UploadFile, onRemove?:(file:UploadFile) => void}> = observer(({file, onRemove}) => {
    const removeHandler = useCallback(async () => { if (onRemove) onRemove(file); }, [onRemove, file]);

    return (
        <div className="ant-upload-list-item ant-upload-list-item-undefined">
            <div className="ant-upload-list-item-info">
                <span>
                    <Icon type={mimeType2Icon(file.type)} title={file.type} className={styles.fileIcon} />
                    <span className="ant-upload-list-item-name" title={file.fileName || file.name}>
                        {file.fileName || file.name}
                        {file.url && (<>
                        {" "}
                        <a href={file.url} target="_blank"><Icon type="download" className="icon-lg paddingLeft-xs" /></a>
                        </>)}
                    </span>
                </span>
            </div>
            {onRemove && (
                <Icon type="close" title="Remove file" tabIndex={-1} onClick={removeHandler} />
            )}
        </div>
    );
});

export interface IDumbDraggerProps extends Omit<DraggerProps, "onChange"> {
    onChange:(file:UploadFile) => void,
    createThumb?:boolean,
}
export const DumbDragger: FC<IDumbDraggerProps> = observer(({onChange, createThumb, ...props}) => {
    const changeHandler = useCallback(async ({ file }:UploadChangeParam) => {
        if (!file.url && !file.thumbUrl && !!createThumb) {
            const dataUrl = await toBase64(file.originFileObj || file as any);
            file.url = file.thumbUrl = dataUrl;
        }
        onChange(file);
    }, [createThumb, onChange]);

    return (<Upload.Dragger fileList={emptyFileList} showUploadList={false}
        onChange={changeHandler} beforeUpload={beforeUpload}
        {...props} />);
})


const mMimeType2Icon = {
    "image/jpeg": ("file-jpg"),
    "image/gif": ("file-jpg"),
    "image/png": ("file-jpg"),
    "image/svg+xml": ("file-jpg"),
    "image/tiff": ("file-jpg"),
    "image/webp": ("file-jpg"),

    "application/rtf": ("file-word"),
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document": ("file-word"),
    "application/vnd.oasis.opendocument.text": ("file-word"),
    "application/msword": ("file-word"),

    "text/csv": ("file-excel"),
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": ("file-excel"),
    "application/vnd.oasis.opendocument.spreadsheet": ("file-excel"),
    "application/vnd.ms-excel": ("file-excel"),

    "application/vnd.openxmlformats-officedocument.presentationml.presentation": ("file-ppt"),
    "application/vnd.oasis.opendocument.presentation": ("file-ppt"),
    "application/vnd.ms-powerpoint": ("file-ppt"),

    "text/plain": ("file-text"),

    "application/pdf": ("file-pdf"),

    "video/mpeg": ("video-camera"),
    "video/webm": ("video-camera"),
};

function mimeType2Icon(mime:string): string {
    const icon = mMimeType2Icon[mime];
    if (icon != null) return icon;
    if (mime.startsWith("image/")) return ("file-jpg");
    if (mime.startsWith("video/")) return ("video-camera");
    return ("file");
}
