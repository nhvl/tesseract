import React, { MouseEvent, cloneElement } from "react";

import { Menu } from "antd";
const { SubMenu } = Menu;

export function subMenu({collapsed, key, subMenuTitle, menuItems, subMenuClassName}: {collapsed:boolean, key:string, subMenuTitle:JSX.Element, menuItems:JSX.Element[], subMenuClassName?:string}) {
    return (
        collapsed ? ([
            <SubMenu className={subMenuClassName} key={key}
                onTitleClick={subMenuOnClickGoToLink}
                title={subMenuTitle}>
                {menuItems}
            </SubMenu>
        ]) : ([
            <Menu.Item className={subMenuClassName} key={key}>
                {subMenuTitle}
            </Menu.Item>,
        ].concat(
            menuItems.map(menuItem => cloneElement(menuItem, {
                className: (menuItem.props.className || "") + " level-2",
                // level: 2,
            }))
        ))
    );
}

function subMenuOnClickGoToLink({domEvent}:{key: string, domEvent: MouseEvent<HTMLElement>}) {
    const a = domEvent.currentTarget.querySelector<HTMLAnchorElement>("a");
    if (a != null && !!a.href) a.click()
}
