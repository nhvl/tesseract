import React, { useCallback, FC } from "react";

import {InputNumber as AntdInputNumber} from "antd"
import { InputNumberProps } from "antd/lib/input-number";

export interface IInputNumberProps extends Omit<InputNumberProps, "onChange"> {
    onChange: (v:number) => void
}

export const InputNumber:FC<IInputNumberProps> = (({onChange, ...props}) => {
    const handleChange = useCallback((v:number|undefined) => {
        onChange(v as number);
    }, [onChange]);

    return (<AntdInputNumber type="number" onChange={handleChange} {...props} />);
});
