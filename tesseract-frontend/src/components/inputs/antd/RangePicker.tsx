import React, { FC, useCallback, useMemo } from "react";
import { observer } from "mobx-react";
import moment, {Moment} from "moment";

import { DatePicker } from "antd";
const { RangePicker: AntdRangePicker } = DatePicker;
import { RangePickerProps, RangePickerValue } from "antd/lib/date-picker/interface";

interface IRangePickerProps extends Omit<RangePickerProps, "value"|"onChange"> {
    from        ?: number,
    to          ?: number,
    onChangeFrom : (v: number|undefined) => void,
    onChangeTo   : (v: number|undefined) => void,
}

export const RangePicker : FC<IRangePickerProps> = observer(({from, to, onChangeFrom, onChangeTo, ...props}) => {
    const mFrom = useMemo(() => from ? moment(from) : undefined, [from]);
    const mTo = useMemo(() => to ? moment(to) : undefined, [to]);
    const value = useMemo<RangePickerValue>(() => [mFrom, mTo], [mFrom, mTo]);
    const changeHandler = useCallback(([mFrom, mTo]: Moment[]) => {
        const vFrom = mFrom ? mFrom.valueOf() : undefined;
        const vTo = mTo ? mTo.valueOf() : undefined;
        if (from !== vFrom) onChangeFrom(vFrom);
        if (to !== vTo) onChangeTo(vTo);
    }, [onChangeFrom, onChangeTo]);

    return (
        <AntdRangePicker
            {...props}
            value={value}
            onChange={changeHandler}
            />
    );
})
