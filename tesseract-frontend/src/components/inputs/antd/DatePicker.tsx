import React, { FC, useCallback, useMemo } from "react";
import { observer } from "mobx-react";
import moment, {Moment} from "moment";

import { DatePicker as AntdDatePicker } from "antd";
import { DatePickerProps } from "antd/lib/date-picker/interface";

interface IDatePickerProps extends Omit<DatePickerProps, "value"|"onChange"> {
    value   : number|undefined,
    onChange: (v: number|undefined) => void;
}

export const DatePicker: FC<IDatePickerProps> = observer(({value, onChange, ...props}) => {
    const v = useMemo(() => value ? moment(value) : undefined, [value]);
    const changeHandler = useCallback((date: Moment) => onChange(date ? date.valueOf() : undefined), [onChange]);

    return (
        <AntdDatePicker
            {...props}
            value={v}
            onChange={changeHandler}
            />
    );
})
