import React, { FC, useCallback, useRef } from "react";
import { observer } from "mobx-react";

import ReactQuill from "react-quill";

export const HtmlInput: FC<{
    value       : string,
    onChange    : (v: string) => void,
    onBlur     ?: () => void,
    placeholder?: string,
    theme      ?: "bubble",
    className  ?: string,
}> = observer(({ value, onChange, onBlur, children, ...props }) => {
    const rEditor = useRef<ReactQuill>(null);
    const blurHandler = useCallback(() => {
        if (onBlur != null) onBlur();
    }, [onBlur]);

    return (
        <ReactQuill
            value={value} onChange={onChange}
            ref={rEditor}
            onBlur={blurHandler}
            {...props}
        />
    );
});
