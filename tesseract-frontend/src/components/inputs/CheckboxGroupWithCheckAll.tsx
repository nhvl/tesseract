import React, { FC, ReactNode, CSSProperties } from 'react';
import { observer } from 'mobx-react';

import { Row } from 'antd';

const style: CSSProperties = { borderBottom: '1px solid #E9E9E9' };
export const CheckboxGroupWithCheckAll: FC<{checkAll:ReactNode}> = observer(({checkAll, children}) => {
    return (
        <div>
            <div style={style}>
                {checkAll}
            </div>
            <Row>{children}</Row>
        </div>
    )
});
