import React, { FC, useState, useCallback, useRef } from "react";
import { observer } from "mobx-react";

import { Button, Icon, Modal } from "antd";

import { ReactMic } from "../ReactMic";
import { useComponentSize } from "../../hooks/useComponentSize";
import {blob2File} from "../../utils/file"

export const AudioRecordButton:FC<{onChange:(file:File) => void}> = observer(({onChange, children}) => {
    const [recording, setRecording] = useState(false);
    const [pause, setPause] = useState(false);
    const rResolveWhenComplete = useRef<(file?:File) => void|undefined>();
    const rCanvasWrap = useRef<HTMLDivElement>(null);
    const {width} = useComponentSize(rCanvasWrap);

    const startRecord = useCallback(() => {
        setRecording(true);
        setPause(false);
    }, [setRecording]);
    const cancelRecord = useCallback(() => {
        setRecording(false);
    }, [setRecording]);
    const togglePause = useCallback(() => {
        setPause(pause => !pause);
    }, [setPause]);

    const onOk = useCallback(() => {
        setRecording(false);
        if (rResolveWhenComplete.current) rResolveWhenComplete.current(undefined);
        new Promise<File|undefined>(resolve => rResolveWhenComplete.current = resolve).then(file => {
            if (file) onChange(file);
        });
    }, [onChange]);

    const onSave = useCallback((blob: Blob) => {
        if (rResolveWhenComplete.current) rResolveWhenComplete.current(blob2File(blob))
    }, [onChange]);

    return (<>
        <Button onClick={startRecord}>{children}</Button>
        <Modal visible={recording}
            onOk={onOk}
            onCancel={cancelRecord}
            >
            <div ref={rCanvasWrap} className="w-full">
                <div><ReactMic
                    width={width || 480}
                    record={recording}
                    pause={pause}
                    onSave={onSave}
                    /></div>
                <Button onClick={togglePause}><Icon type={!pause ? "pause" : "audio"} /></Button>
            </div>
        </Modal>
    </>);
});
