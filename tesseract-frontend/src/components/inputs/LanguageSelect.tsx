import React, { FC } from "react";
import { observer } from "mobx-react";

import { useBaseStore } from '../../stores/useBaseStore';

import { Select } from "antd";
import { SelectProps } from "antd/lib/select";

import { languageIcons, languageLabels } from "../../utils/i18nConfig";

export const LanguageSelect: FC<SelectProps<string>> = observer((props) => {
    const { locales } = useBaseStore();
    return (
        <Select {...props}>{locales.map(locale => (
            <Select.Option key={locale} value={locale} lang={locale}>
                <img src={languageIcons[locale]} /> {languageLabels[locale]}
            </Select.Option>
        ))}</Select>
    );
});
