import React, { FC, ReactElement, Component, CSSProperties } from 'react';
import { observer } from 'mobx-react';

import RpnInput from 'react-phone-number-input';

type FlagsMap = { [countryCode: string]: Component<object, object> };

interface CountrySelectComponentProps {
    name     ?: string; // HTML name attribute
    value    ?: string; //The currently selected country code.
    onChange ?: (value : string) => void; //Updates the value.
    onFocus  ?: () => void; //Is used to toggle the --focus CSS class.
    onBlur   ?: () => void; //Is used to toggle the --focus CSS class.
    options  ?: Array<{ value?: string, label: string, icon: Component }>; //The list of all selectable countries (including "International") each being an object of shape.
    disabled ?: boolean; //HTML disabled attribute.
    tabIndex ?: (number|string); //HTML tabIndex attribute.
    className?: string; //CSS class name.
}
interface InputComponentProps {
    value         : string; // The parsed phone number. E.g.: "", "+", "+123", "123".
    onChange      : (value : string) => void; // Updates the value.
    onFocus      ?: () => void; // Is used to toggle the --focus CSS class.
    onBlur       ?: () => void; // Is used to toggle the --focus CSS class.
    country      ?: string; // The currently selected country. undefined means "International" (no country selected).
    metadata     ?: object; // libphonenumber-js metadata.
}

interface IPhoneInputProps {
    value                            : string;
    onChange                         : (value: string) => void;
    //
    autoComplete                    ?: string;
    className                       ?: string;
    country                         ?: string;
    countries                       ?: string[];
    countryOptions                  ?: string[];
    countrySelectComponent          ?: Component<CountrySelectComponentProps, object>;
    countrySelectTabIndex           ?: number;
    disabled                        ?: boolean;
    displayInitialValueAsLocalNumber?: boolean;
    error                           ?: string;
    ext                             ?: ReactElement,
    flagComponent                   ?: Component<{ country: string, flagsPath: string, flags: FlagsMap }, object>;
    flags                           ?: FlagsMap;
    flagsPath                       ?: string;
    getInputClassName               ?: (params: { disable ?: boolean, invalid ?: boolean }) => string;
    inputComponent                  ?: Component<InputComponentProps, object>;
    international                   ?: boolean;
    internationalIcon               ?: Component<object, object>;
    inputClassName                  ?: string;
    labels                          ?: { [key: string]: string };
    limitMaxLength                  ?: boolean;
    metadata                        ?: object;
    placeholder                     ?: string;
    onCountryChange                 ?: (countryCode?: string) => void;
    showCountrySelect               ?: boolean;
    style                           ?: CSSProperties;
}

export const PhoneInput: FC<IPhoneInputProps> = observer((props) => {
    return (<RpnInput {...props} />)
})
