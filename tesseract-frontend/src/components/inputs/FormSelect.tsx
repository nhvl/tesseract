import React, { useState, useCallback, FC, ChangeEvent, useEffect } from "react";

import {Select, Form} from "antd";
import { SelectProps, SelectValue } from "antd/lib/select";

interface ISelectProps extends SelectProps {
    onChange: (v:any) => void,
    onValid: (f:(vs:any) => any) => void,
    label: string,
    name:string,
    required?:boolean,
    SelectComponent: FC<any>
}


export const FormSelect:FC<ISelectProps> = (({
    label, value, name,
    onChange, onBlur, onValid,
    required,
    SelectComponent, ...props}) => {
    const [isDirty,setDirty] = useState(false);
    let dirty = false;
    let formValidation:any = "";

    function validate(v:any) {
        return ((isDirty && required && !v) ? "error" : "");
    }

    const help = (validate(value) == "error")
                    ? label +" is required."
                    : "";
    useEffect(() => {
        if (onValid) {
            onValid(vs => ({...vs, [name]:help }))
        }
    }, [value, isDirty]);
    const handleBlur = useCallback((event: SelectValue) => {
        setDirty(true);
        if (onBlur) onBlur(event);
        if (onValid) {
            onValid(vs => ({...vs, [name]:help }))
        }

    }, [isDirty]);

    const handleChange = useCallback((event: React.FormEvent<HTMLSelectElement>) => {
        setDirty(true);
        if (onChange) onChange(event);
        if (onValid) {
            onValid(vs => ({...vs, [name]:help}))
        }

    }, [value, isDirty]);

    const Component = SelectComponent == null ? Select : SelectComponent;

    return (
    <Form.Item label={label}
        help={help}
        validateStatus={validate(value)}
        >
        <Component value={value}
            onBlur={handleBlur}
            onChange={handleChange}
            {...props} />
    </Form.Item>
    );
})
