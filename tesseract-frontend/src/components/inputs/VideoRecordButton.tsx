import React, { FC, useState, useCallback, useRef } from "react";
import { observer } from "mobx-react";

import { Button, Modal } from "antd";

import VideoRecorder from "react-video-recorder";
import {blob2File} from "../../utils/file"

interface IRecordedData {
    videoBlob:File, startedAt:number, thumbnailBlob:File, duration:number
}

export const VideoRecordButton:FC<{onChange:(file:File) => void}> = observer(({onChange, children}) => {
    const rVideoRecorder = useRef<VideoRecorder>(null);
    const [recording, setRecording] = useState(false);
    const rRecorded = useRef<IRecordedData|undefined>();
    const rResolveWhenComplete = useRef<(completed:boolean) => void|undefined>();

    const startRecord = useCallback(() => {
        setRecording(true);
        rRecorded.current = undefined;
        if (rResolveWhenComplete.current) rResolveWhenComplete.current(false);
        rResolveWhenComplete.current = undefined;
    }, [setRecording]);
    const cancelRecord = useCallback(() => {
        const videoRecorder = rVideoRecorder.current;
        if (videoRecorder != null && videoRecorder.state.isRecording) {
            videoRecorder.handleStopRecording();
        }
        setRecording(false);
    }, [setRecording]);

    const doneRecord = useCallback(() => {
        const videoRecorder = rVideoRecorder.current;
        if (videoRecorder == null) return;
        if (!videoRecorder.state.isRecording && rRecorded.current != null) onChange(rRecorded.current.videoBlob);
        else {
            if (rResolveWhenComplete.current) rResolveWhenComplete.current(false);
            (new Promise<boolean>(resolve => rResolveWhenComplete.current = resolve)).then((completed) => {
                if (completed && rRecorded.current != null) onChange(blob2File(rRecorded.current.videoBlob));
            });
        }
        if (videoRecorder.state.isRecording) {
            videoRecorder.handleStopRecording();
        }
        setRecording(false);
    }, [onChange]);

    const onRecordingComplete = useCallback((videoBlob:Blob, startedAt:number, thumbnailBlob:Blob, duration:number) => {
        var videoFile = blob2File(videoBlob);
        var thumbnailFile = blob2File(thumbnailBlob);
        rRecorded.current = ({videoBlob: videoFile as File, startedAt, thumbnailBlob:thumbnailFile as File, duration});

        if (rResolveWhenComplete.current) rResolveWhenComplete.current(true);
    }, []);

    return (<>
        <Button onClick={startRecord}>{children}</Button>
        <Modal visible={recording}
            onOk={doneRecord}
            onCancel={cancelRecord}
            >
            {recording && (<VideoRecorder
                ref={rVideoRecorder}
                countdownTime={0}
                isOnInitially

                onRecordingComplete={onRecordingComplete}
                />)}
        </Modal>
    </>);
});
