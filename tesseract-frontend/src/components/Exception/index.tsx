import React, { FC, memo, ReactNode } from 'react';
import classNames from 'classnames';
import config from './typeConfig';
import styles from './index.module.less';

interface IExceptionProps {
  className   ?: string;
  type         : string|number;
  title       ?: ReactNode;
  desc        ?: ReactNode;
  img         ?: string;
  actions     ?: ReactNode;
}

export const Exception: FC<IExceptionProps> = memo(({
  className,
  type,
  title,
  desc,
  img,
  actions,
  ...rest
}) => {
    const pageType = type in config ? type : '404';
    const clsString = classNames(styles.exception, className);

    return (
      <div className={clsString} {...rest}>
        <div className={styles.imgBlock}>
          <div
            className={styles.imgEle}
            style={{ backgroundImage: `url(${img || config[pageType].img})` }}
          />
        </div>
        <div className={styles.content}>
          <h1>{title || config[pageType].title}</h1>
          <div className={styles.desc}>{desc || config[pageType].desc}</div>
          <div className={styles.actions}>
            {actions}
          </div>
        </div>
      </div>
    );
  }
);
