import { FC } from "react";
import { observer } from "mobx-react";


import { useAuthorizedStore } from "../stores/useAuthorizedStore";
import { EPermission } from "../models/types";

export const PermissionGuard: FC<{permissions:EPermission[]}> = observer(({permissions, children}) => {
    const store = useAuthorizedStore();

    const hasPermission = store.currentUser && permissions.every(p => store.currentUser.permissions.includes(p))

    return (hasPermission ? (children as any) : null);
});
