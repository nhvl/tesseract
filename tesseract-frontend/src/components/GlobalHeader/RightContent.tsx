import React, {FC, useCallback} from "react";
import { observer } from "mobx-react";

import { Icon, Tooltip, } from "antd";

import { SelectLang } from "../../components/SelectLang";

import {theme, switchTheme} from "../../theme";

import styles from "./index.module.less";

export const RightContent: FC<{}> = observer(() => {
    const onSwitchTheme = useCallback(() => {
        switchTheme(theme == "dark" ? "light" : "dark");
    }, []);

    return (
        <div className={styles.right}>
            <Tooltip title="High Contrast">
                <a onClick={onSwitchTheme}>
                    <Icon type="bulb" theme={theme == "dark" ? "filled" : undefined} className="icon-lg bulb-color" />
                </a>
            </Tooltip>
            <SelectLang className={styles.action} />
        </div>
    );
});
