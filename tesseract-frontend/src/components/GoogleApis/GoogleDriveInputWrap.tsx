import React, { FC, useState, useCallback, useEffect } from "react";
import { observer } from "mobx-react";
import { useTranslation } from "react-i18next";
import { ActItem, EEmbedAppType } from "../../models/ActDoc";
import { GoogleDriveStore } from "../../stores/GoogleDriveStore";
import { Modal } from "../Modal/ModalAntd";
import { showError } from "../../services/api/ShowError";
import { message, Input, Icon } from "antd";
import { GoogleFilePicker } from "./GoogleFilePicker";
import { HtmlInput } from "../inputs/HtmlInput";
import { GoogleDocItem } from "../../models/GoogleDrive";


export const GoogleDriveInputWrap: FC<{ gStore: GoogleDriveStore,
    item: ActItem }> = observer(({ gStore, item }) => {
    const { t } = useTranslation();
    const [, setEditting] = useState(false);
    const endEdit = useCallback(() => setEditting(false), [setEditting]);
    
    useEffect(() => {
        if(item.embedId){
            gStore.getFilePermissionInfo(item.embedId).then(err => {
                if(err){
                    showError(err, t);
                }
            });
        }
    }, [item, gStore.accessToken]);

    const onClearGoogleMediaFile = useCallback(() => {
        item.set_embedLink("");
        item.set_googleFile(undefined);
        item.clearEmbedContent();
    }, [item]);

    const onGoogleFileChanged = useCallback(async (pickedData: GoogleDocItem | undefined) => {
        if(pickedData){
            const checkAllowed = await gStore.checkAllowedSizeBytes(pickedData);
            if(!checkAllowed)
                Modal.error({content: t("app.activities.actDoc.fileIsTooLarge", { maxSize: gStore.maxSizeMB })})
            else{
                gStore.checkSupportedFile(pickedData).then(result => {
                    if(!result) message.warning(t('app.activities.actDoc.unsupportedFile', {fileName: pickedData.name}));
                    else{
                        Modal.confirm({
                            title:t('app.presentation.embedInput.permissionConfirmation'),
                            content: t('app.presentation.embedInput.confirmAllowSharingContent', {fileName: pickedData.name}),
                            onOk() {
                                gStore.shareFile(pickedData.id).then(err =>{
                                    if(err)
                                        showError(err,t);
                                    else{
                                        message.success(t('app.presentation.embedInput.theFileHasBeenSharedWithAnyone'));
                                        item.set_googleFile(pickedData);
                                    }
                                });
                            },
                            onCancel(){
                            },
                        });
                    }
                });
            }
        }
    }, [item]);

    const onSharedFile = useCallback(() => {
        if(item.embedId)
        gStore.shareFile(item.embedId).then(err => {
                if(err)
                    Modal.error({content : err.message});
                else{
                    message.success(t('app.presentation.embedInput.theFileHasBeenSharedWithAnyone'));
                }
            });
            
    }, [item]);
    
    return (<>
        <div className="paddingBottom-lg">
            {(item.embedId && item.embedApp == EEmbedAppType.GoogleDrive ?
                (<div className="paddingBottom-md">
                    <iframe width="100%" height="600px" src={item.googleFile ? item.googleFile.embedUrl :  
                        `https://drive.google.com/file/d/${item.embedId}/preview?usp=drive_web`}>
                    </iframe>
                    {(gStore.isAuthorized &&
                        (!gStore.filePermission && 
                            <>
                                <div>
                                    {t('app.presentation.previewEmbed.fileIsNotShared')}, 
                                    <a onClick={onSharedFile}>
                                        {t('app.presentation.previewEmbed.clickToShare')}
                                    </a>
                                </div>
                        </>)
                    )}
                </div>) :
                (item.embedLink || item.googleFile) && (
                <div className="paddingBottom-md">
                    <iframe width="100%" height="600px" src={item.googleFile ? item.googleFile.embedUrl :  
                           `https://docs.google.com/viewer?url=${item.embedLink}&embedded=true`}>
                    </iframe>
                </div>)
            )
            }
            {(item.embedId || item.embedLink || item.googleFile)&& 
                (<div className="paddingBottom-sm">
                    <a onClick={onClearGoogleMediaFile} title="Clear media"><Icon type="delete" className="icon-sm" /></a>
                    <span className="paddingLeft-xs">{item.googleFile && item.googleFile.name}</span>
                </div>)
            }
            <GoogleFilePicker gStore={gStore} onChange={(pickedData)=>onGoogleFileChanged(pickedData)}>
                {t('app.presentation.mediaInput.pickGoogleFile')}
            </GoogleFilePicker>
            <div className="paddingBottom-sm">
                <Input value={item.embedLink}
                onBlur={endEdit}
                disabled
                type="url"
                placeholder={t("form.activity.url.placeholder")} />
            </div>
            <HtmlInput value={item.content} onChange={item.set_content}
            onBlur={endEdit} placeholder={t('form.activity.description.placeholder')} />
        </div>
    </>);
});
