import React, { FC, useEffect } from 'react';
import { observer } from "mobx-react";
import { useTranslation } from 'react-i18next';

import { GoogleLogin } from 'react-google-login';
import { message, Button, Tag, Icon, } from 'antd';
import { GoogleDriveStore, AuthenFailed, AuthenErrorCode } from '../../stores/GoogleDriveStore';

import styles from "./GoogleAuthentication.module.less";
import { Modal } from '../Modal/ModalAntd';


export const GoogleAuthentication: FC<{store:GoogleDriveStore}> = observer(({store:sGoogleDriveStore}) => {
  const {t} = useTranslation();
  const onSuccess = (response: any) => {
    if(response) {
      sGoogleDriveStore.offlineCode = response.code;
      sGoogleDriveStore.exchangeAuthorizationCode().then(err => {
        if(err) Modal.error({content: err.message});
        else{
          message.success(t("app.settings.google.authenticationSucces"));
        }
      });
    }
  }

  const onLogout = () => {
    sGoogleDriveStore.revokeToken().then(err => {
      if(err) Modal.error({content: err.message});
      else{
        message.warning(t("app.settings.google.deauthenticationSucces"));
      }
    });
  }

  const onFailed = (response: AuthenFailed) => {
    console.log('Authentication Failed', response);
    if(response) {
      if(response.error == AuthenErrorCode.popup_closed_by_user){
        message.warning(t("app.settings.google.cancelAuthentication"));
      }
      else if(response.error == AuthenErrorCode.access_denied){
        message.warning(t("app.settings.google.accessDenied"));
      }
      else if(response.error == AuthenErrorCode.immediate_failed){
        Modal.warning({content: t("app.settings.google.immediateFailed")});
      }
      else if(response.error == AuthenErrorCode.idpiframe_initialization_failed){
        Modal.warning({content: t("app.settings.google.idpiframe_initialization_failed")});
      }
      else {
        Modal.error({content: t("app.settings.google.systemError")});
      }
    }
  }

  return (<>
        {sGoogleDriveStore.clientInfo  &&
            (<>

          {sGoogleDriveStore.accessToken && sGoogleDriveStore.accessToken.length > 0 ? 
              <div className={`${styles.sectionGoogle} flex items-center`}>
                <a className={styles.googleDrive} onClick={onLogout}>
                  <img src="https://freelogovector.net/wp-content/uploads/logo-images-15/google-favicon-logo-vector-91917.png" alt={t("app.presentation.addBtn.googleAccount")} />
                </a>
                <div>
                  <div className={styles.desGoogle}>
                    <p className="font-size-lg font-bold text-black mb-xs">{t("app.presentation.addBtn.googleAccount")}</p>
                    <span>{t('app.settings.google.descriptionGoogle')}</span>
                  </div>
                  <Button onClick={onLogout} type="link" className="-m-xs">{t('app.settings.google.disconnect')}</Button>
                </div>
                <div className={`${styles.statusConnect}`}>
                    <Tag className="success">{t('app.settings.google.connectstatus')}</Tag>
                    <Icon type="check-circle" theme="filled" />
                </div>
              </div>
            :
            <GoogleLogin
              clientId={sGoogleDriveStore.clientInfo.clientId}
              onSuccess={onSuccess}
              onFailure={onFailed}
              cookiePolicy={'none'}
              responseType={'code'}
              accessType={'offline'}
              prompt={'consent'}
              render={renderProps => (<>
                <div className={`${styles.sectionGoogle} flex items-center`}>
                  <a className={styles.googleDrive} onClick={renderProps.onClick}>
                    <img src="https://freelogovector.net/wp-content/uploads/logo-images-15/google-favicon-logo-vector-91917.png" alt={t("app.presentation.addBtn.googleAccount")} />
                  </a>
                  <div>
                    <div className={styles.desGoogle}>
                      <p className="font-size-lg font-bold text-black mb-xs">{t("app.presentation.addBtn.googleAccount")}</p>
                      <span>{t('app.settings.google.descriptionGoogle')}</span>
                    </div>
                    <Button onClick={renderProps.onClick} disabled={renderProps.disabled} type="link" className="-m-xs">{t('app.settings.google.authentication')}</Button>
                  </div>
                  <div className={`${styles.statusConnect}  ${styles.statusDisconnect}`}>
                    <Tag>{t('app.settings.google.disconnectstatus')}</Tag>
                    <Icon type="check-circle" theme="filled" />
                  </div>
                </div>
              </>)}
              scope={sGoogleDriveStore.scope}
          />
          }
          </>)
        }
    </>);
});

