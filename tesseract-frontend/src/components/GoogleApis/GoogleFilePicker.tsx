import React, { FC, useCallback } from "react";
import { observer } from "mobx-react";

import GooglePicker from './GooglePicker';
import { Button } from "antd";
import { GoogleDocItem } from "../../models/GoogleDrive";
import { GoogleDriveStore } from "../../stores/GoogleDriveStore";
import { GoogleAuthentication } from "./GoogleAuthentication";

export const GoogleFilePicker:FC<{gStore: GoogleDriveStore,
    onChange:(fileInfo: GoogleDocItem | undefined) => void}> = observer((
        {   onChange,
            gStore: sGoogleDriveStore, 
            children}) => {
    
    const googlePickerOnchange = useCallback((data: any) => {
        if(data.action == "picked")
        {
            if(data.docs.length > 0){
                if (data.docs[0]){
                    const doc: GoogleDocItem = data.docs[0];
                    onChange(doc);
                }
            }
        }
    }, [onChange]);

    const onAuthorizeFailed = useCallback((data: any) => {
        if(data.docs.length > 0){
        }
    }, [onChange]);

    return (<>
        <div className="paddingBottom-sm">
        {sGoogleDriveStore.clientInfo ?
            (sGoogleDriveStore.isAuthorized ?
            <Button>
                <GooglePicker 
                
                developerKey={sGoogleDriveStore.clientInfo.developerKey}
                clientId={sGoogleDriveStore.clientInfo.clientId}
                accessToken={sGoogleDriveStore.accessToken}
                onChange={googlePickerOnchange}
                onAuthFailed={onAuthorizeFailed}
                multiselect={false}
                navHidden={true}
                // origin={sGoogleDriveStore.clientInfo.redirectUri}
                // authImmediate={true}
                viewId={'DOCS'}
                // createPicker={ (google, oauthToken) => {
                //     const googleViewId = google.picker.ViewId.DOCS;
                //     const docsView = new google.picker.DocsView(googleViewId)
                //         .setIncludeFolders(true)
                //         .setSelectFolderEnabled(true);
                    
                //     const docsUpload = new google.picker.DocsUploadView()
                //         .setIncludeFolders(true);

                //     const picker = new google.picker.PickerBuilder()
                //         .addView(docsView)
                //         .addView(docsUpload)
                //         .setOAuthToken(oauthToken)
                //         .setDeveloperKey(props.developerKey)
                //         .setCallback(()=>{
                //           console.log('Custom picker is ready!');
                //         });

                //     picker.build().setVisible(true);
                // }}
                >
                    {children}
                </GooglePicker>
            </Button>
            : <GoogleAuthentication store={sGoogleDriveStore}/>)
        : ""}
    </div>
    </>);
});
