import React, { FC, useMemo, useCallback, HTMLAttributes, Children } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { Table, Icon } from 'antd';
import { ColumnProps, TableComponents, TableProps } from 'antd/lib/table';

import { DragDropContext, Draggable, Droppable, DragDropContextProps } from 'react-beautiful-dnd';

const BodyWrapper: FC<HTMLAttributes<HTMLTableSectionElement>> = observer(({
    className,
    children,
    ...props
}) => {
    return (
        <Droppable droppableId="droppable">{(provided, snapshot) => (
            <tbody ref={provided.innerRef} {...provided.droppableProps}
                className={`${className} ${snapshot.isDraggingOver ? "isDraggingOver" : ""}`}
                {...props}>
                {children}
                {provided.placeholder}
            </tbody>
        )}</Droppable>
    );
});

const ClassRow: FC<HTMLAttributes<HTMLTableRowElement>&{index:number, "data-row-key":string}> = observer(({
    style,
    className,
    index,
    ["data-row-key"]: dataRowKey,
    children,
    ...props
}) => {
    return (
        <Draggable draggableId={dataRowKey} index={index}>{(provided, snapshot) => (
            <tr ref={provided.innerRef}
                {...provided.draggableProps}
                className={`${className} ${snapshot.isDragging ? "isDragging" : ""}`}
                style={{...style, ...provided.draggableProps.style}}
                {...props}
            >
                <td className="ant-table-column-has-actions">
                    <div {...provided.dragHandleProps} className="cursor-move text-grey"><i className="fas fa-grip-vertical"></i></div>
                </td>
                {Children.toArray(children).slice(1)}
            </tr>
        )}</Draggable>
    );
});

const components: TableComponents = {
    body: {
        wrapper: BodyWrapper,
        row    : ClassRow,
    },
};

interface IDndProps<T> extends TableProps<T> {
    onDragEnd(sourceIdex:number, destinationIndex:number): void;
}

export const DndTable: FC<IDndProps<any>> = observer(({columns, onDragEnd, ...props}) => {
    const {t} = useTranslation();

    const dragEndHandler:DragDropContextProps["onDragEnd"] = useCallback((result) => {
        if (!result.destination) return;
        onDragEnd(result.source.index, result.destination.index);
    }, []);

    const dndColumns = useMemo<Array<ColumnProps<any>>>(() => [
        { dataIndex: 'sortIndex', key: 'sortIndex', title: '' },
        ...(columns ? columns : []),
    ], [t]);

    return (
        <DragDropContext onDragEnd={dragEndHandler}>
            <Table columns={dndColumns}
                components={components}
                onRow={(record, index) => ({ index })}
                {...props}
                />
        </DragDropContext>
    );
});
