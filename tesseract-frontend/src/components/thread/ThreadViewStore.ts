import { createContext, useContext } from "react";
import { observable, action } from "mobx";

import { DbIdentity } from "../../models/types";

import {CommentStore} from "../../stores/CommentStore";

export class ThreadViewStore {
    constructor(private sComment: CommentStore) {

    }

    @observable replingCommentId ?: DbIdentity;
    @action beginReply = (commentId?:DbIdentity) => {
        this.replingCommentId = commentId;
    }
    @action cancelReply = () => {
        this.replingCommentId = undefined;
    }

    // static context = createContext<ThreadViewStore>(undefined!);
    // static useContext() {
    //     return useContext(this.context);
    // }
}
