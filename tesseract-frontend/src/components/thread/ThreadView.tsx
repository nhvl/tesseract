import React, { FC, useCallback, useState, useMemo, FormEvent, createContext, useContext } from "react";
import { observer, useLocalStore } from "mobx-react";

import { DbIdentity } from "../../models/types";
import { IUser, User } from "../../models/User";
import {Comment} from "../../models/Comment";
import { CommentSeen } from "../../models/CommentSeen";

import { UserStore } from "../../stores/UserStore";
import { CommentStore } from "../../stores/CommentStore";

import { withThreadViewStore, useThreadViewStore } from "./ThreadViewProvider";

import { Button, Comment as AntdComment, Avatar, Form } from "antd";
import { useTranslation } from "react-i18next";
import { TextAreaProps } from "antd/lib/input";

import { DateFromNowToolTip } from "../../components/DateTime";
import { TextArea } from "../../components/inputs/antd/Input";
import classNames from "classnames";
import { Modal } from "../Modal/ModalAntd";

const currentUserContext = createContext<User|undefined>(undefined);
function useCurrentUser() {
    return useContext(currentUserContext);
}

export const ThreadView = withThreadViewStore<{
    threadId     : string,
    readOnly    ?: boolean,
    currentUser ?: User,
    commentSeen ?: CommentSeen,
}>(observer(({threadId, readOnly, currentUser, commentSeen}) => {
    const sComment = CommentStore.useContext();
    const sThreadView = useThreadViewStore();

    const comments = sComment.mThreadId2TopComments.get(threadId);

    return (<currentUserContext.Provider value={currentUser}>
        {comments && comments.map(comment => (
            <CommentItemWrap key={comment.commentId}
                comment={comment}
                replyLevel={1}
                commentSeen={commentSeen}
                readOnly={readOnly}
                />
        ))}
        {sThreadView.replingCommentId == null && !readOnly && (
            <CommentEditorWrap threadId={threadId} />
        )}
    </currentUserContext.Provider>);
}));

const CommentEditorWrap: FC<{
    threadId    : string,
    replyTo    ?: DbIdentity,
    onCancel   ?: () => void,
}> = observer(({threadId, replyTo, onCancel}) => {
    const currentUser = useCurrentUser();
    const sComment = CommentStore.useContext();
    const sLocal = useLocalStore(() => ({
        value: "",
        setValue(v:string) { this.value = v }
    }));

    const handleSubmit = useCallback(async () => {
        if (!sLocal.value || !currentUser) return;
        const [err] = await sComment.submitComment({
            threadId,
            replyTo,
            content:sLocal.value,
            createdBy: currentUser.userId,
        });
        if (err) Modal.error({ content: err.message });
        else sLocal.setValue("");
    }, [threadId, replyTo]);

    return (
        <CommentEditor
            author={currentUser}
            value={sLocal.value} onChange={sLocal.setValue}
            onSubmit={handleSubmit}
            onCancel={onCancel}
            />
    );
});

const CommentItemWrap: FC<{
    comment     : Comment,
    onReplyTo  ?: () => void,
    replyLevel  : number,
    readOnly   ?: boolean,
    commentSeen?: CommentSeen,
}> = observer(({ comment, replyLevel, onReplyTo, readOnly, commentSeen }) => {
    const sUser = UserStore.useContext();
    const sComment = CommentStore.useContext();
    const sThreadView = useThreadViewStore();

    const author = comment.createdBy ? sUser.mId2Item.get(comment.createdBy) : undefined;
    const replies = sComment.mParent2Comments.get(comment.commentId);
    const onReply = useCallback(() => {
        if (0 < replyLevel) sThreadView.beginReply(comment.commentId)
        else if (onReplyTo) onReplyTo();
    }, [sThreadView, comment.commentId, onReplyTo]);

    const currentUser = useCurrentUser();
    const isNew = (currentUser == null || comment.createdBy != currentUser!.userId) &&
        (commentSeen == null || (commentSeen.lastSeen < (comment.dateCreated || 0)));

    return (
        <CommentItem comment={comment}
            author={author}
            onReply={readOnly ? undefined : onReply}
            className={classNames({"unread": isNew})}
            >
            {replies && replies.map(comment => (
                <CommentItemWrap
                    key={comment.commentId}
                    comment={comment}
                    replyLevel={replyLevel-1}
                    readOnly={readOnly}
                    onReplyTo={onReply}
                    commentSeen={commentSeen}
                    />
            ))}
            {sThreadView.replingCommentId == comment.commentId && !readOnly && (
                <CommentEditorWrap threadId={comment.threadId} replyTo={comment.commentId} onCancel={sThreadView.cancelReply} />
            )}
        </CommentItem>
    );
});

// --------------------------

export const CommentItem: FC<{
    comment   : Comment,
    author   ?: IUser,
    onReply  ?: (comment: Comment) => void,
    className?: string,
}> = observer(({comment, author, onReply, className, children}) => {
    const {t} = useTranslation();
    const content = useMemo(() => ({__html:comment.content}), [comment.content]);
    const userName = author ? `${author.firstName} ${author.lastName}` : `User(${comment.createdBy})`;
    const reply = useCallback(() => { onReply && onReply(comment) }, [onReply, comment]);

    return (
        <AntdComment
            className={className}
            author={userName}
            avatar={(
                <Avatar src={author ? author.avatar : undefined}
                    alt={author ? `${userName} avatar` : userName}
                    icon="user" />
            )}
            content={(<div dangerouslySetInnerHTML={content} className="whitespace-pre-line"></div>)}
            datetime={(<DateFromNowToolTip value={comment.dateCreated || ""} />)}
            actions={[
                onReply ? (<a onClick={reply}>{t('app.thread.comment.replyto')}</a>) : undefined
            ].filter(Boolean)}
        >
            {children}
        </AntdComment>
    );
});

const autosize = ({ minRows: 4, maxRows: 8 });

export const CommentEditor: FC<{
    author    ?: IUser,
    value      : string,
    onChange   : (v: string) => void,
    onSubmit   : () => Promise<void>,
    onCancel  ?: () => void,
    inputProps?: TextAreaProps,
}> = observer(({author, value, onChange, onSubmit, onCancel, inputProps}) => {
    const {t} = useTranslation();
    const [submitting, setSubmitting] = useState(false);
    const handleSubmit = useCallback((event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        setSubmitting(true);
        onSubmit().then(() => setSubmitting(false), () => setSubmitting(false));
    }, [onSubmit, setSubmitting]);

    return (
        <AntdComment
            avatar={(<Avatar src={author ? author.avatar : undefined}
                alt={author ? `${author.firstName} ${author.lastName} avatar` : undefined}
                icon="user" />)}
            content={(
                <Form onSubmit={handleSubmit}>
                    <Form.Item>
                        <TextArea
                            autosize={autosize}
                            maxLength={4000}
                            {...inputProps}
                            value={value} onChange={onChange}
                            />
                    </Form.Item>
                    <Form.Item>
                        {onCancel && (<Button onClick={onCancel} className="mr-sm">
                        {t("app.thread.comment.cancel")}
                        </Button>)}
                        <Button htmlType="submit" loading={submitting} type="primary">
                            {t("app.thread.comment.add")}
                        </Button>
                    </Form.Item>
                </Form>
            )}
        />
    );
});
