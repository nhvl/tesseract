import React, { FC, useState } from "react";
import { observer } from "mobx-react";

import { ThreadViewStore } from "./ThreadViewStore";
import { CommentStore } from "../../stores/CommentStore";
import { contextFactory } from "../../utils/contextFactory";

export const {
    // Provider : ThreadViewProvider,
    withStore: withThreadViewStore,
    useStore : useThreadViewStore,
} = contextFactory(function() {
    const sComment = CommentStore.useContext();
    const [sThreadView] = useState(() => new ThreadViewStore(sComment));
    return sThreadView;
});
