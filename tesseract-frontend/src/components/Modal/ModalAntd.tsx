import { Modal as ModalAndtd } from "antd";
import { ModalFuncProps } from "antd/lib/modal";
import i18n from "../../i18n";

export class Modal {
    static error (props: ModalFuncProps){
        props.title = !props.title ? i18n.t('component.modal.error') : props.title;
        props.okText = !props.okText ? i18n.t('form.ok') : props.okText;
        ModalAndtd.error(props);
    }

    static info (props: ModalFuncProps){
        props.title = !props.title ? i18n.t('component.modal.info') : props.title;
        props.okText = !props.okText ? i18n.t('form.ok') : props.okText;
        ModalAndtd.info(props);
    }

    static success (props: ModalFuncProps){
        props.title = !props.title ? i18n.t('component.modal.success') : props.title;
        props.okText = !props.okText ? i18n.t('form.ok') : props.okText;
        ModalAndtd.success(props);
    }

    static warning (props: ModalFuncProps){
        props.title = !props.title ? i18n.t('component.modal.warning') : props.title;
        props.okText = !props.okText ? i18n.t('form.ok') : props.okText;
        ModalAndtd.warning(props);
    }

    static deleteConfirm (props: ModalFuncProps){
        props.title = !props.title ? i18n.t('component.modal.confirm.delete') : props.title;
        this.confirm(props);
    }

    static confirm (props: ModalFuncProps){
        props.title = !props.title ? i18n.t('component.modal.confirm') : props.title;
        props.okText = !props.okText ? i18n.t('form.yes') : props.okText;
        props.cancelText = !props.cancelText ? i18n.t('form.no') : props.cancelText;
        ModalAndtd.confirm(props);
    }
}
