import React, { AnchorHTMLAttributes, FC, useMemo } from 'react';
import { observer } from 'mobx-react';

import { useBaseStore } from '../../stores/useBaseStore';

import { StringMap, RouterState, Link as MsrLink } from "mobx-state-router";

interface ILinkProps extends AnchorHTMLAttributes<HTMLAnchorElement> {
    routeName       : string;
    params         ?: StringMap;
    queryParams    ?: Object;
    activeClassName?: string;
}

export const Link: FC<ILinkProps> = observer(({routeName, params, queryParams, children, ...props}) => {
    const {routerStore} = useBaseStore();
    const state = useMemo(() => new RouterState(routeName, params, queryParams), [routeName, params, queryParams]);

    return (
        <MsrLink {...props} routerStore={routerStore} toState={state}>
            {children}
        </MsrLink>
    )
});
