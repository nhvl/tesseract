import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { useBaseStore } from '../../stores/useBaseStore';

import { RouterView as MsrRouterView, ViewMap } from "mobx-state-router";

export const RouterView: FC<{viewMap:ViewMap}> = observer(({viewMap}) => {
    const {routerStore} = useBaseStore();
    return (
        <MsrRouterView routerStore={routerStore} viewMap={viewMap} />
    )
});
