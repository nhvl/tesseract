import React, { FC, useMemo, CSSProperties } from 'react';
import { observer } from 'mobx-react';

const defaultStyle : CSSProperties = {
    width       : "1rem",
    height      : "1rem",
    borderRadius: "50%",
};

export const CssPieChart: FC<{
    values : number[],
    colors?: string[],
    style ?: CSSProperties,
}> = observer(({
    colors = ["#e8c1a0", "#f47560", "#f1e15b", "#e8a838", "#61cdbb", "#97e3d5"],
    values,
    style,
}) => {
    const pieStyle = useMemo<CSSProperties>(() => {
        const sum = values.reduce((s, v) => s + v, 0);
        if (sum == 0) return {
            ...defaultStyle,
            ...style,
        };

        const percents = values.map(v => (v/sum * 100));
        let acc = 0;
        const accPercents = percents.map((p) => [acc, acc += p] as const);
        const steps = accPercents.map(([s, e], i) => `${colors[i % colors.length]} ${s}% ${e}%`);
        console.log(values, sum, percents, accPercents, steps)
        return {
            ...defaultStyle,
            ...style,
            background: `conic-gradient(${steps.join(", ")}`,
        }
    }, [values, style]);

    return (
        <div style={pieStyle} />
    );
});
