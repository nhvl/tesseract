import React, { ReactNode, FC, CSSProperties } from 'react';
import classNames from 'classnames';

import { Icon } from 'antd';

import styles from './index.module.less';

export interface IResultProps {
  className  ?: string;
  type        : 'success' | 'error';
  title       : ReactNode;
  description?: ReactNode;
  extra      ?: ReactNode;
  actions    ?: ReactNode;
  style      ?: CSSProperties;
}

const iconMap = {
  error  : (<Icon className={styles.error  } type="close-circle" theme="filled" />),
  success: (<Icon className={styles.success} type="check-circle" theme="filled" />),
};

export const Result:FC<IResultProps> = (({
  className,
  type,
  title,
  description,
  extra,
  actions,
  ...restProps
}) => {

  const clsString = classNames(styles.result, className);
  return (
    <div className={clsString} {...restProps}>
                       <div className={styles.icon       }>{iconMap[type]}</div>
                       <div className={styles.title      }>{title        }</div>
      {description && (<div className={styles.description}>{description  }</div>)}
      {extra       && (<div className={styles.extra      }>{extra        }</div>)}
      {actions     && (<div className={styles.actions    }>{actions      }</div>)}
    </div>
  );
});
