import React, { FC, useMemo, useCallback, useState, useEffect } from "react";
import { observer } from "mobx-react";

import { FiveCValues, FiveC, ActivityScore } from "../models/ActivityScore";
import { IFiveCScore } from "../models/SchoolReport";
import { ScoreBadge } from "../models/ScoreBadge";

import { ResponsiveBar, BarSvgProps } from "@nivo/bar";
import { DbIdentity } from "../models/types";
import { values } from "mobx";
import { useTranslation } from "react-i18next";

const defaultProps: BarSvgProps = ({
    keys: ["Low", "Medium", "High"],
    indexBy: "c",
    margin: { top: -32, right: 12, bottom: 0, left: 90 },
    padding: 0.5,
    layout:"horizontal",
    defs: [
        { id: "dots", type: "patternDots", background: "inherit", color: "#38bcb2", size: 4, padding: 1, stagger: true },
        { id: "lines", type: "patternLines", background: "inherit", color: "#eed312", rotation: -45, lineWidth: 6, spacing: 10 },
    ],
    // fill: [
    //     { match: { id: "fries" }, id: "dots" },
    //     { match: { id: "sandwich" }, id: "lines"},
    // ],
    // @ts-ignore
    borderColor: { from: "color", modifiers: [["darker", 1.6]] },
    axisBottom: {
        tickSize: 8,
        tickPadding: 4,
        tickRotation: 0,
        legend: "",
        legendPosition: "middle",
        legendOffset: 64,
        tickValues : 10
    },
    axisLeft: {
        tickSize: 4,
        tickPadding: 2,
        tickRotation: 0,
        legend: "",
        legendPosition: "middle",
        legendOffset: -100,
    },
    labelSkipWidth: 12,
    labelSkipHeight: 12,
    labelTextColor: (props) => {
        return ScoreBadge.getTextColorFromBackground(props.color);
    },
    legends: [],
    animate: true,
    motionStiffness: 90,
    motionDamping: 15
});

const theme = {
  axis: {
    ticks: {
      text: {
        fontSize: 12
      }
    }
  }
};

const colorSquare = {
    width: '12px',
    height: '12px',
    display: 'block',
    marginRight: '7px'
}
export interface I5cChartProps extends Omit<BarSvgProps, "data"> {
    report     : IFiveCScore,
    scoreBadge?: ScoreBadge,
}

export const FiveCChart: FC<I5cChartProps> = observer(({ report, scoreBadge, ...props }) => {
    const {t} = useTranslation();

    const data = useMemo(() => [
        { c:FiveC.Collaboration   , High: report.collaboration   [FiveCValues.High], Medium: report.collaboration   [FiveCValues.Medium], Low: report.collaboration   [FiveCValues.Low] },
        { c:FiveC.Communication   , High: report.communication   [FiveCValues.High], Medium: report.communication   [FiveCValues.Medium], Low: report.communication   [FiveCValues.Low] },
        { c:FiveC.Creativity      , High: report.creativity      [FiveCValues.High], Medium: report.creativity      [FiveCValues.Medium], Low: report.creativity      [FiveCValues.Low] },
        { c:FiveC.Character       , High: report.character       [FiveCValues.High], Medium: report.character       [FiveCValues.Medium], Low: report.character       [FiveCValues.Low] },
        { c:FiveC.CriticalThinking, High: report.criticalThinking[FiveCValues.High], Medium: report.criticalThinking[FiveCValues.Medium], Low: report.criticalThinking[FiveCValues.Low] },
    ], [report]);

    const tickValues = useMemo(() => Math.min(10,
        Math.max(
            report.collaboration   [FiveCValues.High] + report.collaboration   [FiveCValues.Medium] + report.collaboration   [FiveCValues.Low],
            report.communication   [FiveCValues.High] + report.communication   [FiveCValues.Medium] + report.communication   [FiveCValues.Low],
            report.creativity      [FiveCValues.High] + report.creativity      [FiveCValues.Medium] + report.creativity      [FiveCValues.Low],
            report.character       [FiveCValues.High] + report.character       [FiveCValues.Medium] + report.character       [FiveCValues.Low],
            report.criticalThinking[FiveCValues.High] + report.criticalThinking[FiveCValues.Medium] + report.criticalThinking[FiveCValues.Low]
            )
        ), [report]);

    const colors = useCallback(({id, data}:{id:string, data:{c:FiveC}}) => scoreBadge!.getBadgeColor(data.c, FiveCValues[id]), [scoreBadge]);

    if (report == null) return null;

    return (<ResponsiveBar
        {...defaultProps}
        axisLeft={{
            ...defaultProps.axisLeft,
            tickValues: tickValues,
            format: value => `${ActivityScore.getFiveCLocale(String(value), t)}`
        }}
        tooltip={(props) => (
            <div style={{whiteSpace: 'pre', display: 'flex', alignItems: 'center',}}>
                <span style={{...colorSquare,backgroundColor: props.color}}></span>
                <span>
                    {props.id} - {ActivityScore.getFiveCLocale(String(props.indexValue), t)}: <strong>{props.value}</strong>
                </span>
            </div>
        )}
        colors={scoreBadge ? colors : undefined}
        theme = {theme}
        {...props}
        data={data}
    />);
});

interface ISchool5cChartProps extends Omit<I5cChartProps, "scoreBadge"> {
    schoolId: DbIdentity;
}

const scoreBadgeCache = new Map<DbIdentity, ScoreBadge>();

export const School5cChart: FC<ISchool5cChartProps> = observer((props) => {
    const { schoolId } = props;
    const [scoreBadge, setScoreBadge] = useState<ScoreBadge>();
    useEffect(() => {
        if (schoolId == null) return;
        const x = scoreBadgeCache.get(schoolId);
        if (x != null) setScoreBadge(x);
        else {
            ScoreBadge.getSchoolScoreBadge(schoolId).then(([err,x])=>{
                if (err) { console.warn(err); return; }
                setScoreBadge(x);
                scoreBadgeCache.set(schoolId, x);
            });
        }
    }, [schoolId]);

    return (<FiveCChart scoreBadge={scoreBadge} {...props} />)
});

const tinyChartProps: Omit<BarSvgProps, "data"> = {
    margin: undefined,
    // @ts-ignore
    axisBottom: null,
    // @ts-ignore
    axisLeft: null,
    enableGridY: false,
    legends: [],
};

export const Tiny5cChart: FC<ISchool5cChartProps> = observer((props) => {
    return (<School5cChart
        {...tinyChartProps}
        {...props}
    />);
});


