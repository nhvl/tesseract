
import React, { FC, useRef, useEffect, useState } from 'react';
import AudioPlayer from '../libs/AudioPlayer';

import {Visualizer, IVisualizerProps} from "./Visualizer";
import { MyRecorder } from '../libs/BetterMediaRecorder';

export interface IReactMicProps extends IVisualizerProps {
  record             : boolean,
  pause             ?: boolean,
  audioBitsPerSecond?: number,
  mimeType          ?: string,
  onSave            ?: (data: Blob) => void,
  audioElem         ?: HTMLAudioElement,
}

const ReactMic:FC<IReactMicProps> = (({
  audioBitsPerSecond = 128000,
  mimeType           = 'audio/webm;codecs=opus',
  record             = false,
  pause              = false,
  audioElem,
  onSave,
  ...props
}) => {
  const rRecorder = useRef<MyRecorder>();
  const [analyser, setAnalyser] = useState<AnalyserNode|undefined>();

  useEffect(() => {
    if(audioElem) {
      AudioPlayer.create(audioElem);
    } else {
      const recorder = rRecorder.current = new MyRecorder({
        audioBitsPerSecond : audioBitsPerSecond,
        mimeType           : mimeType
      });
      recorder.onAnalyserChange = setAnalyser;
    }

    return (() => {
      if (rRecorder.current) {
        rRecorder.current.destroy();
      }
    });
  }, []);

  useEffect(() => {
    const microphoneRecorder = rRecorder.current;
    if (record) {
      if (microphoneRecorder) {
        if (pause) microphoneRecorder.pause();
        else microphoneRecorder.start();
      }
    } else {
      if (microphoneRecorder) {
        microphoneRecorder.stop().then(blob => {
          if (blob == null) return;
          if (onSave != null) onSave(blob);
        });
      }
    }
  }, [record, pause]);

  return (<Visualizer analyser={analyser} {...props} />);
});

export default ReactMic;
