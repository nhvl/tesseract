import React, { FC, useRef, useEffect } from "react";
import {visualizeFrequencyBars, visualizeFrequencyCircles, visualizeSineWave, IVisualizeOptions} from "../libs/Visualizer";

export type VisualType = "sinewave"|"frequencyBars"|"frequencyCircles";

export interface IVisualizerProps extends Partial<IVisualizeOptions> {
  className         ?: string,
  visualSetting     ?: VisualType,
  analyser          ?: AnalyserNode,
}

export const Visualizer:FC<IVisualizerProps> = (({
  backgroundColor    = 'rgba(255, 255, 255, 0.5)',
  strokeColor        = '#000000',
  className          = 'visualizer',
  width              = 640,
  height             = 100,
  visualSetting      = 'sinewave',
  analyser           = undefined,
}) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current;

    return visualize(canvas, analyser, visualSetting, {width, height, backgroundColor, strokeColor});
  }, [analyser, visualSetting, height, width, backgroundColor, strokeColor]);

  return (<canvas ref={canvasRef} height={height} width={width} className={className} />);
});

function visualize(canvas:HTMLCanvasElement|null, analyser: AnalyserNode|undefined, visualSetting: VisualType, options:IVisualizeOptions) {
  if (canvas == null) return;
  const canvasCtx = canvas.getContext("2d");
  if (canvasCtx == null) return;

  if (analyser == null) {
    canvasCtx.clearRect(0, 0, options.width, options.height);
    return;
  }

  switch (visualSetting) {
    case "frequencyBars":
      return visualizeFrequencyBars(canvasCtx, analyser, options);
    case "frequencyCircles":
      return visualizeFrequencyCircles(canvasCtx, analyser, options);
    case "sinewave":
    default:
      return visualizeSineWave(canvasCtx, analyser, options);
  }
}
