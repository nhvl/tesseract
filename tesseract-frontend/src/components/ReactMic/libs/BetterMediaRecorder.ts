export class BetterMediaRecorder {
    chunks: Blob[];
    private resolve?: (_:Blob|undefined) => void;
    ondataavailable?: (event: BlobEvent) => void;
    onstop?: (event: Event, blob?:Blob) => void;

    constructor(public mediaRecorder: MediaRecorder) {
        this.chunks = [];
        mediaRecorder.onstop = this.handleStop;
        mediaRecorder.ondataavailable = this.handleDataAvailable;
    }

    get stream() { return this.mediaRecorder.stream; }
    get state() { return this.mediaRecorder.state; }

    start(timeSlice?:number) {
        this.mediaRecorder.start(timeSlice);
    }

    stop() {
        if (this.resolve) this.resolve(undefined);
        return new Promise<Blob|undefined>(resolve => {
            this.resolve = resolve;
            this.mediaRecorder.stop();
        });
    }

    resume() {
        this.mediaRecorder.resume();
    }

    pause() {
        this.mediaRecorder.pause();
    }

    private handleStop = (e:Event) => {
        const blob = (this.chunks.length < 1) ? undefined : new Blob(this.chunks, { type : this.chunks[0].type });
        this.chunks = [];
        if (this.resolve) this.resolve(blob);
        if (this.onstop) this.onstop(e, blob);
    }

    private handleDataAvailable = (event: BlobEvent) => {
        if (this.ondataavailable) this.ondataavailable(event)
        this.chunks.push(event.data);
    }
}

const constraints = { audio: true, video: false };

export class MyRecorder {
    public mediaRecorder?: BetterMediaRecorder;
    public audioContext = new AudioContext();
    public analyser?: AnalyserNode;
    onAnalyserChange?: (analyser?: AnalyserNode) => void;

    constructor(private recorderOptions?:MediaRecorderOptions) {
    }

    async start() {
        if (this.mediaRecorder == null) {
            const stream = await navigator.mediaDevices.getUserMedia(constraints);
            if (this.recorderOptions && this.recorderOptions.mimeType && !MediaRecorder.isTypeSupported(this.recorderOptions.mimeType)) delete this.recorderOptions.mimeType;
            this.mediaRecorder = new BetterMediaRecorder(new MediaRecorder(stream, this.recorderOptions));
        }

        if (this.audioContext.state === 'suspended') {
            await this.audioContext.resume();
        } else if (this.audioContext.state === 'closed') {
            this.audioContext = new AudioContext();
            this.removeAnalyser();
        }

        if (this.mediaRecorder.state === 'paused') {
            this.mediaRecorder.resume();
            this.removeAnalyser();
        } else if (this.mediaRecorder.state === 'inactive') {
            this.mediaRecorder.start(10);
            this.removeAnalyser();
        }

        if (this.analyser == undefined) {
            this.createAnalyser();
        }
    }

    async pause() {
        if (this.mediaRecorder != null) this.mediaRecorder.pause();
        this.removeAnalyser();
        await this.audioContext.suspend();
    }

    async stop() {
        let p: Promise<Blob|undefined>|undefined = undefined;
        if (this.mediaRecorder != null) {
            const {stream} = this.mediaRecorder;

            p = this.mediaRecorder.stop();
            this.mediaRecorder = undefined;

            if (stream != null) {
                stream.getTracks().forEach(e => e.stop());
            }
        }
        this.removeAnalyser();
        await this.audioContext.suspend();
        if (p != null) return await p;
        return;
    }

    async destroy() {
        this.onAnalyserChange = undefined;
        const data = await this.stop();
        this.audioContext.close();
        return data;
    }

    private removeAnalyser() {
        if (this.analyser == null) return;
        this.analyser.disconnect();
        this.analyser = undefined;

        if (this.onAnalyserChange) this.onAnalyserChange(this.analyser);
    }

    private createAnalyser() {
        if (this.mediaRecorder == null) return;

        this.analyser = this.audioContext.createAnalyser();
        var source = this.audioContext.createMediaStreamSource(this.mediaRecorder.stream);
        source.connect(this.analyser);

        if (this.onAnalyserChange) this.onAnalyserChange(this.analyser);
    }
}



