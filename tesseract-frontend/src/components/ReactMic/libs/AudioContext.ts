const audioCtx = new AudioContext();
let analyser = audioCtx.createAnalyser();

const audioContext = {

  getAudioContext() {
    return audioCtx;
  },

  getAnalyser() {
    return analyser;
  },

  resetAnalyser() {
    analyser = audioCtx.createAnalyser();
  },
}

export default audioContext;
