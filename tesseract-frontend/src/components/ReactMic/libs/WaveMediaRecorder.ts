import audioContext from "./AudioContext";
import { IBlobObject } from "./MicrophoneRecorder";

let u = 0;
let c0Chunks: Float32Array[] = [];
let c1Chunks: Float32Array[] = [];
let sampleRate:number;
let startTime:number;
let processor: ScriptProcessorNode;
let source:MediaStreamAudioSourceNode;
let gain: GainNode;
let onData: (_: IBlobObject) => void;

export class WaveMediaRecorder {
  audioCtx: AudioContext;
  analyser: AnalyserNode;
  stream: MediaStream;

  constructor(stream: MediaStream, options?: MediaRecorderOptions, r:{onData:typeof onData}) {
    this.stream = stream;
    this.audioCtx = audioContext.getAudioContext();
    this.analyser = audioContext.getAnalyser();
    onData = r.onData;
  }

  start() {
    const { audioCtx, analyser } = this;
    gain = audioCtx.createGain();
    source = audioCtx.createMediaStreamSource(this.stream);
    sampleRate = audioCtx.sampleRate;
    if (audioCtx && "suspended" === audioCtx.state) audioCtx.resume();
    source.connect(gain);
    processor = audioCtx.createScriptProcessor
      ? audioCtx.createScriptProcessor(8192, 2, 2)
      : //@ts-ignore
        audioCtx.createJavaScriptNode(8192, 2, 2);
    processor.onaudioprocess = event => {
      console.log("recording");
      const n = event.inputBuffer.getChannelData(0);
      const r = event.inputBuffer.getChannelData(1);
      c0Chunks.push(new Float32Array(n));
      c1Chunks.push(new Float32Array(r));
      u += 8192;
      if (onData) onData(this.exportWav());
    };
    gain.connect(processor);
    source.connect(analyser);
    processor.connect(analyser);
    startTime = Date.now();
  }

  pause() {
    const { stream, audioCtx } = this;
    stream.getAudioTracks().forEach(e => {
      e.stop();
    });
    gain.disconnect(audioCtx);
    processor.disconnect(audioCtx);
    audioCtx.suspend();
  }

  stop() {
    const { stream, audioCtx } = this;
    u = 0;
      c0Chunks = [];
      c1Chunks = [];
      stream.getAudioTracks().forEach(e => {
        e.stop();
      });
    gain.disconnect(audioCtx);
    processor.disconnect(audioCtx);
    audioCtx.suspend();
  }

  mergeBuffers(e: Float32Array[], t: number) {
    for (var n = new Float32Array(t), r = 0, o = e.length, i = 0; i < o; i++) {
      var a = e[i];
      n.set(a, r), (r += a.length);
    }
    return n;
  }

  interleave(e: Float32Array, t: Float32Array) {
    for (var n = e.length + t.length, r = new Float32Array(n), o = 0, i = 0; i < n;) {
      r[i++] = e[o];
      r[i++] = t[o];
      o++;
    }
    return r;
  }

  writeUTFBytes(e: DataView, t:number, n: string) {
    for (var r = n.length, o = 0; o < r; o++)
      e.setUint8(t + o, n.charCodeAt(o));
  }

  exportWav(): IBlobObject {
    var e = this.mergeBuffers(c0Chunks, u);
    var t = this.mergeBuffers(c1Chunks, u);
    var n = this.interleave(e, t);
    var r = new ArrayBuffer(44 + 2 * n.length);
    var o = new DataView(r);
    this.writeUTFBytes(o, 0, "RIFF");
    o.setUint32(4, 44 + 2 * n.length, !0);
    this.writeUTFBytes(o, 8, "WAVE");
    this.writeUTFBytes(o, 12, "fmt ");
    o.setUint32(16, 16, !0);
    o.setUint16(20, 1, !0);
    o.setUint16(22, 2, !0);
    o.setUint32(24, sampleRate, !0);
    o.setUint32(28, 4 * sampleRate, !0);
    o.setUint16(32, 4, !0);
    o.setUint16(34, 16, !0);
    this.writeUTFBytes(o, 36, "data");
    o.setUint32(40, 2 * n.length, !0);
    for (var i = 44, a = 0; a < n.length; a++)
      o.setInt16(i, 32767 * n[a], !0), (i += 2);
    const blob = new Blob([o], {
      type: "audio/wav"
    });
    return {
      blob     : blob,
      startTime: startTime,
      stopTime : Date.now(),
      options  : {},
      blobURL  : URL.createObjectURL(blob),
    };
  }
}
