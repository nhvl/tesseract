import audioContext from './AudioContext';

let audioSource: MediaElementAudioSourceNode;

const AudioPlayer =  {

  create(audioElem:HTMLAudioElement) {
    const audioCtx = audioContext.getAudioContext();
    const analyser = audioContext.getAnalyser();

    if (audioSource === undefined){
      const source = audioCtx.createMediaElementSource(audioElem);
      source.connect(analyser);
      audioSource = source;
    }

    analyser.connect(audioCtx.destination);
  }

}

export default AudioPlayer;
