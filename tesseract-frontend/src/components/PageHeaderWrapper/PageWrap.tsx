import React, { ReactNode, FunctionComponent, CSSProperties } from 'react';
import { observer } from 'mobx-react';

import { GridContent } from './GridContent';
import styles from './index.module.less';

const style: CSSProperties = { margin: '-24px -24px 0' };

export const PageWrap: FunctionComponent<{top?:ReactNode}> = observer(({top, children}) => {
    return (
        <div style={style}>
            {top}
            <div className={styles.content}>
                <GridContent>
                    {children}
                </GridContent>
            </div>
        </div>
    )
});
