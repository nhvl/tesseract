import React, { FC } from 'react';
import { observer } from 'mobx-react';

import styles from './index.module.less';
import classNames from 'classnames';

export const GridContent: FC<{fixed?:boolean}> = observer(({fixed, children}) => {
    return (
        <div className={classNames(styles.main, !!fixed && styles.wide)}>{children}</div>
    )
});
