import React, { FC } from "react";
import { observer } from "mobx-react";

import { DbIdentity } from "../models/types";
import { ActivityType } from "../models/Activity";

import { Link } from "../components/router/Links";

export const ClassActivityLink: FC<{type:ActivityType, classId:DbIdentity}> = observer(({type, classId, children}) => {
    const routeName = getClassActivityRouteName(type);
    return (<Link routeName={routeName} params={{classId:String(classId)}}>{children}</Link>);
});

function getClassActivityRouteName(type:ActivityType) {
    switch(type) {
        case ActivityType.Activity: return "classActivities";
        case ActivityType.Assignment: return "classAssignments";
        case ActivityType.Assessment: return "classAssessments";
    }
    return "classDetail";
}
