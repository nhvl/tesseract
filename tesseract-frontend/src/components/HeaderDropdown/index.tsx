import React, { FC } from 'react';
import { observer } from 'mobx-react';
import classNames from "classnames";

import { Dropdown } from 'antd';
import { DropDownProps } from 'antd/lib/dropdown';

import styles from './index.module.less';

export const HeaderDropdown: FC<DropDownProps> = observer(({ overlayClassName, ...props }) => {
  return (
    <Dropdown overlayClassName={classNames(styles.container, overlayClassName)} {...props} />
  );
});
