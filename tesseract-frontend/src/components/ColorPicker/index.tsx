import React, { useState, FC, FormEvent, useCallback, useEffect, useMemo } from "react";
import { observer } from "mobx-react";
import classNames from "classnames";

import { GithubPicker } from "react-color";
import styles from "./index.module.less";
export const ColorPicker:FC<{color:string
                    , set_color:(v: string) => void
                    , isDisabled?:boolean, colors:string[]}> = observer(({
                        color, set_color, isDisabled, colors
                    }) => {
    const [displayColorPicker, setDisplayColorPicker] = useState(false);
    const colorItems = useCallback(()=>{
        const whiteColor = "#FFFFFF";
        if(colors.length != 0){
            if(!colors.find(c => c.toUpperCase().startsWith(whiteColor))){
                colors.unshift(whiteColor);
            }
            return colors;
        }
        return undefined;
    },[colors])
    return (<div className="relative">
                <div className={classNames(styles.swatch, isDisabled && styles.disabled)} onClick={ () => setDisplayColorPicker(true) }>
                    <div className={ styles.color } style={{background:color}} />
                </div>
                { displayColorPicker ? <div className={ styles.popover }>
                    <div className={ styles.cover } onClick={ () => setDisplayColorPicker(false) }/>
                    <GithubPicker colors={colorItems()} color={ color } onChange={ (color: any) => set_color(color.hex) } />
                </div> : null }
            </div>);
});
