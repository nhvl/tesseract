import React, { FC, useState, useCallback } from 'react';
import { observer } from 'mobx-react';

import { useTranslation } from "react-i18next";

import { ActItem, ActOption } from '../../models/ActDoc';

import { Form } from 'antd';

import { Checkbox } from '../inputs/antd/Input';

import styles from "./ViewItem.module.less";
import { ViewDefaultItem } from './ViewDefaultItem';

export const ViewChoiceQuizItem: FC<{ item: ActItem, noAnswer:boolean }> = observer((props) => {
    const { t } = useTranslation();
    const {item, noAnswer} = props;
    const [singleChecked, setSingleChecked] = useState(undefined as number|undefined);
    const onCheckedChange = useCallback((index: number, value: boolean) => {
        if(!item.isMultipleAnswer){
            if(value){
                setSingleChecked(index);    
            }
            else{
                setSingleChecked(undefined);
            }
        }
    },[item, noAnswer]);
    return (
        <ViewDefaultItem {...props}>
            {item.isMultipleAnswer && !noAnswer && 
                (<div className={'mb-sm'}>{t('form.activity.question.option.allowMultiple')}</div>)}
            {item.options && item.options.map((option, i) => (
                item.isMultipleAnswer 
                ? <OptionItem key={i} option={option} noAnswer={noAnswer} />
                : <SingleOptionItem key={i} option={option} noAnswer={noAnswer}
                    checked={singleChecked==i} onSingleCheckedChange={(v)=>onCheckedChange(i, v)} />
            ))}
        </ViewDefaultItem>
    );
});

const SingleOptionItem: FC<{ option: ActOption, noAnswer:boolean
        , checked:boolean
        , onSingleCheckedChange: (value: boolean) => void }>
             = observer(({ option, noAnswer, checked, onSingleCheckedChange }) => {
    const validateStatus = (noAnswer || !checked) ? "" : (option.isCorrect ? "success" : "error");
    return (
        <Form.Item validateStatus={validateStatus} hasFeedback={!noAnswer}
        className={`${styles.validationIcon} ${styles.quizChoice} mb-sm`}>
            <Checkbox checked={checked} onChange={onSingleCheckedChange}>
                { (option.image && option.image != "") && <img src={option.image} className="imgFile-sm mb-sm block" />}
                <div className={`${styles.label}`}>
                    {option.label}
                </div>
            </Checkbox>
        </Form.Item>
    );
    });

const OptionItem: FC<{ option: ActOption, noAnswer:boolean}> = observer(({ option, noAnswer}) => {
    const [checked, setChecked] = useState(false);

    const validateStatus = (noAnswer || !checked) ? "" : (option.isCorrect ? "success" : "error");

    return (
        <Form.Item validateStatus={validateStatus} hasFeedback={!noAnswer}
        className={`${styles.validationIcon} ${styles.quizChoice} mb-sm`}>
            <Checkbox checked={checked} onChange={setChecked}>
                { (option.image && option.image != "") && <img src={option.image} className="imgFile-sm mb-sm block" />}
                <div className={`${styles.label}`}>
                    {option.label}
                </div>
            </Checkbox>
        </Form.Item>
    );
});
