import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { MediaTypeEnum } from '../../models/MediaTypeEnum';
import { ActItem } from '../../models/ActDoc';

import { Card, Typography, } from 'antd';
const { Title } = Typography;

import styles from "./ViewItem.module.less";
import ReactPlayer from 'react-player';

export const ViewMediaItem: FC<{item:ActItem}> = observer(({item}) => {

    return (
        <div className="mb-lg">
            <div className={styles.subTitle}>
                <Title level={4}>
                    <div dangerouslySetInnerHTML={{ __html: item.title }} />
                </Title>
            </div>
            <Card bordered={false} className="none-shadow"
                cover={(item.mediaType == MediaTypeEnum.ImageFile && (
                    <div className="paddingBottom-lg">
                        <img src={item.link} className="imgFile w-auto" />
                    </div>
                ))}>
                <div className="paddingBottom-lg">
                    {(  item.mediaType == MediaTypeEnum.VideoFile ||
                        item.mediaType == MediaTypeEnum.VideoLink ||
                        item.mediaType == MediaTypeEnum.AudioLink   ) ? (<>
                        <ReactPlayer url={item.link} controls pip />
                    </>) : ((item.mediaType == MediaTypeEnum.AudioFile) ? (<>
                        <video controls className="imgFile" src={item.link} poster={item.image || undefined} />
                    </>) : null)}
                </div>
                <div dangerouslySetInnerHTML={{__html:item.content}} />
            </Card>
        </div>
    );
});
