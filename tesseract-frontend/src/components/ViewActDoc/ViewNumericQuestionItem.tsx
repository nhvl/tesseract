import React, { FC, useState } from 'react';
import { observer } from 'mobx-react';

import { ActItem } from '../../models/ActDoc';

import { Form, InputNumber } from 'antd';

import { ViewDefaultItem } from './ViewDefaultItem';

import styles from "./ViewItem.module.less";

export const ViewNumericQuestionItem: FC<{ item: ActItem, noAnswer:boolean }> = observer(({ item, noAnswer }) => {
    const [value, setValue] = useState<number|undefined>(undefined);

    const validateStatus = (value == null || noAnswer) ? "" : (
        ((item.numericMin == null || item.numericMin <= value) &&
         (item.numericMax == null || value <= item.numericMax)) ? "success" : "error"
    );

    return (
        <ViewDefaultItem item={item}>
            <Form.Item
                validateStatus={validateStatus}
                hasFeedback={!noAnswer}
                className={`${styles.validationIcon} mb-sm`}>
                <InputNumber value={value} onChange={setValue} />
            </Form.Item>
        </ViewDefaultItem>
    );
});
