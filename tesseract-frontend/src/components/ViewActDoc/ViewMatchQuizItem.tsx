import React, { FC, useState, useCallback, useEffect, useRef, CSSProperties, useMemo } from 'react';
import { observer } from 'mobx-react';
import classNames from 'classnames';
import { shuffle } from 'lodash-es';

import { ActItem } from '../../models/ActDoc';

import { Icon, } from 'antd';

import { DragDropContext, Draggable, Droppable, DragDropContextProps } from 'react-beautiful-dnd';

import { ViewDefaultItem } from './ViewDefaultItem';

import styles from "./ViewMatchQuizItem.module.less";

export const ViewMatchQuizItem: FC<{ item: ActItem, noAnswer:boolean }> = observer(({ item, noAnswer }) => {
    const [options, setOptions] = useState(() => shuffle(item.matchQuizzItems));
    const [validateState, setValidateState] = useState<Array<boolean|undefined>>(() => item.matchQuizzItems.map(() => undefined));
    const [itemHeights, setItemHeights] = useState<number[]>([]);
    const [h, setH] = useState<number|undefined>(undefined);
    const rWrap = useRef<HTMLDivElement>(null);

    useEffect(() => {
        setTimeout(() => {
            const wrap = rWrap.current;
            if (wrap == null) return;
            const [wl, wr] = wrap.children;
            setItemHeights(Array.from(wl.children, (l_, i) => {
                const l = l_ as HTMLElement;
                const r = (wr.children[i] as HTMLElement);
                const lh = l.style.height; l.style.height = null;
                const rh = r.style.height; r.style.height = null;
                const mh = Math.max(l.offsetHeight, r.offsetHeight);
                l.style.height = lh;
                r.style.height = rh;
                return mh;
            }));
        }, 400);
    }, [window.innerWidth, options]);

    const onDragEnd:DragDropContextProps["onDragEnd"] = useCallback((result) => {
        if (!result.destination) return;
        const s = result.source.index;
        const e = result.destination.index;
        const xs = options.slice();
        const [x] = xs.splice(s, 1);
        xs.splice(e, 0, x);
        setOptions(xs);

        validateState[e] = item.matchQuizzItems[e].isEqual(xs[e]);
        if (item.matchQuizzItems.every((o, i) => (o.isEqual(xs[i])))) {
            setValidateState(item.matchQuizzItems.map((o, i) => (o.isEqual(xs[i]))));
        } else {
            setValidateState(item.matchQuizzItems.map((o, i) => validateState[i] != null ? (o.isEqual(xs[i])) : validateState[i]));
        }
    }, [options]);

    const onDragUpdate:DragDropContextProps["onDragUpdate"] = useCallback((initial) => {
        if (initial.destination != null)
            setH(initial.destination.index);
    }, []);

    return (
        <ViewDefaultItem item={item}>
            <div className="flex paddingTop-sm" ref={rWrap}>
                <div className="flex-1">
                    {item.matchQuizzItems.map((item, i) => (
                        <LeftItem key={i}
                            height={itemHeights[i]}
                            isHover={i == h}
                            onHover={setH} index={i}
                            noAnswer={noAnswer}
                            validateState={validateState[i]}
                        >{ (item.leftImage && item.leftImage != "") && <img src={item.leftImage} className="imgFile-sm mb-sm block" />}
                            {item.left}
                        </LeftItem>
                    ))}
                </div>
                <DragDropContext onDragEnd={onDragEnd} onDragUpdate={onDragUpdate}>
                    <Droppable droppableId="droppable">{(provided, dropSnapshot) => (
                        <div ref={provided.innerRef}
                            {...provided.droppableProps}
                            className={classNames("flex-1", {
                                "isDraggingOver" : dropSnapshot.isDraggingOver,
                            })}
                            >
                            {options.map((option, i) => (
                                <Draggable key={i} draggableId={String(i)} index={i}>{(provided, snapshot) => (
                                    <div ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        className={classNames(
                                            "mb-sm drag drag-answer drag-action",
                                            {
                                                ['isDragging']: snapshot.isDragging,
                                                ['isHover']: (!dropSnapshot.isDraggingOver && i == h),
                                                ['drag-answer-error']: !noAnswer && validateState[i] === false,
                                                ['drag-answer-success']: !noAnswer && validateState[i] === true,
                                            })}
                                        style={{...provided.draggableProps.style, height:itemHeights[i]}}
                                        onMouseOver={() => setH(i)}
                                    >
                                        { (option.rightImage && option.rightImage != "") && <img src={option.rightImage} className="imgFile-sm mb-sm block" />}
                                        {option.right}</div>
                                )}</Draggable>
                            ))}
                            {provided.placeholder}
                        </div>
                    )}</Droppable>
                </DragDropContext>
            </div>
        </ViewDefaultItem>
    );
});
const LeftItem: FC<{
    height        : number,
    index         : number,
    onHover       : (index: number) => void,
    isHover       : boolean,
    noAnswer      : boolean,
    validateState?: boolean,
}> = observer(({ height, index, onHover, validateState, isHover, noAnswer, children }) => {
    const onMouseOver = useCallback(() => onHover(index), [onHover, index]);
    const style = useMemo<CSSProperties>(() => ({height}), [height]);

    return (<div className={classNames("mb-sm flex items-center", {[styles.isHover]: isHover})}
        style={style} onMouseOver={onMouseOver}>
        {!noAnswer && (<span className={classNames(styles.answer, {
            "text-success": validateState === true,
            "text-danger"  : validateState === false,
        })}>{
            validateState === true ? (<Icon type="check-circle" theme="filled" className="icon-sm" />) : (
            validateState === false ? (<Icon type="close-circle" theme="filled" className="icon-sm" />) : (
            null))
        }</span>)}
        <div className="drag drag-question">
            {children}
        </div>
        <div><Icon type="swap" /></div>
    </div>);
});

