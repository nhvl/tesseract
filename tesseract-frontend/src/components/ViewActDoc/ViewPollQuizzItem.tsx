import React, { FC, useState, useCallback } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { ActItem, PollQuizItem } from '../../models/ActDoc';

import { Form, Progress, Radio, message, Spin } from 'antd';

const RadioGroup = Radio.Group;

import styles from "./ViewItem.module.less";
import { ViewDefaultItem } from './ViewDefaultItem';

import {useNumberTrans} from "../DateTime/Number";
import { Modal } from '../Modal/ModalAntd';

export const ViewPollQuizItem: FC<{
    item        : ActItem,
    noAnswer    : boolean,
    totalStudent: number,
    savePoll   ?: ((pollItemId: string, votes: number) => Promise<any>)
}> = observer((props) => {
    const {item, noAnswer, savePoll} = props;
    const { t } = useTranslation();
    const [isSaving, setSave] = useState(false);
    const handleSave = useCallback((e)=> {
        if(isSaving) return;
        setSave(true);
        let value = e.target.value;
        item.set_pollValue(value);
        if(savePoll == undefined) return;
        savePoll(item.id, value).then((err) => {
            setSave(false);
            if (err) {
                Modal.error({ title: "savePoll.failed", content: err.message });
                return;
            }
            setTimeout(message.success(t("app.actDoc.editor.editor.voteSuccess"), 0), 1000);
        });
    },[item]);

    return (
        <ViewDefaultItem {...props}>
            <Spin spinning={isSaving} tip={t("app.activities.score.Save")}>
                <RadioGroup value={item.pollValue} onChange={handleSave} className={!noAnswer ? `${styles.hidden} ` : ""} disabled = {savePoll == undefined}>
                    {item.pollItems && item.pollItems.map((item, i) => (
                        <Radio value={i} key={i} className={`${styles.radioStyle} flex items-center`}>
                            <PollContent item={item} />
                        </Radio>
                    ))}
                </RadioGroup>
            </Spin>
            {!noAnswer &&
                (<>
                    {item.pollItems && item.pollItems.map((item, i) => (
                        <PollView key={i} index={i} item={item} totalStudent={props.totalStudent}/>
                    ))}

                    {(item.totalPoll < props.totalStudent) &&
                        <NoPollView totalPoll={item.totalPoll} totalStudent={props.totalStudent}/>
                    }
                    <div className={styles.totalVotes}>
                        <label className="font-bold">{t('app.actDoc.preview.vote.totalVote')}: </label><span className="text-black">{item.totalPoll}</span>
                    </div>
                </>)
            }
        </ViewDefaultItem>
    );
});

const NoPollView: FC<{ totalPoll:number, totalStudent: number }> = observer(({ totalPoll, totalStudent }) => {
    const {t} = useTranslation();
    return (
        <Form.Item className={`${styles.poll} mb-sm`}>
            <div className="leading-normal -mb-2">{t('app.actDoc.preview.vote.noVote')}</div>
            <Progress percent={PollQuizItem.calculatePercent(totalStudent-totalPoll, totalStudent)} />
            <span className={`${styles.votes} ml-xs`}>({t('app.actDoc.preview.vote.countNoVotes', {count: totalStudent-totalPoll})})</span>
        </Form.Item>
    );
});

const PollView: FC<{ index:number, item: PollQuizItem, totalStudent: number }> = observer(({ index, item, totalStudent }) => {
    const {t} = useTranslation();
    return (
        <Form.Item className={`${styles.poll} mb-sm`}>
            <PollContent item={item} />
            <Progress percent={PollQuizItem.calculatePercent(item.votes, totalStudent)} />
            <span className={`${styles.votes} ml-xs`}>({t('app.actDoc.preview.vote.votes', { count : item.votes})})</span>
        </Form.Item>
    );
});

const PollContent: FC<{ item: PollQuizItem }> = observer(({ item }) => {
    return (
        <div className="leading-normal -mb-2">
            {!!item.image && (<img src={item.image} className="imgFile-sm mb-sm block" />)}
            {item.label}
        </div>
    );
});
