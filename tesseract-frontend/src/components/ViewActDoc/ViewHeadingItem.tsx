import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { ActItem } from '../../models/ActDoc';

import { ViewDefaultItem } from './ViewDefaultItem';

export const ViewHeadingItem: FC<{ item: ActItem }> = observer((props) => {
    return (
        <ViewDefaultItem {...props} headerLevel={2} />
    );
});
