import React, { FC } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import { EActItemType, ActDoc, ActItem } from '../../models/ActDoc';
import { LongDateLabel } from "../../components/DateTime/DateTime";

import { Card, Typography, Icon } from 'antd';
const { Title } = Typography;

import { ViewMediaItem } from "./ViewMediaItem";

import styles from "./ViewActDoc.module.less";
export const ViewActDoc: FC<{
    doc      : ActDoc,
    savePoll?: ((pollItemId: string, votes: number) => Promise<any>),
    noAnswer?: boolean
}> = observer((props) => {
    const { doc, savePoll, noAnswer = false } = props;
    const {t} = useTranslation();
    const currentTime = Date.now();

    return (<>
        <Card bordered={false} className="none-shadow paddingBottom-lg"
            cover={!!doc.banner && (<div className={styles.banner}><img src={doc.banner} /></div>)}>
            <Title level={1}>
                <div dangerouslySetInnerHTML={{ __html: doc.title }} />
            </Title>
            {!!doc.summary && (
                <div className={`font-size-lg text-grey description`}
                    dangerouslySetInnerHTML={{ __html: doc.summary }} />
            )}
        </Card>

        {doc.items.map(item => (
            <div key={item.id} className={styles.itemContainer}>
                {((item.startDate && currentTime < item.startDate) || (item.endDate && item.endDate < currentTime)) &&
                <div className={styles.itemTimeRange}>
                    <div className={styles.timeVisible}>
                        <Icon type="clock-circle" />
                        <div className={`${styles.inner} text-black`}>
                            <span className="font-bold">{t("app.actDoc.preview.timevisible.range")}</span>
                            <div>
                                <span> {t("app.actDoc.preview.timevisible.scheduledVisible")} </span>
                                {item.startDate && (<span>{t("app.actDoc.preview.timevisible.from")} <LongDateLabel value={item.startDate}/></span>)}
                                {item.endDate   && (<span>{t("app.actDoc.preview.timevisible.to"  )} <LongDateLabel value={item.endDate  }/></span>)}
                            </div>
                        </div>
                    </div>
                    <div className={styles.grayout}></div>
                </div>}
                <ViewItem item={item} noAnswer={noAnswer} totalStudent={doc.totalStudent} savePoll={savePoll} />
            </div>
        ))}
    </>);
});

import { ViewDefaultItem         } from "./ViewDefaultItem";
import { ViewHeadingItem         } from "./ViewHeadingItem";
import { ViewChoiceQuizItem      } from "./ViewChoiceQuizzItem";
import { ViewPollQuizItem        } from "./ViewPollQuizzItem";
import { ViewMatchQuizItem       } from "./ViewMatchQuizItem";
import { ViewNumericQuestionItem } from "./ViewNumericQuestionItem";
import { ViewDiscussionItem      } from "./ViewDiscussionItem";
import { ViewEmbedItem          } from './ViewEmbedItem';

const ViewItem: FC<{
    item        : ActItem,
    noAnswer    : boolean,
    totalStudent: number,
    savePoll   ?: ((pollItemId:string, votes:number) => Promise<any>)
}> = observer((props) => {
    switch (props.item.type) {
        case EActItemType.Heading        : return (<ViewHeadingItem          {...props} />);
        case EActItemType.ChoiceQuiz     : return (<ViewChoiceQuizItem       {...props} />);
        case EActItemType.PollQuiz       : return (<ViewPollQuizItem         {...props} />);
        case EActItemType.NumericQuestion: return (<ViewNumericQuestionItem  {...props} />);
        case EActItemType.MatchQuiz      : return (<ViewMatchQuizItem        {...props} />);
        case EActItemType.Media          : return (<ViewMediaItem            {...props} />);
        case EActItemType.Embed             : return (<ViewEmbedItem       {...props} />);
        case EActItemType.Discussion     : return (<ViewDiscussionItem       {...props} />);
        case EActItemType.TextQuiz       :
        default                          : return (<ViewDefaultItem          {...props} />);
    }
});
