import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { MediaTypeEnum } from '../../models/MediaTypeEnum';
import { ActItem } from '../../models/ActDoc';

import {useBaseStore} from "../../stores/useBaseStore";

import { Card, Typography, Icon, } from 'antd';
const { Title } = Typography;

import ReactPlayer from 'react-player';

import styles from "./ViewItem.module.less";
import { Link } from '../router/Links';
import classNames from 'classnames';

export const ViewDiscussionItem: FC<{item:ActItem}> = observer(({item}) => {
    // TODO: correct get sDiscussion
    // const sDiscussion = DiscussionStore.useContext();
    const {sDiscussion} = useBaseStore();
    const discussion = sDiscussion.mId2Item.get(item.discussionId);

    return (
        <div className="mb-lg">
            <div className={styles.subTitle}>
                <Title level={4}>
                    <div dangerouslySetInnerHTML={{ __html: item.title }} />
                </Title>
            </div>
            {discussion && (
                <Card bordered={false}
                    className={classNames("none-shadow", {
                        "unPublish": !discussion.isPublish,
                        "isClosed": discussion.isClosed,
                    })}>
                    <div className="view-discussion mb-lg flex items-center">
                        <Icon type="check-circle" theme="filled" className="icon-sm text-success mr-xs" />
                        <Link routeName="discussion" params={discussion.params}><span className="text-link-grey font-medium font-size-md">{discussion.title}</span></Link>
                    </div>
                    <div className="video">
                        {(  discussion.mediaType == MediaTypeEnum.VideoFile ||
                            discussion.mediaType == MediaTypeEnum.VideoLink ||
                            discussion.mediaType == MediaTypeEnum.AudioLink   ) ? (<>
                            <ReactPlayer url={discussion.link} controls pip />
                        </>) : ((discussion.mediaType == MediaTypeEnum.AudioFile) ? (<>
                            <video controls className="imgFile" src={discussion.link} />
                        </>) : null)}

                        {(discussion && discussion.mediaType == MediaTypeEnum.ImageFile && (
                            <div className="paddingBottom-lg">
                                <img src={discussion.link} className="imgFile w-auto" />
                            </div>
                        ))}
                    </div>
                    <div dangerouslySetInnerHTML={{__html:discussion.content}} />
                </Card>
            )}
        </div>
    );
});


