import React, { FC } from 'react';
import { observer } from 'mobx-react';

import { ActItem, EEmbedAppType } from '../../models/ActDoc';

import { Card, Typography, } from 'antd';
const { Title } = Typography;

import styles from "./ViewItem.module.less";
import { useTranslation } from 'react-i18next';

export const ViewEmbedItem: FC<{item:ActItem}> = observer(({item}) => {
    const {t} = useTranslation();
    return (
        <div className="mb-lg">
            <div className={styles.subTitle}>
                <Title level={4}>
                    <div dangerouslySetInnerHTML={{ __html: item.title }} />
                </Title>
            </div>
            <Card bordered={false} className="none-shadow">
                <div className="paddingBottom-lg">
                    {(  item.embedId && item.embedApp == EEmbedAppType.GoogleDrive ) ? (<>
                        <iframe width="100%" height="500px" src={item.googleFile ? item.googleFile.embedUrl :  
                            `https://drive.google.com/file/d/${item.embedId}/preview?usp=drive_web`}>
                        </iframe>
                    </>) : (item.embedLink && (<>
                        <iframe width="100%" height="500px" src={item.googleFile ? item.googleFile.embedUrl :  
                           `https://docs.google.com/viewer?url=${item.embedLink}&embedded=true`}>
                        </iframe>
                    </>))}
                </div>
                <div dangerouslySetInnerHTML={{__html:item.content}} />
            </Card>
        </div>
    );
});
