import React, { useCallback, FC } from 'react';
import { observer } from 'mobx-react';

import classNames from 'classnames';

import { Menu, Icon, message } from 'antd';
import {HeaderDropdown} from '../HeaderDropdown';

import { useBaseStore } from '../../stores/useBaseStore';

import styles from './index.module.less';

import {languageLabels, languageIcons} from "../../utils/i18nConfig";

export const SelectLang: FC<{ className?: string }> = observer(({ className = "" }) => {
  const store = useBaseStore();
  const changeLang = useCallback(async ({ key }) => {
    const err = await store.setLocale(key);
    if (err) message.error(err.message);
  }, []);

  const langMenu = (
    <Menu className={styles.menu} selectedKeys={[store.currentLocale]} onClick={changeLang}>
      {store.locales.map(locale => (
        <Menu.Item key={locale} lang={locale}>
          <span role="img" aria-label={languageLabels[locale]}>
            <img src={languageIcons[locale]} />
          </span>{' '}
          {languageLabels[locale]}
        </Menu.Item>
      ))}
    </Menu>
  );
  return (
    <HeaderDropdown overlay={langMenu} placement="bottomRight">
      <span className={classNames(styles.dropDown, className)}>
        <Icon type="global" title={"Languages"} className="icon-lg" />
      </span>
    </HeaderDropdown>
  );
});
