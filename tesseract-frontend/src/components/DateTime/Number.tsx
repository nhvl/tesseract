import { useMemo } from 'react';

import { useBaseStore } from '../../stores/useBaseStore';

export function useNumberTrans() {
    const store = useBaseStore();

    const intl = useMemo(() => new Intl.NumberFormat(store.currentLocale), [store.currentLocale])

    return useMemo(() => ({
        format: (value:number) => intl.format(value),
        currency: (value:number, currency?:string) => value.toLocaleString(store.currentLocale, {style:"currency", currency}),
        percent: (value?:number, minimumFractionDigits:number = 2) => value == null ? "" : value.toLocaleString(store.currentLocale, {style:"percent", minimumFractionDigits}),
    }), [store.currentLocale]);
};
