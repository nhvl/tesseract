import React, { FC, useMemo } from 'react';
import { observer } from 'mobx-react';

import { useBaseStore } from '../../stores/useBaseStore';

import moment from "moment";

import { Tooltip } from 'antd';

export function fromNow(value:number|Date|string) {
    return moment(value).fromNow()
}
export function calendar(value:number|Date|string) {
    return moment(value).calendar()
}
export function format(value:number|Date|string, format?: string) {
    return moment(value).format(format)
}

export const DateFromNowToolTip: FC<{value:number|string|Date}> = observer(({value}) => {
    const store = useBaseStore();
    const m = useMemo(() => moment(value), [value, store.currentLocale]);

    if (value == null) return null;

    return (<Tooltip title={m.format("L LT")}>
        <time dateTime={m.toJSON()}>{m.fromNow()}</time>
    </Tooltip>);
});

type ScreenBreak = "sm"|"md"|"lg"|"xl";

export const DateFromNowResponsive: FC<{value:number|string|Date, breakAt:ScreenBreak}> = observer(({value, breakAt="sm"}) => {
    const store = useBaseStore();
    const m = useMemo(() => moment(value), [value, store.currentLocale]);

    if (value == null) return null;

    return (<>
        <Tooltip title={m.format("L LT")}><time className={`${breakAt}:hidden`} dateTime={m.toJSON()}>{m.fromNow()}</time></Tooltip>
        <time className={`hidden ${breakAt}:inline`} dateTime={m.toJSON()}>
            <span className="paddingRight-xs">{m.fromNow()}</span>
            ({m.format("L LT")})
        </time>
    </>
    );
});

export const DateFromNow2Row: FC<{value:number|string|Date, breakAt:ScreenBreak}> = observer(({value, breakAt="sm"}) => {
    const store = useBaseStore();
    const m = useMemo(() => moment(value), [value, store.currentLocale]);

    if (value == null) return null;

    return (<>
        <Tooltip title={m.format("L LT")}><time className={`${breakAt}:hidden`} dateTime={m.toJSON()}>{m.fromNow()}</time></Tooltip>
        <time className={`hidden ${breakAt}:inline`} dateTime={m.toJSON()}>
            <div className="text-black">{m.fromNow()}</div>
            <div className="text-grey font-medium">{m.format("L LT")}</div>
        </time>
    </>
    );
});

export const DateFromNowLong: FC<{value:number|string|Date}> = observer(({value}) => {
    const store = useBaseStore();
    const m = useMemo(() => moment(value), [value, store.currentLocale]);

    if (value == null) return null;

    return (
        <time dateTime={m.toJSON()}>{m.fromNow()} ({m.format("L LT")})</time>
    );
});

export const DateFromNow: FC<{value:number|string|Date}> = observer(({value}) => {
    const store = useBaseStore();
    const m = useMemo(() => moment(value), [value, store.currentLocale]);

    if (value == null) return null;

    return (<time dateTime={m.toJSON()}>{m.fromNow()}</time>);
});


const DateTimeComp: FC<{value:number|string|Date|null|undefined, format: string}> = observer(({value, format}) => {
    const store = useBaseStore();
    const m = useMemo(() => moment(value!), [value, store.currentLocale]);

    if (value == null) return null;
    return (<span className="text-grey font-medium"><time dateTime={m.toJSON()}>{m.format(format)}</time></span>);
});

export const DateLabel: FC<{value:number|string|Date|null|undefined}> = observer(({value}) => (<DateTimeComp value={value} format="L"/>));
export const DateTime: FC<{value:number|string|Date|null|undefined}> = observer(({value}) => (<DateTimeComp value={value} format="L LT"/>))
export const LongDateLabel: FC<{value:number|string|Date|null|undefined}> = observer(({value}) => (<DateTimeComp value={value} format="ll LT"/>));

export const StyledDateTime: FC<{value:number|string|Date|null|undefined}> = observer(({value}) => {
    return (
        <span className="text-grey font-medium">
            <DateTime value={value}/>
        </span>
    );
});


export function useDateFromNow(value:number|string|Date) {
    const store = useBaseStore();
    return useMemo(() => fromNow(value), [value, store.currentLocale]);
}

export function useDateTrans() {
    const store = useBaseStore();
    return useMemo(() => ({
        fromNow: (value:number|string|Date) => fromNow(value),
        calendar: (value:number|string|Date) => calendar(value),
    }), [store.currentLocale]);
};

