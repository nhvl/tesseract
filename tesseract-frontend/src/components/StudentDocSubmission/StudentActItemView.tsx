import React, { FC } from "react";
import { observer } from "mobx-react";

import { StudentActItem } from "../../models/StudentActDoc";
import { EActItemType } from "../../models/ActDoc";

import { SViewMediaItem           } from "./SViewMediaItem";
import { SViewHeadingItem         } from "./SViewHeadingItem";
import { SViewChoiceQuizItem      } from "./SViewChoiceQuizzItem";
import { SViewPollQuizItem        } from "./SViewPollQuizzItem";
import { SViewMatchQuizItem       } from "./SViewMatchQuizItem";
import { SViewNumericQuestionItem } from "./SViewNumericQuestionItem";
import { SViewTextQuestionItem    } from "./SViewTextQuestionItem";
import { SViewDefaultItem         } from "./SViewDefaultItem";
import { ViewDiscussionItem       } from "./../ViewActDoc/ViewDiscussionItem";
import { ViewEmbedItem              } from "../ViewActDoc/ViewEmbedItem";

export const StudentActItemView: FC<{ item: StudentActItem, noAnswer?:boolean }> = observer((props) => {
    switch (props.item.type) {
        case EActItemType.Heading        : return (<SViewHeadingItem          {...props} />);
        case EActItemType.ChoiceQuiz     : return (<SViewChoiceQuizItem       {...props} />);
        case EActItemType.PollQuiz       : return (<SViewPollQuizItem         {...props} />);
        case EActItemType.NumericQuestion: return (<SViewNumericQuestionItem  {...props} />);
        case EActItemType.TextQuiz       : return (<SViewTextQuestionItem     {...props} />);
        case EActItemType.MatchQuiz      : return (<SViewMatchQuizItem        {...props} />);
        case EActItemType.Embed          : return (<ViewEmbedItem            {...props} />);
        case EActItemType.Media          : return (<SViewMediaItem            {...props} />);
        case EActItemType.Discussion     : return (< ViewDiscussionItem       {...props} />);
        default                          : return (<SViewDefaultItem          {...props} />);
    }
});
