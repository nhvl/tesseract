import React, { FC, useState, useCallback, useEffect } from "react";
import { observer } from "mobx-react";

import { useTranslation } from "react-i18next";

import { ActOption } from "../../models/ActDoc";
import { StudentActItem } from "../../models/StudentActDoc";

import { Form } from "antd";

import { Checkbox } from "../inputs/antd/Input";

import { SViewDefaultItem } from "./SViewDefaultItem";

import styles from "./SViewItem.module.less";

export const SViewChoiceQuizItem: FC<{ item: StudentActItem, noAnswer ?:boolean }> = observer((props) => {
    const { t } = useTranslation();
    const {item, noAnswer} = props;
    return (
        <SViewDefaultItem {...props}>
            {item.isMultipleAnswer && !noAnswer && 
                (<div className={'mb-sm'}>{t('form.activity.question.option.allowMultiple')}</div>)}
            {item.options && item.options.map((option, i) => (
                <OptionItem key={i} option={option} noAnswer={noAnswer} studentActItem={item} />
            ))}
        </SViewDefaultItem>
    );
});

const OptionItem: FC<{ option: ActOption, noAnswer?:boolean, studentActItem: StudentActItem }> = observer(({ option, noAnswer, studentActItem }) => {
    const setCheckedOption = useCallback((v: boolean) => {
        studentActItem.set_optionsAnswer(option, v);
    },[option, studentActItem]);
    const validateStatus = (noAnswer || !studentActItem.get_optionsAnswer(option)) ? "" : (option.isCorrect ? "success" : "error");

    return (
        <Form.Item validateStatus={validateStatus} hasFeedback={!noAnswer}
        className={`${styles.validationIcon} ${styles.quizChoice} mb-sm`}>
            <Checkbox checked={studentActItem.get_optionsAnswer(option)} onChange={setCheckedOption} disabled={!noAnswer}>
                { (option.image && option.image != "") && <img src={option.image} className="imgFile-sm mb-sm block" />}
                <div className={`${styles.label}`}>
                    {option.label}
                </div>
            </Checkbox>
        </Form.Item>
    );
});
