import React, { FC, useCallback } from "react";
import { observer } from "mobx-react";

import { PollQuizItem } from "../../models/ActDoc";
import { StudentActItem } from "../../models/StudentActDoc";

import { Radio } from "antd";

import { SViewDefaultItem } from "./SViewDefaultItem";

import styles from "./SViewItem.module.less";

export const SViewPollQuizItem: FC<{ item: StudentActItem, noAnswer?:boolean }> = observer((props) => {
    const {item, noAnswer} = props;
    const handleChange = useCallback((e)=> {
        let value = e.target.value;
        item.set_pollItemsAnswer(value);
    },[item]);
    return (
        <SViewDefaultItem {...props}>
            <Radio.Group value={item.pollItemsAnswer} onChange={handleChange}
                disabled={!noAnswer}
                className={styles.poll}>
                {item.pollItems && item.pollItems.map((item, i) => (
                    <Radio value={i} key={i} className={`${styles.radioStyle} flex items-center`}>
                        <PollContent item={item} />
                    </Radio>
                ))}
            </Radio.Group>
        </SViewDefaultItem>
    );
});

const PollContent: FC<{ item: PollQuizItem }> = observer(({ item }) => {
     return (
        <div className="leading-normal">
            {!!item.image && (<img src={item.image} className="imgFile-sm mb-sm block" />)}
            {item.label}
        </div>
    );
});
