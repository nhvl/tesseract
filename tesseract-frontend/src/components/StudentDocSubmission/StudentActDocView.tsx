import React, { FC } from "react";
import { observer } from "mobx-react";

import { StudentActDoc } from "../../models/StudentActDoc";

import { Card, Typography, } from "antd";
const { Title } = Typography;

import { StudentActItemView } from "./StudentActItemView";

import styles from "./StudentDocSubmission.module.less";

export const StudentActDocView: FC<{ doc: StudentActDoc, noAnswer?:boolean }> = observer(({ doc, noAnswer }) => {
    return (<>
        <Card bordered={false} className="none-shadow paddingBottom-lg"
            cover={!!doc.banner && (<div className={styles.banner}><img src={doc.banner} /></div>)}>
            <Title level={1}>
                <div dangerouslySetInnerHTML={{ __html: doc.title }} />
            </Title>
            {doc.summary && (
                <div className="font-size-lg text-grey description"
                    dangerouslySetInnerHTML={{ __html: doc.summary }} />
            )}
        </Card>

        {doc.items.map(item => (
            <StudentActItemView item={item} key={item.id} />
        ))}
    </>);
});

