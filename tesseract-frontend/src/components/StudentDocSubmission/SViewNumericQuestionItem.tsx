import React, { FC } from "react";
import { observer } from "mobx-react";

import { StudentActItem } from "../../models/StudentActDoc";

import { Card, Typography, Form, InputNumber, } from "antd";
const { Title } = Typography;

import styles from "./SViewItem.module.less";

export const SViewNumericQuestionItem: FC<{ item: StudentActItem, noAnswer?:boolean }> = observer(({ item, noAnswer }) => {
    const validateStatus = (item.numberAnswer == null || noAnswer) ? "" : (
        ((item.numericMin == null || item.numericMin <= item.numberAnswer) &&
         (item.numericMax == null || item.numberAnswer <= item.numericMax)) ? "success" : "error"
    );

    return (
        <div className="mb-lg">
            <div className={styles.subTitle}>
                <Title level={4}><div dangerouslySetInnerHTML={{__html:item.title}} /></Title>
            </div>
            <Card bordered={false} className="none-shadow"
                cover={(item.image && (<div className="paddingBottom-lg"><img src={item.image} className="imgFile w-auto" /></div>))}>
                <div dangerouslySetInnerHTML={{ __html: item.content }} />
            </Card>
            <Form.Item validateStatus={validateStatus} hasFeedback={!noAnswer} className={`${styles.validationIcon} mb-sm`}>
                <InputNumber value={item.numberAnswer} onChange={item.set_numericAnswer} disabled={!noAnswer}/>
            </Form.Item>
        </div>
    );
});
