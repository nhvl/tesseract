import React, { FC } from "react";
import { observer } from "mobx-react";

import { StudentActItem } from "../../models/StudentActDoc";

import { SViewDefaultItem } from "./SViewDefaultItem";

export const SViewHeadingItem: FC<{ item: StudentActItem }> = observer((props) => {
    return (
        <SViewDefaultItem {...props} headerLevel={2} />
    );
});
