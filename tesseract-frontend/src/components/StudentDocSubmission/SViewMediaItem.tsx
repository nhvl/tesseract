import React, { FC } from "react";
import { observer } from "mobx-react";

import { MediaTypeEnum } from "../../models/MediaTypeEnum";
import { StudentActItem } from "../../models/StudentActDoc";

import { Card, Typography, } from "antd";
const { Title } = Typography;

import ReactPlayer from "react-player";

import styles from "./SViewItem.module.less";

export const SViewMediaItem: FC<{item:StudentActItem}> = observer(({item}) => {

    return (<>
        <div className={styles.subTitle}>
            <Title level={4}>
                <div dangerouslySetInnerHTML={{ __html: item.title }} />
            </Title>
        </div>
        <Card bordered={false} className="mb-lg none-shadow"
            cover={(item.mediaType == MediaTypeEnum.ImageFile && (
                <div className="paddingBottom-lg">
                    <img src={item.link} className="imgFile w-auto" />
                </div>
            ))}>
            <div className="paddingBottom-lg">
                {(  item.mediaType == MediaTypeEnum.VideoFile ||
                    item.mediaType == MediaTypeEnum.VideoLink ||
                    item.mediaType == MediaTypeEnum.AudioLink   ) ? (<>
                    <ReactPlayer url={item.link} controls pip />
                </>) : ((item.mediaType == MediaTypeEnum.AudioFile) ? (<>
                    <video controls className="imgFile" src={item.link} poster={item.image || undefined} />
                </>) : null)}
            </div>
            <div dangerouslySetInnerHTML={{__html:item.content}} />
        </Card>
    </>);
});
