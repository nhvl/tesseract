import React, { FC, useState, useCallback, useMemo } from "react";
import { observer } from "mobx-react";

import { StudentActItem } from "../../models/StudentActDoc";

import { HtmlInput } from "../inputs/HtmlInput";

import { SViewDefaultItem } from "./SViewDefaultItem";

export const SViewTextQuestionItem: FC<{ item: StudentActItem, noAnswer?: boolean }> = observer((props) => {
    const [, setEditting] = useState(false);
    const endEdit = useCallback(() => setEditting(false), [setEditting]);
    const contentHtml = useMemo(() => ({__html:props.item.textAnswer}), [props.item.textAnswer]);
    return (
        <SViewDefaultItem {...props}>
            {props.noAnswer
                ? (<HtmlInput value={props.item.textAnswer} onChange={props.item.set_textAnswer}
                    onBlur={endEdit} placeholder={"Your answer"} />)
                : (<div dangerouslySetInnerHTML={contentHtml} />)}
        </SViewDefaultItem>
    )
});
