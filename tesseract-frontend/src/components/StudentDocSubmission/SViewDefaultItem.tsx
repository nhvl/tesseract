import React, { FC, useMemo } from "react";
import { observer } from "mobx-react";

import { StudentActItem } from "../../models/StudentActDoc";

import { Card, Typography, } from "antd";
const { Title } = Typography;

import styles from "./SViewItem.module.less";

export const SViewDefaultItem: FC<{
    item        : StudentActItem,
    headerLevel?: (1|2|3|4),
}> = observer(({
    item,
    headerLevel,
    children,
}) => {
    const titleHtml = useMemo(() => ({__html:item.title}), [item.title]);
    const contentHtml = useMemo(() => ({__html:item.content}), [item.content]);

    return (
        <div className="mb-lg">
            <Title level={headerLevel || 4} className={styles.subTitle}>
                <div dangerouslySetInnerHTML={titleHtml} />
            </Title>
            <Card bordered={false} className="none-shadow"
                cover={(item.image && (<img src={item.image} className="imgFile w-auto paddingBottom-lg" />))}>
                <div dangerouslySetInnerHTML={contentHtml} />
            </Card>
            {children}
        </div>
    );
});
