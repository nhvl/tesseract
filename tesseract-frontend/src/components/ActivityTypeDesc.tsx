import { FC } from "react";
import { useTranslation } from "react-i18next";

import { ActivityType } from "../models/Activity";

//@ts-ignore
export const ActivityTypeDesc: FC<{value:ActivityType}> = (({value}) => {
    const {t} = useTranslation();
    switch(value) {
        case ActivityType.Activity: return t('app.activities.activities');
        case ActivityType.Assignment: return t('app.activities.assignments');
        case ActivityType.Assessment: return t('app.activities.assessments');
    }
    return String(value)
});
