import React, { FC, FormEvent, useCallback, useEffect, useState } from "react";
import { observer } from "mobx-react";
import {GradeRange} from "../../models/GradeRange";
import { useTranslation } from "react-i18next";
import { Form, Button, Card, Row, Modal as AntdModal, Col, message, Spin, Divider, Breadcrumb, Icon, Checkbox } from 'antd';
const FormItem = Form.Item;
import { Input, TextArea } from "../inputs/antd/Input";
import { InputNumber } from "../inputs/antd/InputNumber";
import styles from "./index.module.less";

const GradeRangeRow: FC<{
    item: GradeRange,
    index:number,
    gradeRanges:GradeRange[],
    isViewMode:boolean
}> = observer(({ item, index, gradeRanges, isViewMode }) => {
    const [maxValue, setMaxValue] = useState(Infinity);
    const {t} = useTranslation();
    useEffect(()=>{
        calculateMaxValue();
    }, [item, index, isViewMode]);
    const removeOption = useCallback(() => {
        gradeRanges.splice(index, 1);
    }, [item, index, isViewMode]);
    const onFocusOption = useCallback((index: number) => {
        if (index >= (gradeRanges.length - 1)) {
            gradeRanges.push(new GradeRange({percentGrade:gradeRanges[index].percentGrade-1}));
        }
    }, [item, index, isViewMode]);
    const calculateMaxValue = useCallback(()=>{
        if(index == 0){
            setMaxValue(100);
        }
        else{
            setMaxValue(gradeRanges[index-1].percentGrade-1);
            let currentValue: number = gradeRanges[index].percentGrade;
            for(var i = index + 1; i < gradeRanges.length; i++){
                if(gradeRanges[i].percentGrade >= currentValue){
                    currentValue--;
                    gradeRanges[i].set_percentGrade(currentValue);
                    if(currentValue==1) break;
                }
            }
        }
    }, [item, index, isViewMode]);
    const isRequired = useCallback(()=> {
        return ((item.letterGrade == "" || item.percentGrade == null) && index < gradeRanges.length-1);
    }, [item, index, isViewMode]);
    const prefixTerm = useCallback(()=>{
        if(index < gradeRanges.length-1){
            return isViewMode
                    ? t("app.classes.gradeRange.viewTo", {min: gradeRanges[index+1].percentGrade +1, max: gradeRanges[index].percentGrade})
                    : t("app.classes.gradeRange.editTo", {min: gradeRanges[index+1].percentGrade +1});
        }
        return isViewMode
                ? t("app.classes.gradeRange.viewBelow", {max: gradeRanges[index].percentGrade})
                : t("app.classes.gradeRange.editBelow");
    }, [item, index, isViewMode]);
    return (<FormItem className={isViewMode ? `${styles.viewMode}` : "mb-sm"}>
            <div className="flex items-center">
                <div className={styles.grade}>
                    <label hidden={!isViewMode}>{item.letterGrade}</label>
                    <Input hidden={isViewMode} required={isRequired()} value={item.letterGrade} onChange={item.set_letterGrade}
                                    type="text"
                                    maxLength={10}
                                    size="large" className={styles.inputGrade} />
                </div>
                <div className="flex-1">
                    <div className="flex items-center">
                        <label className={`${styles.labelRange} mr-xs`}>{prefixTerm()}</label>
                        <span hidden={isViewMode}><InputNumber required={isRequired()} value={item.percentGrade} onChange={item.set_percentGrade} step={1} min={index == 0 ? 100 : 1} max={maxValue}
                                        size="large" className={styles.inputWidth} onFocus={() => onFocusOption(index)} onBlur={()=>calculateMaxValue()} />
                                        </span>

                    </div>
                </div>
                <div hidden={isViewMode} className={styles.delete}>
                    <a onClick={removeOption} hidden={index==0}>
                        <Icon type="minus-square" className="icon-sm" />
                    </a>
                </div>
            </div>
            </FormItem>);
});
export const GradeRangeModal: FC<{useDefaultGradesRange:boolean, onSubmit: ()=>void, onCancel:()=>void, modalGradeRange:GradeRange[], visible:boolean }> = observer(({useDefaultGradesRange, onSubmit, onCancel, modalGradeRange, visible}) => {
    const {t} = useTranslation();
    const onSubmitHandler = useCallback(async (event:FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        onSubmit();
    },[]);
    return (<AntdModal
            title={useDefaultGradesRange ? t("app.classes.gradeRange.modalDefault") : t("app.classes.gradeRange.modalCustom")}
            visible={visible}
            onCancel={onCancel}
            width={400}
            footer={[
                <Button key="back" onClick={onCancel}>{t('form.cancel')}</Button>,
                <Button key="submit" type="primary" htmlType="submit" form="GradeRangeEditor">{t('form.ok')}</Button>
              ]}>
            <Form onSubmit={onSubmitHandler} id="GradeRangeEditor">
                <div className="flex mb-sm">
                    <div className={styles.grade}>
                        <label className="text-black font-medium">{t("app.classes.letterGrade")}:</label>
                    </div>
                    <div className="flex-1">
                        <label className="text-black font-medium">{t("app.classes.percentGrade")}:</label>
                    </div>
                </div>
                    {modalGradeRange.map((item: GradeRange, index: number) => (
                        <GradeRangeRow item={item} index={index} key={index} gradeRanges={modalGradeRange} isViewMode={useDefaultGradesRange} />))}
            </Form>
        </AntdModal>);
})

