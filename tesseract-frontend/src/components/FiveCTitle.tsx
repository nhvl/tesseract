import React, { FC, ReactNode } from "react";
import { observer } from "mobx-react";

import { FiveC, FiveCValues } from "../models/ActivityScore";

import { useTranslation } from "react-i18next";

export const FiveCValueDesc: FC<{value: FiveCValues}> = observer(({value}) => {
    const {t} = useTranslation();

    switch (value) {
        case FiveCValues.Low   : return t('app.activities.score.lowLevel'   ) as any;
        case FiveCValues.Medium: return t('app.activities.score.mediumLevel') as any;
        case FiveCValues.High  : return t('app.activities.score.highLevel'  ) as any;
    }

    return (value) as any;
});

export const FiveCTitle: FC<{value: FiveC}> = observer(({value}) => {
    const {t} = useTranslation();

    switch (value) {
        case FiveC.Communication   : return t('app.activities.score.Communication');
        case FiveC.Collaboration   : return t('app.activities.score.Collaboration');
        case FiveC.Character       : return t('app.activities.score.Character');
        case FiveC.Creativity      : return t('app.activities.score.Creativity');
        case FiveC.CriticalThinking: return t('app.activities.score.CriticalThinking');
    }

    return value;
});
