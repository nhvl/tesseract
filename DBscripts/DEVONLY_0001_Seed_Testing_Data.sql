﻿IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ActivityCategoryId', N'Color', N'CreatedBy', N'DateCreated', N'DateUpdated', N'FacultyId', N'IsDeleted', N'IsGraded', N'MaxScore', N'Name', N'UpdatedBy', N'Weight') AND [object_id] = OBJECT_ID(N'[ActivityCategories]'))
    SET IDENTITY_INSERT [ActivityCategories] ON;
INSERT INTO [ActivityCategories] ([ActivityCategoryId], [Color], [CreatedBy], [DateCreated], [DateUpdated], [FacultyId], [IsDeleted], [IsGraded], [MaxScore], [Name], [UpdatedBy], [Weight])
VALUES (CAST(3 AS bigint), N'#fce5cd', NULL, NULL, NULL, CAST(5 AS bigint), 0, 1, 100.0, N'Big Project', NULL, 1.0),
(CAST(1 AS bigint), N'#e6b8af', NULL, NULL, NULL, CAST(5 AS bigint), 0, 1, 10.0, N'Daily homework', NULL, 1.0),
(CAST(2 AS bigint), N'#f4cccc', NULL, NULL, NULL, CAST(5 AS bigint), 0, 0, 10.0, N'Reading Assignment', NULL, 1.0);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ActivityCategoryId', N'Color', N'CreatedBy', N'DateCreated', N'DateUpdated', N'FacultyId', N'IsDeleted', N'IsGraded', N'MaxScore', N'Name', N'UpdatedBy', N'Weight') AND [object_id] = OBJECT_ID(N'[ActivityCategories]'))
    SET IDENTITY_INSERT [ActivityCategories] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'DistrictId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'DistrictName', N'Domain', N'IsDeleted', N'LogoUrl', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Districts]'))
    SET IDENTITY_INSERT [Districts] ON;
INSERT INTO [Districts] ([DistrictId], [CreatedBy], [DateCreated], [DateUpdated], [DistrictName], [Domain], [IsDeleted], [LogoUrl], [UpdatedBy])
VALUES (CAST(1 AS bigint), NULL, NULL, NULL, N'Test District', N'district1', 0, N'', NULL),
(CAST(2 AS bigint), NULL, NULL, NULL, N'Test District', N'district2', 0, N'', NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'DistrictId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'DistrictName', N'Domain', N'IsDeleted', N'LogoUrl', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Districts]'))
    SET IDENTITY_INSERT [Districts] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'EmailAccountId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'DisplayName', N'Email', N'EnableSsl', N'Host', N'IsDeleted', N'Password', N'Port', N'UpdatedBy', N'UseDefaultCredentials', N'Username') AND [object_id] = OBJECT_ID(N'[EmailAccounts]'))
    SET IDENTITY_INSERT [EmailAccounts] ON;
INSERT INTO [EmailAccounts] ([EmailAccountId], [CreatedBy], [DateCreated], [DateUpdated], [DisplayName], [Email], [EnableSsl], [Host], [IsDeleted], [Password], [Port], [UpdatedBy], [UseDefaultCredentials], [Username])
VALUES (1, CAST(1 AS bigint), '2019-04-24T06:31:51.8250000Z', NULL, N'Tesseract', N'info@tesseract.com', 1, N'smtp.sendgrid.net', 0, N'lenguyen@894047', 587, NULL, 0, N'maxkingsouth2016');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'EmailAccountId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'DisplayName', N'Email', N'EnableSsl', N'Host', N'IsDeleted', N'Password', N'Port', N'UpdatedBy', N'UseDefaultCredentials', N'Username') AND [object_id] = OBJECT_ID(N'[EmailAccounts]'))
    SET IDENTITY_INSERT [EmailAccounts] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'EmailTemplateId', N'BccEmailAddresses', N'Body', N'CreatedBy', N'DateCreated', N'DateUpdated', N'IsDeleted', N'Name', N'Subject', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[EmailTemplates]'))
    SET IDENTITY_INSERT [EmailTemplates] ON;
INSERT INTO [EmailTemplates] ([EmailTemplateId], [BccEmailAddresses], [Body], [CreatedBy], [DateCreated], [DateUpdated], [IsDeleted], [Name], [Subject], [UpdatedBy])
VALUES (1, NULL, N'<p>If you did not request to reset your password for your {{SCHOOL_DISTRICT_NAME}} portal, you may ignore this email.</p><p>To reset your password, <a href=''{{link}}''>[Click here]</a> or copy and paste this link into your browser:{{link}}</p>', CAST(1 AS bigint), '2019-04-24T06:31:51.8250000Z', NULL, 0, N'Account.ForgotPassword', N'{{SCHOOL_DISTRICT_NAME}} - Your password reset instructions', NULL),
(3, NULL, N'<p>{{STUDENT_NAME}}</p><p>{{PARENT_NAME}} ({{PARENT_EMAIL}}) has registered as your parent.</p><p>If this is a mistake, please contact your administrator.</p>', CAST(1 AS bigint), '2019-06-12T00:00:00.0000000Z', NULL, 0, N'Account.NotifyStudent', N'{{PARENT_NAME}} has registered as your parent', NULL),
(2, NULL, N'<p>Welcome to Tesseract!</p><p>To active your account, <a href=''{{link}}''>[Click here]</a> or copy and paste this link into your browser:{{link}}</p>', CAST(1 AS bigint), '2019-04-24T06:31:51.8250000Z', NULL, 0, N'Account.ConfirmEmail', N'Welcome to Tesseract', NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'EmailTemplateId', N'BccEmailAddresses', N'Body', N'CreatedBy', N'DateCreated', N'DateUpdated', N'IsDeleted', N'Name', N'Subject', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[EmailTemplates]'))
    SET IDENTITY_INSERT [EmailTemplates] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'FacultyId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'ExternalId', N'IsDeleted', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Faculty]'))
    SET IDENTITY_INSERT [Faculty] ON;
INSERT INTO [Faculty] ([FacultyId], [CreatedBy], [DateCreated], [DateUpdated], [ExternalId], [IsDeleted], [UpdatedBy])
VALUES (CAST(5 AS bigint), NULL, NULL, NULL, N'Test External Teacher', 0, NULL),
(CAST(6 AS bigint), NULL, NULL, NULL, N'Test External Teacher 2', 0, NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'FacultyId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'ExternalId', N'IsDeleted', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Faculty]'))
    SET IDENTITY_INSERT [Faculty] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'GradingTermId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'EndDate', N'IsDeleted', N'Name', N'SchoolId', N'StartDate', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[GradingTerms]'))
    SET IDENTITY_INSERT [GradingTerms] ON;
INSERT INTO [GradingTerms] ([GradingTermId], [CreatedBy], [DateCreated], [DateUpdated], [EndDate], [IsDeleted], [Name], [SchoolId], [StartDate], [UpdatedBy])
VALUES (CAST(1 AS bigint), NULL, NULL, NULL, '2019-03-31T00:00:00.0000000Z', 0, N'Spring 2019', CAST(1 AS bigint), '2019-01-01T00:00:00.0000000Z', NULL),
(CAST(2 AS bigint), NULL, NULL, NULL, '2019-06-30T00:00:00.0000000Z', 0, N'Summer 2019', CAST(1 AS bigint), '2019-04-01T00:00:00.0000000Z', NULL),
(CAST(3 AS bigint), NULL, NULL, NULL, '2019-09-30T00:00:00.0000000Z', 0, N'Fall 2019', CAST(1 AS bigint), '2019-07-01T00:00:00.0000000Z', NULL),
(CAST(4 AS bigint), NULL, NULL, NULL, '2019-12-31T00:00:00.0000000Z', 0, N'Winter 2019', CAST(1 AS bigint), '2019-10-01T00:00:00.0000000Z', NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'GradingTermId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'EndDate', N'IsDeleted', N'Name', N'SchoolId', N'StartDate', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[GradingTerms]'))
    SET IDENTITY_INSERT [GradingTerms] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'KeyValueID', N'DistrictID', N'IsActive', N'KeyName', N'OrderIndex', N'SchoolID', N'TextValue') AND [object_id] = OBJECT_ID(N'[KeyValues]'))
    SET IDENTITY_INSERT [KeyValues] ON;
INSERT INTO [KeyValues] ([KeyValueID], [DistrictID], [IsActive], [KeyName], [OrderIndex], [SchoolID], [TextValue])
VALUES (CAST(28 AS bigint), NULL, 0, N'ActivityCategory_3', 29, CAST(1 AS bigint), N'#fce5cd'),
(CAST(29 AS bigint), NULL, 0, N'ActivityCategory_4', 30, CAST(1 AS bigint), N'#fff2cc'),
(CAST(30 AS bigint), NULL, 0, N'ActivityCategory_5', 31, CAST(1 AS bigint), N'#d9ead3'),
(CAST(31 AS bigint), NULL, 0, N'ActivityCategory_6', 32, CAST(1 AS bigint), N'#d0e0e3'),
(CAST(34 AS bigint), NULL, 0, N'ActivityCategory_9', 35, CAST(1 AS bigint), N'#d9d2e9'),
(CAST(33 AS bigint), NULL, 0, N'ActivityCategory_8', 34, CAST(1 AS bigint), N'#cfe2f3'),
(CAST(35 AS bigint), NULL, 0, N'ActivityCategory_10', 36, CAST(1 AS bigint), N'#ead1dc'),
(CAST(27 AS bigint), NULL, 0, N'ActivityCategory_2', 28, CAST(1 AS bigint), N'#f4cccc'),
(CAST(32 AS bigint), NULL, 0, N'ActivityCategory_7', 33, CAST(1 AS bigint), N'#c9daf8'),
(CAST(26 AS bigint), NULL, 0, N'ActivityCategory_1', 27, CAST(1 AS bigint), N'#e6b8af'),
(CAST(1 AS bigint), NULL, 0, N'CommunicationColor', 2, CAST(1 AS bigint), N'FF7D00'),
(CAST(24 AS bigint), NULL, 0, N'D-', 25, CAST(1 AS bigint), N'62'),
(CAST(25 AS bigint), NULL, 0, N'F', 26, CAST(1 AS bigint), N'60'),
(CAST(2 AS bigint), NULL, 0, N'CollaborationColor', 3, CAST(1 AS bigint), N'41C4FF'),
(CAST(3 AS bigint), NULL, 0, N'CharacterColor', 4, CAST(1 AS bigint), N'3DD94A'),
(CAST(4 AS bigint), NULL, 0, N'CreativityColor', 5, CAST(1 AS bigint), N'9574EA'),
(CAST(6 AS bigint), CAST(1 AS bigint), 0, N'CommunicationColor', 7, NULL, N'FF7D00'),
(CAST(7 AS bigint), CAST(1 AS bigint), 0, N'CollaborationColor', 8, NULL, N'41C4FF'),
(CAST(8 AS bigint), CAST(1 AS bigint), 0, N'CharacterColor', 9, NULL, N'3DD94A'),
(CAST(9 AS bigint), CAST(1 AS bigint), 0, N'CreativityColor', 10, NULL, N'9574EA'),
(CAST(10 AS bigint), CAST(1 AS bigint), 0, N'CriticalThinkingColor', 11, NULL, N'FCEE21'),
(CAST(11 AS bigint), NULL, 0, N'GoogleTagManager', 12, CAST(1 AS bigint), N'GTM-TM4ZXV5'),
(CAST(12 AS bigint), CAST(1 AS bigint), 0, N'GoogleTagManager', 13, NULL, N'GTM-N3QFKRR'),
(CAST(5 AS bigint), NULL, 0, N'CriticalThinkingColor', 6, CAST(1 AS bigint), N'FCEE21'),
(CAST(14 AS bigint), NULL, 0, N'A', 15, CAST(1 AS bigint), N'96'),
(CAST(13 AS bigint), NULL, 0, N'A+', 14, CAST(1 AS bigint), N'100'),
(CAST(22 AS bigint), NULL, 0, N'D+', 23, CAST(1 AS bigint), N'69'),
(CAST(23 AS bigint), NULL, 0, N'D', 24, CAST(1 AS bigint), N'66'),
(CAST(20 AS bigint), NULL, 0, N'C', 21, CAST(1 AS bigint), N'76'),
(CAST(19 AS bigint), NULL, 0, N'C+', 20, CAST(1 AS bigint), N'79'),
(CAST(21 AS bigint), NULL, 0, N'C-', 22, CAST(1 AS bigint), N'72'),
(CAST(17 AS bigint), NULL, 0, N'B', 18, CAST(1 AS bigint), N'86'),
(CAST(16 AS bigint), NULL, 0, N'B+', 17, CAST(1 AS bigint), N'89'),
(CAST(15 AS bigint), NULL, 0, N'A-', 16, CAST(1 AS bigint), N'92'),
(CAST(18 AS bigint), NULL, 0, N'B-', 19, CAST(1 AS bigint), N'82');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'KeyValueID', N'DistrictID', N'IsActive', N'KeyName', N'OrderIndex', N'SchoolID', N'TextValue') AND [object_id] = OBJECT_ID(N'[KeyValues]'))
    SET IDENTITY_INSERT [KeyValues] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'RecordId', N'Key', N'Locale', N'Value') AND [object_id] = OBJECT_ID(N'[Localizations]'))
    SET IDENTITY_INSERT [Localizations] ON;
INSERT INTO [Localizations] ([RecordId], [Key], [Locale], [Value])
VALUES (CAST(220 AS bigint), N'app.setting.themecolor.cyan', N'en-US', N'Cyan'),
(CAST(221 AS bigint), N'app.setting.themecolor.green', N'en-US', N'Polar Green'),
(CAST(219 AS bigint), N'app.setting.themecolor.sunset', N'en-US', N'Sunset Orange'),
(CAST(218 AS bigint), N'app.setting.themecolor.volcano', N'en-US', N'Volcano'),
(CAST(217 AS bigint), N'app.setting.themecolor.dust', N'en-US', N'Dust Red'),
(CAST(216 AS bigint), N'app.setting.themecolor', N'en-US', N'Theme Color'),
(CAST(215 AS bigint), N'app.setting.content-width.fluid', N'en-US', N'Fluid'),
(CAST(214 AS bigint), N'app.setting.content-width.fixed', N'en-US', N'Fixed'),
(CAST(210 AS bigint), N'app.setting.pagestyle', N'en-US', N'Page style setting'),
(CAST(212 AS bigint), N'app.setting.pagestyle.light', N'en-US', N'Light style'),
(CAST(211 AS bigint), N'app.setting.pagestyle.dark', N'en-US', N'Dark style'),
(CAST(209 AS bigint), N'app.result.success.btn-print', N'en-US', N'Print'),
(CAST(208 AS bigint), N'app.result.success.btn-project', N'en-US', N'View project'),
(CAST(207 AS bigint), N'app.result.success.btn-return', N'en-US', N'Back to list'),
(CAST(222 AS bigint), N'app.setting.themecolor.daybreak', N'en-US', N'Daybreak Blue (default)'),
(CAST(206 AS bigint), N'app.result.success.step4-title', N'en-US', N'Finish'),
(CAST(213 AS bigint), N'app.setting.content-width', N'en-US', N'Content Width'),
(CAST(223 AS bigint), N'app.setting.themecolor.geekblue', N'en-US', N'Geek Glue'),
(CAST(235 AS bigint), N'app.setting.copy', N'en-US', N'Copy Setting'),
(CAST(225 AS bigint), N'app.setting.navigationmode', N'en-US', N'Navigation Mode'),
(CAST(205 AS bigint), N'app.result.success.step3-title', N'en-US', N'Financial review'),
(CAST(241 AS bigint), N'app.settings.menuMap.notification', N'en-US', N'New Message Notification'),
(CAST(240 AS bigint), N'app.settings.menuMap.binding', N'en-US', N'Account Binding'),
(CAST(239 AS bigint), N'app.settings.menuMap.security', N'en-US', N'Security Settings'),
(CAST(238 AS bigint), N'app.settings.menuMap.basic', N'en-US', N'Basic Settings'),
(CAST(237 AS bigint), N'app.setting.production.hint', N'en-US', N'Setting panel shows in development environment only, please manually modify'),
(CAST(236 AS bigint), N'app.setting.copyinfo', N'en-US', N'copy success，please replace defaultSettings in src/models/setting.js'),
(CAST(224 AS bigint), N'app.setting.themecolor.purple', N'en-US', N'Golden Purple'),
(CAST(234 AS bigint), N'app.setting.weakmode', N'en-US', N'Weak Mode'),
(CAST(232 AS bigint), N'app.setting.hideheader.hint', N'en-US', N'Works when Hidden Header is enabled'),
(CAST(231 AS bigint), N'app.setting.hideheader', N'en-US', N'Hidden Header when scrolling'),
(CAST(230 AS bigint), N'app.setting.fixedsidebar.hint', N'en-US', N'Works on Side Menu Layout'),
(CAST(229 AS bigint), N'app.setting.fixedsidebar', N'en-US', N'Fixed Sidebar'),
(CAST(228 AS bigint), N'app.setting.fixedheader', N'en-US', N'Fixed Header'),
(CAST(227 AS bigint), N'app.setting.topmenu', N'en-US', N'Top Menu Layout'),
(CAST(226 AS bigint), N'app.setting.sidemenu', N'en-US', N'Side Menu Layout'),
(CAST(233 AS bigint), N'app.setting.othersettings', N'en-US', N'Other Settings'),
(CAST(204 AS bigint), N'app.result.success.step2-extra', N'en-US', N'Urge'),
(CAST(179 AS bigint), N'app.monitor.fast-food', N'en-US', N'Fast food'),
(CAST(202 AS bigint), N'app.result.success.step2-title', N'en-US', N'Departmental preliminary review'),
(CAST(180 AS bigint), N'app.monitor.western-food', N'en-US', N'Western food'),
(CAST(242 AS bigint), N'app.settings.basic.avatar', N'en-US', N'Avatar'),
(CAST(178 AS bigint), N'app.monitor.proportion-per-category', N'en-US', N'Proportion Per Category'),
(CAST(177 AS bigint), N'app.monitor.ratio', N'en-US', N'Ratio'),
(CAST(176 AS bigint), N'app.monitor.efficiency', N'en-US', N'Efficiency'),
(CAST(175 AS bigint), N'app.monitor.activity-forecast', N'en-US', N'Activity forecast'),
(CAST(174 AS bigint), N'app.monitor.total-transactions-per-second', N'en-US', N'Total transactions per second'),
(CAST(181 AS bigint), N'app.monitor.hot-pot', N'en-US', N'Hot pot'),
(CAST(173 AS bigint), N'app.monitor.remaining-time', N'en-US', N'Remaining time of activity'),
(CAST(171 AS bigint), N'app.monitor.total-transactions', N'en-US', N'Total transactions today'),
(CAST(170 AS bigint), N'app.monitor.trading-activity', N'en-US', N'Real-Time Trading Activity'),
(CAST(169 AS bigint), N'menu.account.logout', N'en-US', N'Logout'),
(CAST(168 AS bigint), N'menu.account.trigger', N'en-US', N'Trigger Error'),
(CAST(167 AS bigint), N'menu.account.settings', N'en-US', N'Account Settings'),
(CAST(166 AS bigint), N'menu.account.center', N'en-US', N'Account Center'),
(CAST(165 AS bigint), N'menu.account', N'en-US', N'Account'),
(CAST(172 AS bigint), N'app.monitor.sales-target', N'en-US', N'Sales target completion rate'),
(CAST(182 AS bigint), N'app.monitor.waiting-for-implementation', N'en-US', N'Waiting for implementation'),
(CAST(183 AS bigint), N'app.monitor.popular-searches', N'en-US', N'Popular Searches'),
(CAST(184 AS bigint), N'app.monitor.resource-surplus', N'en-US', N'Resource Surplus'),
(CAST(201 AS bigint), N'app.result.success.step1-operator', N'en-US', N'Qu Lili'),
(CAST(200 AS bigint), N'app.result.success.step1-title', N'en-US', N'Create project'),
(CAST(199 AS bigint), N'app.result.success.operate-time', N'en-US', N'Effective time：'),
(CAST(198 AS bigint), N'app.result.success.principal', N'en-US', N'Principal：'),
(CAST(197 AS bigint), N'app.result.success.operate-id', N'en-US', N'Project ID：'),
(CAST(196 AS bigint), N'app.result.success.operate-title', N'en-US', N'Project Name'),
(CAST(195 AS bigint), N'app.result.success.description', N'en-US', N'The submission results page is used to feed back the results of a series of operational tasks. If it is a simple operation, use the Message global prompt feedback. This text area can show a simple supplementary explanation. If there is a similar requirement for displaying “documents”, the following gray area can present more complicated content.'),
(CAST(194 AS bigint), N'app.result.success.title', N'en-US', N'Submission Success'),
(CAST(193 AS bigint), N'app.result.error.btn-text', N'en-US', N'Return to modify'),
(CAST(192 AS bigint), N'app.result.error.hint-btn2', N'en-US', N'Upgrade immediately'),
(CAST(191 AS bigint), N'app.result.error.hint-text2', N'en-US', N'Your account is not yet eligible to apply'),
(CAST(190 AS bigint), N'app.result.error.hint-btn1', N'en-US', N'Thaw immediately'),
(CAST(189 AS bigint), N'app.result.error.hint-text1', N'en-US', N'Your account has been frozen'),
(CAST(188 AS bigint), N'app.result.error.hint-title', N'en-US', N'The content you submitted has the following error] ='),
(CAST(187 AS bigint), N'app.result.error.description', N'en-US', N'Please check and modify the following information before resubmitting.'),
(CAST(186 AS bigint), N'app.result.error.title', N'en-US', N'Submission Failed'),
(CAST(185 AS bigint), N'app.monitor.fund-surplus', N'en-US', N'Fund Surplus'),
(CAST(203 AS bigint), N'app.result.success.step2-operator', N'en-US', N'Zhou Maomao'),
(CAST(243 AS bigint), N'app.settings.basic.change-avatar', N'en-US', N'Change avatar'),
(CAST(268 AS bigint), N'app.settings.security.question-description', N'en-US', N'The security question is not set, and the security policy can effectively protect the account security'),
(CAST(245 AS bigint), N'app.settings.basic.email-message', N'en-US', N'Please input your email!'),
(CAST(302 AS bigint), N'app.classes.gradingTerm', N'es', N'Término de calificación'),
(CAST(301 AS bigint), N'app.classes.className', N'es', N'Nombre de la clase'),
(CAST(300 AS bigint), N'app.classes.period', N'es', N'Período'),
(CAST(299 AS bigint), N'app.classes.editClass', N'es', N'clase de edición'),
(CAST(298 AS bigint), N'app.classes.addClass', N'es', N'Añadir clase'),
(CAST(297 AS bigint), N'component.tagSelect.all', N'en-US', N'All'),
(CAST(296 AS bigint), N'component.tagSelect.collapse', N'en-US', N'Collapse'),
(CAST(303 AS bigint), N'app.classes.classDesc', N'es', N'Descripción de la clase'),
(CAST(295 AS bigint), N'component.tagSelect.expand', N'en-US', N'Expand'),
(CAST(293 AS bigint), N'app.pwa.serviceworker.updated.hint', N'en-US', N'Please press the "Refresh" button to reload current page'),
(CAST(292 AS bigint), N'app.pwa.serviceworker.updated', N'en-US', N'New content is available'),
(CAST(291 AS bigint), N'app.pwa.offline', N'en-US', N'You are offline now'),
(CAST(290 AS bigint), N'app.settings.close', N'en-US', N'Close'),
(CAST(289 AS bigint), N'app.settings.open', N'en-US', N'Open'),
(CAST(288 AS bigint), N'app.settings.notification.todo-description', N'en-US', N'The to-do list will be notified in the form of a letter from the station'),
(CAST(287 AS bigint), N'app.settings.notification.todo', N'en-US', N'To-do Notification'),
(CAST(294 AS bigint), N'app.pwa.serviceworker.updated.ok', N'en-US', N'Refresh'),
(CAST(304 AS bigint), N'app.classes.list.className', N'es', N'Nombre de la clase'),
(CAST(305 AS bigint), N'app.classes.list.period', N'es', N'Período'),
(CAST(306 AS bigint), N'app.classes.list.gradingTerm', N'es', N'Término de calificación'),
(CAST(323 AS bigint), N'api.student.error.studentphonenumbernotfound', N'es', N'El número de teléfono del estudiante no fue encontrado.'),
(CAST(322 AS bigint), N'api.student.error.studentnumbernotfound', N'es', N'El número de estudiante no fue encontrado.'),
(CAST(321 AS bigint), N'api.student.error.studentnotfound', N'es', N'El estudiante no fue encontrado.'),
(CAST(320 AS bigint), N'api.login.error.logininvalid', N'es', N'Usuario o contraseña invalido. Por favor, compruebe su nombre de usuario y contraseña y vuelva a intentarlo.'),
(CAST(319 AS bigint), N'api.login.error.loginrequirestwofactor', N'es', N'Se requiere autenticación de dos factores.'),
(CAST(318 AS bigint), N'api.login.error.userisnotallowed', N'es', N'Este usuario ahora tiene permitido iniciar sesión.'),
(CAST(317 AS bigint), N'api.login.error.userislockedout', N'es', N'Su cuenta está bloqueada. Póngase en contacto con su administrador.'),
(CAST(316 AS bigint), N'api.login.error.emailnotconfirm', N'es', N'El correo electrónico no está confirmado. Por favor revise el correo electrónico para confirmación e intente iniciar sesión nuevamente.'),
(CAST(315 AS bigint), N'app.classes.list.gradingTerm', N'vi', N'Học kỳ'),
(CAST(314 AS bigint), N'app.classes.list.period', N'vi', N'Tiết'),
(CAST(313 AS bigint), N'app.classes.list.className', N'vi', N'Lớp'),
(CAST(312 AS bigint), N'app.classes.classDesc', N'vi', N'Tổng quan'),
(CAST(311 AS bigint), N'app.classes.gradingTerm', N'vi', N'Học kỳ'),
(CAST(310 AS bigint), N'app.classes.className', N'vi', N'Tên lớp'),
(CAST(309 AS bigint), N'app.classes.period', N'vi', N'Tiết'),
(CAST(308 AS bigint), N'app.classes.editClass', N'vi', N'Sửa lớp'),
(CAST(307 AS bigint), N'app.classes.addClass', N'vi', N'Thêm lớp'),
(CAST(286 AS bigint), N'app.settings.notification.messages-description', N'en-US', N'System messages will be notified in the form of a station letter'),
(CAST(244 AS bigint), N'app.settings.basic.email', N'en-US', N'Email'),
(CAST(285 AS bigint), N'app.settings.notification.messages', N'en-US', N'System Messages'),
(CAST(283 AS bigint), N'app.settings.notification.password', N'en-US', N'Account Password'),
(CAST(261 AS bigint), N'app.settings.security.medium', N'en-US', N'Medium'),
(CAST(260 AS bigint), N'app.settings.security.strong', N'en-US', N'Strong'),
(CAST(259 AS bigint), N'app.settings.basic.update', N'en-US', N'Update Information'),
(CAST(258 AS bigint), N'app.settings.basic.phone-message', N'en-US', N'Please input your phone!'),
(CAST(257 AS bigint), N'app.settings.basic.phone', N'en-US', N'Phone Number'),
(CAST(256 AS bigint), N'app.settings.basic.address-message', N'en-US', N'Please input your address!'),
(CAST(255 AS bigint), N'app.settings.basic.address', N'en-US', N'Street Address'),
(CAST(262 AS bigint), N'app.settings.security.weak', N'en-US', N'Weak'),
(CAST(254 AS bigint), N'app.settings.basic.geographic-message', N'en-US', N'Please input your geographic info!'),
(CAST(252 AS bigint), N'app.settings.basic.country-message', N'en-US', N'Please input your country!'),
(CAST(251 AS bigint), N'app.settings.basic.country', N'en-US', N'Country/Region'),
(CAST(250 AS bigint), N'app.settings.basic.profile-placeholder', N'en-US', N'Brief introduction to yourself'),
(CAST(249 AS bigint), N'app.settings.basic.profile-message', N'en-US', N'Please input your personal profile!'),
(CAST(248 AS bigint), N'app.settings.basic.profile', N'en-US', N'Personal profile'),
(CAST(247 AS bigint), N'app.settings.basic.nickname-message', N'en-US', N'Please input your Nickname!'),
(CAST(246 AS bigint), N'app.settings.basic.nickname', N'en-US', N'Nickname'),
(CAST(253 AS bigint), N'app.settings.basic.geographic', N'en-US', N'Province or city'),
(CAST(263 AS bigint), N'app.settings.security.password', N'en-US', N'Account Password'),
(CAST(264 AS bigint), N'app.settings.security.password-description', N'en-US', N'Current password strength'),
(CAST(265 AS bigint), N'app.settings.security.phone', N'en-US', N'Security Phone'),
(CAST(282 AS bigint), N'app.settings.binding.bind', N'en-US', N'Bind'),
(CAST(281 AS bigint), N'app.settings.binding.dingding-description', N'en-US', N'Currently unbound DingTalk account'),
(CAST(280 AS bigint), N'app.settings.binding.dingding', N'en-US', N'Binding DingTalk'),
(CAST(279 AS bigint), N'app.settings.binding.alipay-description', N'en-US', N'Currently unbound Alipay account'),
(CAST(278 AS bigint), N'app.settings.binding.alipay', N'en-US', N'Binding Alipay'),
(CAST(277 AS bigint), N'app.settings.binding.taobao-description', N'en-US', N'Currently unbound Taobao account'),
(CAST(276 AS bigint), N'app.settings.binding.taobao', N'en-US', N'Binding Taobao'),
(CAST(275 AS bigint), N'app.settings.security.bind', N'en-US', N'Bind'),
(CAST(274 AS bigint), N'app.settings.security.set', N'en-US', N'Set'),
(CAST(273 AS bigint), N'app.settings.security.modify', N'en-US', N'Modify'),
(CAST(272 AS bigint), N'app.settings.security.mfa-description', N'en-US', N'Unbound MFA device, after binding, can be confirmed twice'),
(CAST(271 AS bigint), N'app.settings.security.mfa', N'en-US', N'MFA Device'),
(CAST(270 AS bigint), N'app.settings.security.email-description', N'en-US', N'Bound Email'),
(CAST(269 AS bigint), N'app.settings.security.email', N'en-US', N'Backup Email'),
(CAST(164 AS bigint), N'menu.exception.trigger', N'en-US', N'Trigger'),
(CAST(267 AS bigint), N'app.settings.security.question', N'en-US', N'Security Question'),
(CAST(266 AS bigint), N'app.settings.security.phone-description', N'en-US', N'Bound phone'),
(CAST(284 AS bigint), N'app.settings.notification.password-description', N'en-US', N'Messages from other users will be notified in the form of a station letter'),
(CAST(163 AS bigint), N'menu.exception.server-error', N'en-US', N'500'),
(CAST(144 AS bigint), N'menu.form.stepform.result', N'en-US', N'Step Form(finished)'),
(CAST(161 AS bigint), N'menu.exception.not-permission', N'en-US', N'403'),
(CAST(57 AS bigint), N'form.title.placeholder', N'en-US', N'Give the target a name'),
(CAST(56 AS bigint), N'form.title.label', N'en-US', N'Title'),
(CAST(55 AS bigint), N'form.verification-code.placeholder', N'en-US', N'Verification code'),
(CAST(54 AS bigint), N'form.phone-number.placeholder', N'en-US', N'Phone number'),
(CAST(53 AS bigint), N'form.confirm-password.placeholder', N'en-US', N'Confirm password'),
(CAST(52 AS bigint), N'form.password.placeholder', N'en-US', N'Password'),
(CAST(51 AS bigint), N'form.email.placeholder', N'en-US', N'Email'),
(CAST(58 AS bigint), N'form.date.label', N'en-US', N'Start and end date'),
(CAST(50 AS bigint), N'form.lastName.placeholder', N'en-US', N'Last Name'),
(CAST(48 AS bigint), N'form.save', N'en-US', N'Save'),
(CAST(47 AS bigint), N'form.submit', N'en-US', N'Submit'),
(CAST(46 AS bigint), N'form.optional', N'en-US', N' (optional) '),
(CAST(45 AS bigint), N'form.captcha.second', N'en-US', N'sec'),
(CAST(44 AS bigint), N'form.get-captcha', N'en-US', N'Get Captcha'),
(CAST(43 AS bigint), N'app.exception.description.500', N'en-US', N'Sorry, the server is reporting an error'),
(CAST(42 AS bigint), N'app.exception.description.404', N'en-US', N'Sorry, the page you visited does not exist'),
(CAST(49 AS bigint), N'form.firstName.placeholder', N'en-US', N'First Name'),
(CAST(59 AS bigint), N'form.date.placeholder.start', N'en-US', N'Start date'),
(CAST(60 AS bigint), N'form.date.placeholder.end', N'en-US', N'End date'),
(CAST(61 AS bigint), N'form.goal.label', N'en-US', N'Goal description'),
(CAST(78 AS bigint), N'form.publicUsers.option.A', N'en-US', N'Colleague A'),
(CAST(77 AS bigint), N'form.publicUsers.placeholder', N'en-US', N'Open to'),
(CAST(76 AS bigint), N'form.public.radio.private', N'en-US', N'Private'),
(CAST(75 AS bigint), N'form.public.radio.partially-public', N'en-US', N'Partially public'),
(CAST(162 AS bigint), N'menu.exception.not-find', N'en-US', N'404'),
(CAST(73 AS bigint), N'form.public.label.help', N'en-US', N'Customers and invitees are shared by default'),
(CAST(72 AS bigint), N'form.public.label', N'en-US', N'Target disclosure'),
(CAST(71 AS bigint), N'form.weight.placeholder', N'en-US', N'Please enter weight'),
(CAST(70 AS bigint), N'form.weight.label', N'en-US', N'Weight'),
(CAST(69 AS bigint), N'form.invites.placeholder', N'en-US', N'Please direct @ Name / job number, you can invite up to 5 people'),
(CAST(68 AS bigint), N'form.invites.label', N'en-US', N'Inviting critics'),
(CAST(67 AS bigint), N'form.client.placeholder', N'en-US', N'Please describe your customer service, internal customers directly @ Name / job number'),
(CAST(66 AS bigint), N'form.client.label.tooltip', N'en-US', N'Target service object'),
(CAST(65 AS bigint), N'form.client.label', N'en-US', N'Client'),
(CAST(64 AS bigint), N'form.standard.placeholder', N'en-US', N'Please enter a metric'),
(CAST(63 AS bigint), N'form.standard.label', N'en-US', N'Metrics'),
(CAST(62 AS bigint), N'form.goal.placeholder', N'en-US', N'Please enter your work goals'),
(CAST(41 AS bigint), N'app.exception.description.403', N'en-US', N'Sorry, you don''t have access to this page'),
(CAST(79 AS bigint), N'form.publicUsers.option.B', N'en-US', N'Colleague B'),
(CAST(40 AS bigint), N'app.exception.back', N'en-US', N'Back to home'),
(CAST(38 AS bigint), N'app.analysis.table.users', N'en-US', N'Users'),
(CAST(16 AS bigint), N'app.analysis.week', N'en-US', N'WoW Change'),
(CAST(15 AS bigint), N'app.analysis.day-visits', N'en-US', N'Daily Visits'),
(CAST(14 AS bigint), N'app.analysis.visits-ranking', N'en-US', N'Visits Ranking'),
(CAST(13 AS bigint), N'app.analysis.visits-trend', N'en-US', N'Visits Trend'),
(CAST(12 AS bigint), N'app.analysis.visits', N'en-US', N'Visits'),
(CAST(11 AS bigint), N'app.analysis.day-sales', N'en-US', N'Daily Sales'),
(CAST(10 AS bigint), N'app.analysis.total-sales', N'en-US', N'Total Sales'),
(CAST(17 AS bigint), N'app.analysis.day', N'en-US', N'DoD Change'),
(CAST(9 AS bigint), N'app.analysis.introduce', N'en-US', N'Introduce'),
(CAST(7 AS bigint), N'app.forms.basic.description', N'en-US', N'Form pages are used to collect or verify information to users, and basic forms are common in scenarios where there are fewer data items.'),
(CAST(6 AS bigint), N'app.forms.basic.title', N'en-US', N'Basic form'),
(CAST(5 AS bigint), N'app.home.introduce', N'en-US', N'introduce'),
(CAST(4 AS bigint), N'layout.user.link.terms', N'en-US', N'Terms'),
(CAST(3 AS bigint), N'layout.user.link.privacy', N'en-US', N'Privacy'),
(CAST(2 AS bigint), N'layout.user.link.help', N'en-US', N'Help'),
(CAST(1 AS bigint), N'navBar.lang', N'en-US', N'Languages'),
(CAST(8 AS bigint), N'app.analysis.test', N'en-US', N'Gongzhuan No.{no} shop'),
(CAST(18 AS bigint), N'app.analysis.payments', N'en-US', N'Payments'),
(CAST(19 AS bigint), N'app.analysis.conversion-rate', N'en-US', N'Conversion Rate'),
(CAST(20 AS bigint), N'app.analysis.operational-effect', N'en-US', N'Operational Effect'),
(CAST(37 AS bigint), N'app.analysis.table.search-keyword', N'en-US', N'Keyword'),
(CAST(36 AS bigint), N'app.analysis.table.rank', N'en-US', N'Rank'),
(CAST(35 AS bigint), N'app.analysis.traffic', N'en-US', N'Traffic'),
(CAST(34 AS bigint), N'app.analysis.sales', N'en-US', N'Sales'),
(CAST(33 AS bigint), N'app.analysis.channel.stores', N'en-US', N'Stores'),
(CAST(32 AS bigint), N'app.analysis.channel.online', N'en-US', N'Online'),
(CAST(31 AS bigint), N'app.analysis.channel.all', N'en-US', N'ALL'),
(CAST(30 AS bigint), N'app.analysis.the-proportion-of-sales', N'en-US', N'The Proportion Of Sales'),
(CAST(29 AS bigint), N'app.analysis.online-top-search', N'en-US', N'Online Top Search'),
(CAST(28 AS bigint), N'app.analysis.per-capita-search', N'en-US', N'Per Capita Search'),
(CAST(27 AS bigint), N'app.analysis.search-users', N'en-US', N'Search Users'),
(CAST(26 AS bigint), N'app.analysis.all-day', N'en-US', N'All day'),
(CAST(25 AS bigint), N'app.analysis.all-week', N'en-US', N'All Week'),
(CAST(24 AS bigint), N'app.analysis.all-month', N'en-US', N'All Month'),
(CAST(23 AS bigint), N'app.analysis.all-year', N'en-US', N'All Year'),
(CAST(22 AS bigint), N'app.analysis.sales-ranking', N'en-US', N'Sales Ranking'),
(CAST(21 AS bigint), N'app.analysis.sales-trend', N'en-US', N'Stores Sales Trend'),
(CAST(39 AS bigint), N'app.analysis.table.weekly-range', N'en-US', N'Weekly Range'),
(CAST(80 AS bigint), N'form.publicUsers.option.C', N'en-US', N'Colleague C'),
(CAST(74 AS bigint), N'form.public.radio.public', N'en-US', N'Public'),
(CAST(82 AS bigint), N'component.globalHeader.search.example1', N'en-US', N'Search example 1'),
(CAST(139 AS bigint), N'menu.form', N'en-US', N'Form'),
(CAST(138 AS bigint), N'menu.dashboard.workplace', N'en-US', N'Workplace'),
(CAST(137 AS bigint), N'menu.dashboard.monitor', N'en-US', N'Monitor'),
(CAST(136 AS bigint), N'menu.dashboard.analysis', N'en-US', N'Analysis'),
(CAST(135 AS bigint), N'menu.dashboard', N'en-US', N'Dashboard'),
(CAST(134 AS bigint), N'menu.register.result', N'en-US', N'Register Result'),
(CAST(133 AS bigint), N'menu.register', N'en-US', N'Register'),
(CAST(140 AS bigint), N'menu.form.basicform', N'en-US', N'Basic Form'),
(CAST(132 AS bigint), N'menu.login', N'en-US', N'Login'),
(CAST(130 AS bigint), N'validation.standard.required', N'en-US', N'Please enter a metric'),
(CAST(129 AS bigint), N'validation.goal.required', N'en-US', N'Please enter a description of the goal'),
(CAST(81 AS bigint), N'component.globalHeader.search', N'en-US', N'Search'),
(CAST(127 AS bigint), N'validation.title.required', N'en-US', N'Please enter a title'),
(CAST(126 AS bigint), N'validation.verification-code.required', N'en-US', N'Please enter the verification code!'),
(CAST(125 AS bigint), N'validation.phone-number.wrong-format', N'en-US', N'Malformed phone number!'),
(CAST(124 AS bigint), N'validation.phone-number.required', N'en-US', N'Please enter your phone number!'),
(CAST(131 AS bigint), N'menu.home', N'en-US', N'Home'),
(CAST(123 AS bigint), N'validation.confirm-password.required', N'en-US', N'Please confirm your password!'),
(CAST(141 AS bigint), N'menu.form.stepform', N'en-US', N'Step Form'),
(CAST(143 AS bigint), N'menu.form.stepform.confirm', N'en-US', N'Step Form(confirm transfer information)'),
(CAST(160 AS bigint), N'menu.exception', N'en-US', N'Exception'),
(CAST(159 AS bigint), N'menu.result.fail', N'en-US', N'Fail'),
(CAST(158 AS bigint), N'menu.result.success', N'en-US', N'Success'),
(CAST(157 AS bigint), N'menu.result', N'en-US', N'Result'),
(CAST(156 AS bigint), N'menu.profile.advanced', N'en-US', N'Advanced Profile'),
(CAST(155 AS bigint), N'menu.profile.basic', N'en-US', N'Basic Profile'),
(CAST(154 AS bigint), N'menu.profile', N'en-US', N'Profile'),
(CAST(142 AS bigint), N'menu.form.stepform.info', N'en-US', N'Step Form(write transfer information)'),
(CAST(153 AS bigint), N'menu.list.searchlist.applications', N'en-US', N'Search List(applications)'),
(CAST(151 AS bigint), N'menu.list.searchlist.articles', N'en-US', N'Search List(articles)'),
(CAST(150 AS bigint), N'menu.list.searchlist', N'en-US', N'Search List'),
(CAST(149 AS bigint), N'menu.list.cardlist', N'en-US', N'Card List'),
(CAST(148 AS bigint), N'menu.list.basiclist', N'en-US', N'Basic List'),
(CAST(147 AS bigint), N'menu.list.searchtable', N'en-US', N'Search Table'),
(CAST(146 AS bigint), N'menu.list', N'en-US', N'List'),
(CAST(145 AS bigint), N'menu.form.advancedform', N'en-US', N'Advanced Form'),
(CAST(152 AS bigint), N'menu.list.searchlist.projects', N'en-US', N'Search List(projects)'),
(CAST(122 AS bigint), N'validation.password.strength.short', N'en-US', N'Strength] = too short'),
(CAST(128 AS bigint), N'validation.date.required', N'en-US', N'Please select the start and end date'),
(CAST(120 AS bigint), N'validation.password.strength.strong', N'en-US', N'Strength] = strong'),
(CAST(98 AS bigint), N'app.login.message-invalid-credentials', N'en-US', N'Invalid username or password（admin/ant.design）'),
(CAST(97 AS bigint), N'app.login.password', N'en-US', N'password'),
(CAST(96 AS bigint), N'app.login.userName', N'en-US', N'userName'),
(CAST(121 AS bigint), N'validation.password.strength.medium', N'en-US', N'Strength] = medium'),
(CAST(94 AS bigint), N'component.noticeIcon.empty', N'en-US', N'No notifications'),
(CAST(93 AS bigint), N'component.noticeIcon.cleared', N'en-US', N'Cleared'),
(CAST(92 AS bigint), N'component.noticeIcon.clear', N'en-US', N'Clear'),
(CAST(99 AS bigint), N'app.login.message-invalid-verification-code', N'en-US', N'Invalid verification code'),
(CAST(91 AS bigint), N'component.globalHeader.event.empty', N'en-US', N'You have viewed all events.'),
(CAST(89 AS bigint), N'component.globalHeader.message.empty', N'en-US', N'You have viewed all messsages.'),
(CAST(88 AS bigint), N'component.globalHeader.message', N'en-US', N'Message'),
(CAST(87 AS bigint), N'component.globalHeader.notification.empty', N'en-US', N'You have viewed all notifications.'),
(CAST(86 AS bigint), N'component.globalHeader.notification', N'en-US', N'Notification'),
(CAST(85 AS bigint), N'component.globalHeader.help', N'en-US', N'Help'),
(CAST(84 AS bigint), N'component.globalHeader.search.example3', N'en-US', N'Search example 3'),
(CAST(83 AS bigint), N'component.globalHeader.search.example2', N'en-US', N'Search example 2'),
(CAST(90 AS bigint), N'component.globalHeader.event', N'en-US', N'Event'),
(CAST(100 AS bigint), N'app.login.tab-login-credentials', N'en-US', N'Credentials'),
(CAST(95 AS bigint), N'component.noticeIcon.view-more', N'en-US', N'View more'),
(CAST(102 AS bigint), N'app.login.remember-me', N'en-US', N'Remember me'),
(CAST(119 AS bigint), N'validation.password.strength.msg', N'en-US', N'Please enter at least 6 characters and don''t use passwords that are easy to guess.'),
(CAST(118 AS bigint), N'validation.password.twice', N'en-US', N'The passwords entered twice do not match!'),
(CAST(101 AS bigint), N'app.login.tab-login-mobile', N'en-US', N'Mobile number'),
(CAST(116 AS bigint), N'validation.userName.required', N'en-US', N'Please enter your userName!'),
(CAST(115 AS bigint), N'validation.email.wrong-format', N'en-US', N'The email address is in the wrong format!'),
(CAST(114 AS bigint), N'validation.email.required', N'en-US', N'Please enter your email!'),
(CAST(113 AS bigint), N'app.register-result.view-mailbox', N'en-US', N'View mailbox'),
(CAST(112 AS bigint), N'app.register-result.back-home', N'en-US', N'Back to home'),
(CAST(111 AS bigint), N'app.register-result.activation-email', N'en-US', N'The activation email has been sent to your email address and is valid for 24 hours. Please log in to the email in time and click on the link in the email to activate the account.'),
(CAST(117 AS bigint), N'validation.password.required', N'en-US', N'Please enter your password!'),
(CAST(109 AS bigint), N'app.register.sign-in', N'en-US', N'Already have an account?'),
(CAST(108 AS bigint), N'app.register.get-verification-code', N'en-US', N'Get code'),
(CAST(107 AS bigint), N'app.register.register', N'en-US', N'Register'),
(CAST(106 AS bigint), N'app.login.login', N'en-US', N'Login'),
(CAST(105 AS bigint), N'app.login.signup', N'en-US', N'Sign up'),
(CAST(104 AS bigint), N'app.login.sign-in-with', N'en-US', N'Sign in with'),
(CAST(110 AS bigint), N'app.register-result.msg', N'en-US', N'Account：registered at {email}'),
(CAST(103 AS bigint), N'app.login.forgot-password', N'en-US', N'Forgot your password?');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'RecordId', N'Key', N'Locale', N'Value') AND [object_id] = OBJECT_ID(N'[Localizations]'))
    SET IDENTITY_INSERT [Localizations] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ParentId', N'StudentId') AND [object_id] = OBJECT_ID(N'[ParentStudent]'))
    SET IDENTITY_INSERT [ParentStudent] ON;
INSERT INTO [ParentStudent] ([ParentId], [StudentId])
VALUES (CAST(4 AS bigint), CAST(3 AS bigint)),
(CAST(4 AS bigint), CAST(8 AS bigint));
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ParentId', N'StudentId') AND [object_id] = OBJECT_ID(N'[ParentStudent]'))
    SET IDENTITY_INSERT [ParentStudent] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ParentId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'IsDeleted', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Parents]'))
    SET IDENTITY_INSERT [Parents] ON;
INSERT INTO [Parents] ([ParentId], [CreatedBy], [DateCreated], [DateUpdated], [IsDeleted], [UpdatedBy])
VALUES (CAST(4 AS bigint), NULL, NULL, NULL, 0, NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ParentId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'IsDeleted', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Parents]'))
    SET IDENTITY_INSERT [Parents] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'StudentId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'ExternalId', N'IsDeleted', N'StudentNumber', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Students]'))
    SET IDENTITY_INSERT [Students] ON;
INSERT INTO [Students] ([StudentId], [CreatedBy], [DateCreated], [DateUpdated], [ExternalId], [IsDeleted], [StudentNumber], [UpdatedBy])
VALUES (CAST(16 AS bigint), NULL, NULL, NULL, N'0010', 0, N'0010', NULL),
(CAST(3 AS bigint), NULL, NULL, NULL, N'0001', 0, N'0001', NULL),
(CAST(8 AS bigint), NULL, NULL, NULL, N'0002', 0, N'0002', NULL),
(CAST(9 AS bigint), NULL, NULL, NULL, N'0003', 0, N'0003', NULL),
(CAST(10 AS bigint), NULL, NULL, NULL, N'0004', 0, N'0004', NULL),
(CAST(11 AS bigint), NULL, NULL, NULL, N'0005', 0, N'0005', NULL),
(CAST(12 AS bigint), NULL, NULL, NULL, N'0006', 0, N'0006', NULL),
(CAST(13 AS bigint), NULL, NULL, NULL, N'0007', 0, N'0007', NULL),
(CAST(14 AS bigint), NULL, NULL, NULL, N'0008', 0, N'0008', NULL),
(CAST(15 AS bigint), NULL, NULL, NULL, N'0009', 0, N'0009', NULL),
(CAST(17 AS bigint), NULL, NULL, NULL, N'0011', 0, N'0011', NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'StudentId', N'CreatedBy', N'DateCreated', N'DateUpdated', N'ExternalId', N'IsDeleted', N'StudentNumber', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Students]'))
    SET IDENTITY_INSERT [Students] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'SchoolId', N'CreatedBy', N'CurrentGradingTerm', N'DateCreated', N'DateUpdated', N'DistrictId', N'IconUrl', N'IsDeleted', N'LogoUrl', N'SchoolName', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Schools]'))
    SET IDENTITY_INSERT [Schools] ON;
INSERT INTO [Schools] ([SchoolId], [CreatedBy], [CurrentGradingTerm], [DateCreated], [DateUpdated], [DistrictId], [IconUrl], [IsDeleted], [LogoUrl], [SchoolName], [UpdatedBy])
VALUES (CAST(1 AS bigint), NULL, CAST(2 AS bigint), NULL, NULL, CAST(1 AS bigint), N'', 0, N'', N'Test School', NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'SchoolId', N'CreatedBy', N'CurrentGradingTerm', N'DateCreated', N'DateUpdated', N'DistrictId', N'IconUrl', N'IsDeleted', N'LogoUrl', N'SchoolName', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Schools]'))
    SET IDENTITY_INSERT [Schools] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'SchoolId', N'CreatedBy', N'CurrentGradingTerm', N'DateCreated', N'DateUpdated', N'DistrictId', N'IconUrl', N'IsDeleted', N'LogoUrl', N'SchoolName', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Schools]'))
    SET IDENTITY_INSERT [Schools] ON;
INSERT INTO [Schools] ([SchoolId], [CreatedBy], [CurrentGradingTerm], [DateCreated], [DateUpdated], [DistrictId], [IconUrl], [IsDeleted], [LogoUrl], [SchoolName], [UpdatedBy])
VALUES (CAST(2 AS bigint), NULL, CAST(2 AS bigint), NULL, NULL, CAST(2 AS bigint), N'', 0, N'', N'Test School 2', NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'SchoolId', N'CreatedBy', N'CurrentGradingTerm', N'DateCreated', N'DateUpdated', N'DistrictId', N'IconUrl', N'IsDeleted', N'LogoUrl', N'SchoolName', N'UpdatedBy') AND [object_id] = OBJECT_ID(N'[Schools]'))
    SET IDENTITY_INSERT [Schools] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ClassId', N'ClassName', N'CreatedBy', N'DateCreated', N'DateUpdated', N'Description', N'GradeRangesJson', N'GradingTerm', N'IsDeleted', N'IsTemplate', N'Period', N'SchoolId', N'UpdatedBy', N'UseDefaultGradesRange') AND [object_id] = OBJECT_ID(N'[Classes]'))
    SET IDENTITY_INSERT [Classes] ON;
INSERT INTO [Classes] ([ClassId], [ClassName], [CreatedBy], [DateCreated], [DateUpdated], [Description], [GradeRangesJson], [GradingTerm], [IsDeleted], [IsTemplate], [Period], [SchoolId], [UpdatedBy], [UseDefaultGradesRange])
VALUES (CAST(1 AS bigint), N'Class 1', NULL, NULL, NULL, N'No Desc', NULL, CAST(1 AS bigint), 0, 0, N'1', CAST(1 AS bigint), NULL, 1),
(CAST(2 AS bigint), N'Class 2', NULL, NULL, NULL, N'No Desc', NULL, CAST(2 AS bigint), 0, 0, N'2', CAST(1 AS bigint), NULL, 1),
(CAST(3 AS bigint), N'Class 3', NULL, NULL, NULL, N'Desc', NULL, CAST(3 AS bigint), 0, 0, N'3', CAST(1 AS bigint), NULL, 1);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ClassId', N'ClassName', N'CreatedBy', N'DateCreated', N'DateUpdated', N'Description', N'GradeRangesJson', N'GradingTerm', N'IsDeleted', N'IsTemplate', N'Period', N'SchoolId', N'UpdatedBy', N'UseDefaultGradesRange') AND [object_id] = OBJECT_ID(N'[Classes]'))
    SET IDENTITY_INSERT [Classes] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'SchoolId', N'FacultyId') AND [object_id] = OBJECT_ID(N'[SchoolFaculty]'))
    SET IDENTITY_INSERT [SchoolFaculty] ON;
INSERT INTO [SchoolFaculty] ([SchoolId], [FacultyId])
VALUES (CAST(1 AS bigint), CAST(5 AS bigint)),
(CAST(2 AS bigint), CAST(6 AS bigint));
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'SchoolId', N'FacultyId') AND [object_id] = OBJECT_ID(N'[SchoolFaculty]'))
    SET IDENTITY_INSERT [SchoolFaculty] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'SchoolId', N'StudentId', N'GradingPeriod', N'Grade') AND [object_id] = OBJECT_ID(N'[SchoolStudent]'))
    SET IDENTITY_INSERT [SchoolStudent] ON;
INSERT INTO [SchoolStudent] ([SchoolId], [StudentId], [GradingPeriod], [Grade])
VALUES (CAST(1 AS bigint), CAST(3 AS bigint), CAST(1 AS bigint), 0.5),
(CAST(1 AS bigint), CAST(8 AS bigint), CAST(1 AS bigint), 0.4),
(CAST(1 AS bigint), CAST(9 AS bigint), CAST(1 AS bigint), 0.3),
(CAST(1 AS bigint), CAST(10 AS bigint), CAST(1 AS bigint), 0.2),
(CAST(1 AS bigint), CAST(11 AS bigint), CAST(1 AS bigint), 0.1),
(CAST(1 AS bigint), CAST(12 AS bigint), CAST(1 AS bigint), 0.1),
(CAST(1 AS bigint), CAST(13 AS bigint), CAST(1 AS bigint), 0.1),
(CAST(1 AS bigint), CAST(14 AS bigint), CAST(1 AS bigint), 0.1),
(CAST(1 AS bigint), CAST(15 AS bigint), CAST(1 AS bigint), NULL),
(CAST(1 AS bigint), CAST(16 AS bigint), CAST(1 AS bigint), 0.1);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'SchoolId', N'StudentId', N'GradingPeriod', N'Grade') AND [object_id] = OBJECT_ID(N'[SchoolStudent]'))
    SET IDENTITY_INSERT [SchoolStudent] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ActivityId', N'CategoryId', N'ClassId', N'Color', N'CreatedBy', N'DateAssigned', N'DateCreated', N'DateCutoff', N'DateDue', N'DateUpdated', N'Description', N'IsDeleted', N'IsGraded', N'MaxScore', N'ParentActivityId', N'PublishEndDate', N'PublishStartDate', N'SortIndex', N'Title', N'Type', N'UpdatedBy', N'Weight') AND [object_id] = OBJECT_ID(N'[Activities]'))
    SET IDENTITY_INSERT [Activities] ON;
INSERT INTO [Activities] ([ActivityId], [CategoryId], [ClassId], [Color], [CreatedBy], [DateAssigned], [DateCreated], [DateCutoff], [DateDue], [DateUpdated], [Description], [IsDeleted], [IsGraded], [MaxScore], [ParentActivityId], [PublishEndDate], [PublishStartDate], [SortIndex], [Title], [Type], [UpdatedBy], [Weight])
VALUES (CAST(1 AS bigint), NULL, CAST(1 AS bigint), N'#FFFFFF', CAST(4 AS bigint), '2019-03-20T00:00:00.0000000', '2019-03-19T00:00:00.0000000', NULL, '2019-03-31T00:00:00.0000000', '2019-04-24T06:31:51.8250000Z', N'Description activity', 0, 0, 100.0, NULL, NULL, NULL, 0, N'Activity 1', 0, CAST(4 AS bigint), 1.0),
(CAST(2 AS bigint), NULL, CAST(1 AS bigint), N'#FFFFFF', CAST(4 AS bigint), '2019-03-21T00:00:00.0000000', '2019-03-20T00:00:00.0000000', NULL, '2019-03-26T00:00:00.0000000', '2019-04-24T06:31:51.8250000Z', N'Description activity', 0, 0, 100.0, NULL, NULL, NULL, 0, N'Activity 2', 0, CAST(4 AS bigint), 2.0),
(CAST(3 AS bigint), NULL, CAST(2 AS bigint), N'#FFFFFF', CAST(4 AS bigint), '2019-03-20T00:00:00.0000000', '2019-03-19T00:00:00.0000000', NULL, '2019-03-29T00:00:00.0000000', '2019-04-24T06:31:51.8250000Z', N'Description activity', 0, 1, 10.0, NULL, NULL, NULL, 0, N'Activity 2', 0, CAST(4 AS bigint), 1.0);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ActivityId', N'CategoryId', N'ClassId', N'Color', N'CreatedBy', N'DateAssigned', N'DateCreated', N'DateCutoff', N'DateDue', N'DateUpdated', N'Description', N'IsDeleted', N'IsGraded', N'MaxScore', N'ParentActivityId', N'PublishEndDate', N'PublishStartDate', N'SortIndex', N'Title', N'Type', N'UpdatedBy', N'Weight') AND [object_id] = OBJECT_ID(N'[Activities]'))
    SET IDENTITY_INSERT [Activities] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ClassId', N'FacultyId', N'SortIndex') AND [object_id] = OBJECT_ID(N'[ClassFaculty]'))
    SET IDENTITY_INSERT [ClassFaculty] ON;
INSERT INTO [ClassFaculty] ([ClassId], [FacultyId], [SortIndex])
VALUES (CAST(1 AS bigint), CAST(5 AS bigint), 10000),
(CAST(3 AS bigint), CAST(5 AS bigint), 10000),
(CAST(2 AS bigint), CAST(5 AS bigint), 10000);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ClassId', N'FacultyId', N'SortIndex') AND [object_id] = OBJECT_ID(N'[ClassFaculty]'))
    SET IDENTITY_INSERT [ClassFaculty] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ClassId', N'StudentId', N'CumulativeGrade', N'LastAccessed', N'SortIndex') AND [object_id] = OBJECT_ID(N'[ClassStudent]'))
    SET IDENTITY_INSERT [ClassStudent] ON;
INSERT INTO [ClassStudent] ([ClassId], [StudentId], [CumulativeGrade], [LastAccessed], [SortIndex])
VALUES (CAST(3 AS bigint), CAST(8 AS bigint), NULL, NULL, 10000),
(CAST(3 AS bigint), CAST(3 AS bigint), NULL, NULL, 10000),
(CAST(2 AS bigint), CAST(11 AS bigint), NULL, NULL, 10000),
(CAST(2 AS bigint), CAST(9 AS bigint), NULL, NULL, 10000),
(CAST(2 AS bigint), CAST(3 AS bigint), NULL, NULL, 10000),
(CAST(1 AS bigint), CAST(16 AS bigint), NULL, NULL, 10000),
(CAST(1 AS bigint), CAST(15 AS bigint), NULL, NULL, 10000),
(CAST(1 AS bigint), CAST(14 AS bigint), NULL, NULL, 10000),
(CAST(1 AS bigint), CAST(13 AS bigint), NULL, NULL, 10000),
(CAST(1 AS bigint), CAST(12 AS bigint), NULL, NULL, 10000),
(CAST(1 AS bigint), CAST(11 AS bigint), NULL, NULL, 10000),
(CAST(1 AS bigint), CAST(10 AS bigint), NULL, NULL, 10000),
(CAST(1 AS bigint), CAST(9 AS bigint), NULL, NULL, 10000),
(CAST(1 AS bigint), CAST(8 AS bigint), NULL, NULL, 10000),
(CAST(1 AS bigint), CAST(3 AS bigint), NULL, NULL, 10000),
(CAST(3 AS bigint), CAST(9 AS bigint), NULL, NULL, 10000),
(CAST(3 AS bigint), CAST(10 AS bigint), NULL, NULL, 10000);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ClassId', N'StudentId', N'CumulativeGrade', N'LastAccessed', N'SortIndex') AND [object_id] = OBJECT_ID(N'[ClassStudent]'))
    SET IDENTITY_INSERT [ClassStudent] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ActivityId', N'StudentId', N'Character', N'Collaboration', N'Communication', N'Creativity', N'CriticalThinking', N'GradeDate', N'IsSubmitted', N'SavedDate', N'Score', N'ThreadId') AND [object_id] = OBJECT_ID(N'[ActivityScores]'))
    SET IDENTITY_INSERT [ActivityScores] ON;
INSERT INTO [ActivityScores] ([ActivityId], [StudentId], [Character], [Collaboration], [Communication], [Creativity], [CriticalThinking], [GradeDate], [IsSubmitted], [SavedDate], [Score], [ThreadId])
VALUES (CAST(1 AS bigint), CAST(3 AS bigint), NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-03-01T00:00:00.0000000', 4.0, NULL),
(CAST(1 AS bigint), CAST(8 AS bigint), NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-03-10T00:00:00.0000000', 3.0, NULL),
(CAST(1 AS bigint), CAST(9 AS bigint), NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0.0, NULL),
(CAST(1 AS bigint), CAST(10 AS bigint), NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(CAST(1 AS bigint), CAST(11 AS bigint), NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'ActivityId', N'StudentId', N'Character', N'Collaboration', N'Communication', N'Creativity', N'CriticalThinking', N'GradeDate', N'IsSubmitted', N'SavedDate', N'Score', N'ThreadId') AND [object_id] = OBJECT_ID(N'[ActivityScores]'))
    SET IDENTITY_INSERT [ActivityScores] OFF;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20191002054216_Seed_Testing_Data', N'2.2.6-servicing-10079');

GO

