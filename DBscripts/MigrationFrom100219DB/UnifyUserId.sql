--Set IsSystemRole
ALTER TABLE [dbo].[AspNetRoles] ADD [IsSystemRole] bit NOT NULL DEFAULT 0
GO
UPDATE [dbo].[AspNetRoles] SET [IsSystemRole] = 1 WHERE Name IN ('Admin', 'Faculty', 'Parent', 'Student', 'SuperAdmin', 'SchoolAdmin')

--ActivityCategories
UPDATE AC SET AC.[FacultyId] = F.[UserId]
   FROM [dbo].[ActivityCategories] AC
		INNER JOIN [Faculty] F ON AC.[FacultyId] = F.[FacultyId]

--ActivityScores
ALTER TABLE [dbo].[ActivityScores] DROP CONSTRAINT 
	[PK_ActivityScores],
	[FK_ActivityScores_Students_StudentId]	
UPDATE ACT SET ACT.[StudentId] = S.[UserId]
   FROM [dbo].[ActivityScores] ACT
		INNER JOIN [Students] S ON ACT.[StudentId] = S.[StudentId]

--ClassFaculty
ALTER TABLE [dbo].[ClassFaculty] DROP CONSTRAINT 
	[PK_ClassFaculty],
	[FK_ClassFaculty_Faculty_FacultyId]
UPDATE CF SET CF.[FacultyId] = F.[UserId]
   FROM [dbo].[ClassFaculty] CF 
		INNER JOIN [Faculty] F ON CF.[FacultyId] = F.[FacultyId]

--ClassStudent
ALTER TABLE [dbo].[ClassStudent] DROP CONSTRAINT 
	[PK_ClassStudent],
	[FK_ClassStudent_Students_StudentId]
UPDATE CS SET CS.[StudentId] = S.[UserId]
   FROM [dbo].[ClassStudent] CS
		INNER JOIN [Students] S ON CS.[StudentId] = S.[StudentId]

--ParentStudent
ALTER TABLE [dbo].[ParentStudent] DROP CONSTRAINT 
	[PK_ParentStudent],
	[FK_ParentStudent_Parents_ParentId],
	[FK_ParentStudent_Students_StudentId]
UPDATE PS SET PS.[StudentId] = S.[UserId]
   FROM [dbo].[ParentStudent] PS
		INNER JOIN [Students] S ON PS.[StudentId] = S.[StudentId]
UPDATE PS SET PS.[ParentId] = P.[UserId]
   FROM [dbo].[ParentStudent] PS
		INNER JOIN [Parents] P ON PS.[ParentId] = P.[ParentId]

--Polls
UPDATE P SET P.[StudentId] = S.[UserId]
   FROM [dbo].[Polls] P
		INNER JOIN [Students] S ON P.StudentId = S.StudentId

--SchoolFaculty
ALTER TABLE [dbo].[SchoolFaculty] DROP CONSTRAINT 
	[PK_SchoolFaculty],
	[FK_SchoolFaculty_Faculty_FacultyId]		
UPDATE SF SET SF.[FacultyId] = F.[UserId]
   FROM [dbo].[SchoolFaculty] SF 
		INNER JOIN [Faculty] F ON SF.FacultyId = F.FacultyId

--SchoolStudent
ALTER TABLE [dbo].[SchoolStudent] DROP CONSTRAINT 
	[PK_SchoolStudent],
	[FK_SchoolStudent_Students_StudentId]
UPDATE SS SET SS.[StudentId] = S.[UserId]
   FROM [dbo].[SchoolStudent] SS 
		INNER JOIN [Students] S ON SS.StudentId = S.StudentId

--Main Tables
--Parent
ALTER TABLE [dbo].[Parents] DROP CONSTRAINT [PK_Parents]
ALTER TABLE [dbo].[Parents] ADD [NewParentId] bigint NOT NULL DEFAULT -1
GO
UPDATE [dbo].[Parents] SET [NewParentId] = [UserId]
ALTER TABLE [dbo].[Parents] DROP COLUMN [ParentId]
EXEC sp_RENAME '[Parents].[NewParentId]' , 'ParentId', 'COLUMN'
ALTER TABLE [dbo].[Parents] ADD 
    CONSTRAINT [PK_Parents] PRIMARY KEY ([ParentId]),
    CONSTRAINT [FK_Parents_AspNetUsers_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[Parents] DROP COLUMN [UserId]

--Faculty
ALTER TABLE [dbo].[Faculty] DROP CONSTRAINT [PK_Faculty]
ALTER TABLE [dbo].[Faculty] DROP CONSTRAINT [FK_Faculty_AspNetUsers_UserId]
ALTER TABLE [dbo].[Faculty] ADD [NewFacultyId] bigint NOT NULL DEFAULT -1
DROP INDEX [IX_Faculty_UserId] ON [dbo].[Faculty]
GO
UPDATE [dbo].[Faculty] SET [NewFacultyId] = [UserId]
ALTER TABLE [dbo].[Faculty] DROP COLUMN [FacultyId]
EXEC sp_RENAME '[Faculty].[NewFacultyId]' , 'FacultyId', 'COLUMN'
ALTER TABLE [dbo].[Faculty] ADD 
    CONSTRAINT [PK_Faculty] PRIMARY KEY ([FacultyId]),
    CONSTRAINT [FK_Faculty_AspNetUsers_FacultyId] FOREIGN KEY ([FacultyId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[Faculty] DROP COLUMN [UserId]

--Student
ALTER TABLE [dbo].[Students] DROP CONSTRAINT [PK_Students]
ALTER TABLE [dbo].[Students] DROP CONSTRAINT [FK_Students_AspNetUsers_UserId]
ALTER TABLE [dbo].[Students] ADD [NewStudentId] bigint NOT NULL DEFAULT -1
DROP INDEX [IX_Students_UserId] ON [dbo].[Students] 
GO
UPDATE [dbo].[Students] SET [NewStudentId] = [UserId]
ALTER TABLE [dbo].[Students] DROP COLUMN [StudentId]
EXEC sp_RENAME '[Students].[NewStudentId]' , 'StudentId', 'COLUMN'
ALTER TABLE [dbo].[Students] ADD 
    CONSTRAINT [PK_Students] PRIMARY KEY ([StudentId]),
    CONSTRAINT [FK_Students_AspNetUsers_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[Students] DROP COLUMN [UserId]

--Add FK Back
ALTER TABLE [dbo].[SchoolStudent] ADD 
	CONSTRAINT [PK_SchoolStudent] PRIMARY KEY ([SchoolId], [StudentId], [GradingPeriod]),
	CONSTRAINT [FK_SchoolStudent_AspNetUsers_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
ALTER TABLE [dbo].[SchoolFaculty] ADD 
	CONSTRAINT [PK_SchoolFaculty] PRIMARY KEY ([SchoolId], [FacultyId]),
	CONSTRAINT [FK_SchoolFaculty_AspNetUsers_FacultyId] FOREIGN KEY ([FacultyId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
ALTER TABLE [dbo].[ParentStudent] ADD 
	CONSTRAINT [PK_ParentStudent] PRIMARY KEY ([ParentId], [StudentId]),
    CONSTRAINT [FK_ParentStudent_AspNetUsers_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ParentStudent_AspNetUsers_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
ALTER TABLE [dbo].[ClassStudent] ADD 
	CONSTRAINT [PK_ClassStudent] PRIMARY KEY ([ClassId], [StudentId]),
	CONSTRAINT [FK_ClassStudent_AspNetUsers_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
ALTER TABLE [dbo].[ClassFaculty] ADD 
	CONSTRAINT [PK_ClassFaculty] PRIMARY KEY ([ClassId], [FacultyId]),
    CONSTRAINT [FK_ClassFaculty_AspNetUsers_FacultyId] FOREIGN KEY ([FacultyId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
ALTER TABLE [dbo].[ActivityScores] ADD 
	CONSTRAINT [PK_ActivityScores] PRIMARY KEY ([ActivityId], [StudentId]),
    CONSTRAINT [FK_ActivityScores_AspNetUsers_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
ALTER TABLE [dbo].[Classes] ADD 
	CONSTRAINT [FK_Classes_Schools_SchoolId] FOREIGN KEY ([SchoolId]) REFERENCES [Schools] ([SchoolId]) ON DELETE NO ACTION
