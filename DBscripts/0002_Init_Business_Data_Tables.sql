﻿CREATE TABLE [ActivityCategories] (
    [ActivityCategoryId] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [Name] nvarchar(200) NULL,
    [Color] nvarchar(10) NULL,
    [IsGraded] bit NOT NULL,
    [MaxScore] decimal(9, 2) NOT NULL,
    [Weight] decimal(9, 2) NOT NULL,
    [FacultyId] bigint NOT NULL,
    CONSTRAINT [PK_ActivityCategories] PRIMARY KEY ([ActivityCategoryId])
);

GO

CREATE TABLE [ChangeScripts] (
    [ChangeScriptID] bigint NOT NULL IDENTITY,
    [DateExecuted] datetime2 NOT NULL,
    [ScriptName] nvarchar(50) NULL,
    CONSTRAINT [PK_ChangeScripts] PRIMARY KEY ([ChangeScriptID])
);

GO

CREATE TABLE [Comments] (
    [CommentId] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [ThreadId] nvarchar(256) NULL,
    [ReplyTo] bigint NULL,
    [Content] nvarchar(max) NULL,
    CONSTRAINT [PK_Comments] PRIMARY KEY ([CommentId])
);

GO

CREATE TABLE [CommentSeen] (
    [ThreadId] nvarchar(256) NOT NULL,
    [UserId] bigint NOT NULL,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [LastSeen] datetime2 NOT NULL,
    CONSTRAINT [PK_CommentSeen] PRIMARY KEY ([ThreadId], [UserId])
);

GO

CREATE TABLE [Discussions] (
    [DiscussionId] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [Title] nvarchar(500) NULL,
    [Content] nvarchar(max) NULL,
    [Link] nvarchar(2000) NULL,
    [ContentType] nvarchar(100) NULL,
    [MediaType] int NOT NULL,
    [StartTime] datetime2 NULL,
    [EndTime] datetime2 NULL,
    [ClassId] bigint NOT NULL,
    [ActivityId] bigint NOT NULL,
    [ThreadId] nvarchar(256) NULL,
    [IsGraded] bit NOT NULL,
    [DiscussionActivityId] bigint NULL,
    CONSTRAINT [PK_Discussions] PRIMARY KEY ([DiscussionId])
);

GO

CREATE TABLE [Districts] (
    [DistrictId] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [DistrictName] nvarchar(50) NULL,
    [Domain] nvarchar(50) NULL,
    [LogoUrl] nvarchar(2000) NULL,
    CONSTRAINT [PK_Districts] PRIMARY KEY ([DistrictId])
);

GO

CREATE TABLE [EmailAccounts] (
    [EmailAccountId] int NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [Email] nvarchar(256) NULL,
    [EnableSsl] bit NOT NULL,
    [DisplayName] nvarchar(50) NULL,
    [Password] nvarchar(50) NULL,
    [Username] nvarchar(50) NULL,
    [Port] int NOT NULL,
    [Host] nvarchar(256) NULL,
    [UseDefaultCredentials] bit NOT NULL,
    CONSTRAINT [PK_EmailAccounts] PRIMARY KEY ([EmailAccountId])
);

GO

CREATE TABLE [EmailTemplates] (
    [EmailTemplateId] int NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [Name] nvarchar(50) NULL,
    [BccEmailAddresses] nvarchar(256) NULL,
    [Subject] nvarchar(500) NULL,
    [Body] nvarchar(max) NULL,
    CONSTRAINT [PK_EmailTemplates] PRIMARY KEY ([EmailTemplateId])
);

GO

CREATE TABLE [Faculty] (
    [FacultyId] bigint NOT NULL,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [ExternalId] nvarchar(50) NULL,
    CONSTRAINT [PK_Faculty] PRIMARY KEY ([FacultyId]),
    CONSTRAINT [FK_Faculty_AspNetUsers_FacultyId] FOREIGN KEY ([FacultyId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [GradingTerms] (
    [GradingTermId] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [SchoolId] bigint NOT NULL,
    [Name] nvarchar(100) NULL,
    [StartDate] datetime2 NOT NULL,
    [EndDate] datetime2 NOT NULL,
    CONSTRAINT [PK_GradingTerms] PRIMARY KEY ([GradingTermId])
);

GO

CREATE TABLE [KeyValues] (
    [KeyValueID] bigint NOT NULL IDENTITY,
    [DistrictID] bigint NULL,
    [SchoolID] bigint NULL,
    [KeyName] nvarchar(50) NULL,
    [TextValue] nvarchar(100) NULL,
    [OrderIndex] int NOT NULL,
    [IsActive] bit NOT NULL,
    CONSTRAINT [PK_KeyValues] PRIMARY KEY ([KeyValueID])
);

GO

CREATE TABLE [Localizations] (
    [RecordId] bigint NOT NULL IDENTITY,
    [Locale] nvarchar(max) NULL,
    [Key] nvarchar(max) NULL,
    [Value] nvarchar(max) NULL,
    CONSTRAINT [PK_Localizations] PRIMARY KEY ([RecordId])
);

GO

CREATE TABLE [LoginLogs] (
    [LoginLogID] bigint NOT NULL IDENTITY,
    [LoginContent] nvarchar(50) NULL,
    [Success] bit NOT NULL,
    [LoginTime] datetime2 NULL,
    [RemoteIP] nvarchar(20) NULL,
    [UserID] bigint NULL,
    [Domain] nvarchar(50) NULL,
    [Role] nvarchar(50) NULL,
    CONSTRAINT [PK_LoginLogs] PRIMARY KEY ([LoginLogID])
);

GO

CREATE TABLE [Parents] (
    [ParentId] bigint NOT NULL,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    CONSTRAINT [PK_Parents] PRIMARY KEY ([ParentId]),
    CONSTRAINT [FK_Parents_AspNetUsers_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [ParentStudent] (
    [ParentId] bigint NOT NULL,
    [StudentId] bigint NOT NULL,
    CONSTRAINT [PK_ParentStudent] PRIMARY KEY ([ParentId], [StudentId]),
    CONSTRAINT [FK_ParentStudent_AspNetUsers_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ParentStudent_AspNetUsers_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Polls] (
    [PollId] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [StudentId] bigint NOT NULL,
    [ActivityId] bigint NOT NULL,
    [PollQuizId] nvarchar(50) NOT NULL,
    [VoteContent] tinyint NOT NULL,
    CONSTRAINT [PK_Polls] PRIMARY KEY ([PollId])
);

GO

CREATE TABLE [Students] (
    [StudentId] bigint NOT NULL,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [ExternalId] nvarchar(50) NULL,
    [StudentNumber] nvarchar(20) NOT NULL,
    CONSTRAINT [PK_Students] PRIMARY KEY ([StudentId]),
    CONSTRAINT [FK_Students_AspNetUsers_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [UserNotificationSettings] (
    [UserId] bigint NOT NULL,
    [Type] int NOT NULL,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [SendType] int NOT NULL,
    [SendTime] time NOT NULL,
    [WeeklyDay] tinyint NOT NULL,
    CONSTRAINT [PK_UserNotificationSettings] PRIMARY KEY ([UserId], [Type]),
    CONSTRAINT [FK_UserNotificationSettings_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Schools] (
    [SchoolId] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [SchoolName] nvarchar(100) NULL,
    [CurrentGradingTerm] bigint NOT NULL,
    [DistrictId] bigint NOT NULL,
    [LogoUrl] nvarchar(2000) NULL,
    [IconUrl] nvarchar(2000) NULL,
    CONSTRAINT [PK_Schools] PRIMARY KEY ([SchoolId]),
    CONSTRAINT [FK_Schools_Districts_DistrictId] FOREIGN KEY ([DistrictId]) REFERENCES [Districts] ([DistrictId]) ON DELETE CASCADE
);

GO

CREATE TABLE [Classes] (
    [ClassId] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [ClassName] nvarchar(50) NULL,
    [Period] nvarchar(10) NULL,
    [GradingTerm] bigint NOT NULL,
    [SchoolId] bigint NOT NULL,
    [Description] nvarchar(500) NULL,
    [UseDefaultGradesRange] bit NOT NULL,
    [GradeRangesJson] nvarchar(2000) NULL,
    [IsTemplate] bit NOT NULL,
    CONSTRAINT [PK_Classes] PRIMARY KEY ([ClassId]),
    CONSTRAINT [FK_Classes_GradingTerms_GradingTerm] FOREIGN KEY ([GradingTerm]) REFERENCES [GradingTerms] ([GradingTermId]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Classes_Schools_SchoolId] FOREIGN KEY ([SchoolId]) REFERENCES [Schools] ([SchoolId]) ON DELETE NO ACTION
);

GO

CREATE TABLE [SchoolAdmin] (
    [UserId] bigint NOT NULL,
    [SchoolId] bigint NOT NULL,
    CONSTRAINT [PK_SchoolAdmin] PRIMARY KEY ([UserId], [SchoolId]),
    CONSTRAINT [FK_SchoolAdmin_Schools_SchoolId] FOREIGN KEY ([SchoolId]) REFERENCES [Schools] ([SchoolId]) ON DELETE NO ACTION,
    CONSTRAINT [FK_SchoolAdmin_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [SchoolFaculty] (
    [FacultyId] bigint NOT NULL,
    [SchoolId] bigint NOT NULL,
    CONSTRAINT [PK_SchoolFaculty] PRIMARY KEY ([SchoolId], [FacultyId]),
    CONSTRAINT [FK_SchoolFaculty_AspNetUsers_FacultyId] FOREIGN KEY ([FacultyId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_SchoolFaculty_Schools_SchoolId] FOREIGN KEY ([SchoolId]) REFERENCES [Schools] ([SchoolId]) ON DELETE CASCADE
);

GO

CREATE TABLE [SchoolStudent] (
    [SchoolId] bigint NOT NULL,
    [StudentId] bigint NOT NULL,
    [GradingPeriod] bigint NOT NULL,
    [Grade] decimal(7,6) NULL,
    CONSTRAINT [PK_SchoolStudent] PRIMARY KEY ([SchoolId], [StudentId], [GradingPeriod]),
    CONSTRAINT [FK_SchoolStudent_Schools_SchoolId] FOREIGN KEY ([SchoolId]) REFERENCES [Schools] ([SchoolId]) ON DELETE CASCADE,
    CONSTRAINT [FK_SchoolStudent_AspNetUsers_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Activities] (
    [ActivityId] bigint NOT NULL IDENTITY,
    [DateCreated] datetime2 NULL,
    [DateUpdated] datetime2 NULL,
    [CreatedBy] bigint NULL,
    [UpdatedBy] bigint NULL,
    [IsDeleted] bit NOT NULL,
    [ClassId] bigint NOT NULL,
    [Type] int NOT NULL,
    [Title] nvarchar(200) NULL,
    [Description] nvarchar(500) NULL,
    [DateDue] datetime2 NULL,
    [DateAssigned] datetime2 NULL,
    [DateCutoff] datetime2 NULL,
    [SortIndex] int NOT NULL,
    [ParentActivityId] bigint NULL,
    [PublishStartDate] datetime2 NULL,
    [PublishEndDate] datetime2 NULL,
    [CategoryId] bigint NULL,
    [Color] nvarchar(10) NULL,
    [IsGraded] bit NOT NULL,
    [MaxScore] decimal(9, 2) NOT NULL,
    [Weight] decimal(9, 2) NOT NULL,
    CONSTRAINT [PK_Activities] PRIMARY KEY ([ActivityId]),
    CONSTRAINT [FK_Activities_Classes_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Classes] ([ClassId]) ON DELETE CASCADE
);

GO

CREATE TABLE [ClassFaculty] (
    [ClassId] bigint NOT NULL,
    [FacultyId] bigint NOT NULL,
    [SortIndex] int NOT NULL,
    CONSTRAINT [PK_ClassFaculty] PRIMARY KEY ([ClassId], [FacultyId]),
    CONSTRAINT [FK_ClassFaculty_Classes_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Classes] ([ClassId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ClassFaculty_AspNetUsers_FacultyId] FOREIGN KEY ([FacultyId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [ClassStudent] (
    [ClassId] bigint NOT NULL,
    [StudentId] bigint NOT NULL,
    [CumulativeGrade] decimal(7,6) NULL,
    [SortIndex] int NOT NULL,
    [LastAccessed] datetime2 NULL,
    CONSTRAINT [PK_ClassStudent] PRIMARY KEY ([ClassId], [StudentId]),
    CONSTRAINT [FK_ClassStudent_Classes_ClassId] FOREIGN KEY ([ClassId]) REFERENCES [Classes] ([ClassId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ClassStudent_AspNetUsers_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [ActivityScores] (
    [ActivityId] bigint NOT NULL,
    [StudentId] bigint NOT NULL,
    [Score] decimal(9, 2) NULL,
    [GradeDate] datetime2 NULL,
    [SavedDate] datetime2 NULL,
    [IsSubmitted] bit NOT NULL,
    [Communication] tinyint NULL,
    [Collaboration] tinyint NULL,
    [Character] tinyint NULL,
    [Creativity] tinyint NULL,
    [CriticalThinking] tinyint NULL,
    [ThreadId] nvarchar(256) NULL,
    CONSTRAINT [PK_ActivityScores] PRIMARY KEY ([ActivityId], [StudentId]),
    CONSTRAINT [FK_ActivityScores_Activities_ActivityId] FOREIGN KEY ([ActivityId]) REFERENCES [Activities] ([ActivityId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ActivityScores_AspNetUsers_StudentId] FOREIGN KEY ([StudentId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_Activities_ClassId] ON [Activities] ([ClassId]);

GO

CREATE INDEX [IX_ActivityScores_StudentId] ON [ActivityScores] ([StudentId]);

GO

CREATE INDEX [IX_Classes_GradingTerm] ON [Classes] ([GradingTerm]);

GO

CREATE INDEX [IX_Classes_SchoolId] ON [Classes] ([SchoolId]);

GO

CREATE INDEX [IX_ClassFaculty_FacultyId] ON [ClassFaculty] ([FacultyId]);

GO

CREATE INDEX [IX_ClassStudent_StudentId] ON [ClassStudent] ([StudentId]);

GO

CREATE INDEX [IX_Comments_ThreadId] ON [Comments] ([ThreadId]);

GO

CREATE UNIQUE INDEX [IX_Discussions_ThreadId] ON [Discussions] ([ThreadId]) WHERE [ThreadId] IS NOT NULL;

GO

CREATE UNIQUE INDEX [IX_Districts_Domain] ON [Districts] ([Domain]) WHERE [Domain] IS NOT NULL;

GO

CREATE INDEX [IX_ParentStudent_StudentId] ON [ParentStudent] ([StudentId]);

GO

CREATE INDEX [IX_SchoolAdmin_SchoolId] ON [SchoolAdmin] ([SchoolId]);

GO

CREATE INDEX [IX_SchoolFaculty_FacultyId] ON [SchoolFaculty] ([FacultyId]);

GO

CREATE INDEX [IX_Schools_DistrictId] ON [Schools] ([DistrictId]);

GO

CREATE INDEX [IX_SchoolStudent_StudentId] ON [SchoolStudent] ([StudentId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20191002053904_Init_Business_Data_Tables', N'2.2.6-servicing-10079');

GO

