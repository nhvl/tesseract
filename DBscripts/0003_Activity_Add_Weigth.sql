﻿ALTER TABLE [ActivityScores] ADD [IsExclude] bit NOT NULL DEFAULT 0;

GO

ALTER TABLE [Activities] ADD [IsCredit] bit NOT NULL DEFAULT 0;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20191004061518_Activity_Add_Weigth', N'2.2.6-servicing-10079');

GO

